\begin{thebibliography}{30}
  
  %
\bibitem{darkmatter}V. Trimble.
  \newblock \textit{Existence and nature of dark matter in the universe}.
  \newblock  Annual Review of Astronomy and Astrophysics. 25: pp. 425–472,
  \newblock 1987,\\
  \newblock URL: https://escholarship.org/content/qt2hz008rs/qt2hz008rs.pdf
  %
\bibitem{darkmatter2} G. Bertone, D. Hooper and J. Silk.
  \newblock \textit{Particle Dark Matter: Evidence, Candidates and Constraints}.
  \newblock  Physics Reports Volume 405, pp. 279-390,
  \newblock 2005,\\
  \newblock URL: https://www.sciencedirect.com/science/article/pii/S0370157304003515
  %
%\bibitem{ewsb}J. Bagger et al.
%  \newblock \textit{The Strongly Interacting WW System: Gold–Plated Modes}.
%  \newblock Phys. Rev. D 49 (1994) 1246,
%  \newblock URL: https://arxiv.org/abs/hep-ph/9306256.
%
\bibitem{ATLAS}ATLAS experiment.
\newblock URL: https://atlas.cern/
%
\bibitem{ALICE}ALICE experiment.
\newblock URL: https://alice-collaboration.web.cern.ch/
%
\bibitem{CMS}CMS experiment.
\newblock URL: https://cms.cern/
%
\bibitem{LHCb}LHCb.
\newblock URL: https://home.cern/science/experiments/lhcb/
%
\bibitem{diverseexperiments}Diverse experiments at CERN.
\newblock URL: https://home.cern/science/experiments/
%
\bibitem{lhcmagnet}L. Rossi.
  \newblock \textit{THE LHC SUPERCONDUCTING MAGNETS}.
  \newblock Proceedings of the 2003 Particle Accelerator Conference, pp. 141-145,
  \newblock 2003. \\
  \newblock URL: https://accelconf.web.cern.ch/p03/papers/toab001.pdf
  %
 \bibitem{acceleratotcomplex}The accelerator complex.\\
\newblock URL: https://public-archive.web.cern.ch/en/research/AccelComplex-en.html
%
\bibitem{atlasexperiment} ATLAS Collaboration.
  \newblock \textit{The ATLAS Experiment at the CERN Large Hadron Collider}.
  \newblock JINST Volume 3, S08003,
  \newblock 2008. \\
  \newblock URL: https://iopscience.iop.org/article/10.1088/1748-0221/3/08/S08003
  %
 \bibitem{atlascrosssection} Event Cross Section in a computer generated image of the ATLAS detector.
\newblock URL: https://cds.cern.ch/record/1096081
%
 \bibitem{innerdetector} The Inner Detector.
\newblock URL: https://atlas.cern/Discover/Detector/Inner-Detector
%
 \bibitem{calorimeter} Calorimeter.
\newblock URL: https://atlas.cern/Discover/Detector/Calorimeter
%
\bibitem{LAr} Henric Wilkens,
  \newblock \textit{The ATLAS Liquid Argon calorimeter: An overview}.
  \newblock J. Phys. :Conf. Ser. 160 012043,
  \newblock 2009, \\
  \newblock URL: https://iopscience.iop.org/article/10.1088/1742-6596/160/1/012043
  %
 \bibitem{muon} Muon Spectrometer.
\newblock URL: https://atlas.cern/Discover/Detector/Muon-Spectrometer
%
\bibitem{NSWTDR}ATLAS Collaboration
\newblock \textit{Technical Design Report New Small Wheel}
\newblock  2013, \\
\newblock URL: https://cds.cern.ch/record/1552862
%    
 \bibitem{magnet} Magnet System.
   \newblock URL: https://atlas.cern/Discover/Detector/Magnet-System
   %
 \bibitem{trigger} Trigger and Data Acquisition.
\newblock URL: https://atlas.cern/Discover/Detector/Trigger-DAQ.
%
\bibitem{Pixels} G. Aad, et al,
  \newblock \textit{ATLAS pixel detector electronics and sensors}.
  \newblock JINST3 P07007,
  \newblock 2008, \\
  \newblock URL: https://iopscience.iop.org/article/10.1088/1748-0221/3/07/P07007
  %
\bibitem{IBL} Y. Takubo on behalf of the ATLAS collaboration.
  \newblock \textit{The Pixel Detector of the ATLAS experiment for the Run2 at the Large Hadron Collider}.
  \newblock 2014, \\
  \newblock URL: https://iopscience.iop.org/article/10.1088/1748-0221/10/12/C12001
  %      
\bibitem{hllhc}HL-LHC project.
  \newblock URL: https://hilumilhc.web.cern.ch/content/hl-lhc-project
  %
\bibitem{ID}ATLAS Collaboration.
  \newblock \textit{Alignment of the ATLAS Inner Detector in Run-2}.
  \newblock Eur. Phys. J. C 80 (2020) 1194,
  \newblock 2021, \\
  \newblock URL: https://arxiv.org/abs/2007.07624
  %      
%
\bibitem{hllhcTDR}O. Aberle et al.
  \newblock \textit{High-Luminosity Large Hadron Collider (HL-LHC): Technical design report}.
  \newblock  2020, \\
  \newblock URL: https://cds.cern.ch/record/2749422,

%
\bibitem{hllhc_wwjj}G. Aad et al. (ATLAS Collaboration).
  \newblock \textit{Prospects for the measurement of the W$^{\pm}$W$^{\pm}$ scattering cross section and extraction of the longitudinal scattering component in pp collisions at the High-Luminosity LHC with the ATLAS Experiment}.
  \newblock 2018, \\
  \newblock URL: https://cds.cern.ch/record/2652447/files/ATL-PHYS-PUB-2018-052.pdf
%  
\bibitem{physicsHLLHC}J. Nielsen et al. (ATLAS Collaboration).
  \newblock \textit{Physics prospects for ATLAS at the HL-LHC}.
  \newblock J. Phys.: Conf. Ser. 1690 012156,
  \newblock ICPPA,
  \newblock 2020, \\
  \newblock URL: https://cds.cern.ch/record/2740777
  %
\bibitem{quarksinglet} J. A. Aguilar–Saavedra.
  \newblock \textit{Effects of mixing with quark singlets}.
  \newblock Phys. Rev. D 67, 035003,
  \newblock 2003, \\
  \newblock URL: https://journals.aps.org/prd/abstract/10.1103/PhysRevD.67.035003
  %
    \bibitem{bsm}ATLAS Collaboration.
  \newblock \textit{Prospects for searches for staus, charginos and neutralinos at the high luminosity LHC with the ATLAS Detector}.
  \newblock ATL-PHYS-PUB-2018-048,
  \newblock 2018,\\
  \newblock URL: https://cds.cern.ch/record/26651927
  %
\bibitem{bethe}H.A Bethe.
  \newblock \textit{Zur Theorie des Durchgangs schneller Korpuskularstrahlen durch Materie}
  \newblock H. Bethe, Ann. Phys. 5, 325,
  \newblock 1930,\\
  %
  \bibitem{pdg} K.Nakamura et al. (Particle Data Group).
  \newblock \textit{Review of Particle Physics}.
  \newblock Journal of Physics G: Nuclear and Particle Physics 37: 075021,
  \newblock 2010, \\
  \newblock URL: https://iopscience.iop.org/article/10.1088/0954-3899/37/7A/075021
  %
\bibitem{J.B.Johnson} J. B. Johnson,
  \newblock \textit{Thermal Agitation of Electricity in Conductors}.
  \newblock  Phys. Rev. 32, pp. 97-109,
  \newblock 1928,\\
  \newblock URL: https://journals.aps.org/pr/abstract/10.1103/PhysRev.32.97
  %
  \bibitem{mcwhorter} A. L. McWhorter,
  \newblock \textit{1/$f$ noise and germanium surface prosperities. In Semiconductor Surface Physics}.
  \newblock  University of Pennsylvania Press, pp. 207-208
  \newblock 1957,\\
  \newblock URL: https://journals.aps.org/pr/abstract/10.1103/PhysRev.32.97
  %     
\bibitem{PixTDR}ATLAS collaborater.
\newblock \textit{Technical Design Report for the ATLAS Inner Tracker Pixel Detector}.
\newblock  2018, \\
\newblock URL: https://cds.cern.ch/record/2285585
%
\bibitem{semicon}S.M.Sze.
\newblock \textit{SEMICONDUCTOR DEVICE}.
\newblock  1987.
%
\bibitem{Leo}William R. Leo.
  \newblock \textit{Techniques for Nuclear and Particle Physics Experiments}.
  \newblock 1994.
%
\bibitem{RD53Amanual} RD53A collaboration.
  \newblock \textit{The RD53A Integrated Circuit}
  \newblock 2019, \\
  \newblock URL: https://cds.cern.ch/record/2287593
  %
\bibitem{sensor_detail} The ATLAS collaboration,
  \newblock \textit{ATLAS ITk Pixel Pre-production Planar Sensor Characterisation for the HL-LHC Upgrade}.
  \newblock 10th International Workshop on Semiconductor Pixel Detectors for Particles and Imaging (Pixel2022),
  \newblock 2022, \\
  \newblock URL: https://cds.cern.ch/record/2847990
  %                              
\bibitem{3Defficiency} S. Terzo, M. Chmeissani, G. Giannini, S. Grinstein, M. Manna, G. Pellegrini, D. Quirionc and D. Vazquez Furelos.
  \newblock \textit{Performance of Irradiated RD53A 3D Pixel Sensors}.
  \newblock JINST 14 P06005,
  \newblock 2019, \\
  \newblock URL: https://iopscience.iop.org/article/10.1088/1748-0221/14/06/P06005
  
   %                                                                                                                                                        
\bibitem{collectedcharge} D. Passeri, F. Moscatelli, A. Morozzi and G.M. Bilei. 
  \newblock \textit{Modeling of Radiation Damage Effects in Silicon Detectors at High Fluences HL-LHC with Sentaurus TCAD}
  \newblock Nucl. Instrum. Methods Phys. Res., A 824,
  \newblock pp. 443-445,
  \newblock 2016, \\
  \newblock URL: https://www.sciencedirect.com/science/article/pii/S0168900215009730
  %  https://cds.cern.ch/record/2237674?ln=ja
  %
\bibitem{niel_damage} M. Huhtinen.
  \newblock \textit{Simulation of non-ionising energy loss and defect formation in silicon},
  \newblock Nucl. Instrum. Methods Phys. Res. A, Accel. Spectrom. Detect. Assoc. Equip., vol. 491,
  \newblock pp. 194-215,
  \newblock 2002, \\
  \newblock URL: https://www.sciencedirect.com/science/article/pii/S0168900202012275
  %
  \bibitem{Moll2} Michael Moll.
  \newblock \textit{Displacement Damage in Silicon Detectors for High Energy Physics},
  \newblock IEEE E TRANSACTIONS ON NUCLEAR SCIENCE, VOL. 65, NO. 8,
  \newblock pp. 1561-1582,
  \newblock 2018, \\
  \newblock URL: https://ieeexplore.ieee.org/document/8331152
  %
\bibitem{Moll} Michael Moll.
  \newblock \textit{Radiation damage in silicon particle detectors: Microscopic defects and macroscopic properties, PhD thesis}.
  \newblock 1999, \\
  \newblock URL: https://mmoll.web.cern.ch/thesis/pdf/moll-thesis.pdf
%
\bibitem{bandgap} Y.P.Varshni.
  \newblock \textit{Temperature dependence of the energy gap in semiconductors}
  \newblock 1967.
%
\bibitem{Moore} Gordon E. Moore.
  \newblock \textit{Cramming more components onto integrated circuits}
  \newblock 1965.
%
\bibitem{moth} MoTH.
  \newblock {https://github.com/sdungs/moth}
  %
\bibitem{ADCcalib} Lingxin Meng.
  \newblock \textit{RD53A Module ADC Calibration}
  \newblock 2021, \\
  \newblock URL: https://indico.cern.ch/event/1056513/contributions/4440048
  %
  \bibitem{NMOScalib} M. Menouni, D. Fougeron, P. Breugnon.
  \newblock \textit{RD53A Temperature measurements preliminary}
  \newblock 2018, \\
  \newblock URL: https://indico.cern.ch/event/708552/contributions/2982835/attachments/1639045/2616263/2018$\_$24april$\_$TEMPSENS.pdf
  %
\bibitem{module-dossier}
  \newblock \textit{Module dossier}
  \newblock URL: https://gitlab.cern.ch/atlas-itk-pixel-systemtest/plotting/module-dossier/-/tree/master.
%
\bibitem{FLIR} FLIR E75.
  \newblock {https://www.flir.com/support/products/e75/}

\end{thebibliography}
