#!/bin/bash


### IMPORTANT ###

# to have access to eos and the data stored there, run the following command before executing this script:
#kinit [lxplususername]@CERN.CH

# to have ROOT available (if not available yet)
source /cvmfs/sft.cern.ch/lcg/views/LCG_102/x86_64-centos7-gcc11-opt/setup.sh

# M6 PP0 Demonstrator has 6 modules, see 
# https://docs.google.com/spreadsheets/d/1zkZHBOGTa0uhf4_xzP_ETSzCNWwt_YxBgpEYIntBs6I/edit#gid=0

# Module QCs information
# https://docs.google.com/spreadsheets/d/14EmEwtx612Y6Ik55RvC1m7ajNLqZCv3i_HY6z0BFEUQ/edit#gid=853604022


# ------------------------------------------------------------------
# Plot the IV curve comparisons for the modules in the M6 demonstrator

ConfFile=Configuration/M6Demonstrator/PlotConfig_IVcurves.json
echo "Running with ConfFile ${ConfFile}"
python3 Code/IVcurve_comparison.py -p ${ConfFile} -o Figure/M6Demonstrator_IVcurveComparison
