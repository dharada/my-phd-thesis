#!/bin/bash


### IMPORTANT ###
# This: source /cvmfs/sft.cern.ch/lcg/views/LCG_100/x86_64-centos7-gcc10-opt/setup.sh might not be enough.
# If this script is executed, Root will randomly seg fault in the PlotConfig_threshFWB.json creation
# Solution for now: Run the threshFWB plots separately in the terminal
# Note: Seems to be fixed in new ROOT version

# "Stage6": "/home/itkpix/data/M6Demonstrator_ItkSw"

# to have access to eos and the data stored there, run the following command before executing this script:
#kinit [lxplususername]@CERN.CH

# to have ROOT available (if not available yet)
source /cvmfs/sft.cern.ch/lcg/views/LCG_102/x86_64-centos7-gcc11-opt/setup.sh
# source /cvmfs/sft.cern.ch/lcg/views/LCG_100/x86_64-centos7-gcc10-opt/setup.sh

# M6 PP0 Demonstrator has 6 modules, see 
# https://docs.google.com/spreadsheets/d/1zkZHBOGTa0uhf4_xzP_ETSzCNWwt_YxBgpEYIntBs6I/edit#gid=0

# Module QCs information
# https://docs.google.com/spreadsheets/d/14EmEwtx612Y6Ik55RvC1m7ajNLqZCv3i_HY6z0BFEUQ/edit#gid=853604022

# Plot the module comparisons for the modules in the M6 demonstrator
SubFolder=M6M12Demonstrator_QualityTracking_RealisticServices # For results 
ConfigFolder=Configuration/M6M12Demonstrator/RealisticServices/

#########
# M6
#########

# M1: CERN Thin Q10
ConfFile=${ConfigFolder}/GenConfig_Thin10.json
Module=CERN_Thin_Q10_20UPGM20025080
# https://docs.google.com/spreadsheets/d/14EmEwtx612Y6Ik55RvC1m7ajNLqZCv3i_HY6z0BFEUQ/edit#gid=1895845860&range=A222
                                                                                                                         
echo "Running with Module ${Module}"
#echo "running with PlotConfig_dig.json"
#python3 Code/stages_comparison.py -c ${ConfFile} -o Figure/${SubFolder}/${Module}/ -p Configuration/PlotConfig_dig.json 
#echo "running with PlotConfig_ana.json"                          
#python3 Code/stages_comparison.py -c ${ConfFile} -o Figure/${SubFolder}/${Module}/ -p Configuration/PlotConfig_ana.json
# echo "running with PlotConfig_cross.json"
# python3 Code/stages_comparison.py -c ${ConfFile} -o Figure/${SubFolder}/${Module}/ -p Configuration/PlotConfig_cross.json
#echo "running with PlotConfig_dcb.json"
#python3 Code/stages_comparison.py -c ${ConfFile} -o Figure/${SubFolder}/${Module}/ -p Configuration/PlotConfig_dcb.json
echo "running with PlotConfig_threshFWB.json"
python3 Code/stages_comparison.py -c ${ConfFile} -o Figure/${SubFolder}/${Module}/ -p Configuration/PlotConfig_threshFWB.json
# python3 Code/stages_comparison.py -c Configuration/GenConfig_Thin10.json -o Figure/M6Demonstrator/CERN_Thin_Q10_20UPGM20025080/ -p Configuration/PlotConfig_threshFWB.json

# M2: Paris 10
ConfFile=${ConfigFolder}/GenConfig_Paris10.json
Module=Paris10
# https://docs.google.com/spreadsheets/d/14EmEwtx612Y6Ik55RvC1m7ajNLqZCv3i_HY6z0BFEUQ/edit#gid=1895845860&range=A275
                                                                                                                         
echo "Running with Module ${Module}"
#echo "running with PlotConfig_dig.json"
#python3 Code/stages_comparison.py -c ${ConfFile} -o Figure/${SubFolder}/${Module}/ -p Configuration/PlotConfig_dig.json 
#echo "running with PlotConfig_ana.json"                          
#python3 Code/stages_comparison.py -c ${ConfFile} -o Figure/${SubFolder}/${Module}/ -p Configuration/PlotConfig_ana.json
# echo "running with PlotConfig_cross.json"
# python3 Code/stages_comparison.py -c ${ConfFile} -o Figure/${SubFolder}/${Module}/ -p Configuration/PlotConfig_cross.json
#echo "running with PlotConfig_dcb.json"
#python3 Code/stages_comparison.py -c ${ConfFile} -o Figure/${SubFolder}/${Module}/ -p Configuration/PlotConfig_dcb.json
echo "running with PlotConfig_threshFWB.json"
python3 Code/stages_comparison.py -c ${ConfFile} -o Figure/${SubFolder}/${Module}/ -p Configuration/PlotConfig_threshFWB.json
# python3 Code/stages_comparison.py -c Configuration/GenConfig_Paris10.json -o Figure/M6Demonstrator/Paris10/ -p Configuration/PlotConfig_threshFWB.json

# M3: Paris 3 (doesn't work)
ConfFile=${ConfigFolder}/GenConfig_Paris3.json
Module=Paris3
# https://docs.google.com/spreadsheets/d/14EmEwtx612Y6Ik55RvC1m7ajNLqZCv3i_HY6z0BFEUQ/edit#gid=1895845860&range=A345
                                                                                                                         
# echo "Running with Module ${Module}"
# echo "running with PlotConfig_dig.json"
# python3 Code/stages_comparison.py -c ${ConfFile} -o Figure/${SubFolder}/${Module}/ -p Configuration/PlotConfig_dig.json 
# echo "running with PlotConfig_ana.json"                          
# python3 Code/stages_comparison.py -c ${ConfFile} -o Figure/${SubFolder}/${Module}/ -p Configuration/PlotConfig_ana.json
# echo "running with PlotConfig_cross.json"
# python3 Code/stages_comparison.py -c ${ConfFile} -o Figure/${SubFolder}/${Module}/ -p Configuration/PlotConfig_cross.json
# echo "running with PlotConfig_dcb.json"
# python3 Code/stages_comparison.py -c ${ConfFile} -o Figure/${SubFolder}/${Module}/ -p Configuration/PlotConfig_dcb.json
# echo "running with PlotConfig_threshFWB.json"
# python3 Code/stages_comparison.py -c ${ConfFile} -o Figure/${SubFolder}/${Module}/ -p Configuration/PlotConfig_threshFWB.json


# M4: Siegen 1
ConfFile=${ConfigFolder}/GenConfig_Siegen1.json
Module=SiegenQ1
# https://docs.google.com/spreadsheets/d/14EmEwtx612Y6Ik55RvC1m7ajNLqZCv3i_HY6z0BFEUQ/edit#gid=1895845860&range=A406
                                                                                                                         
echo "Running with Module ${Module}"
#echo "running with PlotConfig_dig.json"
#python3 Code/stages_comparison.py -c ${ConfFile} -o Figure/${SubFolder}/${Module}/ -p Configuration/PlotConfig_dig.json 
#echo "running with PlotConfig_ana.json"                          
#python3 Code/stages_comparison.py -c ${ConfFile} -o Figure/${SubFolder}/${Module}/ -p Configuration/PlotConfig_ana.json
# echo "running with PlotConfig_cross.json"
# python3 Code/stages_comparison.py -c ${ConfFile} -o Figure/${SubFolder}/${Module}/ -p Configuration/PlotConfig_cross.json
#echo "running with PlotConfig_dcb.json"
#python3 Code/stages_comparison.py -c ${ConfFile} -o Figure/${SubFolder}/${Module}/ -p Configuration/PlotConfig_dcb.json
echo "running with PlotConfig_threshFWB.json"
python3 Code/stages_comparison.py -c ${ConfFile} -o Figure/${SubFolder}/${Module}/ -p Configuration/PlotConfig_threshFWB.json
# python3 Code/stages_comparison.py -c Configuration/GenConfig_Siegen1.json -o Figure/M6Demonstrator/SiegenQ1/ -p Configuration/PlotConfig_threshFWB.json


# M5: Paris 12
ConfFile=${ConfigFolder}/GenConfig_Paris12.json
Module=Paris12
# https://docs.google.com/spreadsheets/d/14EmEwtx612Y6Ik55RvC1m7ajNLqZCv3i_HY6z0BFEUQ/edit#gid=1895845860&range=A305
                                                                                                                         
echo "Running with Module ${Module}"
#echo "running with PlotConfig_dig.json"
#python3 Code/stages_comparison.py -c ${ConfFile} -o Figure/${SubFolder}/${Module}/ -p Configuration/PlotConfig_dig.json 
#echo "running with PlotConfig_ana.json"                          
#python3 Code/stages_comparison.py -c ${ConfFile} -o Figure/${SubFolder}/${Module}/ -p Configuration/PlotConfig_ana.json
# echo "running with PlotConfig_cross.json"
# python3 Code/stages_comparison.py -c ${ConfFile} -o Figure/${SubFolder}/${Module}/ -p Configuration/PlotConfig_cross.json
#echo "running with PlotConfig_dcb.json"
#python3 Code/stages_comparison.py -c ${ConfFile} -o Figure/${SubFolder}/${Module}/ -p Configuration/PlotConfig_dcb.json
echo "running with PlotConfig_threshFWB.json"
python3 Code/stages_comparison.py -c ${ConfFile} -o Figure/${SubFolder}/${Module}/ -p Configuration/PlotConfig_threshFWB.json
# python3 Code/stages_comparison.py -c Configuration/GenConfig_Paris12.json -o Figure/M6Demonstrator/Paris12/ -p Configuration/PlotConfig_threshFWB.json


# M6: Siegen 2
ConfFile=${ConfigFolder}/GenConfig_Siegen2.json
Module=SiegenQ2
# https://docs.google.com/spreadsheets/d/14EmEwtx612Y6Ik55RvC1m7ajNLqZCv3i_HY6z0BFEUQ/edit#gid=1895845860&range=A417
                                                                                                                         
echo "Running with Module ${Module}"
#echo "running with PlotConfig_dig.json"
#python3 Code/stages_comparison.py -c ${ConfFile} -o Figure/${SubFolder}/${Module}/ -p Configuration/PlotConfig_dig.json 
#echo "running with PlotConfig_ana.json"                          
#python3 Code/stages_comparison.py -c ${ConfFile} -o Figure/${SubFolder}/${Module}/ -p Configuration/PlotConfig_ana.json
# echo "running with PlotConfig_cross.json"
# python3 Code/stages_comparison.py -c ${ConfFile} -o Figure/${SubFolder}/${Module}/ -p Configuration/PlotConfig_cross.json
#echo "running with PlotConfig_dcb.json"
#python3 Code/stages_comparison.py -c ${ConfFile} -o Figure/${SubFolder}/${Module}/ -p Configuration/PlotConfig_dcb.json
echo "running with PlotConfig_threshFWB.json"
python3 Code/stages_comparison.py -c ${ConfFile} -o Figure/${SubFolder}/${Module}/ -p Configuration/PlotConfig_threshFWB.json
# python3 Code/stages_comparison.py -c Configuration/GenConfig_Siegen2.json -o Figure/M6Demonstrator/SiegenQ2/ -p Configuration/PlotConfig_threshFWB.json


#########
# M12
#########

# M1: SiegenQ3
ConfFile=${ConfigFolder}/GenConfig_SiegenQ3.json
Module=SiegenQ3
                                                                                                                         
echo "Running with Module ${Module}"
#echo "running with PlotConfig_dig.json"
#python3 Code/stages_comparison.py -c ${ConfFile} -o Figure/${SubFolder}/${Module}/ -p Configuration/PlotConfig_dig.json 
echo "running with PlotConfig_ana.json"                          
python3 Code/stages_comparison.py -c ${ConfFile} -o Figure/${SubFolder}/${Module}/ -p Configuration/PlotConfig_ana.json
# echo "running with PlotConfig_cross.json"
# python3 Code/stages_comparison.py -c ${ConfFile} -o Figure/${SubFolder}/${Module}/ -p Configuration/PlotConfig_cross.json
# echo "running with PlotConfig_dcb.json"
# python3 Code/stages_comparison.py -c ${ConfFile} -o Figure/${SubFolder}/${Module}/ -p Configuration/PlotConfig_dcb.json
echo "running with PlotConfig_threshFWB.json"
python3 Code/stages_comparison.py -c ${ConfFile} -o Figure/${SubFolder}/${Module}/ -p Configuration/PlotConfig_threshFWB.json

# M2: CERN ThinQ11
ConfFile=${ConfigFolder}/GenConfig_Thin11.json
Module=CERN_Thin_Q11_20UPGM20025141
                                                                                                                         
echo "Running with Module ${Module}"
#echo "running with PlotConfig_dig.json"
#python3 Code/stages_comparison.py -c ${ConfFile} -o Figure/${SubFolder}/${Module}/ -p Configuration/PlotConfig_dig.json 
echo "running with PlotConfig_ana.json"                          
python3 Code/stages_comparison.py -c ${ConfFile} -o Figure/${SubFolder}/${Module}/ -p Configuration/PlotConfig_ana.json
# echo "running with PlotConfig_cross.json"
# python3 Code/stages_comparison.py -c ${ConfFile} -o Figure/${SubFolder}/${Module}/ -p Configuration/PlotConfig_cross.json
# echo "running with PlotConfig_dcb.json"
#python3 Code/stages_comparison.py -c ${ConfFile} -o Figure/${SubFolder}/${Module}/ -p Configuration/PlotConfig_dcb.json
echo "running with PlotConfig_threshFWB.json"
python3 Code/stages_comparison.py -c ${ConfFile} -o Figure/${SubFolder}/${Module}/ -p Configuration/PlotConfig_threshFWB.json

# M3: Paris9
ConfFile=${ConfigFolder}/GenConfig_Paris9.json
Module=Paris9
                                                                                                                         
echo "Running with Module ${Module}"
#echo "running with PlotConfig_dig.json"
#python3 Code/stages_comparison.py -c ${ConfFile} -o Figure/${SubFolder}/${Module}/ -p Configuration/PlotConfig_dig.json 
echo "running with PlotConfig_ana.json"                          
python3 Code/stages_comparison.py -c ${ConfFile} -o Figure/${SubFolder}/${Module}/ -p Configuration/PlotConfig_ana.json
# echo "running with PlotConfig_cross.json"
# python3 Code/stages_comparison.py -c ${ConfFile} -o Figure/${SubFolder}/${Module}/ -p Configuration/PlotConfig_cross.json
# echo "running with PlotConfig_dcb.json"
# python3 Code/stages_comparison.py -c ${ConfFile} -o Figure/${SubFolder}/${Module}/ -p Configuration/PlotConfig_dcb.json
echo "running with PlotConfig_threshFWB.json"
python3 Code/stages_comparison.py -c ${ConfFile} -o Figure/${SubFolder}/${Module}/ -p Configuration/PlotConfig_threshFWB.json

# M4: SiegenQ4
ConfFile=${ConfigFolder}/GenConfig_SiegenQ4.json
Module=SiegenQ4_Si_RD53A_Q6
                                                                                                                         
echo "Running with Module ${Module}"
#echo "running with PlotConfig_dig.json"
#python3 Code/stages_comparison.py -c ${ConfFile} -o Figure/${SubFolder}/${Module}/ -p Configuration/PlotConfig_dig.json 
echo "running with PlotConfig_ana.json"                          
python3 Code/stages_comparison.py -c ${ConfFile} -o Figure/${SubFolder}/${Module}/ -p Configuration/PlotConfig_ana.json
# echo "running with PlotConfig_cross.json"
# python3 Code/stages_comparison.py -c ${ConfFile} -o Figure/${SubFolder}/${Module}/ -p Configuration/PlotConfig_cross.json
# echo "running with PlotConfig_dcb.json"
# python3 Code/stages_comparison.py -c ${ConfFile} -o Figure/${SubFolder}/${Module}/ -p Configuration/PlotConfig_dcb.json
echo "running with PlotConfig_threshFWB.json"
python3 Code/stages_comparison.py -c ${ConfFile} -o Figure/${SubFolder}/${Module}/ -p Configuration/PlotConfig_threshFWB.json

# M5: Paris7
ConfFile=${ConfigFolder}/GenConfig_Paris7.json
Module=Paris7
                                                                                                                         
echo "Running with Module ${Module}"
#echo "running with PlotConfig_dig.json"
#python3 Code/stages_comparison.py -c ${ConfFile} -o Figure/${SubFolder}/${Module}/ -p Configuration/PlotConfig_dig.json 
echo "running with PlotConfig_ana.json"                          
python3 Code/stages_comparison.py -c ${ConfFile} -o Figure/${SubFolder}/${Module}/ -p Configuration/PlotConfig_ana.json
# echo "running with PlotConfig_cross.json"
# python3 Code/stages_comparison.py -c ${ConfFile} -o Figure/${SubFolder}/${Module}/ -p Configuration/PlotConfig_cross.json
# echo "running with PlotConfig_dcb.json"
# python3 Code/stages_comparison.py -c ${ConfFile} -o Figure/${SubFolder}/${Module}/ -p Configuration/PlotConfig_dcb.json
echo "running with PlotConfig_threshFWB.json"
python3 Code/stages_comparison.py -c ${ConfFile} -o Figure/${SubFolder}/${Module}/ -p Configuration/PlotConfig_threshFWB.json

# M6: Paris13
ConfFile=${ConfigFolder}/GenConfig_Paris13.json
Module=Paris13
                                                                                                                         
echo "Running with Module ${Module}"
#echo "running with PlotConfig_dig.json"
#python3 Code/stages_comparison.py -c ${ConfFile} -o Figure/${SubFolder}/${Module}/ -p Configuration/PlotConfig_dig.json 
echo "running with PlotConfig_ana.json"                          
python3 Code/stages_comparison.py -c ${ConfFile} -o Figure/${SubFolder}/${Module}/ -p Configuration/PlotConfig_ana.json
# echo "running with PlotConfig_cross.json"
# python3 Code/stages_comparison.py -c ${ConfFile} -o Figure/${SubFolder}/${Module}/ -p Configuration/PlotConfig_cross.json
# echo "running with PlotConfig_dcb.json"
# python3 Code/stages_comparison.py -c ${ConfFile} -o Figure/${SubFolder}/${Module}/ -p Configuration/PlotConfig_dcb.json
echo "running with PlotConfig_threshFWB.json"
python3 Code/stages_comparison.py -c ${ConfFile} -o Figure/${SubFolder}/${Module}/ -p Configuration/PlotConfig_threshFWB.json

# M7: Goe10
ConfFile=${ConfigFolder}/GenConfig_Goe10.json
Module=Goe10
                                                                                                                         
echo "Running with Module ${Module}"
#echo "running with PlotConfig_dig.json"
#python3 Code/stages_comparison.py -c ${ConfFile} -o Figure/${SubFolder}/${Module}/ -p Configuration/PlotConfig_dig.json 
echo "running with PlotConfig_ana.json"                          
python3 Code/stages_comparison.py -c ${ConfFile} -o Figure/${SubFolder}/${Module}/ -p Configuration/PlotConfig_ana.json
# echo "running with PlotConfig_cross.json"
# python3 Code/stages_comparison.py -c ${ConfFile} -o Figure/${SubFolder}/${Module}/ -p Configuration/PlotConfig_cross.json
# echo "running with PlotConfig_dcb.json"
# python3 Code/stages_comparison.py -c ${ConfFile} -o Figure/${SubFolder}/${Module}/ -p Configuration/PlotConfig_dcb.json
echo "running with PlotConfig_threshFWB.json"
python3 Code/stages_comparison.py -c ${ConfFile} -o Figure/${SubFolder}/${Module}/ -p Configuration/PlotConfig_threshFWB.json

# M8: CERN ThinQ8
ConfFile=${ConfigFolder}/GenConfig_Thin8.json
Module=CERN_Thin_Q8_20UPGM20025078
                                                                                                                         
echo "Running with Module ${Module}"
#echo "running with PlotConfig_dig.json"
#python3 Code/stages_comparison.py -c ${ConfFile} -o Figure/${SubFolder}/${Module}/ -p Configuration/PlotConfig_dig.json 
echo "running with PlotConfig_ana.json"                          
python3 Code/stages_comparison.py -c ${ConfFile} -o Figure/${SubFolder}/${Module}/ -p Configuration/PlotConfig_ana.json
# echo "running with PlotConfig_cross.json"
# python3 Code/stages_comparison.py -c ${ConfFile} -o Figure/${SubFolder}/${Module}/ -p Configuration/PlotConfig_cross.json
# echo "running with PlotConfig_dcb.json"
# python3 Code/stages_comparison.py -c ${ConfFile} -o Figure/${SubFolder}/${Module}/ -p Configuration/PlotConfig_dcb.json
echo "running with PlotConfig_threshFWB.json"
python3 Code/stages_comparison.py -c ${ConfFile} -o Figure/${SubFolder}/${Module}/ -p Configuration/PlotConfig_threshFWB.json

# M9: Goe4
ConfFile=${ConfigFolder}/GenConfig_Goe4.json
Module=Goe4
                                                                                                                         
echo "Running with Module ${Module}"
#echo "running with PlotConfig_dig.json"
#python3 Code/stages_comparison.py -c ${ConfFile} -o Figure/${SubFolder}/${Module}/ -p Configuration/PlotConfig_dig.json 
echo "running with PlotConfig_ana.json"                          
python3 Code/stages_comparison.py -c ${ConfFile} -o Figure/${SubFolder}/${Module}/ -p Configuration/PlotConfig_ana.json
# echo "running with PlotConfig_cross.json"
# python3 Code/stages_comparison.py -c ${ConfFile} -o Figure/${SubFolder}/${Module}/ -p Configuration/PlotConfig_cross.json
# echo "running with PlotConfig_dcb.json"
# python3 Code/stages_comparison.py -c ${ConfFile} -o Figure/${SubFolder}/${Module}/ -p Configuration/PlotConfig_dcb.json
echo "running with PlotConfig_threshFWB.json"
python3 Code/stages_comparison.py -c ${ConfFile} -o Figure/${SubFolder}/${Module}/ -p Configuration/PlotConfig_threshFWB.json

# M10: CERN ThinQ4
ConfFile=${ConfigFolder}/GenConfig_Thin4.json
Module=CERN_Thin_Q4_20UPGM20023012
                                                                                                                         
echo "Running with Module ${Module}"
#echo "running with PlotConfig_dig.json"
#python3 Code/stages_comparison.py -c ${ConfFile} -o Figure/${SubFolder}/${Module}/ -p Configuration/PlotConfig_dig.json 
echo "running with PlotConfig_ana.json"                          
python3 Code/stages_comparison.py -c ${ConfFile} -o Figure/${SubFolder}/${Module}/ -p Configuration/PlotConfig_ana.json
# echo "running with PlotConfig_cross.json"
# python3 Code/stages_comparison.py -c ${ConfFile} -o Figure/${SubFolder}/${Module}/ -p Configuration/PlotConfig_cross.json
# echo "running with PlotConfig_dcb.json"
# python3 Code/stages_comparison.py -c ${ConfFile} -o Figure/${SubFolder}/${Module}/ -p Configuration/PlotConfig_dcb.json
echo "running with PlotConfig_threshFWB.json"
python3 Code/stages_comparison.py -c ${ConfFile} -o Figure/${SubFolder}/${Module}/ -p Configuration/PlotConfig_threshFWB.json

# M11: KEKQ20
ConfFile=${ConfigFolder}/GenConfig_KEKQ20.json
Module=KEKQ20
                                                                                                                         
echo "Running with Module ${Module}"
#echo "running with PlotConfig_dig.json"
#python3 Code/stages_comparison.py -c ${ConfFile} -o Figure/${SubFolder}/${Module}/ -p Configuration/PlotConfig_dig.json 
echo "running with PlotConfig_ana.json"                          
python3 Code/stages_comparison.py -c ${ConfFile} -o Figure/${SubFolder}/${Module}/ -p Configuration/PlotConfig_ana.json
# echo "running with PlotConfig_cross.json"
# python3 Code/stages_comparison.py -c ${ConfFile} -o Figure/${SubFolder}/${Module}/ -p Configuration/PlotConfig_cross.json
# echo "running with PlotConfig_dcb.json"
# python3 Code/stages_comparison.py -c ${ConfFile} -o Figure/${SubFolder}/${Module}/ -p Configuration/PlotConfig_dcb.json
echo "running with PlotConfig_threshFWB.json"
python3 Code/stages_comparison.py -c ${ConfFile} -o Figure/${SubFolder}/${Module}/ -p Configuration/PlotConfig_threshFWB.json

# M12: Liv5
# ConfFile=${ConfigFolder}/GenConfig_Liv5.json
# Module=Liv5
                                                                                                                         
# echo "Running with Module ${Module}"
# echo "running with PlotConfig_dig.json"
# python3 Code/stages_comparison.py -c ${ConfFile} -o Figure/${SubFolder}/${Module}/ -p Configuration/PlotConfig_dig.json 
# echo "running with PlotConfig_ana.json"                          
# python3 Code/stages_comparison.py -c ${ConfFile} -o Figure/${SubFolder}/${Module}/ -p Configuration/PlotConfig_ana.json
# echo "running with PlotConfig_cross.json"
# python3 Code/stages_comparison.py -c ${ConfFile} -o Figure/${SubFolder}/${Module}/ -p Configuration/PlotConfig_cross.json
# echo "running with PlotConfig_dcb.json"
# python3 Code/stages_comparison.py -c ${ConfFile} -o Figure/${SubFolder}/${Module}/ -p Configuration/PlotConfig_dcb.json
# echo "running with PlotConfig_threshFWB.json"
# python3 Code/stages_comparison.py -c ${ConfFile} -o Figure/${SubFolder}/${Module}/ -p Configuration/PlotConfig_threshFWB.json



