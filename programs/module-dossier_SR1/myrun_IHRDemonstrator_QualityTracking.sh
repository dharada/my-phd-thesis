#!/bin/bash


### IMPORTANT ###
# This: source /cvmfs/sft.cern.ch/lcg/views/LCG_100/x86_64-centos7-gcc10-opt/setup.sh might not be enough.
# If this script is executed, Root will randomly seg fault in the PlotConfig_threshFWB.json creation
# Solution for now: Run the threshFWB plots separately in the terminal
# Note: Seems to be fixed in new ROOT version

# "Stage6": "/home/itkpix/data/M6Demonstrator_ItkSw"

# to have access to eos and the data stored there, run the following command before executing this script:
#kinit [lxplususername]@CERN.CH

# to have ROOT available (if not available yet)
source /cvmfs/sft.cern.ch/lcg/views/LCG_102/x86_64-centos7-gcc11-opt/setup.sh
# source /cvmfs/sft.cern.ch/lcg/views/LCG_100/x86_64-centos7-gcc10-opt/setup.sh

# M12 PP0 Demonstrator has 12 modules, see 
# https://docs.google.com/spreadsheets/d/1L_POGvDwJrkWnH7phwecN8dxRgnek8KVERBHk3FD6_A/edit#gid=807148771

# Module QCs information
# https://docs.google.com/spreadsheets/d/14EmEwtx612Y6Ik55RvC1m7ajNLqZCv3i_HY6z0BFEUQ/edit#gid=853604022


# ------------------------------------------------------------------
# Plot the module comparisons for the modules in the M6 demonstrator
SubFolder=IHRDemonstrator_QualityTracking # For results 
ConfigFolder=Configuration/IHRDemonstrator
OutputFolder=/eos/home-d/dharada/Documents/Figure_LS3

# M1: KEKQ22
ConfFile=${ConfigFolder}/GenConfig_KEKQ22.json
Module=KEKQ22
                                                                                                                         
echo "Running with Module ${Module}"
echo "running with PlotConfig_dig.json"
python3 Code/stages_comparison.py -c ${ConfFile} -o ${OutputFolder}/${SubFolder}/${Module}/ -p Configuration/PlotConfig_dig.json 
echo "running with PlotConfig_ana.json"                          
python3 Code/stages_comparison.py -c ${ConfFile} -o ${OutputFolder}/${SubFolder}/${Module}/ -p Configuration/PlotConfig_ana.json
echo "running with PlotConfig_cross.json"
python3 Code/stages_comparison.py -c ${ConfFile} -o ${OutputFolder}/${SubFolder}/${Module}/ -p Configuration/PlotConfig_cross.json
echo "running with PlotConfig_dcb.json"
python3 Code/stages_comparison.py -c ${ConfFile} -o ${OutputFolder}/${SubFolder}/${Module}/ -p Configuration/PlotConfig_dcb.json
echo "running with PlotConfig_threshFWB.json"
python3 Code/stages_comparison.py -c ${ConfFile} -o ${OutputFolder}/${SubFolder}/${Module}/ -p Configuration/PlotConfig_threshFWB.json

# M2: KEKQ24
ConfFile=${ConfigFolder}/GenConfig_KEKQ24.json
Module=KEKQ24
                                                                                                                         
echo "Running with Module ${Module}"
echo "running with PlotConfig_dig.json"
python3 Code/stages_comparison.py -c ${ConfFile} -o ${OutputFolder}/${SubFolder}/${Module}/ -p Configuration/PlotConfig_dig.json 
echo "running with PlotConfig_ana.json"                          
python3 Code/stages_comparison.py -c ${ConfFile} -o ${OutputFolder}/${SubFolder}/${Module}/ -p Configuration/PlotConfig_ana.json
echo "running with PlotConfig_cross.json"
python3 Code/stages_comparison.py -c ${ConfFile} -o ${OutputFolder}/${SubFolder}/${Module}/ -p Configuration/PlotConfig_cross.json
echo "running with PlotConfig_dcb.json"
python3 Code/stages_comparison.py -c ${ConfFile} -o ${OutputFolder}/${SubFolder}/${Module}/ -p Configuration/PlotConfig_dcb.json
echo "running with PlotConfig_threshFWB.json"
python3 Code/stages_comparison.py -c ${ConfFile} -o ${OutputFolder}/${SubFolder}/${Module}/ -p Configuration/PlotConfig_threshFWB.json

# M3: KEKQ25
ConfFile=${ConfigFolder}/GenConfig_KEKQ25.json
Module=KEKQ25
                                                                                                                         
echo "Running with Module ${Module}"
echo "running with PlotConfig_dig.json"
python3 Code/stages_comparison.py -c ${ConfFile} -o ${OutputFolder}/${SubFolder}/${Module}/ -p Configuration/PlotConfig_dig.json 
echo "running with PlotConfig_ana.json"                          
python3 Code/stages_comparison.py -c ${ConfFile} -o ${OutputFolder}/${SubFolder}/${Module}/ -p Configuration/PlotConfig_ana.json
echo "running with PlotConfig_cross.json"
python3 Code/stages_comparison.py -c ${ConfFile} -o ${OutputFolder}/${SubFolder}/${Module}/ -p Configuration/PlotConfig_cross.json
echo "running with PlotConfig_dcb.json"
python3 Code/stages_comparison.py -c ${ConfFile} -o ${OutputFolder}/${SubFolder}/${Module}/ -p Configuration/PlotConfig_dcb.json
echo "running with PlotConfig_threshFWB.json"
python3 Code/stages_comparison.py -c ${ConfFile} -o ${OutputFolder}/${SubFolder}/${Module}/ -p Configuration/PlotConfig_threshFWB.json

# M4: Liv8
ConfFile=${ConfigFolder}/GenConfig_Liv8.json
Module=Liv8
                                                                                                                         
echo "Running with Module ${Module}"
echo "running with PlotConfig_dig.json"
python3 Code/stages_comparison.py -c ${ConfFile} -o ${OutputFolder}/${SubFolder}/${Module}/ -p Configuration/PlotConfig_dig.json 
echo "running with PlotConfig_ana.json"                          
python3 Code/stages_comparison.py -c ${ConfFile} -o ${OutputFolder}/${SubFolder}/${Module}/ -p Configuration/PlotConfig_ana.json
echo "running with PlotConfig_cross.json"
python3 Code/stages_comparison.py -c ${ConfFile} -o ${OutputFolder}/${SubFolder}/${Module}/ -p Configuration/PlotConfig_cross.json
echo "running with PlotConfig_dcb.json"
python3 Code/stages_comparison.py -c ${ConfFile} -o ${OutputFolder}/${SubFolder}/${Module}/ -p Configuration/PlotConfig_dcb.json
echo "running with PlotConfig_threshFWB.json"
python3 Code/stages_comparison.py -c ${ConfFile} -o ${OutputFolder}/${SubFolder}/${Module}/ -p Configuration/PlotConfig_threshFWB.json

# M5: KEKQ19
ConfFile=${ConfigFolder}/GenConfig_KEKQ19.json
Module=KEKQ19
                                                                                                                         
echo "Running with Module ${Module}"
echo "running with PlotConfig_dig.json"
python3 Code/stages_comparison.py -c ${ConfFile} -o ${OutputFolder}/${SubFolder}/${Module}/ -p Configuration/PlotConfig_dig.json 
echo "running with PlotConfig_ana.json"                          
python3 Code/stages_comparison.py -c ${ConfFile} -o ${OutputFolder}/${SubFolder}/${Module}/ -p Configuration/PlotConfig_ana.json
echo "running with PlotConfig_cross.json"
python3 Code/stages_comparison.py -c ${ConfFile} -o ${OutputFolder}/${SubFolder}/${Module}/ -p Configuration/PlotConfig_cross.json
echo "running with PlotConfig_dcb.json"
python3 Code/stages_comparison.py -c ${ConfFile} -o ${OutputFolder}/${SubFolder}/${Module}/ -p Configuration/PlotConfig_dcb.json
echo "running with PlotConfig_threshFWB.json"
python3 Code/stages_comparison.py -c ${ConfFile} -o ${OutputFolder}/${SubFolder}/${Module}/ -p Configuration/PlotConfig_threshFWB.json

# M6: Paris6
ConfFile=${ConfigFolder}/GenConfig_Paris6.json
Module=Paris6
                                                                                                                         
echo "Running with Module ${Module}"
echo "running with PlotConfig_dig.json"
python3 Code/stages_comparison.py -c ${ConfFile} -o ${OutputFolder}/${SubFolder}/${Module}/ -p Configuration/PlotConfig_dig.json 
echo "running with PlotConfig_ana.json"                          
python3 Code/stages_comparison.py -c ${ConfFile} -o ${OutputFolder}/${SubFolder}/${Module}/ -p Configuration/PlotConfig_ana.json
echo "running with PlotConfig_cross.json"
python3 Code/stages_comparison.py -c ${ConfFile} -o ${OutputFolder}/${SubFolder}/${Module}/ -p Configuration/PlotConfig_cross.json
echo "running with PlotConfig_dcb.json"
python3 Code/stages_comparison.py -c ${ConfFile} -o ${OutputFolder}/${SubFolder}/${Module}/ -p Configuration/PlotConfig_dcb.json
echo "running with PlotConfig_threshFWB.json"
python3 Code/stages_comparison.py -c ${ConfFile} -o ${OutputFolder}/${SubFolder}/${Module}/ -p Configuration/PlotConfig_threshFWB.json

# M7: Goe5
ConfFile=${ConfigFolder}/GenConfig_Goe5.json
Module=Goe5
                                                                                                                         
echo "Running with Module ${Module}"
echo "running with PlotConfig_dig.json"
python3 Code/stages_comparison.py -c ${ConfFile} -o ${OutputFolder}/${SubFolder}/${Module}/ -p Configuration/PlotConfig_dig.json 
echo "running with PlotConfig_ana.json"                          
python3 Code/stages_comparison.py -c ${ConfFile} -o ${OutputFolder}/${SubFolder}/${Module}/ -p Configuration/PlotConfig_ana.json
echo "running with PlotConfig_cross.json"
python3 Code/stages_comparison.py -c ${ConfFile} -o ${OutputFolder}/${SubFolder}/${Module}/ -p Configuration/PlotConfig_cross.json
echo "running with PlotConfig_dcb.json"
python3 Code/stages_comparison.py -c ${ConfFile} -o ${OutputFolder}/${SubFolder}/${Module}/ -p Configuration/PlotConfig_dcb.json
echo "running with PlotConfig_threshFWB.json"
python3 Code/stages_comparison.py -c ${ConfFile} -o ${OutputFolder}/${SubFolder}/${Module}/ -p Configuration/PlotConfig_threshFWB.json

# M8: Paris16
ConfFile=${ConfigFolder}/GenConfig_Paris16.json
Module=Paris16
                                                                                                                         
echo "Running with Module ${Module}"
echo "running with PlotConfig_dig.json"
python3 Code/stages_comparison.py -c ${ConfFile} -o ${OutputFolder}/${SubFolder}/${Module}/ -p Configuration/PlotConfig_dig.json 
echo "running with PlotConfig_ana.json"                          
python3 Code/stages_comparison.py -c ${ConfFile} -o ${OutputFolder}/${SubFolder}/${Module}/ -p Configuration/PlotConfig_ana.json
echo "running with PlotConfig_cross.json"
python3 Code/stages_comparison.py -c ${ConfFile} -o ${OutputFolder}/${SubFolder}/${Module}/ -p Configuration/PlotConfig_cross.json
echo "running with PlotConfig_dcb.json"
python3 Code/stages_comparison.py -c ${ConfFile} -o ${OutputFolder}/${SubFolder}/${Module}/ -p Configuration/PlotConfig_dcb.json
echo "running with PlotConfig_threshFWB.json"
python3 Code/stages_comparison.py -c ${ConfFile} -o ${OutputFolder}/${SubFolder}/${Module}/ -p Configuration/PlotConfig_threshFWB.json

# M9: Paris11
ConfFile=${ConfigFolder}/GenConfig_Paris11.json
Module=Paris11
                                                                                                                         
echo "Running with Module ${Module}"
echo "running with PlotConfig_dig.json"
python3 Code/stages_comparison.py -c ${ConfFile} -o ${OutputFolder}/${SubFolder}/${Module}/ -p Configuration/PlotConfig_dig.json 
echo "running with PlotConfig_ana.json"                          
python3 Code/stages_comparison.py -c ${ConfFile} -o ${OutputFolder}/${SubFolder}/${Module}/ -p Configuration/PlotConfig_ana.json
echo "running with PlotConfig_cross.json"
python3 Code/stages_comparison.py -c ${ConfFile} -o ${OutputFolder}/${SubFolder}/${Module}/ -p Configuration/PlotConfig_cross.json
echo "running with PlotConfig_dcb.json"
python3 Code/stages_comparison.py -c ${ConfFile} -o ${OutputFolder}/${SubFolder}/${Module}/ -p Configuration/PlotConfig_dcb.json
echo "running with PlotConfig_threshFWB.json"
python3 Code/stages_comparison.py -c ${ConfFile} -o ${OutputFolder}/${SubFolder}/${Module}/ -p Configuration/PlotConfig_threshFWB.json

# M10: Goe7
ConfFile=${ConfigFolder}/GenConfig_Goe7.json
Module=Goe7
                                                                                                                         
echo "Running with Module ${Module}"
echo "running with PlotConfig_dig.json"
python3 Code/stages_comparison.py -c ${ConfFile} -o ${OutputFolder}/${SubFolder}/${Module}/ -p Configuration/PlotConfig_dig.json 
echo "running with PlotConfig_ana.json"                          
python3 Code/stages_comparison.py -c ${ConfFile} -o ${OutputFolder}/${SubFolder}/${Module}/ -p Configuration/PlotConfig_ana.json
echo "running with PlotConfig_cross.json"
python3 Code/stages_comparison.py -c ${ConfFile} -o ${OutputFolder}/${SubFolder}/${Module}/ -p Configuration/PlotConfig_cross.json
echo "running with PlotConfig_dcb.json"
python3 Code/stages_comparison.py -c ${ConfFile} -o ${OutputFolder}/${SubFolder}/${Module}/ -p Configuration/PlotConfig_dcb.json
echo "running with PlotConfig_threshFWB.json"
python3 Code/stages_comparison.py -c ${ConfFile} -o ${OutputFolder}/${SubFolder}/${Module}/ -p Configuration/PlotConfig_threshFWB.json

# M11: Paris8
ConfFile=${ConfigFolder}/GenConfig_Paris8.json
Module=Paris8
                                                                                                                         
echo "Running with Module ${Module}"
echo "running with PlotConfig_dig.json"
python3 Code/stages_comparison.py -c ${ConfFile} -o ${OutputFolder}/${SubFolder}/${Module}/ -p Configuration/PlotConfig_dig.json 
echo "running with PlotConfig_ana.json"                          
python3 Code/stages_comparison.py -c ${ConfFile} -o ${OutputFolder}/${SubFolder}/${Module}/ -p Configuration/PlotConfig_ana.json
echo "running with PlotConfig_cross.json"
python3 Code/stages_comparison.py -c ${ConfFile} -o ${OutputFolder}/${SubFolder}/${Module}/ -p Configuration/PlotConfig_cross.json
echo "running with PlotConfig_dcb.json"
python3 Code/stages_comparison.py -c ${ConfFile} -o ${OutputFolder}/${SubFolder}/${Module}/ -p Configuration/PlotConfig_dcb.json
echo "running with PlotConfig_threshFWB.json"
python3 Code/stages_comparison.py -c ${ConfFile} -o ${OutputFolder}/${SubFolder}/${Module}/ -p Configuration/PlotConfig_threshFWB.json
