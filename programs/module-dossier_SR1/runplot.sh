#!/bin/bash


### IMPORTANT ###
# This: source /cvmfs/sft.cern.ch/lcg/views/LCG_100/x86_64-centos7-gcc10-opt/setup.sh might not be enough.
# If this script is executed, Root will randomly seg fault in the PlotConfig_threshFWB.json creation
# Solution for now: Run the threshFWB plots separately in the terminal
# Note: Seems to be fixed in new ROOT version

# "Stage6": "/home/itkpix/data/M6Demonstrator_ItkSw"

# to have access to eos and the data stored there, run the following command before executing this script:
#kinit [lxplususername]@CERN.CH

# to have ROOT available (if not available yet)
source /cvmfs/sft.cern.ch/lcg/views/LCG_102/x86_64-centos7-gcc11-opt/setup.sh
# source /cvmfs/sft.cern.ch/lcg/views/LCG_100/x86_64-centos7-gcc10-opt/setup.sh

# M12 PP0 Demonstrator has 12 modules, see 
# https://docs.google.com/spreadsheets/d/1L_POGvDwJrkWnH7phwecN8dxRgnek8KVERBHk3FD6_A/edit#gid=807148771

# Module QCs information
# https://docs.google.com/spreadsheets/d/14EmEwtx612Y6Ik55RvC1m7ajNLqZCv3i_HY6z0BFEUQ/edit#gid=853604022


# ------------------------------------------------------------------
# Plot the module comparisons for the modules in the M6 demonstrator
SubFolder=IHRDemonstrator_QualityTracking # For results 
ConfigFolder=Configuration/IHRDemonstrator
OutputFolder=/eos/home-d/dharada/Documents/Figure_LS3
#for Module in "Paris16"  "Paris6" "Liv8"  ;do #exclude good
#    for Module in "Paris8" "Paris11" "Paris16" "Goe5" "Paris6" "KEKQ19" "Liv8" "KEKQ25" "KEKQ24" "KEKQ22";do #original 
for Module in "Paris11" ;do
    # M1: KEKQ22
    ConfFile=${ConfigFolder}/GenConfig_${Module}.json
                                                                                                                         
    echo "Running with Module ${Module}"
    echo "running with PlotConfig_dig.json"
#    python3 Code/stages_comparison.py -c ${ConfFile} -o ${OutputFolder}/${Module}/ -p Configuration/PlotConfig_dig.json 
    echo "running with PlotConfig_ana.json"                          
#    python3 Code/stages_comparison.py -c ${ConfFile} -o ${OutputFolder}/${Module}/ -p Configuration/PlotConfig_ana.json
    echo "running with PlotConfig_cross.json"
#    python3 Code/stages_comparison.py -c ${ConfFile} -o ${OutputFolder}/${Module}/ -p Configuration/PlotConfig_cross.json
    echo "running with PlotConfig_dcb.json"
#    python3 Code/stages_comparison.py -c ${ConfFile} -o ${OutputFolder}/${Module}/ -p Configuration/PlotConfig_dcb.json
    echo "running with PlotConfig_threshFWB.json"
    python3 Code/stages_comparison.py -c ${ConfFile} -o ${OutputFolder}/${Module}/ -p Configuration/PlotConfig_threshFWB.json
done
