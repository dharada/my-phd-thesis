#!/bin/bash


### IMPORTANT ###
# This: source /cvmfs/sft.cern.ch/lcg/views/LCG_100/x86_64-centos7-gcc10-opt/setup.sh might not be enough.
# If this script is executed, Root will randomly seg fault in the PlotConfig_threshFWB.json creation
# Solution for now: Run the threshFWB plots separately in the terminal
# Note: Seems to be fixed in new ROOT version

# "Stage6": "/home/itkpix/data/M6Demonstrator_ItkSw"

# to have access to eos and the data stored there, run the following command before executing this script:
#kinit [lxplususername]@CERN.CH

# to have ROOT available (if not available yet)
source /cvmfs/sft.cern.ch/lcg/views/LCG_102/x86_64-centos7-gcc11-opt/setup.sh
# source /cvmfs/sft.cern.ch/lcg/views/LCG_100/x86_64-centos7-gcc10-opt/setup.sh

# M6 PP0 Demonstrator has 6 modules, see 
# https://docs.google.com/spreadsheets/d/1zkZHBOGTa0uhf4_xzP_ETSzCNWwt_YxBgpEYIntBs6I/edit#gid=0

# Module QCs information
# https://docs.google.com/spreadsheets/d/14EmEwtx612Y6Ik55RvC1m7ajNLqZCv3i_HY6z0BFEUQ/edit#gid=853604022


# ------------------------------------------------------------------
# Plot the module comparisons for the modules in the M6 demonstrator

SubFolder=M6M12Demonstrator_Crosstalk/M12
ConfFile=Configuration/M6M12DemonstratorCrosstalk/M12/PlotConfig_ModuleComparison.json
echo "Running with ConfFile ${ConfFile}"
python3 Code/modules_comparison.py -p ${ConfFile} -o Figure/${SubFolder}/ModuleComparison


SubFolder=M6M12Demonstrator_Crosstalk/M6
ConfFile=Configuration/M6M12DemonstratorCrosstalk/M6/PlotConfig_ModuleComparison.json
echo "Running with ConfFile ${ConfFile}"
python3 Code/modules_comparison.py -p ${ConfFile} -o Figure/${SubFolder}/ModuleComparison