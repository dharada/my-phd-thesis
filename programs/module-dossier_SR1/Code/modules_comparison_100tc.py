import pandas as pd
import ROOT
import json
import os
import math
from typing import List
from typing import Dict
from glob import glob
import shutil


class plots:
    """Parent (abstract) class that holds the drawing data (in form of ROOT.TH1F) for the module comparison and does the general drawing and exporting as pdf/png.
    Different module comparisons should inherit from this class and at least implement the produceData() member function.
    """

    def __init__(self, Stages: Dict[str, Dict], binLabels: List[str], drawOptions: str, plotInfo: Dict, plottitle: str, ytitle: str, filename: str):
        """Constructor of the 'plots' class that creates instance attributes that hold information about the plots.

        Args:
            Stages (Dict[str, Dict]): Dictionary that maps stageIDs (key) to a dict (value) with detailed information about the stage, e.g. a descriptive name used in the legend
            binLabels (List[str]): (descriptive but short) labels of the bins
            drawOptions (str): extra drawing options for the histograms, e.g. if error bars are needed (https://root.cern/doc/v626/classTHistPainter.html#HP09)
            plotInfo (Dict): contains the column names in the csv file that have the data and error
            plottitle (str): title to be used as bookmark in the resulting pdf
            ytitle (str): y axis title
            filename (str): name of the file when saved
        """
        self.nStages = len(Stages)  # number of stages
        self.stageIDs = Stages.keys()  # unique stage IDs that are used to collect the data later
        self.nBins = len(binLabels)  # number of bins in the plot (usually corresponds to number of modules or number of modules * number of FEs)
        self.drawOptions = drawOptions  # to add additional drawing options, e.g. error bars (https://root.cern/doc/v626/classTHistPainter.html#HP09)
        self.plottitle = plottitle  # used in the pdf bookmarks
        self.filename = filename  # name of the saved file
        myKeys = ["Data", "Error"]  # column names in .csv file for data and error column
        self.plotInfo = {key: plotInfo[key] for key in myKeys}

        # Create dict of histograms
        self.histos: Dict[str, ROOT.TH1F] = dict()
        PhantomBarWidth = 1.0/(self.nStages+2)
        BarWidth = 0.9*PhantomBarWidth
        FirstBarOffset = PhantomBarWidth + 0.5*(PhantomBarWidth - BarWidth)
        Colors = [ROOT.kAzure-3, ROOT.kGreen+3, ROOT.kOrange+1, ROOT.kGray+1, ROOT.kRed+1, ROOT.kCyan+2]  # https://root.cern/doc/master/classTColor.html

        for idx, (s, stageDetails) in enumerate(Stages.items()):
            StageName = stageDetails["Name"]
            # Create histograms (one histogram per stage, the bins are the module/FEs and the bin content is the module data per this stage)
            self.histos[s] = ROOT.TH1F(s, StageName, self.nBins, 0.5, self.nBins + 0.5)
            self.histos[s].SetBit(ROOT.TH1.kNoTitle)  # don't associate a title to print on the graph later
            self.histos[s].SetBarWidth(BarWidth)
            self.histos[s].SetBarOffset(FirstBarOffset + idx*PhantomBarWidth)
            self.histos[s].SetFillColor(Colors[idx])
            self.histos[s].SetDirectory(ROOT.nullptr)  # don't add histogram to the internal registry to avoid RuntimeWarnings
            # self.histos[s].SetLineWidth(0) # don't associate a line width with the bar (would appear in the legend)
            self.histos[s].SetLabelOffset(0.01)

            for i, label in enumerate(binLabels):
                self.histos[s].GetXaxis().SetBinLabel(i + 1, label)
                self.histos[s].GetXaxis().ChangeLabel(i + 1, 300., -1, 1, -1, -1, label)  # https://root.cern/doc/v626/classTAttText.html#ATTTEXT1
            self.histos[s].SetYTitle(ytitle)
            self.histos[s].SetStats(0)  # don't show box with histogram statistics

    def produceData(self, binID: int, evolutionData: pd.DataFrame):
        raise NotImplementedError()

    def draw(self):
        """Draws the histograms
        """
        if self.drawOptions != "":
            drawOptions = " " + self.drawOptions
        else:
            drawOptions = ""

        # Create canvas
        self.c = ROOT.TCanvas('c', 'Bar chart', 0, 0, 2100, 1200)  # 200, 10, 2100, 1200 ) #ATLAS recommendation: 0, 0, 800, 600
        # self.c.SetGrid()
        self.c.SetBottomMargin(0.25)
        ROOT.gStyle.SetErrorX(0)  # don't draw error bars in x direction
        ROOT.gStyle.SetEndErrorSize(8.) # Small lines are drawn at the end of the error bars. (default = 0, from AtlasStyle) 
        self.legend = ROOT.TLegend(0.2, 0.73, 0.48, 0.92)
        # Draw histograms and legend on canvas
        self.firstHist = ""
        for idx, (s, h) in enumerate(self.histos.items()):
            reuseAxis = " same"
            if idx == 0:
                reuseAxis = ""
                self.firstHist = s
            h.Draw("bar" + drawOptions + reuseAxis)  # only reuse axis if it's not the first histogram to be drawn (https://root.cern/doc/v626/classTHistPainter.html#HP10)
            self.legend.AddEntry(h, "", "f")  # don't draw marker, only draw filling
            if idx is len(self.histos)-1:
                self.histos[self.firstHist].Draw("sameaxis")  # redraw axis of first histogram to avoid overlapping of later histograms over axis
        self.legend.Draw()

        self.c.Modified()
        self.c.Update()

    def addExtras(self):
        """For additional text, limit settings, etc. 
        Use self.histos[self.firstHist] to change the drawn axis.
        """
        pass

    def save(self, directory: str):
        """Saves a png and pdf of the plot.

        Args:
            directory (str): Output directory (should end with a '/')
        """
        self.c.Update()
        # Save the canvas to separate pngs and one pdf per scan
        if not os.path.exists(directory):
            os.makedirs(directory)
        self.c.SaveAs(directory+self.filename+".png")
        self.c.Print(directory+self.filename+".pdf", 'Title:'+self.plottitle)
        self.c.Close()


class FEcounterPlot(plots):
    """Show the evolution of working FEs for each module across the different stages.
    """

    def __init__(self, Stages: Dict[str, Dict], moduleLabels: List[str], moduleIDs: List[str], plotInfo: Dict, filename: str):
        """Constructor for a class of plots, that shows the evolution of working FEs for each module across the different stages.

        Args:
            Stages (Dict[str, Dict]): Dictionary that maps stageIDs (key) to a dict (value) with detailed information about the stage, e.g. a descriptive name used in the legend
            moduleLabels (List[str]): List of labels for the modules as used in the x-axis of the plots
            moduleIDs (List[str]): List of module IDs (the corresponding labels should be ordered the same)
            plotInfo (Dict): contains the limit for the y-axis which shows the number of working FEs
            filename (str): filename when saved
        """
        if len(moduleIDs) != len(moduleLabels):
            raise RuntimeError("Not exactly one label per module")
        drawOptions = ""
        plottitle = "FE_counter"
        ytitle = "# of working FEs"
        self.moduleIDs = moduleIDs  # binIDs
        self.upperLimit = plotInfo["Limits"]
        self.scan = plotInfo["Scan"]
        super().__init__(Stages=Stages, binLabels=moduleLabels, drawOptions=drawOptions, plotInfo=plotInfo, plottitle=plottitle, ytitle=ytitle, filename=filename)

    def produceData(self, moduleID: str, evolutionData: pd.DataFrame):
        """Writes the FE_counter data from a pd.DataFrame into ROOT.TH1F histograms. This writes only the data from a given module and iterates over all stages. 
        The number of working FEs is taken from the 'Scan' field in the plot information. 

        Args:
            moduleID (str): The module ID
            evolutionData (pd.DataFrame): contains the data that was read from "FE_counter_evolution.csv" by readEvolutionFile()
        """
        binID = self.moduleIDs.index(moduleID) + 1

        if evolutionData.empty:
            return

        for stage in self.stageIDs:
            data = evolutionData[self.plotInfo["Data"]].loc[(evolutionData['Stage'] == stage) & (evolutionData["Scan"] == self.scan)].values[0]

            self.histos[stage].SetBinContent(binID, data)
            self.histos[stage].SetBinError(binID, 0.0)

    def addExtras(self):
        # Use histos[firstHist] to change settings of the histogram that is drawn first
        latex = ROOT.TLatex()
        latex.SetTextSize(0.03)
        latex.DrawLatexNDC(0.2, 0.96, self.plottitle)
        self.histos[self.firstHist].GetYaxis().SetRangeUser(0.0, self.upperLimit)
        self.histos[self.firstHist].GetYaxis().SetNdivisions(8)  # only show integer ticks and labels


class IVPlot(plots):
    """Show the evolution of leakage current at different HV settings for each module across the different stages.
    """

    def __init__(self, Stages: Dict[str, Dict], moduleLabels: List[str], moduleIDs: List[str], plotInfo: Dict, filename: str):
        """Constructor for a class of plots, that shows the evolution of leakage current at different HV settings for each module across the different stages.

        Args:
            Stages (Dict[str, Dict]): Dictionary that maps stageIDs (key) to a dict (value) with detailed information about the stage, e.g. a descriptive name used in the legend
            moduleLabels (List[str]): List of labels for the modules as used in the x-axis of the plots
            moduleIDs (List[str]): List of module IDs (the corresponding labels should be ordered the same)
            plotInfo (Dict): contains the limit for the y-axis that shows the number of working FEs
            filename (str): filename when saved
        """
        if len(moduleIDs) != len(moduleLabels):
            raise RuntimeError("Not exactly one label per module")
        drawOptions = "E1"
        plottitle = "IV"
        self.prefix = 1e6  # to convert A to uA
        ytitle = "Leakage Current [#muA]"  # https://root.cern.ch/doc/master/classTLatex.html#L7
        self.voltages = [50.0, 100.0]  # as given in the 'IV_evolution.csv' file
        self.moduleIDs = moduleIDs
        labels = []
        for mod in moduleLabels:
            for voltage in self.voltages:
                labels.append(mod + ", " + str(voltage) + "V")

        if "Limits" in plotInfo:
            self.upperLimit = plotInfo["Limits"]
        else:
            self.upperLimit = None
        self.voltagesColumnName = plotInfo["Scan"]
        super().__init__(Stages=Stages, binLabels=labels, drawOptions=drawOptions, plotInfo=plotInfo, plottitle=plottitle, ytitle=ytitle, filename=filename)

    def produceData(self, moduleID: str, evolutionData: pd.DataFrame):
        """Writes the FE_counter data from a pd.DataFrame into ROOT.TH1F histograms. This writes only the data from a given module and iterates over all stages. 
        The number of working FEs is taken from the 'thresholdscan'. 

        Args:
            moduleID (str): The module ID
            evolutionData (pd.DataFrame): contains the data that was read from "FE_counter_evolution.csv" by readEvolutionFile()
            dataColumnName (str): The column name in the "FE_counter_evolution.csv" file that countains the data. Should probably always be '#FEs'
        """
        binID = 0
        # binID = self.moduleIDs.index(moduleID) + 1

        if evolutionData.empty:
            return

        for stage in self.stageIDs:
            for idx, scan in enumerate(self.voltages):
                binID = self.moduleIDs.index(moduleID)*len(self.voltages) + idx + 1
                if stage in evolutionData['Stage'].values and scan in evolutionData['Voltage'].loc[(evolutionData['Stage'] == stage)].values:
                    try:
                        myy = evolutionData[self.plotInfo["Data"]].loc[(evolutionData['ModuleID'] == moduleID) & (evolutionData['Stage'] == stage) & (evolutionData[self.voltagesColumnName] == scan)].values[0]
                        myey = evolutionData[self.plotInfo["Error"]].loc[(evolutionData['ModuleID'] == moduleID) & (evolutionData['Stage'] == stage) & (evolutionData[self.voltagesColumnName] == scan)].values[0]
                    except:
                        try:
                            myy = evolutionData[self.plotInfo["Data"]].loc[(evolutionData['Stage'] == stage) & (evolutionData[self.voltagesColumnName] == scan)].values[0]
                            myey = evolutionData[self.plotInfo["Error"]].loc[(evolutionData['Stage'] == stage) & (evolutionData[self.voltagesColumnName] == scan)].values[0]
                        except:
                            myy = 0
                            myey = 0
                else:
                    myy = 0
                    myey = 0

                if myy == float("inf"):
                    myy = 0.0
                if myey == float("inf"):
                    myey = 0.0
                self.histos[stage].SetBinContent(binID, self.prefix*myy)
                self.histos[stage].SetBinError(binID, self.prefix*myey)

                # single histogram configurations
                self.histos[stage].SetMarkerSize(0.000001)  # don't show marker

    def addExtras(self):
        # Use histos[firstHist] to change settings of the histogram that is drawn first
        latex = ROOT.TLatex()
        latex.SetTextSize(0.03)
        latex.DrawLatexNDC(0.2, 0.96, self.plottitle)
        if self.upperLimit is None:
            ymax = 0.0
            for h in self.histos.values():
                max = h.GetMaximum()
                if max > ymax:
                    ymax = max
            self.histos[self.firstHist].GetYaxis().SetRangeUser(0.0, 1.5*ymax)
        else:
            self.histos[self.firstHist].GetYaxis().SetRangeUser(0.0, self.upperLimit)


class GenericPlotForAFE(plots):
    """Show the evolution of given property for each FE in all modules for a specific analog FE across the different stages.
    """

    def __init__(self, Stages: Dict[str, Dict], moduleLabels: List[str], moduleIDs: List[str], plotInfo: Dict, plotTitle: str, FEs: List[str], afe: str, filename: str, ignoredFEs: Dict[str, str]):
        if len(moduleIDs) != len(moduleLabels):
            raise RuntimeError("Not exactly one label per module")
        drawOptions = ""
        if plotInfo["Error"] != "":
            drawOptions = "E1"
        plottitle = plotTitle
        ytitle = plotInfo["ytitle"]

        if "Limits" in plotInfo:
            self.upperLimit = plotInfo["Limits"]
        else:
            self.upperLimit = None
        self.afe = afe
        self.moduleIDs = moduleIDs
        self.FEs = FEs
        labels = []
        self.ignoredBins: List[bool] = []
        self.plotBin: List[int] = []
        for idx, mod in enumerate(moduleLabels):
            for FE in self.FEs:
                if moduleIDs[idx] in ignoredFEs and FE in ignoredFEs[moduleIDs[idx]]:
                    self.ignoredBins.append(True)
                    if self.plotBin:
                        self.plotBin.append(self.plotBin[-1])
                    else:
                        self.plotBin.append(-1)
                    continue
                labels.append(mod + ", " + FE)
                if self.plotBin:
                    self.plotBin.append(self.plotBin[-1] + 1)
                else:
                    self.plotBin.append(0)
                self.ignoredBins.append(False)

        super().__init__(Stages=Stages, binLabels=labels, drawOptions=drawOptions, plotInfo=plotInfo, plottitle=plottitle, ytitle=ytitle, filename=filename)

    def produceData(self, moduleID: str, evolutionData: pd.DataFrame):
        binID = 0

        for stage in self.stageIDs:
            for idx, fe in enumerate(self.FEs):
                binID = self.moduleIDs.index(moduleID)*len(self.FEs) + idx
                if self.ignoredBins[binID]:
                    continue
                binID = self.plotBin[binID] + 1
                if stage in evolutionData['Stage'].values and fe in evolutionData['Chip'].loc[(evolutionData['Stage'] == stage)].values and self.afe in evolutionData['FE'].loc[(evolutionData['Stage'] == stage)].values:
                    try:
                        myy = evolutionData[self.plotInfo["Data"]].loc[(evolutionData['ModuleID'] == moduleID) & (evolutionData['Chip'] == fe) & (evolutionData['FE'] == self.afe) & (evolutionData['Stage'] == stage)].values[0]
                        if self.plotInfo["Error"] != "":
                            myey = evolutionData[self.plotInfo["Error"]].loc[(evolutionData['ModuleID'] == moduleID) & (evolutionData['Chip'] == fe) &
                                                                             (evolutionData['FE'] == self.afe) & (evolutionData['Stage'] == stage)].values[0]
                        else:
                            myey = 0.0
                    except:
                        try:
                            myy = evolutionData[self.plotInfo["Data"]].loc[(evolutionData['Chip'] == fe) & (evolutionData['FE'] == self.afe) & (evolutionData['Stage'] == stage)].values[0]
                            if self.plotInfo["Error"] != "":
                                myey = evolutionData[self.plotInfo["Error"]].loc[(evolutionData['Chip'] == fe) & (evolutionData['FE'] == self.afe) & (evolutionData['Stage'] == stage)].values[0]
                            else:
                                myey = 0.0
                        except:
                            myy = 0
                            myey = 0
                else:
                    myy = 0
                    myey = 0

                self.histos[stage].SetBinContent(binID, myy)
                self.histos[stage].SetBinError(binID, math.sqrt(myey))  # assume, myey contains std deviation, convert to error

                # single histogram configurations
                self.histos[stage].SetMarkerSize(0.000001)  # don't show marker

    def addExtras(self):
        # Use histos[firstHist] to change settings of the histogram that is drawn first
        latex = ROOT.TLatex()
        latex.SetTextSize(0.03)
        latex.DrawLatexNDC(0.2, 0.96, self.plottitle)
        latex.DrawLatexNDC(0.8, 0.9, self.afe + " AFE")

        if self.upperLimit is None or len(self.upperLimit) != 3:  # upperLimit corresponds to AFES = ['syn', 'lin', 'diff']
            ymax = 0.0
            for h in self.histos.values():
                max = h.GetMaximum()
                if max > ymax:
                    ymax = max
            self.histos[self.firstHist].GetYaxis().SetRangeUser(0.0, 1.5*ymax)
        else:
            if self.afe == 'syn':
                ylimit = self.upperLimit[0]
            if self.afe == 'lin':
                ylimit = self.upperLimit[1]
            if self.afe == 'diff':
                ylimit = self.upperLimit[2]
            self.histos[self.firstHist].GetYaxis().SetRangeUser(0.0, ylimit)
        self.histos[self.firstHist].SetLabelOffset(0.01)


def readEvolutionFile(scaninfo: str, path: str, module: str) -> pd.DataFrame:
    """Read the csv-files that contain the evolution of interesting parameters of modules over several stages.
    These csv-files are produced by the stages_comparison.py script and often end with '_evolution.csv'.

    Args:
        scaninfo (str): type of the scan information, e.g. 'discbumpscan', 'sourcescan', 'ThresholdDist', 'NoiseDist', 'IV', 'FE_counter'. 
        path (str): Path to the folder that contains the results of the stages_comparison.py script.
        module (str): module ID

    Returns:
        pd.DataFrame: the csv-data as a pandas DataFrame
    """
    foldername = path + "/" + module + "/" # includes full path
    if not os.path.isdir(foldername):
        # SiegenQ4 has a different folder name
        if module == "SiegenQ4":
            foldername = path + "/" + "SiegenQ4_Si_RD53A_Q6" + "/"
            if not os.path.isdir(foldername):
                print("Error: Couldn't find directory " + foldername + " for module " + module)
                return pd.DataFrame
        # CERN modules might have a module ID different from the folder name created by the stages_comparison.py script
        elif module.startswith('Thin') or module.startswith('Thick'):
            num = ''.join(i for i in module if i.isdigit())  # get the module number
            s = ""
            if module.startswith('Thin'):
                s = 'Thin'
            else:
                s = 'Thick'
            folderpattern = 'CERN_' + s + '_Q' + str(num) + '_*'
            subfolders = glob(path + '/' + folderpattern + '/', recursive=True)
            if len(subfolders) > 0:
                foldername = subfolders[0]
            else:
                print("Error: Couldn't find a directory " + foldername + " for module " + module)
                return pd.DataFrame
        else:
            print("Error: Couldn't find directory " + foldername + " for module " + module)
            return pd.DataFrame
    
    filename = scaninfo + ".csv"
    myfile = foldername + filename
    if not os.path.isfile(myfile):
        ending = ""
        if scaninfo != "discbumpscan" and scaninfo != "sourcescan" and scaninfo != 'digitalscan' and not scaninfo.startswith('analogscan'):
            ending = '_evolution.csv'
        elif scaninfo.startswith('analogscan'):
            ending = ".csv"
        else:
            ending = '_evolution_0.csv'
        myfile = foldername + scaninfo + ending
        if not os.path.isfile(myfile):
            print("Error: Couldn't find file " + myfile + " for module " + module)
            return pd.DataFrame

    evolutionData = pd.read_csv(myfile)
    return evolutionData


def plotGraph(config, outdir):

    conf = open(config, "r")
    CONFIG = json.loads(conf.read())

    # Read the module specific information (ModuleID, folder path, stages, FE keys, FEs to use)
    modules = CONFIG["Modules"]
    moduleNames = modules  # names used in plots
    if "ModulesNames" in CONFIG.keys():
        moduleNames = CONFIG["ModulesNames"]
    Stages = CONFIG["Stages"]

    path = CONFIG["Path"]
    AFES = ['syn', 'lin', 'diff']
    FEs = []
    if "FEs" in CONFIG.keys():
        FEs = CONFIG["FEs"]
    else:
        FEs = ['FE1', 'FE2', 'FE3', 'FE4']
    Ignored = {}  # to ignore a specific FE of a specific module
    if "Ignore" in CONFIG.keys():
        Ignored = CONFIG["Ignore"]  # key: an entry from "Modules", value: array of strings with specified FEs e.g. ['FE2', 'FE3'] that should be ignored

    # Loop over all the scans for which an overview plot should be made
    for i in CONFIG["Plots"].keys():
        print("Doing  ", i)
        plot = CONFIG["Plots"][i]
        for AFE in AFES:

            modulechain = ""
            #for j in modules:
                #modulechain = modulechain+'_'+j

            barPlot = None
            if i == "FE_counter":
                barPlot = FEcounterPlot(Stages, moduleNames, modules, plot, i + modulechain)
                for mod_i, mod in enumerate(modules):
                    data = readEvolutionFile(i, path, mod)
                    barPlot.produceData(mod, data)
            elif i == "IV":
                barPlot = IVPlot(Stages, moduleNames, modules, plot, i + modulechain)
                for mod_i, mod in enumerate(modules):
                    data = readEvolutionFile(i, path, mod)
                    barPlot.produceData(mod, data)
            else:
                barPlot = GenericPlotForAFE(Stages, moduleNames, modules, plot, plot["Title"], FEs, AFE, i + "_" + AFE + modulechain, Ignored)
                for mod_i, mod in enumerate(modules):
                    data = readEvolutionFile(i, path, mod)
                    barPlot.produceData(mod, data)

            try:
                barPlot.draw()
            except:
                print("Error: Could not draw the plots for " + i + " and AFE " + AFE)

            try:
                barPlot.addExtras()
            except:
                pass

            try:
                barPlot.save(outdir)
            except:
                pass

    # Loop over all the scans for which an overview plot should be made
    if "SumPlots" in CONFIG:
        plottype = "SumPlots"
        for i in CONFIG[plottype].keys():
            print("Doing  ", i)
            plot = CONFIG[plottype][i]
            for AFE in AFES:

                modulechain = ""
#                for j in modules:
#                    modulechain = modulechain+'_'+j

                barPlot = GenericPlotForAFE(Stages, moduleNames, modules, plot, plot["Title"], FEs, AFE, i + "_" + AFE + modulechain, Ignored)
                for mod_i, mod in enumerate(modules):
                        data = None
                        sumColumn = "#sumColumn"
                        for file, column in plot["Data"].items():
                            if data is None:
                                data = readEvolutionFile(file, path, mod)
                                data[sumColumn] = data[column]
                            else:
                                data[sumColumn] += readEvolutionFile(file, path, mod)[column]
                        barPlot.plotInfo["Data"] = sumColumn
                        barPlot.produceData(mod, data)

                try:
                    barPlot.draw()
                except:
                    print("Error: Could not draw the plots for " + i + " and AFE " + AFE)

                try:
                    barPlot.addExtras()
                except:
                    pass

                try:
                    barPlot.save(outdir)
                except:
                    pass


if __name__ == "__main__":
    # ========================= Define Folder Paths  =====================================
    # Set to batch mode -> do not display graphics
    ROOT.gROOT.SetBatch(True)
    import AtlasStyle
    ROOT.SetAtlasStyle()

    import argparse
    parser = argparse.ArgumentParser(description=" Module's comparison plots")
    parser = argparse.ArgumentParser(description="Make a comparison of a given parameter across different stages of different modules FEs.")
    parser.add_argument("-p", "--plotconfig", type=str, default="./Configuration/PlotConfig_comparison.json", help='PlotConfig file')
    parser.add_argument("-o", "--outdir", type=str, default="./Figure/ModuleComparisons", help='Output directory')
    args = parser.parse_args()

    # Output directory needs to have a trailing slash
    if args.outdir[-1] != "/":
        args.outdir = args.outdir+"/"

    output_folder = args.outdir
    plotconfig = args.plotconfig

    plotGraph(plotconfig, output_folder)

    shutil.copy(plotconfig, output_folder + "PlotConfig.json")  # copy PlotConfig.json file for backup of source data destination
