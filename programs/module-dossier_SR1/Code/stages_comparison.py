import json
import sys
import argparse
import os
import ROOT
import math
import glob
import numpy as np
import copy
import csv
import time
from array import array
from helper_functions import *


# ========================= Plotting Functions =====================================
'''This python script plots module testing data and especially a
comparison of the data of a single quad module with 4 FE chips at
different testing stages. For each type of scan that is run in the
module testing a separate pdf output file is produced. In the pdf is
an overview of the 2D plots, layed out in the way the FE chips are
arranged in a module, and 1D plots, showing the evolution of, for
example, the number of pixels with 0 hits. For threshold and totscans
1D histograms are plotted, for IV and VI scans graphs are plotted.
For all different kind of plots separate plotting functions are
defined: plot_hist1ds, plot_hist2ds, plot_hist2d_merge, plot_hists,
evolution_plot, plot_iv_curve, plot_vi_curve, plot_graphs. Basic
functions are defined in the script helper_functions.py. At the end of
this script the data is loaded and the actual analysis steps are
started.  '''
# ========================= 1D Histograms =====================================

def plot_hist1ds(hist1d, data_folder_path, data_folder_path_fwb, FE_key, FE_types):
    '''Load data for threshold and totscan and for forwardbiasscan if
    available. By calling the function plot_hists all actual plotting
    steps are performed. This function returns a dictionary which is
    used by the evolution scan. In this function adjustments are
    necessary in case some scans are not properly loaded (std scan
    might be problematic!)  '''

    only_std_plots, ID, evo_dt, tuned_files_dt, fwb_files_dt, source_files_dt, i, fe_counter, stages = 0, CONFIG_MOD[FE_key], {}, {}, {}, {}, 0, 0, list(sorted([i for i in data_folder_path.keys()]))     # list of all the stages in reverse order

    for FE_type in FE_types:
        for stage in stages:
            if hist1d == 'MeanTotDist':
                # output from get_tuned_folder is a dictionary of only the current stage -> save the explicit stage to have all stages in a dictionary for later
                tuned_files_dt[stage] = get_tuned_folder(data_folder_path[stage], stage, ID, [FE_type], 'totscan')[stage]
            else:
                tuned_files_dt[stage] = get_tuned_folder(data_folder_path[stage], stage, ID, [FE_type], 'thresholdscan')[stage]
            try:
                # FWB data does not always exist, try to load it if available
                fwb_files_dt[stage] = get_tuned_folder(data_folder_path_fwb[stage], stage, ID, [FE_type])[stage]
                plot_fwb_dt = get_file(fwb_files_dt, hist1d)
                # load the source scan data additionally for comparison for the FWB scan (this only needs to be done if the FWB scan exists)
                source_files_dt[stage] = get_tuned_folder(data_folder_path[stage], stage, ID, [FE_type], 'sourcescan')[stage]
                plot_source_dt = get_file(source_files_dt, 'Occupancy')
            except:
                pass
        plot_dt = get_file(tuned_files_dt, hist1d)

        # parameter to mark empty stages
        isZero = True        
        for stage in stages:
            if len(plot_dt[stage]) != 0:
                isZero = False
        if isZero:                
            fe_counter += 1
            if i == 0:
                only_std_plots += 1
                break
            else:
                break
        else:
            print('Getting {} data'.format(hist1d))
            plot_data_dt = get_data(plot_dt)
            plot_fwb_data_dt = get_data(plot_fwb_dt)
            plot_source_data_dt = get_data(plot_source_dt)
            print('Plotting {} comparison'.format(hist1d))
            mean_dt = plot_hists(plot_data_dt, plot_fwb_data_dt, plot_source_data_dt, hist1d, FE_type, PLOT_CONFIG['Hist1D'][hist1d]['CompareType'])
            evo_dt[FE_type] = mean_dt
            i += 1
                                
    if only_std_plots != 0 and fe_counter == 3:
        # load the data if in one stage there are only std plots and
        # no separate plots for each FE, if output for KEK modules is
        # not as expected, adjust something in this function to make
        # std plots show up
        for stage in stages:
            tuned_files_dt = get_tuned_folder(data_folder_path, ID, ['std'])
        plot_dt = get_file(tuned_files_dt, hist1d)  
        print('Getting {} data'.format(hist1d))
        plot_data_dt = get_data(plot_dt)
        print('Plotting {} comparison'.format(hist1d))
        mean_dt = plot_hists(plot_data_dt, '', '', hist1d, 'std', PLOT_CONFIG['Hist1D'][hist1d]['CompareType'])
        evo_dt['std'] = mean_dt
    return evo_dt # the dictionary that is filled with the dictionary from plot_hists is returned here to be fed into the evolution scan

# ========================= 2D Histograms Plots =====================================

def plot_hist2ds(scan, z_range):
    ''' Load data for all the scans that were performed. Additionally
    extract the masking data for the later scans from the digital and
    analog scan. For sourcescan the config file ...json.before is
    needed. For the thresholdnoisescan also load the data from the
    previous stage to make difference plots available.  '''
    
    evo_dt, evo_dt_new, masks_dt, mask_dt, ocu_dt_previous, plot_dt, stages, stage_previous = {}, {}, {}, {}, {}, {}, list(reversed(sorted([i for i in CONFIG_MOD['FolderPath'].keys()]))), ''

    print('Working on {}:'.format(scan))
    # initializing of dictionaries for masks which are needed in different scans than the currently used one
    masks_dt[scan] = {}
    mask_dt[scan] = {}
    if scan == 'analogscan' or scan == 'crosstalkscan' or scan == 'discbumpscan':
        # Define the dictionaries masks_dt and mask_dt, where mask_dt is the
        # dictionary that will be given to the function plot_hist2d_merge.
        # These need to be defined here to be filled with data from analog and digital scans
        # when these scans are not directly called from the plot config.
        masks_dt['digitalscan'] = {}
        masks_dt['analogscan'] = {}
        mask_dt['digitalscan'] = {}
        mask_dt['analogscan'] = {}
    elif scan == 'sourcescan':
        mask_dt['noisescan'] = {}
    for stage in stages:
        # in the ocu_dt all the data should be saved in the end to be given to the function plot_hist2d_merge
        ocu_dt = {}
        masks_dt[scan][stage] = {}
        if scan == 'analogscan' or scan == 'crosstalkscan' or scan == 'discbumpscan':
            masks_dt['digitalscan'][stage] = {}
            masks_dt['analogscan'][stage] = {}
        for FE_key in ['FE1', 'FE4', 'FE2', 'FE3']:
            mask_dt[scan][FE_key] = {}
            if scan == 'analogscan' or scan == 'crosstalkscan' or scan == 'discbumpscan':
                mask_dt['digitalscan'][FE_key] = {}
                mask_dt['analogscan'][FE_key] = {}
            ID = CONFIG_MOD[FE_key]
            data_folder_path = CONFIG_MOD['FolderPath'][stage]
            if data_folder_path:
                # only the current stage is saved in tuned_scan_dt
                if scan == 'analogscan' or scan == 'sourcescan':
                    tuned_scan_dt = get_tuned_folder(data_folder_path, stage, ID, ['std','syn','lindiff','lin','diff'], scan)
                elif scan == 'thresholdscan' or scan == 'discbumpscan' or scan == 'crosstalkscan':
                    tuned_scan_dt = get_tuned_folder(data_folder_path, stage, ID, ['std','syn','lindiff','lin','diff'], scan)
                elif scan == 'FWB':
                    # the FWB scans have the same name as the threshold scans but lie in a separate folder
                    tuned_scan_dt = get_tuned_folder(CONFIG_MOD['FWB'][stage], stage, ID, ['std','syn','lindiff','lin','diff'], 'thresholdscan')
                elif scan == 'thresholdnoisescan':
                    tuned_scan_dt = get_tuned_folder(data_folder_path, stage, ID, ['std','syn','lindiff','lin','diff'], 'thresholdscan')
                else:
                    # for digitalscan
                    tuned_scan_dt = get_tuned_folder(data_folder_path, stage, ID, ['std','syn','lindiff'], scan)
                try:
                    # get the actual files for the stage in a dictionary
                    plot_dt[stage] = get_file(tuned_scan_dt, PLOT_CONFIG['Hist2D'][scan]['Name'])[stage]                    
                except:
                    return None
                try:
                    # load the data in a dictionary 
                    ocu_dt[FE_key] = get_data(plot_dt)[stage]
                    try:
                        # for the noise difference analysis load the previous stage data for the thresholdscan.
                        if scan == 'thresholdnoisescan':
                            ocu_dt_previous[FE_key] = get_data(plot_dt)[stage_previous]
                    except: 
                        ocu_dt_previous[FE_key] = None
                    try:
                        # get the masks for the analysis of the different scans,
                        # for these scans the digital scan is needed
                        if scan == 'analogscan' or scan == 'discbumpscan' or scan == 'crosstalkscan':
                            tuned_mask_dt = get_tuned_folder(data_folder_path, stage, ID, ['std','syn','lindiff'], 'digitalscan')
                            masks_dt['digitalscan'][stage] = get_file(tuned_mask_dt, PLOT_CONFIG['Hist2D'][scan]['Mask'])[stage]
                        # for these additionally the results from the analog scan are needed
                        if scan == 'discbumpscan' or scan == 'crosstalkscan':
                            tuned_mask_dt = get_tuned_folder(data_folder_path, stage, ID, ['std','syn','lindiff','lin','diff'], 'analogscan')
                            masks_dt['analogscan'][stage] = get_file(tuned_mask_dt, PLOT_CONFIG['Hist2D'][scan]['Mask'])[stage]
                        # for the source scan the mask is in the scan folder and the file is defined via the 'Mask' entry in the plot config file
                        if scan == 'sourcescan':
                            masks_dt[scan][stage] = get_file(tuned_scan_dt, PLOT_CONFIG['Hist2D'][scan]['Mask'])[stage]
                    except:
                        pass
                    try:
                        # try to load all the mask data
                        # Attention: the names of the sourcescan plots are 'noisescan_sourcescan', so
                        # we need to save it under noisescan to function later, also we use 'get_mask_data'.
                        # For all other scans we use get_data as we are using
                        # the normal occupancy files to calculate the mask.
                        if scan == 'sourcescan':
                            mask_dt['noisescan'][FE_key] = get_mask_data(masks_dt[scan], ID)[stage]
                        elif scan == 'analogscan' or scan == 'discbumpscan' or scan == 'crosstalkscan':
                            mask_dt['digitalscan'][FE_key] = get_data(masks_dt['digitalscan'])[stage]
                            if scan == 'discbumpscan' or scan == 'crosstalkscan':
                                mask_dt['analogscan'][FE_key] = get_data(masks_dt['analogscan'])[stage]
                    except:
                        pass
                except KeyError:
                    ocu_dt[FE_key] = None
                    ocu_dt_previous[FE_key] = None
            else: 
                break
        # the names of the sourcescan plots are 'noisescan_sourcescan', so
        # call the function plot_hist2d_merge differently for the sourcescan
        if scan == 'sourcescan':
            ocu = plot_hist2d_merge(ocu_dt, ocu_dt_previous, mask_dt, 'noisescan', PLOT_CONFIG['Hist2D'][scan]['Name'], stage, stage_previous, z_range)
        else:
            ocu = plot_hist2d_merge(ocu_dt, ocu_dt_previous, mask_dt, scan, PLOT_CONFIG['Hist2D'][scan]['Name'], stage, stage_previous, z_range)
        # fill evo_dt for the evolution plots
        evo_dt[stage] = ocu
        stage_previous = stage
                                
    # rearranging the keys and value, to match the same input format to the evolution_plot function
    # e.g. {FE1: {'lin': 'Stage1': ...}, ...}, ...})
    for FE_key in ['FE1', 'FE4', 'FE2', 'FE3']:
        evo_dt_new[FE_key] = dict(zip(['syn', 'lin', 'diff'], [{}]*3))
    for fe, v1 in evo_dt_new.items():
        dt_fe = {}
        for fe_type, v2 in v1.items():
            dt_fe_type = {}
            for sg in stages:
                try:
                    dt_fe_type[sg] = evo_dt[sg][fe][fe_type]
                except:
                    continue
            dt_fe[fe_type] = dt_fe_type
        evo_dt_new[fe] = dt_fe
    return evo_dt_new
    
# ========================= 2D Histograms Merge =====================================

def plot_hist2d_merge(data_dt, data_previous_dt, mask_dt, scan, hist_name, stage, stage_previous, z_range=None):
    ''' Plot the 2D histograms, in the same layout as in the module,
    with the borders between the FE chips merged together. Input is
    the data dictionary, with keys = FEs (FE1, to FE4) and values =
    dictionary of data in stages.  '''
   
    pixel_count_glo = {}   # count number of pixels in the given range

    # mirror the data array to plot it in the same layout as in the real module
    def mirror_arr(arr):
        return arr[:, ::-1]

    # rotate the data array clockwise
    def rotate_arr(arr, times):
        if times == 0:
            return arr
        else:
            return rotate_arr(arr[::-1].T, times - 1)

    # rotate the whole data dictionary, including the data array, and the x, y axes, and the bins, border locations, ...
    def rotate_data(data, times):
        try:
            data['Data'] = mirror_arr(np.array(data['Data']))
            data['Data'] = rotate_arr(np.array(data['Data']), times)
        except:
            data['Data'] = np.zeros((192, 400))
        try:
            # this should only be done for the data and not for the mask, that is why it is in a try and except catch
            for i_t in range(times):
                data['x']['Bins'], data['y']['Bins'] = data['y']['Bins'], data['x']['Bins']
                data['x']['Low'], data['y']['Low'] = data['y']['Low'], data['x']['Low']
                data['x']['High'], data['y']['High'] = data['y']['High'], data['x']['High']
                for b in data['Borders']:
                    if i_t % 2 == 0:
                        b[0], b[1], b[2], b[3] = (data['x']['High'] - data['x']['Low']) - b[1], \
                                                 (data['y']['High'] - data['y']['Low']) - b[0], \
                                                 (data['x']['High'] - data['x']['Low']) - b[3], \
                                                 (data['y']['High'] - data['y']['Low']) - b[2]
                    else:
                        b[0], b[1], b[2], b[3] = b[1], b[0], b[3], b[2]
        except: 
            pass
        return data

    # book canvas, and styling
    c1 = ROOT.TCanvas('canvas1_'+scan+'_'+str(z_range)+'_'+str(stage), 'canvas1_'+scan+'_'+str(z_range)+'_'+str(stage), 2100, 1500)
    c1.Divide(2, 2)
    # book 2nd canvas for thresholdnoise plots
    c3 = ROOT.TCanvas('canvas3_'+scan+'_'+str(z_range)+'_'+str(stage), 'canvas3_'+scan+'_'+str(z_range)+'_'+str(stage), 2100, 1500)
    c3.Divide(2, 2) 
    ROOT.gStyle.SetTitleFontSize(0.05)

    # index for the plot (pad), list of histograms, lines (borders of the FE types), pads for plotting the FE 2D histograms
    j, hs, hs_diff, lines, lines2, lines3, pads, pads2, pads_fraction = 0, [], [], [], [], [], [], [], [(0, 0.5, 0.5, 1), (0.5, 0.5, 1, 1),  (0, 0, 0.5, 0.5), (0.5, 0, 1, 0.5)]
    # layout of the module, dt with plotting information (e.g. FE ID, FE type, counts), counter for # of functioning FEs, counters for dead/masked pixel 
    rotation, pals_dt, pals2_dt, mask_ars, data_mask_ars, FE_counter, tot_count, dead_not_masked_count, dead_masked_count, total_masked_count, zeros_count, bad_count, unknown_count, axis_range = {'FE1': 1, 'FE2': 1, 'FE3': 3, 'FE4': 3}, {}, {}, {}, {}, 0, 0, 0, 0, 0, 0, 0, 0, 100.
    mask_ars[scan], data_mask_ars[scan] = {}, {}
    if scan == 'analogscan' or scan == 'crosstalkscan' or scan == 'discbumpscan':
        mask_ars['digitalscan'] = {}
        mask_ars['analogscan'] = {}

    # rotate the data for the thresholdnoise plots
    for k, data in data_previous_dt.items():
        if data is not None:
            # original border locations
            data['Borders'] = [[0, 0, 0, 384], [127, 0, 127, 384], [263, 0, 263, 384], [400, 0, 400, 384]]
            # rotate the data according to the FE location
            rotation_times = rotation[k]
            data = rotate_data(data, rotation_times)
            data_previous_dt[k] = data

    # work with the data
    for k, data in data_dt.items():
        if data is not None:
            # raise the counter if the FE is working
            FE_counter +=1
            # original border locations
            data['Borders'] = [[0, 0, 0, 384], [127, 0, 127, 384], [263, 0, 263, 384], [400, 0, 400, 384]]
            # book the dictionaries for the masks
            mask_ars[scan][k] = {}
            if scan == 'analogscan' or scan == 'crosstalkscan' or scan == 'discbumpscan':
                mask_ars['digitalscan'][k] = {}
                mask_ars['analogscan'][k] = {}
            elif scan == 'noisescan':
                mask_dt[scan][k]['Borders'] = [[0, 0, 0, 384], [127, 0, 127, 384], [263, 0, 263, 384], [400, 0, 400, 384]]
            # rotate the data according to the FE location
            rotation_times = rotation[k]
            data = rotate_data(data, rotation_times)
            # cuts, to count the number of pixels in the given occupancy range
            # keys = rotation times
            cuts = {1: {'syn': (None, None, 400-128, 400),
                        'lin': (None, None, 400-128-136, 400-128),
                        'diff': (None, None, None, 400-128-136)},
                    3: {'syn': (None, None, 0, 128),
                        'lin': (None, None, 128, 128+136),
                        'diff': (None, None, 128+136, None)}}
            # create arrays out of all the data sets
            data_ar = np.array(data['Data'])

            # create array from subtracting mask of the data:
            # -1 is a pixel with 0 hits, but not masked, 0 is 0 hits and masked pixel or 1 hit and not masked
            # => to not count the 1 hits without mask we copy the dataset and manipulate it so that 1s are now 2s
            #print((data_ar == 1).sum()) # not used, print this to get an idea of how many there are
            data_manip_ar = data_ar
            data_manip_ar[data_manip_ar == 1.] = 2.
            # create the mask that will be used for the analogscan
            if scan == 'analogscan':
                mask = rotate_data(mask_dt['digitalscan'][k], rotation_times)
                mask_ars['digitalscan'][k] = np.array(mask['Data'])
                # create the mask from the digitalscan occpancy data file, where every pixel with <98 or >102 hits will be turned off
                mask_ars['digitalscan'][k][np.logical_or(mask_ars['digitalscan'][k]<98.,mask_ars['digitalscan'][k]>102.)] = 0.
                mask_ars['digitalscan'][k][np.logical_and(mask_ars['digitalscan'][k]>97.,mask_ars['digitalscan'][k]<103.)] = 1.
                # subtract the mask from the data
                data_mask_ars[scan][k] = np.subtract(data_manip_ar,mask_ars['digitalscan'][k])
            # create the mask that will be used for the discbump and crosstalkscan
            elif scan == 'discbumpscan' or scan == 'crosstalkscan':
                mask = rotate_data(mask_dt['digitalscan'][k], rotation_times)
                mask_ars['digitalscan'][k] = np.array(mask['Data'])
                mask_ars['digitalscan'][k][np.logical_or(mask_ars['digitalscan'][k]<98.,mask_ars['digitalscan'][k]>102.)] = 0.
                mask_ars['digitalscan'][k][np.logical_and(mask_ars['digitalscan'][k]>97.,mask_ars['digitalscan'][k]<103.)] = 1.
                mask = rotate_data(mask_dt['analogscan'][k], rotation_times)
                mask_ars['analogscan'][k] = np.array(mask['Data'])
                # create the mask from the analog occupancydata file, every pixel with <98 or >102 hits will be turned off for the discbump and crosstalkscans
                for i in range(len(mask_ars['analogscan'][k])):
                    for l in range(len(mask_ars['analogscan'][k][i])):
                        if np.logical_and(np.logical_and(mask_ars['analogscan'][k][i][l]>97,mask_ars['analogscan'][k][i][l]<103), mask_ars['digitalscan'][k][i][l] == 1):
                            mask_ars['analogscan'][k][i][l] = 1
                        else:
                            mask_ars['analogscan'][k][i][l] = 0
                data_mask_ars[scan][k] = np.subtract(data_manip_ar,mask_ars['analogscan'][k])
            # get the mask that is used for the sourcescan and rotate
            elif scan == 'noisescan':
                mask = rotate_data(mask_dt[scan][k], rotation_times)
                mask_ars[scan][k] = np.array(mask['Data'])
                data_mask_ars[scan][k] = np.subtract(data_manip_ar,mask_ars[scan][k]) # subtract the mask from the data

            # ================================= count the number of pixels in defined occupancy ranges ==============================
            # set the zranges of the 2D scans
            if scan == 'noisescan':    
                if stage == 'Stage1':
                    axis_range = 500.
                else:
                    axis_range = 5000.                             
            elif scan == 'crosstalkscan':
                axis_range = 50000. 
            elif scan == 'discbumpscan':
                axis_range = 150. 
            elif scan == 'thresholdnoisescan':
                axis_range = 150. 
            elif scan == 'thresholdscan' or scan == 'FWB':
                axis_range = 2500. 

            # manipulate the data sets for the plotting of the pixels in the specific ranges
            if z_range is not None:
                if z_range == 0:    # dead pixel
                    zeros_count += (data_ar == 0).sum() # get the number of pixel with 0 hits
                    if scan == 'discbumpscan' or scan == 'crosstalkscan' or scan == 'noisescan' or scan == 'analogscan':
                        # data_mask_ar where -1 is a pixel with 0 hits, but not masked, 0 is 0 hits and masked pixel
                        dead_not_masked_count += (data_mask_ars[scan][k] == -1).sum() # counting dead not masked pixels
                        dead_masked_count += (data_mask_ars[scan][k] == 0).sum()      # counting dead and masked pixels
                        data_mask_ars[scan][k][data_mask_ars[scan][k] > 0] = -99999   # replace other pixel by dummy
                        data_mask_ars[scan][k][data_mask_ars[scan][k] == -1] = 1      # replace dead not masked pixel by 1
                        data_mask_ars[scan][k][data_mask_ars[scan][k] == 0] = 2       # replace dead and masked pixel by 2
                        data_mask_ars[scan][k][data_mask_ars[scan][k] == -99999] = 0  # replace dummy by 0
                    else:
                        # save all dead pixels in the variable for not masked dead pixels
                        dead_not_masked_count += (data_ar == 0).sum() # all dead pixels (as there is no mask save them in not_masked)
                        data_ar[data_ar != 0] = -99999 # replace all non-zero pixels by dummy
                        data_ar[data_ar == 0] = 1      # replace dead pixel by 1
                        data_ar[data_ar == -99999] = 0 # replace dummy by 0
                    range_title = 'occupancy=0'
                    axis_range = 2.
                elif type(z_range) == tuple and len(z_range) == 2:  # given range (e.g. 0<occupancy<200)
                    # hack for sourcescan to plot occ<z_range
                    if z_range[0] == z_range[1]:
                        tot_count += (data_ar < z_range[0]).sum()     
                        data_ar[~(data_ar < z_range[0])] = 0  
                        range_title = 'occupancy<' + str(z_range[0])
                    else:        
                        tot_count += ((data_ar > z_range[0]) & (data_ar < z_range[1])).sum()    
                        data_ar[~((data_ar > z_range[0]) & (data_ar < z_range[1]))] = 0
                        range_title = str(z_range[0]) + '<occupancy<' + str(z_range[1])
                        axis_range = 100.
                        if scan == 'discbumpscan':
                            axis_range = 101
                elif z_range != 0:
                    if scan == 'discbumpscan':
                        zeros_count += (data_ar == 0).sum() # number of dead bumps
                        data_ar[data_ar > 101] = -2 # sort the data
                        data_ar[data_ar > 98] = -1
                        data_ar[data_ar > 0] = -2
                        data_ar[data_ar == -2] = 3  # bad bumps are written as 3
                        data_ar[data_ar == -1] = 1  # good bumps are written as 1
                        data_ar[data_ar == 0] = 0.5 # dead bumps are written as 0.5 to have them show up in color in the plot
                        # Disconnected bump scan works this way: 100 e are injected in all 4
                        # surrounding pixels and the hope is to see 25 e in the middle pixel
                        # in which nothing is injected => edge pixel will see less due to
                        # reduced number of neighbors
                        # If the neighboring pixel is bad or broken we
                        # don't get any definite information about the
                        # status of the pixel => set to unknown
                        for m in range(len(data_ar)):
                            for n in range(len(data_ar[m])):
                                # analyze data in within the edges
                                if m != 0 and m != 191 and n != 0 and n != 399:
                                    if data_ar[m][n-1] == 3 or data_ar[m][n+1] == 3 or data_ar[m-1][n] == 3 or data_ar[m+1][n] == 3:
                                        if data_ar[m][n] == 1:
                                            data_ar[m][n] = 2 # set good pixel to unknown (2) if a bad pixel is next to it as it is unclear how much is injected from that pixel
                                    if data_ar[m][n-1] == 0.5 or data_ar[m][n+1] == 0.5 or data_ar[m-1][n] == 0.5 or data_ar[m+1][n] == 0.5:
                                        if data_ar[m][n] == 1:
                                            data_ar[m][n] = 2 # set good pixel to unknown (2) if a dead pixel is next to it as it is unclear how much is injected from that pixel
                                # analyze data for FE3 and FE4 at the edges
                                elif (k == 'FE3' or k == 'FE4') and (m == 0 or m == 191 or n == 0 or n == 127 or n == 128 or n == 263 or n == 264 or n == 399):
                                    # in the corners
                                    if (m == 0 and n == 0) or (m == 191 and n == 0) or (m == 0 and n == 399) or (m == 191 and n == 399):
                                        if data_ar[m][n] < 49 or data_ar[m][n] > 51:
                                            data_ar[m][n] = 2 # set corner pixel to unknown (2)
                                    else:
                                        if data_ar[m][n] < 74 or data_ar[m][n] > 76:
                                            data_ar[m][n] = 2 # set edge pixel to unknown (2)
                                # analyze data for FE1 and FE2 at the edges
                                elif (k == 'FE1' or k == 'FE2') and (m == 0 or m == 191 or n == 0 or n == 136 or n == 135 or n == 271 or n == 272 or n == 399):
                                    # in the corners
                                    if (m == 0 and n == 0) or (m == 191 and n == 0) or (m == 0 and n == 399) or (m == 191 and n == 399):
                                        if data_ar[m][n] < 49 or data_ar[m][n] > 51:
                                            data_ar[m][n] = 2 # set corner pixel to unknown (2)
                                    else:
                                        if data_ar[m][n] < 74 or data_ar[m][n] > 76:
                                            data_ar[m][n] = 2 # set edge pixel to unknown (2)
                        tot_count += (data_ar == 1).sum()     # total amount of good bumps
                        unknown_count += (data_ar == 2).sum() # number of unknown bumps
                        bad_count += (data_ar == 3).sum()     # number of bad bumps
                        range_title = 'analysis'
                        axis_range = 3.
                    else: # all other scans
                        tot_count += (data_ar > z_range).sum() # total count
                        data_ar[~(data_ar > z_range)] = 0      # replace outside the range by 0
                        range_title = 'occupancy>' + str(z_range)
                        axis_range = 600.
                   
                # update data
                if z_range == 0:
                    if scan == 'discbumpscan' or scan == 'crosstalkscan' or scan == 'noisescan' or scan == 'analogscan':
                        data['Data'] = data_mask_ars[scan][k]
                    else:
                        data['Data'] = data_ar
                else:
                    data['Data'] = data_ar

            FE_count = {}   # count for each FE (FE1, FE2, FE3, FE4)
            # set the counters for the separate FEs 
            for FE in ['syn', 'lin', 'diff']: 
                cut = cuts[rotation_times][FE]    # find the boundary of the FE type
                num_pix = np.prod(data['Data'][cut[0]:cut[1], cut[2]:cut[3]].shape) # total number of pixels  
                num_zeros = (data['Data'][cut[0]:cut[1], cut[2]:cut[3]] == 0).sum() # data NOT in the range of interest
                if z_range != 0 and scan == 'discbumpscan' and type(z_range) is not tuple:
                    num_bad = (data['Data'][cut[0]:cut[1], cut[2]:cut[3]] == 3).sum()
                    num_unknown = (data['Data'][cut[0]:cut[1], cut[2]:cut[3]] == 2).sum()
                    num_good = (data['Data'][cut[0]:cut[1], cut[2]:cut[3]] == 1).sum()
                    num_zeros = (data['Data'][cut[0]:cut[1], cut[2]:cut[3]] == 0.5).sum()
                    FE_count[FE] = (num_bad, num_unknown, num_good, num_zeros)
                elif z_range == 0:
                    # number of not masked dead pixels if data_mask_ar is used, for others simply all dead pixels
                    num_dead_not_masked = (data['Data'][cut[0]:cut[1], cut[2]:cut[3]] == 1).sum() 
                    # #dead masked, #pixels that meet critera, total # pixels, #dead not masked
                    FE_count[FE] = (num_pix-num_zeros-num_dead_not_masked, num_pix-num_zeros, num_pix, num_dead_not_masked) 
                else:
                    FE_count[FE] = (num_pix-num_zeros, num_pix) # number in the range = number of pixel - number of zero
            pixel_count_glo[k] = FE_count  # store in the global dictionary which is used for the evolution plot

            # ================================= plotting of the 2D plots with the correct counts ==============================
            
            # add title, only for the upper left panel
            if j == 0:
                if scan == 'noisescan':
                    title = ''.join(('sourcescan ', data['Name'], ' (Module: ', mod_ID, ') (', stage, ')'))
                elif scan == 'FWB':
                    title = ''.join(('forwardbiasscan ', data['Name'], ' (Module: ', mod_ID, ') (', stage, ')'))
                elif scan == 'thresholdnoisescan':
                    title_diff = ''.join(('thresholdnoisescan ', ' #Delta', data['Name'], ' (Module: ', mod_ID, ') (', stage_previous, '-', stage, ')'))
                    title = ''.join(('thresholdscan ', data['Name'], ' (Module: ', mod_ID, ') (', stage, ')'))
                else:
                    title = ''.join((scan, ' ', data['Name'], ' (Module: ', mod_ID, ') (', stage, ')'))
                if z_range is not None:
                    title = ''.join((title, ' (', range_title, ')'))
            else:
                title = ''
                title_diff = ''

            x_bins, x_low, x_high = data['x']['Bins'], data['x']['Low'], data['x']['High']
            y_bins, y_low, y_high = data['y']['Bins'], data['y']['Low'], data['y']['High']

        else: # to: if data is not none
            # set a title even if that FE is missing in the module
            if j == 0:
                if scan == 'noisescan':
                    title = ''.join(('sourcescan ', ' (Module: ', mod_ID, ') (', stage, ')'))
                elif scan == 'FWB':
                    title = ''.join(('forwardbiasscan ', ' (Module: ', mod_ID, ') (', stage, ')'))
                elif scan == 'thresholdnoisescan':
                        title_diff = ''.join(('#Delta thresholdnoisescan', ' (Module: ', mod_ID, ') (', stage_previous, '-', stage, ')'))
                        title = ''.join(('thresholdnoisescan', ' (Module: ', mod_ID, ') (', stage, ')'))
                else:
                    title = ''.join((scan, ' ', ' (Module: ', mod_ID, ') (', stage, ')'))

                # add additions to the title for the specific ranges 
                if z_range is not None:
                    if z_range == 0:
                        title = ''.join((title, ' (', 'occupancy=0', ')'))
                    elif type(z_range) == tuple and len(z_range) == 2:
                        title = ''.join((title, ' (', str(z_range[0]) + '<occupancy<' + str(z_range[1]), ')'))
                    elif type(z_range) == tuple and len(z_range) == 1:
                        title = ''.join((title, ' (', 'occupancy>' + str(z_range[0]), ')'))
                    elif z_range != 0:
                        if scan != 'discbumpscan':
                            title = ''.join((title, ' (', 'occupancy=' + str(z_range), ')'))
            else:
                title = ''
                title_diff = ''

            # set the bins in the histograms
            x_bins, x_low, x_high = 192, 0, 192
            y_bins, y_low, y_high = 400, 0, 400
                
        # Create histograms, create an extra one for the thresholdnoisescan difference analysis
        if scan == 'thresholdnoisescan':
            hs.append(ROOT.TH2D('h2_'+str(k)+'_'+str(z_range)+'_'+str(scan)+'_'+str(stage), title,
                            x_bins*2, x_low, x_high*2,
                            y_bins, y_low, y_high))
            hs_diff.append(ROOT.TH2D('h2_diff_'+str(k)+'_'+str(z_range)+'_'+str(scan)+'_'+str(stage), title_diff,
                                     x_bins*2, x_low, x_high*2,
                                     y_bins, y_low, y_high))
        else: 
            hs.append(ROOT.TH2D('h2_'+str(k)+'_'+str(z_range)+'_'+str(scan)+'_'+str(stage), title,
                                x_bins*2, x_low, x_high*2,
                                y_bins, y_low, y_high))

        # fill histogram
        diffplot = False # Indicate that diff plots are plotted for thresholdnoisescan
        if data is not None:
            if scan == 'thresholdnoisescan':
                # special filling of two histograms
                diffplot = True
                if k == 'FE1' or k == 'FE2':
                    # fill differences between stages for thresholdnoisescan for FE 1 and 2
                    for i1 in range(x_bins):
                        for i2 in range(y_bins):
                            h = data['Data'][i1][i2]
                            hs[j].SetBinContent(i1, i2, h)
                            if data_previous_dt[k] is not None and stage_previous is not '':
                                h_diff = data_previous_dt[k]['Data'][i1][i2] - data['Data'][i1][i2]
                                hs_diff[j].SetBinContent(i1, i2, h_diff)
                    for i1 in range(x_bins,x_bins+x_bins):
                        for i2 in range(y_bins):
                            hs[j].SetBinContent(i1, i2, 0)
                else:
                    # fill differences between stages for thresholdnoisescan for FE 3 and 4 (shift in the bins necessary)
                    for i1 in range(x_bins):
                        for i2 in range(y_bins):
                            h = data['Data'][i1][i2]
                            hs[j].SetBinContent(i1+x_bins, i2, h)
                            if data_previous_dt[k] is not None and stage_previous is not '':
                                h_diff = data_previous_dt[k]['Data'][i1][i2] - data['Data'][i1][i2]
                                hs_diff[j].SetBinContent(i1+x_bins, i2, h_diff)
                    for i1 in range(x_bins):
                        for i2 in range(y_bins):
                            hs[j].SetBinContent(i1, i2, 0)
            else:
                # for all other scans only one histogram is needed
                if k == 'FE1' or k == 'FE2':
                    for i1 in range(x_bins):
                        for i2 in range(y_bins):
                            h = data['Data'][i1][i2]
                            hs[j].SetBinContent(i1, i2, h)
                    for i1 in range(x_bins,x_bins+x_bins):
                        for i2 in range(y_bins):
                            hs[j].SetBinContent(i1, i2, 0)
                else:
                    for i1 in range(x_bins):
                        for i2 in range(y_bins):
                            h = data['Data'][i1][i2]
                            hs[j].SetBinContent(i1+x_bins, i2, h)
                    for i1 in range(x_bins):
                        for i2 in range(y_bins):
                            hs[j].SetBinContent(i1, i2, 0)
        else:
            # fill empty histograms when data is non-existant
            for i1 in range(x_bins*2):
                for i2 in range(y_bins):
                    hs[j].SetBinContent(i1, i2, 0)
                    if scan == 'thresholdnoisescan':
                        diffplot = True

        if diffplot:
            # Canvas setup for the difference thresholdnoisescan plots
            c3.cd(0)
            pads2.append(ROOT.TPad('pad2', 'pad2', pads_fraction[j][0], pads_fraction[j][1], pads_fraction[j][2], pads_fraction[j][3]))
            # set the margin=0, according to the location
            if j // 2 == 0:
                pads2[j].SetBottomMargin(0)
            else:
                pads2[j].SetTopMargin(0)
            if j % 2 == 0:
                pads2[j].SetRightMargin(0)
                pads2[j].SetLeftMargin(0.15)
            else:
                pads2[j].SetLeftMargin(0)
                pads2[j].SetRightMargin(0.15)
            pads2[j].Draw()
            pads2[j].cd()

            if j == 1:
                # Use own defined palette
                set_color_palette()
                hs_diff[j].Draw('colz')
            else:
                hs_diff[j].Draw('col')
            hs_diff[j].SetStats(0)

            if data is not None and data_previous_dt[k] is not None:
                # draw borders for the different types of FE
                for b in data['Borders']:
                    if k == 'FE1' or k == 'FE2':
                        lines2.append(ROOT.TLine(b[0]+192, b[1], b[2]+192, b[3]))
                    else:
                        lines2.append(ROOT.TLine(b[0], b[1], b[2], b[3]))
                    lines3.append(ROOT.TLine(192, 400, 192, 0))
                for i in range(len(data['Borders'])):
                    lines2[j*len(data['Borders']) + i].SetLineColor(ROOT.kBlack)
                    lines2[j*len(data['Borders']) + i].SetLineWidth(1)
                    lines2[j*len(data['Borders']) + i].Draw('same')
                # draw extra vertical lines to show where the area of the active FEs is ending
                for i in range(len(lines3)):
                    lines3[i].SetLineColor(ROOT.kGray+2)
                    lines3[i].SetLineWidth(1)
                    lines3[i].SetLineStyle(3)
                    lines3[i].Draw('same')

                # draw texts on the plot, including FE information (and counts)
                pals2_dt[k] = []
                if j == 0 or j == 2:
                    loc1 = [0.55*384, 325, 0.95*384, 350]
                    offset2 = -135
                else:   
                    loc1 = [0.05*384, 50, 0.45*384, 75]
                    offset2 = 130
                for i_tex, tex in enumerate(['syn', 'lin', 'diff']):
                    line3 = ''.join(('FE: ', CONFIG_MOD[k], ' (', tex, ')'))
                    pals2_dt[k].append(ROOT.TPaveText(loc1[0], loc1[1], loc1[2], loc1[3]))
                    loc1[1], loc1[3] = loc1[1] + offset2, loc1[3] + offset2
                    pals2_dt[k][i_tex].AddText(line3)
                    pals2_dt[k][i_tex].SetBorderSize(1)
                    pals2_dt[k][i_tex].Draw()
            else:
                # Define default boarders and lines for empty quadrants
                for b in [[0.0, 0.0, 384.0, 0.0], [0.0, 127.0, 384.0, 127.0], [0.0, 263.0, 384.0, 263.0], [0.0, 400.0, 384.0, 400.0]]:
                    lines2.append(ROOT.TLine(b[0], b[1], b[2], b[3]))
                pals2_dt[k] = []
                line_tot2 = 'Not Available'
                pals2_dt[k].append(ROOT.TPaveText(0.25*384, 0.40*400, 0.75*384, 0.6*400))
                pals2_dt[k][0].AddText(line_tot2)
                pals2_dt[k][0].SetBorderSize(1)
                pals2_dt[k][0].Draw()

            # Setting the axis labels
            if (j == 2 or j == 0) and data:
                hs_diff[j].GetXaxis().SetTitle('Rows')
                hs_diff[j].GetYaxis().SetTitle('Columns')
                hs_diff[j].GetYaxis().ChangeLabel(0, -1, -1, -1, ROOT.kBlack, -1, '400')
                hs_diff[j].GetYaxis().ChangeLabel(1, -1, -1, -1, ROOT.kBlack, -1, '350')
                hs_diff[j].GetYaxis().ChangeLabel(2, -1, -1, -1, ROOT.kBlack, -1, '300')
                hs_diff[j].GetYaxis().ChangeLabel(3, -1, -1, -1, ROOT.kBlack, -1, '250')
                hs_diff[j].GetYaxis().ChangeLabel(4, -1, -1, -1, ROOT.kBlack, -1, '200')
                hs_diff[j].GetYaxis().ChangeLabel(5, -1, -1, -1, ROOT.kBlack, -1, '150')
                hs_diff[j].GetYaxis().ChangeLabel(6, -1, -1, -1, ROOT.kBlack, -1, '100')
                hs_diff[j].GetYaxis().ChangeLabel(7, -1, -1, -1, ROOT.kBlack, -1, '50')
                hs_diff[j].GetYaxis().ChangeLabel(8, -1, -1, -1, ROOT.kBlack, -1, ' ')
                hs_diff[j].GetXaxis().ChangeLabel(7, -1, -1, -1, ROOT.kBlack, -1, '350')
                hs_diff[j].GetXaxis().ChangeLabel(6, -1, -1, -1, ROOT.kBlack, -1, '300')
                hs_diff[j].GetXaxis().ChangeLabel(5, -1, -1, -1, ROOT.kBlack, -1, '250')
                hs_diff[j].GetXaxis().ChangeLabel(4, -1, -1, -1, ROOT.kBlack, -1, '200')
                hs_diff[j].GetXaxis().ChangeLabel(3, -1, -1, -1, ROOT.kBlack, -1, '150')
                hs_diff[j].GetXaxis().ChangeLabel(2, -1, -1, -1, ROOT.kBlack, -1, '100')
                hs_diff[j].GetXaxis().ChangeLabel(1, -1, -1, -1, ROOT.kBlack, -1, '50')
            elif j == 3 and data:
                hs_diff[j].GetXaxis().SetTitle('Rows')
                hs_diff[j].GetXaxis().ChangeLabel(7, -1, -1, -1, ROOT.kBlack, -1, '34')
                hs_diff[j].GetXaxis().ChangeLabel(6, -1, -1, -1, ROOT.kBlack, -1, '84')
                hs_diff[j].GetXaxis().ChangeLabel(5, -1, -1, -1, ROOT.kBlack, -1, '134')
                hs_diff[j].GetXaxis().ChangeLabel(4, -1, -1, -1, ROOT.kBlack, -1, '184')
                hs_diff[j].GetXaxis().ChangeLabel(3, -1, -1, -1, ROOT.kBlack, -1, '234')
                hs_diff[j].GetXaxis().ChangeLabel(2, -1, -1, -1, ROOT.kBlack, -1, '284')
                hs_diff[j].GetXaxis().ChangeLabel(1, -1, -1, -1, ROOT.kBlack, -1, '334')
            else:       
                hs_diff[j].GetXaxis().SetLabelSize(0)
                hs_diff[j].GetYaxis().SetLabelSize(0)

            hs_diff[j].GetZaxis().SetRangeUser(0.,axis_range)
            hs_diff[j].GetZaxis().SetRangeUser(-150.,axis_range) 
            hs_diff[j].GetZaxis().SetTitle('Noise [e]')
            hs_diff[j].GetZaxis().SetTitleOffset(1.4)

            c3.Update()

        ######### Same thing again for all other plots than difference plot of thresholdnoisecan ###############
        
        c1.cd(0)
        pads.append(ROOT.TPad('pad', 'pad',
                              pads_fraction[j][0], pads_fraction[j][1], pads_fraction[j][2], pads_fraction[j][3]))

        # set the margin=0, according to the location
        if j // 2 == 0:
            pads[j].SetBottomMargin(0)
        else:
            pads[j].SetTopMargin(0)
        if j % 2 == 0:
            pads[j].SetRightMargin(0)
            pads[j].SetLeftMargin(0.15)
        else:
            pads[j].SetLeftMargin(0)
            pads[j].SetRightMargin(0.15)
        pads[j].Draw()
        pads[j].cd()

        if j == 1:
            # Use own defined palette
            set_color_palette()
            hs[j].Draw('colz')
        else:
            hs[j].Draw('col')
        hs[j].SetStats(0)

        if data is not None:
            # draw borders of different types of FE
            for b in data['Borders']:
                if k == 'FE1' or k == 'FE2':
                    lines.append(ROOT.TLine(b[0]+192, b[1], b[2]+192, b[3]))
                else:
                    lines.append(ROOT.TLine(b[0], b[1], b[2], b[3]))
                lines3.append(ROOT.TLine(192, 400, 192, 0))
            for i in range(len(data['Borders'])):
                lines[j*len(data['Borders']) + i].SetLineColor(ROOT.kBlack)
                lines[j*len(data['Borders']) + i].SetLineWidth(1)
                lines[j*len(data['Borders']) + i].Draw('same')
            # draw extra vertical lines to show where the area of the active FEs is ending
            for i in range(len(lines3)):
                lines3[i].SetLineColor(ROOT.kGray+2)
                lines3[i].SetLineWidth(1)
                lines3[i].SetLineStyle(3)
                lines3[i].Draw('same')

            # draw texts on the plot, including FE information (and counts)
            pals_dt[k] = []
            if j == 0 or j == 2:
                loc0 = [0.55*384, 325, 0.95*384, 350]
                offset = -135
            else:   
                loc0 = [0.05*384, 50, 0.45*384, 75]
                offset = 130
            for i_tex, tex in enumerate(['syn', 'lin', 'diff']):
                line1 = ''.join(('FE: ', CONFIG_MOD[k], ' (', tex, ')'))
                if z_range == 0:
                    if scan == 'digitalscan':
                        line2 = 'counts: {}/{}={:.2f}%'.format(FE_count[tex][1], FE_count[tex][2],\
                                                                   100*FE_count[tex][1]/FE_count[tex][2]) # all dead / total # of pixels
                    else:
                        # dead not masked (dead and masked) / total # of pixels
                        line2 = 'counts: {}({})/{}={:.2f}%({:.2f}%)'.format(FE_count[tex][3], FE_count[tex][0], FE_count[tex][2],\
                                                                           100*FE_count[tex][3]/FE_count[tex][2], 100*FE_count[tex][0]/FE_count[tex][2]) 
                else:
                    if scan == 'discbumpscan' and type(z_range) is not tuple:
                        line2 = '{} bad, {} unknown, {} good, {} dead'.format(FE_count[tex][0], FE_count[tex][1], FE_count[tex][2], FE_count[tex][3])
                    else:
                        line2 = 'counts: {}/{}={:.2f}%'.format(FE_count[tex][0], FE_count[tex][1],
                                                           100*FE_count[tex][0]/FE_count[tex][1]) # pixels in range / total # of pixels

                pals_dt[k].append(ROOT.TPaveText(loc0[0], loc0[1], loc0[2], loc0[3]))
                loc0[1], loc0[3] = loc0[1] + offset, loc0[3] + offset
                # if not specified occupancy range, then only print first line
                if z_range is None:
                    pals_dt[k][i_tex].AddText(line1)
                else:   # otherwise add the counts into the text
                    pals_dt[k][i_tex].AddText(line2) 
                pals_dt[k][i_tex].SetBorderSize(1)
                pals_dt[k][i_tex].Draw()

        else:
            # define default boarders and lines for empty quadrants
            for b in [[0.0, 0.0, 384.0, 0.0], [0.0, 127.0, 384.0, 127.0], [0.0, 263.0, 384.0, 263.0], [0.0, 400.0, 384.0, 400.0]]:
                lines.append(ROOT.TLine(b[0], b[1], b[2], b[3]))

            pals_dt[k] = []
            line_tot = 'Not Available'
            pals_dt[k].append(ROOT.TPaveText(0.25*384, 0.40*400, 0.75*384, 0.6*400))
            pals_dt[k][0].AddText(line_tot)
            pals_dt[k][0].SetBorderSize(1)
            pals_dt[k][0].Draw()

        # setting the axis labels
        if (j == 2 or j == 0) and data:
            hs[j].GetXaxis().SetTitle('Rows')
            hs[j].GetYaxis().SetTitle('Columns')
            hs[j].GetYaxis().ChangeLabel(0, -1, -1, -1, ROOT.kBlack, -1, '400')
            hs[j].GetYaxis().ChangeLabel(1, -1, -1, -1, ROOT.kBlack, -1, '350')
            hs[j].GetYaxis().ChangeLabel(2, -1, -1, -1, ROOT.kBlack, -1, '300')
            hs[j].GetYaxis().ChangeLabel(3, -1, -1, -1, ROOT.kBlack, -1, '250')
            hs[j].GetYaxis().ChangeLabel(4, -1, -1, -1, ROOT.kBlack, -1, '200')
            hs[j].GetYaxis().ChangeLabel(5, -1, -1, -1, ROOT.kBlack, -1, '150')
            hs[j].GetYaxis().ChangeLabel(6, -1, -1, -1, ROOT.kBlack, -1, '100')
            hs[j].GetYaxis().ChangeLabel(7, -1, -1, -1, ROOT.kBlack, -1, '50')
            hs[j].GetYaxis().ChangeLabel(8, -1, -1, -1, ROOT.kBlack, -1, ' ')
            hs[j].GetXaxis().ChangeLabel(6, -1, -1, -1, ROOT.kBlack, -1, '350')
            hs[j].GetXaxis().ChangeLabel(5, -1, -1, -1, ROOT.kBlack, -1, '300')
            hs[j].GetXaxis().ChangeLabel(4, -1, -1, -1, ROOT.kBlack, -1, '250')
            hs[j].GetXaxis().ChangeLabel(3, -1, -1, -1, ROOT.kBlack, -1, '200')
            hs[j].GetXaxis().ChangeLabel(2, -1, -1, -1, ROOT.kBlack, -1, '150')
            hs[j].GetXaxis().ChangeLabel(1, -1, -1, -1, ROOT.kBlack, -1, '100')
            hs[j].GetXaxis().ChangeLabel(0, -1, -1, -1, ROOT.kBlack, -1, '50')
        elif j == 3 and data:
            hs[j].GetXaxis().SetTitle('Rows')
            hs[j].GetXaxis().ChangeLabel(7, -1, -1, -1, ROOT.kBlack, -1, '34')
            hs[j].GetXaxis().ChangeLabel(6, -1, -1, -1, ROOT.kBlack, -1, '84')
            hs[j].GetXaxis().ChangeLabel(5, -1, -1, -1, ROOT.kBlack, -1, '134')
            hs[j].GetXaxis().ChangeLabel(4, -1, -1, -1, ROOT.kBlack, -1, '184')
            hs[j].GetXaxis().ChangeLabel(3, -1, -1, -1, ROOT.kBlack, -1, '234')
            hs[j].GetXaxis().ChangeLabel(2, -1, -1, -1, ROOT.kBlack, -1, '284')
            hs[j].GetXaxis().ChangeLabel(1, -1, -1, -1, ROOT.kBlack, -1, '334')
        else:       
            hs[j].GetXaxis().SetLabelSize(0)
            hs[j].GetYaxis().SetLabelSize(0)

        hs[j].GetZaxis().SetRangeUser(0.,axis_range)
        if scan == 'discbumpscan' and axis_range == 101:
            hs[j].GetZaxis().SetRangeUser(99.,axis_range)
            hs[j].GetZaxis().SetNdivisions(3)
        elif scan == 'thresholdnoisescan' and data is not None:
            hs[j].GetZaxis().SetRangeUser(0.,200.) 
        elif z_range is not None and type(z_range) is tuple and z_range[0] is not 0 and (scan == 'analogscan' or scan == 'digitalscan'):
            hs[j].GetZaxis().SetRangeUser(98.,102.)

        if z_range == 0:
            if scan == 'noisescan' or scan == 'crosstalkscan' or scan == 'discbumpscan' or scan == 'analogscan':
                set_color_palette_zero()
                hs[j].GetZaxis().SetNdivisions(3)
                hs[j].GetZaxis().ChangeLabel(0, 90., -1, -1, ROOT.kBlack, -1, ' ')
                hs[j].GetZaxis().ChangeLabel(1, 90., -1, -1, ROOT.kBlack, -1, 'dead (masked/not masked)')
                hs[j].GetZaxis().ChangeLabel(2, 0., -1, -1, ROOT.kBlack, -1, 'nm')
                hs[j].GetZaxis().ChangeLabel(3, 0., -1, -1, ROOT.kBlack, -1, 'msk')
                hs[j].GetZaxis().SetLabelOffset(0.02)
                hs[j].GetZaxis().Paint('R')
            else:
                ROOT.gStyle.SetPalette(51)
                ROOT.TColor().InvertPalette()
                hs[j].GetZaxis().SetNdivisions(3)
                hs[j].GetZaxis().ChangeLabel(0, 90., -1, -1, ROOT.kBlack, -1, ' ')
                hs[j].GetZaxis().ChangeLabel(1, 0., -1, -1, ROOT.kBlack, -1, ' ')
                hs[j].GetZaxis().ChangeLabel(2, 90., -1, -1, ROOT.kBlack, -1, 'dead pixel')
                hs[j].GetZaxis().ChangeLabel(3, 0., -1, -1, ROOT.kBlack, -1, ' ')
                hs[j].GetZaxis().SetLabelOffset(0.02)
                hs[j].GetZaxis().Paint('R')
        elif z_range is not None and type(z_range) is not tuple and z_range != 0 and scan == 'discbumpscan':
            # set 3 color block palette for discbumpscan analysis
            set_color_palette_block()
            hs[j].GetZaxis().SetNdivisions(3)
            hs[j].GetZaxis().ChangeLabel(0, 0., -1, -1, ROOT.kBlack, -1, '')
            hs[j].GetZaxis().ChangeLabel(1, 45., -1, 1, ROOT.kBlack, -1, 'dead')
            hs[j].GetZaxis().ChangeLabel(2, 0., -1, -1, ROOT.kBlack, -1, 'good')
            hs[j].GetZaxis().ChangeLabel(3, 0., -1, -1, ROOT.kBlack, -1, 'unknown')
            hs[j].GetZaxis().ChangeLabel(4, 0., -1, -1, ROOT.kBlack, -1, 'bad')
            hs[j].GetZaxis().SetLabelOffset(0.01)
            hs[j].GetZaxis().Paint('R')
        elif z_range is not None and type(z_range) is tuple and z_range[0] is not 0 and (scan == 'analogscan' or scan == 'digitalscan'):
            hs[j].GetZaxis().SetNdivisions(5)
            hs[j].GetZaxis().Paint('R')
        else:
            if scan == 'thresholdscan' or scan == 'fordwardbiasscan':
                hs[j].GetZaxis().SetTitle('Threshold [e]')
            elif scan == 'thresholdnoisescan':
                hs[j].GetZaxis().SetTitle('Noise [e]')
            else:
                hs[j].GetZaxis().SetTitle('Occupancy')
            hs[j].GetZaxis().SetTitleOffset(1.4)

        c1.Update()
        j+= 1

        ######################################### For all scans ###############################
        # add the counting information for the whole module at the given stage
        if j == 4 and z_range is not None:
            tex_count = ROOT.TLatex()
            tex_count.SetNDC()
            tex_count.SetTextSize(0.025)
            c1.cd() 
            if z_range == 0:
                if scan == 'digitalscan':
                    text = ''.join(('Total counts: ', str(zeros_count), '/', str(192*400*4), '=',str(round(zeros_count/(192*400*4)*100, 3)), '% dead pixels'))
                    tex_count.DrawText(0.5, 0.965, text)
                else:
                    text = ''.join(('Total counts: ', str(dead_not_masked_count), '(nm), (', str(dead_masked_count), '(msk))', '/', str(192*400*4), '=(', str(round(dead_not_masked_count/(192*400*4)*100, 3)), '%)',str(round(dead_masked_count/(192*400*4)*100, 3)), '% dead'))
                    tex_count.DrawText(0.45, 0.965, text)
            else:
                if z_range is not None and type(z_range) is not tuple and z_range != 0 and scan == 'discbumpscan':
                    text = ''.join(('Total good bumps: ', str(tot_count), '/', str(192*400*4), \
                                    '=', str(round(tot_count/(192*400*4)*100, 3)), '%'))
                else:
                    text = ''.join(('Total counts: ', str(tot_count), '/', str(192*400*4), \
                                    '=', str(round(tot_count/(192*400*4)*100, 3)), '%'))
                tex_count.DrawText(0.5, 0.965, text)   
     
    for output in output_format:
        if output == '.png':
            range_name = ''
            if z_range is not None:
                if type(z_range) == tuple:
                    if len(z_range) == 2:
                        range_name = '_{}-{}'.format(z_range[0], z_range[1])
                    elif len(z_range) == 1:
                        range_name = '_{}-'.format(z_range[0])
                else:
                    range_name = '_{}'.format(z_range)
            output_name = output_folder+'{}_{}_{}{}{}'.format(scan, hist_name, stage, range_name, output)
            output_name2 = output_folder+'{}_{}_{}_{}{}{}'.format(scan, hist_name, 'diff', stage, range_name, output)
            c1.SaveAs(output_name)
            if scan == 'thresholdnoisescan':
                c3.SaveAs(output_name2)
        elif output == '.pdf':
            if scan == 'digitalscan':
                c1.Print(output_folder+mod_ID+'overview_Digital.pdf(', 'pdf')
            elif scan == 'analogscan':
                c1.Print(output_folder+mod_ID+'overview_Analog.pdf(', 'pdf')
            elif scan == 'discbumpscan':
                c1.Print(output_folder+mod_ID+'overview_DiscBump.pdf(', 'pdf')
            elif scan == 'crosstalkscan':
                c1.Print(output_folder+mod_ID+'overview_Crosstalk.pdf(', 'pdf')
            elif scan == 'noisescan':
                c1.Print(output_folder+mod_ID+'overview_Source.pdf(', 'pdf')
            else:
                c1.Print(output_folder+mod_ID+'overview_Threshold.pdf(','pdf')
                if scan == 'thresholdnoisescan':
                    c3.Print(output_folder+mod_ID+'overview_Threshold.pdf(','pdf')
                
    # write number of functioning FEs in csv file
    with open(output_folder+'{}_{}.{}'.format('FE_counter', 'evolution', 'csv'), 'a') as csvfile:
        writer = csv.writer(csvfile)
        writer.writerow([mod_ID, scan, hist_name, stage, FE_counter])

    return pixel_count_glo

# ========================= Plot Histograms =====================================

def plot_hists(data_dt, data_fwb_dt, data_source_dt, hist_name, FE_type, compare_type='Ratio'):
    ''' Plot the 1D histograms. There are 3 types of 1D histograms
    plotted, which are placed in 2x3 layout: 
    1st page: up to 6 individual histograms at each stage of one
    module are plotted with raw data and fitted by a Gaussian fit.
    2nd page: Comparison of the histograms.
    1. All histograms of the up to 6 stages are plotted into one for
    pure comparison.  
    2-6. one-to-one comparison (e.g. stage1 v.s. stage2 & stage2
    v.s. stage3, ...), upper panel shows the pure comparison of the
    histogram, and lower panel shows the ratio/% change
    The input to this function are the data dictionaries (2nd one for
    forwardbias data, 3rd one for source data) from the plot_hist1ds
    function, with keys = stages, values = data from .json, and the
    histogram name (e.g. 'NoiseDist'), and the comparison type
    ('Ratio'/'Percentage').
    Output is the mean value dictionary for the evolution plot. The
    mean value of the fitted Gaussian curve will be stored for the
    threshold and the noise scan. For the totscan there is no fit but
    the mean value is also stored.  '''

    latex = ROOT.TLatex()
    latex.SetNDC()

    mean_dt = {}    # for fitted mean value of Gaussian curve for threshold and noise and tot mean values

    # book canvas and styling of the plots
    c1 = ROOT.TCanvas('canvas1_'+hist_name+'_'+FE_type, 'canvas1_'+hist_name+'_'+FE_type, 2100, 1500)
    c1.Divide(3, 2)     # 2 by 3 layout
    ROOT.gStyle.SetTitleY(1.0)
    ROOT.gStyle.SetTitleFontSize(0.03)

    # ==================================================================================
    # ========================= plotting individual histograms =========================
    # ==================================================================================
    
    j = 0  # index for the canvas
    c1.cd()

    if hist_name == 'ThresholdDist' or hist_name == 'NoiseDist' or hist_name == 'MeanTotDist':
        hs = [] # list of histograms
        latex.SetTextSize(0.022)
        latex.DrawText(0.25, 0.975, hist_name + ' - ' + FE_type)
        # loop over stages and data in the dictionary
        for st, data in data_dt.items():
            for ix in range(2,7):
                if 'Stage'+str(ix) in st:
                    # if in any of the stages something is missing, add empty histo
                    for k in range(ix-j-1):
                        hs.append(ROOT.TH1D())
                    j = ix - 1  
            c1.cd(j+1)
            c1.cd(j+1).SetLeftMargin(0.13)
            # title of the histogram: e.g. [diff FE] NoiseDist-0 (Stage1)
            title = ''.join(('[', FE_type, ' FE] ', data['Name'], ' (', st, ')'))
            # book histogram
            hs.append(ROOT.TH1D('h1_'+str(st), title, data['x']['Bins'], data['x']['Low'], data['x']['High']))
            hs[j].GetXaxis().SetTitle(data['x']['AxisTitle'])
            hs[j].GetYaxis().SetTitle(data['y']['AxisTitle'])
            hs[j].SetStats(0)
            if hist_name == 'MeanTotDist':
                hs[j].SetMaximum(max(data['Data']) * 1.5)
            else:
                hs[j].SetMaximum(max(data['Data']) * 1.25)
            hs[j].GetYaxis().SetTitleOffset(1.8)
            # fill histograms
            for i, h in enumerate(data['Data']):
                hs[j].SetBinContent(i, h)   # height of the bin
                if hist_name != 'MeanTotDist':
                    hs[j].SetBinError(i, h**0.5)    # Poissonian std for threshold and noise
                else:
                    hs[j].SetBinError(i, 0)

            # Determine the fitting range of the Gaussian by looking
            # at PlotConfig_threshFWB They options are a specified
            # range (list with start and end), the whole range or
            # automatic.

            input_range = PLOT_CONFIG['Hist1D'][hist_name]['FitRange'][FE_type][st]
            if type(input_range) is list:   
                fit_range = PLOT_CONFIG['Hist1D'][hist_name]['FitRange'][FE_type][st]
            elif input_range == 'Full':
                fit_range = [data['x']['Low'], data['x']['High']]
            elif input_range == 'Auto':
                '''  automatically determine the range by iteration
                1. assume the center to be the averaged x location of the highest 4 points
                2. define the range by (center - std, center + std)
                where std is from standard deviation from the raw histogram (same as RMS here)
                3. check if chi2/ndf <= 10, if yes, then return this results,
                if not, then iterate the range, by replacing std by the fitted std from the previous Gaussian
                4. if iterated 10 times, then stop and use the last result.
                '''
                # std from histogram, sorted array of bin numbers with largest elements first, used to determine the x location,bin width
                std, sort_idx, x_avg, bin_width = hs[j].GetStdDev(), np.argsort(data['Data'])[::-1], 0, \
                                                  (data['x']['High'] - data['x']['Low']) / data['x']['Bins']
                # finding the averaged x location of the 4 highest points for all plots except totscan
                try:
                    if hist_name != 'MeanTotDist':
                        for i in range(4):
                            x_avg += bin_width * (sort_idx[i]) + data['x']['Low']
                        x_avg = x_avg / 4
                        # iterate the fitting range up to ten times
                        for i in range(10):
                            fit_range = [x_avg - std, x_avg + std]
#                            fit_range = [x_avg - std*3, x_avg + std*1.5] #Modified for KEKQ19, Goe5, Liv8, and Paris11
                            gaussFit = ROOT.TF1('gaussfit', 'gaus', float(fit_range[0]), float(fit_range[1]))
                            hs[j].Fit(gaussFit, 'R')  # fit inside a given range
                            if gaussFit.GetNDF() != 0:
                                if gaussFit.GetChisquare() / (gaussFit.GetNDF()) > 10:
                                    std = gaussFit.GetParameter(2)
                                else:
                                    break
                    else: 
                        # for the ToT scan directly pick the bin with the highest entry as the binning is very large
                        x_avg = bin_width * (sort_idx[0]) + data['x']['Low']
                        hs[j].Draw()
                except:
                    continue
            else: continue

            # fit information: mean, std, chis/ndf
            latex.SetTextSize(0.03)
            if hist_name != 'MeanTotDist':
                mean, std = gaussFit.GetParameter(1), gaussFit.GetParameter(2)
                chi2, ndf = gaussFit.GetChisquare(), gaussFit.GetNDF()
                latex.DrawText(0.55, 0.8, 'mean = %.1f;    std = %.1f' % (mean, std))
                latex.DrawText(0.55, 0.75, 'Fit range: (%.1f, %.1f)' % (float(fit_range[0]), float(fit_range[1])))
                if ndf != 0:
                    latex.DrawLatex(0.55, 0.7, '#chi^{2}/n.d.f.: %.1f/%.1f = %.1f' % (chi2, ndf, chi2 / (ndf)))
                # latex.DrawText(0.55, 0.65, 'RMS: %.1f;    sigma: %.1f' % (hs[j].GetRMS(), hs[j].GetStdDev()))
            else: 
                mean, std = x_avg - 0.5, 0 # For some reason 0.5 more than the actual BC values is given
                latex.DrawText(0.7, 0.7, 'mean = %.1f' % (mean))
            
            # module information: module ID and front-end ID
            latex.SetTextSize(0.04)
            latex.DrawText(0.150, 0.85, 'Module: ' + mod_ID)
            latex.SetTextSize(0.035)
            latex.DrawText(0.150, 0.8, FE_key + ': ' + ID)
            c1.Update()      
            # store the fitted mean from Gaussian to dictionary
            mean_dt[st] = [mean, std]
            j += 1
            
        # ==================================================================================
        # ========================= plotting all-in-one comparison =========================
        # ==================================================================================

        # put the comparison plots on another canvas
        c2 = ROOT.TCanvas('canvas2_'+hist_name, 'canvas2_'+hist_name, 2100, 1500)
        c2.Divide(3, 2)     # 2 by 3 layout
        ROOT.gStyle.SetTitleY(1.0)
        ROOT.gStyle.SetTitleFontSize(0.04)
        kk=0
        c2.cd(kk+1)
        c2.cd(kk+1).SetLeftMargin(0.13)

        latex.SetTextSize(0.022)
        latex.DrawText(0.25, 0.975, hist_name + ' - ' + FE_type)

        # find the width of the bin, for later 're-scaling' the x range
        bins_width = {stage: (data['x']['High'] - data['x']['Low']) / data['x']['Bins'] for stage, data in data_dt.items()}

        if (len(np.unique(list(bins_width.values())))) != 1:
            same_width = False
        else:
            same_width = True

        # min & max of x axis of all histograms
        try:
            xmin = min([v['x']['Low'] for k, v in data_dt.items()])
            xmax = max([v['x']['High'] for k, v in data_dt.items()])
        except: 
            pass

        # filling the empty bins with zeros
        for k, data in data_dt.items():
            data['Data'] = [0.0] * math.ceil((data['x']['Low'] - xmin) / bins_width[k]) + data['Data'] + [0.0] * math.ceil(
                (xmax - data['x']['High']) / bins_width[k])

        # book legend, which color corresponds to which stage
        legend = ROOT.TLegend(0.7, 0.5, 0.88, 0.75)
        legend.SetTextSize(0.04)
        # colors, list of histograms for re-scaled plots, index of hs_rescale
        colors, hs_rescale, jj = {'Stage1': ROOT.kAzure-3, 'Stage2': ROOT.kGreen+3, 'Stage3': ROOT.kOrange+1, 'Stage4': ROOT.kGray+1, 'Stage5': ROOT.kRed+1, 'Stage6': ROOT.kCyan-3}, [], 0
        
        for stage, data in data_dt.items():
            title = ''.join(('[', FE_type, ' FE] ', data['Name'], ' (Comparison)'))
            # book histogram
            hs_rescale.append(ROOT.TH1D('h11_'+str(stage), title, len(data['Data']), xmin, xmax))
            hs_rescale[jj].GetXaxis().SetTitle(data['x']['AxisTitle'])
            hs_rescale[jj].GetYaxis().SetTitle(data['y']['AxisTitle']+"/"+data['x']['AxisTitle'])
            hs_rescale[jj].GetYaxis().SetTitleOffset(1.8)
            hs_rescale[jj].SetStats(0)
            hs_rescale[jj].SetLineColor(colors[stage])
            # fill histogram
            for i, h in enumerate(data['Data']):
                hs_rescale[jj].SetBinContent(i, h)
                hs_rescale[jj].SetBinError(i, h**0.5)
            # normalize by bin width
            hs_rescale[jj].Scale(1.0, "width")
            legend.AddEntry(hs_rescale[jj], stage)  # add legend
            jj += 1
        try:
            ymax = max([hs.GetMaximum() for hs in hs_rescale])  # highest point among all histogram
            # Draw the re-scaled plots
            for idx, h_rescale in enumerate(hs_rescale):
                if idx == 0:    # first hisotgram, then set the y max and 'ehist' draw option
                    h_rescale.SetMaximum(ymax * 1.25)
                    h_rescale.Draw('ehist')
                else:   # later ones use 'ehistsame' draw option
                    h_rescale.Draw('ehistsame')
                h_rescale.GetYaxis().SetTitleOffset(1.8)
        except:
            pass

        # draw legend
        legend.SetLineWidth(1)
        legend.Draw('same')

        # book latex, draw module information
        latex.SetTextSize(0.04)
        latex.DrawText(0.150, 0.85, 'Module: ' + mod_ID)
        latex.SetTextSize(0.035)
        latex.DrawText(0.150, 0.8, FE_key + ': ' + ID)

        # ==================================================================================
        # ========================= plotting comparison & ratio ============================
        # ==================================================================================
     
        # list of histos for ratio plots, legend for the ratio plots, lines for the ratio plots (just horizontal line (e.g. at 0 or 1) for reference), index of hs_ratio, lines, etc
        hs_ratio, legends_ratio, lines, jjj = [], [], [], 0

        for k, data in data_dt.items():
            jjj += 1
            # no comparison for the first stage
            if jjj == 1:
                data_previous = data['Data']    # store for comparing with stage2 in the next loop
                k_previous = k  # k='Stage1'
                continue

            if same_width:
                # filling the lower panel (comparison panel: 'Ratio'/'Percentage')
                hs_ratio.append(ROOT.TH1D('', '', len(data['Data']), xmin, xmax))
                for i, h in enumerate(data['Data']):
                    try:
                        if compare_type == 'Ratio':     # if 'Ratio', fill 'Stage_{n}/Stage_{n-1}'
                            hs_ratio[jjj - 2].SetBinContent(i, h/data_previous[i])
                            hs_ratio[jjj - 2].SetBinError(i, h/data_previous[i] * (1/h + 1/data_previous[i])**0.5)
                            hs_ratio[jjj - 2].GetYaxis().SetTitle(k+'/'+k_previous)
                            lines.append(ROOT.TLine(xmin, 1, xmax, 1))  # line at y=1 for reference
                            hs_ratio[jjj - 2].GetYaxis().SetRangeUser(0, 2)
                        elif compare_type == 'Percentage':  # if 'Percentage', fill 'Stage_{n}/Stage_{n-1} - 1'
                            hs_ratio[jjj - 2].SetBinContent(i, h/data_previous[i]-1)
                            hs_ratio[jjj - 2].SetBinError(i, h/data_previous[i] * (1/h + 1/data_previous[i])**0.5)
                            hs_ratio[jjj - 2].GetYaxis().SetTitle(k+'/'+k_previous)
                            lines.append(ROOT.TLine(xmin, 0, xmax, 0))  # line at y=0 for reference
                            hs_ratio[jjj - 2].GetYaxis().SetRangeUser(-1, 1)
                    except ZeroDivisionError:   # when there is no data in the bin
                        pass

            c2.cd(kk + 2)

            # upper panel for pure 1-1 comparison on the hiostograms
            pad1 = ROOT.TPad(' pad1 ', ' pad1 ', 0, 0.3, 1, 1)
            pad1.Draw()
            pad1.cd()
            pad1.SetLeftMargin(0.15)

            if same_width:
                pad1.SetBottomMargin(0)
            hs_rescale[jjj - 1].SetMaximum(ymax * 1.25)
            hs_rescale[jjj - 1].Draw('ehist')
            hs_rescale[jjj - 2].Draw('ehistsame')
            hs_rescale[jjj - 1].GetYaxis().SetLabelSize(0.05)
            hs_rescale[jjj - 1].GetYaxis().SetTitleSize(0.05)
            hs_rescale[jjj - 1].GetYaxis().SetTitleOffset(1.4)
            hs_rescale[jjj - 1].GetXaxis().SetLabelSize(0.05)
            hs_rescale[jjj - 1].GetXaxis().SetTitleSize(0.05)
            hs_rescale[jjj - 1].GetXaxis().SetLabelOffset(0.015)
            if same_width:
                hs_ratio[jjj - 2].GetYaxis().SetNdivisions(8)
                hs_ratio[jjj - 2].SetStats(0)
                hs_ratio[jjj - 2].SetLineColor(colors[k])

            # book legend
            legends_ratio.append(ROOT.TLegend(0.7, 0.6, 0.85, 0.75))
            legends_ratio[jjj - 2].SetTextSize(0.05)
            legends_ratio[jjj - 2].SetLineWidth(1)
            legends_ratio[jjj - 2].AddEntry(hs_rescale[jjj - 2], k_previous)
            legends_ratio[jjj - 2].AddEntry(hs_rescale[jjj - 1], k)
            legends_ratio[jjj - 2].Draw('same')

            # book latex, write module information
            latex.SetTextSize(0.055)
            latex.DrawText(0.165, 0.83, 'Module: ' + mod_ID)
            latex.SetTextSize(0.05)
            latex.DrawText(0.165, 0.78, FE_key + ': ' + ID)

            c2.cd(kk + 2)
            if same_width:
                # lower panel for 'Ratio' or 'Percentage'
                pad2 = ROOT.TPad(' pad2 ', ' pad2 ', 0, 0, 1, 0.3)
                pad2.SetTopMargin(0)
                pad2.SetBottomMargin(0.25)
                pad2.SetLeftMargin(0.15)
                pad2.Draw()
                pad2.cd()

                # draw reference line
                hs_ratio[jjj - 2].GetXaxis().SetTitle(data['x']['AxisTitle'])
                hs_ratio[jjj - 2].GetXaxis().SetLabelSize(0.11)
                hs_ratio[jjj - 2].GetXaxis().SetLabelOffset(0.03)
                hs_ratio[jjj - 2].GetXaxis().SetTitleSize(0.11)
                hs_ratio[jjj - 2].GetYaxis().SetLabelSize(0.11)
                hs_ratio[jjj - 2].GetYaxis().SetTitleSize(0.11)
                hs_ratio[jjj - 2].GetYaxis().SetTitleOffset(0.5)
                hs_ratio[jjj-2].Draw('pe')
                try: 
                    lines[jjj - 2].SetLineColor(ROOT.kBlack)
                    lines[jjj - 2].SetLineWidth(2)
                    lines[jjj-2].Draw('same')
                except: pass

            kk += 1
            k_previous = k
            data_previous = data['Data']
        c2.Update()          

    # ==================================================================================
    # ========================= plotting fwb comparison =========================
    # ==================================================================================
    
    if hist_name == 'NoiseMap' or hist_name == 'ThresholdMap':

        hs_fwb, hs_source, legends = [], [], [] # list of fwb comp histograms, list of source histograms, list of legends 
        latex.SetTextSize(0.022)
        latex.DrawText(0.25, 0.975, hist_name + ' - ' + FE_type)
   
        for st, data_fwb in data_fwb_dt.items():
            legends.append(ROOT.TLegend(0.63, 0.55, 0.88, 0.65))
            legends[j].SetTextSize(0.03)

            data_zip, data_source = [], [] # list for fwb and normal data zipped, list for source data   
            # If in any of the stages something is missing, add empty histos and legend entry
            for ix in range(2,7):
                if 'Stage'+str(ix) in st:
                    for k in range(ix-j-1):
                        hs_fwb.append(ROOT.TH1D())
                        hs_source.append(ROOT.TH1D())
                        legends.append(ROOT.TLegend(0.63, 0.55, 0.88, 0.65))
                    j = ix - 1  
            c1.cd(j+1)
            c1.cd(j+1).SetLeftMargin(0.13)
            c1.cd(j+1).SetLogy()
            # get original threshold data
            try:
                data = data_dt[st]
            except:
                print("Skipping " + st + " of fwb comparison plots")
                continue
            try:
                source = data_source_dt[st]
            except:
                pass
            # title of the histogram: e.g. [diff FE] NoiseDist-0 (Stage1)
            title = ''.join(('[', FE_type, ' FE] #Delta', data['Name'], ' (', st, ')'))
            # book histogram
            if hist_name == 'NoiseMap':
                hs_fwb.append(ROOT.TH1D('h1_fwb_'+str(st), title, data['x']['Bins'], -600, 600))
                hs_fwb[j].GetXaxis().SetTitle('#DeltaNoise (FWB-HVon) [e]')
                try:
                    hs_source.append(ROOT.TH1D('h1_source_'+str(st), title, data['x']['Bins'], -600, 600))
                    hs_source[j].GetXaxis().SetTitle('#DeltaNoise (FWB-HVon) [e]')
                except:
                    pass
            if hist_name == 'ThresholdMap':
                hs_fwb.append(ROOT.TH1D('h1_fwb_'+str(st), title, data['x']['Bins'], -3000, 3000))
                hs_fwb[j].GetXaxis().SetTitle('#DeltaThreshold (FWB-HVon) [e]')
                try:
                    hs_source.append(ROOT.TH1D('h1_source_'+str(st), title, data['x']['Bins'], -3000, 3000))
                    hs_source[j].GetXaxis().SetTitle('#DeltaThreshold (FWB-HVon) [e]')
                except:
                    pass
            hs_fwb[j].GetYaxis().SetTitle('Number of pixels')
            hs_fwb[j].SetStats(0)
            hs_fwb[j].SetMaximum(10000)
            hs_fwb[j].GetYaxis().SetTitleOffset(1.8)
            try:
                hs_source[j].GetYaxis().SetTitle('Number of pixels')
                hs_source[j].SetStats(0)
                hs_source[j].SetMaximum(10000)
                hs_source[j].GetYaxis().SetTitleOffset(1.8)
                hs_source[j].SetLineColor(ROOT.kRed)
            except:
                pass
            
            # fill separately the histograms for the different FW types
            disc_counter_0 = 0 # counting number of pixels where difference is 0
            disc_counter_10 = 0 # counting number of pixels where difference withing |10|, 10 to be adjusted if more clarity on definition
            source_counter_0 = 0 # counting number of pixels where the source scan is also 0
            if FE_type == 'syn':
                for i in range(0,128):
                    data_zip += list(zip(data['Data'][i],data_fwb['Data'][i]))
                    try:
                        data_source += source['Data'][i]
                    except:
                        pass
                # filling histograms
                for i, h in enumerate(data_zip):   
                    hs_fwb[j].Fill(data_zip[i][1]-data_zip[i][0])   # difference of fwb threshold/noise values and biased values
                    if (data_zip[i][1] - data_zip[i][0]) == 0:
                        disc_counter_0 += 1
                    if (data_zip[i][1] - data_zip[i][0]) > -10 and (data_zip[i][1] - data_zip[i][0]) < 10:
                        disc_counter_10 += 1
                    try:
                        if data_source[i] == 0:
                            source_counter_0 += 1
                            hs_source[j].Fill(data_zip[i][1]-data_zip[i][0])
                    except:
                        pass
            if FE_type == 'lin':
                for i in range(128,264):
                    data_zip += list(zip(data['Data'][i],data_fwb['Data'][i]))
                    try:
                        data_source += source['Data'][i]
                    except:
                        pass
                # filling histograms
                for i, h in enumerate(data_zip):   
                    hs_fwb[j].Fill(data_zip[i][1]-data_zip[i][0])   
                    if (data_zip[i][1] - data_zip[i][0]) == 0:
                        disc_counter_0 += 1
                    if (data_zip[i][1] - data_zip[i][0]) > -10 and (data_zip[i][1] - data_zip[i][0]) < 10:
                        disc_counter_10 += 1
                    try:
                        if data_source[i] == 0:
                            source_counter_0 += 1
                            hs_source[j].Fill(data_zip[i][1]-data_zip[i][0])
                    except:
                        pass
            if FE_type == 'diff':
                for i in range(264,400):
                    data_zip += list(zip(data['Data'][i],data_fwb['Data'][i]))
                    try:
                        data_source += source['Data'][i]
                    except:
                        pass
                # filling histograms
                for i, h in enumerate(data_zip):
                    hs_fwb[j].Fill(data_zip[i][1]-data_zip[i][0]) 
                    if (data_zip[i][1] - data_zip[i][0]) == 0:
                        disc_counter_0 += 1
                    if (data_zip[i][1] - data_zip[i][0]) > -10 and (data_zip[i][1] - data_zip[i][0]) < 10:
                        disc_counter_10 += 1
                    try:
                        if data_source[i] == 0:
                            source_counter_0 += 1
                            hs_source[j].Fill(data_zip[i][1]-data_zip[i][0])
                    except: 
                        pass
            hs_fwb[j].Draw()
            try:
                hs_source[j].Draw('same')
            except:
                pass
            # draw legend
            legends[j].AddEntry(hs_fwb[j], 'all pixels')  # add legend for fwb histo
            try:
                legends[j].AddEntry(hs_source[j], 'w/o source hit')  # add legend for source no hit pixel
            except:
                pass
            legends[j].SetLineWidth(1)
            legends[j].Draw('same')

            # book latex, draw module information
            latex.SetTextSize(0.04)
            latex.DrawText(0.150, 0.85, 'Module: ' + mod_ID)
            latex.SetTextSize(0.035)
            latex.DrawText(0.15, 0.8, FE_key + ': ' + ID)
            latex.SetTextSize(0.03)
            latex.DrawLatex(0.6, 0.8, '#Delta == 0: ' + str(disc_counter_0) + ' pixels')
            latex.DrawLatex(0.6, 0.75, '#Delta < |10|: ' + str(disc_counter_10) + ' pixels')
            latex.DrawLatex(0.6, 0.7, '0 source hits: ')
            latex.DrawLatex(0.75, 0.67, str(source_counter_0) + ' pixels')
            
            j += 1
        c1.Update()
    
    for output in output_format:
        if output == '.png':
            output_name = output_folder+'{}_{}_{}{}'.format(ID, FE_type, hist_name, output)
            output_name2 = output_folder+'{}_{}_{}_{}{}'.format(ID, FE_type, hist_name, 'comparison', output)
            try:
                c1.SaveAs(output_name)
                c2.SaveAs(output_name2)
            except:
                continue
        elif output == '.pdf':
            try:
                c1.Print(output_folder+mod_ID+'overview_Threshold.pdf(', 'pdf')
                c2.Print(output_folder+mod_ID+'overview_Threshold.pdf(', 'pdf')
            except:
                continue

    # return dictionary of the mean values extracted from the Gaussian fit
    try:
        return mean_dt
    except:
        return 0
 
# ========================= Evolution Plot =====================================

def evolution_plot(counts_dt, scan, z_range=None, y_type=None): 
    ''' Function to plot the evolution plots for all the values
    analysed such as the number of dead pixel per stage and FE. The
    counts_dt has the number of pixels in specific ranges
    specified. '''

    # book canvas and styling
    c1 = ROOT.TCanvas('canvas1_'+scan+'_'+str(z_range), 'canvas1_'+scan+'_'+str(z_range), 2100, 1500)
    c1.SetGrid()
    c1.Divide(2, 2)     # 2 by 2 layout
    ROOT.gStyle.SetTitleFontSize(0.03)

    # define csv files for FE count, disconnected bumps and noise and threshold evolution
    with open(output_folder+'{}_{}.{}'.format('FE_counter', 'evolution', 'csv'), 'w') as csvfile:
                writer = csv.writer(csvfile)
                writer.writerow(['ModuleID', 'Scan', 'HistName', 'Stage', '#FEs'])
    if z_range == 0:
        if scan == 'sourcescan' or scan == 'discbumpscan' or scan == 'analogscan':
            with open(output_folder+'{}_{}_{}.{}'.format(scan, 'evolution', z_range, 'csv'), 'w') as csvfile:
                writer = csv.writer(csvfile)
                writer.writerow(['ModuleID', 'Chip', 'ChipID', 'FE', 'Stage', 'Range', '#pixels_mask', '#pixels_nm'])
        elif scan == 'digitalscan':
            with open(output_folder+'{}_{}_{}.{}'.format(scan, 'evolution', z_range, 'csv'), 'w') as csvfile:
                writer = csv.writer(csvfile)
                writer.writerow(['ModuleID', 'Chip', 'ChipID', 'FE', 'Stage', 'Range', '#dead_pixels'])
    elif scan == 'NoiseDist':
        with open(output_folder+'{}_{}.{}'.format(scan, 'evolution', 'csv'), 'w') as csvfile:
            writer = csv.writer(csvfile)
            writer.writerow(['ModuleID', 'Chip', 'ChipID', 'FE', 'Stage', 'Noise[e]', 'Stdev[e]'])
    elif scan == 'ThresholdDist':
        with open(output_folder+'{}_{}.{}'.format(scan, 'evolution', 'csv'), 'w') as csvfile:
            writer = csv.writer(csvfile)
            writer.writerow(['ModuleID', 'Chip', 'ChipID', 'FE', 'Stage', 'Threshold[e]', 'Stdev[e]'])
    elif scan == 'MeanTotDist':
        with open(output_folder+'{}_{}.{}'.format(scan, 'evolution', 'csv'), 'w') as csvfile:
            writer = csv.writer(csvfile)
            writer.writerow(['ModuleID', 'Chip', 'ChipID', 'FE', 'Stage', 'MeanTot[bc]', 'Stdev[bc]'])
    elif scan == 'analogscan':
        with open(output_folder+'{}_{}_{}.{}'.format(scan, 'evolution', z_range, 'csv'), 'w') as csvfile:
            writer = csv.writer(csvfile)
            writer.writerow(['ModuleID', 'Chip', 'ChipID', 'FE', 'Stage', 'Range', '#pixels_in_range'])
    elif scan == 'digitalscan':
        with open(output_folder+'{}_{}_{}.{}'.format(scan, 'evolution', z_range, 'csv'), 'w') as csvfile:
            writer = csv.writer(csvfile)
            writer.writerow(['ModuleID', 'Chip', 'ChipID', 'FE', 'Stage', 'Range', '#pixels_in_range'])

    l, j, mgrs, legs, legs2, grs = 0, 0, [], [], [], []   # index for canvas (FE1 to FE4), multi graphs, legend, list of graph (syn, lin, diff)

    for fe in ['FE1', 'FE4', 'FE2', 'FE3']:
        c1.cd(l+1)
        tick, ymax, ymin, colors = 0, -999999, 999999,  {'syn': ROOT.kAzure-3, 'lin': ROOT.kRed+1, 'diff': ROOT.kBlack, 'std': ROOT.kGreen+3}  # used to determine the y axis range

        # book graph, put into a multi graph
        mgrs.append(ROOT.TMultiGraph())
       
        # fill the points in the graph
        for fe_type in ['syn', 'lin', 'diff', 'std']:
            got_graph = False
            # stage, y value, errors
            stg, stg2, count_masked, count_not_masked, thr_errx, thr_erry = array('f'), array('f'), array('f'), array('f'), array('f'), array('f')
            for i, sg in enumerate([i for i in data_folder_path.keys()]): 
                try:
                    if y_type == 'Absolute':    # absolute number
                        # fill the dead pixel plots
                        if z_range == 0:
                            if scan == 'digitalscan':
                                # book small legend
                                legend = ROOT.TLegend(0.1, 0.77, 0.19, 0.9)
                                legs.append(legend)
                                legs[l].SetTextSize(0.04)
                                legs[l].SetMargin(0.4)
                                count_not_masked.append(counts_dt[fe][fe_type][sg][1]) # all dead, there are no dead and notmasked, but store in this variable for consistency
                                with open(output_folder+'{}_{}_{}.{}'.format(scan, 'evolution', z_range, 'csv'), 'a') as csvfile:
                                    writer = csv.writer(csvfile)
                                    # dead pixels
                                    writer.writerow([mod_ID, fe, CONFIG_MOD[fe], fe_type, sg, z_range, counts_dt[fe][fe_type][sg][1]]) 
                            elif scan == 'crosstalkscan' or scan == 'discbumpscan' or scan == 'analogscan' or scan == 'sourcescan':
                                # book larger legend
                                legend = ROOT.TLegend(0.09, 0.75, 0.25, 0.98)
                                legs.append(legend)
                                legs[l].SetTextSize(0.04)
                                legs[l].SetMargin(0.3)
                                count_not_masked.append(counts_dt[fe][fe_type][sg][3]) # not masked but dead
                                count_masked.append(counts_dt[fe][fe_type][sg][0])  # dead and masked 
                                if scan == 'discbumpscan' or scan == 'sourcescan' or scan == 'analogscan':
                                    with open(output_folder+'{}_{}_{}.{}'.format(scan, 'evolution', z_range, 'csv'), 'a') as csvfile:
                                        writer = csv.writer(csvfile)
                                        # dead and masked, dead and not masked
                                        writer.writerow([mod_ID, fe, CONFIG_MOD[fe], fe_type, sg, z_range, counts_dt[fe][fe_type][sg][0], counts_dt[fe][fe_type][sg][3]]) 
                        # fill the special analysis discbump plot
                        elif scan == 'discbumpscan' and z_range is not None and type(z_range) is not tuple and z_range != 0:
                            count_not_masked.append(counts_dt[fe][fe_type][sg][0]) # for discbump this is bad pixels
                            count_masked.append(counts_dt[fe][fe_type][sg][2])  # for discbump this is good pixels
                            # book larger legend
                            legend = ROOT.TLegend(0.09, 0.75, 0.23, 0.98)
                            legs.append(legend)
                            legs[l].SetTextSize(0.04)
                            legs[l].SetMargin(0.3)
                        # fill all other 2D evolution analyses
                        else:
                            # book small legend
                            legend = ROOT.TLegend(0.1, 0.77, 0.19, 0.9)
                            legs.append(legend)
                            legs[l].SetTextSize(0.04)
                            legs[l].SetMargin(0.4)
                            count_not_masked.append(counts_dt[fe][fe_type][sg][0]) # count of pixels in the given range 
                            if scan == 'analogscan':
                                with open(output_folder+'{}_{}_{}.{}'.format(scan, 'evolution', z_range, 'csv'), 'a') as csvfile:
                                    writer = csv.writer(csvfile)
                                    # pixels in given range
                                    writer.writerow([mod_ID, fe, CONFIG_MOD[fe], fe_type, sg, z_range, counts_dt[fe][fe_type][sg][0]]) 
                            if scan == 'digitalscan':
                                with open(output_folder+'{}_{}_{}.{}'.format(scan, 'evolution', z_range, 'csv'), 'a') as csvfile:
                                    writer = csv.writer(csvfile)
                                    # pixels in given range
                                    writer.writerow([mod_ID, fe, CONFIG_MOD[fe], fe_type, sg, z_range, counts_dt[fe][fe_type][sg][0]]) 
                        legs[l].SetLineWidth(0)
                        thr_errx.append(0.0)
                        thr_erry.append(0.0)
                        got_graph = True                    
                    elif y_type == 'Percentage':    # percentage (might be useful for pixel counts)
                        # ATTENTION: Not used so far, so set element you actually want when using it
                        count_not_masked.append(counts_dt[fe][fe_type][sg][1]/counts_dt[fe][fe_type][sg][2]*100)
                        thr_errx.append(0.0)
                        thr_erry.append(0.0)
                        got_graph = True
                    else:
                        # For threshold and noise and tot evolution plots
                        legend = ROOT.TLegend(0.1, 0.77, 0.19, 0.9)
                        legs.append(legend)
                        legs[l].SetTextSize(0.04)
                        legs[l].SetMargin(0.4)
                        count_not_masked.append(counts_dt[fe][fe_type][sg][0]) # mean saved in not masked counts
                        thr_errx.append(0.01) # give a tiny x error to allow programme to work
                        thr_erry.append(counts_dt[fe][fe_type][sg][1])
                        if scan != 'MeanTotDist':
                            with open(output_folder+'{}_{}.{}'.format(scan, 'evolution', 'csv'), 'a') as csvfile:
                                writer = csv.writer(csvfile)
                                # mean, error
                                writer.writerow([mod_ID, fe, CONFIG_MOD[fe], fe_type, sg, '%.1f' % counts_dt[fe][fe_type][sg][0], '%.1f' % counts_dt[fe][fe_type][sg][1]])
                        else: # MeanTotDist
                            with open(output_folder+'{}_{}.{}'.format(scan, 'evolution', 'csv'), 'a') as csvfile:
                                writer = csv.writer(csvfile)
                                writer.writerow([mod_ID, fe, CONFIG_MOD[fe], fe_type, sg, '%.1f' % counts_dt[fe][fe_type][sg][0], '%.1f' % counts_dt[fe][fe_type][sg][1]])
                        got_graph = True
                    # shift the x axis points so that the entries do not lie over each other for the different FEs
                    if fe_type == 'syn' or fe_type == 'std':
                        stg.append(i+1)
                    elif fe_type == 'lin':
                        stg.append(i+1.15)
                    else:
                        stg.append(i+1.3)
                    # if there is a distinction between masked and unmaksed dead pixels shift further 
                    if count_masked:                   
                        if fe_type == 'syn' or fe_type == 'std':
                            stg2.append(i+1.45)
                        elif fe_type == 'lin':
                            stg2.append(i+1.6)
                        else:
                            stg2.append(i+1.75) 
                except: 
                    continue 

            if got_graph is True:
                try:
                    legs[l].SetLineWidth(1)
                except:
                    pass
                grs.append(ROOT.TGraphErrors(len(stg), stg, count_not_masked, thr_errx, thr_erry))
                grs[-1].SetLineColor(colors[fe_type])
                grs[-1].SetMarkerStyle(4)
                grs[-1].SetMarkerSize(1.5)
                grs[-1].SetMarkerColor(colors[fe_type])
                # for special discbump analysis
                if scan == 'discbumpscan' and z_range is not None and type(z_range) is not tuple and z_range != 0:
                    legs[l].AddEntry(grs[-1], fe_type + ' bad')
                # for scans with distinction in masked or not masked dead pixels
                elif scan == 'crosstalkscan' and z_range == 0 or scan == 'sourcescan' and z_range == 0 or scan == 'discbumpscan' and z_range == 0 or scan == 'analogscan' and z_range == 0:
                    legs[l].AddEntry(grs[-1], fe_type + ' (nm)')
                else:
                    legs[l].AddEntry(grs[-1], fe_type)
                mgrs[l].Add(grs[-1])
                # if there are masked and unmasked pixel
                try:
                    grs.append(ROOT.TGraphErrors(len(stg2), stg2, count_masked, thr_errx, thr_erry))
                    grs[-1].SetLineColor(colors[fe_type])
                    grs[-1].SetMarkerStyle(3)
                    grs[-1].SetMarkerSize(1.5)
                    grs[-1].SetMarkerColor(colors[fe_type])
                    # for special discbump analysis
                    if scan == 'discbumpscan' and z_range is not None and type(z_range) is not tuple and z_range != 0:
                        legs[l].AddEntry(grs[-1], fe_type + ' good')
                    # for the other 3 scans which have masked and unmasked distinction
                    elif scan == 'crosstalkscan' or scan == 'sourcescan' or scan == 'discbumpscan' or scan == 'analogscan':
                        legs[l].AddEntry(grs[-1], fe_type + ' (msk)')
                    mgrs[l].Add(grs[-1])
                except:
                    pass
                mgrs[l].Add(grs[-1])
           
                # dynamically set the range of the graphs
                if max(count_not_masked) > ymax:    # update max. of y
                    ymax = max(count_not_masked)
                if min(count_not_masked) < ymin:    # update min. of y
                    ymin = min(count_not_masked)
                try:
                    if max(count_masked) > ymax:    # update max. of y
                        ymax = max(count_masked)
                except:
                    pass
                try:
                    if min(count_masked) < ymin:    # update min. of y
                        ymin = min(count_masked)
                except:
                    pass
            else:
                # removing empty legends for non-existant scans
                if scan != 'thresholdscan':
                    if fe_type != 'std': # std is manually put into the fe_types, so this must be ignored
                        try:
                            legs[l].SetLineWidth(0)
                        except: 
                            pass
                else:
                    if mgrs[l] is {}:
                        legs[l].SetLineWidth(0)
        j += 1

        # printing titles on the plots
        title1 = ''.join(('Module: ', mod_ID, ', ', CONFIG_MOD[fe], ' (', fe, ')'))
        title2 = ''.join((scan, ' Evolution plot'))
        if z_range is not None:
            if z_range == 0:
                range_title = 'occupancy=0'
            elif type(z_range) == tuple:
                if len(z_range) == 2:
                    if z_range[0] == z_range[1]:
                        range_title = str('occupancy<' + str(z_range[0]))
                    else:
                        range_title = str(z_range[0]) + '<occupancy<' + str(z_range[1])
                else:
                    range_title = 'occupancy>' + str(z_range[0])
            elif z_range != 0:
                if scan == 'discbumpscan':
                    range_title = 'analysis'
                else:
                    range_title = 'occupancy>' + str(z_range)
         
            title2 += ' (' + range_title + ')'

        title = '#splitline{' + title1 + '}{' + title2 + '}'
        mgrs[l].SetTitle(title)
        mgrs[l].GetYaxis().SetTitleOffset(1.4)
        mgrs[l].GetXaxis().SetTitle('Stage')

        # formatting the plot
        if y_type == 'Percentage':
            mgrs[l].GetYaxis().SetTitle('Fraction [%]')
        else:
            if z_range == 0:
                mgrs[l].GetYaxis().SetTitle('Number of dead pixels')
            elif scan == 'NoiseDist':
                mgrs[l].GetYaxis().SetTitle('Noise [e]')
            elif scan == 'ThresholdDist':
                mgrs[l].GetYaxis().SetTitle('Threshold [e]')
            elif scan == 'MeanTotDist':
                mgrs[l].GetYaxis().SetTitleOffset(1.3)
                mgrs[l].GetYaxis().SetTitle('Mean ToT [BC]')
            elif z_range is not None and type(z_range) is tuple and z_range[0] is not 0 and (scan == 'analogscan' or scan == 'digitalscan' or scan == 'discbumpscan'):
                mgrs[l].GetYaxis().SetTitle('Number of good pixels')
            else:
                mgrs[l].GetYaxis().SetTitle('Number of pixels')

        # adjusting the axis ranges for the specific plots, trying to get rid of useless plots due to too high pixel counts
        if ymin == ymax:
            mgrs[l].GetHistogram().SetMinimum(ymin - 3)
            mgrs[l].GetHistogram().SetMaximum(ymax + 3)
        else:
            if scan == 'discbumpscan':
                if z_range is not None and type(z_range) is not tuple and z_range != 0:
                    ymax = 15000
                if ymax > 500:
                    if z_range == 0:
                        ymax = 150
            elif scan == 'sourcescan':
                if ymax > 500:
                    if z_range == 0:
                        ymax = 20
                    else:
                        ymax = 800
            elif scan == 'analogscan' or scan == 'digitalscan':
                if ymax > 10000 and z_range != 100:
                    ymax = 20000
            mgrs[l].GetHistogram().SetMinimum(ymin * 0.75 - (ymax - ymin))
            mgrs[l].GetHistogram().SetMaximum(ymax * 1.25 + (ymax - ymin))
        mgrs[l].GetXaxis().SetRangeUser(0.8, 7.2)

        mgrs[l].Draw('APe')
        try:
            legs[l].Draw('same')
        except:
            pass
        l += 1

    c1.Update()

    for output in output_format:
        if output == '.png':
            range_name = ''
            if z_range is not None:
                if type(z_range) == tuple:
                    if len(z_range) == 2:
                        range_name = '_{}-{}'.format(z_range[0], z_range[1])
                    elif len(z_range) == 1:
                        range_name = '_{}-'.format(z_range[0])
                else:
                    range_name = '_{}'.format(z_range)
            output_name = output_folder+'{}_{}{}{}'.format(scan, 'evolution', range_name, output)
            c1.SaveAs(output_name)
        elif output == '.pdf':
            if scan == 'digitalscan':
                c1.Print(output_folder+mod_ID+'overview_Digital.pdf(', 'pdf')
            elif scan == 'analogscan':
                c1.Print(output_folder+mod_ID+'overview_Analog.pdf(', 'pdf')
            elif scan == 'sourcescan':
                c1.Print(output_folder+mod_ID+'overview_Source.pdf(', 'pdf')
            elif scan == 'discbumpscan':
                c1.Print(output_folder+mod_ID+'overview_DiscBump.pdf(', 'pdf')
            elif scan == 'crosstalkscan':
                c1.Print(output_folder+mod_ID+'overview_Crosstalk.pdf(', 'pdf')
            else:
                c1.Print(output_folder+mod_ID+'overview_Threshold.pdf(','pdf')


# ========================= Plot IV =====================================

def plot_iv_curve(data_dt, fit_point, fit_step, E_eff=1.21, T_ref=293.15):
    ''' Plot sensor IV curve, and extract leakage current at specified
    bias voltage. Input the data dictionary (keys=stages,
    values=data), and the bias voltage, and the fitting step (width of
    the range to fit line).  '''

    # book canvas and styling
    c1 = ROOT.TCanvas('canvas1', 'canvas1', 2100, 1500)
    c1.Divide(4, 2)
    c1.SetGrid()
    ROOT.gStyle.SetTitleY(1.0)
    ROOT.gStyle.SetTitleFontSize(0.05)

    # define csv files for IV evolution
    with open(output_folder+'{}_{}.{}'.format('IV', 'evolution', 'csv'), 'w') as csvfile:
                writer = csv.writer(csvfile)
                writer.writerow(['ModuleID', 'Stage', 'Voltage', 'IC', 'IC_e'])

    # temperature correction for measured current
    k_B = 8.617333262145e-5
    def temp_corr(T, T_ref=T_ref, E_eff=E_eff):
        return (T_ref / T) ** 2 * np.exp(-E_eff / (2 * k_B) * (1 / T_ref - 1 / T))      
  
    def pointbypoint(data_iv,T_meas):
        try:
            Vs, Is, Ts, Vs_e, Is_e, Ts_e = array('f'), array('f'), array('f'), array('f'), array('f'), array('f')
            for ele in data_iv:
                Vs.append(abs(ele['Voltage']))
                Vs_e.append(0)
                Is.append(abs(ele['Current_mean']))
                Is_e.append(ele['Current_sigma'])
                Ts.append(T_meas)  # set T of module testing setup
                Ts_e.append(1)  # 1°C error as it is not logged in the file
            return Vs, Is, Ts, Vs_e, Is_e, Ts_e
        except KeyError:
            raise

    def arraybyarray(data_iv,T_meas):
        try:
            # voltage, current, temperature; voltage error, current error, temperature error
            Vs, Is, Ts, Vs_e, Is_e, Ts_e = array('f'), array('f'), array('f'), array('f'), array('f'), array('f')
            if len(data_iv['Current']) != len(data_iv['Voltage']):
                print("len(data_iv['Current']) != len(data_iv['Voltage']")
            for i in range(len(data_iv['Current'])):
                Vs.append(abs(data_iv['Voltage'][i]))
                Vs_e.append(0)
                Is.append(abs(data_iv['Current'][i]))
                Is_e.append(0)
                Ts.append(T_meas)  # set T of module testing setup
                Ts_e.append(1)  # 1°C error as it is not logged in the file
            return Vs, Is, Ts, Vs_e, Is_e, Ts_e
        except KeyError:
            raise
    
    j, grs, grs_pure, fits, texs, XMAX, YMAX = -1, [], [], [], [], 0, 0   # index for canvas, list of graphs
    stageskipped = [True, True, True, True, True, True]
    for ix in range(6):
        for stage, data in data_dt.items():
            if 'Stage'+str(ix+1) in stage:
                T_meas = CONFIG_MOD['Temperature'][stage]
                stageskipped[ix] = False
                for k in range(ix-j-1):
                    grs.append(ROOT.TGraphErrors())
                    grs_pure.append(ROOT.TGraphErrors())
                    fits.append(ROOT.TF1())
                c1.cd(ix+1)
                c1.cd(ix+1).SetRightMargin(0.05)
                c1.cd(ix+1).SetLeftMargin(-2)

                try:
                    Vs, Is, Ts, Vs_e, Is_e, Ts_e = arraybyarray(data['values'],T_meas)
                except KeyError:
                    Vs, Is, Ts, Vs_e, Is_e, Ts_e = pointbypoint(data['Sensor_IV'],T_meas)
                
                # corrected current
                ICs = array('f')
                for i, t in enumerate(Ts):
                    ICs.append(Is[i] * temp_corr(t))
              
                # corrected current error
                ICs_e = array('f')
                beta = E_eff/(2*k_B)
                for i, t in enumerate(Is_e):
                    try:
                        ICs_e.append(ICs[i] * ((t/Is[i])**2 + ((2*Ts[i]+beta)/Ts[i])**2 * (Ts_e[i]/Ts[i])**2)**0.5)
                    except:
                        continue
                
                # write IC and IC_e for 50V and 100V in csv file
                with open(output_folder+'{}_{}.{}'.format('IV', 'evolution', 'csv'), 'a') as csvfile:
                    writer = csv.writer(csvfile)
                    for i,v in enumerate(Vs):
                        if v == 50 or v == 100:
                            writer.writerow([mod_ID, stage, v, ICs[i], ICs_e[i]])

                xmax = max(Vs)
                ymax = max(ICs)

                if xmax > XMAX:
                    XMAX = xmax
                if ymax > YMAX:
                    YMAX = ymax

                # plot graph once there will be temperatures to be corrected
                grs.append(ROOT.TGraphErrors(len(Vs), Vs, ICs, Vs_e, ICs_e))
                grs_pure.append(ROOT.TGraphErrors(len(Vs), Vs, ICs, Vs_e, ICs_e))
                # title e.g. Sensor IV curve (Stage1)
                title = 'Sensor IV curve (' + stage + ')'
                grs[ix].SetTitle(title)
                grs[ix].GetXaxis().SetTitle('Reversed Bias [V]')
                grs[ix].GetYaxis().SetTitle('Leakage Current [A]')
                grs[ix].GetYaxis().SetTitleOffset(1.3)
                grs[ix].GetYaxis().SetTitleSize(0.04)
                grs[ix].GetYaxis().SetLabelSize(0.042)
                grs[ix].GetYaxis().SetLabelOffset(0.01)
                grs[ix].GetXaxis().SetLabelSize(0.042)
                #grs[ix].GetXaxis().SetLabelOffset(0.01)
                grs[ix].GetXaxis().SetTitleOffset(1.0)
                grs[ix].GetXaxis().SetTitleSize(0.04)

                # fit a linear line to extract leakage current
                fit_range = (fit_point[stage] - fit_step[stage], fit_point[stage] + fit_step[stage])
                fits.append(ROOT.TF1('func', '[0] + [1]*x', fit_range[0], fit_range[1]))
                grs[ix].Fit('func', 'R')
                grs[ix].Draw('ALP')

                # book latex
                latex = ROOT.TLatex()
                latex.SetNDC()

                # module information (module ID)
                latex.SetTextSize(0.05)
                latex.DrawText(0.125, 0.85, 'Module: ' + mod_ID)
                
                # fit information (Fitting range, fitted eqn, extracted leakage current, chi2/ndf)
                latex.SetTextSize(0.04)
                latex.DrawText(0.2, 0.35, 'Linear fit range: [%.1i, %.1i] V' % (fit_range[0], fit_range[1]))
                # extract parameter (slope, intersection)
                para = fits[ix].GetParameters()
                # rescale the parameters for writing it in the latex in a better way
                scale_1 = (max([np.floor(np.log10(abs(para[0]))), np.floor(np.log10(abs(para[1])))]))
                #scale_ = scale_1+(3-scale_1%3) 
                scale_ = scale_1-scale_1%3 
                latex.DrawLatex(0.2, 0.3, 'Fit: I = (%.1f + %.3f#timesV) A #times (%.e)' % (
                    para[0] / (10 ** scale_), para[1] / (10 ** scale_), 10 ** scale_))
    
                # extract leakage current
                fit_value =  para[0] + para[1] * fit_point[stage]
                latex.DrawLatex(0.2, 0.25, 'I(%.1f V) = %.2f A #times (%.e)' % (fit_point[stage], fit_value/(10 ** scale_),10 **scale_))
                # extract chi2/ndf
                chi2, ndf = fits[ix].GetChisquare(), fits[ix].GetNDF()
                if ndf != 0:
                    latex.DrawLatex(0.2, 0.2, '#chi^{2}/n.d.f. = %.2f/%i = %.2f' % (chi2, ndf, chi2 / (ndf)))

                latex.DrawLatex(0.2, 0.15, 'T_{meas} = %.2f K, corrected to 293.15 K' % (T_meas))

                c1.Update()
                j = ix

    legend = ROOT.TLegend(0.2, 0.6, 0.4, 0.85)
    legend.SetTextSize(0.04)
    colors = {'Stage1': ROOT.kAzure-3, 'Stage2': ROOT.kGreen+3, 'Stage3': ROOT.kOrange+1, 'Stage4': ROOT.kGray+1, 'Stage5': ROOT.kRed+1, 'Stage6': ROOT.kCyan-3}
    c1.cd(6 + 1)
    c1.cd(6 + 1).SetLeftMargin(2.9)
    c1.cd(6 + 1).SetRightMargin(0.05)

    # cut plot axis range for too high leakage currents
    if YMAX <= 5e-6:
        frame = c1.DrawFrame(0,0,XMAX,YMAX*1.1)
    else: 
        frame = c1.DrawFrame(0,0,XMAX,5e-6)
    title = 'Sensor IV curve (comparison)'
    frame.SetTitle(title)
    frame.GetXaxis().SetTitle('Reversed Bias [V]')
    frame.GetYaxis().SetTitle('Leakage Current [A]')
    frame.GetYaxis().SetTitleOffset(1.3)
    frame.GetYaxis().SetTitleSize(0.04)
    frame.GetYaxis().SetLabelSize(0.042)
    frame.GetYaxis().SetLabelOffset(0.01)
    frame.GetXaxis().SetLabelSize(0.042)
    frame.GetXaxis().SetTitleOffset(1.0)
    frame.GetXaxis().SetTitleSize(0.04)

    for i_gr in range(len(grs_pure)):
        if i_gr == 0:
            grs_pure[i_gr].Draw('LP')
        else:
            grs_pure[i_gr].Draw('same')
        grs_pure[i_gr].SetLineColor(colors[list(colors.keys())[i_gr]])

    for ix in range(6):
        if not stageskipped[ix]:
            legend.AddEntry(grs_pure[ix], list(colors.keys())[ix])  # add legend

    legend.SetLineWidth(1)
    legend.Draw('same')
    c1.Update()

    for output in output_format:
        if output == '.png':
            output_name = output_folder+'{}{}'.format('iv', output)
            c1.SaveAs(output_name)
        elif output == '.pdf':
            c1.Print(output_folder+mod_ID+'overview_IV_VI.pdf(', 'pdf')
           
# ========================= Plot VI =====================================

def plot_vi_curve(data_dt):
    ''' Plot the SLDO VI curve. Input the data dictionary.  '''

    # book canvas and styling
    c1 = ROOT.TCanvas('canvas1', 'canvas1', 2100, 1500)
    c1.Divide(4, 2)
    c1.SetGrid()
    ROOT.gStyle.SetTitleY(1.0)
    ROOT.gStyle.SetTitleFontSize(0.05)

    def pointbypoint(data_iv):
        try:
            Vs, Is, Vs_e, Is_e = array('f'), array('f'), array('f'), array('f')
            for ele in data_iv:
                Vs.append(abs(ele['Voltage_mean']))
                Vs_e.append(abs(ele['Voltage_sigma']))
                Is.append(abs(ele['Current']))
                Is_e.append(0)
            return Vs, Is, Vs_e, Is_e
        except KeyError:
            return None

    def arraybyarray(data_iv):
        try:
            Vs, Is, Vs_e, Is_e = array('f'), array('f'), array('f'), array('f')
            if len(data_iv['Current']) != len(data_iv['Voltage']):
                print("len(data_iv['Current']) != len(data_iv['Voltage']")
            for i in range(len(data_iv['Current'])):
                Vs.append(abs(data_iv['Voltage'][i]))
                Vs_e.append(0)
                Is.append(abs(data_iv['Current'][i]))
                Is_e.append(0)
            return Vs, Is, Vs_e, Is_e
        except KeyError:
            raise

    j, grs, XMAX, YMAX, YMIN = -1, [], 0, 0, 1   # index for canvas
    stageskipped = [True, True, True, True, True, True]
    for ix in range(6):
        for stage, data in data_dt.items():
            if 'Stage'+str(ix+1) in stage:
                stageskipped[ix] = False
                for k in range(ix-j-1):
                    grs.append(ROOT.TGraphErrors())

                c1.cd(ix+1)
                c1.cd(ix+1).SetLeftMargin(1.7)
                c1.cd(ix+1).SetRightMargin(0.05)

                # voltage, current; voltage error, current error
                try:
                    Vs, Is, Vs_e, Is_e = arraybyarray(data['values'])
                except KeyError:
                    Vs, Is, Vs_e, Is_e = pointbypoint(data['SLDO_VI'])

                xmax = max(Is)
                ymax = max(Vs)
                ymin = min(Vs)

                if xmax > XMAX:
                    XMAX = xmax
                if ymax > YMAX:
                    YMAX = ymax
                if ymin < YMIN:
                    YMIN = ymin

                grs.append(ROOT.TGraphErrors(len(Is), Is, Vs, Is_e, Vs_e))
                title = 'SLDO VI curve (' + stage + ')'
                grs[ix].SetTitle(title)
                grs[ix].GetXaxis().SetTitle('Current [A]')
                grs[ix].GetYaxis().SetTitle('Voltage [V]')
                grs[ix].GetYaxis().SetTitle('Leakage Current [A]')
                grs[ix].GetYaxis().SetTitleOffset(1.3)
                grs[ix].GetYaxis().SetTitleSize(0.04)
                grs[ix].GetYaxis().SetLabelSize(0.042)
                grs[ix].GetYaxis().SetLabelOffset(0.01)
                grs[ix].GetXaxis().SetLabelSize(0.042)
                grs[ix].GetXaxis().SetTitleOffset(1.0)
                grs[ix].GetXaxis().SetTitleSize(0.04)
                grs[ix].Draw('ALP')

                # book latex
                latex = ROOT.TLatex()
                latex.SetNDC()
                # module information (module ID)
                latex.SetTextSize(0.04)
                latex.DrawText(0.125, 0.85, 'Module: ' + mod_ID)
                c1.Update()
                j = ix;
    
    legend = ROOT.TLegend(0.2, 0.55, 0.4, 0.8)
    legend.SetTextSize(0.04)
    colors = {'Stage1': ROOT.kAzure-3, 'Stage2': ROOT.kGreen+3, 'Stage3': ROOT.kOrange+1, 'Stage4': ROOT.kGray+1, 'Stage5': ROOT.kRed+1, 'Stage6': ROOT.kCyan-3}
    c1.cd(6 + 1)
    c1.cd(6 + 1).SetLeftMargin(2.0)
    c1.cd(6 + 1).SetRightMargin(0.05)

    frame = c1.DrawFrame(0,YMIN*0.9,XMAX,YMAX*1.1)
    title = 'SLDO VI curve (comparison)'
    frame.SetTitle(title)
    frame.GetXaxis().SetTitle('Current [A]')
    frame.GetYaxis().SetTitle('Voltage [V]')
    frame.GetYaxis().SetTitleOffset(1.3)
    frame.GetYaxis().SetTitleSize(0.04)
    frame.GetYaxis().SetLabelSize(0.042)
    frame.GetYaxis().SetLabelOffset(0.01)
    frame.GetXaxis().SetLabelSize(0.042)
    frame.GetXaxis().SetTitleOffset(1.0)
    frame.GetXaxis().SetTitleSize(0.04)

    for i_gr in range(len(grs)):
        if i_gr == 0:
            grs[i_gr].Draw('LP')
        else:
            grs[i_gr].Draw('same')
        grs[i_gr].SetLineColor(colors[list(colors.keys())[i_gr]])

    for ix in range(6):
        if not stageskipped[ix]:
            legend.AddEntry(grs[ix], list(colors.keys())[ix])  # add legend

    legend.SetLineWidth(1)
    legend.Draw('same')
    c1.Update()
    
    for output in output_format:
        if output == '.png':
            output_name = output_folder+'{}{}'.format('vi', output)
            c1.SaveAs(output_name)
        elif output == '.pdf':
            c1.Print(output_folder+mod_ID+'overview_IV_VI.pdf(', 'pdf')
        
# ========================= Plot Graphs =====================================

def plot_graphs(graph, data_folder_path):
    ''' Function to actually call the iv and vi plotting functions'''
   
    if graph == 'IV':
        print("Getting '{}' files".format(graph))
        plot_dt = get_curve_file(data_folder_path, 'iv') 
        
    elif graph == 'VI':
        print("Getting '{}' files".format(graph))
        plot_dt = get_curve_file(data_folder_path, 'vi')
           
    if plot_dt:
        print("Getting '{}' data".format(graph))
        plot_data_dt = get_data(plot_dt)
                        
        if graph == 'IV':
            plot_iv_curve(data_dt=plot_data_dt, fit_point=PLOT_CONFIG['Graph'][graph]['FitPoint'],
                          fit_step=PLOT_CONFIG['Graph'][graph]['FitStep'], T_ref=PLOT_CONFIG['Graph'][graph]['Tref'])
        elif graph == 'VI':
            plot_vi_curve(data_dt=plot_data_dt)
 
# ========================= Define Folder Paths  =====================================

# start counter
tic = time.perf_counter()

# set to batch mode -> do not display graphics
ROOT.gROOT.SetBatch(True)

# read command line arguments
parser = argparse.ArgumentParser(description = 'Make comparison plots for different stages of module testing.')
parser.add_argument('-c', '--configfile', type=str, default='../Configuration/GenConfig.json', help='Path to the configuration file')
parser.add_argument('-o', '--outdir', type=str, default='./Figure', help='Output directory')
parser.add_argument('-p', '--plotconfig', type=str, default='../Configuration/PlotConfig.json', help='PlotConfig file')
args = parser.parse_args()

# expand user's ~ is they prefer to keep the files there
user_home_dir = os.path.expanduser('~')
args.configfile = os.path.realpath(args.configfile.replace('~',user_home_dir))
args.outdir = os.path.realpath(args.outdir.replace('~',user_home_dir))
args.plotconfig = os.path.realpath(args.plotconfig.replace('~',user_home_dir))

# output directory needs to have a trailing slash
if args.outdir[-1] != '/':
    args.outdir = args.outdir+'/'

# set output and configuration folder
output_folder = args.outdir    
config_folder = os.path.dirname(args.configfile)

# creat output folder if it does not yet exist
if not os.path.exists(output_folder):
    os.makedirs(output_folder)

# ========================= Load Data Files =====================================

# load GenConfig file which defines the module data being used for the analysis
genconf = open(args.configfile, 'r')
CONFIG_GEN = json.loads(genconf.read())

# read the module specific information (ModuleID, FE keys)
CONFIG_MOD = CONFIG_GEN['Module']
mod_ID = CONFIG_MOD['ModuleID']
FE_keys = [i for i in CONFIG_MOD.keys() if 'FE' in i]

output_format = CONFIG_GEN['OutputFormat']

# load PlotConfig file
plotconf = open(args.plotconfig, 'r')
PLOT_CONFIG = json.loads(plotconf.read())

hist1ds = list(PLOT_CONFIG['Hist1D'].keys())
hist2ds = list(PLOT_CONFIG['Hist2D'].keys())
graphs = list(PLOT_CONFIG['Graph'].keys())

data_folder_path = CONFIG_MOD['FolderPath']

# ========================= Information Slide =====================================

def set_infoslide():
    ''' Sets the information slide at the beginning of every pdf for each scan type'''

    c1 = ROOT.TCanvas('canvas1', 'canvas1')
    c1.cd()
    fe_type_text = ROOT.TLatex()
    fe_type_text.SetNDC()
    fe_type_text.SetTextSize(0.03)
    fe_type_text.DrawText(0.05, 0.9, 'Module testing stages comparison for: ' + mod_ID)

    for i in range(len([j for j in data_folder_path.keys()])):
        if data_folder_path['Stage' + str(i+1)][-60:] is not '':
            fe_type_text.DrawLatex(0.05, 0.78 - i*0.12, 'Stage ' + str(i+1) +': ...' + data_folder_path['Stage' + str(i+1)][-60:])
        if CONFIG_MOD['Temperature']['Stage' + str(i+1)] is not 1:
            fe_type_text.DrawLatex(0.15, 0.73 - i*0.12, 'IV: T_{meas} = %.1f K' % (CONFIG_MOD['Temperature']['Stage' + str(i+1)]))
      
    for name in hist2ds:
        if name == 'digitalscan':
            c1.Print(output_folder+mod_ID+'overview_Digital.pdf(', 'pdf')
        elif name == 'analogscan':
            c1.Print(output_folder+mod_ID+'overview_Analog.pdf(', 'pdf')
        elif name == 'crosstalkscan':
            c1.Print(output_folder+mod_ID+'overview_Crosstalk.pdf(', 'pdf')
        elif name == 'discbumpscan':
            c1.Print(output_folder+mod_ID+'overview_DiscBump.pdf(', 'pdf')
        elif name == 'sourcescan':
            c1.Print(output_folder+mod_ID+'overview_Source.pdf(', 'pdf')

    for k, name in enumerate([i for i in hist1ds]):
        if k == 0:
            c1.Print(output_folder+mod_ID+'overview_Threshold.pdf(', 'pdf')
            
    for graph in graphs:
        if graph == 'IV':
            c1.Print(output_folder+mod_ID+'overview_IV_VI.pdf(', 'pdf')

set_infoslide()

# ========================= Filling of Histrograms and Graphs =====================================
# ======================================== 1D Hist =========================================================

for hist1d in hist1ds:
    # for all given hist11ds in the plot config files the code is run
    fe_evo_dt = {}
    data_folder_path_fwb = CONFIG_MOD['FWB']
    if data_folder_path:
        for FE_key in FE_keys:  # (FE1, FE2, FE3, FE4)
            ID = CONFIG_MOD[FE_key]
            
            evo_dt = plot_hist1ds(hist1d, data_folder_path, data_folder_path_fwb, FE_key, CONFIG_MOD['Type']+['std'])
            fe_evo_dt[FE_key] = evo_dt

        if hist1d != 'ThresholdMap' and hist1d != 'NoiseMap':
            # no evolution plot is created for fwb analysis
            evolution_plot(fe_evo_dt, hist1d)
    else:
        continue

# ======================================== 2D Hist =========================================================

for scan in hist2ds:
    c4 = ROOT.TCanvas('canvas4_'+scan, 'canvas4_'+scan, 2100, 1500)
    # here for all the scans defined in the plot config files the z_ranges for analysis are defined 
    y_type = PLOT_CONFIG['Hist2D'][scan]['EvolutionPlot']['Y']
    if scan == 'digitalscan' or scan == 'analogscan':
        cut_point_1 = PLOT_CONFIG['Hist2D'][scan]['Cut1']
        cut_point_2 = PLOT_CONFIG['Hist2D'][scan]['Cut2']
        z_ranges = [None, (0), (0, cut_point_1), (cut_point_1-1, cut_point_2+1), (cut_point_2)]
    elif scan == 'sourcescan':
        cut_point_1 = PLOT_CONFIG['Hist2D'][scan]['Cut1']
        cut_point_2 = PLOT_CONFIG['Hist2D'][scan]['Cut2']
        z_ranges = [None, (0), (0,cut_point_1), (cut_point_1-1,cut_point_2)]
    elif scan == 'discbumpscan':
        cut_point_1 = PLOT_CONFIG['Hist2D'][scan]['Cut1']
        cut_point_2 = PLOT_CONFIG['Hist2D'][scan]['Cut2']
        z_ranges = [None, (0), (cut_point_1,cut_point_2), (cut_point_2)]
    elif scan == 'crosstalkscan':
        z_ranges = [None, (0)]
    else:
        z_ranges = [None]
     
    for z_range in z_ranges:
        # for all z_ranges the code is run
        evo_dt = plot_hist2ds(scan, z_range)
        if (z_range is not None) and (evo_dt is not None):
            evolution_plot(evo_dt, scan, z_range, y_type)

    # the output pdf files need to be closed (this adds an empty slide at the end)
    if scan == 'digitalscan':
        c4.Print(output_folder+mod_ID+'overview_Digital.pdf)', 'pdf')
    elif scan == 'analogscan':
        c4.Print(output_folder+mod_ID+'overview_Analog.pdf)', 'pdf')
    elif scan == 'crosstalkscan':
        c4.Print(output_folder+mod_ID+'overview_Crosstalk.pdf)', 'pdf')
    elif scan == 'discbumpscan':
        c4.Print(output_folder+mod_ID+'overview_DiscBump.pdf)', 'pdf')
    elif scan == 'sourcescan':
        c4.Print(output_folder+mod_ID+'overview_Source.pdf)', 'pdf') 
    elif scan == 'FWB':
        c4.Print(output_folder+mod_ID+'overview_Threshold.pdf)', 'pdf')

# ====================================== VI and IV Graphs ==================================================

c4 = ROOT.TCanvas('canvas4', 'canvas4')
for graph in graphs:
    if data_folder_path:
        plot_graphs(graph, data_folder_path)

# the output pdf file need to be closed (this adds an empty slide at the end)
c4.Print(output_folder+mod_ID+'overview_IV_VI.pdf)', 'pdf')

toc = time.perf_counter()
print(f"Executed the programme in {toc - tic:0.4f} seconds")
print('Done')
