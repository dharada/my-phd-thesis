import glob
import json
import numpy as np
from array import array
import ROOT

###### Find the folder with the scans ######

def get_tuned_folder(data_folder_path, stage, ID, types, scan='thresholdscan'):
    """ Find the folder names for different stages, and put them into
    a dictionary. Input the data folder path which contains all the
    results from QC test at the given stage, and the type of front-end
    ('lin', 'diff', 'syn', 'lindiff' or 'std'), and the scan name.
    Output is a dictionary with keys = stages ('Stage1',
    'Stage2',...), and values = list of data file path, which are
    after tuning.  """
    tuned_files_dt = {}
    # lists of folders for the scans
    folders_lt_1 = []
    folders_lt_2 = []
    # loop over the different FE types
    for type in types:
        # if there are more folders of the same scan, containing only one FE each
        for i in glob.glob(data_folder_path + '/'  + '*' + type + '*' + scan + '*'):
            folders_lt_1.append(i)
            # if there are scans of several FEs in one folder
        for i in glob.glob(data_folder_path + '/'  + '*' + type + '*' + scan + '*' + '/'+ID+'*'):
            folders_lt_2.append(i)
    code = -9999    # number code in front of the file name
    # e.g. 001244 & 001260 in '001244_lin_thresholdscan' & '001260_lin_thresholdscan'
    folder_tuned_1 = ""
    folder_tuned_2 = []
    for folder in folders_lt_1:
        # split '<data>/001244_lin_thresholdscan' -> '<data>', '001244_lin_thresholdscan'
        # -> '001244', 'lin', 'thresholdscan'
        code_i = int(folder.split(data_folder_path + '/')[1].split('_')[0])
        # !!! Previously used to find the max, in case there is more than folder with the same name:
        # currently NOT WORKING -> Programme fails with more than one folder of the same scan
        if code_i > code:    
            code = code_i
            folder_tuned_1 = folder.split(data_folder_path + '/')[1]
    for folder in folders_lt_2:
        folder_tuned_2.append(folder.split(data_folder_path + '/')[1].split('/')[0])
    folder_tuned_2 = list(dict.fromkeys(folder_tuned_2))
    path1 = '{}/{}/{}*.json'.format(data_folder_path, folder_tuned_1, ID)
    files_lt = glob.glob(path1) 
    # Add by hand the config files
    files_lt += glob.glob('{}/{}/*.json.before'.format(data_folder_path, folder_tuned_1))
    for f in folder_tuned_2:
        path2 = '{}/{}/{}*.json'.format(data_folder_path, f, ID)
        files_lt += glob.glob(path2)
        # Add by hand the config files
        files_lt += glob.glob('{}/{}/*.json.before'.format(data_folder_path, f))
    tuned_files_dt[stage] = list(dict.fromkeys(files_lt)) 
    return tuned_files_dt

def get_file(tuned_files_dt, name):
    """ Find the given type of data from the tuned files dictionary.
    Input the tuned files dictionary, with keys = stages, values =
    list of data files, and specified 'name' which is the QC test
    result type (e.g. 'NoiseDist'). Output a dictionary, with keys =
    stages, values = single data path of the specified type of
    measurement """
    # search for the file by the plot name (e.g. 'NoiseDist') and store into a dictionary
    files_dt = {}
    for stage, files_lt in tuned_files_dt.items():
        files =  []
        for file_ in files_lt:
            if name in file_:
                if not "Noise" + name in file_: 
                    files.append(file_)
        files_dt[stage] = list(dict.fromkeys(files))
    return files_dt

# function to merge files if lindiff and syn are measured separately
def merge_occ_data(data):
    for i in range(len(data[0]['Data'])):
        if type(data[0]['Data'][i]) is list:
            for j in range(0, len(data[0]['Data'][i])):
                data[0]['Data'][i][j] += data[1]['Data'][i][j]
        else:
            print("ERROR: There exist two folders of the same scan (e.g. 2x diff_thresholdscan), please check all your input data folders and move additional data. \nIf the additional files are from before the tuning into folder 'tuning', if they are from forward bias scans, then to folder 'fwb'.")
            quit()
    return data[0]

# function to merge the mask from the config files if lindiff and syn are measured separately
def merge_mask_data(data):
    for i in range(len(data[0]["RD53A"]["PixelConfig"])):
        for j in range(len(data[0]["RD53A"]["PixelConfig"][i]["Enable"])):
            data[0]["RD53A"]["PixelConfig"][i]["Enable"][j] += data[1]["RD53A"]["PixelConfig"][i]["Enable"][j]
    return data[0]["RD53A"]["PixelConfig"]

# function to merge files if lin, diff and syn are measured separately
def merge_occ_data_3(data):
    for i in range(0, len(data[0]['Data'])):
        for j in range(0, len(data[0]['Data'][i])):
            data[0]['Data'][i][j] += data[1]['Data'][i][j]+data[2]['Data'][i][j]
    return data[0]

# function to merge the mask from the config files if lin, diff and syn are measured separately
def merge_mask_data_3(data):
    for i in range(len(data[0]["RD53A"]["PixelConfig"])):
        for j in range(len(data[0]["RD53A"]["PixelConfig"][i]["Enable"])):
            data[0]["RD53A"]["PixelConfig"][i]["Enable"][j] += data[1]["RD53A"]["PixelConfig"][i]["Enable"][j] + data[2]["RD53A"]["PixelConfig"][i]["Enable"][j]
    return data[0]["RD53A"]["PixelConfig"]

def get_data(files_dt):
    """ Read the data and store into a dictionary. Input the files
    dictionary, with keys = stages, values = data path to be read.
    Output a dictionary with keys = stages, values = data in the .json
    file """
    data_dt = {}
    for stage, file_names in files_dt.items():
        data = []
        j = 0
        for file_name in file_names:
            data_FE = [] # save data for each file separately in a list
            f = open(file_name, 'r')
            data.append(json.loads(f.read())) # read the data file into data
            # For each FE only the columns which are actually turned
            # on and measured should be regarded -> set all other
            # colums to 0
            if "Dist" not in file_name:
                if "_lin_" in file_name:
                    for i in range(128):
                        data_FE.append(np.zeros(192).tolist())
                    for i in range(128,264):
                        data_FE.append(data[j]["Data"][i])
                    for i in range(264,400):
                        data_FE.append(np.zeros(192).tolist())
                    data[j]["Data"]=data_FE
                elif "_diff_" in file_name:
                    for i in range(264):
                        data_FE.append(np.zeros(192).tolist())
                    for i in range(264,400):
                        data_FE.append(data[j]["Data"][i])
                    data[j]["Data"]=data_FE
                elif "_lindiff_" in file_name:
                    for i in range(128):
                        data_FE.append(np.zeros(192).tolist())
                    for i in range(128,400):
                        data_FE.append(data[j]["Data"][i])
                    data[j]["Data"]=data_FE
                elif "_syn_" in file_name:
                    for i in range(0,128):
                        data_FE.append(data[j]["Data"][i])
                    for i in range(128,400):
                        data_FE.append(np.zeros(192).tolist())
                    data[j]["Data"]=data_FE
                j += 1
            else: 
                pass
        #If for a given stage, there are two different files, we must merge them
        if len(data) == 2:
            data_dt[stage] = merge_occ_data(data)
        elif len(data) == 1:
            data_dt[stage] = data[0]
        elif len(data) == 3:
            data_dt[stage] = merge_occ_data_3(data)
    return data_dt

def get_mask_data(files_dt, ID):
    """ Read the mask from the config files after scan and store into
    a dictionary. Input the list of dictionaries from the config file
    in the section "PixelConfig", where the enable mask is
    stored. Check the ID, as not all config files have the ID in the
    name. Output a dictionary with keys = stages, values = data in
    the .json file """
    data_dt = {}
    for stage, file_names in files_dt.items():
        data = []
        data_mask = []
        mask_dt = {}
        helper_dt = {}
        for file_name in file_names:
            f = open(file_name, 'r')
            d = json.load(f)
            if d["RD53A"]["Parameter"]["Name"] == ID:
                data.append(d)
        if len(data) == 2:
            mask_dt = merge_mask_data(data)
            for i in range(400):
                data_mask.append(mask_dt[i]["Enable"])
        elif len(data) == 3:
            mask_dt = merge_mask_data_3(data)
            for i in range(400):
                data_mask.append(mask_dt[i]["Enable"])
        elif len(data) == 1:
            if "_lindiff_" in file_name:
                for i in range(128):
                    data_mask.append(np.zeros(192).tolist())
                for i in range(128,400):
                    data_mask.append(data[0]["RD53A"]["PixelConfig"][i]["Enable"])
            else:
                for i in range(400):
                    data_mask.append(data[0]["RD53A"]["PixelConfig"][i]["Enable"])
        helper_dt["Data"] = data_mask
        helper_dt["Borders"] = []
        data_dt[stage] = helper_dt
    return data_dt

# For IV and VI plotting
def get_curve_file(data_folder_path, name):
    """ Search data path, store them into dictionary. 
    Input the big data folder, and the name of the data (e.g. 'iv'). 
    Output dictionary with keys = stages, values = single data path """
    curve_dt = {}
    for k in data_folder_path.keys():
        if len(glob.glob(data_folder_path[k] + '/*' + name + '*')) >= 1:
            curve_dt[k] = [glob.glob(data_folder_path[k] + '/*' + name + '*')[0]]
            # access element 1 or 2 in case other iv data should be used
    return curve_dt

def set_color_palette():
    """Set a color palette from a given RGB list stops, red, green and
    blue should all be lists of the same length"""
    red    = [0.00, 0.00, 0.87, 1.00, 0.510]          
    green  = [0.00, 0.81, 1.00, 0.20, 0.00]
    blue   = [0.51, 1.00, 0.12, 0.00, 0.00]
    length = [0.00, 0.34, 0.61, 0.84, 1.00]
    l = array('d',length)
    r = array('d',red)
    g = array('d',green)
    b = array('d',blue)
    npoints = len(l)
    ncontours = 999
    ROOT.TColor.CreateGradientColorTable(npoints,l,r,g,b,ncontours)
    ROOT.gStyle.SetNumberContours(ncontours)

def set_color_palette_block_3():
    """Set a color palette which has only three colors in a block for
    the discbumpscan"""
    red    = [0.00, 0.00, 0.87, 1.00, 0.510]          
    green  = [0.00, 0.81, 1.00, 0.20, 0.00]
    blue   = [0.51, 1.00, 0.12, 0.00, 0.00]
    length = [0.00, 0.34, 0.61, 0.84, 1.00]
    l = array('d',length)
    r = array('d',red)
    g = array('d',green)
    b = array('d',blue)
    npoints = len(l)
    ncontours = 3
    ROOT.TColor.CreateGradientColorTable(npoints,l,r,g,b,ncontours)
    ROOT.gStyle.SetNumberContours(ncontours)

def set_color_palette_block():
    """Set a color palette which has four colors in a block for
    the new discbumpscan analysis"""
    red    = [0.00, 0.00, 0.87, 1.00, 0.51]
    green  = [0.00, 1.01, 0.20, 0.40, 0.00]
    blue   = [0.51, 0.90, 0.22, 0.00, 0.00]
    length = [0.00, 0.34, 0.61, 0.84, 1.00]
    l = array('d',length)
    r = array('d',red)
    g = array('d',green)
    b = array('d',blue)
    npoints = len(l)
    ncontours = 4
    ROOT.TColor.CreateGradientColorTable(npoints,l,r,g,b,ncontours)
    ROOT.gStyle.SetNumberContours(ncontours)

def set_color_palette_zero():
    """Set a color palette with only three colors of which one is white"""
    red    = [1.0, 1.00, 0.05, 1.00, 1.000]          
    green  = [1.00, 0.81, 1.00, 0.20, 0.00]
    blue   = [1.00, 1.00, 0.12, 0.00, 0.00]
    length = [0.00, 0.34, 0.61, 0.84, 1.00]
    l = array('d',length)
    r = array('d',red)
    g = array('d',green)
    b = array('d',blue)
    npoints = len(l)
    ncontours = 3
    ROOT.TColor.CreateGradientColorTable(npoints,l,r,b,g,ncontours)
    ROOT.gStyle.SetNumberContours(ncontours)
