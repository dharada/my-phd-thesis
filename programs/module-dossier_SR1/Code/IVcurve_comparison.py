#!/usr/bin/env python3

import numpy as np
import pandas as pd
import argparse
import json
import ROOT
from typing import List
import shutil
import json
import os


def read_IV_csv(filename: str) -> pd.DataFrame:
    """Read the csv file containing the IV curve data into a pandas DataFrame

    Args:
        filename (str): name (can include path) of the csv file. The header of the csv file looks like:
                        time,voltage,current,currentSigma

    Returns:
        pd.DataFrame: stores the IV data in 3 columns ('voltage' in V, 'current' in A, 'currentSigma' in A). Uses as index the time of measurement.
    """
    return pd.read_csv(filename, index_col="time", parse_dates=["time"], date_parser=lambda col: pd.to_datetime(col, utc=True))


def read_IV_json(filename: str) -> pd.DataFrame:
    """Read the json file containing the IV curve data into a pandas DataFrame

    Args:
        filename (str): name (can include path) of the json file.

    Returns:
        pd.DataFrame: stores the IV data in 3 columns ('voltage' in V, 'current' in A, 'currentSigma' in A). Uses as index the time of measurement.
    """
    # open JSON file
    f = open(filename)
    # return JSON object as a dictionary
    data = json.load(f)

    # close file
    f.close()

    # create DataFrame
    df = pd.DataFrame.from_records(data['Sensor_IV'], index='Time')
    # rename columns and convert the reverse bias voltage to positive values
    df.rename(columns={"Voltage": "voltage", "Current_mean": "current", "Current_sigma": "currentSigma"}, inplace=True)
    df["current"] *= -1
    df["voltage"] *= -1

    return df


def read_IV(filename: str, voltageOffset: float = 0.0) -> pd.DataFrame:
    base_filename, file_extension = os.path.splitext(filename)
    df = pd.DataFrame()
    if file_extension == ".csv":
        df = read_IV_csv(filename=filename)
    elif file_extension == ".json":
        df = read_IV_json(filename=filename)
    else:
        print("Error: could not read file '" + filename + "'. Unknown extension '" + file_extension + "'!")
    
    df["voltage"] -= voltageOffset

    return df


def scaleToReferenceTemperature(data: pd.DataFrame, T: float, Tref: float, TrefError: float):
    """Scale the leakage current at a certain temperature to a reference temperature to compare results.
    For the formular see https://doi.org/10.23731/CYRM-2021-001 (p. 63, eq. 25).
    Track of temperatures during HV scans: https://docs.google.com/spreadsheets/d/14EmEwtx612Y6Ik55RvC1m7ajNLqZCv3i_HY6z0BFEUQ/edit#gid=1895845860

    Args:
        data (pd.DataFrame): leakage current data at temperature T (will be rescaled on output)
        T (float): temperature T (in K)
        Tref (float): reference temperature (in K)
        TrefError (float): error of reference temperature (in K)
    """
    kB = 8.617333262e-5  # Boltzmann constant in eV/K
    Eeff = 1.21  # in eV
    exponent = np.exp(-Eeff*(1/Tref - 1/T)/(2*kB))
    scaleFactor = (Tref/T)**2 * exponent
    scaleFactorError = (2*Tref + Eeff/(2*kB))/(T**2) * exponent * TrefError
    data["current"] *= scaleFactor
    data["currentSigma"] = scaleFactor*data["currentSigma"] + data["current"]*scaleFactorError


def create_IVplotWithComparison(data: pd.DataFrame, SPname: str, HVchannel: int, comparisonData: List[pd.DataFrame], comparisonLabels: List[str], outdir: str):
    c1 = ROOT.TCanvas('c1', 'A Simple Graph with error bars', 0, 0, 1600, 1200)  # standard size: 0, 0, 800, 600
    c1.SetGrid()

    ROOT.gStyle.SetEndErrorSize(10)  # default: 1

    prefix = 1e6  # to convert A to uA
    gr = ROOT.TGraphErrors(data["voltage"].size, data["voltage"].to_numpy(), prefix*data["current"].to_numpy(), ROOT.nullptr, prefix*data["currentSigma"].to_numpy())
    gr.SetTitle("HV CH" + str(HVchannel))
    gr.SetMarkerStyle(20)  # default setting of AtlasStyle
    gr.SetMarkerSize(2) # default 1
    gr.SetLineWidth(2) # default 1

    mg = ROOT.TMultiGraph()
    mg.SetTitle(SPname + "HV CH" + str(HVchannel) + ";reverse bias voltage [V];leakage current [#muA]")
    mg.Add(gr, "LP")

    fLegend1  = ROOT.TLegend(0.18,0.65,0.44,0.93)
    fLegend1.AddEntry(gr, gr.GetTitle(), "lp")

    # Create stack of histograms for comparison
    hs = ROOT.THStack("hs","")
    maxVoltage = data["voltage"].max()
    minVoltage = data["voltage"].min()
    nsteps = data["voltage"].size - 1
    delta = (maxVoltage - minVoltage)/nsteps
    auxmax = maxVoltage
    auxmin = minVoltage
    for auxdata in comparisonData:
        if auxmax < auxdata["voltage"].max():
            auxmax = auxdata["voltage"].max()
        if auxmin > auxdata["voltage"].min():
            auxmin = auxdata["voltage"].min()
    minVoltage = minVoltage - (minVoltage - auxmin) // delta * delta - delta / 2
    maxVoltage = maxVoltage + (auxmax - maxVoltage) // delta * delta + delta / 2
    nsteps =  int((maxVoltage - minVoltage) / delta)

    minPlotRange = auxmin
    maxPlotRange = auxmax
    for auxdata in comparisonData:
        if maxPlotRange > auxdata["voltage"].max():
            maxPlotRange = auxdata["voltage"].max()
        if minPlotRange < auxdata["voltage"].min():
            minPlotRange = auxdata["voltage"].min()
    minPlotRange = minPlotRange // delta * delta
    maxPlotRange = maxPlotRange // delta * delta


    for idx, auxdata in enumerate(comparisonData):
        auxdataLimited = auxdata.loc[(auxdata["voltage"] >= minPlotRange) & (auxdata["voltage"] <= maxPlotRange)]
        auxgr = ROOT.TGraphErrors(auxdataLimited["voltage"].size, auxdataLimited["voltage"].to_numpy(), prefix*auxdataLimited["current"].to_numpy(), ROOT.nullptr, prefix*auxdataLimited["currentSigma"].to_numpy())
        auxgr.SetTitle(comparisonLabels[idx])
        auxgr.SetMarkerStyle(21+idx)
        auxgr.SetMarkerSize(2)
        auxgr.SetLineWidth(2)
        #auxgr.SetLineStyle(5)
        auxgr.SetMarkerColor(12)
        auxgr.SetLineColor(12)
        auxgr.SetFillColorAlpha(idx + 1, 0.3)
        mg.Add(auxgr, "LP")
        fLegend1.AddEntry(auxgr, auxgr.GetTitle(), "lpf")

        auxh = ROOT.TH1F("h" + str(idx), comparisonLabels[idx], nsteps, minVoltage, maxVoltage)
        auxh.SetDirectory(ROOT.nullptr)  # don't add histogram to the internal registry to avoid RuntimeWarnings
        auxh.SetFillColorAlpha(idx + 1, 0.3)
        for index, row in auxdataLimited.iterrows():
            if row["voltage"] > minPlotRange and row["voltage"] < maxPlotRange:
                binNumber = auxh.FindBin(row["voltage"])
                auxh.SetBinContent(binNumber, prefix*row["current"])
            #auxh.SetBinError(binNumber, prefix*row["currentSigma"])
        #auxh.GetYaxis().SetRangeUser(minPlotRange, maxPlotRange)
        hs.Add(auxh)

    mg.Draw("A")
    hs.Draw("same")
    mg.Draw()

    c1.Update()
    #c1.BuildLegend(0.2, 0.7, 0.45, 0.91)
    fLegend1.Draw()
    # Save the canvas to separate pngs and one pdf per scan
    if not os.path.exists(outdir):
        os.makedirs(outdir)
    filename = SPname.replace("/", "_") + "_HV_CH" + str(HVchannel)
    c1.SaveAs(outdir+filename+".png")
    c1.Print(outdir+filename+".pdf", 'Title:'+SPname + " HV CH" + str(HVchannel))
    c1.Close()


def plotGraph(config: str, outdir: str):

    with open(config, 'r') as f:
        CONFIG = json.load(f)

        for SPchain in CONFIG["SPchains"]:
            for HVGroup in SPchain["HVGroups"]:
                SPchainName: str = SPchain["Name"]
                HVchannel: int = HVGroup["HVchannel"]
                commonIV = read_IV(HVGroup["commonIVcurveFile"])
                comparisonData: List[pd.DataFrame] = []
                ModuleLabels: List[str] = []
                for Module in HVGroup["Modules"]:
                    moduleData = read_IV(Module["singleIVcurveFile"], Module["groundPotentialInSPchain"])
                    scaleToReferenceTemperature(moduleData, T=Module["Temperature"], Tref=Module["refTemperatureInSPchain"], TrefError=Module["refTemperaturError"])
                    comparisonData.append(moduleData)
                    ModuleLabels.append(Module["ModuleID"] + ", " + Module["Position"])
                create_IVplotWithComparison(data=commonIV, SPname=SPchainName, HVchannel=HVchannel, comparisonData=comparisonData, comparisonLabels=ModuleLabels, outdir=outdir)


if __name__ == "__main__":
    # Set to batch mode -> do not display graphics
    ROOT.gROOT.SetBatch(True)
    import AtlasStyle
    ROOT.SetAtlasStyle()

    import argparse
    parser = argparse.ArgumentParser(description=" Create IV scan plots with the possibility to compare to single module IV curves")
    parser.add_argument("-p", "--plotconfig", type=str, default="./Configuration/M6Demonstrator/PlotConfig_IVcurves.json", help='PlotConfig file')
    parser.add_argument("-o", "--outdir", type=str, default="./Figure/IVcurves", help='Output directory')
    args = parser.parse_args()

    # Output directory needs to have a trailing slash
    if args.outdir[-1] != "/":
        args.outdir = args.outdir+"/"

    output_folder = args.outdir
    plotconfig = args.plotconfig

    plotGraph(plotconfig, output_folder)

    shutil.copy(plotconfig, output_folder + "PlotConfig_IV.json")  # copy PlotConfig.json file for backup of source data destination
