#!/usr/bin/env python3

import numpy as np
import pandas as pd
import argparse
import json
import ROOT
from typing import List
from typing import Dict
import shutil
import json
import os


def read_VI_csv(filename: str, maxNumberOfWorkingFEs: int = 0) -> pd.DataFrame:
    """Read the csv file containing the VI curve data into a pandas DataFrame

    Args:
        filename (str): name (can include path) of the csv file. The header of the csv file looks like:
                        current[A],voltage[V],M1,...,decoding1,...
                        where current and voltage are values taken from the power supply.
        maxNumberOfWorkingFEs (int): to calculate ratios

    Returns:
        pd.DataFrame: stores the VI data with renamed columns ('current' in A, 'voltage' in V).
    """
    df =  pd.read_csv(filename)
    df.rename(columns={"current[A]": "current", "voltage[V]": "voltage"}, inplace=True)

    decodings: List[str] = []
    for colname in df.columns.values.tolist():
        if colname.startswith("decoding"):
            decodings.append(colname)
            df[colname] = df[colname].map(lambda a: a.count('1')) # convert the hex e.g. '0x000000030010110' (stored as string) to number of working FEs
    
    df["workingFEs"] = 0
    for decoding in decodings:
        df["workingFEs"] += df[decoding]
    df.drop(columns=decodings, inplace=True)

    df["workingFEsRatio"] = 0.0
    maxNumberOfWorkingFEs = max(maxNumberOfWorkingFEs, df["workingFEs"].max())
    if maxNumberOfWorkingFEs != 0:
        df["workingFEsRatio"] = 100.0*df["workingFEs"]/maxNumberOfWorkingFEs

    return df


def create_VIplotWithComparison(data: pd.DataFrame, SPname: str, Temperature: int, ModuleMetaData: Dict, outdir: str):
    c1 = ROOT.TCanvas('c1', 'VI curves', 0, 0, 1800, 1200)  # standard size: 0, 0, 800, 600
    c1.SetGrid()
    ROOT.gStyle.SetPadRightMargin(0.15)

    ROOT.gStyle.SetEndErrorSize(10)  # default: 1

    prefix = 1.0
    gr = ROOT.TGraphErrors(data["voltage"].size, data["current"].to_numpy(), prefix*data["voltage"].to_numpy(), ROOT.nullptr, ROOT.nullptr)
    gr.SetTitle("LV PSU")
    gr.SetMarkerStyle(20)  # default setting of AtlasStyle
    gr.SetMarkerSize(2) # default 1
    gr.SetLineWidth(2) # default 1

    fLegend1  = ROOT.TLegend(0.18,0.55,0.44,0.93)
    # fLegend1.SetTextFont(22)
    # fLegend1.SetBorderSize(0)
    # fLegend1.SetFillColor(ROOT.kWhite)

    mg = ROOT.TMultiGraph()
    mg.SetTitle(SPname + " VI curves at " + str(Temperature) + "°C" + ";current [A];voltage [V]")
    mg.Add(gr, "LP")
    fLegend1.AddEntry(gr,gr.GetTitle(),"lp")

    # add modules
    for idx, (position, details) in enumerate(ModuleMetaData.items()):
        auxgr = ROOT.TGraphErrors(data["voltage"].size, data["current"].to_numpy(), prefix*data[position].to_numpy(), ROOT.nullptr, ROOT.nullptr)
        auxgr.SetTitle(details["ModuleID"] + ", " + position)
        auxgr.SetMarkerStyle(21+idx)
        auxgr.SetMarkerSize(2)
        auxgr.SetLineWidth(2)
        auxgr.SetMarkerColor(16)
        auxgr.SetLineColor(16)
        mg.Add(auxgr, "LP")
        fLegend1.AddEntry(auxgr,auxgr.GetTitle(),"lp")

    padding = 0.25
    pad1 = ROOT.TPad("pad1","",0,0,1,1)
    pad1.SetFillStyle(4000)
    #pad1.SetFillColor(ROOT.kWhite)
    pad1.SetFrameBorderMode(0)
    pad1.SetFrameFillStyle(0)
    pad1.SetTicky(0)
    pad1.Draw()
    pad1.cd()

    mg.Draw("A")
    mg.SetMinimum(0.)
    mg.SetMaximum(data["voltage"].max()*1.4)
    mg.GetXaxis().SetLimits(data["current"].min() - padding, data["current"].max() + padding)

    # draw working FEs
    c1.cd()
    pad2 = ROOT.TPad("pad2","",0,0,1,1)
    pad2.SetFillStyle(4000)
    #pad2.SetFillColor(ROOT.kWhite)
    pad2.SetFrameBorderMode(0)
    pad2.SetFrameFillStyle(0)
    pad2.SetTicky(0)
    pad2.Draw()
    pad2.cd()
    grFE = ROOT.TGraph(data["current"].size, data["current"].to_numpy(), data["workingFEsRatio"].to_numpy())
    grFE.SetMarkerStyle(47)  # default setting of AtlasStyle is 20
    grFE.SetMarkerSize(2) # default 1
    grFE.SetLineWidth(2) # default 1
    grFE.SetLineColor(2) #red
    grFE.SetMarkerColor(2) #red
    grFE.GetHistogram().Draw("Y+")
    grFE.Draw("LP")
    grFE.SetMinimum(0.)
    grFE.GetXaxis().SetLimits(data["current"].min() - padding, data["current"].max() + padding)
    grFE.GetYaxis().SetTitle("working FEs [%]")
    grFE.GetYaxis().SetAxisColor(2)
    grFE.GetYaxis().SetLabelColor(2)

    c1.cd()
    c1.Update()
    fLegend1.Draw()
    #c1.BuildLegend(0.2, 0.7, 0.41, 0.93)
    # Save the canvas to separate pngs and one pdf per scan
    if not os.path.exists(outdir):
        os.makedirs(outdir)
    filename = SPname.replace("/", "_") + "_VI_" + str(Temperature)
    c1.SaveAs(outdir+filename+".png")
    c1.Print(outdir+filename+".pdf", 'Title:'+SPname + " VI " + str(Temperature) + "°C")
    c1.Close()


def plotGraph(config: str, outdir: str):

    with open(config, 'r') as f:
        CONFIG = json.load(f)

        for SPchain in CONFIG["SPchains"]:
            for VIcurve in SPchain["VIcurves"]:
                SPchainName: str = SPchain["Name"]
                maxNumberOfWorkingFEs = 0
                if "maxNumberOfWorkingFEs" in SPchain:
                    maxNumberOfWorkingFEs = SPchain["maxNumberOfWorkingFEs"]
                ModuleMetaData = SPchain["ModuleMetaData"]
                Temperature = VIcurve["Temperature"]
                VIcurves = read_VI_csv(VIcurve["DataFile"], maxNumberOfWorkingFEs)
                create_VIplotWithComparison(data=VIcurves, SPname=SPchainName, Temperature=Temperature, ModuleMetaData=ModuleMetaData, outdir=outdir)


if __name__ == "__main__":
    # Set to batch mode -> do not display graphics
    ROOT.gROOT.SetBatch(True)
    import AtlasStyle
    ROOT.SetAtlasStyle()

    import argparse
    parser = argparse.ArgumentParser(description=" Create IV scan plots with the possibility to compare to single module IV curves")
    parser.add_argument("-p", "--plotconfig", type=str, default="./Configuration/M6Demonstrator/PlotConfig_IVcurves.json", help='PlotConfig file')
    parser.add_argument("-o", "--outdir", type=str, default="./Figure/IVcurves", help='Output directory')
    args = parser.parse_args()

    # Output directory needs to have a trailing slash
    if args.outdir[-1] != "/":
        args.outdir = args.outdir+"/"

    output_folder = args.outdir
    plotconfig = args.plotconfig

    plotGraph(plotconfig, output_folder)

    if not os.path.exists(output_folder):
        os.makedirs(output_folder)
    shutil.copy(plotconfig, output_folder + "PlotConfig_VI.json")  # copy PlotConfig.json file for backup of source data destination
