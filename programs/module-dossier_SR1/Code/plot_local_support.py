print("\nLoading packages\n")
import json
import os
import argparse
import ROOT
import math
import glob
import numpy as np
import copy
from array import array
from helper_functions import *


"""
"""
def plot_mod_pad(data_dt_, rotation, m):
    data_dt = copy.deepcopy(data_dt_)   # copy the data, need to chaneg the data later when selecting occupancy range.

    # mirror the data array to plot it in the same layout as in the real module
    def mirror_arr(arr):
        return arr[:, ::-1]

    # rotate the data array clockwise
    def rotate_arr(arr, times):
        if times == 0:
            return arr
        else:
            return rotate_arr(arr[::-1].T, times - 1)

    # rotate the whole data dictionary, including the data array, and the x, y axes, and the bins, border locations, ...
    def rotate_data(data, times):
        data['Data'] = mirror_arr(np.array(data['Data']))
        data['Data'] = rotate_arr(np.array(data['Data']), times)
        for i_t in range(times):
            data['x']['Bins'], data['y']['Bins'] = data['y']['Bins'], data['x']['Bins']
            data['x']['Low'], data['y']['Low'] = data['y']['Low'], data['x']['Low']
            data['x']['High'], data['y']['High'] = data['y']['High'], data['x']['High']
            for b in data['Borders']:
                if i_t % 2 == 0:
                    b[0], b[1], b[2], b[3] = (data['x']['High'] - data['x']['Low']) - b[1], \
                                             (data['y']['High'] - data['y']['Low']) - b[0], \
                                             (data['x']['High'] - data['x']['Low']) - b[3], \
                                             (data['y']['High'] - data['y']['Low']) - b[2]
                else:
                    b[0], b[1], b[2], b[3] = b[1], b[0], b[3], b[2]
        return data

    # book canvas, and styling
    ROOT.gStyle.SetTitleFontSize(0.05)
    hs, lines, lines2, pals_dt, j = [], [], [], {}, 0     # list of histograms, list of lines (borders of the FE types), index for the plot (pad)

    for k, data in data_dt.items():
        if data is not None:
            # original border locations
            data['Borders'] = [[0, 0, 0, 384], [127, 0, 127, 384], [263, 0, 263, 384], [400, 0, 400, 384]]
            # rotate the data according to the FE
            rotation_times = rotation[k]
            data = rotate_data(data, times=rotation_times)
            hs.append(ROOT.TH2D('h2_'+k+"_"+m+"_"+ stage, '',
                                data['x']['Bins'], data['x']['Low'], data['x']['High'],
                                data['y']['Bins']*2, data['y']['Low'], data['y']['High']*2))
            hs[j].GetXaxis().SetLabelSize(0)
            hs[j].GetYaxis().SetLabelSize(0)
            # fill histogram
            xbins, ybins = data['x']['Bins'], data['y']['Bins']
            if k == 'FE1' or k == 'FE2':
                # module 1, 3 and 5
                if int(m[-1]) % 2 == 1:
                    for i1 in range(xbins):
                        for i2 in range(ybins):
                            h = data['Data'][i1][i2]
                            hs[j].SetBinContent(i1, i2, h)
                    for i1 in range(xbins):
                        for i2 in range(ybins, ybins+ybins):
                            hs[j].SetBinContent(i1, i2, 0)
                # modules 2, 4, 6
                else:
                    for i1 in range(xbins):
                        for i2 in range(ybins):
                            h = data['Data'][i1][i2]
                            hs[j].SetBinContent(i1, i2+ybins, h)
                    for i1 in range(xbins):
                        for i2 in range(ybins):
                            hs[j].SetBinContent(i1, i2, 0)
            else:
                # modules 2, 4, 6
                if int(m[-1]) % 2 == 0:
                    for i1 in range(xbins):
                        for i2 in range(ybins):
                            h = data['Data'][i1][i2]
                            hs[j].SetBinContent(i1, i2, h)
                    for i1 in range(xbins):
                        for i2 in range(ybins, ybins+ybins):
                            hs[j].SetBinContent(i1, i2, 0)
                # modules 1, 3, 5
                else:
                    for i1 in range(xbins):
                        for i2 in range(ybins):
                            h = data['Data'][i1][i2]
                            hs[j].SetBinContent(i1, i2+ybins, h)
                    for i1 in range(xbins):
                        for i2 in range(ybins):
                            hs[j].SetBinContent(i1, i2, 0)
            lines_b = []
            # draw borders of different types of FE
            for b in data['Borders']:
                lines_b.append(ROOT.TLine(b[0], b[1], b[2], b[3]))
            for i in range(len(data['Borders'])):
                lines_b[i].SetLineColor(ROOT.kBlack)
                lines_b[i].SetLineWidth(2)
            lines.append(lines_b)
        else:
            xbins, xbins_low, xbins_high, ybins, ybins_low, ybins_high = 400, 0, 400, 192, 0, 192
            hs.append(ROOT.TH2D('h2_'+k+'_'+m+'_'+stage, '',
                        xbins, xbins_low, xbins_high,
                        ybins*2, ybins_low, ybins_high*2))
            for i1 in range(xbins):
                for i2 in range(ybins*2):
                    hs[j].SetBinContent(i1, i2, 0)
            # Define default boarders and lines for empty quadrants to make sure the indexing works, but set line width to 0
            lines_b = []
            for b in [[0.0, 0.0, 384.0, 0.0], [0.0, 127.0, 384.0, 127.0], [0.0, 263.0, 384.0, 263.0], [0.0, 400.0, 384.0, 400.0]]:
                lines_b.append(ROOT.TLine(b[0], b[1], b[2], b[3]))
            for i in range(4):
                lines_b[i].SetLineColor(ROOT.kBlack)
                lines_b[i].SetLineWidth(0)
            lines.append(lines_b)
            # Try tp wrote Not Available on missing data FEs, but not workin TODO
            pals_dt[k] = []
            line_tot2 = 'Not Available'
            pals_dt[k].append(ROOT.TPaveText(0.25*384, 0.40*400, 0.75*384, 0.6*400))
            pals_dt[k][0].AddText(line_tot2)
            pals_dt[k][0].SetBorderSize(1)
            pals_dt[k][0].Draw()
        hs[j].SetStats(0)
        j += 1
    return hs, lines

def mirror_arr_y(arr):
    return arr[::-1, :]

def mirror_data_y(data):
    data['Data'] = mirror_arr_y(np.array(data['Data']))
    for b in data['Borders']:
        b[0], b[2] = (data['x']['High'] - data['x']['Low']) - b[0], (data['x']['High'] - data['x']['Low']) - b[2]
    return data

def rotate_mod(data, rotations, times):
    for i_t in range(1, times+1):
        order = list(data.keys())
        # rotate the whole module, update the 4 FE position. and re-define the modeule data dictionary
        # order[1], order[2], order[3], order[0] = order[0], order[1], order[2], order[3]
        order[0], order[1], order[2], order[3] = order[2], order[0], order[3], order[1]
        data = {o: data[o] for o in order}
        rotations = {k: (rotations[k] + 1) % 4 for k, v in rotations.items()}
    return data, rotations


def merge_modules(mod_keys, types, scan, stage):
    """
    put series of modules in the right orientation and location. and re-scaling according to the physical dimension
    """
    # histograms, border lines, module ID's, pad locations, plot dictionary, number of modules in total
    hs, ls, mod_ids, pads_locations, plot_dt, num_mod = [], [], [], [], {}, len(mod_keys)
    if layout == 'Ring':
        num_rows = 1
    elif layout == 'Longeron':
        num_rows = 2
    # number of modules per row
    num_per_row = num_mod/num_rows
    # width FE, since one module has 2x2 FE, so multiply 1/2
    FE_width = 1/2 * 1/num_per_row
    # re-scale according to the physical dimension
    FE_height = 38.6/40.2 * FE_width

    # if half ring, the module is rotated, so swap width and height
    if layout == 'Ring':
        FE_width, FE_height = FE_height, FE_width

    # initial pad location of FE
    pads_fraction_ = np.array([[0, 1 - FE_height, FE_width, 1],
                              [FE_width, 1 - FE_height, FE_width * 2, 1],
                              [0, 1 - FE_height * 2, FE_width, 1 - FE_height],
                              [FE_width, 1 - FE_height * 2, FE_width * 2, 1 - FE_height]])
    pads_fraction = pads_fraction_
    for idx_m, m in enumerate(mod_keys):
        CONFIG_MOD = CONFIG_GEN[m]
        mod_ID = CONFIG_MOD['ModuleID']

        # store different FE data into one dictionary (Module data)
        ocu_dt = {}     # storing occupancy maps for different stages
        for FE_key in ['FE1', 'FE4', 'FE2', 'FE3']:
            ID = CONFIG_MOD[FE_key]
            data_folder_path = CONFIG_MOD['FolderPath'][stage]
            tuned_scan_dt = get_tuned_folder(data_folder_path, stage, ID, types, scan)
            plot_dt[stage] = get_file(tuned_scan_dt, CONFIG_GEN['Plot'][scan]['ScanName'])[stage]
            try:
                ocu_dt[FE_key] = get_data(plot_dt)[stage]
                ocu_dt[FE_key]['Borders'] = [[0, 0, 0, 384], [127, 0, 127, 384], [263, 0, 263, 384], [400, 0, 400, 384]]
            except:
                ocu_dt[FE_key] = None

        # orient the modules according to the type (Longeron or Ring)
        if layout == 'Longeron':
            # initial rotation times of the FE
            rotations = {'FE1': 1, 'FE2': 1, 'FE3': 3, 'FE4': 3}
            # rotate the whole module clockwise 90 degrees (wire-bonding || to beam axis)
            mod_rotations = 1
            # update the dictionary and the rotation after rotating the whole module
            ocu_dt, rotations = rotate_mod(ocu_dt, rotations, mod_rotations)
            # check if it is the bottom row and rotate 2 more times so
            # that the FE layout in the module is 1 in the bottom
            # left, 2 in the bottom right, 3 top right and 4 top left
            if idx_m % 2  == 0:
                ocu_dt, rotations = rotate_mod(ocu_dt, rotations, 2)
            # update location of the module on the demonstrator
            if idx_m != 0:
                if idx_m % 2 == 0:  # if in the first (bottom) row, then shift to the right for module 3 and 5
                    pads_fraction = pads_fraction_ + (idx_m / 2) * np.array([[FE_width * 2, 0, FE_width * 2, 0]])
                elif idx_m % 2 == 1:  # if in the second (top) row, then shift up and to the right for module 2 and 4
                    pads_fraction = pads_fraction_ + np.array([[0, FE_height * 2, 0, FE_height * 2]])
                    if idx_m != 1: # only shift to the right for module 2 and 4
                        pads_fraction += (int(idx_m / 2)) * np.array([[FE_width * 2, 0, FE_width * 2, 0]])
            else:  # if first module in the bottom row, keep the original pads_fraction
                pads_fraction = pads_fraction_
        elif layout == 'Ring':
            mod_rotations = 0
            rotations = {'FE1': 1, 'FE2': 1, 'FE3': 3, 'FE4': 3}
            if idx_m % 2 == 1:  # check if the module is even or odd
                ocu_dt = {k: mirror_data_y(v) for k, v in ocu_dt.items()}
            # update location of the module: shift one module width to the right
            if idx_m != 0:
                pads_fraction = pads_fraction + np.array([[FE_width*2, 0, FE_width*2, 0]])
        else:
            print('not defined')
            break

        if idx_m == 0:
            first_mod_fe_locations = ['syn', 'lin', 'diff']
            if mod_rotations == 1 or mod_rotations == 3:
                first_mod_fe_locations = first_mod_fe_locations[::-1]

        # store the histograms and border lines
        hs_i, ls_i = plot_mod_pad(ocu_dt, rotations, m)

        # add to the main list of all plot for the whole structure
        hs.append(hs_i)
        ls.append(ls_i)
        mod_ids.append(mod_ID)

        pads_locations_mod = []
        # define the pad location
        for i_h, h in enumerate(hs_i):
            pads_locations_mod.append([pads_fraction[i_h][0], pads_fraction[i_h][1], pads_fraction[i_h][2], pads_fraction[i_h][3]])
        pads_locations.append(pads_locations_mod)
    return hs, ls, pads_locations, mod_ids, first_mod_fe_locations


def plot_together(hs, ls, pads_locations, mod_ids, first_mod_fe_locations, stage, ring_max_mod=4, longeron_max_mod=6):
    """
    Plot the whole structure (series of modules on a support)
    and store in png / pdf.
    First plot for the whole structure, but may not see the details.
    So break the whole support into smaller parts for the details -> might not be functioning yet.. not tested TODO
    """
    # ==============================================================================================
    # ================================= Plot the overview ==========================================
    # ==============================================================================================
    c2 = ROOT.TCanvas('', '', 2100, 1500)
    c2.Divide(1, 1)
    c2.cd(1)
    pads, jj, detail = [], 0, False

    if scan == "sourcescan":
        axis_range = 2000.
    elif scan == "analogscan":
        axis_range = 100.

    for j in range(len(hs)):    # loop over modules
        # set user defined color palette
        set_color_palette()
        if j == 0:
            pals_dt = []

        for i_pad, pad in enumerate(pads_locations[j]):     # loop over FE in a module
            pads.append(ROOT.TPad("pad", "pad", pad[0], pad[1], pad[2], pad[3]))
            c2.cd(1)
            # set the margin=0, according to the location
            if pad[0] != 0:  # check if in the most left
                pads[jj].SetTopMargin(0); pads[jj].SetBottomMargin(0); pads[jj].SetLeftMargin(0)
            if pad[2] != 1:  # check if in the most right
                pads[jj].SetTopMargin(0); pads[jj].SetBottomMargin(0); pads[jj].SetRightMargin(0)
            pads[jj].Draw()
            pads[jj].cd()
            # draw the color z axis on the right and top pad
            if j == 5 and i_pad == 1:
                hs[j][i_pad].Draw('colz')
            else:
                hs[j][i_pad].Draw('col')
            # set the zrange according to the scan
            hs[j][i_pad].GetZaxis().SetRangeUser(0., axis_range)
            # draw the FE border lines
            for i in range(4):
                ls[j][i_pad][i].Draw("same")
            # draw the labelling syn, lin and diff on the FEs of the module in the top left
            if j == 1 and i_pad == 0:
                pads[jj].cd()
                if layout == 'Ring':
                    loc = [65, 280, 125, 370]
                    loc_offset = [0, -125, 0, -125]
                elif layout == 'Longeron':
                    loc = [30, 75, 120, 125]
                    loc_offset = [130, 0, 130, 0]
                for i_pal, fe in enumerate(first_mod_fe_locations):
                    pals_dt.append(ROOT.TPaveText(loc[0], loc[1], loc[2], loc[3]))
                    pals_dt[i_pal].AddText(fe)
                    pals_dt[i_pal].Draw('same')
                    for i in range(len(loc)):
                        loc[i] = loc[i] + loc_offset[i]
            c2.Update()
            jj += 1
        # writing the module names
        c2.cd()
        tex = mod_ids[j]
        tex_modid = ROOT.TLatex()
        tex_modid.SetNDC()
        if layout == 'Ring':
            tex_modid.SetTextAlign(21)
            tex_modid.SetTextSize(0.1/len(mod_ids))
            tex_modid.DrawText(pads_locations[j][0][2], pads_locations[j][0][3], tex)
        elif layout == 'Longeron':
            tex_modid.SetTextSize(0.1/len(mod_ids)*2)
            if j % 2 == 1:
                tex_modid.SetTextAlign(21)
                tex_modid.DrawText(pads_locations[j][0][2], pads_locations[j][0][3], tex)
            else:
                tex_modid.SetTextAlign(23)
                tex_modid.DrawText(pads_locations[j][-2][2], pads_locations[j][-2][1], tex)
        c2.Update()

    figure_name = output_folder+"{}_{}_{}_{}".format(layout, len(hs), scan, "overview")
    c2.Print(figure_name + '_' + stage + '.png', 'png')
    c2.Print(figure_name + '.pdf(', 'pdf')


    # ==============================================================================================
    # ================================= Break Ring into smaller parts ==============================
    # ==============================================================================================
    if layout == 'Ring' and len(hs) > ring_max_mod:    # if Ring, and too long (> 4 modules), then break into smaller parts
        detail, pads, jj, FE_width = True, [], 0, pads_locations[0][0][2] - pads_locations[0][0][0]
        # FE_height = pads_locations[0][0][3] - pads_locations[0][0][1]
        mod_num = len(hs)   # number of modules in total (originally, before breaking)

        for j_d in range(len(hs)):  # loop over modules
            if j_d % ring_max_mod == 0:     # book new canvas every 4 modules
                c = ROOT.TCanvas('', '', 2100, 1500)
                c.Divide(1, 1)
                c.cd(1)

            for i_pad, pad in enumerate(pads_locations[j_d]):   # loop over FE in a module
                # modify the size and location of the modules, so that it fits the new canvas
                pad[0] = (pad[0] - FE_width * 2 * ring_max_mod * (j_d//4)) * mod_num / ring_max_mod
                pad[2] = (pad[2] - FE_width * 2 * ring_max_mod * (j_d//4)) * mod_num / ring_max_mod
                pad[1] = (pad[1] - 0.5) * mod_num / ring_max_mod + 0.5
                pad[3] = (pad[3] - 0.5) * mod_num / ring_max_mod + 0.5

                for i_p, p in enumerate(pad):   # rounding problem, sometime break (e.g. -1e-16, cannot be negative)
                    if round(p, 5) > -1e-5:
                        pad[i_p] = abs(round(p, 5))

                pads.append(ROOT.TPad("pad", "pad", pad[0], pad[1], pad[2], pad[3]))
                c.cd(1)
                # set the margin=0, according to the location
                if pad[0] != 0:  # check if in the most left
                    pads[jj].SetTopMargin(0); pads[jj].SetBottomMargin(0); pads[jj].SetLeftMargin(0)
                if pad[2] != 1:  # check if in the most right
                    pads[jj].SetTopMargin(0); pads[jj].SetBottomMargin(0); pads[jj].SetRightMargin(0)
                pads[jj].Draw()
                pads[jj].cd()
                hs[j_d][i_pad].Draw('col')  # draw FE
                for i in range(4):  # draw border lines
                    ls[j_d][i_pad][i].Draw("same")
                if j_d == 0 and i_pad == 0:
                    pads[jj].cd()
                    for i_pal, fe in enumerate(first_mod_fe_locations):
                        pals_dt[i_pal].Draw('same')
                c.Update()
                jj += 1
            c.cd()
            tex = mod_ids[j_d]
            tex_modid = ROOT.TLatex()
            tex_modid.SetNDC()
            tex_modid.SetTextSize(0.1 / ring_max_mod)
            tex_modid.SetTextAlign(21)
            tex_modid.DrawText(pads_locations[j_d][0][2], pads_locations[j_d][0][3], tex)

            if j_d % 4 == 3:    # save the plot, if current module is the 4th one
                figure_name_detail = output_folder+"{}_{}_{}_detail_{}".format(layout, len(hs), scan, (j_d//4))
                c.Print(figure_name_detail + '.png', 'png')
                c.Print(figure_name + '.pdf)', 'pdf')

        if j_d % 4 != 3:    # save the plot, if the current module is the last one (but not the 4th)
            figure_name_detail = output_folder+"{}_{}_{}_detail_{}".format(layout, len(hs), scan, (j_d//4))
            c.Print(figure_name_detail + '_' + stage + '.png', 'png')
            c.Print(figure_name + '.pdf)', 'pdf')

    # ==============================================================================================
    # ============================= Break Longeron into smaller parts ==============================
    # ==============================================================================================
    if layout == 'Longeron' and len(hs) > longeron_max_mod:
        detail, pads, jj, FE_width = True, [], 0, pads_locations[0][0][2] - pads_locations[0][0][0]
        # FE_height = pads_locations[0][0][3] - pads_locations[0][0][1]

        mod_num, mod_per_row = len(hs), longeron_max_mod/2   # number of modules in total,  number of modules in a row (after breaking)
        num_part = math.ceil(mod_num/longeron_max_mod)  # break into how many parts

        for part in range(num_part):    # loop over the parts
            c3 = ROOT.TCanvas('c3', 'c3', 2100, 1500)
            c3.Divide(1, 1)
            c3.cd(1)
            for j_d in range(len(hs)):  # loop over the modules
                # check if the current module belongs to the current breaking part
                selected_hs = False
                if j_d >= math.ceil(mod_num/2):     # check rows except 1st row
                    if (j_d - math.ceil(mod_num/2)) // mod_per_row == part:
                        selected_hs = True
                        in_row = 2
                elif j_d // mod_per_row == part:    # check 1st row
                    selected_hs = True
                    in_row = 1

                if selected_hs:     # if it's the correct module then plot
                    for i_pad, pad in enumerate(pads_locations[j_d]):
                        # modify the location and size
                        pad[0] = (pad[0] - FE_width * 2 * mod_per_row * part) * math.ceil(mod_num/2) / (longeron_max_mod/2)
                        pad[2] = (pad[2] - FE_width * 2 * mod_per_row * part) * math.ceil(mod_num/2) / (longeron_max_mod/2)
                        # pad[1] = 1 - (1 - pad[1]) * math.ceil(mod_num/2) / 3
                        # pad[3] = 1 - (1 - pad[3]) * math.ceil(mod_num/2) / 3
                        pad[1] = (pad[1] - 0.5) * math.ceil(mod_num/2) / (longeron_max_mod/2) + 0.5
                        pad[3] = (pad[3] - 0.5) * math.ceil(mod_num/2) / (longeron_max_mod/2) + 0.5

                        # rounding problem
                        for i_p, p in enumerate(pad):
                            if round(p, 5) > -1e-5:
                                pad[i_p] = abs(round(p, 5))

                        pads.append(ROOT.TPad("pad", "pad", pad[0], pad[1], pad[2], pad[3]))
                        c3.cd(1)
                        # set the margin=0, according to the location
                        if pad[0] != 0:  # check if in the most left
                            pads[jj].SetTopMargin(0); pads[jj].SetBottomMargin(0); pads[jj].SetLeftMargin(0)
                        if pad[2] != 1:  # check if in the most right
                            pads[jj].SetTopMargin(0); pads[jj].SetBottomMargin(0); pads[jj].SetRightMargin(0)

                        pads[jj].Draw()
                        pads[jj].cd()
                        hs[j_d][i_pad].Draw('col')
                        for i in range(4):
                            ls[j_d][i_pad][i].Draw("same")

                        if j_d == 0 and i_pad == 0:
                            pads[jj].cd()
                            for i_pal, fe in enumerate(first_mod_fe_locations):
                                pals_dt[i_pal].Draw('same')
                        c3.Update()
                        jj += 1
                    c3.cd()
                    tex = mod_ids[j_d]
                    tex_modid = ROOT.TLatex()
                    tex_modid.SetNDC()
                    tex_modid.SetTextSize(0.1 / longeron_max_mod * 2)
                    if in_row == 1:
                        tex_modid.SetTextAlign(21)
                        tex_modid.DrawText(pads_locations[j_d][0][2], pads_locations[j_d][0][3], tex)
                    else:
                        tex_modid.SetTextAlign(23)
                        tex_modid.DrawText(pads_locations[j_d][-2][2], pads_locations[j_d][-2][1], tex)

            figure_name_detail = output_folder+"{}_{}_{}_detail_{}".format(layout, len(hs), scan, part)
            c3.Print(figure_name_detail + '_' + stage + '.png', 'png')
            c3.Print(figure_name + '.pdf)', 'pdf')
    if detail:
        pass
    else:
        if stage != "Stage1":
            pass
        else:
            c1 = ROOT.TCanvas('c1', 'c1')
            c1.Print(figure_name + '.pdf)', 'pdf')

# ========================= Load Data Files =====================================

# Set to batch mode -> do not display graphics
ROOT.gROOT.SetBatch(True)

parser = argparse.ArgumentParser(description = "Demonstrator view plots")
parser = argparse.ArgumentParser(description = "Plot an overview of a scan on the different demonstrator modules.")
parser.add_argument("-p", "--plotconfig", type=str, default="./Configuration/LocalSupportConfig.json", help='LocalSupportPlotConfig file')
parser.add_argument("-o", "--outdir", type=str, default="./Figure/DemonstratorView", help='Output directory')
args = parser.parse_args()

# Output directory needs to have a trailing slash
if args.outdir[-1] != "/":
    args.outdir = args.outdir+"/"

output_folder = args.outdir
plotconfig = args.plotconfig

# creat output folder if it does not yet exist
if not os.path.exists(output_folder):
    os.makedirs(output_folder)

# load LocalSupportConfig file which defines the module data being used for the analysis
genconf = open(args.plotconfig, 'r')
CONFIG_GEN = json.loads(genconf.read())

layout = CONFIG_GEN['Layout']
number_of_mods = CONFIG_GEN['NumberOfMods']
output_format = CONFIG_GEN['OutputFormat']
mod_keys = [i for i in CONFIG_GEN.keys() if "Module" in i]




stages = CONFIG_GEN["Stages"]
#stages = ['Stage1']
for stage in stages:
    for scan in CONFIG_GEN['Plot'].keys():
        types = CONFIG_GEN['Plot'][scan]['Type']
        hs, ls, pads_locations, mod_ids, first_mod_fe_locations = merge_modules(mod_keys[:number_of_mods], types, scan, stage)
        pads_locations = np.array(pads_locations)
        min_ = pads_locations[:, :, 1].min()
        max_ = pads_locations[:, :, 3].max()
        mid_point = min_ + (max_ - min_)/2

        pads_locations[:, :, 1], pads_locations[:, :, 3] = pads_locations[:, :, 1] - (mid_point - 0.5),  pads_locations[:, :, 3] - (mid_point - 0.5)
        plot_together(hs, ls, pads_locations, mod_ids, first_mod_fe_locations, stage)

print("Done")
