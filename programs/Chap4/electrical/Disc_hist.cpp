#include <iostream>
#include <fstream>
#include <nlohmann/json.hpp>
#include <tuple>
#include <math.h>

#include <TROOT.h>
#include <TGraph.h>
#include <TGraphErrors.h>
#include <TH1D.h>
#include <THStack.h>
#include <TStyle.h>
#include <TLegend.h>
#include <TCanvas.h>
#include <TGaxis.h>
#include <TAxis.h>

using namespace std;
using json = nlohmann::json;

class moduleData{
public:
  int digitalscan[400][192];
  int analogscan[400][192];
  int digitalCount[3];
  int analogCount[3];
};


class byType{
public:
  bool isDigital=false;
  string pigtailName;
  vector <string> moduleList;
  TH1D *hist[5];

  //  int scanMap[9][400][192];
  //  int counting[9][3];
  /*
  int digitalscan[9][400][192];
  int analogscan[9][400][192];
  int digitalCount[9][3]; // 0 is good, 1 is bad, 2 is dead.
  int analogCount[9][3];
  */
  void countPixel(){
    cout<<"Start counting"<<endl;
    for(int ii=0;ii<moduleList.size();ii++){
      int counting[3]={0,0,0};
      /*
      for(int jj=0;jj<3;jj++){
	counting[ii][jj]=0;
      }
      */
	
      string configDir="config/GenConfig_"+moduleList[ii]+".json";
      ifstream iCon(configDir.c_str());
      json jCon;
      iCon >> jCon;

      for(int fe=0;fe<2;fe++){
	string FE;
	//	if(fe==0){FE=jCon["Module"]["FE1"];}
	if(fe==0){FE=jCon["Module"]["FE2"];}
	//	if(fe==2){FE=jCon["Module"]["FE3"];}
	if(fe==1){FE=jCon["Module"]["FE4"];}
      string file;
      for(int sld=0;sld<3;sld++){
	int colStart=0;
	int colEnd=0;
	if(sld==0){file="../../../../ThesisData/Chap4/electrical/data/"+moduleList[ii]+"/Pigtail/syn_discbumpscan/"+FE+"_OccupancyMap.json"; colStart=0;colEnd=128;}
	else if(sld==1){file="../../../../ThesisData/Chap4/electrical/data/"+moduleList[ii]+"/Pigtail/lin_discbumpscan/"+FE+"_OccupancyMap.json"; colStart=128;colEnd=264;}
	else if(sld==2){file="../../../../ThesisData/Chap4/electrical/data/"+moduleList[ii]+"/Pigtail/diff_discbumpscan/"+FE+"_OccupancyMap.json"; colStart=265;colEnd=400;}
      //      if(isDigital){file="../../../../ThesisData/Chap3/electrical/"+moduleList[ii]+"/std_digitalscan/"+FE+"_OccupancyMap.json"; colStart=0;}
      //      else{file="../../../../ThesisData/Chap3/electrical/"+moduleList[ii]+"/lindiff_analogscan/"+FE+"_OccupancyMap.json"; colStart=128;}
	cout<<file<<endl;
	ifstream iFile(file.c_str());
	if(iFile.is_open()){
	  json jFile;
	  iFile >> jFile;
	  for(int col=colStart; col<colEnd;col++){
	    for(int row=0;row<192;row++){
	      double dfe=fe;
	      if(jFile["Data"][col][row]==100){hist[0]->Fill(ii+0.4+0.33*fe,1);counting[0]++;}
	      else if(jFile["Data"][col][row]==0){hist[2]->Fill(ii+0.4+0.33*fe,1);counting[2]++;}
	      else{hist[1]->Fill(ii+0.4+0.33*fe,1);counting[1]++;}
	    }
	  }
	  cout<<moduleList[ii]<<"Bad: "<<counting[1]<<", Dead"<<counting[2]<<endl;
	}else{
	  if(sld==0){hist[3]->Fill(ii+0.4+0.33*fe,76800);}
	}
      }
      }
    }
  }
	/*
      if(!isDigital){
	file="../../../../ThesisData/Chap3/electrical/"+moduleList[ii]+"/syn_analogscan/"+FE+"_OccupancyMap.json";
	ifstream iFile(file.c_str());
	if(iFile.is_open()){
	  json jFile;
	  iFile >> jFile;
	  for(int col=0; col<128;col++){
	    for(int row=0;row<192;row++){
	      double dfe=fe;
	      if(jFile["Data"][col][row]==100){hist[0]->Fill(ii+0.252+0.125*fe,1);}
	      else if(jFile["Data"][col][row]==0){hist[2]->Fill(ii+0.252+0.125*fe,1);}
	      else{hist[1]->Fill(ii+0.252+0.125*fe,1);}
	      //	      if(jFile["Data"][col][row]!=100){
	      //		cout<<moduleList[ii]<<" "<<col<<" "<<row<<" "<<jFile["Data"][col][row]<<endl;
	      //	      }
	    }
	  }
	
      }else{
	if(sld==0)hist[3]->Fill(ii+0.252+0.125*fe,76800);
      }
      
	}
	
      }
    }
  }
	*/
  void definePlot(){
    int number=moduleList.size();
    for(int ii=0;ii<5;ii++){
      hist[ii]=new TH1D("","",3*number+1, 0, number+1./3);
      hist[ii]->SetBarWidth(0.65);
      hist[ii]->SetBarOffset(0.5);
      hist[ii]->SetLineColor(1);
      for(int jj=0;jj<number;jj++){
	hist[ii]->GetXaxis()->SetBinLabel(3*jj+1,"");
	hist[ii]->GetXaxis()->SetBinLabel(3*jj+2,moduleList[jj].c_str());
	hist[ii]->GetXaxis()->SetBinLabel(3*jj+3,"");
      }
    }
  }
  void createPlot(string saveName){
    string title;
    //    if(isDigital){title =pigtailName+", Digital scan; ; # of Pixel";}
    //    else{title =pigtailName+", Analog scan; ; # of Pixel";}
    title =pigtailName+", Disconnected bump scan; ; # of Pixel";
    THStack *hs = new THStack("hs", title.c_str());
    hist[0]->SetFillColor(3);
    hist[1]->SetFillColor(5);
    hist[2]->SetFillColor(2);
    hist[3]->SetFillColor(1);
    hist[4]->SetFillColorAlpha(0,0);
    hs->Add(hist[4]);
    hs->Add(hist[0]);
    hs->Add(hist[1]);
    hs->Add(hist[2]);
    hs->Add(hist[3]);
    //    hs->SetMinimum(0.); // "ymin"
    //    hs->SetMaximum(100.);
    //        hs->SetMinimum(0); // "ymin"
    //        hs->SetMaximum(76800);
    //    hs->SetMinimum(280000); // "ymin"
    //    hs->SetMaximum(307200);
	
    
    TLegend* leg = new TLegend(0.6,0.2,0.85,0.45);
    leg->AddEntry(hist[0],"Good pixel","f");
    leg->AddEntry(hist[1],"Bad pixel","f");
    leg->AddEntry(hist[2],"Dead pixel","f");
    leg->AddEntry(hist[3],"Uncommunicatable","f");

    TCanvas *c = new TCanvas("","",600,400);
    hs->Draw("BHIST");
    leg->Draw();

    gPad->SetLeftMargin(0.15);
    gPad->SetBottomMargin(0.15);
    //    hs->GetYaxis()->SetLimits(70000, 76800);
    //    hs->Draw("");
    //    gPad->SetLogy(1);
    hs->GetHistogram()->GetXaxis()->SetLabelSize(0.055);
    hs->GetHistogram()->GetYaxis()->SetLabelSize(0.05);
    hs->GetHistogram()->GetYaxis()->SetTitleSize(0.05);

    c->SaveAs(saveName.c_str());
  }
  void debug(){
    cout<<pigtailName<<": ";
    for(int ii=0;ii<moduleList.size();ii++){
      cout<<moduleList[ii]<<" ";
    }
    cout<<endl;
  }
};

int main(){
  //int Occ_hist(){
  int stop=4;
  vector <string> module_list[4];
  string input1[5]={"KEKQ24", "Liv8", "Paris6", "Paris16", "Goe7"};
  string input2[6]={"KEKQ22", "KEKQ25", "KEKQ19", "Goe5", "Paris11", "Paris8"};
  string input3[9]={"Paris10", "SiegenQ1", "SiegenQ2", "CERNQ11", "SiegenQ4", "Paris13", "CERNQ8", "CERNQ4", "Liv5"};
  string input4[9]={"CERNQ10", "CERNQ9", "Paris12", "SiegenQ3", "Paris9", "Paris7", "Goe10", "Goe4", "KEKQ20"};

  /*
  string input1[5]={"", "", "", "", ""};
  string input2[6]={"", "", "", "", "", ""};
  string input3[9]={"Paris10", "SiegenQ1", "SiegenQ2", "CERNQ11", "SiegenQ4", "Paris13", "CERNQ8", "CERNQ4", "Liv5"};
  string input4[9]={"CERNQ10", "CERNQ9", "Paris12", "SiegenQ3", "Paris9", "Paris7", "Goe10", "Goe4", ""};
  */
  for(int ii=0;ii<9;ii++){
    if(ii<5){module_list[0].push_back(input1[ii]);}
    if(ii<6){module_list[1].push_back(input2[ii]);}
    if(ii<9){module_list[2].push_back(input3[ii]);}
    if(ii<9){module_list[3].push_back(input4[ii]);}
  }

  byType pigtail[4];
  pigtail[0].pigtailName="Front pigtail";
  pigtail[1].pigtailName="Back pigtail";
  pigtail[2].pigtailName="Flat top pigtail";
  pigtail[3].pigtailName="Flat bottom pigtail";



  for(int ii=0;ii<stop;ii++){
    for(int jj=0;jj<module_list[ii].size();jj++){
      pigtail[ii].moduleList.push_back(module_list[ii][jj]);
    }
  }
  for(int ii=0;ii<stop;ii++){
    pigtail[ii].definePlot();
    pigtail[ii].countPixel();

  }
  pigtail[0].createPlot("front_disc.pdf");
  pigtail[1].createPlot("back_disc.pdf");
  pigtail[2].createPlot("top_disc.pdf");
  pigtail[3].createPlot("bottom_disc.pdf");    
  /*
  pigtail[0].debug();
  pigtail[1].debug();
  pigtail[2].debug();
  pigtail[3].debug();
  */
  //  cout<<module_list[0]<<endl;
}
