#include <iostream>
#include <fstream>
#include <nlohmann/json.hpp>
#include <tuple>
#include <math.h>

#include <TROOT.h>
#include <TGraph.h>
#include <TGraphErrors.h>
#include <TH1D.h>
#include <THStack.h>
#include <TStyle.h>
#include <TLegend.h>
#include <TCanvas.h>
#include <TGaxis.h>
#include <TAxis.h>

using namespace std;
using json = nlohmann::json;

class moduleData{
public:
  int digitalscan[400][192];
  int analogscan[400][192];
  int digitalCount[3];
  int analogCount[3];
};


class byType{
public:
  bool isDigital=true;
  string pigtailName;
  vector <string> moduleList;
  TH1D *hist[4];

  //  int scanMap[9][400][192];
  //  int counting[9][3];
  /*
  int digitalscan[9][400][192];
  int analogscan[9][400][192];
  int digitalCount[9][3]; // 0 is good, 1 is bad, 2 is dead.
  int analogCount[9][3];
  */
  void countPixel(){
    cout<<"Start counting"<<endl;
    for(int ii=0;ii<moduleList.size();ii++){
      int counting[3]={0,0,0};
      /*
      for(int jj=0;jj<3;jj++){
	counting[ii][jj]=0;
      }
      */
      string configDir="config/GenConfig_"+moduleList[ii]+".json";
      ifstream iCon(configDir.c_str());
      json jCon;
      iCon >> jCon;
      string FE;
      for(int fe=0;fe<2;fe++){
	int colStart=0;
	int colEnd=0;
	if(fe==0){FE=jCon["Module"]["FE2"];}
	if(fe==1){FE=jCon["Module"]["FE4"];}
      string file;
      if(isDigital){file="../../../../ThesisData/Chap4/electrical/data/"+moduleList[ii]+"/Pigtail/std_digitalscan/"+FE+"_OccupancyMap.json";colStart=0; colEnd=400;}
      else{file="../../../../ThesisData/Chap4/electrical/data/"+moduleList[ii]+"/Pigtail/lindiff_analogscan/"+FE+"_OccupancyMap.json";colStart=128;colEnd=400;}
      cout<<file<<endl;
      
      ifstream iFile(file.c_str());

      if(iFile.is_open()){
	json jFile;
	iFile >> jFile;
	for(int col=colStart; col<colEnd;col++){
	  for(int row=0;row<192;row++){
	    if(jFile["Data"][col][row]==100){hist[0]->Fill(ii+0.4+0.33*fe,1); counting[0]++;}
	    else if(jFile["Data"][col][row]==0){hist[2]->Fill(ii+0.4+0.33*fe,1); counting[1]++;}
	    else{hist[1]->Fill(ii+0.4+0.33*fe,1); counting[2]++;}
	    if(jFile["Data"][col][row]!=100){
	      cout<<moduleList[ii]<<" "<<col<<" "<<row<<" "<<jFile["Data"][col][row]<<endl;
	    }
	    //  cout<<jFile["Data"][col][row]<<" ";
	  }
	//	cout<<endl;
	}
      }else{
	cout<<"Uncommunicatable chip"<<endl;
	hist[3]->Fill(ii+0.5+0.25*fe,76800);
	cout<<"This chip was skipped"<<endl;
      }
      if(!isDigital){
	file="../../../../ThesisData/Chap4/electrical/data/"+moduleList[ii]+"/Pigtail/syn_analogscan/"+FE+"_OccupancyMap.json";
	colStart=0;
	colEnd=128;
	ifstream iFile(file.c_str());
	if(iFile.is_open()){
	  json jFile;
	  iFile >> jFile;
	  for(int col=colStart; col<colEnd;col++){
	    for(int row=0;row<192;row++){
	      if(jFile["Data"][col][row]==100){hist[0]->Fill(ii+0.4+0.33*fe,1); counting[0]++;}
	      else if(jFile["Data"][col][row]==0){hist[2]->Fill(ii+0.4+0.33*fe,1); counting[1]++;}
	      else{hist[1]->Fill(ii+0.4+0.33*fe,1); counting[2]++;}
	      if(jFile["Data"][col][row]!=100){
		cout<<moduleList[ii]<<" "<<col<<" "<<row<<" "<<jFile["Data"][col][row]<<endl;
	      }
	    }
	  }
	}
      }
      
      }
    }
  }

  void definePlot(){
    int number=moduleList.size();
    for(int ii=0;ii<4;ii++){
      hist[ii]=new TH1D("","",3*number+1, 0, number+1./3);
      hist[ii]->SetBarWidth(0.65);
      
      for(int jj=0;jj<number;jj++){
	hist[ii]->GetXaxis()->SetBinLabel(3*jj+1,"");
	hist[ii]->GetXaxis()->SetBinLabel(3*jj+2,moduleList[jj].c_str());
	hist[ii]->GetXaxis()->SetBinLabel(3*jj+3,"");
	//	hist[ii]->GetXaxis()->SetBinLabel(4*jj+4,"");
      }
    }
  }
  void createPlot(string saveName){
    string title;
    if(isDigital){title =pigtailName+", Digital scan; ; # of Pixel";}
    else{title =pigtailName+", Analog scan; ; # of Pixel";}
    THStack *hs = new THStack("hs", title.c_str());
    hist[0]->SetFillColor(3);
    hist[1]->SetFillColor(5);
    hist[2]->SetFillColor(2);
    hist[3]->SetFillColor(1);
    hs->Add(hist[0]);
    hs->Add(hist[1]);
    hs->Add(hist[2]);
    hs->Add(hist[3]);
    //    hs->SetMinimum(0); // "ymin"
    //    hs->SetMaximum(2);
    //    hs->SetMinimum(70000); // "ymin"
    //    hs->SetMaximum(76800);
    
    TLegend* leg = new TLegend(0.6,0.2,0.85,0.45);
    leg->AddEntry(hist[0],"Good pixel","f");
    leg->AddEntry(hist[1],"Bad pixel","f");
    leg->AddEntry(hist[2],"Dead pixel","f");
    leg->AddEntry(hist[3],"Uncommunicatable","f");

    TCanvas *c = new TCanvas("","",600,400);
    hs->Draw("BHIST");
    leg->Draw();
    gPad->SetLeftMargin(0.15);
    hs->GetHistogram()->GetXaxis()->SetLabelSize(0.055);
    hs->GetHistogram()->GetYaxis()->SetLabelSize(0.05);
    hs->GetHistogram()->GetYaxis()->SetTitleSize(0.05);

    c->SaveAs(saveName.c_str());
  }
  void debug(){
    cout<<pigtailName<<": ";
    for(int ii=0;ii<moduleList.size();ii++){
      cout<<moduleList[ii]<<" ";
    }
    cout<<endl;
  }
};

int main(){
  vector <string> module_list[4];
  string input1[5]={"KEKQ24", "Liv8", "Paris6", "Paris16", "Goe7"};
  string input2[6]={"KEKQ22", "KEKQ25", "KEKQ19", "Goe5", "Paris11", "Paris8"};
  string input3[9]={"Paris10", "SiegenQ1", "SiegenQ2", "CERNQ11", "SiegenQ4", "Paris13", "CERNQ8", "CERNQ4", "Liv5"};
  string input4[9]={"CERNQ10", "CERNQ9", "Paris12", "SiegenQ3", "Paris9", "Paris7", "Goe10", "Goe4", "KEKQ20"};
  for(int ii=0;ii<9;ii++){
    if(ii<5){module_list[0].push_back(input1[ii]);}
    if(ii<6){module_list[1].push_back(input2[ii]);}
    module_list[2].push_back(input3[ii]);
    module_list[3].push_back(input4[ii]);
  }

  byType pigtail[4];
  pigtail[0].pigtailName="Front pigtail";
  pigtail[1].pigtailName="Back pigtail";
  pigtail[2].pigtailName="Flat top pigtail";
  pigtail[3].pigtailName="Flat bottom pigtail";


  for(int ii=0;ii<4;ii++){
    for(int jj=0;jj<module_list[ii].size();jj++){
      pigtail[ii].moduleList.push_back(module_list[ii][jj]);
    }
  }
  for(int ii=0;ii<4;ii++){
    pigtail[ii].definePlot();
    pigtail[ii].countPixel();

  }
  pigtail[0].createPlot("front_digital.pdf");
  pigtail[1].createPlot("back_digital.pdf");
  pigtail[2].createPlot("top_digital.pdf");
  pigtail[3].createPlot("bottom_digital.pdf");
      
  /*
  pigtail[0].debug();
  pigtail[1].debug();
  pigtail[2].debug();
  pigtail[3].debug();
  */
  //  cout<<module_list[0]<<endl;
}
