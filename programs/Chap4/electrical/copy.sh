#!/bin/bash
#if [ $# -eq 0 ]                                                                                                                
#then                                                                                                                           
#    echo "Usage: $0 <module name>"                                                                                             
#    exit 0                                                                                                                     
#else                                                                                                                           
#    module=$1                                                                                                                  
#fi           
#module=("CERN_Thin_Q10" "CERN_ThinQ9" "CERN_ThinQ11" "CERN_ThinQ8" "CERN_ThinQ4")
module=("SiegenQ1" "SiegenQ2" "SiegenQ3" "Paris9" "SiegenQ4" "Paris7" "Paris13" "Goe4" "KEKQ20")
#module=("KEKQ22" "KEKQ24" "KEKQ25" "Liv8" "KEKQ19" "Paris6" "Goe5" "Paris16" "Paris11" "Goe7" "Paris8" "CERNQ10" "Paris10" "CERNQ9" "SiegenQ1" "Paris12" "SiegenQ2" "SiegenQ3" "CERNQ11" "Paris9" "SiegenQ4" "Paris7" "Paris13" "Goe10" "CERNQ8" "Goe4" "CERNQ4" "KEKQ20" "Liv5")
#str1='test'
#str2 = ""
#str3 = ""
#for module in "KEKQ22" "KEKQ24" "KEKQ25" "Liv8" "KEKQ19" "Paris6" "Goe5" "Paris16" "Paris11" "Goe7" "Paris8" "CERNQ10" "Paris10" "CERNQ9" "SiegenQ1" "Paris12" "SiegenQ2" "SiegenQ3" "CERNQ11" "Paris9" "SiegenQ4" "Paris7" "Paris13" "Goe10" "CERNQ8" "Goe4" "CERNQ4" "KEKQ20" "Liv5"; do
for m in ${module[@]}
do
    str1='/eos/atlas/atlascerngroupdisk/det-itk/general/pixels/systemtest/lls-prototype-rd53a-ob/moduledata/'
    str2=$str1$m
    str3=${str2}'*/2-AfterLoading/*igtail*'
    echo $str3
    scp -r dharada@lxplus:$str3 "data/"$m
done
#    location ="/eos/atlas/atlascerngroupdisk/det-itk/general/pixels/systemtest/lls-prototype-rd53a-ob/moduledata/"
#    location2 = location+m
#    location3 = location2+"/2-AfterLoading/Pigtail*"
#    print(location3)

    #    scp dharada@lxplus:${location} ${m}
