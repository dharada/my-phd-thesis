#include <iostream>
#include <fstream>
#include <nlohmann/json.hpp>
#include <tuple>
#include <math.h>
using namespace std;
using json = nlohmann::json;

int main(){
  string modules[29]={"KEKQ22", "KEKQ24", "KEKQ25", "Liv8", "KEKQ19", "Paris6", "Goe5", "Paris16", "Paris11", "Goe7", "Paris8", "CERNQ10", "Paris10", "CERNQ9", "SiegenQ1", "Paris12", "SiegenQ2", "SiegenQ3", "CERNQ11", "Paris9", "SiegenQ4", "Paris7", "Paris13", "Goe10", "CERNQ8", "Goe4", "CERNQ4", "KEKQ20", "Liv5"};

  for(int ii=0;ii<29;ii++){
    string configDir="config/GenConfig_"+modules[ii]+".json";
    ifstream iCon(configDir.c_str());
    json jCon;
    iCon >> jCon;
    //    cout<<jCon["Module"]["FolderPath"]["Stage4"]<<endl;;
    cout<<"cp -r "<<jCon["Module"]["FolderPath"]["Stage4"]<<"/*digital* "<<modules[ii]<<endl;;
  }
}
