#include <iostream>
#include <fstream>
#include <nlohmann/json.hpp>
#include <tuple>
#include <math.h>

#include <TROOT.h>
#include <TGraph.h>
#include <TGraphErrors.h>
#include <TH1F.h>
#include <THStack.h>
#include <TStyle.h>
#include <TLegend.h>
#include <TCanvas.h>
#include <TGaxis.h>
#include <TAxis.h>

using namespace std;
/*Memo
M6 cell pigtail
CERNQ10 1.58 1.62
Paris10 1.62 1.74
Paris3 1.58 1.62
Siegen1 1.57 1.60
Paris12 1.57 1.61
Siegen2 1.55 1.59

M12
Siegen 3 1.60 1.64
CERNQ11 1.58　?
Paris 9 1.85 1.89
Siegen 4 1.57 1.60
Paris 7 1.74 1.78
Paris 13 1.61 1.65
Goe 10 1.62 1.66
CERNQ8 1.61 1.64
Goe 4 1.51 1.56 (from spread sheet)
CERNQ4 1.56 1.61
KEKQ20 1.62 1.66
Liv 5 1.56 1.58

IHR
KEKQ22 1.61 1.64
KEKQ24 1.61 1.65
KEKQ25 1.64 1.67
Liv8 1.63 1.65
KEKQ19 1.64 1.66
Paris6 1.64 1.68
Goe5 1.57 1.61
Paris16 1.62 1.66
Paris11 1.61 1.64
Goe7 1.58 1.61
Paris8 1.71 1.75
*/

//M6
string moduleM6[6]={"CERNQ10", "Paris10", "Paris3", "Siegen1", "Paris12", "Siegen2"};
double voltageM6[6][2]={{1.58, 1.62},{1.62, 1.74},{1.58, 1.62},{1.57, 1.60},{1.57, 1.61},{1.55, 1.59}};

//M12
string moduleM12[12]={"Siegen3", "CERNQ11", "Paris9", "Siegen4", "Paris7", "Paris13", "Goe10", "CERNQ8", "Goe4", "CERNQ4", "KEKQ20", "Liv5"};
double voltageM12[12][2]={{1.60, 1.64},{1.58, 1.61},{1.85, 1.89},{1.57, 1.60},{1.74, 1.78},{1.61, 1.65},{1.62, 1.66},{1.61, 1.64},{1.51, 1.56},{1.56, 1.61},{1.62, 1.66},{1.56, 1.58}};

//IHR
string moduleIHR[11]={"KEKQ22", "KEKQ24", "KEKQ25", "Liv8", "KEKQ19", "Paris6", "Goe5", "Paris16", "Paris11", "Goe7", "Paris8"};
double voltageIHR[11][2]={{1.61, 1.64},{1.61, 1.65},{1.64, 1.67},{1.63, 1.65},{1.64, 1.66},{1.64, 1.68},{1.57, 1.61},{1.62, 1.66},{1.61, 1.64},{1.58, 1.61},{1.71, 1.75}};

int main(){
  TCanvas* c1 = new TCanvas("c1","",1200,800);
  gPad->SetBottomMargin(0.18);
  gPad->SetLeftMargin(0.12);
  TGraph *gr[2];
  vector <double> vec_voltage[2];
  vector <string> vec_label;
  for(int ii=0;ii<6;ii++){
    vec_voltage[0].push_back(voltageM6[ii][0]);
    vec_voltage[1].push_back(voltageM6[ii][1]);
    vec_label.push_back(moduleM6[ii]);
  }
  for(int ii=0;ii<12;ii++){
    vec_voltage[0].push_back(voltageM12[ii][0]);
    vec_voltage[1].push_back(voltageM12[ii][1]);
    vec_label.push_back(moduleM12[ii]);
  }
  for(int ii=0;ii<11;ii++){
    vec_voltage[0].push_back(voltageIHR[ii][0]);
    vec_voltage[1].push_back(voltageIHR[ii][1]);
    vec_label.push_back(moduleIHR[ii]);
  }
  double voltage[2][29];
  double xAxis[29];
  string label[29];
  for(int ii=0;ii<29;ii++){
    voltage[0][ii]=vec_voltage[0][ii];
    voltage[1][ii]=vec_voltage[1][ii];
    xAxis[ii]=ii+1;
    label[ii]=vec_label[ii];
  }

  gr[0]=new TGraph(29,xAxis,voltage[0]);
  gr[1]=new TGraph(29,xAxis,voltage[1]);

  //      cout<<"check"<<endl;
  gr[0]->SetMarkerStyle(20);
  gr[0]->SetMarkerColor(1);
  gr[0]->SetMarkerSize(2);
  gr[1]->SetMarkerStyle(23);
  gr[1]->SetMarkerColor(2);
  gr[1]->SetMarkerSize(2);
  gr[0]->SetTitle("The LV of each stage");
  gr[0]->GetYaxis()->SetTitle("LV [V]");
  gr[0]->GetYaxis()->SetRangeUser(1.48, 1.9);

  for(int ii=0;ii<29;ii++){
    gr[0]->GetXaxis()->SetBinLabel(3.15*ii+4.0,label[ii].c_str());
    //gr[0]->GetXaxis()->SetBinLabel(3.18*ii+4.0,label[ii].c_str());
  }
  gr[0]->GetXaxis()->SetLabelSize(0.058);
  gr[0]->GetYaxis()->SetLabelSize(0.04);
  gr[0]->GetYaxis()->SetTitleSize(0.04);
  gr[0]->Draw("ap");
  gr[1]->Draw("psame");
  TLine *tick;
  for(int ii=0;ii<31;ii++){
    tick=new TLine(ii, 1.48, ii, 1.5);
    tick->Draw();
  }

  TLegend* leg = new TLegend(0.5,0.65,0.8,0.85);
  leg->AddEntry(gr[0],"Reception test at CERN","p");
  leg->AddEntry(gr[1],"After Type-0 pigtail connection","p");
  leg->Draw();

  c1->SaveAs("voltage.pdf");
  TCanvas* c2 = new TCanvas("c2","",1200,800);
  gPad->SetBottomMargin(0.18);
  gPad->SetLeftMargin(0.12);
  TH1F *hist=new TH1F("","",16,-0.005,0.155);
  double mean=0;
  double sigma=0;
  for(int ii=0;ii<29;ii++){
    cout<<voltage[1][ii]-voltage[0][ii]<<endl;
    if(voltage[1][ii]-voltage[0][ii]>=-50){mean=mean+(voltage[1][ii]-voltage[0][ii]);}
    hist->Fill(voltage[1][ii]-voltage[0][ii]);
  }
  mean = mean/29;
  for(int ii=0;ii<29;ii++){
    if(voltage[1][ii]-voltage[0][ii]>=-50){sigma=sigma + pow(mean-(voltage[1][ii]-voltage[0][ii]),2);}
  }
  sigma=sigma/28;
  sigma=sqrt(sigma);
  cout<<"Mean: "<<mean<<endl;
  cout<<"Sigma: "<<sigma<<endl;
  hist->Draw();
  gStyle->SetOptStat(0);
  hist->SetTitle("The increase of LV after pigtail connection");
  hist->GetXaxis()->SetTitle("#Delta V [V]");
  hist->GetYaxis()->SetTitle("# of module");
  hist->GetXaxis()->SetLabelSize(0.05);
  hist->GetXaxis()->SetTitleSize(0.05);
  hist->GetYaxis()->SetLabelSize(0.04);
  hist->GetYaxis()->SetTitleSize(0.05);
  c2->SaveAs("hist.pdf");
}
