#include <iostream>
#include <fstream>
#include <nlohmann/json.hpp>
#include <tuple>
#include <math.h>
#include <vector>
#include <TROOT.h>
#include <TGraph.h>
#include <TGraphErrors.h>
#include <TH1D.h>
#include <THStack.h>
#include <TStyle.h>
#include <TLegend.h>
#include <TCanvas.h>
#include <TGaxis.h>
#include <TAxis.h>
using namespace std;
int timestamp(int hour, int min, int sec){
  return hour*3600+min*60+sec;
}
int main(){
  string run[4]={"1136", "1150", "1151","1083"};
  string envDir="data/EnvNTC_00";
  string modDir="data/ModuleNTC_00";
  int id=0;
  int startTime[4]={timestamp(15,48,30),timestamp(11,13,30),timestamp(12,29,0),timestamp(10,0,0)};
  int endTime[4]={startTime[0]+200, startTime[1]+120, startTime[2]+180,startTime[3]+300};
  cout<<"1: X+, 2: X-, 3: Front, 4: Traditional"<<endl;
  cin>>id;
  id=id-1;
  envDir=envDir+run[id]+".txt";
  modDir=modDir+run[id]+".txt";
  //  cout<<envDir<<" "<<modDir<<endl;
  ifstream envFile(envDir);
  ifstream modFile(modDir);
  string hoge;
  int iHour, iMin, iSec;
  double iTemp, iHum;
  vector<double> v_Etemp, v_Mtemp;
  if(id<3){
    while(envFile>>hoge>>iHour>>iMin>>iSec>>iTemp>>iHum){
      if(timestamp(iHour, iMin, iSec)>=startTime[id]&& timestamp(iHour, iMin, iSec)<endTime[id]){
	v_Etemp.push_back(iTemp);
      }
    }
  }else if(id==3){
    while(envFile>>hoge>>iHour>>iMin>>iSec>>iTemp>>iHum>>hoge>>hoge>>hoge>>hoge>>hoge>>hoge){
      if(timestamp(iHour, iMin, iSec)>=startTime[id]&& timestamp(iHour, iMin, iSec)<endTime[id]){
	v_Etemp.push_back(iTemp);
      }
    }
  }

  while(modFile>>hoge>>iHour>>iMin>>iSec>>iTemp){
    if(timestamp(iHour, iMin, iSec)>=startTime[id]&& timestamp(iHour, iMin, iSec)<endTime[id]){
      v_Mtemp.push_back(iTemp);
    }
  }
  
  double eMean=0, mMean=0, eSigma=0, mSigma=0;
  for(int ii=0;ii<v_Etemp.size();ii++){
    eMean+=v_Etemp[ii]/v_Etemp.size();
    cout<<v_Etemp[ii]<<" "<<v_Mtemp[ii]<<endl;
  }
  for(int ii=0;ii<v_Etemp.size();ii++){
    eSigma+=pow(eMean-v_Etemp[ii],2);
  }
  eSigma=eSigma/v_Etemp.size();
  eSigma=sqrt(eSigma);

  for(int ii=0;ii<v_Mtemp.size();ii++){
    mMean+=v_Mtemp[ii]/v_Mtemp.size();
  }
  for(int ii=0;ii<v_Mtemp.size();ii++){
    mSigma+=pow(mMean-v_Mtemp[ii],2);
  }
  mSigma=mSigma/v_Mtemp.size();
  mSigma=sqrt(mSigma);

  cout<<"Environmental mean: "<<eMean<<", sigma: "<<eSigma<<endl;
  cout<<"Module mean: "<<mMean<<", sigma: "<<mSigma<<endl;
  
}
  
