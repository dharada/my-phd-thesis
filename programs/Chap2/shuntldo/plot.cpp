#include <iostream>
#include <fstream>
#include <nlohmann/json.hpp>
#include <tuple>
#include <math.h>

#include <TROOT.h>
#include <TGraph.h>
#include <TGraphErrors.h>
#include <TH1F.h>
#include <TF1.h>
#include <TText.h>
#include <THStack.h>
#include <TStyle.h>
#include <TLegend.h>
#include <TCanvas.h>
#include <TGaxis.h>
#include <TAxis.h>

using namespace std;

int main(){
  TCanvas *c1 = new TCanvas("","",600,400);
  c1->SetBottomMargin(0.15);
  c1->SetLeftMargin(0.14);
  TF1 *f_vin=new TF1("vin", "0.8+0.6*x",0.,2);

  TF1 *f_out[3];
  f_out[0]=new TF1("out1", "7*x",0,0.101);
  f_out[1]=new TF1("out2", "0.5*x+0.65",0.095,1.105);
  f_out[2]=new TF1("out3", "1.2",1.095,2);
  f_vin->SetLineColor(2);
  f_vin->GetYaxis()->SetRangeUser(0,2);
  //  f_vin->GetXaxis()->SetLimits(0 ,2);
  //  f_vin->GetXaxis()->SetRangeUser(0.02 ,2);
  f_vin->SetTitle("Shunt-LDO");
  f_vin->GetYaxis()->SetTitle("Voltage [V]");
  f_vin->GetXaxis()->SetTitle("Input Current/chip [A]");

  f_vin->GetXaxis()->SetTitleSize(0.06);
  f_vin->GetYaxis()->SetTitleSize(0.06);

  f_vin->GetXaxis()->SetLabelSize(0.06);
  f_vin->GetYaxis()->SetLabelSize(0.06);

  f_vin->Draw();
  for(int ii=0;ii<3;ii++){
    f_out[ii]->SetLineColor(4);
    f_out[ii]->Draw("same");
  }
  TText *t_vin=new TText(1.2, 1.7, "VIN");
  t_vin->SetTextColor(2);
  t_vin->Draw("same");

  TText *t_out=new TText(1.1, 1.0, "VOUT");
  t_out->SetTextColor(4);
  t_out->Draw("same");

  c1->SaveAs("vi_curve.pdf");
}
  
