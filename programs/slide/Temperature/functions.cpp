#include <iostream>
#include <stdio.h>
#include <math.h>
#include <fstream>
#include <tuple>
#include <string>
#include <vector>
#include <TH1F.h>
#include <TCanvas.h>
#include <TF1.h>
#include <TStyle.h>
using namespace std;
tuple<double, double> tempMean(string tempFile_Path){
  ifstream tempfile(tempFile_Path.c_str());
  vector <double> v_temp;
  string hoge;
  double data;
  double average=0;
  double sigma=0;
  TH1F *hist=new TH1F("","",5,22.6,23.6);  
  while(tempfile>>hoge>>hoge>>data){
    v_temp.push_back(data);
  }
  for(int ii=0;ii<v_temp.size();ii++){
    average=average+v_temp[ii]/v_temp.size();
  }
  for(int ii=0;ii<v_temp.size();ii++){
    hist->Fill(v_temp[ii],1./v_temp.size());
    sigma=sigma+pow(average-v_temp[ii],2);
  }
  sigma=sqrt(sigma/v_temp.size());

  gStyle->SetOptStat(0);
    TCanvas *c=new TCanvas("","", 400, 400);
  c->SetLeftMargin(0.15);
  hist->SetTitle("NTC at room temperature");
  hist->GetXaxis()->SetTitle("Temperature [#circC]");
  hist->GetYaxis()->SetTitle("Normalized event");

  hist->Draw("hist");
  c->SaveAs("TempDist.pdf");

  return forward_as_tuple(average, sigma);
}

tuple<double, double> currMean(string currFile_Path, string title){
  ifstream currfile(currFile_Path.c_str());
  vector <double> v_curr;
  string hoge;
  double data;
  double average=0;
  double sigma=0;

  while(currfile>>hoge>>hoge>>hoge>>hoge>>hoge>>hoge>>hoge>>hoge>>data>>hoge>>hoge>>hoge>>hoge>>hoge>>hoge>>hoge>>hoge){
    v_curr.push_back(data*(-1)*1e9);
  }
  for(int ii=0;ii<v_curr.size();ii++){
    average=average+v_curr[ii]/v_curr.size();
  }
  for(int ii=0;ii<v_curr.size();ii++){
    sigma=sigma+pow(average-v_curr[ii],2);
  }
  sigma=sqrt(sigma/v_curr.size());
  TH1F *hist=new TH1F("","",31,average-5*sigma,average+5*sigma);
  for(int ii=0;ii<v_curr.size();ii++){
    hist->Fill(v_curr[ii],1./v_curr.size());
  }

  gStyle->SetOptStat(0);
  hist->SetTitle("Leakage current at room temperature");
  hist->GetXaxis()->SetTitle("Leakage current [nA]");
  hist->GetYaxis()->SetTitle("Normalized event");
  TCanvas *c=new TCanvas("","", 600, 600);
    c->SetLeftMargin(0.15);
  hist->Draw("hist");
  c->SaveAs(title.c_str());
  
  return forward_as_tuple(average, sigma);
}
