#include <iostream>
#include <stdio.h>
#include <math.h>
#include <tuple>
#include "functions.cpp"
using namespace std;

int main(){
  double T1, eT1;
  double I1, eI1;
  double I2[2], eI2[2];
  double term1,term2,term3,term4;  
  string TempFile_Path("Temp_room.csv");
  tie(T1, eT1) = tempMean(TempFile_Path);
  cout<<"Temperature(room): "<<T1<<" "<<eT1<<endl;

  string CurrentFile_Path[3]={"CurrentData/roomtemp.txt", "CurrentData/Jig20off.txt", "CurrentData/Jig-10off.txt"};
  tie(I1, eI1)= currMean(CurrentFile_Path[0],"roomCurrent.pdf");
  cout<<"Current(room): "<<I1<<" "<<eI1<<endl;

  tie(I2[0], eI2[0])= currMean(CurrentFile_Path[1],"Jig20.pdf");
  tie(I2[1], eI2[1])= currMean(CurrentFile_Path[2],"Jig-10.pdf");
  cout<<"Current(20C): "<<I2[0]<<" "<<eI2[0]<<endl;
  cout<<"Current(-10C): "<<I2[1]<<" "<<eI2[1]<<endl;

  double kB = 8.617 * pow(10,-5) ; //Boltzmann constant                                                     
  double Eg= 1.21; //Energy gap
  double Eg0=1.17;
  double alpha=4.73*0.0001;
  double beta=636;
  double absT = 273.15;
  T1=T1+absT;
  double T2;
  bool areMatching;
  for(int ii=0;ii<2;ii++){
    T2=0;
    areMatching=false;
    while(areMatching==false){
      if((I2[ii]/I1)<=(T2/T1)*(T2/T1)*exp((-(1.17-(4.73*0.0001*T2*T2)/(T2+636))/(2*kB))*(1./T2)-(1.17-(4.73*0.0001*T1*T1)/(T1+636))/(2*kB)*(-1./T1))){
	areMatching=true;
      }else{
	T2+=0.001;
      }
    }
    if(ii==0){cout<<"20C, Temperature: ";}
    if(ii==1){cout<<"-10C, Temperature: ";}
    cout<<T2-absT<<endl;
  

    double EgT1=1.17-4.73*pow(10,-4)/(T1+636)*pow(T1,2);
    double EgT2=1.17-4.73*pow(10,-4)/(T2+636)*pow(T2,2);
    //  double EgT1=1.21;                                                                                     
    //  double EgT2=1.21;                                                                                     
    term1=pow(eI1,2)*pow(I2[ii],2)/pow(I1,4)+pow(eI2[ii],2)/pow(I1,2);
    term2=pow((pow(T2,2))/(2*kB*pow(T1,4))*((alpha*pow(T1,3))/(pow(beta+T1,2))-(alpha*pow(T1,2))/(beta+T1)-Eg0-2*kB*T1),2);  //deltaT1
    term3=pow(1./(pow(T1,2)*2*kB)*(alpha*pow(T2,2)/(beta+T2)-(alpha*pow(T2,3)/pow(beta+T2,2))+2*kB*T2+Eg0),2);  //deltaT2
    term4=pow(exp(-EgT2/(2*kB*T2)+EgT1/(2*kB*T1)),2);
    double deltaT2=sqrt((term1/term4+term2*pow(eT1,2))/term3);
    cout<<"deltaT1: "<<sqrt(term2*pow(eT1,2)/term3)<<endl;
    cout<<"deltaI1: "<<sqrt(pow(eI1,2)*pow(I2[ii],2)/pow(I1,4)/term4/term3)<<endl;
    cout<<"deltaI2: "<<sqrt(pow(eI2[ii],2)/pow(I1,2)/term4/term3)<<endl;
    cout<<"total: "<<sqrt(term2*pow(eT1,2)/term3+pow(eI1,2)*pow(I2[ii],2)/pow(I1,4)/term4/term3+pow(eI2[ii],2)/pow(I1,2)/term4/term3)<<endl;;
  }
}
