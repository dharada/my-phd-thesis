
#include <iostream>
#include <fstream>
#include <nlohmann/json.hpp>
#include <tuple>
#include <math.h>

#include <TROOT.h>
#include <TGraph.h>
#include <TGraphErrors.h>
#include <TH1F.h>
#include <TF1.h>
#include <THStack.h>
#include <TStyle.h>
#include <TLegend.h>
#include <TCanvas.h>
#include <TGaxis.h>
#include <TAxis.h>

using namespace std;

int main(){
  TCanvas *c=new TCanvas("","", 1000, 600);
  double low=-50-900;
  double high=3050-900;
  const int bins=31;
  double data[bins]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 7, 33, 47, 50, 50, 50, 50, 50, 50, 50, 50};
  for(int ii=0;ii<bins;ii++){
    data[ii]=data[ii]/50;
  }
  
  TH1F *hist=new TH1F("","",bins, low, high);
  for(int ii=0;ii<bins;ii++){
    hist->Fill(low+(high-low)*ii/bins, data[ii]);
  }
  hist->SetTitle("Threshold scan");
  hist->GetXaxis()->SetTitle("Injection charge [e]");
  hist->GetYaxis()->SetTitle("Occupancy");
  hist->GetXaxis()->SetTitleSize(0.05);
  hist->GetYaxis()->SetTitleSize(0.05);
  hist->GetXaxis()->SetLabelSize(0.05);
  hist->GetYaxis()->SetLabelSize(0.05);
  hist->SetLineColor(1);
  hist->SetFillColorAlpha(11,0.3);
  hist->GetXaxis()->SetRangeUser(600,1800);
  gStyle->SetOptStat(0);
  hist->Draw("hist");

  TF1 *f1 = new TF1("fit","0.5*ROOT::Math::erfc(([0]-x)/(sqrt(2)*[1]))",0,3000);
            f1->SetParameters(1000,1000);

  hist->Fit(f1);
  TLine *l1=new TLine(570,0.5,1192,0.5);
  TLine *l2=new TLine(1192,0.0,1192,0.5);
  l1->SetLineStyle(7);
  l2->SetLineStyle(7);
  l1->SetLineWidth(2);
  l2->SetLineWidth(2);
  f1->Draw("same");
  l1->Draw("same");
  l2->Draw("same");
  c->SaveAs("plot.pdf");
}
