#include <iostream>
#include <fstream>
#include <nlohmann/json.hpp>
#include <tuple>
#include <math.h>
#include <fstream>
#include <sstream>
#include <string.h>
#include <algorithm>
#include <TROOT.h>
#include <TGraph.h>
#include <TH1F.h>
#include <THStack.h>
#include <TStyle.h>
#include <TLegend.h>
#include <TCanvas.h>
#include <TGaxis.h>
#include <TAxis.h>

using namespace std;
using json = nlohmann::json;

vector<string> split(string& input, char delimiter)
{
    istringstream stream(input);
    string field;
    vector<string> result;
    while (getline(stream, field, delimiter)) {
        result.push_back(field);
    }
    return result;
}


class newCanvas{
  public:
  double Rmax=-1000;
  double Rmin=1000;
  double Amax=-1000;
  double Amin=1000;
  double gFactor;
  double gOffset;
  double axisScale;
  double yAxisMax;
  string titleName;
  string fileName;
  vector <string> moduleName;

  void setModuleName(string setModule){
    moduleName.push_back(setModule);
  }
  void setName(string setName, string setFile){
    titleName=setName;
    fileName=setFile;
  }
  void setData(double rmax, double rmin, double amax, double amin, double gfactor, double goffset, double axisscale, double yaxismax){
    Rmax=rmax;
    Rmin=rmin;
    Amax=amax;
    Amin=amin;
    gFactor=gfactor;
    gOffset=goffset;
    axisScale=max(1.,axisscale);
    yAxisMax=yaxismax;
  }
  /*
  void makeGraph(TGraph *gr, THStack *hs, TGraph *ratio, TH1F *increasement, TLegend *leg1, TLegend *leg2){
    pad1->Draw();
    pad1->cd();
    gr->Draw();
    hs->Draw("same");
    leg1->Draw();
    
    pad2->Draw();
    pad2->cd();
    ratio->Draw();
    increasement->Draw();
    leg2->Draw();
  }
  */
  void makeGraph(TGraph *gr, THStack *hs, TGraph *ratio, TH1F* hist){
    TH1F *legHist[6];
    for(int ii=0;ii<6;ii++){
      legHist[ii]=new TH1F("","",1,0,1);
      legHist[ii]->SetFillColorAlpha(ii+1,0.5);
      legHist[ii]->SetLineColor(ii+1);
    }
      
    TCanvas *c = new TCanvas("canvas", "", 1200, 800);
    TPad *pad1= new TPad("pad1", "pad1", 0, 0.3, 1, 1.0);
    //TPad *pad1= new TPad("pad1", "pad1", 0.1, 0.3, 0.9, 1.0);
    pad1->Draw();
    pad1->cd();
    pad1->SetBottomMargin(0.03);
    gr->GetYaxis()->SetRange(0, yAxisMax+0.5e-6);
    gr->GetXaxis()->SetRangeUser(0, 110);
    gr->SetTitle(titleName.c_str());
    gr->GetYaxis()->SetTitle("Leakage current [A]");
    gr->GetYaxis()->SetTitleSize(0.05);
    gr->GetYaxis()->SetLabelSize(0.05);
    gr->GetXaxis()->SetTitle("Bias voltage [V]");
    gr->GetXaxis()->SetLabelSize(0);
    gr->GetXaxis()->SetTitleSize(0);
    gPad->SetGrid(1, 1); gPad->Update();
    gr->Draw("");
    gr->SetMarkerStyle(20);
    hs->Draw("same");
    gr->Draw("plsame");

    TLegend* leg_ch1 = new TLegend(0.15,0.6,0.4,0.85);
    leg_ch1->AddEntry(gr,"IV scan on LS","pl");
    for(int ii=0;ii<moduleName.size() ;ii++){
      leg_ch1->AddEntry(legHist[ii],moduleName[ii].c_str(),"f");
    }
    leg_ch1->Draw();

    c->cd();
    TPad *pad2 = new TPad("pad2", "pad2", 0, 0.0, 1, 0.31);
    //    TPad *pad2 = new TPad("pad2", "pad2", 0.1, 0.0, 0.9, 0.3);
    //    pad2->GetYaxis()->SetTitle("Rate of change");
    pad2->Draw();
    pad2->cd();
    pad2->SetTopMargin(0.1);
    pad2->SetBottomMargin(0.25);
    ratio->GetXaxis()->SetRangeUser(0,110);
    //    hist->GetXaxis()->SetRangeUser(gr->GetRange);
    ratio->GetYaxis()->SetRangeUser(axisScale*Rmin-5, axisScale*Rmax+5);
    ratio->GetXaxis()->SetLabelSize(0.1);
    ratio->SetTitle("");
    ratio->GetXaxis()->SetTitle("Bias voltage [V]");
    ratio->GetYaxis()->SetTitle("Rate of change");
    ratio->GetXaxis()->SetTitleSize(0.12);
    ratio->GetYaxis()->SetTitleSize(0.1);

    for(int ii=0;ii<40;ii++){
      //      hist[ii].Fill(5,gOffset);
    }
    ratio->Draw();
    hist->Draw("histsame");
    ratio->Draw("samepl");

    c->Modified(); c->Update();
    cout<<"GFactor Offset: "<<gFactor<<" "<<gOffset<<endl;
    TGaxis *left_axis = new TGaxis(gPad->GetUxmin(),gPad->GetUymin(), gPad->GetUxmin(), gPad->GetUymax(),Rmin-0.05, Rmax+0.05, 100,"R");
    //    TGaxis *left_axis = new TGaxis(gPad->GetUxmin(),gPad->GetUymin(), gPad->GetUxmin(), gPad->GetUymax(),Rmin,Rmax, 505,"R");
    left_axis->SetTitle("Rate of change [%]");
    left_axis->SetLabelSize(0);
    left_axis->SetTitleSize(0.08);
    left_axis->SetTitleOffset(0.5);	
    left_axis->Draw("same");
    
    TGaxis *right_axis = new TGaxis(gPad->GetUxmax(),gPad->GetUymin(), gPad->GetUxmax(), gPad->GetUymax(),(axisScale*Rmin-0.05-gOffset)/gFactor, (axisScale*Rmax+0.05-gOffset)/gFactor, 505,"+L");
  right_axis->SetLineColor(kRed);
  right_axis->SetTextColor(kRed);
  right_axis->SetTitle("Amount of change [A]");
  right_axis->SetTitleSize(0.08);
  right_axis->CenterTitle();
  right_axis->SetLabelOffset(0.008);
  //  right_axis->SetTextFont(82);
  //  right_axis->SetLabelFont(82);
  right_axis->SetLabelSize(0.1);
  right_axis->SetLabelColor(kRed);
  right_axis->SetTitleOffset(0.5);	
 right_axis->Draw("same");
    
    
    ratio->GetYaxis()->SetLabelSize(0.1);
    
    c->SaveAs(fileName.c_str());
  }
  /*
  void saveGraph(string fileName){
    c->SaveAs(fileName.c_str());
  }
  */
};

class thisChannel{
public:
  string LS;
  string channel;
  vector<string> vec_modList;
  vector<double> vec_Voltage;
  vector<double> vec_Current;

  vector<string> vec_filePath;
  vector<double> vec_singleVol;
  vector<double> vec_singleCurr;

  int dataSize_ls=0;
  int dataSize_single=999;

  vector<double> tempLS;
  vector<double> tempSingle;
  
  double volLS[41];
  double currLS[41];
  double singleVol[41];
  double singleCurr[41];
  TGraph *grDiff;
  TGraph *grRatio;
  TH1F *histDiff;
  TH1F *hist[6];

  double axisscale=0;
  double gFactor=0;
  double gOffset=0;
  double Rmax=-1000;
  double Rmin=1000;
  double Amax=-1000;
  double Amin=1000;
  double yAxisMax=0;
  //  vector <TH1F> histSingle;

  TGraph *grLS;
  THStack *hs= new THStack("", "");;
  
  double ratio(int tempSingle, int tempIHR){
    double Eg0=1.17;
    double alpha=4.73*0.0001;
    double beta=636;
    double kB=8.617 * pow(10,-5);
    double T1=tempSingle+273.2;
    double T2=tempIHR+273.2;
    //cout<<moduleName<<": "<<(T2/T1)*(T2/T1)*exp((-(1.17-(4.73*0.0001*T2*T2)/(T2+636))/(2*kB))*(1./T2)-(1.17-(4.73*0. \
0001*T1*T1)/(T1+636))/(2*kB)*(-1./T1))<<endl;
    return (T2/T1)*(T2/T1)*exp((-(1.17-(4.73*0.0001*T2*T2)/(T2+636))/(2*kB))*(1./T2)-(1.17-(4.73*0.0001*T1*T1)/(T1+636))/(2*kB)*(-1./T1));
  }
  
  void setInfo(string setLS, string setCh){
    LS=setLS;
    channel = setCh;
    for(int ii=0;ii<41;ii++){
      singleVol[ii]=0;
      singleCurr[ii]=0;
    }
  }
  void setTemp(double setLS, double setSingle){
    tempLS.push_back(setLS);
    tempSingle.push_back(setSingle);
  }
  void setModule(string module){
    vec_modList.push_back(module);
    vec_filePath.push_back(LS+"/IVbefore/"+module+"_iv_LVoff.json");
  }

  void setLS(string csvFile){
    ifstream ifs(csvFile.c_str());
    bool isFirstLine=true;
    string line;
    while(getline(ifs,line)){
      if(isFirstLine==true){isFirstLine=false;}
      else{
	vector<string> strvec = split(line,',');
	vec_Voltage.push_back(stod(strvec[1]));
	vec_Current.push_back(stod(strvec[2]));
      }
      dataSize_ls=vec_Voltage.size();
      for(int ii=0;ii<vec_Voltage.size();ii++){
	volLS[ii]=vec_Voltage[ii];
	currLS[ii]=vec_Current[ii];
	//	cout<<volLS[ii]<<" "<<currLS[ii]<<endl;
      }
    }
    grLS = new TGraph(dataSize_ls, volLS, currLS);
    grLS->SetMarkerStyle(8);
  }

    void setSingle(){
    json jsonFile;
    vector <double> vec_volt;
    vector <double> vec_vurr;
    for(int ii=0;ii<vec_filePath.size();ii++){
      string fileName=vec_filePath[ii].c_str();
      cout<<vec_modList[ii]<<" "<<fileName<<endl;
      ifstream ifs(fileName);
      ifs >> jsonFile;
      int voltCount=0;
      double plusCurr=0;;
      if(jsonFile["Sensor_IV"].size()<=dataSize_single){dataSize_single=jsonFile["Sensor_IV"].size();}
      for(int jj=2;jj<jsonFile["Sensor_IV"].size();jj++){

	//	cout<<jsonFile["Sensor_IV"][jj]["Voltage"]<<" "<< jsonFile["Sensor_IV"][jj]["Current_mean"]<<endl;
	singleVol[jj-2]=jsonFile["Sensor_IV"][jj]["Voltage"];
	plusCurr=jsonFile["Sensor_IV"][jj]["Current_mean"];
	//	cout<<jsonFile["Sensor_IV"][jj]["Voltage"]<<" "<<plusCurr<<endl;
	singleCurr[jj-2]=singleCurr[jj-2]-plusCurr*ratio(tempSingle[ii], tempLS[ii]);
	//	cout<<singleCurr[jj]<<endl;
	
      }
      hist[ii] = new TH1F("","",41, -2.5, 202.5);
      //      hist[ii] = new TH1F("","",jsonFile["Sensor_IV"].size(), -2.5, -1*singleVol[jsonFile["Sensor_IV"].size()-3]);
      
      for(int jj=0;jj<jsonFile["Sensor_IV"].size();jj++){
	
	int setVol=jsonFile["Sensor_IV"][jj]["Voltage"];
	plusCurr=jsonFile["Sensor_IV"][jj]["Current_mean"];


	hist[ii]->Fill((-1)*setVol, plusCurr*(-1)*ratio(tempSingle[ii], tempLS[ii]));
      }
      hist[ii]->SetFillColorAlpha(ii+1,0.5);
      hs->Add(hist[ii],"hist");
    }
  }


  void makeGraph(int color, int type){
    //    for(int ii=0;ii<21;ii++){cout<<singleCurr[ii]<<endl;}      
    int dataSize=min(dataSize_single, dataSize_ls);
    //    dataSize=dataSize-1;
    double xAxis[dataSize];
    double yAxis_diff[dataSize];
    double yAxis_ratio[dataSize];
    Rmax=-1000;
    Rmin=1000;
    Amax=-1000;
    Amin=1000;
    for(int ii=0;ii<dataSize;ii++){

      xAxis[ii]=-singleVol[ii];
      yAxis_diff[ii]=currLS[ii]-singleCurr[ii];
      yAxis_ratio[ii]=currLS[ii]/singleCurr[ii];
      yAxis_ratio[ii]=(currLS[ii]/singleCurr[ii]-1)*100;
      cout<<volLS[ii]<<" "<<singleVol[ii]<<" "<<currLS[ii]<<" "<<singleCurr[ii]<<" "<<yAxis_diff[ii]<<" "<<yAxis_ratio[ii]<<endl;
      Rmax=max(Rmax, yAxis_ratio[ii]);
      Amax=max(Amax, yAxis_diff[ii]);
      Rmin=min(Rmin, yAxis_ratio[ii]);
      Amin=min(Amin, yAxis_diff[ii]);
      gFactor=max(gFactor, yAxis_ratio[ii]/yAxis_diff[ii]);
    }
    grDiff=new TGraph(dataSize, xAxis, yAxis_diff);
    grRatio=new TGraph(dataSize, xAxis, yAxis_ratio);
    histDiff = new TH1F("","",41, -2.5, 202.5);
    //    histDiff = new TH1F("","",dataSize-1, xAxis[0]-2.5, xAxis[dataSize-1]-2.5);
    /*
    gFactor=(Rmax-Rmin)/(Amax-Amin);
    gOffset=(Amax*Rmin-Amin*Rmax)/(Amax-Amin);
    */
    axisscale = (Amax*gFactor+gOffset)/Rmax;
    
    for(int ii=0;ii<dataSize;ii++){
      histDiff->Fill(xAxis[ii], yAxis_diff[ii]*gFactor+gOffset);
      yAxisMax=max(yAxisMax, currLS[ii]);
    }
    histDiff->SetFillColor(2);
    histDiff->SetLineColor(2);    
    histDiff->SetFillStyle(3002);

    grDiff->SetMarkerStyle(type);
    grDiff->SetMarkerColor(color);
    grDiff->SetLineColor(color);

    grRatio->SetMarkerStyle(8);
    grRatio->SetMarkerColor(1);
    grRatio->SetLineColor(1);    
  }

  void setIHR(int ch){
    dataSize_ls=20;
    if(ch==1){
      double data[21][2]={{9.9603857040405,     2.0348462612674E-06},
			  {14.993734931946,   2.9511917546188E-06},
			  {19.994020271301,   3.5518244203558E-06},
			  {24.994869613647,   3.9302646655415E-06},
			  {30.000791358948,   4.1879675336531E-06},
			  {35.051953887939,   4.4030138724338E-06},
			  {39.999917984009,   4.5996223889233E-06},
                    {45.054046630859,   4.8038546083262E-06},
                    {49.996028900146,   5.0065241339325E-06},
                    {54.995752334595,   5.2210909871064E-06},
		    {59.994956970215,   5.4283750614559E-06},
		    {65.015140533447,   5.6583292007417E-06},
                    {69.996391296387,   5.8959437865269E-06},
                    {75.015560150146,   6.1506425481639E-06},
                    {80.007595062256,   6.411181857402E-06},
                    {84.99031829834,    6.6890247580886E-06},
                    {90.004524230957,   6.9800369146833E-06},
                    {94.948551177979,   7.2837412972149E-06},
                    {100.00233078003,   7.5952043516736E-06},
                    {104.95900344849,   8.0641084423405E-06},
                    {109.99940872192,   8.3862432802562E-06}};

	    for(int ii=0;ii<21;ii++){
      volLS[ii]=data[ii][0];
      currLS[ii]=data[ii][1];
      //      vec_Voltage.push_back(data[ii][0]);
      //      vec_Current.push_back(data[ii][1]);
	    }
    }else{
      dataSize_ls=20;
      double data[21][2]={
	{10.00492811203,    1.3916094303568E-06},
	{15.005541324615,   2.150052637262E-06},
	{19.996049880981,   2.562996769484E-06},
	{25.02348613739,    2.7756968847825E-06},
	{29.986834526062,   2.8820414854636E-06},
	{34.973385238647,   2.9421018552966E-06},
	{39.997954940796,   2.9835036002623E-06},
	{45.00118522644,    3.0141585966703E-06},
	{49.99899559021,    3.0443506830125E-06},
	{55.038782501221,   3.0702118465342E-06},
	{60.005322647095,   3.0875972242939E-06},
	{65.008479309082,   3.1096054726731E-06},
	{70.000802612305,   3.1362914342026E-06},
	{75.058638000488,   3.1737351264383E-06},
	{79.993145751953,   3.2204302215177E-06},
	{84.989559936523,   3.271412651884E-06},
	{89.996752929688,   3.3316150393148E-06},
	{94.975744628906,   3.4057223729178E-06},
	{99.999736785889,   3.488829042908E-06},
	{104.97593917847,   3.5704441643247E-06},
	{110.00538482666,   3.649287737062E-06}
      };
          for(int ii=0;ii<21;ii++){
      volLS[ii]=data[ii][0];
      currLS[ii]=data[ii][1];
      //      vec_Voltage.push_back(data[ii][0]);
      //      vec_Current.push_back(data[ii][1]);
	  }
    }
  }
};


int main(){
TCanvas *c1 = new TCanvas("c1","",600,400);
  string module_M6_ch1[3]={"Siegen1", "Paris12", "Siegen2"};
  string module_M6_ch2[3]={"CERNQ10", "Paris10", "Paris3"};

  string module_M12_ch1[6]={"Siegen4", "Paris13", "CERNQ8", "CERNQ4", "KEKQ20", "Liv5"};
  string module_M12_ch2[6]={"Siegen3", "CERNQ11", "Paris9", "Paris7", "Goe10", "Goe4"};

  string module_IHR_ch1[6]={"KEKQ22", "KEKQ25", "KEKQ19", "Goe5", "Paris11", "Paris8"};
  string module_IHR_ch2[5]={"KEKQ24", "Liv8", "Paris6", "Paris16", "Goe7"};

  double module_M6_ch1_ls[3]={23, 24.7, 24.6};
  double module_M6_ch1_single[3]={19.8, 19.8, 20.5};

  double module_M6_ch2_ls[3]={21.6, 23.7, 21.8};
  double module_M6_ch2_single[3]={20.0, 20.4, 20.2};

  double module_M12_ch1_ls[6]={21.6, 22.5, 22.5, 22.5, 21.4, 22.3};
  double module_M12_ch1_single[6]={19.9, 20.5, 20.7, 20.0, 20.5, 20.0};

  double module_M12_ch2_ls[6]={22.4, 21.0, 25.6, 21.2, 22.5, 20.8};
  double module_M12_ch2_single[6]={20.5, 20.2, 20.4, 20.5, 20.2, 20.4};

  double module_IHR_ch1_ls[6]={20.3, 20.1, 23.3, 22.0, 22.2, 21.2};
  double module_IHR_ch1_single[6]={19.8, 19.8, 20.5, 20.0, 20.0, 20.2};

  double module_IHR_ch2_ls[5]={21.8, 24.2, 22.2, 23.3, 21.3};
  double module_IHR_ch2_single[5]={20.0, 20.4, 20.2, 20.2, 20.2};
  

  // M6 channel 1;
  thisChannel M6Ch1;
  M6Ch1.setInfo("M6", "Channel 1");
  for(int ii=0;ii<3;ii++){
    M6Ch1.setModule(module_M6_ch1[ii]);
    M6Ch1.setTemp(module_M6_ch1_ls[ii], module_M6_ch1_single[ii]);
  }
  M6Ch1.setLS("M6/OB_L3_R01_T_A_SP2_HV_CH1-2022-08-11_14-36-23.csv");
  M6Ch1.setSingle();
  M6Ch1.makeGraph(1,8);
  
  //M6 channel 2
  thisChannel M6Ch2;
  M6Ch2.setInfo("M6", "Channel 2");
  for(int ii=0;ii<3;ii++){
    M6Ch2.setModule(module_M6_ch2[ii]);
    M6Ch2.setTemp(module_M6_ch2_ls[ii], module_M6_ch2_single[ii]);
  }
  M6Ch2.setLS("M6/OB_L3_R01_T_A_SP2_HV_CH2-2022-08-11_14-36-31.csv");
  M6Ch2.setSingle();
  M6Ch2.makeGraph(2,8);
  
  //M12 channel 1;
  thisChannel M12Ch1;
  M12Ch1.setInfo("M12", "Channel 1");
  for(int ii=0;ii<6;ii++){
    M12Ch1.setModule(module_M12_ch1[ii]);
    M12Ch1.setTemp(module_M12_ch1_ls[ii], module_M12_ch1_single[ii]);
  }
  M12Ch1.setLS("M12/OB_L3_B03_A_SP2_HV_CH1-2023-02-17_18-22-57.csv");
  M12Ch1.setSingle();
  M12Ch1.makeGraph(1,21);

  //M12 channel 1;
  thisChannel M12Ch2;
  M12Ch2.setInfo("M12", "Channel 2");
  for(int ii=0;ii<6;ii++){
    M12Ch2.setModule(module_M12_ch2[ii]);
    M12Ch2.setTemp(module_M12_ch2_ls[ii], module_M12_ch2_single[ii]);
  }
  M12Ch2.setLS("M12/OB_L3_B03_A_SP2_HV_CH2-2023-02-20_13-31-55.csv");
  M12Ch2.setSingle();
  M12Ch2.makeGraph(2,21);

  //IHR channel 1;
  thisChannel IHRCh1;
  IHRCh1.setInfo("IHR", "Channel 1");
  for(int ii=0;ii<6;ii++){
    IHRCh1.setModule(module_IHR_ch1[ii]);
    IHRCh1.setTemp(module_IHR_ch1_ls[ii], module_IHR_ch1_single[ii]);
  }
  IHRCh1.setLS("IHR/OB_L3_R01_T_A_SP2_HV_CH1-2022-12-13_11-46-02.csv");
  IHRCh1.setIHR(1);
  IHRCh1.setSingle();
  IHRCh1.makeGraph(1,22);

  //IHR channel 2;
  thisChannel IHRCh2;
  IHRCh2.setInfo("IHR", "Channel 2");
  for(int ii=0;ii<5;ii++){
    IHRCh2.setModule(module_IHR_ch2[ii]);
    IHRCh2.setTemp(module_IHR_ch2_ls[ii], module_IHR_ch2_single[ii]);
  }
  IHRCh2.setLS("IHR/OB_L3_R01_T_A_SP2_HV_CH2-2022-12-13_11-52-49.csv");
  IHRCh2.setIHR(2);
  IHRCh2.setSingle();
  IHRCh2.makeGraph(2,22);

  cout<<"M6C1 "<<M6Ch1.dataSize_single<<" "<<M6Ch1.dataSize_ls<<endl;
  cout<<"M6C2 "<<M6Ch2.dataSize_single<<" "<<M6Ch2.dataSize_ls<<endl;

  cout<<"M12C1 "<<M12Ch1.dataSize_single<<" "<<M12Ch1.dataSize_ls<<endl;
  cout<<"M12C2 "<<M12Ch2.dataSize_single<<" "<<M12Ch2.dataSize_ls<<endl;

  cout<<"IHRC1 "<<IHRCh1.dataSize_single<<" "<<IHRCh1.dataSize_ls<<endl;
  cout<<"IHRC2 "<<IHRCh2.dataSize_single<<" "<<IHRCh2.dataSize_ls<<endl;


  TLegend* leg = new TLegend(0.15,0.6,0.4,0.85);
  leg->AddEntry(M6Ch1.grDiff,"M6 Channel 1","pl");
  leg->AddEntry(M6Ch2.grDiff,"M6 Channel 2","pl");
  leg->AddEntry(M12Ch1.grDiff,"M12 Channel 1","pl");
  leg->AddEntry(M12Ch2.grDiff,"M12 Channel 2","pl");
  leg->AddEntry(IHRCh1.grDiff,"IHR Channel 1","pl");
  leg->AddEntry(IHRCh2.grDiff,"IHR Channel 2","pl");

  TLegend* leg2 = new TLegend(0.15,0.6,0.4,0.85);
  //  TLegend* leg2 = new TLegend(0.65,0.2,0.9,0.45);
  leg2->AddEntry(M6Ch1.grDiff,"M6 Channel 1","pl");
  leg2->AddEntry(M6Ch2.grDiff,"M6 Channel 2","pl");
  leg2->AddEntry(M12Ch1.grDiff,"M12 Channel 1","pl");
  leg2->AddEntry(M12Ch2.grDiff,"M12 Channel 2","pl");
  leg2->AddEntry(IHRCh1.grDiff,"IHR Channel 1","pl");
  leg2->AddEntry(IHRCh2.grDiff,"IHR Channel 2","pl");

  
  M6Ch1.grDiff->Draw("apl");
  M6Ch1.grDiff->GetXaxis()->SetRangeUser(0,200);
  M6Ch1.grDiff->GetYaxis()->SetRangeUser(-2e-6,5e-6);
  M6Ch1.grDiff->SetTitle("");
  M6Ch1.grDiff->GetXaxis()->SetTitle("Bias voltage [V]");
  M6Ch1.grDiff->GetYaxis()->SetTitle("Current discrepancy [A]");
  
  M6Ch2.grDiff->Draw("plsame");
  M12Ch1.grDiff->Draw("plsame");
  M12Ch2.grDiff->Draw("plsame");
  IHRCh1.grDiff->Draw("plsame");
  IHRCh2.grDiff->Draw("plsame");
  leg->Draw();
   gPad->SetGrid(1, 1); gPad->Update();
  c1->SaveAs("all_diff.pdf");

  TCanvas *c2 = new TCanvas("c1","",600,400);
  M6Ch1.grRatio->Draw("apl");
  M6Ch1.grRatio->GetXaxis()->SetRangeUser(0,200);
  M6Ch1.grRatio->GetYaxis()->SetRangeUser(0.3,3);
  M6Ch1.grRatio->SetTitle("");
  M6Ch1.grRatio->GetXaxis()->SetTitle("Bias voltage [V]");
  M6Ch1.grRatio->GetYaxis()->SetTitle("Current increase rate");
  M6Ch2.grRatio->Draw("plsame");
  
  M12Ch1.grRatio->Draw("plsame");
  M12Ch2.grRatio->Draw("plsame");
  IHRCh1.grRatio->Draw("plsame");
  IHRCh2.grRatio->Draw("plsame");
  leg2->Draw();
  gPad->SetGrid(1, 1); gPad->Update();
  c2->SaveAs("all_ratio.pdf");

  newCanvas thisCanvas[6];
  int countCan=0;
  for(int ii=0;ii<3;ii++){
    thisCanvas[countCan].setModuleName(module_M6_ch1[ii]);
  }
  thisCanvas[countCan].setName("M6 channel1","diffs_plot/M6Ch1_diff.pdf");
  thisCanvas[countCan].setData(M6Ch1.Rmax, M6Ch1.Rmin, M6Ch1.Amax, M6Ch1.Amin, M6Ch1.gFactor, M6Ch1.gOffset, M6Ch1.axisscale, M6Ch1.yAxisMax);
  thisCanvas[countCan].makeGraph(M6Ch1.grLS, M6Ch1.hs, M6Ch1.grRatio, M6Ch1.histDiff);
  countCan++;

  for(int ii=0;ii<3;ii++){
    thisCanvas[countCan].setModuleName(module_M6_ch2[ii]);
  }
  thisCanvas[countCan].setName("M6 channel2","diffs_plot/M6Ch2_diff.pdf");
  thisCanvas[countCan].setData(M6Ch2.Rmax, M6Ch2.Rmin, M6Ch2.Amax, M6Ch2.Amin, M6Ch2.gFactor, M6Ch2.gOffset, M6Ch2.axisscale, M6Ch2.yAxisMax);
  thisCanvas[countCan].makeGraph(M6Ch2.grLS, M6Ch2.hs, M6Ch2.grRatio, M6Ch2.histDiff);
  countCan++;
  
  for(int ii=0;ii<6;ii++){
    thisCanvas[countCan].setModuleName(module_M12_ch1[ii]);
  }
  thisCanvas[countCan].setName("M12 channel1","diffs_plot/M12Ch1_diff.pdf");
  thisCanvas[countCan].setData(M12Ch1.Rmax, M12Ch1.Rmin, M12Ch1.Amax, M12Ch1.Amin, M12Ch1.gFactor, M12Ch1.gOffset, M12Ch1.axisscale, M12Ch1.yAxisMax);
  thisCanvas[countCan].makeGraph(M12Ch1.grLS, M12Ch1.hs, M12Ch1.grRatio, M12Ch1.histDiff);
  countCan++;

  for(int ii=0;ii<6;ii++){
    thisCanvas[countCan].setModuleName(module_M12_ch2[ii]);
  }
  thisCanvas[countCan].setName("M12 channel2","diffs_plot/M12Ch2_diff.pdf");
  thisCanvas[countCan].setData(M12Ch2.Rmax, M12Ch2.Rmin, M12Ch2.Amax, M12Ch2.Amin, M12Ch2.gFactor, M12Ch2.gOffset, M12Ch2.axisscale, M12Ch2.yAxisMax);
  thisCanvas[countCan].makeGraph(M12Ch2.grLS, M12Ch2.hs, M12Ch2.grRatio, M12Ch2.histDiff);
  countCan++;
  
  for(int ii=0;ii<6;ii++){
    thisCanvas[countCan].setModuleName(module_IHR_ch1[ii]);
  }
  cout<<"check1"<<endl;
  thisCanvas[countCan].setName("IHR channel1","diffs_plot/IHRCh1_diff.pdf");
  cout<<"check2"<<endl;
  thisCanvas[countCan].setData(IHRCh1.Rmax, IHRCh1.Rmin, IHRCh1.Amax, IHRCh1.Amin, IHRCh1.gFactor, IHRCh1.gOffset, IHRCh1.axisscale, IHRCh1.yAxisMax);
  cout<<"check3"<<endl;
  thisCanvas[countCan].makeGraph(IHRCh1.grLS, IHRCh1.hs, IHRCh1.grRatio, IHRCh1.histDiff); //jump
  //thisCanvas[countCan].makeGraph(M12Ch1.grLS, IHRCh1.hs, IHRCh1.grRatio, IHRCh1.histDiff);
  cout<<"check4"<<endl;
  countCan++;
  
  for(int ii=0;ii<5;ii++){
    thisCanvas[countCan].setModuleName(module_IHR_ch2[ii]);
  }
  thisCanvas[countCan].setName("IHR channel2","diffs_plot/IHRCh2_diff.pdf");
  thisCanvas[countCan].setData(IHRCh2.Rmax, IHRCh2.Rmin, IHRCh2.Amax, IHRCh2.Amin, IHRCh2.gFactor, IHRCh2.gOffset, IHRCh2.axisscale, IHRCh2.yAxisMax);
  thisCanvas[countCan].makeGraph(IHRCh2.grLS, IHRCh2.hs, IHRCh2.grRatio, IHRCh2.histDiff);
  countCan++;
}
