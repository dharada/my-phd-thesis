#include <iostream>
#include <fstream>
#include <nlohmann/json.hpp>
#include <tuple>
#include <math.h>

#include <TROOT.h>
#include <TGraph.h>
#include <TGraphErrors.h>
#include <TH1F.h>
#include <THStack.h>
#include <TStyle.h>
#include <TLegend.h>
#include <TCanvas.h>
#include <TGaxis.h>
#include <TAxis.h>

using namespace std;
using json = nlohmann::json;

class thisModule{
public:
  string moduleName;
  double tempSingle;
  double tempIHR;
  double ratio(){
    double Eg0=1.17;
    double alpha=4.73*0.0001;
    double beta=636;
    double kB=8.617 * pow(10,-5);
    double T1=tempSingle+273.2;
    double T2=tempIHR+273.2;
    cout<<moduleName<<": "<<(T2/T1)*(T2/T1)*exp((-(1.17-(4.73*0.0001*T2*T2)/(T2+636))/(2*kB))*(1./T2)-(1.17-(4.73*0.0001*T1*T1)/(T1+636))/(2*kB)*(-1./T1))<<endl;
    return (T2/T1)*(T2/T1)*exp((-(1.17-(4.73*0.0001*T2*T2)/(T2+636))/(2*kB))*(1./T2)-(1.17-(4.73*0.0001*T1*T1)/(T1+636))/(2*kB)*(-1./T1));
  }
};

int main(){

  TCanvas *c1 = new TCanvas("c1", "", 1200, 800);
  const int ch1Size=13;
  const int ch2Size=31;

  double data_ch1[ch1Size][3]={
    {10.000499820709, 1.8127831253878E-06, 6.2748070735705E-09},
    {15.019072151184, 2.474267512298E-06, 1.2450038553062E-08},
    {20.035640144348, 2.8448808507164E-06, 1.3075308445476E-08},
    {25.034341049194, 3.0388902814593E-06, 1.1941233967469E-08},
    {30.042508125305, 3.140018270642E-06, 1.1946794483882E-08},
    {35.092691802979, 3.2342398299079E-06, 1.3637781555534E-08},
    {40.098617172241, 3.336431268508E-06, 1.101235814179E-08},
    {45.15213394165, 3.4679555938055E-06, 1.0975327627332E-08},
    {50.09069442749, 3.6264769505578E-06, 1.026856486399E-08},
    {55.092134857178, 3.8806027987448E-06, 1.0782017231135E-08},
    {60.072681808472, 4.4402851472114E-06, 1.0412456805377E-08},
    {65.084735870361, 5.253539984551E-06, 1.0874316279614E-08},
    {70.070037841797, 6.2037204770604E-06, 1.282376322403E-08}
  };
  
  double data_ch2[ch2Size][3]={
    {9.9808540344238, 1.7654785096965E-06, 1.5349149434089E-08},
    {14.996296405792, 2.4630385041746E-06, 9.9626491329718E-09},
    {20.039123725891, 2.8302944429015E-06, 1.4106763189519E-08},
    {25.035631370544, 3.0021054953977E-06, 1.6980165964688E-08},
    {30.002844429016, 3.0717656045454E-06, 1.4009230581592E-08},
    {34.982150650024, 3.113293610113E-06, 1.8485441009907E-08},
    {39.984056854248, 3.1298810199587E-06, 1.7199660949068E-08},
    {44.998300933838, 3.1542418355457E-06, 1.7130863246301E-08},
    {49.978741836548, 3.1552659720546E-06, 1.7080433877001E-08},
    {55.00744934082, 3.1638191558159E-06, 1.4500095211982E-08},
    {59.99475402832, 3.1848657499722E-06, 1.7679374820099E-08},
    {64.997783660889, 3.192684403075E-06, 1.1906309704858E-08},
    {69.977136230469, 3.2297578854923E-06, 1.5398715794847E-08},
    {75.020623779297, 3.2408698189101E-06, 1.7116445075122E-08},
    {80.048143005371, 3.2861202043932E-06, 1.3369032951646E-08},
    {85.020750427246, 3.3392008390365E-06, 1.4479493639612E-08},
    {90.022900390625, 3.4580775263748E-06, 1.803500994214E-08},
    {94.976192474365, 3.6119463857176E-06, 1.5696275755082E-08},
    {99.996391296387, 3.8036407659092E-06, 1.5695797687025E-08},
    {104.98215179443, 4.0163894936995E-06, 1.9368340669942E-08},
    {109.95025177002, 4.2116553686355E-06, 1.2364339902769E-08},
    {114.98765411377, 4.4453300233727E-06, 2.0136456962809E-08},
    {119.99251251221, 4.6520231990144E-06, 1.3034429086861E-08},
    {125.03480758667, 4.8880212489166E-06, 1.4164022006152E-08},
    {130.01562347412, 5.1064834678982E-06, 1.7020499495947E-08},
    {134.98708953857, 5.3402285175252E-06, 1.3884509939895E-08},
    {140.04335784912, 5.5691928537271E-06, 1.4832110098149E-08},
    {145.00591125488, 5.8075740525965E-06, 1.5743518781294E-08},
    {149.98931121826, 6.0581383422686E-06, 1.7980977238951E-08},
    {155.04350585937, 6.3031556692295E-06, 1.2663448884028E-08},
    {160.01646881104, 6.5664318753988E-06, 1.7767852398912E-08}
  };
  const int ch1Mod=6;
  const int ch2Mod=6;

  thisModule ch1[ch1Mod];
  thisModule ch2[ch2Mod];
  
  double xaxis_ch1[ch1Size];
  double yaxis_ch1[ch1Size];

  double xaxis_ch2[ch2Size];
  double yaxis_ch2[ch2Size];
  THStack *hs_ch1 = new THStack("", "");
  THStack *hs_ch2 = new THStack("", "");
  TH1F* hist_ch1[ch1Mod];
  TH1F* hist_ch2[ch2Mod];
  for(int ii=0;ii<ch1Mod;ii++){
    //    hist_ch1[ii]= new TH1F("","",24, 107.5, -2.5);
    hist_ch1[ii]= new TH1F("","",30, 0, 150);
    //    hist_ch1[ii]->Sumw2();
  }
  for(int ii=0;ii<ch2Mod;ii++){
    hist_ch2[ii]= new TH1F("","",30, 0, 150);
    //    hist_ch2[ii]->Sumw2();
  }


  string modules_ch1[ch1Mod]={"Siegen4", "Paris13", "CERNQ8", "CERNQ4", "KEKQ20", "Liv5"};
  double tempSingle_ch1[ch1Mod]={19.9, 20.5, 20.7, 20.0, 20.5, 20.0};
  double tempIHR_ch1[ch1Mod]={21.6, 22.5, 22.5, 22.5, 21.4, 22.3};
  for(int ii=0;ii<ch1Mod;ii++){
    ch1[ii].moduleName=modules_ch1[ii];
    ch1[ii].tempSingle=tempSingle_ch1[ii];
    ch1[ii].tempIHR=tempIHR_ch1[ii];
  }
    string modules_ch2[ch2Mod]={"Siegen3", "CERNQ11", "Paris9", "Paris7", "Goe10", "Goe4"}; 
    double tempSingle_ch2[ch2Mod]={20.5, 20.2, 20.4, 20.5, 20.2, 20.4};
    double tempIHR_ch2[ch2Mod]={22.4, 21.0, 25.6, 21.2, 22.5, 20.8};
  for(int ii=0;ii<ch2Mod;ii++){
    ch2[ii].moduleName=modules_ch2[ii];
    ch2[ii].tempSingle=tempSingle_ch2[ii];
    ch2[ii].tempIHR=tempIHR_ch2[ii];
  }
  for(int ii=0;ii<ch1Mod;ii++){
    string fileName = "IVbefore/"+ch1[ii].moduleName+"_iv_LVoff.json";
    cout<<fileName<<endl;
    ifstream ifs(fileName);
    json jsonFile;
    ifs >> jsonFile;

    for(int jj=0;jj<ch1Size;jj++){
      double voltage = jsonFile["Sensor_IV"][jj]["Voltage"];
      double current = jsonFile["Sensor_IV"][jj]["Current_mean"];
      voltage = abs(voltage);
      current = abs(current);
      current = current*ch1[ii].ratio();
      cout<<voltage<<" "<<current<<endl;
      hist_ch1[ii]->Fill(voltage, current);
    }
    hist_ch1[ii]->SetFillColorAlpha(ii+1,0.5);
    hs_ch1->Add(hist_ch1[ii],"hist");
  }


    for(int ii=0;ii<ch2Mod;ii++){
    string fileName = "IVbefore/"+ch2[ii].moduleName+"_iv_LVoff.json";
    cout<<fileName<<endl;
    ifstream ifs(fileName);
    json jsonFile;
    ifs >> jsonFile;

    for(int jj=0;jj<ch2Size;jj++){
      double voltage = jsonFile["Sensor_IV"][jj]["Voltage"];
      double current = jsonFile["Sensor_IV"][jj]["Current_mean"];
      voltage = abs(voltage);
      current = abs(current);
      current = current*ch2[ii].ratio();
      cout<<voltage<<" "<<current<<endl;
      hist_ch2[ii]->Fill(voltage, current);
    }
    hist_ch2[ii]->SetFillColorAlpha(ii+1,0.5);
    hs_ch2->Add(hist_ch2[ii],"hist");
  }

    //Create TGraph
  for(int ii=0;ii<ch1Size;ii++){
    xaxis_ch1[ii]=data_ch1[ii][0];
    yaxis_ch1[ii]=data_ch1[ii][1];
  }
  for(int ii=0;ii<ch2Size;ii++){
    xaxis_ch2[ii]=data_ch2[ii][0];
    yaxis_ch2[ii]=data_ch2[ii][1];
  }
  double exaxis_ch1[ch1Size];
  double exaxis_ch2[ch2Size];
  for(int ii=0;ii<ch1Size;ii++){
    exaxis_ch1[ii]=0;
  }
  for(int ii=0;ii<ch2Size;ii++){
    exaxis_ch2[ii]=0;
  }
  
  TGraphErrors *gr_ch1 = new TGraphErrors(ch1Size,xaxis_ch1,yaxis_ch1,exaxis_ch1,exaxis_ch1);
  TGraphErrors *gr_ch2 = new TGraphErrors(ch2Size,xaxis_ch2,yaxis_ch2,exaxis_ch2,exaxis_ch2);
    
  gr_ch1->GetYaxis()->SetRangeUser(0,10e-6);
  gr_ch1->SetMarkerStyle(8);
  gr_ch1->SetMarkerSize(1.5);
  gr_ch1->SetLineWidth(2);
  gr_ch1->SetTitle("IV scan ch1");
  gr_ch1->GetXaxis()->SetTitle("Bias Voltage [V]");
  gr_ch1->GetYaxis()->SetTitle("Leakage Current [A]");
  gr_ch1->GetXaxis()->SetTitleOffset(1.5);
  gr_ch1->GetXaxis()->SetLabelSize(0.045);
  gr_ch1->GetXaxis()->SetTitleSize(0.035);
  gr_ch1->GetYaxis()->SetLabelSize(0.045);
  gr_ch1->GetYaxis()->SetTitleSize(0.03);
  gr_ch1->Draw();

  gr_ch2->GetYaxis()->SetRangeUser(0,5e-6);
  gr_ch2->SetMarkerStyle(8);
  gr_ch2->SetMarkerSize(1.5);
  gr_ch2->SetLineWidth(2);
  gr_ch2->SetTitle("IV scan ch2");
  gr_ch2->GetXaxis()->SetTitle("Bias Voltage [V]");
  gr_ch2->GetYaxis()->SetTitle("Leakage Current [A]");
  gr_ch2->GetXaxis()->SetTitleOffset(1.5);
  gr_ch2->GetXaxis()->SetLabelSize(0.045);
  gr_ch2->GetXaxis()->SetTitleSize(0.035);
  gr_ch2->GetYaxis()->SetLabelSize(0.045);
  gr_ch2->GetYaxis()->SetTitleSize(0.03);

  //  gr_ch2->Draw();

  TLegend* leg_ch1 = new TLegend(0.15,0.6,0.4,0.85);
  leg_ch1->AddEntry(gr_ch1,"IV scan on M12","pl");
  for(int ii=0;ii<ch1Mod;ii++){
    leg_ch1->AddEntry(hist_ch1[ii],modules_ch1[ii].c_str(),"f");
  }
  leg_ch1->Draw();

  TLegend* leg_ch2 = new TLegend(0.15,0.6,0.4,0.85);
  leg_ch2->AddEntry(gr_ch2,"IV scan on M12","pl");
  for(int ii=0;ii<ch2Mod;ii++){
    leg_ch2->AddEntry(hist_ch2[ii],modules_ch2[ii].c_str(),"f");
  }
  //  leg_ch2->Draw();

  gPad->SetGrid(1, 1); gPad->Update();
  hs_ch1->Draw("same");
  gr_ch1->Draw("same");
  c1->SaveAs("test.pdf");

  TCanvas *c2 = new TCanvas("c2", "", 1200, 800);
  gr_ch2->Draw();
  leg_ch2->Draw();
  hs_ch2->Draw("same");
  gr_ch2->Draw("same");
  gr_ch2->Draw("plsame");
  gPad->SetGrid(1, 1); gPad->Update();
  c2->SaveAs("test_2.pdf");
}
  
