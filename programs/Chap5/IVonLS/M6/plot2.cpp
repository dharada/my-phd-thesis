#include <iostream>
#include <fstream>
#include <nlohmann/json.hpp>
#include <tuple>
#include <math.h>

#include <TROOT.h>
#include <TGraph.h>
#include <TGraphErrors.h>
#include <TH1F.h>
#include <THStack.h>
#include <TStyle.h>
#include <TLegend.h>
#include <TCanvas.h>
#include <TGaxis.h>
#include <TAxis.h>

using namespace std;
using json = nlohmann::json;

class thisModule{
public:
  string moduleName;
  double tempSingle;
  double tempIHR;
  double ratio(){
    double Eg0=1.17;
    double alpha=4.73*0.0001;
    double beta=636;
    double kB=8.617 * pow(10,-5);
    double T1=tempSingle+273.2;
    double T2=tempIHR+273.2;
    cout<<moduleName<<": "<<(T2/T1)*(T2/T1)*exp((-(1.17-(4.73*0.0001*T2*T2)/(T2+636))/(2*kB))*(1./T2)-(1.17-(4.73*0.0001*T1*T1)/(T1+636))/(2*kB)*(-1./T1))<<endl;
    return (T2/T1)*(T2/T1)*exp((-(1.17-(4.73*0.0001*T2*T2)/(T2+636))/(2*kB))*(1./T2)-(1.17-(4.73*0.0001*T1*T1)/(T1+636))/(2*kB)*(-1./T1));
  }
};

int main(){

  TCanvas *c1 = new TCanvas("c1", "", 1200, 1000);
  //    c1->Divide(1,2);
  //    c1->cd(1);
    
 TPad *pad1 = new TPad("pad1", "pad1", 0, 0.3, 1, 1.0);
 //   pad1->SetBottomMargin(0); // Upper and lower plot are joined
 //   pad1->SetGridx();         // Vertical grid
   pad1->Draw();             // Draw the upper pad: pad1
   pad1->cd();               // pad1 becomes the current pad
   /*
   p1->Divide(1,1);
   TPad *p11 = (TPad*)p1->cd(1);
   //   p11->SetFillColor(kRed);
   p11->Draw();
   */
   /*
   c1->cd(0);
   TPad *p2 = new TPad("p1","p1",0.,0.3,1.,1.);
   p2->Draw();
   p2->Divide(1,1);
   TPad *p21 = (TPad*)p2->cd(1);
   //   p21->SetFillColor(kBlue);
   p21->Draw();
   */
   //   c1->cd(0);
  
  //  gClient->SetDisplayHeight()
  const int ch1Size=19;
  const int ch2Size=19;
  cout<<"check0"<<endl;  
  double data_ch1[ch1Size][3]={
    {9.9874826431274, 1.0485252573744E-06, 2.627389074291E-09},
    {15.025173664093, 1.6605057226116E-06, 2.0710879813143E-09},
    {20.025046539307, 2.0058816971869E-06, 9.7154877701669E-10},
    {25.033677482605, 2.1699562239519E-06, 2.0029182242399E-09},
    {30.031855773926, 2.251110367979E-06, 1.5503944914513E-09},
    {35.095556640625, 2.2847345462651E-06, 1.2881282770845E-09},
    {40.086881637573, 2.2962707134866E-06, 1.3827182251648E-09},
    {45.150611877441, 2.3038195422487E-06, 2.7236281685139E-09},
    {50.093798065186, 2.3119585193854E-06, 1.8177614796583E-09},
    {55.081708526611, 2.3186961470856E-06, 1.4627983270491E-09},
    {60.071537399292, 2.3259297222467E-06, 9.351078230601E-10},
    {65.08039932251, 2.3600445956617E-06, 2.413443262771E-09},
    {70.073056793213, 2.4457136760248E-06, 1.6829168878606E-09},
    {75.086817169189, 2.5763193661987E-06, 1.4565957983645E-09},
    {80.04175491333, 2.7442663167676E-06, 1.246115392431E-09},
    {85.025927734375, 2.9596637432405E-06, 1.9191607406075E-09},
    {90.06390838623, 3.1982055588742E-06, 3.0349614976306E-09},
    {95.018264007568, 3.4469954243832E-06, 1.3103180786864E-09},
    {100.06045532227, 3.7104408875166E-06, 1.6653647292051E-09}
  };
  
  double data_ch2[ch2Size][3]={
    {10.019448757172, 4.2458892721697E-07, 3.8611731985181E-09},
    {14.996429443359, 1.0759154406514E-06, 1.6802644572592E-09},
    {19.998021697998, 1.443854421268E-06, 9.4190009777036E-10},
    {25.004500961304, 1.6289491554744E-06, 7.6448021495871E-10},
    {30.002479171753, 1.7085382637561E-06, 8.2914835836001E-10},
    {35.003999710083, 1.7386470290148E-06, 1.2141699820216E-09},
    {40.000835418701, 1.7584883607924E-06, 8.6005986995237E-10},
    {45.001155471802, 1.7720679579725E-06, 8.4108947090063E-10},
    {50.003094100952, 1.7822297422754E-06, 8.3562957589233E-10},
    {54.992969894409, 1.8052864106721E-06, 6.0101855266876E-10},
    {60.006281280518, 1.8591698903947E-06, 1.046534601866E-09},
    {64.997589874268, 1.9519438637872E-06, 9.158254009141E-10},
    {70.002800750732, 2.0721625332953E-06, 1.3793245339565E-09},
    {75.00277633667, 2.2078273104853E-06, 1.2255639153747E-09},
    {80.001065826416, 2.354652269787E-06, 1.3107566435387E-09},
    {85.001396179199, 2.4997324089782E-06, 1.716122956905E-09},
    {89.99504776001, 2.6402677804072E-06, 1.6542644168495E-09},
    {94.995769500732, 2.7880812695003E-06, 1.1412446487493E-09},
    {99.995040893555, 2.9373953793765E-06, 1.7487396392123E-09}
  };
  const int ch1Mod=3;
  const int ch2Mod=3;

  thisModule ch1[ch1Mod];
  thisModule ch2[ch2Mod];
  
  double xaxis_ch1[ch1Size];
  double yaxis_ch1[ch1Size];

  double xaxis_ch2[ch2Size];
  double yaxis_ch2[ch2Size];
  THStack *hs_ch1 = new THStack("", "");
  THStack *hs_ch2 = new THStack("", "");
  TH1F* hist_ch1[6];
  TH1F* hist_ch2[5];
  for(int ii=0;ii<6;ii++){
        hist_ch1[ii]= new TH1F("","",24, 107.5, -2.5);
    //    hist_ch1[ii]= new TH1F("","",40, 197.5, -2.5);
    //    hist_ch1[ii]->Sumw2();
  }
  for(int ii=0;ii<5;ii++){
        hist_ch2[ii]= new TH1F("","",24, 107.5, -2.5);
	//hist_ch2[ii]= new TH1F("","",40, 197.5, -2.5);
    //    hist_ch2[ii]->Sumw2();
  }
  cout<<"check1"<<endl;

  string modules_ch1[ch1Mod]={"Siegen1", "Paris12", "Siegen2"};
  double tempSingle_ch1[ch1Mod]={19.8, 19.8, 20.5};
  double tempIHR_ch1[ch1Mod]={23, 24.7, 24.6};
  for(int ii=0;ii<ch1Mod;ii++){
    ch1[ii].moduleName=modules_ch1[ii];
    ch1[ii].tempSingle=tempSingle_ch1[ii];
    ch1[ii].tempIHR=tempIHR_ch1[ii];
  }
    cout<<"check1.1"<<endl;
  string modules_ch2[ch2Mod]={"CERNQ10", "Paris10", "Paris3"};
  double tempSingle_ch2[ch2Mod]={20.0, 20.4, 20.2};
  double tempIHR_ch2[ch2Mod]={21.6, 23.7, 21.8};
  for(int ii=0;ii<ch2Mod;ii++){
    ch2[ii].moduleName=modules_ch2[ii];
    ch2[ii].tempSingle=tempSingle_ch2[ii];
    ch2[ii].tempIHR=tempIHR_ch2[ii];
  }
    cout<<"check1.2"<<endl;
  for(int ii=0;ii<ch1Mod;ii++){
    string fileName = "IVbefore/"+ch1[ii].moduleName+"_iv_LVoff.json";
    cout<<fileName<<endl;
    ifstream ifs(fileName);
    json jsonFile;
    ifs >> jsonFile;

    for(int jj=0;jj<23;jj++){
      double voltage = jsonFile["Sensor_IV"][jj]["Voltage"];
      double current = jsonFile["Sensor_IV"][jj]["Current_mean"];
      voltage = abs(voltage);
      current = abs(current);
      current = current*ch1[ii].ratio();
      cout<<voltage<<" "<<current<<endl;
      hist_ch1[ii]->Fill(voltage, current);
    }
    hist_ch1[ii]->SetFillColorAlpha(ii+1,0.5);
    hs_ch1->Add(hist_ch1[ii],"hist");
  }


    for(int ii=0;ii<ch2Mod;ii++){
    string fileName = "IVbefore/"+ch2[ii].moduleName+"_iv_LVoff.json";
    cout<<fileName<<endl;
    ifstream ifs(fileName);
    json jsonFile;
    ifs >> jsonFile;

    for(int jj=0;jj<23;jj++){
      double voltage = jsonFile["Sensor_IV"][jj]["Voltage"];
      double current = jsonFile["Sensor_IV"][jj]["Current_mean"];
      voltage = abs(voltage);
      current = abs(current);
      current = current*ch2[ii].ratio();
      cout<<voltage<<" "<<current<<endl;
      hist_ch2[ii]->Fill(voltage, current);
    }
    hist_ch2[ii]->SetFillColorAlpha(ii+1,0.5);
    hs_ch2->Add(hist_ch2[ii],"hist");
  }

  cout<<"check2"<<endl;
    //Create TGraph
  for(int ii=0;ii<ch1Size;ii++){
    xaxis_ch1[ii]=data_ch1[ii][0];
    yaxis_ch1[ii]=data_ch1[ii][1];
  }
  for(int ii=0;ii<ch2Size;ii++){
    xaxis_ch2[ii]=data_ch2[ii][0];
    yaxis_ch2[ii]=data_ch2[ii][1];
  }
  double exaxis_ch1[ch1Size];
  double exaxis_ch2[ch2Size];
  for(int ii=0;ii<ch1Size;ii++){
    exaxis_ch1[ii]=0;
  }
  for(int ii=0;ii<ch2Size;ii++){
    exaxis_ch2[ii]=0;
  }
  
  TGraphErrors *gr_ch1 = new TGraphErrors(ch1Size,xaxis_ch1,yaxis_ch1,exaxis_ch1,exaxis_ch1);
  TGraphErrors *gr_ch2 = new TGraphErrors(ch2Size,xaxis_ch2,yaxis_ch2,exaxis_ch2,exaxis_ch2);
    
  gr_ch1->GetYaxis()->SetRangeUser(0,10e-6);
  gr_ch1->SetMarkerStyle(8);
  gr_ch1->SetMarkerSize(1.5);
  gr_ch1->SetLineWidth(2);
  gr_ch1->SetTitle("IV scan ch1");
  gr_ch1->GetXaxis()->SetTitle("Bias Voltage [V]");
  gr_ch1->GetYaxis()->SetTitle("Leakage Current [A]");
  gr_ch1->GetXaxis()->SetTitleOffset(1.5);
  gr_ch1->GetXaxis()->SetLabelSize(0.045);
  gr_ch1->GetXaxis()->SetTitleSize(0.035);
  gr_ch1->GetYaxis()->SetLabelSize(0.045);
  gr_ch1->GetYaxis()->SetTitleSize(0.03);
  gr_ch1->Draw();

  gr_ch2->GetYaxis()->SetRangeUser(0,5e-6);
  gr_ch2->SetMarkerStyle(8);
  gr_ch2->SetMarkerSize(1.5);
  gr_ch2->SetLineWidth(2);
  gr_ch2->SetTitle("IV scan ch2");
  gr_ch2->GetXaxis()->SetTitle("Bias Voltage [V]");
  gr_ch2->GetYaxis()->SetTitle("Leakage Current [A]");
  gr_ch2->GetXaxis()->SetTitleOffset(1.5);
  gr_ch2->GetXaxis()->SetLabelSize(0.045);
  gr_ch2->GetXaxis()->SetTitleSize(0.035);
  gr_ch2->GetYaxis()->SetLabelSize(0.045);
  gr_ch2->GetYaxis()->SetTitleSize(0.03);

  //  gr_ch2->Draw();
  cout<<"check3"<<endl;
  TLegend* leg_ch1 = new TLegend(0.15,0.6,0.4,0.85);
  leg_ch1->AddEntry(gr_ch1,"IV scan on M6","pl");
  for(int ii=0;ii<ch1Mod;ii++){
    leg_ch1->AddEntry(hist_ch1[ii],modules_ch1[ii].c_str(),"f");
  }
  leg_ch1->Draw();

  TLegend* leg_ch2 = new TLegend(0.15,0.6,0.4,0.85);
  leg_ch2->AddEntry(gr_ch2,"IV scan on M6","pl");
  for(int ii=0;ii<ch2Mod;ii++){
    leg_ch2->AddEntry(hist_ch2[ii],modules_ch2[ii].c_str(),"f");
  }
  //  leg_ch2->Draw();

  gPad->SetGrid(1, 1); gPad->Update();
  hs_ch1->Draw("same");
  gr_ch1->Draw("same");

  //c1->cd(1);
  
  c1->cd();          // Go back to the main canvas before defining pad2
   TPad *pad2 = new TPad("pad2", "pad2", 0, 0.05, 1, 0.3);
   //   pad2->SetTopMargin(0);
   // pad2->SetBottomMargin(0.2);
   //   pad2->SetGridx(); // vertical grid
   pad2->Draw();
   pad2->cd();       // pad2 becomes the current
  gr_ch2->Draw();
  leg_ch2->Draw();
  
  /*
  p2->Divide(1,1);
  TPad *p21 = (TPad*)p2->cd(1);
  //   p21->SetFillColor(kBlue);
  p21->Draw();
  */
  //  hs_ch1->Draw("same");
  //  gr_ch1->Draw("same");
  //Test of split
  c1->SaveAs("test.pdf");

  TCanvas *c2 = new TCanvas("c2", "", 1200, 800);
  gr_ch2->Draw();
  leg_ch2->Draw();
  hs_ch2->Draw("same");
  gr_ch2->Draw("same");
  gr_ch2->Draw("plsame");
  gPad->SetGrid(1, 1); gPad->Update();
  c2->SaveAs("test_2.pdf");
}
  
