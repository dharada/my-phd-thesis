#include <iostream>
#include <fstream>
#include <nlohmann/json.hpp>
#include <tuple>
#include <math.h>

#include <TROOT.h>
#include <TGraph.h>
#include <TH1F.h>
#include <TH2F.h>
#include <THStack.h>
#include <TStyle.h>
#include <TLegend.h>
#include <TCanvas.h>
#include <TGaxis.h>
#include <TAxis.h>

using namespace std;
using json = nlohmann::json;


int goe5(){
  //  ifstream i("./006953_std_digitalscan/20UPGFC0012436_OccupancyMap.json");
  ifstream i("./007212_std_digitalscan/20UPGFC0012436_OccupancyMap.json");
  json j;
  i >> j;
  TCanvas *c1=new TCanvas("","",1200,800);
  TH2F *hist = new TH2F("","", 400, -0.5, 399.5, 192, -0.5, 191.5);
  double fill;
  
  for(int col=0;col<400;col++){
    for(int row=0;row<192;row++){
      fill=j["Data"][col][row];
      //if(fill!=0){cout<<col<<" ";}
      hist->Fill(col, row, fill);
    }
  }
  hist->SetTitle("Goe5 FE4 Digital scan occupancy map");
  hist->GetXaxis()->SetTitle("Column");
  hist->GetYaxis()->SetTitle("Row");
  hist->GetZaxis()->SetRangeUser(95, 101);  
  gStyle->SetOptStat(0);
  hist->Draw("colz");

  TLine *line1=new TLine(128, 0, 128, 192);
  TLine *line2=new TLine(264, 0, 264, 192);
  line1->SetLineStyle(9);
  line2->SetLineStyle(9);
  line1->SetLineWidth(2);
  line2->SetLineWidth(2);
  TText *t1=new TText(1, 5, "Sync. FE");
  TText *t2=new TText(130, 5, "Lin. FE");
  TText *t3=new TText(266, 5, "Diff. FE");

  line1->Draw();
  line2->Draw();
  t1->Draw();
  t2->Draw();
  t3->Draw();


  c1->SaveAs("Goe5_map.pdf");

};



//}
