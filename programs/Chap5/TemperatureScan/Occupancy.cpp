#include <iostream>
#include <fstream>
#include <nlohmann/json.hpp>
#include <tuple>
#include <math.h>

#include <TROOT.h>
#include <TGraph.h>
#include <TH1F.h>
#include <THStack.h>
#include <TStyle.h>
#include <TLegend.h>
#include <TCanvas.h>
#include <TGaxis.h>
#include <TAxis.h>

using namespace std;
using json = nlohmann::json;

class data{
public:
  bool isCooling;
  int temperature[5];
  int runnumber[5];
  double totalPix = 76800;
  string fileDirectory[5];
  string moduleName;
  string chipId;
  string fileName[5];
  int greenPix[5]={0,0,0,0,0};
  int yellowPix[5]={0,0,0,0,0};
  int redPix[5]={0,0,0,0,0};
  json jfile[5];
  ifstream *file[5];
  THStack *TS;
  TLegend* leg = new TLegend(0.6,0.65,0.85,0.85);
  void setTStack(){
    string label;
    if(isCooling){
      label = moduleName +" Cooling down;Cooling pipe temperature[C] ; Number of Pixels";
    }else{
      label = moduleName +" Warming up;Cooling pipe temperature[C] ; Number of Pixels";
    }
    TS = new THStack("", label.c_str());
  }
  
  void setRunInfo(int *Temperature, int * RunNumber){
    for(int ii=0;ii<5;ii++){
      temperature[ii] = Temperature[ii];
      runnumber[ii] = RunNumber[ii];
    }
  }

  void setFileDirectory(){
    for(int ii=0;ii<5;ii++){
      fileDirectory[ii] = "data/00"+ to_string(runnumber[ii]) + "_std_digitalscan/";
      fileName[ii] = fileDirectory[ii] + chipId + "_OccupancyMap.dat";
      cout<<fileName[ii]<<endl;
    }
  }

  void openFiles(){
    for(int ii=0;ii<5;ii++){
      file[ii] = new ifstream(fileName[ii].c_str());
      int count = 0;
      string hoge;
      string s_occupancy;
      int occupancy;
      while(*file[ii]>>s_occupancy){
	count++;
	if(count>13){
	  occupancy = stoi(s_occupancy);
	  if(occupancy==100){greenPix[ii]++;}
	  else if(occupancy==0){redPix[ii]++;}
	  else{yellowPix[ii]++;}
	}
      }
    }
  }

  void makeHistogram(){
    setTStack();
    TH1D *hist[3];
    hist[0]= new TH1D("","",20, 0, 20); //green
    hist[1]= new TH1D("","",20, 0, 20); //yellow
    hist[2]= new TH1D("","",20, 0, 20); //red

    hist[0]->GetYaxis()->SetLabelSize(1);
    hist[1]->GetYaxis()->SetLabelSize(1);
    hist[2]->GetYaxis()->SetLabelSize(1);
    for(int ii=0;ii<20;ii++){
      if((ii+1)%4==0){
	hist[0]->GetXaxis()->SetBinLabel(ii, to_string(temperature[(ii-1)/4]).c_str());
	hist[1]->GetXaxis()->SetBinLabel(ii, to_string(temperature[(ii-1)/4]).c_str());
	hist[2]->GetXaxis()->SetBinLabel(ii, to_string(temperature[(ii-1)/4]).c_str());
      }else{
	hist[0]->GetXaxis()->SetBinLabel(ii, "");
	hist[1]->GetXaxis()->SetBinLabel(ii, "");
	hist[2]->GetXaxis()->SetBinLabel(ii, "");
      }
    }

	    
    for(int ii=0;ii<5;ii++){
      hist[0]->Fill(4*ii+1, greenPix[ii]);
      hist[1]->Fill(4*ii+2, yellowPix[ii]);
      hist[2]->Fill(4*ii+3, redPix[ii]);
    }
    hist[0]->SetFillColor(3);
    hist[1]->SetFillColor(5);
    hist[2]->SetFillColor(2);

    leg->AddEntry(hist[0],"Green (Occ=100)","f");
    leg->AddEntry(hist[1],"Yellow (1<Occ<99,Occ>101)","f");
    leg->AddEntry(hist[2],"Red (Occ=0)","f");

      
    TS->Add(hist[0],"hist");
    TS->Add(hist[1],"hist");
    TS->Add(hist[2],"hist");
 
  }
  void checkPixels(){
    cout<<moduleName<<endl;
    cout<<"GreenPixels"<<endl;
    for(int ii=0;ii<5;ii++){
      cout<<greenPix[ii]<<" ";
    }
    cout<<endl<<endl;
    cout<<"YellowPixels"<<endl;
    for(int ii=0;ii<5;ii++){
      cout<<yellowPix[ii]<<" ";
    }
    cout<<endl<<endl;
    cout<<"RedPixels"<<endl;
    for(int ii=0;ii<5;ii++){
      cout<<redPix[ii]<<" ";
    }
  }
};    

int main(){
  bool isCooling = false;
  int filltemp[5];
  int fillrunnum[5];
  if(isCooling){
    int temperature[5]={10, 0, -10, -20, -28};
    int runnumber[5]={7544, 7545, 7546, 7547, 7549};
    memcpy(filltemp, temperature, sizeof(temperature));
    memcpy(fillrunnum, runnumber, sizeof(runnumber));
  }else{
    int temperature[5]={-28, -20, -10, 0, -10};
    int runnumber[5]={7552, 7554, 7556, 7558, 1};
    memcpy(filltemp, temperature, sizeof(temperature));
    memcpy(fillrunnum, runnumber, sizeof(runnumber));
  }

  TCanvas *c1 = new TCanvas("c1", "", 1200, 800);
  string chipInfo[11][2]={{"KEKQ22", "20UPGFC0009319"},
			{"KEKQ24", "20UPGFC0009349"},
			{"KEKQ25", "20UPGFC0009382"},
			{"Liv8",   "20UPGFC0010037"},
			{"KEKQ19", "20UPGFC0005719"},
			{"Paris6", "20UPGFC0008763"},
			{"Goe5",   "20UPGFC0012436"},
			{"Paris16","20UPGFC0008759"},
			{"Paris11","20UPGFC0008773"},
			{"Goe7",   "20UPGFC0012454"},
			{"Paris8", "20UPGFC0008843"}};

  data thisModule[11];
  for(int ii=0;ii<11;ii++){
    thisModule[ii].isCooling = isCooling;
    thisModule[ii].moduleName = chipInfo[ii][0];
    thisModule[ii].chipId = chipInfo[ii][1];
    thisModule[ii].setRunInfo(filltemp, fillrunnum);
    thisModule[ii].setFileDirectory();
    thisModule[ii].openFiles();
    //    thisModule[ii].checkPixels();
    thisModule[ii].makeHistogram();
    thisModule[ii].TS->Draw();
    thisModule[ii].leg->Draw();
    string saveName;
    if(isCooling){
      saveName = "plots/"+thisModule[ii].moduleName+"_cool.pdf";
    }else{
      saveName = "plots/"+thisModule[ii].moduleName+"_warm.pdf";
    }c1->SaveAs(saveName.c_str());
  }

 
}
