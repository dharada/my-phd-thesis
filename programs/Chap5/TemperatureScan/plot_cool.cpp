{
  double temp[5]={10, 0, -10, -20, -28};
  double hoge[5]={1,2,3,4,5};
  double func[5]={10, 10, 10, 10, 10};
  double volt[5]={21.225, 21.225, 21.225, 21.225, 21.225};
  gStyle->SetOptStat(0);
  frame = new TH1F("frame", "", 10, -12, 30);
  frame->SetMinimum(0.0);
  frame->SetMaximum(11.0);
  frame->SetTitle("Cooling down");
  frame->Draw("AB");
  TF1 *f1 = new TF1("f1", "x", 11, 0);
  TF1 *f2 = new TF1("f2", "-x", 12, -30);
  TF1 *f3 = new TF1("f3", "x", 22, 0);
  TGaxis *ay1 = new TGaxis(-12, 0, -12, 11, "f1", 510, "-");
  TGaxis *ay2 = new TGaxis(30, 0, 30, 11, "f3", 510, "+L");
  TGaxis *ax1 = new TGaxis(-12, 0, 30, 0, "f2", 510, "");
  ay1->SetTitle("# of Functional modules");
  ay2->SetTitle("LV voltage");
  ay2->SetLineColor(2);
  ay2->SetLabelColor(2);
  ay2->SetTitleColor(2);
  ax1->SetTitle("Cooling pipe temperature[C]");
  ay1->Draw();
  ay2->Draw();
  ax1->Draw();
  
  for(int ii=0;ii<5;ii++){
    temp[ii]=-temp[ii];
    volt[ii]=volt[ii]/2;
  }
  TGraph *gr = new TGraph(5, temp, func);
  gr->SetMarkerStyle(8);
  gr->SetMarkerSize(1);
  gr->Draw("plsame");

  TGraph *gr2 = new TGraph(5, temp, volt);
  gr2->SetMarkerStyle(8);
  gr2->SetMarkerSize(1);
  gr2->SetLineColor(2);
  gr2->SetMarkerColor(2);
  gr2->Draw("plsame");

  TLegend *leg= new TLegend(0.15,0.15,0.4,0.3);
  leg->AddEntry(gr,"Functional module","pl");
  leg->AddEntry(gr2,"LV voltage","pl");
  
  leg->Draw();

}
