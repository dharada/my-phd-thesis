{
  /*
  frame = new TH1F("frame", "", 10, -50, 1000);
  frame->SetMinimum(0.0);
  frame->SetMaximum(20.0);
  frame->Draw("AB");
  */
  double curr[13]={5, 4.75, 4.5, 4.25, 4, 3.75, 3.5, 3.25, 3, 2.75, 2.5, 2.25, 2};
  //  double func[13]={10, 10,  10,  9,    10, 9,   8,   5,    3, 2,    0,   0,    0}; //10C
  //  double voltage[13]={22.201, 21.486, 20.838, 19.998, 19.598, 18.689, 17.904, 17.16, 16.32, 15.301, 14.406, 13.717, 11.596}; //10C

  double func[13]={9, 9, 8, 7, 5, 5, 4, 2, 1, 1, 0, 0, 0};
  double voltage[13]={22.092, 21.402, 20.631, 19.777, 19.158, 18.221, 17.408, 16.555, 15.77, 14.998, 14.062, 12.877, 11.059};
    
  for(int ii=0;ii<13;ii++){
    voltage[ii]=voltage[ii]-10;
  }
  TGraph *gr = new TGraph(13,curr, func);
  TGraph *gr2 = new TGraph(13,curr, voltage);
  gr->SetMarkerStyle(8);
  gr->GetYaxis()->SetRangeUser(0,12.5);
  gr2->SetMarkerStyle(8);
  gr2->SetMarkerColor(2);
  gr2->SetLineColor(2);
  gr->SetTitle("VI scan (-10C)");
  gr->GetXaxis()->SetTitle("Current [A]");
  gr->GetYaxis()->SetTitle("Functional module");
  //  TGaxis *a1 = new TGaxis(-50, 0, -50, 20, "f1", 510, "-");

  TAxis *mg2Xaxis = gr2->GetXaxis();
  TAxis *mg2Yaxis = gr2->GetYaxis();
  Double_t xmin = mg2Xaxis->GetXmin();
  Double_t xmax = mg2Xaxis->GetXmax();
  Double_t ymin = mg2Yaxis->GetXmin();
  Double_t ymax = mg2Yaxis->GetXmax();
  mg2Xaxis->SetLabelSize(0);
  mg2Xaxis->SetTickLength(0);
  mg2Yaxis->SetLabelSize(0);
  mg2Yaxis->SetTickLength(0);
  //  TGaxis *yaxis = new TGaxis(xmax,ymin,xmax,20,ymin,0,510,"+L");
  TGaxis *yaxis = new TGaxis(xmax,0,xmax,12.5,10,22.5,510,"+L");
  gr->Draw("apl");
  gr2->Draw("plsame");
  yaxis->SetLabelColor(kRed);
  yaxis->Draw();

  yaxis->SetTitle("Voltage [V]");
  yaxis->SetTitleColor(kRed);


  TLegend *leg= new TLegend(0.15,0.65,0.4,0.85);
  leg->AddEntry(gr,"Functional module","pl");
  leg->AddEntry(gr2,"LV voltage","pl");

  leg->Draw();


}
