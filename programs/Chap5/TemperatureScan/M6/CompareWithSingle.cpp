#include <iostream>
#include <fstream>
#include <nlohmann/json.hpp>
#include <tuple>
#include <math.h>

#include <TROOT.h>
#include <TGraph.h>
#include <TH1F.h>
#include <THStack.h>
#include <TStyle.h>
#include <TLegend.h>
#include <TCanvas.h>
#include <TGaxis.h>
#include <TAxis.h>

using namespace std;
using json = nlohmann::json;

class moduleTemp{
private:

public:
  string moduleName;
  int id;
  TGraph *grIHR;
  TGraph *grSingle;
  TGraph *grCompare;
  double coolingIHR[5]={10.5, 0.5, -9.35, -19.4, -26.3};
  double moduleTempIHR[5];
  //  string modules[5] ={"CERN10", "Paris10", "Paris3", "Paris12", "Siegen2"};
  double coolingSingle[5][3]={
    {20.3, 10.7, -10.6},
    {20.5, 10.7, -9.3},
    {20.6, 10.5, -10.9},
    {20.3, 10.9, -10.9},
    {20.3, 10.7, -11.0}};
  //  double coolingSingle[3]={20, 10, -10};
  double moduleTempSingle[3];

  TGraph *gr[2];
  string retModuleName(){
    return moduleName;
  }
  void setModuleTempIHR(double temp10, double temp0, double tempm10, double tempm20, double tempm28, double *mean){
    moduleTempIHR[0]=temp10;
    moduleTempIHR[1]=temp0;
    moduleTempIHR[2]=tempm10;
    moduleTempIHR[3]=tempm20;
    moduleTempIHR[4]=tempm28;

    for(int ii=0;ii<5;ii++){
      //moduleTempIHR[ii] = moduleTempIHR[ii]-mean[ii];
      //      moduleTempIHR[ii]= moduleTempIHR[ii]-coolingIHR[ii];
    }
  }
  void setModuleTempSingle(double temp20, double temp10, double tempm10, double *mean){
    moduleTempSingle[0]=temp20;
    moduleTempSingle[1]=temp10;
    moduleTempSingle[2]=tempm10;

    for(int ii=0;ii<3;ii++){
      //moduleTempSingle[ii] = moduleTempSingle[ii]-mean[ii];
      //moduleTempSingle[ii]= moduleTempSingle[ii]-coolingSingle[id][ii];
    }

  }

  void makeThisGraph(){
    TCanvas *can = new TCanvas("can","", 1200, 800);
    double xaxis[2]={0, 11};
    
    TGraph *frame = new TGraph(2, xaxis, xaxis);
    frame->GetXaxis()->SetRangeUser(-30, 22);
    frame->GetYaxis()->SetRangeUser(-15, 30);
    frame->SetTitle(moduleName.c_str());
    frame->GetXaxis()->SetTitle("Cooling temperature [C]");
    frame->GetYaxis()->SetTitle("Module temperature [C]");

    frame->Draw("ap");
    gr[0] = new TGraph(5, coolingIHR, moduleTempIHR);
    gr[1] = new TGraph(3, coolingSingle[id], moduleTempSingle);

    gr[0]->SetMarkerStyle(8);
    
    gr[1]->SetLineStyle(4);
    gr[1]->SetLineColor(2);
    gr[1]->SetMarkerColor(2);
    gr[1]->SetMarkerStyle(8);
    
    gr[0]->Draw("plsame");
    gr[1]->Draw("plsame");

    TLegend* leg = new TLegend(0.15,0.65,0.4,0.85);
    leg->AddEntry(gr[0], "on IHR","pl");
    leg->AddEntry(gr[1], "Single cell setup","pl");
    leg->Draw();

    string saveName = moduleName+"_plot.pdf";
    can->SaveAs(saveName.c_str());
  }
    
    
  void makeGraphIHR(){
    grIHR=new TGraph(5, coolingIHR, moduleTempIHR);
    grIHR->SetMarkerStyle(24+id);
    grIHR->SetMarkerSize(2);
    if(id!=9&&id!=4){
      grIHR->SetMarkerColor(id+1);
      grIHR->SetLineColor(id+1);
    }else if(id==9){
      grIHR->SetMarkerColor(12);
      grIHR->SetLineColor(12);
    }else{
      grIHR->SetMarkerColor(6);
      grIHR->SetLineColor(6);
    }
    
    grIHR->SetLineWidth(3);
    grIHR->GetXaxis()->SetTitle("Cooling pipe temperature [C]");
    //        grIHR->GetYaxis()->SetTitle("delta T (Module temperature - cooling pipe)");
    grIHR->GetYaxis()->SetTitle("Module temperature [C]");
  }

  void makeGraphSingle(){
    grSingle=new TGraph(3, coolingSingle[id], moduleTempSingle);
    grSingle->SetMarkerStyle(24+id);
    grSingle->SetMarkerSize(2);
    if(id!=9&&id!=4){
      grSingle->SetMarkerColor(id+1);
      grSingle->SetLineColor(id+1);
    }else if(id==9){
      grSingle->SetMarkerColor(12);
      grSingle->SetLineColor(12);
    }else{
      grSingle->SetMarkerColor(6);
      grSingle->SetLineColor(6);
    }   
    grSingle->SetLineWidth(3);
    grSingle->GetXaxis()->SetTitle("Cooling jig temperature [C]");
    //    grSingle->GetYaxis()->SetTitle("delta T (Module temperature - cooling block)");
    grSingle->GetYaxis()->SetTitle("Module temperature [C]");
  }

  void compareGraph(){
    cout<<moduleName<<endl;
    double xAxis[2]={moduleTempSingle[1], moduleTempSingle[2]};
    double yAxis[2]={moduleTempIHR[0], moduleTempIHR[2]};
    cout<<xAxis[0]<<" "<<xAxis[1]<<endl;
    cout<<yAxis[0]<<" "<<yAxis[1]<<endl;
    
    grCompare = new TGraph(2, xAxis, yAxis);
    grCompare->SetMarkerStyle(24+id);
    grCompare->SetMarkerSize(2);
    if(id!=9&&id!=4){
      grCompare->SetMarkerColor(id+1);
      grCompare->SetLineColor(id+1);
    }else if(id==9){
      grCompare->SetMarkerColor(12);
      grCompare->SetLineColor(12);
    }else{
      grCompare->SetMarkerColor(13);
      grCompare->SetLineColor(13);
    }
    grCompare->SetLineWidth(3);
    grCompare->GetXaxis()->SetTitle("Single testing [C]");
    grCompare->GetYaxis()->SetTitle("On IHR [C]");
  }

  
  void checkData(){
    cout<<moduleName<<" ";
    for(int ii=0;ii<5;ii++){
      cout<<moduleTempIHR[ii]<<" ";
    }
    cout<<endl;
  }

};
  
double returnMean(double *data){
  double average=0;
  for(int ii=0;ii<5;ii++){
    average = average + data[ii]/11;
    cout<<data[ii]<<" ";
  }
  cout<<average<<endl;
  return average;
}

int main(){
  TCanvas *c1 = new TCanvas("c1","", 1200, 800);
  moduleTemp data[5];
  string modules[5] ={"CERN10", "Paris10", "Paris3", "Paris12", "Siegen2"};

  double IHRtemp_10[5]={20.2, 22.03, 20.39, 22.84, 23.1};
  double IHRtemp_0[5]={11.444, 13.24, 11.589, 14.11, 14.502};
  double IHRtemp_m10[5]={2.579, 4.711, 2.797, 5.22, 5.895};
  double IHRtemp_m20[5]={-6.33, -4.085, -6.246, -3.76, -2.723};
  double IHRtemp_m28[5]={-13.0022, -10.609, -12.737, -10.19, -9.233};

  double Singletemp_20[5]= {25.7, 26.5, 25.9, 27.3, 28.3};
  double Singletemp_10[5]= {15.9, 16.6, 16.2, 17.7, 18.8};
  double Singletemp_m10[5]={-5.1, -3.7, -5.3, -3.8, -2.6};

  
  double IHRyAxis[5][5];
  double singleyAxis[3][5];
  
  for(int ii=0;ii<5;ii++){
    IHRyAxis[0][ii]=IHRtemp_10[ii];
    IHRyAxis[1][ii]=IHRtemp_0[ii];
    IHRyAxis[2][ii]=IHRtemp_m10[ii];
    IHRyAxis[3][ii]=IHRtemp_m20[ii];
    IHRyAxis[4][ii]=IHRtemp_m28[ii];

    singleyAxis[0][ii]=Singletemp_20[ii];
    singleyAxis[1][ii]=Singletemp_10[ii];
    singleyAxis[2][ii]=Singletemp_m10[ii];	    
  }


  double IHRxAxis[5]={0.5, 1.5,2.5,3.5,4.5};
  TGraph *grIHR2[5];
  TGraph *grSingle2[3];
  for(int ii=0;ii<3;ii++){
    grSingle2[ii]=new TGraph(5,IHRxAxis, singleyAxis[ii]);
    grSingle2[ii]->SetMarkerSize(2);
    grSingle2[ii]->SetLineWidth(3);
  }
  grSingle2[0]->SetMarkerColor(7);
  grSingle2[0]->SetMarkerStyle(23);

  grSingle2[1]->SetMarkerColor(1);
  grSingle2[1]->SetMarkerStyle(20);

  grSingle2[2]->SetMarkerColor(3);
  grSingle2[2]->SetMarkerStyle(22);
  for(int ii=0;ii<5;ii++){
    grIHR2[ii]=new TGraph(5,IHRxAxis, IHRyAxis[ii]);
    grIHR2[ii]->SetMarkerStyle(24+ii);
    grIHR2[ii]->SetMarkerSize(2);
    grIHR2[ii]->SetLineWidth(3);
    
    if(ii!=4){
      grIHR2[ii]->SetMarkerColor(ii+1);
      grIHR2[ii]->SetLineColor(ii+1);
    }else{
      grIHR2[ii]->SetMarkerColor(ii+2);
      grIHR2[ii]->SetLineColor(ii+2);
    }
  }
  double xaxis1[2]={0, 91.5};
  double yaxis1[2]={-100, 100};
  TGraph *frame = new TGraph(2, xaxis1, yaxis1);
  frame->GetXaxis()->SetTitle("Module");
  frame->GetYaxis()->SetTitle("Module temperature [C]");
  frame->SetTitle("Module temperatures");

  string first_label[5]={"10C", "0C", "-10C", "-20C", "-28C"};
  string first_label2[3]={"20C", "10C", "-10C"};
  TLegend* leg_first = new TLegend(0.73,0.15,0.88,0.45);
  TLegend* leg_first2 = new TLegend(0.73,0.55,0.88,0.8);
  leg_first->SetHeader("#splitline{On M6}{Cooling pipe temp}");
  leg_first2->SetHeader("#splitline{Single test}{Cooling jig temp}");
  for(int ii=0;ii<5;ii++){
    leg_first->AddEntry(grIHR2[ii],first_label[ii].c_str(),"p");
  }
  for(int ii=0;ii<3;ii++){
    leg_first2->AddEntry(grSingle2[ii],first_label2[ii].c_str(),"p");
  }
  for(int ii=0;ii<5;ii++){
    frame->GetXaxis()->SetBinLabel(ii+1,modules[ii].c_str());
    //    frame->GetXaxis()->SetLabelSize(0.001);
  }
  frame->Draw("ap");
  frame->GetXaxis()->SetRangeUser(1, 7);
  //  frame->GetXaxis()->SetRangeUser(1, 11);
  // frame->GetYaxis()->SetRangeUser(20, 25);
  frame->GetYaxis()->SetRangeUser(-17, 30);

  for(int ii=0;ii<5;ii++){
    grIHR2[ii]->Draw("psame");
  }
  for(int ii=0;ii<3;ii++){
    grSingle2[ii]->Draw("psame");
  }
  leg_first->Draw();
  leg_first2->Draw();
  c1->SaveAs("M6_location.pdf");
  c1->Delete();
  TCanvas *c2 = new TCanvas("c2","", 1200, 800);
  double meansIHR[5];
  meansIHR[0]=returnMean(IHRtemp_10);
  meansIHR[1]=returnMean(IHRtemp_0);
  meansIHR[2]=returnMean(IHRtemp_m10);
  meansIHR[3]=returnMean(IHRtemp_m20);
  meansIHR[4]=returnMean(IHRtemp_m28);


  double meansSingle[3];
  meansSingle[0]=returnMean(Singletemp_20);
  meansSingle[1]=returnMean(Singletemp_10);
  meansSingle[2]=returnMean(Singletemp_m10);
  
  for(int ii=0;ii<5;ii++){
    data[ii].id=ii;
    data[ii].moduleName=modules[ii];
    data[ii].setModuleTempIHR(IHRtemp_10[ii],IHRtemp_0[ii],IHRtemp_m10[ii],IHRtemp_m20[ii],IHRtemp_m28[ii], meansIHR);
    data[ii].makeGraphIHR();
    
    if(ii==0){
      data[ii].grIHR->SetTitle("Module temperatures on M6");
      data[ii].grIHR->GetYaxis()->SetRangeUser(9, 18);
      data[ii].grIHR->GetYaxis()->SetRangeUser(-16, 25);
      data[ii].grIHR->Draw();
    }
    else{data[ii].grIHR->Draw("samepl");}
  }
  TLegend* leg_IHR = new TLegend(0.15,0.55,0.3,0.85);
  for(int ii=0;ii<5;ii++){
    leg_IHR->AddEntry(data[ii].grIHR,data[ii].moduleName.c_str(),"pl");
  }
  leg_IHR->Draw();
  c2->SaveAs("M6.pdf");
  TCanvas *c3 = new TCanvas("c3","", 1200, 800);
  for(int ii=0;ii<5;ii++){
    data[ii].setModuleTempSingle(Singletemp_20[ii], Singletemp_10[ii], Singletemp_m10[ii], meansSingle);
    data[ii].makeGraphSingle();
    // data[ii].grCompare->Draw();
    if(ii==0){
      cout<<"check"<<endl;
      data[ii].grSingle->SetTitle("Module temperatures of Single testing");
      data[ii].grSingle->GetYaxis()->SetRangeUser(4, 11);
      data[ii].grSingle->GetYaxis()->SetRangeUser(-7, 30);
      data[ii].grSingle->Draw("apl");
    }
    else{data[ii].grSingle->Draw("samepl");}
  }



  TLegend* leg_Single = new TLegend(0.15,0.55,0.3,0.85);
  for(int ii=0;ii<5;ii++){
    //    data[ii].makeThisGraph();		  
    leg_Single->AddEntry(data[ii].grSingle,data[ii].moduleName.c_str(),"pl");
  }
  leg_Single->Draw();
  c3->SaveAs("single.pdf");
  
  TCanvas *c4 = new TCanvas("c4","", 1200, 800);
  double xaxis[2]={-100, 100};
  TGraph *frame2 = new TGraph(2, xaxis, xaxis);
  //  frame2->SetTitle(moduleName.c_str());
  frame2->GetXaxis()->SetTitle("Single testing [C]");
  frame2->GetYaxis()->SetTitle("On M6 [C]");
  frame2->SetTitle("Module temperatures, Single testing vs On M6");
  frame2->Draw("ap");

  TLegend* leg_compare = new TLegend(0.15,0.55,0.3,0.85);
  
  frame2->GetXaxis()->SetRangeUser(-6, 22);
  frame2->GetYaxis()->SetRangeUser(0, 26);
  for(int ii=0;ii<5;ii++){
    data[ii].compareGraph();
    //data[ii].grCompare->Draw("apl");
    data[ii].grCompare->Draw("plsame");
    leg_compare->AddEntry(data[ii].grCompare, data[ii].moduleName.c_str(),"pl");
  }
  leg_compare->Draw(); 
  c4->SaveAs("single_m6.pdf");
}
