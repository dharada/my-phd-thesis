#include <iostream>
#include <fstream>
#include <nlohmann/json.hpp>
#include <tuple>
#include <math.h>

#include <TROOT.h>
#include <TGraph.h>
#include <TH1F.h>
#include <THStack.h>
#include <TStyle.h>
#include <TLegend.h>
#include <TCanvas.h>
#include <TGaxis.h>
#include <TAxis.h>

using namespace std;
using json = nlohmann::json;

class moduleTemp{
private:

public:
  string moduleName;
  int id;
  TGraph *grIHR;
  TGraph *grSingle;
  TGraph *grCompare;
  double coolingIHR[4]={10.6, 0.5, -9.35, -19.25};
  double moduleTempIHR[4];
  //  string modules[12] ={"Siegen3", "CERN11", "Paris9", "Siegen4", "Paris7", "Paris13", "Goe10", "CERN8", "Goe4", "CERN4", "KEK20", "Liv5"};
  double coolingSingle[12][3]={
    {20.3, 10.5, -10.5},
    {20.6, 10.7, -10.7},
    {20.5, 10.8, -10.6},
    {20.5, 10.8, -9.4},
    {20.6, 10.8, -10.5},
    {20.0, 10.4, -10.7},
    {20.3, 10.4, -9.5},
    {20.5, 10.4, -10.3},
    {20.3, 10.7, -9.4},
    {20.1, 10.7, -10.5},
    {20.5, 10.7, -9.4},
    {20.5, 10.5, -10.9}};
  //  double coolingSingle[3]={20, 10, -10};
  double moduleTempSingle[3];

  TGraph *gr[2];
  string retModuleName(){
    return moduleName;
  }
  void setModuleTempIHR(double temp10, double temp0, double tempm10, double tempm20, double tempm28, double *mean){
    moduleTempIHR[0]=temp10;
    moduleTempIHR[1]=temp0;
    moduleTempIHR[2]=tempm10;
    moduleTempIHR[3]=tempm20;
    //    moduleTempIHR[4]=tempm28;

    for(int ii=0;ii<5;ii++){
      //moduleTempIHR[ii] = moduleTempIHR[ii]-mean[ii];
      //      moduleTempIHR[ii]= moduleTempIHR[ii]-coolingIHR[ii];
    }
  }
  void setModuleTempSingle(double temp20, double temp10, double tempm10, double *mean){
    moduleTempSingle[0]=temp20;
    moduleTempSingle[1]=temp10;
    moduleTempSingle[2]=tempm10;

    for(int ii=0;ii<3;ii++){
      //moduleTempSingle[ii] = moduleTempSingle[ii]-mean[ii];
      //moduleTempSingle[ii]= moduleTempSingle[ii]-coolingSingle[id][ii];
    }

  }

  void makeThisGraph(){
    TCanvas *can = new TCanvas("can","", 1200, 800);
    double xaxis[2]={0, 11};
    
    TGraph *frame = new TGraph(2, xaxis, xaxis);
    frame->GetXaxis()->SetRangeUser(-30, 22);
    frame->GetYaxis()->SetRangeUser(-15, 30);
    frame->SetTitle(moduleName.c_str());
    frame->GetXaxis()->SetTitle("Cooling temperature [C]");
    frame->GetYaxis()->SetTitle("Module temperature [C]");

    frame->Draw("ap");
    gr[0] = new TGraph(5, coolingIHR, moduleTempIHR);
    gr[1] = new TGraph(3, coolingSingle[id], moduleTempSingle);

    gr[0]->SetMarkerStyle(8);
    
    gr[1]->SetLineStyle(4);
    gr[1]->SetLineColor(2);
    gr[1]->SetMarkerColor(2);
    gr[1]->SetMarkerStyle(8);
    
    gr[0]->Draw("plsame");
    gr[1]->Draw("plsame");

    TLegend* leg = new TLegend(0.15,0.65,0.4,0.85);
    leg->AddEntry(gr[0], "on IHR","pl");
    leg->AddEntry(gr[1], "Single cell setup","pl");
    leg->Draw();

    string saveName = moduleName+"_plot.pdf";
    can->SaveAs(saveName.c_str());
  }
    
    
  void makeGraphIHR(){
    grIHR=new TGraph(4, coolingIHR, moduleTempIHR);
    grIHR->SetMarkerStyle(24+id);
    grIHR->SetMarkerSize(2);
    if(id!=9&&id!=4){
      grIHR->SetMarkerColor(id+1);
      grIHR->SetLineColor(id+1);
    }else if(id==9){
      grIHR->SetMarkerColor(12);
      grIHR->SetLineColor(12);
    }else{
      grIHR->SetMarkerColor(13);
      grIHR->SetLineColor(13);
    }
    
    grIHR->SetLineWidth(3);
    grIHR->GetXaxis()->SetTitle("Cooling pipe temperature [C]");
    //        grIHR->GetYaxis()->SetTitle("delta T (Module temperature - cooling pipe)");
    grIHR->GetYaxis()->SetTitle("Module temperature [C]");
  }

  void makeGraphSingle(){
    grSingle=new TGraph(3, coolingSingle[id], moduleTempSingle);
    grSingle->SetMarkerStyle(24+id);
    grSingle->SetMarkerSize(2);
    if(id!=9&&id!=4){
      grSingle->SetMarkerColor(id+1);
      grSingle->SetLineColor(id+1);
    }else if(id==9){
      grSingle->SetMarkerColor(12);
      grSingle->SetLineColor(12);
    }else{
      grSingle->SetMarkerColor(13);
      grSingle->SetLineColor(13);
    }   
    grSingle->SetLineWidth(3);
    grSingle->GetXaxis()->SetTitle("Cooling jig temperature [C]");
    //    grSingle->GetYaxis()->SetTitle("delta T (Module temperature - cooling block)");
    grSingle->GetYaxis()->SetTitle("Module temperature [C]");
  }

  void compareGraph(){
    cout<<moduleName<<endl;
    double xAxis[2]={moduleTempSingle[1], moduleTempSingle[2]};
    double yAxis[2]={moduleTempIHR[0], moduleTempIHR[2]};
    cout<<xAxis[0]<<" "<<xAxis[1]<<endl;
    cout<<yAxis[0]<<" "<<yAxis[1]<<endl;
    
    grCompare = new TGraph(2, xAxis, yAxis);
    grCompare->SetMarkerStyle(24+id);
    grCompare->SetMarkerSize(2);
    if(id!=9&&id!=4){
      grCompare->SetMarkerColor(id+1);
      grCompare->SetLineColor(id+1);
    }else if(id==9){
      grCompare->SetMarkerColor(12);
      grCompare->SetLineColor(12);
    }else{
      grCompare->SetMarkerColor(13);
      grCompare->SetLineColor(13);
    }
    grCompare->SetLineWidth(3);
    grCompare->GetXaxis()->SetTitle("Single testing [C]");
    grCompare->GetYaxis()->SetTitle("On IHR [C]");
  }

  
  void checkData(){
    cout<<moduleName<<" ";
    for(int ii=0;ii<5;ii++){
      cout<<moduleTempIHR[ii]<<" ";
    }
    cout<<endl;
  }

};
  
double returnMean(double *data){
  double average=0;
  for(int ii=0;ii<12;ii++){
    average = average + data[ii]/11;
    cout<<data[ii]<<" ";
  }
  cout<<average<<endl;
  return average;
}

int main(){
  TCanvas *c1 = new TCanvas("c1","", 1200, 800);
  moduleTemp data[12];
  string modules[12] ={"Siegen3", "CERN11", "Paris9", "Siegen4", "Paris7", "Paris13", "Goe10", "CERN8", "Goe4", "CERN4", "KEK20", "Liv5"};

  double IHRtemp_10[12]={22.2, 21, 25.4, 21.6, 21.2, 22.5, 22.5, 22.5, 21, 22.5, 21.5, 22.4};
  double IHRtemp_0[12]={13, 12, 16.3, 12.5, 12, 13.3, 13.2, 13.2, 12, 12.8, 12.3, 12.65};
  double IHRtemp_m10[12]={4, 3.2, 7.5, 3.7, 3, 4.5, 4.5, 4.1, 3.1, 3.6, 3.3, 3.02};
  double IHRtemp_m20[12]={-5.2, -5.6, -1.5, -5.8, -5.8, -5.4, -4.4, -5.3, -5.6, -6, -5.6, -6.15};
  double IHRtemp_m28[12]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

  double Singletemp_20[12]= {26.7, 25.5, 30.0, 26.3, 26.3, 26.3, 27.1, 27.9, 26.3, 26.3, 25.7, 26.5};
  double Singletemp_10[12]= {17.1, 15.9, 20.0, 16.3, 16.6, 16.8, 17.2, 18.5, 16.9, 16.9, 15.9, 16.5};
  double Singletemp_m10[12]={-4.2, -5.4, -1.3, -3.9, -4.8, -4.7, -2.6, -2.0, -3.3, -5.0, -4.5, -5.0};

  
  double IHRyAxis[5][12];
  double singleyAxis[3][12];
  
  for(int ii=0;ii<12;ii++){
    IHRyAxis[0][ii]=IHRtemp_10[ii];
    IHRyAxis[1][ii]=IHRtemp_0[ii];
    IHRyAxis[2][ii]=IHRtemp_m10[ii];
    IHRyAxis[3][ii]=IHRtemp_m20[ii];
    IHRyAxis[4][ii]=IHRtemp_m28[ii];

    singleyAxis[0][ii]=Singletemp_20[ii];
    singleyAxis[1][ii]=Singletemp_10[ii];
    singleyAxis[2][ii]=Singletemp_m10[ii];	    
  }


  double IHRxAxis[12]={0.5, 1.5,2.5,3.5,4.5,5.5,6.5,7.5,8.5,9.5,10.5,11.5};
  TGraph *grIHR2[5];
  TGraph *grSingle2[3];
  for(int ii=0;ii<3;ii++){
    grSingle2[ii]=new TGraph(12,IHRxAxis, singleyAxis[ii]);
    grSingle2[ii]->SetMarkerSize(2);
    grSingle2[ii]->SetLineWidth(3);
  }
  grSingle2[0]->SetMarkerColor(7);
  grSingle2[0]->SetMarkerStyle(23);

  grSingle2[1]->SetMarkerColor(1);
  grSingle2[1]->SetMarkerStyle(20);

  grSingle2[2]->SetMarkerColor(3);
  grSingle2[2]->SetMarkerStyle(22);
  for(int ii=0;ii<5;ii++){
    grIHR2[ii]=new TGraph(12,IHRxAxis, IHRyAxis[ii]);
    grIHR2[ii]->SetMarkerStyle(24+ii);
    grIHR2[ii]->SetMarkerSize(2);
    grIHR2[ii]->SetLineWidth(3);
    
    if(ii!=4){
      grIHR2[ii]->SetMarkerColor(ii+1);
      grIHR2[ii]->SetLineColor(ii+1);
    }else{
      grIHR2[ii]->SetMarkerColor(ii+2);
      grIHR2[ii]->SetLineColor(ii+2);
    }
  }
  double xaxis1[2]={0, 91.5};
  double yaxis1[2]={-100, 100};
  TGraph *frame = new TGraph(2, xaxis1, yaxis1);
  frame->GetXaxis()->SetTitle("Module");
  frame->GetYaxis()->SetTitle("Module temperature [C]");
  frame->SetTitle("Module temperatures");

  string first_label[5]={"10C", "0C", "-10C", "-20C", "-28C"};
  string first_label2[3]={"20C", "10C", "-10C"};
  TLegend* leg_first = new TLegend(0.73,0.2,0.88,0.5);
  TLegend* leg_first2 = new TLegend(0.73,0.58,0.88,0.83);
  leg_first->SetHeader("#splitline{On M12}{Cooling pipe temp}");
  leg_first2->SetHeader("#splitline{Single test}{Cooling jig temp}");
  for(int ii=0;ii<4;ii++){
    leg_first->AddEntry(grIHR2[ii],first_label[ii].c_str(),"p");
  }
  for(int ii=0;ii<3;ii++){
    leg_first2->AddEntry(grSingle2[ii],first_label2[ii].c_str(),"p");
  }
  for(int ii=0;ii<12;ii++){
    frame->GetXaxis()->SetBinLabel(ii+1,modules[ii].c_str());
    //    frame->GetXaxis()->SetOffset(1);
    gPad->SetBottomMargin(0.17);
    frame->GetXaxis()->LabelsOption("v");
    frame->GetXaxis()->SetLabelSize(0.065);
    frame->GetYaxis()->SetLabelSize(0.04);
    frame->GetXaxis()->SetTitleSize(0.04);
    frame->GetYaxis()->SetTitleSize(0.04);
  }
  frame->Draw("ap");
  frame->GetXaxis()->SetRangeUser(1, 15);
  //  frame->GetXaxis()->SetRangeUser(1, 11);
  // frame->GetYaxis()->SetRangeUser(20, 25);
  frame->GetYaxis()->SetRangeUser(-17, 30);

  for(int ii=0;ii<4;ii++){
    grIHR2[ii]->Draw("psame");
  }
  for(int ii=0;ii<3;ii++){
    grSingle2[ii]->Draw("psame");
  }
  leg_first->Draw();
  leg_first2->Draw();
  c1->SaveAs("M12_location.pdf");
  c1->Delete();
  TCanvas *c2 = new TCanvas("c2","", 1200, 800);
  double meansIHR[5];
  meansIHR[0]=returnMean(IHRtemp_10);
  meansIHR[1]=returnMean(IHRtemp_0);
  meansIHR[2]=returnMean(IHRtemp_m10);
  meansIHR[3]=returnMean(IHRtemp_m20);
  meansIHR[4]=returnMean(IHRtemp_m28);


  double meansSingle[3];
  meansSingle[0]=returnMean(Singletemp_20);
  meansSingle[1]=returnMean(Singletemp_10);
  meansSingle[2]=returnMean(Singletemp_m10);
  
  for(int ii=0;ii<12;ii++){
    data[ii].id=ii;
    data[ii].moduleName=modules[ii];
    data[ii].setModuleTempIHR(IHRtemp_10[ii],IHRtemp_0[ii],IHRtemp_m10[ii],IHRtemp_m20[ii],IHRtemp_m28[ii], meansIHR);
    data[ii].makeGraphIHR();
    
    if(ii==0){
      data[ii].grIHR->SetTitle("Module temperatures on M12");
      data[ii].grIHR->GetYaxis()->SetRangeUser(9, 18);
      data[ii].grIHR->GetYaxis()->SetRangeUser(-10, 31);
      data[ii].grIHR->Draw();
    }
    else{data[ii].grIHR->Draw("samepl");}
  }
  TLegend* leg_IHR = new TLegend(0.15,0.53,0.3,0.88);
  for(int ii=0;ii<12;ii++){
    leg_IHR->AddEntry(data[ii].grIHR,data[ii].moduleName.c_str(),"pl");
  }
  leg_IHR->Draw();
  c2->SaveAs("M12.pdf");
  TCanvas *c3 = new TCanvas("c3","", 1200, 800);
  for(int ii=0;ii<12;ii++){
    data[ii].setModuleTempSingle(Singletemp_20[ii], Singletemp_10[ii], Singletemp_m10[ii], meansSingle);
    data[ii].makeGraphSingle();
    // data[ii].grCompare->Draw();
    if(ii==0){
      cout<<"check"<<endl;
      data[ii].grSingle->SetTitle("Module temperatures of Single testing");
      data[ii].grSingle->GetYaxis()->SetRangeUser(4, 11);
      data[ii].grSingle->GetYaxis()->SetRangeUser(-7, 31);
      data[ii].grSingle->Draw("apl");
    }
    else{data[ii].grSingle->Draw("samepl");}
  }



  TLegend* leg_Single = new TLegend(0.15,0.53,0.3,0.88);
  for(int ii=0;ii<12;ii++){
    //    data[ii].makeThisGraph();		  
    leg_Single->AddEntry(data[ii].grSingle,data[ii].moduleName.c_str(),"pl");
  }
  leg_Single->Draw();
  c3->SaveAs("single.pdf");
  
  TCanvas *c4 = new TCanvas("c4","", 1200, 800);
  double xaxis[2]={-100, 100};
  TGraph *frame2 = new TGraph(2, xaxis, xaxis);
  //  frame2->SetTitle(moduleName.c_str());
  frame2->GetXaxis()->SetTitle("Single testing [C]");
  frame2->GetYaxis()->SetTitle("On M12 [C]");
  frame2->SetTitle("Module temperatures, Single testing vs On M12");
  frame2->Draw("ap");
  
  frame2->GetXaxis()->SetRangeUser(-6, 21);
  frame2->GetYaxis()->SetRangeUser(0, 26);
  for(int ii=0;ii<12;ii++){
    data[ii].compareGraph();
    //data[ii].grCompare->Draw("apl");
    data[ii].grCompare->Draw("plsame");
  }
  leg_Single->Draw(); 
  c4->SaveAs("single_m12.pdf");
}
