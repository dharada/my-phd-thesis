#include <iostream>
#include <fstream>
#include <nlohmann/json.hpp>
#include <tuple>
#include <math.h>

#include <TROOT.h>
#include <TGraph.h>
#include <TH1F.h>
#include <THStack.h>
#include <TStyle.h>
#include <TLegend.h>
#include <TCanvas.h>
#include <TGaxis.h>
#include <TAxis.h>
using namespace std;
using json = nlohmann::json;

int main(){
    TCanvas *c1 = new TCanvas("c1","", 1200, 800);

  double voltages[5][13]={
    {20.355, 17.458, 15.857, 14.401, 12.763, 11.052, 9.56, 8.031, 6.466, 4.864, 3.263, 1.735,0},//-28
    {20.631, 17.749, 16.039, 14.473, 12.972, 11.198, 9.633, 8.213, 6.611, 4.974, 3.299, 1.697,0}, //-20
    {20.977, 17.968, 16.33, 14.801, 13.127, 11.452, 9.887, 8.213, 6.648, 4.974, 3.336, 1.735,0}, //-10
    {21.113, 18.004, 16.403, 14.837, 13.2, 11.562, 9.887, 8.213, 6.648, 4.974, 3.336, 1.697,0}, //0
    {21.225, 18.113, 16.475, 14.837, 13.2, 11.525, 9.887, 8.213, 6.611, 4.974, 3.299, 1.697,0}, //10
  };
  bool status[5][11]={
    {true, false, true, false, false, false, true, false, false, false, true},
    {true, false, true, true, false, false, true, true, true, false, true},
    {true, false, true, true, false, true, true, true, true, false, true},
    {true, false, true, true, true, true, true, true, true, false, true},
    {true, true, true, true, true, true, true, true, true, false, true}
  };
  string modules[11] ={"KEK22", "KEK24", "KEK25", "Liv8", "KEK19", "Paris6", "Goe5", "Paris16", "Paris11", "Goe7", "Paris8"};

  double temperature[5]={-28,-20,-10,0,10};
  double voltageDiff[5][11];
  for(int ii=0;ii<5;ii++){
    for(int jj=0;jj<11;jj++){
      voltageDiff[ii][jj]=voltages[ii][jj+1]-voltages[ii][jj+2];
    }
  }

  double IHRxAxis[11]={0.5, 1.5,2.5,3.5,4.5,5.5,6.5,7.5,8.5,9.5,10.5};
  TGraph *gr[5];
  for(int ii=0;ii<5;ii++){
    gr[ii]=new TGraph(11, IHRxAxis, voltageDiff[ii]);
  }


  double xaxis1[2]={0, 91.5};
  double yaxis1[2]={-100, 100};
  TGraph *frame = new TGraph(2, xaxis1, yaxis1);
  frame->GetXaxis()->SetTitle("Module");
  frame->GetYaxis()->SetTitle("Voltages [V]");
  frame->SetTitle("Module voltages at 4.6 A");
  for(int ii=0;ii<11;ii++){
    frame->GetXaxis()->SetBinLabel(ii+1,modules[ii].c_str());
    gPad->SetBottomMargin(0.17);
    frame->GetXaxis()->LabelsOption("v");
    frame->GetXaxis()->SetLabelSize(0.065);
    frame->GetYaxis()->SetLabelSize(0.04);
    frame->GetXaxis()->SetTitleSize(0.04);
    frame->GetYaxis()->SetTitleSize(0.04);
  }
  frame->Draw("ap");
  frame->GetXaxis()->SetRangeUser(1, 14);
  //  frame->GetXaxis()->SetRangeUser(1, 11);                                                                                   
  // frame->GetYaxis()->SetRangeUser(20, 25);                                                                                   
  frame->GetYaxis()->SetRangeUser(1.3, 1.8);
  for(int ii=0;ii<5;ii++){
    if(ii!=4){
      gr[ii]->SetMarkerStyle(ii+24);
      gr[ii]->SetMarkerColor(ii+1);
    }else{
      gr[ii]->SetMarkerStyle(28);
      gr[ii]->SetMarkerColor(6);
    }
    gr[ii]->SetMarkerSize(3);
    //    gr[ii]->Draw("psame");
  }
  TLegend* leg = new TLegend(0.73,0.45,0.88,0.85);
  leg->SetHeader("#splitline{Cooling pipe}{temperature}","C");
  leg->AddEntry(gr[0], "-28 #circC","p");
  leg->AddEntry(gr[1], "-20 #circC","p");
  leg->AddEntry(gr[2], "-10 #circC","p");
  leg->AddEntry(gr[3], "0 #circC","p");
  leg->AddEntry(gr[4], "10 #circC","p");
  leg->Draw();




  TGraph *gr2[5];
  double voltageDiff2[5][11];
  for(int ii=0;ii<5;ii++){
    for(int jj=0;jj<11;jj++){
      if(status[ii][jj]==true){
	voltageDiff2[ii][jj]=voltageDiff[ii][jj];
      }else{
	voltageDiff2[ii][jj]=-99;
      }
    }
  }
  for(int ii=0;ii<5;ii++){
    gr2[ii]=new TGraph(11, IHRxAxis, voltageDiff2[ii]);
  }
  for(int ii=0;ii<5;ii++){
    if(ii!=4){
      gr2[ii]->SetMarkerStyle(ii+20);
      gr2[ii]->SetMarkerColor(ii+1);
    }else{
      gr2[ii]->SetMarkerStyle(34);
      gr2[ii]->SetMarkerColor(6);
    }
    gr2[ii]->SetMarkerSize(3);
    gr2[ii]->Draw("psame");
    gr[ii]->Draw("psame");
  }
  TLegend* leg2 = new TLegend(0.73,0.25,0.88,0.4);
  leg2->SetHeader("Module status","C");
  leg2->AddEntry(gr2[0], "Configured","p");
  leg2->AddEntry(gr[0], "Not configured","p");
  leg2->Draw();
  c1->SaveAs("voltage.pdf");  
  
}
