#include <iostream>
#include <fstream>
#include <nlohmann/json.hpp>
#include <tuple>
#include <math.h>

#include <TROOT.h>
#include <TGraph.h>
#include <TH1F.h>
#include <THStack.h>
#include <TStyle.h>
#include <TLegend.h>
#include <TCanvas.h>
#include <TGaxis.h>
#include <TAxis.h>

using namespace std;
using json = nlohmann::json;

class moduleTemp{
private:

public:
  //  string modules[11] ={"KEK22", "KEK24", "KEK25", "Liv8", "KEK19", "Paris6", "Goe5", "Paris16", "Paris11", "Goe7", "Paris8"};
  string moduleName;
  int id;
  TGraph *grIHR;
  TGraph *grSingle;
  TGraph *grCompare;
  double coolingIHR[5]={10.55, 0.55, -9.35, -19.2, -26.9};
  double moduleTempIHR[5];

  double coolingSingle[11][3]={
    {20.5, 10.8, -10.5},
    {20.5, 10.4, -10.6},
    {20.5, 10.7, -10.6},
    {20.5, 10.8, -10.4},
    {20.6, 10.2, -10.5},
    {20.6, 10.7, -9.4},
    {20.5, 10.4, -11.0},
    {20.1, 10.7, -10.8},
    {20.1, 10.3, -10.8},
    {20.3, 10.7, -9.4},
    {20.5, 10.5, -9.5}};
  //  double coolingSingle[3]={20, 10, -10};
  double moduleTempSingle[3];

  TGraph *gr[2];
  string retModuleName(){
    return moduleName;
  }
  void setModuleTempIHR(double temp10, double temp0, double tempm10, double tempm20, double tempm28, double *mean){
    moduleTempIHR[0]=temp10;
    moduleTempIHR[1]=temp0;
    moduleTempIHR[2]=tempm10;
    moduleTempIHR[3]=tempm20;
    moduleTempIHR[4]=tempm28;

    for(int ii=0;ii<5;ii++){
      //moduleTempIHR[ii] = moduleTempIHR[ii]-mean[ii];
      //      moduleTempIHR[ii]= moduleTempIHR[ii]-coolingIHR[ii];
    }
  }
  void setModuleTempSingle(double temp20, double temp10, double tempm10, double *mean){
    moduleTempSingle[0]=temp20;
    moduleTempSingle[1]=temp10;
    moduleTempSingle[2]=tempm10;

    for(int ii=0;ii<3;ii++){
      //moduleTempSingle[ii] = moduleTempSingle[ii]-mean[ii];
      //moduleTempSingle[ii]= moduleTempSingle[ii]-coolingSingle[id][ii];
    }

  }

  void makeThisGraph(){
    TCanvas *can = new TCanvas("can","", 1200, 800);
    double xaxis[2]={0, 11};
    
    TGraph *frame = new TGraph(2, xaxis, xaxis);
    frame->GetXaxis()->SetRangeUser(-30, 22);
    frame->GetYaxis()->SetRangeUser(-15, 30);
    frame->SetTitle(moduleName.c_str());
    frame->GetXaxis()->SetTitle("Cooling temperature [#circC]");
    frame->GetYaxis()->SetTitle("Module temperature [#circC]");

    frame->Draw("ap");
    gr[0] = new TGraph(5, coolingIHR, moduleTempIHR);
    gr[1] = new TGraph(3, coolingSingle[id], moduleTempSingle);

    gr[0]->SetMarkerStyle(8);
    
    gr[1]->SetLineStyle(4);
    gr[1]->SetLineColor(2);
    gr[1]->SetMarkerColor(2);
    gr[1]->SetMarkerStyle(8);
    
    gr[0]->Draw("plsame");
    gr[1]->Draw("plsame");

    TLegend* leg = new TLegend(0.15,0.65,0.4,0.85);
    leg->AddEntry(gr[0], "on IHR","pl");
    leg->AddEntry(gr[1], "Single cell setup","pl");
    leg->Draw();

    string saveName = moduleName+"_plot.pdf";
    can->SaveAs(saveName.c_str());
  }
    
    
  void makeGraphIHR(){
    grIHR=new TGraph(5, coolingIHR, moduleTempIHR);
    grIHR->SetMarkerStyle(24+id);
    grIHR->SetMarkerSize(3);
    if(id!=9&&id!=4){
      grIHR->SetMarkerColor(id+1);
      grIHR->SetLineColor(id+1);
    }else if(id==9){
      grIHR->SetMarkerColor(12);
      grIHR->SetLineColor(12);
    }else{
      grIHR->SetMarkerColor(13);
      grIHR->SetLineColor(13);
    }
    
    grIHR->SetLineWidth(3);
    grIHR->GetXaxis()->SetTitle("Cooling pipe temperature [#circC]");
    //        grIHR->GetYaxis()->SetTitle("delta T (Module temperature - cooling pipe)");
    grIHR->GetYaxis()->SetTitle("Module temperature [#circC]");
  }

  void makeGraphSingle(){
    grSingle=new TGraph(3, coolingSingle[id], moduleTempSingle);
    grSingle->SetMarkerStyle(24+id);
    grSingle->SetMarkerSize(3);
    if(id!=9&&id!=4){
      grSingle->SetMarkerColor(id+1);
      grSingle->SetLineColor(id+1);
    }else if(id==9){
      grSingle->SetMarkerColor(12);
      grSingle->SetLineColor(12);
    }else{
      grSingle->SetMarkerColor(13);
      grSingle->SetLineColor(13);
    }   
    grSingle->SetLineWidth(3);
    grSingle->GetXaxis()->SetTitle("Cooling jig temperature [#circC]");
    //    grSingle->GetYaxis()->SetTitle("delta T (Module temperature - cooling block)");
    grSingle->GetYaxis()->SetTitle("Module temperature [#circC]");
  }

  void compareGraph(){
    cout<<moduleName<<endl;
    double xAxis[2]={moduleTempSingle[1], moduleTempSingle[2]};
    double yAxis[2]={moduleTempIHR[0], moduleTempIHR[2]};
    cout<<xAxis[0]<<" "<<xAxis[1]<<endl;
    cout<<yAxis[0]<<" "<<yAxis[1]<<endl;
    
    grCompare = new TGraph(2, xAxis, yAxis);
    grCompare->SetMarkerStyle(24+id);
    grCompare->SetMarkerSize(2);
    if(id!=9&&id!=4){
      grCompare->SetMarkerColor(id+1);
      grCompare->SetLineColor(id+1);
    }else if(id==9){
      grCompare->SetMarkerColor(12);
      grCompare->SetLineColor(12);
    }else{
      grCompare->SetMarkerColor(13);
      grCompare->SetLineColor(13);
    }
    grCompare->SetLineWidth(3);
    grCompare->GetXaxis()->SetTitle("Single testing [C]");
    grCompare->GetYaxis()->SetTitle("On IHR [C]");
  }

  
  void checkData(){
    cout<<moduleName<<" ";
    for(int ii=0;ii<5;ii++){
      cout<<moduleTempIHR[ii]<<" ";
    }
    cout<<endl;
  }

};
  
double returnMean(double *data){
  double average=0;
  for(int ii=0;ii<11;ii++){
    average = average + data[ii]/11;
    cout<<data[ii]<<" ";
  }
  cout<<average<<endl;
  return average;
}

int main(){
  TCanvas *c1 = new TCanvas("c1","", 1200, 800);
  c1->SetLeftMargin(0.12);
  TPad *pad1=new TPad("pad1", "pad1", 0, 0.45, 1, 1.0);
  pad1->Draw();
      pad1->cd();

  moduleTemp data[11];
  string modules[11] ={"KEK22", "KEK24", "KEK25", "Liv8", "KEK19", "Paris6", "Goe5", "Paris16", "Paris11", "Goe7", "Paris8"};

  double IHRtemp_10[11]={20.511,	21.754,	20.309,	24.423,	23.279,	22.399,	21.968,	23.279,	22.399,	21.544,	21.1};
  double IHRtemp_0[11]={10.525,	11.544,	10.099,	14.18,	13.064,	12.295,	11.842,	13.379,	12.448,	11.842,	11.28};
  double IHRtemp_m10[11]={1.438,	2.522,	0.701,	5.045,	3.988,	2.966,	2.632,	3.988,	3.078,	2.522,	2.01};
  double IHRtemp_m20[11]={-7.758,	-6.507,	-8.328,	-3.872,	-5.037,	-5.995,	-6.337,	-4.949,	-5.736,	-6.592, -7};
  double IHRtemp_m28[11]={-14.557,	-13.637,	-14.976,	-10.913,	-11.964,	-12.99,	-13.27,	-11.964,	-12.48,	-13.566, -13.97};

  double Singletemp_20[11]= {26.5, 26.7, 25.9, 29.4, 27.5, 26.9, 26.9, 28.7, 26.7, 26.1, 26.3};
  double Singletemp_10[11]= {16.8, 17.2, 16.3, 20.0, 16.9, 17.2, 17.1, 19.2, 16.8, 16.6, 16.3};
  double Singletemp_m10[11]={-4.8, -4.2, -5.0, -0.9, -4.0, -2.7, -4.6, -1.7, -4.6, -3.1, -3.9};

  
  double IHRyAxis[5][11];
  double singleyAxis[3][11];
  
  for(int ii=0;ii<11;ii++){
    IHRyAxis[0][ii]=IHRtemp_10[ii];
    IHRyAxis[1][ii]=IHRtemp_0[ii];
    IHRyAxis[2][ii]=IHRtemp_m10[ii];
    IHRyAxis[3][ii]=IHRtemp_m20[ii];
    IHRyAxis[4][ii]=IHRtemp_m28[ii];

    singleyAxis[0][ii]=Singletemp_20[ii];
    singleyAxis[1][ii]=Singletemp_10[ii];
    singleyAxis[2][ii]=Singletemp_m10[ii];	    
  }


  double IHRxAxis[11]={0.5, 1.5,2.5,3.5,4.5,5.5,6.5,7.5,8.5,9.5,10.5};
  TGraph *grIHR2[5];
  TGraph *grSingle2[3];
  for(int ii=0;ii<3;ii++){
    grSingle2[ii]=new TGraph(11,IHRxAxis, singleyAxis[ii]);
    grSingle2[ii]->SetMarkerSize(3);
    grSingle2[ii]->SetLineWidth(3);
  }
  grSingle2[0]->SetMarkerColor(7);
  grSingle2[0]->SetMarkerStyle(23);

  grSingle2[1]->SetMarkerColor(1);
  grSingle2[1]->SetMarkerStyle(20);

  grSingle2[2]->SetMarkerColor(3);
  grSingle2[2]->SetMarkerStyle(22);
  for(int ii=0;ii<5;ii++){
    grIHR2[ii]=new TGraph(11,IHRxAxis, IHRyAxis[ii]);
    grIHR2[ii]->SetMarkerStyle(24+ii);
    grIHR2[ii]->SetMarkerSize(3);
    grIHR2[ii]->SetLineWidth(3);
    
    if(ii!=4){
      grIHR2[ii]->SetMarkerColor(ii+1);
      grIHR2[ii]->SetLineColor(ii+1);
    }else{
      grIHR2[ii]->SetMarkerColor(ii+2);
      grIHR2[ii]->SetLineColor(ii+2);
    }
  }
  double xaxis1[2]={0, 91.5};
  double yaxis1[2]={-100, 100};
  TGraph *frame = new TGraph(2, xaxis1, yaxis1);
  frame->GetXaxis()->SetTitle("Module");
  frame->GetYaxis()->SetTitle("Module temperature [#circC]");
  frame->SetTitle("Module temperatures");

  string first_label[2]={"10C", "-10C"};
  string first_label2[2]={"10C", "-10C"};
  //  string first_label[5]={"10C", "0C", "-10C", "-20C", "-28C"};
  //  string first_label2[3]={"20C", "10C", "-10C"};
  TLegend* leg_first = new TLegend(0.73,0.2,0.88,0.45);
  TLegend* leg_first2 = new TLegend(0.73,0.5,0.88,0.83);
  leg_first->SetHeader("#splitline{On IHR}{Cooling pipe temp}");
  leg_first2->SetHeader("#splitline{Single test}{Cooling jig temp}");
  leg_first->AddEntry(grIHR2[0],"10 #circC","p");
  leg_first->AddEntry(grIHR2[2],"-10 #circC","p");
  leg_first2->AddEntry(grSingle2[1],"10 #circC","p");
  leg_first2->AddEntry(grSingle2[2],"-10 #circC","p");
  /*
  for(int ii=0;ii<5;ii++){
    leg_first->AddEntry(grIHR2[ii],first_label[ii].c_str(),"p");
  }
  for(int ii=0;ii<3;ii++){
    leg_first2->AddEntry(grSingle2[ii],first_label2[ii].c_str(),"p");
  }
  */
  pad1->SetLeftMargin(0.12);
  for(int ii=0;ii<11;ii++){
    frame->GetXaxis()->SetBinLabel(ii+1,modules[ii].c_str());
    gPad->SetBottomMargin(0.);
    frame->GetXaxis()->LabelsOption("v");
    frame->GetXaxis()->SetLabelSize(0.0);
    frame->GetYaxis()->SetLabelSize(0.08);
    frame->GetXaxis()->SetTitleSize(0.04);
    frame->GetYaxis()->SetTitleOffset(0.55);
    frame->GetYaxis()->SetTitleSize(0.08);
  }
  gPad->SetGrid(1, 1); gPad->Update();
  frame->Draw("ap");
  frame->GetXaxis()->SetRangeUser(1, 14);
  //  frame->GetXaxis()->SetRangeUser(1, 11);
  // frame->GetYaxis()->SetRangeUser(20, 25);
  frame->GetYaxis()->SetRangeUser(-12, 30);

  grIHR2[0]->Draw("psame");
  grIHR2[2]->Draw("psame");
  grSingle2[1]->Draw("psame");
  grSingle2[2]->Draw("psame"); //jump

  leg_first->Draw();
  leg_first2->Draw();
  c1->cd();
  TPad *pad2 = new TPad("pad2", "pad2", 0, 0., 1, 0.43);
  pad2->Draw();
  pad2->cd();
  pad2->SetTopMargin(0.);
  pad2->SetBottomMargin(0.4);
  pad2->SetLeftMargin(0.12);
  TGraph *frame3 = new TGraph(2, xaxis1, yaxis1);
  for(int ii=0;ii<11;ii++){
    frame3->GetXaxis()->SetBinLabel(ii+1,modules[ii].c_str());
    frame3->GetXaxis()->LabelsOption("v");
    frame3->GetXaxis()->SetLabelSize(0.15);
    frame3->GetYaxis()->SetLabelSize(0.1);
    frame3->GetYaxis()->SetTitleOffset(0.45);
    frame3->GetXaxis()->SetTitleSize(0.13);
    frame3->GetYaxis()->SetTitleSize(0.1);
  }
  gPad->SetGrid(1, 1); gPad->Update();
  frame3->GetXaxis()->SetRangeUser(1, 14);
  frame3->SetTitle("");
  frame3->GetYaxis()->SetRangeUser(2,9);
  frame3->GetYaxis()->SetTitle("#DeltaT [#circC]");
  
  frame3->Draw();
  double delta[2][11];
  for(int ii=0;ii<11;ii++){
    delta[0][ii]=IHRtemp_10[ii]-Singletemp_10[ii];
    delta[1][ii]=IHRtemp_m10[ii]-Singletemp_m10[ii];
  }
  TGraph *grD[2];
  grD[0]=new TGraph(11, IHRxAxis, delta[0]);
  grD[1]=new TGraph(11, IHRxAxis, delta[1]);
  grD[0]->SetMarkerColor(1);
  grD[0]->SetMarkerStyle(20);
  grD[0]->SetMarkerSize(3);

  grD[1]->SetMarkerColor(3);
  grD[1]->SetMarkerStyle(22);
  grD[1]->SetMarkerSize(3);
  grD[0]->Draw("psame");
  grD[1]->Draw("psame");
  TLegend* leg_lower = new TLegend(0.73,0.47,0.88,0.93);
  leg_lower->SetHeader("#splitline{#DeltaT}{(IHR-single)}");
  leg_lower->AddEntry(grD[0],"10 #circC","p");
  leg_lower->AddEntry(grD[1],"-10 #circC","p");
  leg_lower->Draw();
  c1->SaveAs("IHRlocation.pdf");



  
  TCanvas *c2 = new TCanvas("c2","", 1200, 800);
  double meansIHR[5];
  meansIHR[0]=returnMean(IHRtemp_10);
  meansIHR[1]=returnMean(IHRtemp_0);
  meansIHR[2]=returnMean(IHRtemp_m10);
  meansIHR[3]=returnMean(IHRtemp_m20);
  meansIHR[4]=returnMean(IHRtemp_m28);


  double meansSingle[3];
  meansSingle[0]=returnMean(Singletemp_20);
  meansSingle[1]=returnMean(Singletemp_10);
  meansSingle[2]=returnMean(Singletemp_m10);
  
  for(int ii=0;ii<11;ii++){
    data[ii].id=ii;
    data[ii].moduleName=modules[ii];
    data[ii].setModuleTempIHR(IHRtemp_10[ii],IHRtemp_0[ii],IHRtemp_m10[ii],IHRtemp_m20[ii],IHRtemp_m28[ii], meansIHR);
    data[ii].makeGraphIHR();
    
    if(ii==0){
      data[ii].grIHR->SetTitle("Module temperatures on IHR");
      data[ii].grIHR->GetYaxis()->SetRangeUser(9, 18);
      data[ii].grIHR->GetYaxis()->SetRangeUser(-16, 25);
      data[ii].grIHR->Draw();
    }
    else{data[ii].grIHR->Draw("samepl");}
  }
  TLegend* leg_IHR = new TLegend(0.15,0.4,0.3,0.85);
  for(int ii=0;ii<11;ii++){
    leg_IHR->AddEntry(data[ii].grIHR,data[ii].moduleName.c_str(),"pl");
  }
  leg_IHR->Draw();
  c2->SaveAs("ihr.pdf");
  TCanvas *c3 = new TCanvas("c3","", 1200, 800);
  for(int ii=0;ii<11;ii++){
    data[ii].setModuleTempSingle(Singletemp_20[ii], Singletemp_10[ii], Singletemp_m10[ii], meansSingle);
    data[ii].makeGraphSingle();
    // data[ii].grCompare->Draw();
    if(ii==0){
      cout<<"check"<<endl;
      data[ii].grSingle->SetTitle("Module temperatures of Single testing");
      data[ii].grSingle->GetYaxis()->SetRangeUser(4, 11);
      data[ii].grSingle->GetYaxis()->SetRangeUser(-7, 30);
      data[ii].grSingle->Draw("apl");
    }
    else{data[ii].grSingle->Draw("samepl");}
  }



  TLegend* leg_Single = new TLegend(0.15,0.42,0.3,0.87);
  for(int ii=0;ii<11;ii++){
    //    data[ii].makeThisGraph();		  
    leg_Single->AddEntry(data[ii].grSingle,data[ii].moduleName.c_str(),"pl");
  }
  leg_Single->Draw();
  c3->SaveAs("single.pdf");
  TCanvas *c4 = new TCanvas("c4","", 1200, 800);
  double xaxis[2]={-100, 100};
  TGraph *frame2 = new TGraph(2, xaxis, xaxis);
  //  frame->SetTitle(moduleName.c_str());
  frame2->GetXaxis()->SetTitle("Single testing [C]");
  frame2->GetYaxis()->SetTitle("On IHR [C]");
  frame2->SetTitle("Module temperatures, Single testing vs On IHR");
  frame2->Draw("ap");
  
  frame2->GetXaxis()->SetRangeUser(-6, 22);
  frame2->GetYaxis()->SetRangeUser(0, 26);
  for(int ii=0;ii<11;ii++){
    data[ii].compareGraph();
    //data[ii].grCompare->Draw("apl");
    data[ii].grCompare->Draw("plsame");
  }
  leg_Single->Draw(); 
  c4->SaveAs("single_ihr.pdf");
}
