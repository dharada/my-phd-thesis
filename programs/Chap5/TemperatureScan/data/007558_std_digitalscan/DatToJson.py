import os
import sys
import json
import numpy as np


def DatToJson(inputName):

    tmpFile = open(inputName)
    Lines = tmpFile.readlines()
    OutputJson={}

    Lines = [ line.replace("\n","") for line in Lines ]
    
    if "Histo2d" in Lines[0]:

        OutputJson["Type"] = Lines[0]
        OutputJson["Name"] = Lines[1]
        

        OutputJson["x"]={}
        OutputJson["y"]={}
        OutputJson["z"]={}
        
        OutputJson["x"]["AxisTitle"] = Lines[2]
        OutputJson["y"]["AxisTitle"] = Lines[3]
        OutputJson["z"]["AxisTitle"] = Lines[4]
        xBins,xLow,xHigh = Lines[5].split(" ")
        OutputJson["x"]["Bins"]=int(xBins)
        OutputJson["x"]["High"]=float(xHigh)
        OutputJson["x"]["Low"]=float(xLow)
        yBins,yLow,yHigh = Lines[6].split(" ")
        OutputJson["y"]["Bins"]=int(yBins)
        OutputJson["y"]["High"]=float(yHigh)
        OutputJson["y"]["Low"]=float(yLow)

        Overflow,Underflow = Lines[7].split(" ")

        OutputJson["Overflow"]=Overflow
        OutputJson["Underflow"]=Underflow
        
        OutArr=[]
        DataLine = Lines[8:]

        for Line in DataLine:
            Line = Line.strip()
            Line =Line.replace("\n","")
            Data= Line.split(" ")
            # print (Data)
            Data = [ float(item) for item in Data ]
            OutArr.append(Data)

        OutArr=np.array(OutArr)
        OutArr=np.transpose(OutArr)
        OutArr = OutArr.tolist()




        OutputJson["Data"]=OutArr

    elif "Histo1d" in Lines[0]:
        OutputJson["Type"] = Lines[0]
        OutputJson["Name"] = Lines[1]
        
        OutputJson["x"]={}
        OutputJson["y"]={}
        OutputJson["z"]={}
        
        OutputJson["x"]["AxisTitle"] = Lines[2]
        OutputJson["y"]["AxisTitle"] = Lines[3]
        OutputJson["z"]["AxisTitle"] = Lines[4]
        xBins,xLow,xHigh = Lines[5].split(" ")
        OutputJson["x"]["Bins"]=int(xBins)
        OutputJson["x"]["High"]=float(xHigh)
        OutputJson["x"]["Low"]=float(xLow)

        Overflow,Underflow = Lines[6].split(" ")

        OutputJson["Overflow"]=Overflow
        OutputJson["Underflow"]=Underflow
        
        OutArr=[]
        Line = Lines[7]

        
        Line = Line.strip()
        Line =Line.replace("\n","")
        Data= Line.split(" ")
        # print (Data)
        Data = [ float(item) for item in Data ]
        OutputJson["Data"]=Data
        
        
    return OutputJson

#Loop over all DatFiles in the given inputs
for DatFile in sys.argv[1:]:
    if not ".dat" in DatFile: continue
    
    newJSON = DatToJson(DatFile)
    njson = DatFile.replace(".dat",".json")    
    fJSON = open(njson,'w')
    json.dump(newJSON,fJSON,sort_keys=True)
