#include <fstream>
#include <nlohmann/json.hpp>
#include <iostream>
#include <vector>
#include <TCanvas.h>
#include <TGraph.h>
#include <TGraphErrors.h>
#include <TH1D.h>
#include <THStack.h>
#include <TF1.h>
#include <TLegend.h>
#include <TAxis.h>
#include <TLine.h>
using namespace std;

using json = nlohmann::json;

int main(){

  double data[4][4]={
    {754, 3709, 112.5, 511}, //volt, dac, volt, dac
    {759, 3698, 115.4, 521},
    {762, 3703, 113.3, 474},
    {756, 3727, 112.2, 526},
  };
  TGraph *gr[4];
  TF1* f[4];
  double xAxis[4][2]; //DAC
  double yAxis[4][2]; //Volt
  for(int ii=0;ii<4;ii++){
    xAxis[ii][0]=data[ii][1];
    xAxis[ii][1]=data[ii][3];
    yAxis[ii][0]=data[ii][0];
    yAxis[ii][1]=data[ii][2];
    gr[ii]=new TGraph(2, xAxis[ii], yAxis[ii]);
    gr[ii]->SetMarkerStyle(20+ii);
    gr[ii]->SetMarkerColor(1+ii);
    gr[ii]->SetLineColor(1+ii);
    gr[ii]->SetMarkerSize(4);
    f[ii]=new TF1("", "pol1",-100,4196);
    f[ii]->SetLineColor(ii+1);

    
    gr[ii]->Fit(f[ii]);

  };

  gr[0]->SetTitle("ADC calibration, KEKQ17");
  gr[0]->GetXaxis()->SetTitle("ADC output [LSB]");
  gr[0]->GetYaxis()->SetTitle("Probed voltage [mV]");
  
  gr[0]->GetXaxis()->SetTitleSize(0.06);
  gr[0]->GetYaxis()->SetTitleSize(0.06);
  gr[0]->GetXaxis()->SetLabelSize(0.05);
  gr[0]->GetYaxis()->SetLabelSize(0.05);
  gr[0]->GetXaxis()->SetRangeUser(0,4096);
  gr[0]->GetYaxis()->SetRangeUser(0,900);
  TCanvas *c=new TCanvas("", "",1200,800);
  c->SetLeftMargin(0.14);
  c->SetBottomMargin(0.14);
  gr[0]->Draw("apl");
  gr[1]->Draw("plsame");
  gr[3]->Draw("plsame");
  gr[2]->Draw("plsame");

  TLegend* leg1 = new TLegend(0.2,0.6,0.35,0.85);
  leg1->AddEntry(gr[0],"FE1","pl");
  leg1->AddEntry(gr[1],"FE2","pl");
  leg1->AddEntry(gr[2],"FE3","pl");
  leg1->AddEntry(gr[3],"FE4","pl");
  leg1->Draw();
  c->SaveAs("ADCcalibration.pdf");
}
