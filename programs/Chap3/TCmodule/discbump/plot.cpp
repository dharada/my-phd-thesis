#include <iostream>
#include <fstream>
#include <nlohmann/json.hpp>
#include <tuple>
#include <math.h>

#include <TROOT.h>
#include <TGraph.h>
#include <TH1F.h>
#include <THStack.h>
#include <TStyle.h>
#include <TLegend.h>
#include <TCanvas.h>
#include <TGaxis.h>
#include <TAxis.h>

using namespace std;
using json = nlohmann::json;

int main(){
  double discBump[7][5]={
    {0.773, 0.687, 1.052, 0.711, 0.723}, //KEKQ14
    {0.327, 0.307, 0.797, 0.295, 0.304}, // KEKQ17
    {0.298, 0.269, 1.323, 0.279, 0.300}, // KEKQ18
    {0.753, 0.733, 0.729, 0.728, 2.618}, //CERNQ3 Warm is not availabe at 100TC, cold one
    {0.681, 0.594, 0.583, 0.582, 0.587}, // CERNQ5
    {0.37, 0.359, 0.355, 0.346, 0.704}, //CERNQ6 lin is dead 34.179 if all inclusive
    {0.375, 0.385, 0.350, 0.386, 0.366} // CERNQ7
  };
  double xAxis[5]={0.5,1.5,2.5,3.5,4.5};

  TGraph *gr[7];
  for(int ii=0;ii<7;ii++){
    gr[ii]=new TGraph(5, xAxis, discBump[ii]);
    if(ii!=4){
    gr[ii]->SetLineColor(ii+1);
    gr[ii]->SetMarkerColor(ii+1);
    }else{
      gr[ii]->SetLineColor(8);
      gr[ii]->SetMarkerColor(8);
    }
    if(ii<=3){gr[ii]->SetMarkerStyle(20+ii);}
    else if(ii==4){gr[ii]->SetMarkerStyle(29);}
    else if(ii==5){gr[ii]->SetMarkerStyle(33);}
    else if(ii==6){gr[ii]->SetMarkerStyle(34);}
    gr[ii]->SetMarkerSize(4);
    gr[ii]->SetLineWidth(2);

  }

  string label[5]={"#splitline{Before}{thermal cycle}","#splitline{After}{25 cycles}","#splitline{After}{50 cycles}","#splitline{After}{75 cycles}","#splitline{After}{100 cycles}"};
  TCanvas *c1 = new TCanvas("c1", "",1200,800);
  c1->SetLeftMargin(0.12);
  c1->SetBottomMargin(0.24);
  for(int ii=0;ii<5;ii++){
    gr[0]->GetXaxis()->SetBinLabel(8.0+21*ii,label[ii].c_str());
  }
  gr[0]->SetTitle("Disconnected pixel development");
  gr[0]->GetYaxis()->SetTitle("Occupancy 0 pixel [%]");
  gr[0]->GetYaxis()->SetLabelSize(0.05);
  gr[0]->GetYaxis()->SetTitleSize(0.06);
  gr[0]->GetYaxis()->SetTitleOffset(0.8);
  gr[0]->GetXaxis()->SetLabelSize(0.06);
  gr[0]->GetYaxis()->SetRangeUser(0,3);
  gr[0]->Draw("apl");
  for(int ii=1;ii<7;ii++){
    gr[ii]->Draw("plsame");
  }

  TLegend* leg1 = new TLegend(0.15,0.48,0.35,0.88);
  leg1->AddEntry(gr[0],"KEKQ14","pl");
  leg1->AddEntry(gr[1],"KEKQ17","pl");
  leg1->AddEntry(gr[2],"KEKQ18","pl");
  leg1->AddEntry(gr[3],"CERNQ3","pl");
  leg1->AddEntry(gr[4],"CERNQ5","pl");
  leg1->AddEntry(gr[5],"CERNQ6","pl");
  leg1->AddEntry(gr[6],"CERNQ7","pl");
  leg1->Draw();
  
  c1->SaveAs("disconnected100tc.pdf");
}
