[15:57:40:229][  info  ][               ]: #####################################
[15:57:40:229][  info  ][               ]: # Welcome to the YARR Scan Console! #
[15:57:40:229][  info  ][               ]: #####################################
[15:57:40:229][  info  ][               ]: -> Parsing command line parameters ...
[15:57:40:229][  info  ][               ]: Configuring logger ...
[15:57:40:231][  info  ][  scanConsole  ]: Scan Type/Config configs/scans/rd53a/reg_readtemp.json
[15:57:40:231][  info  ][  scanConsole  ]: Connectivity:
[15:57:40:231][  info  ][  scanConsole  ]:     configs/KEKQ17/connectivity.json
[15:57:40:231][  info  ][  scanConsole  ]: Target ToT: -1
[15:57:40:231][  info  ][  scanConsole  ]: Target Charge: -1
[15:57:40:231][  info  ][  scanConsole  ]: Output Plots: true
[15:57:40:231][  info  ][  scanConsole  ]: Output Directory: ./data/001826_reg_readtemp/
[15:57:40:235][  info  ][  scanConsole  ]: Timestamp: 2022-08-25_15:57:40
[15:57:40:235][  info  ][  scanConsole  ]: Run Number: 1826
[15:57:40:235][  info  ][  scanConsole  ]: #################
[15:57:40:235][  info  ][  scanConsole  ]: # Init Hardware #
[15:57:40:235][  info  ][  scanConsole  ]: #################
[15:57:40:235][  info  ][  scanConsole  ]: -> Opening controller config: configs/controller/specCfg-rd53a-16x1.json
[15:57:40:235][  info  ][  ScanHelper   ]: Loading controller ...
[15:57:40:235][  info  ][  ScanHelper   ]: Found controller of type: spec
[15:57:40:235][  info  ][  ScanHelper   ]: ... loading controler config:
[15:57:40:235][  info  ][  ScanHelper   ]: ~~~ {
[15:57:40:235][  info  ][  ScanHelper   ]: ~~~     "cmdPeriod": 6.250000073038109e-9,
[15:57:40:235][  info  ][  ScanHelper   ]: ~~~     "idle": {
[15:57:40:235][  info  ][  ScanHelper   ]: ~~~         "word": 1768515945
[15:57:40:235][  info  ][  ScanHelper   ]: ~~~     },
[15:57:40:235][  info  ][  ScanHelper   ]: ~~~     "pulse": {
[15:57:40:235][  info  ][  ScanHelper   ]: ~~~         "interval": 500,
[15:57:40:235][  info  ][  ScanHelper   ]: ~~~         "word": 1549575846
[15:57:40:235][  info  ][  ScanHelper   ]: ~~~     },
[15:57:40:235][  info  ][  ScanHelper   ]: ~~~     "rxActiveLanes": 1,
[15:57:40:235][  info  ][  ScanHelper   ]: ~~~     "rxPolarity": 0,
[15:57:40:235][  info  ][  ScanHelper   ]: ~~~     "specNum": 0,
[15:57:40:235][  info  ][  ScanHelper   ]: ~~~     "spiConfig": 541200,
[15:57:40:235][  info  ][  ScanHelper   ]: ~~~     "sync": {
[15:57:40:235][  info  ][  ScanHelper   ]: ~~~         "interval": 32,
[15:57:40:235][  info  ][  ScanHelper   ]: ~~~         "word": 2172551550
[15:57:40:235][  info  ][  ScanHelper   ]: ~~~     },
[15:57:40:235][  info  ][  ScanHelper   ]: ~~~     "txPolarity": 0
[15:57:40:235][  info  ][  ScanHelper   ]: ~~~ }
[15:57:40:235][  info  ][    SpecCom    ]: Opening SPEC with id #0
[15:57:40:235][  info  ][    SpecCom    ]: Mapping BARs ...
[15:57:40:235][  info  ][    SpecCom    ]: ... Mapped BAR0 at 0x7f40b0e8f000 with size 1048576
[15:57:40:235][warning ][    SpecCom    ]: ... BAR4 not mapped (Mmap failed)
[15:57:40:235][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[15:57:40:235][  info  ][    SpecCom    ]: Firmware Version: 0x4d9ff6d
[15:57:40:235][  info  ][    SpecCom    ]: Firmware Identifier: 0x4030232
[15:57:40:235][  info  ][    SpecCom    ]: FPGA card: PLDA XpressK7 325
[15:57:40:235][  info  ][    SpecCom    ]: FE Chip Type: RD53A/B
[15:57:40:235][  info  ][    SpecCom    ]: FMC Card Type: Ohio Card (Display Port)
[15:57:40:235][  info  ][    SpecCom    ]: RX Speed: 640Mbps
[15:57:40:235][  info  ][    SpecCom    ]: Channel Configuration: 16x1
[15:57:40:235][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[15:57:40:235][  info  ][    SpecCom    ]: Flushing buffers ...
[15:57:40:235][  info  ][    SpecCom    ]: Init success!
[15:57:40:235][  info  ][  scanConsole  ]: #######################
[15:57:40:235][  info  ][  scanConsole  ]: ##  Loading Configs  ##
[15:57:40:235][  info  ][  scanConsole  ]: #######################
[15:57:40:235][  info  ][  scanConsole  ]: Opening global config: configs/KEKQ17/connectivity.json
[15:57:40:235][  info  ][  ScanHelper   ]: Chip type: RD53A
[15:57:40:235][  info  ][  ScanHelper   ]: Chip count 4
[15:57:40:235][  info  ][  ScanHelper   ]: Loading chip #0
[15:57:40:236][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(4)
[15:57:40:236][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ17/20UPGFC0013396.json
[15:57:40:410][  info  ][  ScanHelper   ]: Loading chip #1
[15:57:40:411][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(5)
[15:57:40:411][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ17/20UPGFC0013428.json
[15:57:40:585][  info  ][  ScanHelper   ]: Loading chip #2
[15:57:40:585][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(6)
[15:57:40:585][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ17/20UPGFC0013381.json
[15:57:40:760][  info  ][  ScanHelper   ]: Loading chip #3
[15:57:40:760][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(7)
[15:57:40:760][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ17/20UPGFC0013349.json
[15:57:40:938][  info  ][  scanConsole  ]: #################
[15:57:40:938][  info  ][  scanConsole  ]: # Configure FEs #
[15:57:40:938][  info  ][  scanConsole  ]: #################
[15:57:40:938][  info  ][  scanConsole  ]: Configuring 20UPGFC0013396
[15:57:40:968][  info  ][  scanConsole  ]: Configuring 20UPGFC0013428
[15:57:41:008][  info  ][  scanConsole  ]: Configuring 20UPGFC0013381
[15:57:41:039][  info  ][  scanConsole  ]: Configuring 20UPGFC0013349
[15:57:41:069][  info  ][  scanConsole  ]: Sent configuration to all FEs in 130 ms!
[15:57:41:070][  info  ][  scanConsole  ]: Checking com 20UPGFC0013396
[15:57:41:070][  info  ][    SpecRx     ]: Active Rx channels: 0x10
[15:57:41:070][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[15:57:41:070][  info  ][    SpecRx     ]: Rx Status 0xf0
[15:57:41:070][  info  ][    SpecRx     ]: Number of lanes: 1
[15:57:41:070][  info  ][    SpecRx     ]: Channel 4 Lane 0 synchronized!
[15:57:41:081][  info  ][  scanConsole  ]: ... success!
[15:57:41:081][  info  ][  scanConsole  ]: Checking com 20UPGFC0013428
[15:57:41:081][  info  ][    SpecRx     ]: Active Rx channels: 0x20
[15:57:41:081][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[15:57:41:081][  info  ][    SpecRx     ]: Rx Status 0xf0
[15:57:41:081][  info  ][    SpecRx     ]: Number of lanes: 1
[15:57:41:081][  info  ][    SpecRx     ]: Channel 5 Lane 0 synchronized!
[15:57:41:091][  info  ][  scanConsole  ]: ... success!
[15:57:41:091][  info  ][  scanConsole  ]: Checking com 20UPGFC0013381
[15:57:41:091][  info  ][    SpecRx     ]: Active Rx channels: 0x40
[15:57:41:091][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[15:57:41:091][  info  ][    SpecRx     ]: Rx Status 0xf0
[15:57:41:091][  info  ][    SpecRx     ]: Number of lanes: 1
[15:57:41:091][  info  ][    SpecRx     ]: Channel 6 Lane 0 synchronized!
[15:57:41:102][  info  ][  scanConsole  ]: ... success!
[15:57:41:102][  info  ][  scanConsole  ]: Checking com 20UPGFC0013349
[15:57:41:102][  info  ][    SpecRx     ]: Active Rx channels: 0x80
[15:57:41:102][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[15:57:41:102][  info  ][    SpecRx     ]: Rx Status 0xf0
[15:57:41:102][  info  ][    SpecRx     ]: Number of lanes: 1
[15:57:41:102][  info  ][    SpecRx     ]: Channel 7 Lane 0 synchronized!
[15:57:41:112][  info  ][  scanConsole  ]: ... success!
[15:57:41:112][  info  ][  scanConsole  ]: Enabling Tx channels
[15:57:41:112][  info  ][  scanConsole  ]: Enabling Tx channel 1
[15:57:41:112][  info  ][  scanConsole  ]: Enabling Tx channel 1
[15:57:41:112][  info  ][  scanConsole  ]: Enabling Tx channel 1
[15:57:41:112][  info  ][  scanConsole  ]: Enabling Tx channel 1
[15:57:41:112][  info  ][  scanConsole  ]: Enabling Rx channels
[15:57:41:112][  info  ][  scanConsole  ]: Enabling Rx channel 4
[15:57:41:112][  info  ][  scanConsole  ]: Enabling Rx channel 5
[15:57:41:112][  info  ][  scanConsole  ]: Enabling Rx channel 6
[15:57:41:112][  info  ][  scanConsole  ]: Enabling Rx channel 7
[15:57:41:112][  info  ][  scanConsole  ]: ##############
[15:57:41:113][  info  ][  scanConsole  ]: # Setup Scan #
[15:57:41:113][  info  ][  scanConsole  ]: ##############
[15:57:41:113][  info  ][  scanConsole  ]: Found Scan config, constructing scan ...
[15:57:41:113][  info  ][  ScanFactory  ]: Loading Scan:
[15:57:41:113][  info  ][  ScanFactory  ]:   Name: AnalogScan
[15:57:41:113][  info  ][  ScanFactory  ]:   Number of Loops: 3
[15:57:41:113][  info  ][  ScanFactory  ]:   Loading Loop #0
[15:57:41:113][  info  ][  ScanFactory  ]:    Type: Rd53aReadRegLoop
[15:57:41:114][  info  ][  ScanFactory  ]:    Loading loop config ... 
[15:57:41:114][  info  ][  ScanFactory  ]: ~~~ {
[15:57:41:114][  info  ][  ScanFactory  ]: ~~~     "CurMux": [],
[15:57:41:114][  info  ][  ScanFactory  ]: ~~~     "EnblRingOsc": 0,
[15:57:41:114][  info  ][  ScanFactory  ]: ~~~     "Registers": [],
[15:57:41:114][  info  ][  ScanFactory  ]: ~~~     "RingOscDur": 9,
[15:57:41:114][  info  ][  ScanFactory  ]: ~~~     "RingOscRep": 10,
[15:57:41:114][  info  ][  ScanFactory  ]: ~~~     "VoltMux": [3, 5, 7, 15]
[15:57:41:114][  info  ][  ScanFactory  ]: ~~~ }
[15:57:41:114][  info  ][  ScanFactory  ]:   Loading Loop #1
[15:57:41:114][  info  ][  ScanFactory  ]:    Type: Rd53aTriggerLoop
[15:57:41:114][  info  ][  ScanFactory  ]:    Loading loop config ... 
[15:57:41:114][  info  ][  ScanFactory  ]: ~~~ {
[15:57:41:114][  info  ][  ScanFactory  ]: ~~~     "count": 1,
[15:57:41:114][  info  ][  ScanFactory  ]: ~~~     "delay": 48,
[15:57:41:114][  info  ][  ScanFactory  ]: ~~~     "extTrig": false,
[15:57:41:114][  info  ][  ScanFactory  ]: ~~~     "frequency": 30000.0,
[15:57:41:114][  info  ][  ScanFactory  ]: ~~~     "noInject": false,
[15:57:41:114][  info  ][  ScanFactory  ]: ~~~     "sendEcr": false,
[15:57:41:114][  info  ][  ScanFactory  ]: ~~~     "time": 0.0,
[15:57:41:114][  info  ][  ScanFactory  ]: ~~~     "trigMultiplier": 16
[15:57:41:114][  info  ][  ScanFactory  ]: ~~~ }
[15:57:41:114][  info  ][  ScanFactory  ]:   Loading Loop #2
[15:57:41:114][  info  ][  ScanFactory  ]:    Type: StdDataLoop
[15:57:41:114][warning ][  ScanFactory  ]: ~~~ Config empty.
[15:57:41:114][  info  ][ScanBuildHistogrammers]: Loading histogrammer ...
[15:57:41:115][  info  ][ScanBuildHistogrammers]: ... done!
[15:57:41:115][  info  ][ScanBuildAnalyses]: Loading analyses ...
[15:57:41:115][  info  ][  scanConsole  ]: Running pre scan!
[15:57:41:115][  info  ][  ScanFactory  ]: Entering pre scan phase ...
[15:57:41:115][  info  ][     Rd53a     ]: Write named register: GlobalPulseRt -> 16384
[15:57:41:115][  info  ][     Rd53a     ]: Write named register: InjEnDig -> 0
[15:57:41:115][  info  ][     Rd53a     ]: Write named register: InjVcalDiff -> 0
[15:57:41:115][  info  ][     Rd53a     ]: Write named register: InjVcalHigh -> 3500
[15:57:41:115][  info  ][     Rd53a     ]: Write named register: InjVcalMed -> 500
[15:57:41:115][  info  ][     Rd53a     ]: Write named register: LatencyConfig -> 48
[15:57:41:115][  info  ][  scanConsole  ]: Starting histogrammer and analysis threads:
[15:57:41:116][  info  ][  scanConsole  ]:  .. started threads of Fe 4
[15:57:41:116][  info  ][  scanConsole  ]:  .. started threads of Fe 5
[15:57:41:116][  info  ][  scanConsole  ]:  .. started threads of Fe 6
[15:57:41:116][  info  ][  scanConsole  ]:  .. started threads of Fe 7
[15:57:41:116][  info  ][Rd53aDataProcessor]:   -> Processor thread #0 started!
[15:57:41:116][  info  ][Rd53aDataProcessor]:   -> Processor thread #1 started!
[15:57:41:116][  info  ][Rd53aDataProcessor]:   -> Processor thread #2 started!
[15:57:41:116][  info  ][Rd53aDataProcessor]:   -> Processor thread #3 started!
[15:57:41:116][  info  ][  scanConsole  ]: ########
[15:57:41:117][  info  ][  scanConsole  ]: # Scan #
[15:57:41:117][  info  ][  scanConsole  ]: ########
[15:57:41:117][  info  ][  scanConsole  ]: Starting scan!
[15:57:41:117][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0013396 on Rx 4
[15:57:41:121][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0013396] MON MUX_V: 3 Bias 0 1327 -> 0.276185 V Bias 1 1709 -> 0.352812 V, Temperature 0.0766269 -> -15.2225 C
[15:57:41:125][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0013396] MON MUX_V: 5 Bias 0 1296 -> 0.269966 V Bias 1 1669 -> 0.344788 V, Temperature 0.0748216 -> -16.6028 C
[15:57:41:129][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0013396] MON MUX_V: 7 Bias 0 1271 -> 0.264952 V Bias 1 1653 -> 0.341578 V, Temperature 0.076627 -> -16.981 C
[15:57:41:133][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0013396] MON MUX_V: 15 Bias 0 1279 -> 0.266556 V Bias 1 1669 -> 0.344788 V, Temperature 0.0782317 -> -14.5547 C
[15:57:41:168][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0013428 on Rx 5
[15:57:41:172][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0013428] MON MUX_V: 3 Bias 0 1302 -> 0.273616 V Bias 1 1667 -> 0.347558 V, Temperature 0.0739421 -> -18.5441 C
[15:57:41:177][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0013428] MON MUX_V: 5 Bias 0 1261 -> 0.26531 V Bias 1 1634 -> 0.340873 V, Temperature 0.0755627 -> -17.1993 C
[15:57:41:181][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0013428] MON MUX_V: 7 Bias 0 1262 -> 0.265513 V Bias 1 1634 -> 0.340873 V, Temperature 0.0753601 -> -19.6542 C
[15:57:41:185][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0013428] MON MUX_V: 15 Bias 0 1250 -> 0.263082 V Bias 1 1628 -> 0.339657 V, Temperature 0.0765756 -> -16.159 C
[15:57:41:220][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0013381 on Rx 6
[15:57:41:224][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0013381] MON MUX_V: 3 Bias 0 1298 -> 0.27884 V Bias 1 1662 -> 0.351967 V, Temperature 0.0731269 -> -19.2416 C
[15:57:41:228][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0013381] MON MUX_V: 5 Bias 0 1260 -> 0.271206 V Bias 1 1641 -> 0.347748 V, Temperature 0.0765422 -> -18.231 C
[15:57:41:232][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0013381] MON MUX_V: 7 Bias 0 1245 -> 0.268192 V Bias 1 1624 -> 0.344333 V, Temperature 0.0761404 -> -17.8323 C
[15:57:41:236][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0013381] MON MUX_V: 15 Bias 0 1219 -> 0.262969 V Bias 1 1602 -> 0.339913 V, Temperature 0.076944 -> -20.3323 C
[15:57:41:271][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0013349 on Rx 7
[15:57:41:276][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0013349] MON MUX_V: 3 Bias 0 1348 -> 0.277524 V Bias 1 1720 -> 0.352343 V, Temperature 0.0748184 -> -14.2732 C
[15:57:41:280][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0013349] MON MUX_V: 5 Bias 0 1313 -> 0.270485 V Bias 1 1686 -> 0.345505 V, Temperature 0.0750195 -> -16.0032 C
[15:57:41:284][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0013349] MON MUX_V: 7 Bias 0 1296 -> 0.267066 V Bias 1 1675 -> 0.343292 V, Temperature 0.0762262 -> -14.884 C
[15:57:41:288][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0013349] MON MUX_V: 15 Bias 0 1303 -> 0.268474 V Bias 1 1680 -> 0.344298 V, Temperature 0.075824 -> -14.4546 C
[15:57:41:325][  info  ][  scanConsole  ]: Scan done!
[15:57:41:325][  info  ][  scanConsole  ]: Waiting for processors to finish ...
[15:57:41:325][  info  ][  scanConsole  ]: Processor done, waiting for histogrammer ...
[15:57:41:725][  info  ][HistogramAlgorithm]: Histogrammer done!
[15:57:41:725][  info  ][HistogramAlgorithm]: Histogrammer done!
[15:57:41:725][  info  ][HistogramAlgorithm]: Histogrammer done!
[15:57:41:725][  info  ][HistogramAlgorithm]: Histogrammer done!
[15:57:41:725][  info  ][  scanConsole  ]: Processor done, waiting for analysis ...
[15:57:42:126][  info  ][AnalysisAlgorithm]: Analysis done!
[15:57:42:126][  info  ][AnalysisAlgorithm]: Analysis done!
[15:57:42:126][  info  ][AnalysisAlgorithm]: Analysis done!
[15:57:42:126][  info  ][AnalysisAlgorithm]: Analysis done!
[15:57:42:126][  info  ][  scanConsole  ]: All done!
[15:57:42:126][  info  ][  scanConsole  ]: ##########
[15:57:42:126][  info  ][  scanConsole  ]: # Timing #
[15:57:42:126][  info  ][  scanConsole  ]: ##########
[15:57:42:126][  info  ][  scanConsole  ]: -> Configuration: 130 ms
[15:57:42:126][  info  ][  scanConsole  ]: -> Scan:          208 ms
[15:57:42:126][  info  ][  scanConsole  ]: -> Processing:    0 ms
[15:57:42:126][  info  ][  scanConsole  ]: -> Analysis:      800 ms
[15:57:42:126][  info  ][  scanConsole  ]: ###########
[15:57:42:126][  info  ][  scanConsole  ]: # Cleanup #
[15:57:42:126][  info  ][  scanConsole  ]: ###########
[15:57:42:135][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0013396 to configs/KEKQ17/20UPGFC0013396.json
[15:57:42:358][  info  ][  scanConsole  ]: -> Storing output results of FE 4
[15:57:42:358][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0013396, this usually means that the chip did not send any data at all.
[15:57:42:360][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0013428 to configs/KEKQ17/20UPGFC0013428.json
[15:57:42:553][  info  ][  scanConsole  ]: -> Storing output results of FE 5
[15:57:42:553][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0013428, this usually means that the chip did not send any data at all.
[15:57:42:554][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0013381 to configs/KEKQ17/20UPGFC0013381.json
[15:57:42:732][  info  ][  scanConsole  ]: -> Storing output results of FE 6
[15:57:42:732][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0013381, this usually means that the chip did not send any data at all.
[15:57:42:733][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0013349 to configs/KEKQ17/20UPGFC0013349.json
[15:57:42:904][  info  ][  scanConsole  ]: -> Storing output results of FE 7
[15:57:42:904][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0013349, this usually means that the chip did not send any data at all.
[15:57:42:905][  info  ][  scanConsole  ]: Finishing run: 1826
20UPGFC0013349.json.after
20UPGFC0013349.json.before
20UPGFC0013381.json.after
20UPGFC0013381.json.before
20UPGFC0013396.json.after
20UPGFC0013396.json.before
20UPGFC0013428.json.after
20UPGFC0013428.json.before
reg_readtemp.json
scanLog.json

