[15:49:39:327][  info  ][               ]: #####################################
[15:49:39:327][  info  ][               ]: # Welcome to the YARR Scan Console! #
[15:49:39:327][  info  ][               ]: #####################################
[15:49:39:327][  info  ][               ]: -> Parsing command line parameters ...
[15:49:39:327][  info  ][               ]: Configuring logger ...
[15:49:39:330][  info  ][  scanConsole  ]: Scan Type/Config configs/scans/rd53a/reg_readtemp.json
[15:49:39:330][  info  ][  scanConsole  ]: Connectivity:
[15:49:39:330][  info  ][  scanConsole  ]:     configs/KEKQ17/connectivity.json
[15:49:39:330][  info  ][  scanConsole  ]: Target ToT: -1
[15:49:39:330][  info  ][  scanConsole  ]: Target Charge: -1
[15:49:39:330][  info  ][  scanConsole  ]: Output Plots: true
[15:49:39:330][  info  ][  scanConsole  ]: Output Directory: ./data/001823_reg_readtemp/
[15:49:39:336][  info  ][  scanConsole  ]: Timestamp: 2022-08-25_15:49:39
[15:49:39:336][  info  ][  scanConsole  ]: Run Number: 1823
[15:49:39:336][  info  ][  scanConsole  ]: #################
[15:49:39:336][  info  ][  scanConsole  ]: # Init Hardware #
[15:49:39:336][  info  ][  scanConsole  ]: #################
[15:49:39:336][  info  ][  scanConsole  ]: -> Opening controller config: configs/controller/specCfg-rd53a-16x1.json
[15:49:39:336][  info  ][  ScanHelper   ]: Loading controller ...
[15:49:39:336][  info  ][  ScanHelper   ]: Found controller of type: spec
[15:49:39:336][  info  ][  ScanHelper   ]: ... loading controler config:
[15:49:39:336][  info  ][  ScanHelper   ]: ~~~ {
[15:49:39:336][  info  ][  ScanHelper   ]: ~~~     "cmdPeriod": 6.250000073038109e-9,
[15:49:39:336][  info  ][  ScanHelper   ]: ~~~     "idle": {
[15:49:39:336][  info  ][  ScanHelper   ]: ~~~         "word": 1768515945
[15:49:39:336][  info  ][  ScanHelper   ]: ~~~     },
[15:49:39:336][  info  ][  ScanHelper   ]: ~~~     "pulse": {
[15:49:39:336][  info  ][  ScanHelper   ]: ~~~         "interval": 500,
[15:49:39:336][  info  ][  ScanHelper   ]: ~~~         "word": 1549575846
[15:49:39:336][  info  ][  ScanHelper   ]: ~~~     },
[15:49:39:336][  info  ][  ScanHelper   ]: ~~~     "rxActiveLanes": 1,
[15:49:39:336][  info  ][  ScanHelper   ]: ~~~     "rxPolarity": 0,
[15:49:39:336][  info  ][  ScanHelper   ]: ~~~     "specNum": 0,
[15:49:39:336][  info  ][  ScanHelper   ]: ~~~     "spiConfig": 541200,
[15:49:39:336][  info  ][  ScanHelper   ]: ~~~     "sync": {
[15:49:39:336][  info  ][  ScanHelper   ]: ~~~         "interval": 32,
[15:49:39:336][  info  ][  ScanHelper   ]: ~~~         "word": 2172551550
[15:49:39:336][  info  ][  ScanHelper   ]: ~~~     },
[15:49:39:336][  info  ][  ScanHelper   ]: ~~~     "txPolarity": 0
[15:49:39:336][  info  ][  ScanHelper   ]: ~~~ }
[15:49:39:336][  info  ][    SpecCom    ]: Opening SPEC with id #0
[15:49:39:337][  info  ][    SpecCom    ]: Mapping BARs ...
[15:49:39:337][  info  ][    SpecCom    ]: ... Mapped BAR0 at 0x7fad52860000 with size 1048576
[15:49:39:337][warning ][    SpecCom    ]: ... BAR4 not mapped (Mmap failed)
[15:49:39:337][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[15:49:39:337][  info  ][    SpecCom    ]: Firmware Version: 0x4d9ff6d
[15:49:39:337][  info  ][    SpecCom    ]: Firmware Identifier: 0x4030232
[15:49:39:337][  info  ][    SpecCom    ]: FPGA card: PLDA XpressK7 325
[15:49:39:337][  info  ][    SpecCom    ]: FE Chip Type: RD53A/B
[15:49:39:337][  info  ][    SpecCom    ]: FMC Card Type: Ohio Card (Display Port)
[15:49:39:337][  info  ][    SpecCom    ]: RX Speed: 640Mbps
[15:49:39:337][  info  ][    SpecCom    ]: Channel Configuration: 16x1
[15:49:39:337][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[15:49:39:337][  info  ][    SpecCom    ]: Flushing buffers ...
[15:49:39:337][  info  ][    SpecCom    ]: Init success!
[15:49:39:337][  info  ][  scanConsole  ]: #######################
[15:49:39:337][  info  ][  scanConsole  ]: ##  Loading Configs  ##
[15:49:39:337][  info  ][  scanConsole  ]: #######################
[15:49:39:337][  info  ][  scanConsole  ]: Opening global config: configs/KEKQ17/connectivity.json
[15:49:39:337][  info  ][  ScanHelper   ]: Chip type: RD53A
[15:49:39:337][  info  ][  ScanHelper   ]: Chip count 4
[15:49:39:337][  info  ][  ScanHelper   ]: Loading chip #0
[15:49:39:337][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(4)
[15:49:39:337][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ17/20UPGFC0013396.json
[15:49:39:514][  info  ][  ScanHelper   ]: Loading chip #1
[15:49:39:514][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(5)
[15:49:39:514][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ17/20UPGFC0013428.json
[15:49:39:689][  info  ][  ScanHelper   ]: Loading chip #2
[15:49:39:689][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(6)
[15:49:39:689][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ17/20UPGFC0013381.json
[15:49:39:865][  info  ][  ScanHelper   ]: Loading chip #3
[15:49:39:866][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(7)
[15:49:39:866][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ17/20UPGFC0013349.json
[15:49:40:043][  info  ][  scanConsole  ]: #################
[15:49:40:043][  info  ][  scanConsole  ]: # Configure FEs #
[15:49:40:043][  info  ][  scanConsole  ]: #################
[15:49:40:043][  info  ][  scanConsole  ]: Configuring 20UPGFC0013396
[15:49:40:074][  info  ][  scanConsole  ]: Configuring 20UPGFC0013428
[15:49:40:104][  info  ][  scanConsole  ]: Configuring 20UPGFC0013381
[15:49:40:135][  info  ][  scanConsole  ]: Configuring 20UPGFC0013349
[15:49:40:165][  info  ][  scanConsole  ]: Sent configuration to all FEs in 121 ms!
[15:49:40:166][  info  ][  scanConsole  ]: Checking com 20UPGFC0013396
[15:49:40:166][  info  ][    SpecRx     ]: Active Rx channels: 0x10
[15:49:40:166][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[15:49:40:166][  info  ][    SpecRx     ]: Rx Status 0xf0
[15:49:40:166][  info  ][    SpecRx     ]: Number of lanes: 1
[15:49:40:166][  info  ][    SpecRx     ]: Channel 4 Lane 0 synchronized!
[15:49:40:177][  info  ][  scanConsole  ]: ... success!
[15:49:40:177][  info  ][  scanConsole  ]: Checking com 20UPGFC0013428
[15:49:40:177][  info  ][    SpecRx     ]: Active Rx channels: 0x20
[15:49:40:177][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[15:49:40:177][  info  ][    SpecRx     ]: Rx Status 0xf0
[15:49:40:177][  info  ][    SpecRx     ]: Number of lanes: 1
[15:49:40:177][  info  ][    SpecRx     ]: Channel 5 Lane 0 synchronized!
[15:49:40:187][  info  ][  scanConsole  ]: ... success!
[15:49:40:187][  info  ][  scanConsole  ]: Checking com 20UPGFC0013381
[15:49:40:187][  info  ][    SpecRx     ]: Active Rx channels: 0x40
[15:49:40:187][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[15:49:40:187][  info  ][    SpecRx     ]: Rx Status 0xf0
[15:49:40:187][  info  ][    SpecRx     ]: Number of lanes: 1
[15:49:40:187][  info  ][    SpecRx     ]: Channel 6 Lane 0 synchronized!
[15:49:40:197][  info  ][  scanConsole  ]: ... success!
[15:49:40:197][  info  ][  scanConsole  ]: Checking com 20UPGFC0013349
[15:49:40:197][  info  ][    SpecRx     ]: Active Rx channels: 0x80
[15:49:40:197][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[15:49:40:197][  info  ][    SpecRx     ]: Rx Status 0xf0
[15:49:40:197][  info  ][    SpecRx     ]: Number of lanes: 1
[15:49:40:197][  info  ][    SpecRx     ]: Channel 7 Lane 0 synchronized!
[15:49:40:208][  info  ][  scanConsole  ]: ... success!
[15:49:40:208][  info  ][  scanConsole  ]: Enabling Tx channels
[15:49:40:208][  info  ][  scanConsole  ]: Enabling Tx channel 1
[15:49:40:208][  info  ][  scanConsole  ]: Enabling Tx channel 1
[15:49:40:208][  info  ][  scanConsole  ]: Enabling Tx channel 1
[15:49:40:208][  info  ][  scanConsole  ]: Enabling Tx channel 1
[15:49:40:208][  info  ][  scanConsole  ]: Enabling Rx channels
[15:49:40:208][  info  ][  scanConsole  ]: Enabling Rx channel 4
[15:49:40:208][  info  ][  scanConsole  ]: Enabling Rx channel 5
[15:49:40:208][  info  ][  scanConsole  ]: Enabling Rx channel 6
[15:49:40:208][  info  ][  scanConsole  ]: Enabling Rx channel 7
[15:49:40:208][  info  ][  scanConsole  ]: ##############
[15:49:40:208][  info  ][  scanConsole  ]: # Setup Scan #
[15:49:40:208][  info  ][  scanConsole  ]: ##############
[15:49:40:209][  info  ][  scanConsole  ]: Found Scan config, constructing scan ...
[15:49:40:209][  info  ][  ScanFactory  ]: Loading Scan:
[15:49:40:209][  info  ][  ScanFactory  ]:   Name: AnalogScan
[15:49:40:209][  info  ][  ScanFactory  ]:   Number of Loops: 3
[15:49:40:209][  info  ][  ScanFactory  ]:   Loading Loop #0
[15:49:40:209][  info  ][  ScanFactory  ]:    Type: Rd53aReadRegLoop
[15:49:40:209][  info  ][  ScanFactory  ]:    Loading loop config ... 
[15:49:40:209][  info  ][  ScanFactory  ]: ~~~ {
[15:49:40:209][  info  ][  ScanFactory  ]: ~~~     "CurMux": [],
[15:49:40:209][  info  ][  ScanFactory  ]: ~~~     "EnblRingOsc": 0,
[15:49:40:209][  info  ][  ScanFactory  ]: ~~~     "Registers": [],
[15:49:40:209][  info  ][  ScanFactory  ]: ~~~     "RingOscDur": 9,
[15:49:40:209][  info  ][  ScanFactory  ]: ~~~     "RingOscRep": 10,
[15:49:40:209][  info  ][  ScanFactory  ]: ~~~     "VoltMux": [3, 5, 7, 15]
[15:49:40:210][  info  ][  ScanFactory  ]: ~~~ }
[15:49:40:210][  info  ][  ScanFactory  ]:   Loading Loop #1
[15:49:40:210][  info  ][  ScanFactory  ]:    Type: Rd53aTriggerLoop
[15:49:40:210][  info  ][  ScanFactory  ]:    Loading loop config ... 
[15:49:40:210][  info  ][  ScanFactory  ]: ~~~ {
[15:49:40:210][  info  ][  ScanFactory  ]: ~~~     "count": 1,
[15:49:40:210][  info  ][  ScanFactory  ]: ~~~     "delay": 48,
[15:49:40:210][  info  ][  ScanFactory  ]: ~~~     "extTrig": false,
[15:49:40:210][  info  ][  ScanFactory  ]: ~~~     "frequency": 30000.0,
[15:49:40:210][  info  ][  ScanFactory  ]: ~~~     "noInject": false,
[15:49:40:210][  info  ][  ScanFactory  ]: ~~~     "sendEcr": false,
[15:49:40:210][  info  ][  ScanFactory  ]: ~~~     "time": 0.0,
[15:49:40:210][  info  ][  ScanFactory  ]: ~~~     "trigMultiplier": 16
[15:49:40:210][  info  ][  ScanFactory  ]: ~~~ }
[15:49:40:210][  info  ][  ScanFactory  ]:   Loading Loop #2
[15:49:40:210][  info  ][  ScanFactory  ]:    Type: StdDataLoop
[15:49:40:210][warning ][  ScanFactory  ]: ~~~ Config empty.
[15:49:40:210][  info  ][ScanBuildHistogrammers]: Loading histogrammer ...
[15:49:40:210][  info  ][ScanBuildHistogrammers]: ... done!
[15:49:40:210][  info  ][ScanBuildAnalyses]: Loading analyses ...
[15:49:40:211][  info  ][  scanConsole  ]: Running pre scan!
[15:49:40:211][  info  ][  ScanFactory  ]: Entering pre scan phase ...
[15:49:40:211][  info  ][     Rd53a     ]: Write named register: GlobalPulseRt -> 16384
[15:49:40:211][  info  ][     Rd53a     ]: Write named register: InjEnDig -> 0
[15:49:40:211][  info  ][     Rd53a     ]: Write named register: InjVcalDiff -> 0
[15:49:40:211][  info  ][     Rd53a     ]: Write named register: InjVcalHigh -> 3500
[15:49:40:211][  info  ][     Rd53a     ]: Write named register: InjVcalMed -> 500
[15:49:40:211][  info  ][     Rd53a     ]: Write named register: LatencyConfig -> 48
[15:49:40:211][  info  ][  scanConsole  ]: Starting histogrammer and analysis threads:
[15:49:40:211][  info  ][  scanConsole  ]:  .. started threads of Fe 4
[15:49:40:211][  info  ][  scanConsole  ]:  .. started threads of Fe 5
[15:49:40:211][  info  ][  scanConsole  ]:  .. started threads of Fe 6
[15:49:40:212][  info  ][  scanConsole  ]:  .. started threads of Fe 7
[15:49:40:212][  info  ][Rd53aDataProcessor]:   -> Processor thread #0 started!
[15:49:40:212][  info  ][Rd53aDataProcessor]:   -> Processor thread #1 started!
[15:49:40:212][  info  ][Rd53aDataProcessor]:   -> Processor thread #2 started!
[15:49:40:212][  info  ][Rd53aDataProcessor]:   -> Processor thread #3 started!
[15:49:40:212][  info  ][  scanConsole  ]: ########
[15:49:40:212][  info  ][  scanConsole  ]: # Scan #
[15:49:40:213][  info  ][  scanConsole  ]: ########
[15:49:40:213][  info  ][  scanConsole  ]: Starting scan!
[15:49:40:213][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0013396 on Rx 4
[15:49:40:217][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0013396] MON MUX_V: 3 Bias 0 1328 -> 0.276385 V Bias 1 1708 -> 0.352611 V, Temperature 0.0762258 -> -16.5727 C
[15:49:40:221][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0013396] MON MUX_V: 5 Bias 0 1296 -> 0.269966 V Bias 1 1669 -> 0.344788 V, Temperature 0.0748216 -> -16.6028 C
[15:49:40:225][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0013396] MON MUX_V: 7 Bias 0 1271 -> 0.264952 V Bias 1 1653 -> 0.341578 V, Temperature 0.076627 -> -16.981 C
[15:49:40:229][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0013396] MON MUX_V: 15 Bias 0 1280 -> 0.266757 V Bias 1 1669 -> 0.344788 V, Temperature 0.0780311 -> -15.2179 C
[15:49:40:264][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0013428 on Rx 5
[15:49:40:268][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0013428] MON MUX_V: 3 Bias 0 1304 -> 0.274021 V Bias 1 1666 -> 0.347355 V, Temperature 0.0733343 -> -20.6368 C
[15:49:40:272][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0013428] MON MUX_V: 5 Bias 0 1262 -> 0.265513 V Bias 1 1635 -> 0.341075 V, Temperature 0.0755627 -> -17.1994 C
[15:49:40:276][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0013428] MON MUX_V: 7 Bias 0 1261 -> 0.26531 V Bias 1 1635 -> 0.341075 V, Temperature 0.0757653 -> -18.2911 C
[15:49:40:280][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0013428] MON MUX_V: 15 Bias 0 1250 -> 0.263082 V Bias 1 1628 -> 0.339657 V, Temperature 0.0765756 -> -16.159 C
[15:49:40:315][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0013381 on Rx 6
[15:49:40:319][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0013381] MON MUX_V: 3 Bias 0 1299 -> 0.279041 V Bias 1 1662 -> 0.351967 V, Temperature 0.072926 -> -19.9392 C
[15:49:40:323][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0013381] MON MUX_V: 5 Bias 0 1261 -> 0.271407 V Bias 1 1641 -> 0.347748 V, Temperature 0.0763413 -> -18.9 C
[15:49:40:328][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0013381] MON MUX_V: 7 Bias 0 1246 -> 0.268393 V Bias 1 1625 -> 0.344534 V, Temperature 0.0761404 -> -17.8322 C
[15:49:40:332][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0013381] MON MUX_V: 15 Bias 0 1220 -> 0.26317 V Bias 1 1603 -> 0.340114 V, Temperature 0.076944 -> -20.3325 C
[15:49:40:367][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0013349 on Rx 7
[15:49:40:371][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0013349] MON MUX_V: 3 Bias 0 1349 -> 0.277726 V Bias 1 1719 -> 0.352142 V, Temperature 0.0744161 -> -15.6651 C
[15:49:40:375][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0013349] MON MUX_V: 5 Bias 0 1313 -> 0.270485 V Bias 1 1687 -> 0.345706 V, Temperature 0.0752206 -> -15.3138 C
[15:49:40:379][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0013349] MON MUX_V: 7 Bias 0 1296 -> 0.267066 V Bias 1 1675 -> 0.343292 V, Temperature 0.0762262 -> -14.884 C
[15:49:40:383][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0013349] MON MUX_V: 15 Bias 0 1305 -> 0.268876 V Bias 1 1681 -> 0.344499 V, Temperature 0.0756229 -> -15.1408 C
[15:49:40:420][  info  ][  scanConsole  ]: Scan done!
[15:49:40:420][  info  ][  scanConsole  ]: Waiting for processors to finish ...
[15:49:40:420][  info  ][  scanConsole  ]: Processor done, waiting for histogrammer ...
[15:49:40:820][  info  ][HistogramAlgorithm]: Histogrammer done!
[15:49:40:820][  info  ][HistogramAlgorithm]: Histogrammer done!
[15:49:40:820][  info  ][HistogramAlgorithm]: Histogrammer done!
[15:49:40:820][  info  ][HistogramAlgorithm]: Histogrammer done!
[15:49:40:820][  info  ][  scanConsole  ]: Processor done, waiting for analysis ...
[15:49:41:221][  info  ][AnalysisAlgorithm]: Analysis done!
[15:49:41:221][  info  ][AnalysisAlgorithm]: Analysis done!
[15:49:41:221][  info  ][AnalysisAlgorithm]: Analysis done!
[15:49:41:221][  info  ][AnalysisAlgorithm]: Analysis done!
[15:49:41:221][  info  ][  scanConsole  ]: All done!
[15:49:41:221][  info  ][  scanConsole  ]: ##########
[15:49:41:221][  info  ][  scanConsole  ]: # Timing #
[15:49:41:221][  info  ][  scanConsole  ]: ##########
[15:49:41:221][  info  ][  scanConsole  ]: -> Configuration: 121 ms
[15:49:41:221][  info  ][  scanConsole  ]: -> Scan:          207 ms
[15:49:41:221][  info  ][  scanConsole  ]: -> Processing:    0 ms
[15:49:41:221][  info  ][  scanConsole  ]: -> Analysis:      800 ms
[15:49:41:221][  info  ][  scanConsole  ]: ###########
[15:49:41:221][  info  ][  scanConsole  ]: # Cleanup #
[15:49:41:221][  info  ][  scanConsole  ]: ###########
[15:49:41:230][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0013396 to configs/KEKQ17/20UPGFC0013396.json
[15:49:41:397][  info  ][  scanConsole  ]: -> Storing output results of FE 4
[15:49:41:397][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0013396, this usually means that the chip did not send any data at all.
[15:49:41:398][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0013428 to configs/KEKQ17/20UPGFC0013428.json
[15:49:41:561][  info  ][  scanConsole  ]: -> Storing output results of FE 5
[15:49:41:561][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0013428, this usually means that the chip did not send any data at all.
[15:49:41:562][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0013381 to configs/KEKQ17/20UPGFC0013381.json
[15:49:41:724][  info  ][  scanConsole  ]: -> Storing output results of FE 6
[15:49:41:724][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0013381, this usually means that the chip did not send any data at all.
[15:49:41:726][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0013349 to configs/KEKQ17/20UPGFC0013349.json
[15:49:41:889][  info  ][  scanConsole  ]: -> Storing output results of FE 7
[15:49:41:889][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0013349, this usually means that the chip did not send any data at all.
[15:49:41:890][  info  ][  scanConsole  ]: Finishing run: 1823
20UPGFC0013349.json.after
20UPGFC0013349.json.before
20UPGFC0013381.json.after
20UPGFC0013381.json.before
20UPGFC0013396.json.after
20UPGFC0013396.json.before
20UPGFC0013428.json.after
20UPGFC0013428.json.before
reg_readtemp.json
scanLog.json

