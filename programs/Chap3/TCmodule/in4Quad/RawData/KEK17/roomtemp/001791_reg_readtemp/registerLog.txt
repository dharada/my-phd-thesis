[12:38:49:220][  info  ][               ]: #####################################
[12:38:49:220][  info  ][               ]: # Welcome to the YARR Scan Console! #
[12:38:49:220][  info  ][               ]: #####################################
[12:38:49:220][  info  ][               ]: -> Parsing command line parameters ...
[12:38:49:220][  info  ][               ]: Configuring logger ...
[12:38:49:222][  info  ][  scanConsole  ]: Scan Type/Config configs/scans/rd53a/reg_readtemp.json
[12:38:49:222][  info  ][  scanConsole  ]: Connectivity:
[12:38:49:222][  info  ][  scanConsole  ]:     configs/KEKQ17/connectivity.json
[12:38:49:222][  info  ][  scanConsole  ]: Target ToT: -1
[12:38:49:222][  info  ][  scanConsole  ]: Target Charge: -1
[12:38:49:222][  info  ][  scanConsole  ]: Output Plots: true
[12:38:49:222][  info  ][  scanConsole  ]: Output Directory: ./data/001791_reg_readtemp/
[12:38:49:226][  info  ][  scanConsole  ]: Timestamp: 2022-08-25_12:38:49
[12:38:49:226][  info  ][  scanConsole  ]: Run Number: 1791
[12:38:49:226][  info  ][  scanConsole  ]: #################
[12:38:49:226][  info  ][  scanConsole  ]: # Init Hardware #
[12:38:49:226][  info  ][  scanConsole  ]: #################
[12:38:49:226][  info  ][  scanConsole  ]: -> Opening controller config: configs/controller/specCfg-rd53a-16x1.json
[12:38:49:227][  info  ][  ScanHelper   ]: Loading controller ...
[12:38:49:227][  info  ][  ScanHelper   ]: Found controller of type: spec
[12:38:49:227][  info  ][  ScanHelper   ]: ... loading controler config:
[12:38:49:227][  info  ][  ScanHelper   ]: ~~~ {
[12:38:49:227][  info  ][  ScanHelper   ]: ~~~     "cmdPeriod": 6.250000073038109e-9,
[12:38:49:227][  info  ][  ScanHelper   ]: ~~~     "idle": {
[12:38:49:227][  info  ][  ScanHelper   ]: ~~~         "word": 1768515945
[12:38:49:227][  info  ][  ScanHelper   ]: ~~~     },
[12:38:49:227][  info  ][  ScanHelper   ]: ~~~     "pulse": {
[12:38:49:227][  info  ][  ScanHelper   ]: ~~~         "interval": 500,
[12:38:49:227][  info  ][  ScanHelper   ]: ~~~         "word": 1549575846
[12:38:49:227][  info  ][  ScanHelper   ]: ~~~     },
[12:38:49:227][  info  ][  ScanHelper   ]: ~~~     "rxActiveLanes": 1,
[12:38:49:227][  info  ][  ScanHelper   ]: ~~~     "rxPolarity": 0,
[12:38:49:227][  info  ][  ScanHelper   ]: ~~~     "specNum": 0,
[12:38:49:227][  info  ][  ScanHelper   ]: ~~~     "spiConfig": 541200,
[12:38:49:227][  info  ][  ScanHelper   ]: ~~~     "sync": {
[12:38:49:227][  info  ][  ScanHelper   ]: ~~~         "interval": 32,
[12:38:49:227][  info  ][  ScanHelper   ]: ~~~         "word": 2172551550
[12:38:49:227][  info  ][  ScanHelper   ]: ~~~     },
[12:38:49:227][  info  ][  ScanHelper   ]: ~~~     "txPolarity": 0
[12:38:49:227][  info  ][  ScanHelper   ]: ~~~ }
[12:38:49:227][  info  ][    SpecCom    ]: Opening SPEC with id #0
[12:38:49:227][  info  ][    SpecCom    ]: Mapping BARs ...
[12:38:49:227][  info  ][    SpecCom    ]: ... Mapped BAR0 at 0x7f8504a2d000 with size 1048576
[12:38:49:227][warning ][    SpecCom    ]: ... BAR4 not mapped (Mmap failed)
[12:38:49:227][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[12:38:49:227][  info  ][    SpecCom    ]: Firmware Version: 0x4d9ff6d
[12:38:49:227][  info  ][    SpecCom    ]: Firmware Identifier: 0x4030232
[12:38:49:227][  info  ][    SpecCom    ]: FPGA card: PLDA XpressK7 325
[12:38:49:227][  info  ][    SpecCom    ]: FE Chip Type: RD53A/B
[12:38:49:227][  info  ][    SpecCom    ]: FMC Card Type: Ohio Card (Display Port)
[12:38:49:227][  info  ][    SpecCom    ]: RX Speed: 640Mbps
[12:38:49:227][  info  ][    SpecCom    ]: Channel Configuration: 16x1
[12:38:49:227][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[12:38:49:227][  info  ][    SpecCom    ]: Flushing buffers ...
[12:38:49:227][  info  ][    SpecCom    ]: Init success!
[12:38:49:227][  info  ][  scanConsole  ]: #######################
[12:38:49:227][  info  ][  scanConsole  ]: ##  Loading Configs  ##
[12:38:49:227][  info  ][  scanConsole  ]: #######################
[12:38:49:227][  info  ][  scanConsole  ]: Opening global config: configs/KEKQ17/connectivity.json
[12:38:49:227][  info  ][  ScanHelper   ]: Chip type: RD53A
[12:38:49:227][  info  ][  ScanHelper   ]: Chip count 4
[12:38:49:227][  info  ][  ScanHelper   ]: Loading chip #0
[12:38:49:228][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(4)
[12:38:49:228][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ17/20UPGFC0013396.json
[12:38:49:403][  info  ][  ScanHelper   ]: Loading chip #1
[12:38:49:404][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(5)
[12:38:49:404][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ17/20UPGFC0013428.json
[12:38:49:580][  info  ][  ScanHelper   ]: Loading chip #2
[12:38:49:580][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(6)
[12:38:49:580][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ17/20UPGFC0013381.json
[12:38:49:759][  info  ][  ScanHelper   ]: Loading chip #3
[12:38:49:760][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(7)
[12:38:49:760][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ17/20UPGFC0013349.json
[12:38:49:942][  info  ][  scanConsole  ]: #################
[12:38:49:942][  info  ][  scanConsole  ]: # Configure FEs #
[12:38:49:942][  info  ][  scanConsole  ]: #################
[12:38:49:942][  info  ][  scanConsole  ]: Configuring 20UPGFC0013396
[12:38:49:972][  info  ][  scanConsole  ]: Configuring 20UPGFC0013428
[12:38:50:002][  info  ][  scanConsole  ]: Configuring 20UPGFC0013381
[12:38:50:033][  info  ][  scanConsole  ]: Configuring 20UPGFC0013349
[12:38:50:080][  info  ][  scanConsole  ]: Sent configuration to all FEs in 138 ms!
[12:38:50:081][  info  ][  scanConsole  ]: Checking com 20UPGFC0013396
[12:38:50:081][  info  ][    SpecRx     ]: Active Rx channels: 0x10
[12:38:50:081][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[12:38:50:081][  info  ][    SpecRx     ]: Rx Status 0xf0
[12:38:50:081][  info  ][    SpecRx     ]: Number of lanes: 1
[12:38:50:081][  info  ][    SpecRx     ]: Channel 4 Lane 0 synchronized!
[12:38:50:092][  info  ][  scanConsole  ]: ... success!
[12:38:50:092][  info  ][  scanConsole  ]: Checking com 20UPGFC0013428
[12:38:50:092][  info  ][    SpecRx     ]: Active Rx channels: 0x20
[12:38:50:092][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[12:38:50:092][  info  ][    SpecRx     ]: Rx Status 0xf0
[12:38:50:092][  info  ][    SpecRx     ]: Number of lanes: 1
[12:38:50:092][  info  ][    SpecRx     ]: Channel 5 Lane 0 synchronized!
[12:38:50:102][  info  ][  scanConsole  ]: ... success!
[12:38:50:103][  info  ][  scanConsole  ]: Checking com 20UPGFC0013381
[12:38:50:103][  info  ][    SpecRx     ]: Active Rx channels: 0x40
[12:38:50:103][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[12:38:50:103][  info  ][    SpecRx     ]: Rx Status 0xf0
[12:38:50:103][  info  ][    SpecRx     ]: Number of lanes: 1
[12:38:50:103][  info  ][    SpecRx     ]: Channel 6 Lane 0 synchronized!
[12:38:50:113][  info  ][  scanConsole  ]: ... success!
[12:38:50:113][  info  ][  scanConsole  ]: Checking com 20UPGFC0013349
[12:38:50:113][  info  ][    SpecRx     ]: Active Rx channels: 0x80
[12:38:50:113][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[12:38:50:113][  info  ][    SpecRx     ]: Rx Status 0xf0
[12:38:50:113][  info  ][    SpecRx     ]: Number of lanes: 1
[12:38:50:113][  info  ][    SpecRx     ]: Channel 7 Lane 0 synchronized!
[12:38:50:124][  info  ][  scanConsole  ]: ... success!
[12:38:50:124][  info  ][  scanConsole  ]: Enabling Tx channels
[12:38:50:124][  info  ][  scanConsole  ]: Enabling Tx channel 1
[12:38:50:124][  info  ][  scanConsole  ]: Enabling Tx channel 1
[12:38:50:124][  info  ][  scanConsole  ]: Enabling Tx channel 1
[12:38:50:124][  info  ][  scanConsole  ]: Enabling Tx channel 1
[12:38:50:124][  info  ][  scanConsole  ]: Enabling Rx channels
[12:38:50:124][  info  ][  scanConsole  ]: Enabling Rx channel 4
[12:38:50:124][  info  ][  scanConsole  ]: Enabling Rx channel 5
[12:38:50:124][  info  ][  scanConsole  ]: Enabling Rx channel 6
[12:38:50:124][  info  ][  scanConsole  ]: Enabling Rx channel 7
[12:38:50:124][  info  ][  scanConsole  ]: ##############
[12:38:50:124][  info  ][  scanConsole  ]: # Setup Scan #
[12:38:50:124][  info  ][  scanConsole  ]: ##############
[12:38:50:124][  info  ][  scanConsole  ]: Found Scan config, constructing scan ...
[12:38:50:125][  info  ][  ScanFactory  ]: Loading Scan:
[12:38:50:125][  info  ][  ScanFactory  ]:   Name: AnalogScan
[12:38:50:125][  info  ][  ScanFactory  ]:   Number of Loops: 3
[12:38:50:125][  info  ][  ScanFactory  ]:   Loading Loop #0
[12:38:50:125][  info  ][  ScanFactory  ]:    Type: Rd53aReadRegLoop
[12:38:50:125][  info  ][  ScanFactory  ]:    Loading loop config ... 
[12:38:50:125][  info  ][  ScanFactory  ]: ~~~ {
[12:38:50:125][  info  ][  ScanFactory  ]: ~~~     "CurMux": [],
[12:38:50:125][  info  ][  ScanFactory  ]: ~~~     "EnblRingOsc": 0,
[12:38:50:125][  info  ][  ScanFactory  ]: ~~~     "Registers": [],
[12:38:50:125][  info  ][  ScanFactory  ]: ~~~     "RingOscDur": 9,
[12:38:50:125][  info  ][  ScanFactory  ]: ~~~     "RingOscRep": 10,
[12:38:50:125][  info  ][  ScanFactory  ]: ~~~     "VoltMux": [3, 5, 7, 15]
[12:38:50:125][  info  ][  ScanFactory  ]: ~~~ }
[12:38:50:125][  info  ][  ScanFactory  ]:   Loading Loop #1
[12:38:50:125][  info  ][  ScanFactory  ]:    Type: Rd53aTriggerLoop
[12:38:50:125][  info  ][  ScanFactory  ]:    Loading loop config ... 
[12:38:50:126][  info  ][  ScanFactory  ]: ~~~ {
[12:38:50:126][  info  ][  ScanFactory  ]: ~~~     "count": 1,
[12:38:50:126][  info  ][  ScanFactory  ]: ~~~     "delay": 48,
[12:38:50:126][  info  ][  ScanFactory  ]: ~~~     "extTrig": false,
[12:38:50:126][  info  ][  ScanFactory  ]: ~~~     "frequency": 30000.0,
[12:38:50:126][  info  ][  ScanFactory  ]: ~~~     "noInject": false,
[12:38:50:126][  info  ][  ScanFactory  ]: ~~~     "sendEcr": false,
[12:38:50:126][  info  ][  ScanFactory  ]: ~~~     "time": 0.0,
[12:38:50:126][  info  ][  ScanFactory  ]: ~~~     "trigMultiplier": 16
[12:38:50:126][  info  ][  ScanFactory  ]: ~~~ }
[12:38:50:126][  info  ][  ScanFactory  ]:   Loading Loop #2
[12:38:50:126][  info  ][  ScanFactory  ]:    Type: StdDataLoop
[12:38:50:126][warning ][  ScanFactory  ]: ~~~ Config empty.
[12:38:50:126][  info  ][ScanBuildHistogrammers]: Loading histogrammer ...
[12:38:50:126][  info  ][ScanBuildHistogrammers]: ... done!
[12:38:50:126][  info  ][ScanBuildAnalyses]: Loading analyses ...
[12:38:50:127][  info  ][  scanConsole  ]: Running pre scan!
[12:38:50:127][  info  ][  ScanFactory  ]: Entering pre scan phase ...
[12:38:50:127][  info  ][     Rd53a     ]: Write named register: GlobalPulseRt -> 16384
[12:38:50:127][  info  ][     Rd53a     ]: Write named register: InjEnDig -> 0
[12:38:50:127][  info  ][     Rd53a     ]: Write named register: InjVcalDiff -> 0
[12:38:50:127][  info  ][     Rd53a     ]: Write named register: InjVcalHigh -> 3500
[12:38:50:127][  info  ][     Rd53a     ]: Write named register: InjVcalMed -> 500
[12:38:50:127][  info  ][     Rd53a     ]: Write named register: LatencyConfig -> 48
[12:38:50:127][  info  ][  scanConsole  ]: Starting histogrammer and analysis threads:
[12:38:50:127][  info  ][  scanConsole  ]:  .. started threads of Fe 4
[12:38:50:127][  info  ][  scanConsole  ]:  .. started threads of Fe 5
[12:38:50:127][  info  ][  scanConsole  ]:  .. started threads of Fe 6
[12:38:50:127][  info  ][  scanConsole  ]:  .. started threads of Fe 7
[12:38:50:128][  info  ][Rd53aDataProcessor]:   -> Processor thread #0 started!
[12:38:50:128][  info  ][Rd53aDataProcessor]:   -> Processor thread #1 started!
[12:38:50:128][  info  ][Rd53aDataProcessor]:   -> Processor thread #2 started!
[12:38:50:128][  info  ][Rd53aDataProcessor]:   -> Processor thread #3 started!
[12:38:50:128][  info  ][  scanConsole  ]: ########
[12:38:50:128][  info  ][  scanConsole  ]: # Scan #
[12:38:50:128][  info  ][  scanConsole  ]: ########
[12:38:50:128][  info  ][  scanConsole  ]: Starting scan!
[12:38:50:128][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0013396 on Rx 4
[12:38:50:132][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0013396] MON MUX_V: 3 Bias 0 1002 -> 0.210992 V Bias 1 1460 -> 0.302864 V, Temperature 0.0918721 -> 36.0931 C
[12:38:50:137][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0013396] MON MUX_V: 5 Bias 0 965 -> 0.20357 V Bias 1 1418 -> 0.294439 V, Temperature 0.0908691 -> 38.4208 C
[12:38:50:141][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0013396] MON MUX_V: 7 Bias 0 939 -> 0.198354 V Bias 1 1402 -> 0.291229 V, Temperature 0.0928751 -> 37.3376 C
[12:38:50:145][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0013396] MON MUX_V: 15 Bias 0 947 -> 0.199959 V Bias 1 1416 -> 0.294038 V, Temperature 0.0940787 -> 37.8274 C
[12:38:50:180][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0013428 on Rx 5
[12:38:50:184][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0013428] MON MUX_V: 3 Bias 0 992 -> 0.210816 V Bias 1 1438 -> 0.301167 V, Temperature 0.0903512 -> 37.9575 C
[12:38:50:189][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0013428] MON MUX_V: 5 Bias 0 943 -> 0.200889 V Bias 1 1400 -> 0.293469 V, Temperature 0.0925795 -> 40.441 C
[12:38:50:193][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0013428] MON MUX_V: 7 Bias 0 943 -> 0.200889 V Bias 1 1401 -> 0.293671 V, Temperature 0.0927821 -> 38.9499 C
[12:38:50:197][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0013428] MON MUX_V: 15 Bias 0 929 -> 0.198053 V Bias 1 1392 -> 0.291848 V, Temperature 0.093795 -> 41.6301 C
[12:38:50:232][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0013381 on Rx 6
[12:38:50:236][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0013381] MON MUX_V: 3 Bias 0 980 -> 0.214954 V Bias 1 1427 -> 0.304756 V, Temperature 0.0898015 -> 38.6551 C
[12:38:50:241][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0013381] MON MUX_V: 5 Bias 0 941 -> 0.207119 V Bias 1 1401 -> 0.299533 V, Temperature 0.0924131 -> 34.6262 C
[12:38:50:245][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0013381] MON MUX_V: 7 Bias 0 924 -> 0.203704 V Bias 1 1385 -> 0.296318 V, Temperature 0.092614 -> 37.408 C
[12:38:50:249][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0013381] MON MUX_V: 15 Bias 0 899 -> 0.198682 V Bias 1 1365 -> 0.2923 V, Temperature 0.0936185 -> 34.4557 C
[12:38:50:284][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0013349 on Rx 7
[12:38:50:288][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0013349] MON MUX_V: 3 Bias 0 1007 -> 0.208941 V Bias 1 1453 -> 0.298643 V, Temperature 0.0897016 -> 37.2238 C
[12:38:50:293][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0013349] MON MUX_V: 5 Bias 0 968 -> 0.201097 V Bias 1 1419 -> 0.291804 V, Temperature 0.0907072 -> 37.7701 C
[12:38:50:297][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0013349] MON MUX_V: 7 Bias 0 950 -> 0.197477 V Bias 1 1407 -> 0.289391 V, Temperature 0.091914 -> 38.2684 C
[12:38:50:302][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0013349] MON MUX_V: 15 Bias 0 953 -> 0.19808 V Bias 1 1408 -> 0.289592 V, Temperature 0.0915117 -> 39.0686 C
[12:38:50:338][  info  ][  scanConsole  ]: Scan done!
[12:38:50:338][  info  ][  scanConsole  ]: Waiting for processors to finish ...
[12:38:50:339][  info  ][  scanConsole  ]: Processor done, waiting for histogrammer ...
[12:38:50:739][  info  ][HistogramAlgorithm]: Histogrammer done!
[12:38:50:739][  info  ][HistogramAlgorithm]: Histogrammer done!
[12:38:50:739][  info  ][HistogramAlgorithm]: Histogrammer done!
[12:38:50:739][  info  ][HistogramAlgorithm]: Histogrammer done!
[12:38:50:739][  info  ][  scanConsole  ]: Processor done, waiting for analysis ...
[12:38:51:139][  info  ][AnalysisAlgorithm]: Analysis done!
[12:38:51:139][  info  ][AnalysisAlgorithm]: Analysis done!
[12:38:51:139][  info  ][AnalysisAlgorithm]: Analysis done!
[12:38:51:139][  info  ][AnalysisAlgorithm]: Analysis done!
[12:38:51:139][  info  ][  scanConsole  ]: All done!
[12:38:51:139][  info  ][  scanConsole  ]: ##########
[12:38:51:139][  info  ][  scanConsole  ]: # Timing #
[12:38:51:139][  info  ][  scanConsole  ]: ##########
[12:38:51:139][  info  ][  scanConsole  ]: -> Configuration: 138 ms
[12:38:51:139][  info  ][  scanConsole  ]: -> Scan:          210 ms
[12:38:51:139][  info  ][  scanConsole  ]: -> Processing:    0 ms
[12:38:51:139][  info  ][  scanConsole  ]: -> Analysis:      800 ms
[12:38:51:140][  info  ][  scanConsole  ]: ###########
[12:38:51:140][  info  ][  scanConsole  ]: # Cleanup #
[12:38:51:140][  info  ][  scanConsole  ]: ###########
[12:38:51:148][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0013396 to configs/KEKQ17/20UPGFC0013396.json
[12:38:51:345][  info  ][  scanConsole  ]: -> Storing output results of FE 4
[12:38:51:345][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0013396, this usually means that the chip did not send any data at all.
[12:38:51:346][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0013428 to configs/KEKQ17/20UPGFC0013428.json
[12:38:51:531][  info  ][  scanConsole  ]: -> Storing output results of FE 5
[12:38:51:531][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0013428, this usually means that the chip did not send any data at all.
[12:38:51:533][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0013381 to configs/KEKQ17/20UPGFC0013381.json
[12:38:51:716][  info  ][  scanConsole  ]: -> Storing output results of FE 6
[12:38:51:716][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0013381, this usually means that the chip did not send any data at all.
[12:38:51:717][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0013349 to configs/KEKQ17/20UPGFC0013349.json
[12:38:51:884][  info  ][  scanConsole  ]: -> Storing output results of FE 7
[12:38:51:884][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0013349, this usually means that the chip did not send any data at all.
[12:38:51:885][  info  ][  scanConsole  ]: Finishing run: 1791
20UPGFC0013349.json.after
20UPGFC0013349.json.before
20UPGFC0013381.json.after
20UPGFC0013381.json.before
20UPGFC0013396.json.after
20UPGFC0013396.json.before
20UPGFC0013428.json.after
20UPGFC0013428.json.before
reg_readtemp.json
scanLog.json

