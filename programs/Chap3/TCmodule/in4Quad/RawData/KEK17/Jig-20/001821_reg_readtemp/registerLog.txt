[15:34:03:061][  info  ][               ]: #####################################
[15:34:03:061][  info  ][               ]: # Welcome to the YARR Scan Console! #
[15:34:03:061][  info  ][               ]: #####################################
[15:34:03:061][  info  ][               ]: -> Parsing command line parameters ...
[15:34:03:061][  info  ][               ]: Configuring logger ...
[15:34:03:063][  info  ][  scanConsole  ]: Scan Type/Config configs/scans/rd53a/reg_readtemp.json
[15:34:03:063][  info  ][  scanConsole  ]: Connectivity:
[15:34:03:063][  info  ][  scanConsole  ]:     configs/KEKQ17/connectivity.json
[15:34:03:063][  info  ][  scanConsole  ]: Target ToT: -1
[15:34:03:063][  info  ][  scanConsole  ]: Target Charge: -1
[15:34:03:063][  info  ][  scanConsole  ]: Output Plots: true
[15:34:03:063][  info  ][  scanConsole  ]: Output Directory: ./data/001821_reg_readtemp/
[15:34:03:068][  info  ][  scanConsole  ]: Timestamp: 2022-08-25_15:34:03
[15:34:03:068][  info  ][  scanConsole  ]: Run Number: 1821
[15:34:03:068][  info  ][  scanConsole  ]: #################
[15:34:03:068][  info  ][  scanConsole  ]: # Init Hardware #
[15:34:03:068][  info  ][  scanConsole  ]: #################
[15:34:03:068][  info  ][  scanConsole  ]: -> Opening controller config: configs/controller/specCfg-rd53a-16x1.json
[15:34:03:068][  info  ][  ScanHelper   ]: Loading controller ...
[15:34:03:068][  info  ][  ScanHelper   ]: Found controller of type: spec
[15:34:03:068][  info  ][  ScanHelper   ]: ... loading controler config:
[15:34:03:069][  info  ][  ScanHelper   ]: ~~~ {
[15:34:03:069][  info  ][  ScanHelper   ]: ~~~     "cmdPeriod": 6.250000073038109e-9,
[15:34:03:069][  info  ][  ScanHelper   ]: ~~~     "idle": {
[15:34:03:069][  info  ][  ScanHelper   ]: ~~~         "word": 1768515945
[15:34:03:069][  info  ][  ScanHelper   ]: ~~~     },
[15:34:03:069][  info  ][  ScanHelper   ]: ~~~     "pulse": {
[15:34:03:069][  info  ][  ScanHelper   ]: ~~~         "interval": 500,
[15:34:03:069][  info  ][  ScanHelper   ]: ~~~         "word": 1549575846
[15:34:03:069][  info  ][  ScanHelper   ]: ~~~     },
[15:34:03:069][  info  ][  ScanHelper   ]: ~~~     "rxActiveLanes": 1,
[15:34:03:069][  info  ][  ScanHelper   ]: ~~~     "rxPolarity": 0,
[15:34:03:069][  info  ][  ScanHelper   ]: ~~~     "specNum": 0,
[15:34:03:069][  info  ][  ScanHelper   ]: ~~~     "spiConfig": 541200,
[15:34:03:069][  info  ][  ScanHelper   ]: ~~~     "sync": {
[15:34:03:069][  info  ][  ScanHelper   ]: ~~~         "interval": 32,
[15:34:03:069][  info  ][  ScanHelper   ]: ~~~         "word": 2172551550
[15:34:03:069][  info  ][  ScanHelper   ]: ~~~     },
[15:34:03:069][  info  ][  ScanHelper   ]: ~~~     "txPolarity": 0
[15:34:03:069][  info  ][  ScanHelper   ]: ~~~ }
[15:34:03:069][  info  ][    SpecCom    ]: Opening SPEC with id #0
[15:34:03:069][  info  ][    SpecCom    ]: Mapping BARs ...
[15:34:03:069][  info  ][    SpecCom    ]: ... Mapped BAR0 at 0x7f572de8b000 with size 1048576
[15:34:03:069][warning ][    SpecCom    ]: ... BAR4 not mapped (Mmap failed)
[15:34:03:069][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[15:34:03:069][  info  ][    SpecCom    ]: Firmware Version: 0x4d9ff6d
[15:34:03:069][  info  ][    SpecCom    ]: Firmware Identifier: 0x4030232
[15:34:03:069][  info  ][    SpecCom    ]: FPGA card: PLDA XpressK7 325
[15:34:03:069][  info  ][    SpecCom    ]: FE Chip Type: RD53A/B
[15:34:03:069][  info  ][    SpecCom    ]: FMC Card Type: Ohio Card (Display Port)
[15:34:03:069][  info  ][    SpecCom    ]: RX Speed: 640Mbps
[15:34:03:069][  info  ][    SpecCom    ]: Channel Configuration: 16x1
[15:34:03:069][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[15:34:03:069][  info  ][    SpecCom    ]: Flushing buffers ...
[15:34:03:069][  info  ][    SpecCom    ]: Init success!
[15:34:03:069][  info  ][  scanConsole  ]: #######################
[15:34:03:069][  info  ][  scanConsole  ]: ##  Loading Configs  ##
[15:34:03:069][  info  ][  scanConsole  ]: #######################
[15:34:03:069][  info  ][  scanConsole  ]: Opening global config: configs/KEKQ17/connectivity.json
[15:34:03:069][  info  ][  ScanHelper   ]: Chip type: RD53A
[15:34:03:069][  info  ][  ScanHelper   ]: Chip count 4
[15:34:03:069][  info  ][  ScanHelper   ]: Loading chip #0
[15:34:03:070][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(4)
[15:34:03:070][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ17/20UPGFC0013396.json
[15:34:03:247][  info  ][  ScanHelper   ]: Loading chip #1
[15:34:03:247][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(5)
[15:34:03:247][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ17/20UPGFC0013428.json
[15:34:03:422][  info  ][  ScanHelper   ]: Loading chip #2
[15:34:03:423][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(6)
[15:34:03:423][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ17/20UPGFC0013381.json
[15:34:03:598][  info  ][  ScanHelper   ]: Loading chip #3
[15:34:03:599][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(7)
[15:34:03:599][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ17/20UPGFC0013349.json
[15:34:03:777][  info  ][  scanConsole  ]: #################
[15:34:03:777][  info  ][  scanConsole  ]: # Configure FEs #
[15:34:03:777][  info  ][  scanConsole  ]: #################
[15:34:03:777][  info  ][  scanConsole  ]: Configuring 20UPGFC0013396
[15:34:03:808][  info  ][  scanConsole  ]: Configuring 20UPGFC0013428
[15:34:03:838][  info  ][  scanConsole  ]: Configuring 20UPGFC0013381
[15:34:03:875][  info  ][  scanConsole  ]: Configuring 20UPGFC0013349
[15:34:03:924][  info  ][  scanConsole  ]: Sent configuration to all FEs in 146 ms!
[15:34:03:925][  info  ][  scanConsole  ]: Checking com 20UPGFC0013396
[15:34:03:925][  info  ][    SpecRx     ]: Active Rx channels: 0x10
[15:34:03:925][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[15:34:03:925][  info  ][    SpecRx     ]: Rx Status 0xf0
[15:34:03:925][  info  ][    SpecRx     ]: Number of lanes: 1
[15:34:03:925][  info  ][    SpecRx     ]: Channel 4 Lane 0 synchronized!
[15:34:03:936][  info  ][  scanConsole  ]: ... success!
[15:34:03:936][  info  ][  scanConsole  ]: Checking com 20UPGFC0013428
[15:34:03:936][  info  ][    SpecRx     ]: Active Rx channels: 0x20
[15:34:03:936][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[15:34:03:936][  info  ][    SpecRx     ]: Rx Status 0xf0
[15:34:03:936][  info  ][    SpecRx     ]: Number of lanes: 1
[15:34:03:936][  info  ][    SpecRx     ]: Channel 5 Lane 0 synchronized!
[15:34:03:946][  info  ][  scanConsole  ]: ... success!
[15:34:03:946][  info  ][  scanConsole  ]: Checking com 20UPGFC0013381
[15:34:03:947][  info  ][    SpecRx     ]: Active Rx channels: 0x40
[15:34:03:947][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[15:34:03:947][  info  ][    SpecRx     ]: Rx Status 0xf0
[15:34:03:947][  info  ][    SpecRx     ]: Number of lanes: 1
[15:34:03:947][  info  ][    SpecRx     ]: Channel 6 Lane 0 synchronized!
[15:34:03:957][  info  ][  scanConsole  ]: ... success!
[15:34:03:957][  info  ][  scanConsole  ]: Checking com 20UPGFC0013349
[15:34:03:957][  info  ][    SpecRx     ]: Active Rx channels: 0x80
[15:34:03:957][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[15:34:03:957][  info  ][    SpecRx     ]: Rx Status 0xf0
[15:34:03:957][  info  ][    SpecRx     ]: Number of lanes: 1
[15:34:03:957][  info  ][    SpecRx     ]: Channel 7 Lane 0 synchronized!
[15:34:03:968][  info  ][  scanConsole  ]: ... success!
[15:34:03:968][  info  ][  scanConsole  ]: Enabling Tx channels
[15:34:03:968][  info  ][  scanConsole  ]: Enabling Tx channel 1
[15:34:03:968][  info  ][  scanConsole  ]: Enabling Tx channel 1
[15:34:03:968][  info  ][  scanConsole  ]: Enabling Tx channel 1
[15:34:03:968][  info  ][  scanConsole  ]: Enabling Tx channel 1
[15:34:03:968][  info  ][  scanConsole  ]: Enabling Rx channels
[15:34:03:968][  info  ][  scanConsole  ]: Enabling Rx channel 4
[15:34:03:968][  info  ][  scanConsole  ]: Enabling Rx channel 5
[15:34:03:968][  info  ][  scanConsole  ]: Enabling Rx channel 6
[15:34:03:968][  info  ][  scanConsole  ]: Enabling Rx channel 7
[15:34:03:968][  info  ][  scanConsole  ]: ##############
[15:34:03:968][  info  ][  scanConsole  ]: # Setup Scan #
[15:34:03:968][  info  ][  scanConsole  ]: ##############
[15:34:03:968][  info  ][  scanConsole  ]: Found Scan config, constructing scan ...
[15:34:03:969][  info  ][  ScanFactory  ]: Loading Scan:
[15:34:03:969][  info  ][  ScanFactory  ]:   Name: AnalogScan
[15:34:03:969][  info  ][  ScanFactory  ]:   Number of Loops: 3
[15:34:03:969][  info  ][  ScanFactory  ]:   Loading Loop #0
[15:34:03:969][  info  ][  ScanFactory  ]:    Type: Rd53aReadRegLoop
[15:34:03:969][  info  ][  ScanFactory  ]:    Loading loop config ... 
[15:34:03:969][  info  ][  ScanFactory  ]: ~~~ {
[15:34:03:969][  info  ][  ScanFactory  ]: ~~~     "CurMux": [],
[15:34:03:969][  info  ][  ScanFactory  ]: ~~~     "EnblRingOsc": 0,
[15:34:03:969][  info  ][  ScanFactory  ]: ~~~     "Registers": [],
[15:34:03:969][  info  ][  ScanFactory  ]: ~~~     "RingOscDur": 9,
[15:34:03:969][  info  ][  ScanFactory  ]: ~~~     "RingOscRep": 10,
[15:34:03:969][  info  ][  ScanFactory  ]: ~~~     "VoltMux": [3, 5, 7, 15]
[15:34:03:969][  info  ][  ScanFactory  ]: ~~~ }
[15:34:03:969][  info  ][  ScanFactory  ]:   Loading Loop #1
[15:34:03:969][  info  ][  ScanFactory  ]:    Type: Rd53aTriggerLoop
[15:34:03:969][  info  ][  ScanFactory  ]:    Loading loop config ... 
[15:34:03:969][  info  ][  ScanFactory  ]: ~~~ {
[15:34:03:969][  info  ][  ScanFactory  ]: ~~~     "count": 1,
[15:34:03:969][  info  ][  ScanFactory  ]: ~~~     "delay": 48,
[15:34:03:969][  info  ][  ScanFactory  ]: ~~~     "extTrig": false,
[15:34:03:969][  info  ][  ScanFactory  ]: ~~~     "frequency": 30000.0,
[15:34:03:969][  info  ][  ScanFactory  ]: ~~~     "noInject": false,
[15:34:03:969][  info  ][  ScanFactory  ]: ~~~     "sendEcr": false,
[15:34:03:969][  info  ][  ScanFactory  ]: ~~~     "time": 0.0,
[15:34:03:970][  info  ][  ScanFactory  ]: ~~~     "trigMultiplier": 16
[15:34:03:970][  info  ][  ScanFactory  ]: ~~~ }
[15:34:03:970][  info  ][  ScanFactory  ]:   Loading Loop #2
[15:34:03:970][  info  ][  ScanFactory  ]:    Type: StdDataLoop
[15:34:03:970][warning ][  ScanFactory  ]: ~~~ Config empty.
[15:34:03:970][  info  ][ScanBuildHistogrammers]: Loading histogrammer ...
[15:34:03:970][  info  ][ScanBuildHistogrammers]: ... done!
[15:34:03:970][  info  ][ScanBuildAnalyses]: Loading analyses ...
[15:34:03:970][  info  ][  scanConsole  ]: Running pre scan!
[15:34:03:970][  info  ][  ScanFactory  ]: Entering pre scan phase ...
[15:34:03:970][  info  ][     Rd53a     ]: Write named register: GlobalPulseRt -> 16384
[15:34:03:971][  info  ][     Rd53a     ]: Write named register: InjEnDig -> 0
[15:34:03:971][  info  ][     Rd53a     ]: Write named register: InjVcalDiff -> 0
[15:34:03:971][  info  ][     Rd53a     ]: Write named register: InjVcalHigh -> 3500
[15:34:03:971][  info  ][     Rd53a     ]: Write named register: InjVcalMed -> 500
[15:34:03:971][  info  ][     Rd53a     ]: Write named register: LatencyConfig -> 48
[15:34:03:971][  info  ][  scanConsole  ]: Starting histogrammer and analysis threads:
[15:34:03:971][  info  ][  scanConsole  ]:  .. started threads of Fe 4
[15:34:03:971][  info  ][  scanConsole  ]:  .. started threads of Fe 5
[15:34:03:971][  info  ][  scanConsole  ]:  .. started threads of Fe 6
[15:34:03:971][  info  ][  scanConsole  ]:  .. started threads of Fe 7
[15:34:03:972][  info  ][Rd53aDataProcessor]:   -> Processor thread #0 started!
[15:34:03:972][  info  ][Rd53aDataProcessor]:   -> Processor thread #1 started!
[15:34:03:972][  info  ][Rd53aDataProcessor]:   -> Processor thread #2 started!
[15:34:03:972][  info  ][Rd53aDataProcessor]:   -> Processor thread #3 started!
[15:34:03:972][  info  ][  scanConsole  ]: ########
[15:34:03:972][  info  ][  scanConsole  ]: # Scan #
[15:34:03:972][  info  ][  scanConsole  ]: ########
[15:34:03:972][  info  ][  scanConsole  ]: Starting scan!
[15:34:03:972][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0013396 on Rx 4
[15:34:03:976][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0013396] MON MUX_V: 3 Bias 0 1303 -> 0.271371 V Bias 1 1690 -> 0.349 V, Temperature 0.07763 -> -11.8463 C
[15:34:03:980][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0013396] MON MUX_V: 5 Bias 0 1272 -> 0.265152 V Bias 1 1650 -> 0.340977 V, Temperature 0.0758246 -> -13.1637 C
[15:34:03:984][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0013396] MON MUX_V: 7 Bias 0 1246 -> 0.259937 V Bias 1 1634 -> 0.337767 V, Temperature 0.0778305 -> -12.9574 C
[15:34:03:988][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0013396] MON MUX_V: 15 Bias 0 1254 -> 0.261541 V Bias 1 1650 -> 0.340977 V, Temperature 0.0794353 -> -10.5764 C
[15:34:04:023][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0013428 on Rx 5
[15:34:04:027][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0013428] MON MUX_V: 3 Bias 0 1280 -> 0.269159 V Bias 1 1649 -> 0.343911 V, Temperature 0.0747524 -> -15.7539 C
[15:34:04:032][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0013428] MON MUX_V: 5 Bias 0 1238 -> 0.260651 V Bias 1 1618 -> 0.337631 V, Temperature 0.0769808 -> -12.396 C
[15:34:04:036][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0013428] MON MUX_V: 7 Bias 0 1238 -> 0.260651 V Bias 1 1617 -> 0.337429 V, Temperature 0.0767782 -> -14.884 C
[15:34:04:040][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0013428] MON MUX_V: 15 Bias 0 1226 -> 0.25822 V Bias 1 1609 -> 0.335808 V, Temperature 0.0775886 -> -12.7596 C
[15:34:04:075][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0013381 on Rx 6
[15:34:04:079][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0013381] MON MUX_V: 3 Bias 0 1275 -> 0.274219 V Bias 1 1644 -> 0.348351 V, Temperature 0.0741314 -> -15.754 C
[15:34:04:083][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0013381] MON MUX_V: 5 Bias 0 1236 -> 0.266384 V Bias 1 1622 -> 0.343931 V, Temperature 0.0775467 -> -14.8855 C
[15:34:04:087][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0013381] MON MUX_V: 7 Bias 0 1220 -> 0.26317 V Bias 1 1605 -> 0.340516 V, Temperature 0.0773458 -> -13.7903 C
[15:34:04:091][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0013381] MON MUX_V: 15 Bias 0 1195 -> 0.258148 V Bias 1 1585 -> 0.336498 V, Temperature 0.0783503 -> -15.7117 C
[15:34:04:126][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0013349 on Rx 7
[15:34:04:130][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0013349] MON MUX_V: 3 Bias 0 1323 -> 0.272496 V Bias 1 1699 -> 0.348119 V, Temperature 0.0756229 -> -11.4897 C
[15:34:04:135][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0013349] MON MUX_V: 5 Bias 0 1286 -> 0.265055 V Bias 1 1665 -> 0.341281 V, Temperature 0.0762262 -> -11.8669 C
[15:34:04:139][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0013349] MON MUX_V: 7 Bias 0 1269 -> 0.261636 V Bias 1 1653 -> 0.338867 V, Temperature 0.0772319 -> -11.4769 C
[15:34:04:143][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0013349] MON MUX_V: 15 Bias 0 1277 -> 0.263245 V Bias 1 1659 -> 0.340074 V, Temperature 0.0768296 -> -11.0236 C
[15:34:04:180][  info  ][  scanConsole  ]: Scan done!
[15:34:04:180][  info  ][  scanConsole  ]: Waiting for processors to finish ...
[15:34:04:181][  info  ][  scanConsole  ]: Processor done, waiting for histogrammer ...
[15:34:04:580][  info  ][HistogramAlgorithm]: Histogrammer done!
[15:34:04:580][  info  ][HistogramAlgorithm]: Histogrammer done!
[15:34:04:580][  info  ][HistogramAlgorithm]: Histogrammer done!
[15:34:04:580][  info  ][HistogramAlgorithm]: Histogrammer done!
[15:34:04:580][  info  ][  scanConsole  ]: Processor done, waiting for analysis ...
[15:34:04:981][  info  ][AnalysisAlgorithm]: Analysis done!
[15:34:04:981][  info  ][AnalysisAlgorithm]: Analysis done!
[15:34:04:981][  info  ][AnalysisAlgorithm]: Analysis done!
[15:34:04:981][  info  ][AnalysisAlgorithm]: Analysis done!
[15:34:04:981][  info  ][  scanConsole  ]: All done!
[15:34:04:981][  info  ][  scanConsole  ]: ##########
[15:34:04:981][  info  ][  scanConsole  ]: # Timing #
[15:34:04:981][  info  ][  scanConsole  ]: ##########
[15:34:04:981][  info  ][  scanConsole  ]: -> Configuration: 146 ms
[15:34:04:981][  info  ][  scanConsole  ]: -> Scan:          207 ms
[15:34:04:981][  info  ][  scanConsole  ]: -> Processing:    1 ms
[15:34:04:981][  info  ][  scanConsole  ]: -> Analysis:      800 ms
[15:34:04:981][  info  ][  scanConsole  ]: ###########
[15:34:04:981][  info  ][  scanConsole  ]: # Cleanup #
[15:34:04:981][  info  ][  scanConsole  ]: ###########
[15:34:04:990][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0013396 to configs/KEKQ17/20UPGFC0013396.json
[15:34:05:160][  info  ][  scanConsole  ]: -> Storing output results of FE 4
[15:34:05:160][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0013396, this usually means that the chip did not send any data at all.
[15:34:05:161][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0013428 to configs/KEKQ17/20UPGFC0013428.json
[15:34:05:324][  info  ][  scanConsole  ]: -> Storing output results of FE 5
[15:34:05:324][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0013428, this usually means that the chip did not send any data at all.
[15:34:05:325][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0013381 to configs/KEKQ17/20UPGFC0013381.json
[15:34:05:488][  info  ][  scanConsole  ]: -> Storing output results of FE 6
[15:34:05:488][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0013381, this usually means that the chip did not send any data at all.
[15:34:05:489][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0013349 to configs/KEKQ17/20UPGFC0013349.json
[15:34:05:651][  info  ][  scanConsole  ]: -> Storing output results of FE 7
[15:34:05:651][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0013349, this usually means that the chip did not send any data at all.
[15:34:05:653][  info  ][  scanConsole  ]: Finishing run: 1821
20UPGFC0013349.json.after
20UPGFC0013349.json.before
20UPGFC0013381.json.after
20UPGFC0013381.json.before
20UPGFC0013396.json.after
20UPGFC0013396.json.before
20UPGFC0013428.json.after
20UPGFC0013428.json.before
reg_readtemp.json
scanLog.json

