[15:29:03:508][  info  ][               ]: #####################################
[15:29:03:508][  info  ][               ]: # Welcome to the YARR Scan Console! #
[15:29:03:508][  info  ][               ]: #####################################
[15:29:03:508][  info  ][               ]: -> Parsing command line parameters ...
[15:29:03:508][  info  ][               ]: Configuring logger ...
[15:29:03:510][  info  ][  scanConsole  ]: Scan Type/Config configs/scans/rd53a/reg_readtemp.json
[15:29:03:510][  info  ][  scanConsole  ]: Connectivity:
[15:29:03:510][  info  ][  scanConsole  ]:     configs/KEKQ17/connectivity.json
[15:29:03:510][  info  ][  scanConsole  ]: Target ToT: -1
[15:29:03:510][  info  ][  scanConsole  ]: Target Charge: -1
[15:29:03:510][  info  ][  scanConsole  ]: Output Plots: true
[15:29:03:510][  info  ][  scanConsole  ]: Output Directory: ./data/001820_reg_readtemp/
[15:29:03:514][  info  ][  scanConsole  ]: Timestamp: 2022-08-25_15:29:03
[15:29:03:514][  info  ][  scanConsole  ]: Run Number: 1820
[15:29:03:514][  info  ][  scanConsole  ]: #################
[15:29:03:514][  info  ][  scanConsole  ]: # Init Hardware #
[15:29:03:514][  info  ][  scanConsole  ]: #################
[15:29:03:514][  info  ][  scanConsole  ]: -> Opening controller config: configs/controller/specCfg-rd53a-16x1.json
[15:29:03:514][  info  ][  ScanHelper   ]: Loading controller ...
[15:29:03:514][  info  ][  ScanHelper   ]: Found controller of type: spec
[15:29:03:514][  info  ][  ScanHelper   ]: ... loading controler config:
[15:29:03:514][  info  ][  ScanHelper   ]: ~~~ {
[15:29:03:514][  info  ][  ScanHelper   ]: ~~~     "cmdPeriod": 6.250000073038109e-9,
[15:29:03:514][  info  ][  ScanHelper   ]: ~~~     "idle": {
[15:29:03:514][  info  ][  ScanHelper   ]: ~~~         "word": 1768515945
[15:29:03:514][  info  ][  ScanHelper   ]: ~~~     },
[15:29:03:514][  info  ][  ScanHelper   ]: ~~~     "pulse": {
[15:29:03:514][  info  ][  ScanHelper   ]: ~~~         "interval": 500,
[15:29:03:514][  info  ][  ScanHelper   ]: ~~~         "word": 1549575846
[15:29:03:514][  info  ][  ScanHelper   ]: ~~~     },
[15:29:03:514][  info  ][  ScanHelper   ]: ~~~     "rxActiveLanes": 1,
[15:29:03:514][  info  ][  ScanHelper   ]: ~~~     "rxPolarity": 0,
[15:29:03:514][  info  ][  ScanHelper   ]: ~~~     "specNum": 0,
[15:29:03:514][  info  ][  ScanHelper   ]: ~~~     "spiConfig": 541200,
[15:29:03:514][  info  ][  ScanHelper   ]: ~~~     "sync": {
[15:29:03:514][  info  ][  ScanHelper   ]: ~~~         "interval": 32,
[15:29:03:514][  info  ][  ScanHelper   ]: ~~~         "word": 2172551550
[15:29:03:514][  info  ][  ScanHelper   ]: ~~~     },
[15:29:03:514][  info  ][  ScanHelper   ]: ~~~     "txPolarity": 0
[15:29:03:514][  info  ][  ScanHelper   ]: ~~~ }
[15:29:03:514][  info  ][    SpecCom    ]: Opening SPEC with id #0
[15:29:03:514][  info  ][    SpecCom    ]: Mapping BARs ...
[15:29:03:514][  info  ][    SpecCom    ]: ... Mapped BAR0 at 0x7f38ed8e6000 with size 1048576
[15:29:03:515][warning ][    SpecCom    ]: ... BAR4 not mapped (Mmap failed)
[15:29:03:515][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[15:29:03:515][  info  ][    SpecCom    ]: Firmware Version: 0x4d9ff6d
[15:29:03:515][  info  ][    SpecCom    ]: Firmware Identifier: 0x4030232
[15:29:03:515][  info  ][    SpecCom    ]: FPGA card: PLDA XpressK7 325
[15:29:03:515][  info  ][    SpecCom    ]: FE Chip Type: RD53A/B
[15:29:03:515][  info  ][    SpecCom    ]: FMC Card Type: Ohio Card (Display Port)
[15:29:03:515][  info  ][    SpecCom    ]: RX Speed: 640Mbps
[15:29:03:515][  info  ][    SpecCom    ]: Channel Configuration: 16x1
[15:29:03:515][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[15:29:03:515][  info  ][    SpecCom    ]: Flushing buffers ...
[15:29:03:515][  info  ][    SpecCom    ]: Init success!
[15:29:03:515][  info  ][  scanConsole  ]: #######################
[15:29:03:515][  info  ][  scanConsole  ]: ##  Loading Configs  ##
[15:29:03:515][  info  ][  scanConsole  ]: #######################
[15:29:03:515][  info  ][  scanConsole  ]: Opening global config: configs/KEKQ17/connectivity.json
[15:29:03:515][  info  ][  ScanHelper   ]: Chip type: RD53A
[15:29:03:515][  info  ][  ScanHelper   ]: Chip count 4
[15:29:03:515][  info  ][  ScanHelper   ]: Loading chip #0
[15:29:03:515][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(4)
[15:29:03:516][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ17/20UPGFC0013396.json
[15:29:03:691][  info  ][  ScanHelper   ]: Loading chip #1
[15:29:03:692][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(5)
[15:29:03:692][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ17/20UPGFC0013428.json
[15:29:03:865][  info  ][  ScanHelper   ]: Loading chip #2
[15:29:03:866][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(6)
[15:29:03:866][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ17/20UPGFC0013381.json
[15:29:04:040][  info  ][  ScanHelper   ]: Loading chip #3
[15:29:04:041][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(7)
[15:29:04:041][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ17/20UPGFC0013349.json
[15:29:04:223][  info  ][  scanConsole  ]: #################
[15:29:04:223][  info  ][  scanConsole  ]: # Configure FEs #
[15:29:04:223][  info  ][  scanConsole  ]: #################
[15:29:04:223][  info  ][  scanConsole  ]: Configuring 20UPGFC0013396
[15:29:04:254][  info  ][  scanConsole  ]: Configuring 20UPGFC0013428
[15:29:04:284][  info  ][  scanConsole  ]: Configuring 20UPGFC0013381
[15:29:04:327][  info  ][  scanConsole  ]: Configuring 20UPGFC0013349
[15:29:04:357][  info  ][  scanConsole  ]: Sent configuration to all FEs in 134 ms!
[15:29:04:358][  info  ][  scanConsole  ]: Checking com 20UPGFC0013396
[15:29:04:358][  info  ][    SpecRx     ]: Active Rx channels: 0x10
[15:29:04:358][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[15:29:04:358][  info  ][    SpecRx     ]: Rx Status 0xf0
[15:29:04:358][  info  ][    SpecRx     ]: Number of lanes: 1
[15:29:04:358][  info  ][    SpecRx     ]: Channel 4 Lane 0 synchronized!
[15:29:04:369][  info  ][  scanConsole  ]: ... success!
[15:29:04:369][  info  ][  scanConsole  ]: Checking com 20UPGFC0013428
[15:29:04:369][  info  ][    SpecRx     ]: Active Rx channels: 0x20
[15:29:04:369][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[15:29:04:369][  info  ][    SpecRx     ]: Rx Status 0xf0
[15:29:04:369][  info  ][    SpecRx     ]: Number of lanes: 1
[15:29:04:369][  info  ][    SpecRx     ]: Channel 5 Lane 0 synchronized!
[15:29:04:379][  info  ][  scanConsole  ]: ... success!
[15:29:04:379][  info  ][  scanConsole  ]: Checking com 20UPGFC0013381
[15:29:04:379][  info  ][    SpecRx     ]: Active Rx channels: 0x40
[15:29:04:379][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[15:29:04:380][  info  ][    SpecRx     ]: Rx Status 0xf0
[15:29:04:380][  info  ][    SpecRx     ]: Number of lanes: 1
[15:29:04:380][  info  ][    SpecRx     ]: Channel 6 Lane 0 synchronized!
[15:29:04:390][  info  ][  scanConsole  ]: ... success!
[15:29:04:390][  info  ][  scanConsole  ]: Checking com 20UPGFC0013349
[15:29:04:390][  info  ][    SpecRx     ]: Active Rx channels: 0x80
[15:29:04:390][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[15:29:04:390][  info  ][    SpecRx     ]: Rx Status 0xf0
[15:29:04:390][  info  ][    SpecRx     ]: Number of lanes: 1
[15:29:04:390][  info  ][    SpecRx     ]: Channel 7 Lane 0 synchronized!
[15:29:04:400][  info  ][  scanConsole  ]: ... success!
[15:29:04:401][  info  ][  scanConsole  ]: Enabling Tx channels
[15:29:04:401][  info  ][  scanConsole  ]: Enabling Tx channel 1
[15:29:04:401][  info  ][  scanConsole  ]: Enabling Tx channel 1
[15:29:04:401][  info  ][  scanConsole  ]: Enabling Tx channel 1
[15:29:04:401][  info  ][  scanConsole  ]: Enabling Tx channel 1
[15:29:04:401][  info  ][  scanConsole  ]: Enabling Rx channels
[15:29:04:401][  info  ][  scanConsole  ]: Enabling Rx channel 4
[15:29:04:401][  info  ][  scanConsole  ]: Enabling Rx channel 5
[15:29:04:401][  info  ][  scanConsole  ]: Enabling Rx channel 6
[15:29:04:401][  info  ][  scanConsole  ]: Enabling Rx channel 7
[15:29:04:401][  info  ][  scanConsole  ]: ##############
[15:29:04:401][  info  ][  scanConsole  ]: # Setup Scan #
[15:29:04:401][  info  ][  scanConsole  ]: ##############
[15:29:04:401][  info  ][  scanConsole  ]: Found Scan config, constructing scan ...
[15:29:04:402][  info  ][  ScanFactory  ]: Loading Scan:
[15:29:04:402][  info  ][  ScanFactory  ]:   Name: AnalogScan
[15:29:04:402][  info  ][  ScanFactory  ]:   Number of Loops: 3
[15:29:04:402][  info  ][  ScanFactory  ]:   Loading Loop #0
[15:29:04:402][  info  ][  ScanFactory  ]:    Type: Rd53aReadRegLoop
[15:29:04:402][  info  ][  ScanFactory  ]:    Loading loop config ... 
[15:29:04:402][  info  ][  ScanFactory  ]: ~~~ {
[15:29:04:402][  info  ][  ScanFactory  ]: ~~~     "CurMux": [],
[15:29:04:402][  info  ][  ScanFactory  ]: ~~~     "EnblRingOsc": 0,
[15:29:04:402][  info  ][  ScanFactory  ]: ~~~     "Registers": [],
[15:29:04:402][  info  ][  ScanFactory  ]: ~~~     "RingOscDur": 9,
[15:29:04:402][  info  ][  ScanFactory  ]: ~~~     "RingOscRep": 10,
[15:29:04:402][  info  ][  ScanFactory  ]: ~~~     "VoltMux": [3, 5, 7, 15]
[15:29:04:402][  info  ][  ScanFactory  ]: ~~~ }
[15:29:04:402][  info  ][  ScanFactory  ]:   Loading Loop #1
[15:29:04:402][  info  ][  ScanFactory  ]:    Type: Rd53aTriggerLoop
[15:29:04:402][  info  ][  ScanFactory  ]:    Loading loop config ... 
[15:29:04:402][  info  ][  ScanFactory  ]: ~~~ {
[15:29:04:402][  info  ][  ScanFactory  ]: ~~~     "count": 1,
[15:29:04:402][  info  ][  ScanFactory  ]: ~~~     "delay": 48,
[15:29:04:402][  info  ][  ScanFactory  ]: ~~~     "extTrig": false,
[15:29:04:402][  info  ][  ScanFactory  ]: ~~~     "frequency": 30000.0,
[15:29:04:402][  info  ][  ScanFactory  ]: ~~~     "noInject": false,
[15:29:04:402][  info  ][  ScanFactory  ]: ~~~     "sendEcr": false,
[15:29:04:402][  info  ][  ScanFactory  ]: ~~~     "time": 0.0,
[15:29:04:402][  info  ][  ScanFactory  ]: ~~~     "trigMultiplier": 16
[15:29:04:402][  info  ][  ScanFactory  ]: ~~~ }
[15:29:04:402][  info  ][  ScanFactory  ]:   Loading Loop #2
[15:29:04:402][  info  ][  ScanFactory  ]:    Type: StdDataLoop
[15:29:04:402][warning ][  ScanFactory  ]: ~~~ Config empty.
[15:29:04:402][  info  ][ScanBuildHistogrammers]: Loading histogrammer ...
[15:29:04:403][  info  ][ScanBuildHistogrammers]: ... done!
[15:29:04:403][  info  ][ScanBuildAnalyses]: Loading analyses ...
[15:29:04:403][  info  ][  scanConsole  ]: Running pre scan!
[15:29:04:403][  info  ][  ScanFactory  ]: Entering pre scan phase ...
[15:29:04:403][  info  ][     Rd53a     ]: Write named register: GlobalPulseRt -> 16384
[15:29:04:403][  info  ][     Rd53a     ]: Write named register: InjEnDig -> 0
[15:29:04:403][  info  ][     Rd53a     ]: Write named register: InjVcalDiff -> 0
[15:29:04:403][  info  ][     Rd53a     ]: Write named register: InjVcalHigh -> 3500
[15:29:04:403][  info  ][     Rd53a     ]: Write named register: InjVcalMed -> 500
[15:29:04:403][  info  ][     Rd53a     ]: Write named register: LatencyConfig -> 48
[15:29:04:403][  info  ][  scanConsole  ]: Starting histogrammer and analysis threads:
[15:29:04:403][  info  ][  scanConsole  ]:  .. started threads of Fe 4
[15:29:04:403][  info  ][  scanConsole  ]:  .. started threads of Fe 5
[15:29:04:404][  info  ][  scanConsole  ]:  .. started threads of Fe 6
[15:29:04:404][  info  ][  scanConsole  ]:  .. started threads of Fe 7
[15:29:04:404][  info  ][Rd53aDataProcessor]:   -> Processor thread #0 started!
[15:29:04:404][  info  ][Rd53aDataProcessor]:   -> Processor thread #1 started!
[15:29:04:404][  info  ][Rd53aDataProcessor]:   -> Processor thread #2 started!
[15:29:04:404][  info  ][Rd53aDataProcessor]:   -> Processor thread #3 started!
[15:29:04:404][  info  ][  scanConsole  ]: ########
[15:29:04:404][  info  ][  scanConsole  ]: # Scan #
[15:29:04:404][  info  ][  scanConsole  ]: ########
[15:29:04:404][  info  ][  scanConsole  ]: Starting scan!
[15:29:04:404][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0013396 on Rx 4
[15:29:04:408][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0013396] MON MUX_V: 3 Bias 0 1301 -> 0.270969 V Bias 1 1690 -> 0.349 V, Temperature 0.0780311 -> -10.4959 C
[15:29:04:413][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0013396] MON MUX_V: 5 Bias 0 1272 -> 0.265152 V Bias 1 1649 -> 0.340776 V, Temperature 0.075624 -> -13.8515 C
[15:29:04:417][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0013396] MON MUX_V: 7 Bias 0 1246 -> 0.259937 V Bias 1 1633 -> 0.337567 V, Temperature 0.0776299 -> -13.628 C
[15:29:04:421][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0013396] MON MUX_V: 15 Bias 0 1254 -> 0.261541 V Bias 1 1650 -> 0.340977 V, Temperature 0.0794353 -> -10.5764 C
[15:29:04:456][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0013428 on Rx 5
[15:29:04:460][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0013428] MON MUX_V: 3 Bias 0 1279 -> 0.268956 V Bias 1 1649 -> 0.343911 V, Temperature 0.074955 -> -15.0564 C
[15:29:04:464][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0013428] MON MUX_V: 5 Bias 0 1237 -> 0.260448 V Bias 1 1617 -> 0.337429 V, Temperature 0.0769808 -> -12.396 C
[15:29:04:468][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0013428] MON MUX_V: 7 Bias 0 1238 -> 0.260651 V Bias 1 1617 -> 0.337429 V, Temperature 0.0767782 -> -14.884 C
[15:29:04:472][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0013428] MON MUX_V: 15 Bias 0 1226 -> 0.25822 V Bias 1 1609 -> 0.335808 V, Temperature 0.0775886 -> -12.7596 C
[15:29:04:507][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0013381 on Rx 6
[15:29:04:512][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0013381] MON MUX_V: 3 Bias 0 1274 -> 0.274018 V Bias 1 1645 -> 0.348552 V, Temperature 0.0745332 -> -14.3589 C
[15:29:04:516][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0013381] MON MUX_V: 5 Bias 0 1235 -> 0.266183 V Bias 1 1621 -> 0.34373 V, Temperature 0.0775466 -> -14.8857 C
[15:29:04:520][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0013381] MON MUX_V: 7 Bias 0 1221 -> 0.263371 V Bias 1 1606 -> 0.340717 V, Temperature 0.0773458 -> -13.7903 C
[15:29:04:524][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0013381] MON MUX_V: 15 Bias 0 1194 -> 0.257947 V Bias 1 1585 -> 0.336498 V, Temperature 0.0785511 -> -15.0518 C
[15:29:04:560][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0013349 on Rx 7
[15:29:04:564][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0013349] MON MUX_V: 3 Bias 0 1322 -> 0.272295 V Bias 1 1698 -> 0.347918 V, Temperature 0.0756229 -> -11.4896 C
[15:29:04:568][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0013349] MON MUX_V: 5 Bias 0 1286 -> 0.265055 V Bias 1 1665 -> 0.341281 V, Temperature 0.0762262 -> -11.8669 C
[15:29:04:572][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0013349] MON MUX_V: 7 Bias 0 1268 -> 0.261434 V Bias 1 1655 -> 0.33927 V, Temperature 0.0778353 -> -9.43243 C
[15:29:04:576][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0013349] MON MUX_V: 15 Bias 0 1277 -> 0.263245 V Bias 1 1659 -> 0.340074 V, Temperature 0.0768296 -> -11.0236 C
[15:29:04:612][  info  ][  scanConsole  ]: Scan done!
[15:29:04:612][  info  ][  scanConsole  ]: Waiting for processors to finish ...
[15:29:04:613][  info  ][  scanConsole  ]: Processor done, waiting for histogrammer ...
[15:29:05:013][  info  ][HistogramAlgorithm]: Histogrammer done!
[15:29:05:013][  info  ][HistogramAlgorithm]: Histogrammer done!
[15:29:05:013][  info  ][HistogramAlgorithm]: Histogrammer done!
[15:29:05:013][  info  ][HistogramAlgorithm]: Histogrammer done!
[15:29:05:014][  info  ][  scanConsole  ]: Processor done, waiting for analysis ...
[15:29:05:414][  info  ][AnalysisAlgorithm]: Analysis done!
[15:29:05:414][  info  ][AnalysisAlgorithm]: Analysis done!
[15:29:05:414][  info  ][AnalysisAlgorithm]: Analysis done!
[15:29:05:414][  info  ][AnalysisAlgorithm]: Analysis done!
[15:29:05:414][  info  ][  scanConsole  ]: All done!
[15:29:05:414][  info  ][  scanConsole  ]: ##########
[15:29:05:414][  info  ][  scanConsole  ]: # Timing #
[15:29:05:414][  info  ][  scanConsole  ]: ##########
[15:29:05:414][  info  ][  scanConsole  ]: -> Configuration: 134 ms
[15:29:05:414][  info  ][  scanConsole  ]: -> Scan:          208 ms
[15:29:05:414][  info  ][  scanConsole  ]: -> Processing:    0 ms
[15:29:05:414][  info  ][  scanConsole  ]: -> Analysis:      801 ms
[15:29:05:414][  info  ][  scanConsole  ]: ###########
[15:29:05:414][  info  ][  scanConsole  ]: # Cleanup #
[15:29:05:414][  info  ][  scanConsole  ]: ###########
[15:29:05:423][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0013396 to configs/KEKQ17/20UPGFC0013396.json
[15:29:05:595][  info  ][  scanConsole  ]: -> Storing output results of FE 4
[15:29:05:595][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0013396, this usually means that the chip did not send any data at all.
[15:29:05:597][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0013428 to configs/KEKQ17/20UPGFC0013428.json
[15:29:05:762][  info  ][  scanConsole  ]: -> Storing output results of FE 5
[15:29:05:762][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0013428, this usually means that the chip did not send any data at all.
[15:29:05:763][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0013381 to configs/KEKQ17/20UPGFC0013381.json
[15:29:05:929][  info  ][  scanConsole  ]: -> Storing output results of FE 6
[15:29:05:929][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0013381, this usually means that the chip did not send any data at all.
[15:29:05:930][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0013349 to configs/KEKQ17/20UPGFC0013349.json
[15:29:06:096][  info  ][  scanConsole  ]: -> Storing output results of FE 7
[15:29:06:096][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0013349, this usually means that the chip did not send any data at all.
[15:29:06:097][  info  ][  scanConsole  ]: Finishing run: 1820
20UPGFC0013349.json.after
20UPGFC0013349.json.before
20UPGFC0013381.json.after
20UPGFC0013381.json.before
20UPGFC0013396.json.after
20UPGFC0013396.json.before
20UPGFC0013428.json.after
20UPGFC0013428.json.before
reg_readtemp.json
scanLog.json

