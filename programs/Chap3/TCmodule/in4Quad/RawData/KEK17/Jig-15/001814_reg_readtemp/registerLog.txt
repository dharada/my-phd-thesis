[14:52:32:857][  info  ][               ]: #####################################
[14:52:32:857][  info  ][               ]: # Welcome to the YARR Scan Console! #
[14:52:32:857][  info  ][               ]: #####################################
[14:52:32:857][  info  ][               ]: -> Parsing command line parameters ...
[14:52:32:857][  info  ][               ]: Configuring logger ...
[14:52:32:860][  info  ][  scanConsole  ]: Scan Type/Config configs/scans/rd53a/reg_readtemp.json
[14:52:32:860][  info  ][  scanConsole  ]: Connectivity:
[14:52:32:860][  info  ][  scanConsole  ]:     configs/KEKQ17/connectivity.json
[14:52:32:860][  info  ][  scanConsole  ]: Target ToT: -1
[14:52:32:860][  info  ][  scanConsole  ]: Target Charge: -1
[14:52:32:860][  info  ][  scanConsole  ]: Output Plots: true
[14:52:32:860][  info  ][  scanConsole  ]: Output Directory: ./data/001814_reg_readtemp/
[14:52:32:864][  info  ][  scanConsole  ]: Timestamp: 2022-08-25_14:52:32
[14:52:32:864][  info  ][  scanConsole  ]: Run Number: 1814
[14:52:32:864][  info  ][  scanConsole  ]: #################
[14:52:32:864][  info  ][  scanConsole  ]: # Init Hardware #
[14:52:32:864][  info  ][  scanConsole  ]: #################
[14:52:32:864][  info  ][  scanConsole  ]: -> Opening controller config: configs/controller/specCfg-rd53a-16x1.json
[14:52:32:864][  info  ][  ScanHelper   ]: Loading controller ...
[14:52:32:864][  info  ][  ScanHelper   ]: Found controller of type: spec
[14:52:32:865][  info  ][  ScanHelper   ]: ... loading controler config:
[14:52:32:865][  info  ][  ScanHelper   ]: ~~~ {
[14:52:32:865][  info  ][  ScanHelper   ]: ~~~     "cmdPeriod": 6.250000073038109e-9,
[14:52:32:865][  info  ][  ScanHelper   ]: ~~~     "idle": {
[14:52:32:865][  info  ][  ScanHelper   ]: ~~~         "word": 1768515945
[14:52:32:865][  info  ][  ScanHelper   ]: ~~~     },
[14:52:32:865][  info  ][  ScanHelper   ]: ~~~     "pulse": {
[14:52:32:865][  info  ][  ScanHelper   ]: ~~~         "interval": 500,
[14:52:32:865][  info  ][  ScanHelper   ]: ~~~         "word": 1549575846
[14:52:32:865][  info  ][  ScanHelper   ]: ~~~     },
[14:52:32:865][  info  ][  ScanHelper   ]: ~~~     "rxActiveLanes": 1,
[14:52:32:865][  info  ][  ScanHelper   ]: ~~~     "rxPolarity": 0,
[14:52:32:865][  info  ][  ScanHelper   ]: ~~~     "specNum": 0,
[14:52:32:865][  info  ][  ScanHelper   ]: ~~~     "spiConfig": 541200,
[14:52:32:865][  info  ][  ScanHelper   ]: ~~~     "sync": {
[14:52:32:865][  info  ][  ScanHelper   ]: ~~~         "interval": 32,
[14:52:32:865][  info  ][  ScanHelper   ]: ~~~         "word": 2172551550
[14:52:32:865][  info  ][  ScanHelper   ]: ~~~     },
[14:52:32:865][  info  ][  ScanHelper   ]: ~~~     "txPolarity": 0
[14:52:32:865][  info  ][  ScanHelper   ]: ~~~ }
[14:52:32:865][  info  ][    SpecCom    ]: Opening SPEC with id #0
[14:52:32:865][  info  ][    SpecCom    ]: Mapping BARs ...
[14:52:32:865][  info  ][    SpecCom    ]: ... Mapped BAR0 at 0x7f641de35000 with size 1048576
[14:52:32:865][warning ][    SpecCom    ]: ... BAR4 not mapped (Mmap failed)
[14:52:32:865][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[14:52:32:865][  info  ][    SpecCom    ]: Firmware Version: 0x4d9ff6d
[14:52:32:865][  info  ][    SpecCom    ]: Firmware Identifier: 0x4030232
[14:52:32:865][  info  ][    SpecCom    ]: FPGA card: PLDA XpressK7 325
[14:52:32:865][  info  ][    SpecCom    ]: FE Chip Type: RD53A/B
[14:52:32:865][  info  ][    SpecCom    ]: FMC Card Type: Ohio Card (Display Port)
[14:52:32:865][  info  ][    SpecCom    ]: RX Speed: 640Mbps
[14:52:32:865][  info  ][    SpecCom    ]: Channel Configuration: 16x1
[14:52:32:865][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[14:52:32:865][  info  ][    SpecCom    ]: Flushing buffers ...
[14:52:32:865][  info  ][    SpecCom    ]: Init success!
[14:52:32:865][  info  ][  scanConsole  ]: #######################
[14:52:32:865][  info  ][  scanConsole  ]: ##  Loading Configs  ##
[14:52:32:865][  info  ][  scanConsole  ]: #######################
[14:52:32:865][  info  ][  scanConsole  ]: Opening global config: configs/KEKQ17/connectivity.json
[14:52:32:865][  info  ][  ScanHelper   ]: Chip type: RD53A
[14:52:32:865][  info  ][  ScanHelper   ]: Chip count 4
[14:52:32:865][  info  ][  ScanHelper   ]: Loading chip #0
[14:52:32:866][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(4)
[14:52:32:866][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ17/20UPGFC0013396.json
[14:52:33:042][  info  ][  ScanHelper   ]: Loading chip #1
[14:52:33:043][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(5)
[14:52:33:043][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ17/20UPGFC0013428.json
[14:52:33:218][  info  ][  ScanHelper   ]: Loading chip #2
[14:52:33:218][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(6)
[14:52:33:218][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ17/20UPGFC0013381.json
[14:52:33:392][  info  ][  ScanHelper   ]: Loading chip #3
[14:52:33:393][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(7)
[14:52:33:393][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ17/20UPGFC0013349.json
[14:52:33:571][  info  ][  scanConsole  ]: #################
[14:52:33:571][  info  ][  scanConsole  ]: # Configure FEs #
[14:52:33:571][  info  ][  scanConsole  ]: #################
[14:52:33:571][  info  ][  scanConsole  ]: Configuring 20UPGFC0013396
[14:52:33:601][  info  ][  scanConsole  ]: Configuring 20UPGFC0013428
[14:52:33:632][  info  ][  scanConsole  ]: Configuring 20UPGFC0013381
[14:52:33:662][  info  ][  scanConsole  ]: Configuring 20UPGFC0013349
[14:52:33:692][  info  ][  scanConsole  ]: Sent configuration to all FEs in 121 ms!
[14:52:33:693][  info  ][  scanConsole  ]: Checking com 20UPGFC0013396
[14:52:33:693][  info  ][    SpecRx     ]: Active Rx channels: 0x10
[14:52:33:693][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[14:52:33:693][  info  ][    SpecRx     ]: Rx Status 0xf0
[14:52:33:693][  info  ][    SpecRx     ]: Number of lanes: 1
[14:52:33:693][  info  ][    SpecRx     ]: Channel 4 Lane 0 synchronized!
[14:52:33:704][  info  ][  scanConsole  ]: ... success!
[14:52:33:704][  info  ][  scanConsole  ]: Checking com 20UPGFC0013428
[14:52:33:704][  info  ][    SpecRx     ]: Active Rx channels: 0x20
[14:52:33:704][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[14:52:33:704][  info  ][    SpecRx     ]: Rx Status 0xf0
[14:52:33:704][  info  ][    SpecRx     ]: Number of lanes: 1
[14:52:33:704][  info  ][    SpecRx     ]: Channel 5 Lane 0 synchronized!
[14:52:33:714][  info  ][  scanConsole  ]: ... success!
[14:52:33:714][  info  ][  scanConsole  ]: Checking com 20UPGFC0013381
[14:52:33:714][  info  ][    SpecRx     ]: Active Rx channels: 0x40
[14:52:33:714][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[14:52:33:714][  info  ][    SpecRx     ]: Rx Status 0xf0
[14:52:33:714][  info  ][    SpecRx     ]: Number of lanes: 1
[14:52:33:714][  info  ][    SpecRx     ]: Channel 6 Lane 0 synchronized!
[14:52:33:724][  info  ][  scanConsole  ]: ... success!
[14:52:33:724][  info  ][  scanConsole  ]: Checking com 20UPGFC0013349
[14:52:33:724][  info  ][    SpecRx     ]: Active Rx channels: 0x80
[14:52:33:724][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[14:52:33:724][  info  ][    SpecRx     ]: Rx Status 0xf0
[14:52:33:724][  info  ][    SpecRx     ]: Number of lanes: 1
[14:52:33:724][  info  ][    SpecRx     ]: Channel 7 Lane 0 synchronized!
[14:52:33:735][  info  ][  scanConsole  ]: ... success!
[14:52:33:735][  info  ][  scanConsole  ]: Enabling Tx channels
[14:52:33:735][  info  ][  scanConsole  ]: Enabling Tx channel 1
[14:52:33:735][  info  ][  scanConsole  ]: Enabling Tx channel 1
[14:52:33:735][  info  ][  scanConsole  ]: Enabling Tx channel 1
[14:52:33:735][  info  ][  scanConsole  ]: Enabling Tx channel 1
[14:52:33:735][  info  ][  scanConsole  ]: Enabling Rx channels
[14:52:33:735][  info  ][  scanConsole  ]: Enabling Rx channel 4
[14:52:33:735][  info  ][  scanConsole  ]: Enabling Rx channel 5
[14:52:33:735][  info  ][  scanConsole  ]: Enabling Rx channel 6
[14:52:33:735][  info  ][  scanConsole  ]: Enabling Rx channel 7
[14:52:33:735][  info  ][  scanConsole  ]: ##############
[14:52:33:735][  info  ][  scanConsole  ]: # Setup Scan #
[14:52:33:735][  info  ][  scanConsole  ]: ##############
[14:52:33:735][  info  ][  scanConsole  ]: Found Scan config, constructing scan ...
[14:52:33:735][  info  ][  ScanFactory  ]: Loading Scan:
[14:52:33:735][  info  ][  ScanFactory  ]:   Name: AnalogScan
[14:52:33:735][  info  ][  ScanFactory  ]:   Number of Loops: 3
[14:52:33:735][  info  ][  ScanFactory  ]:   Loading Loop #0
[14:52:33:735][  info  ][  ScanFactory  ]:    Type: Rd53aReadRegLoop
[14:52:33:735][  info  ][  ScanFactory  ]:    Loading loop config ... 
[14:52:33:735][  info  ][  ScanFactory  ]: ~~~ {
[14:52:33:735][  info  ][  ScanFactory  ]: ~~~     "CurMux": [],
[14:52:33:735][  info  ][  ScanFactory  ]: ~~~     "EnblRingOsc": 0,
[14:52:33:735][  info  ][  ScanFactory  ]: ~~~     "Registers": [],
[14:52:33:735][  info  ][  ScanFactory  ]: ~~~     "RingOscDur": 9,
[14:52:33:735][  info  ][  ScanFactory  ]: ~~~     "RingOscRep": 10,
[14:52:33:735][  info  ][  ScanFactory  ]: ~~~     "VoltMux": [3, 5, 7, 15]
[14:52:33:735][  info  ][  ScanFactory  ]: ~~~ }
[14:52:33:735][  info  ][  ScanFactory  ]:   Loading Loop #1
[14:52:33:735][  info  ][  ScanFactory  ]:    Type: Rd53aTriggerLoop
[14:52:33:735][  info  ][  ScanFactory  ]:    Loading loop config ... 
[14:52:33:735][  info  ][  ScanFactory  ]: ~~~ {
[14:52:33:735][  info  ][  ScanFactory  ]: ~~~     "count": 1,
[14:52:33:735][  info  ][  ScanFactory  ]: ~~~     "delay": 48,
[14:52:33:735][  info  ][  ScanFactory  ]: ~~~     "extTrig": false,
[14:52:33:735][  info  ][  ScanFactory  ]: ~~~     "frequency": 30000.0,
[14:52:33:735][  info  ][  ScanFactory  ]: ~~~     "noInject": false,
[14:52:33:735][  info  ][  ScanFactory  ]: ~~~     "sendEcr": false,
[14:52:33:735][  info  ][  ScanFactory  ]: ~~~     "time": 0.0,
[14:52:33:735][  info  ][  ScanFactory  ]: ~~~     "trigMultiplier": 16
[14:52:33:735][  info  ][  ScanFactory  ]: ~~~ }
[14:52:33:735][  info  ][  ScanFactory  ]:   Loading Loop #2
[14:52:33:735][  info  ][  ScanFactory  ]:    Type: StdDataLoop
[14:52:33:735][warning ][  ScanFactory  ]: ~~~ Config empty.
[14:52:33:735][  info  ][ScanBuildHistogrammers]: Loading histogrammer ...
[14:52:33:735][  info  ][ScanBuildHistogrammers]: ... done!
[14:52:33:735][  info  ][ScanBuildAnalyses]: Loading analyses ...
[14:52:33:735][  info  ][  scanConsole  ]: Running pre scan!
[14:52:33:735][  info  ][  ScanFactory  ]: Entering pre scan phase ...
[14:52:33:735][  info  ][     Rd53a     ]: Write named register: GlobalPulseRt -> 16384
[14:52:33:735][  info  ][     Rd53a     ]: Write named register: InjEnDig -> 0
[14:52:33:735][  info  ][     Rd53a     ]: Write named register: InjVcalDiff -> 0
[14:52:33:736][  info  ][     Rd53a     ]: Write named register: InjVcalHigh -> 3500
[14:52:33:736][  info  ][     Rd53a     ]: Write named register: InjVcalMed -> 500
[14:52:33:736][  info  ][     Rd53a     ]: Write named register: LatencyConfig -> 48
[14:52:33:736][  info  ][  scanConsole  ]: Starting histogrammer and analysis threads:
[14:52:33:736][  info  ][  scanConsole  ]:  .. started threads of Fe 4
[14:52:33:736][  info  ][  scanConsole  ]:  .. started threads of Fe 5
[14:52:33:736][  info  ][  scanConsole  ]:  .. started threads of Fe 6
[14:52:33:736][  info  ][  scanConsole  ]:  .. started threads of Fe 7
[14:52:33:736][  info  ][Rd53aDataProcessor]:   -> Processor thread #0 started!
[14:52:33:736][  info  ][Rd53aDataProcessor]:   -> Processor thread #1 started!
[14:52:33:736][  info  ][Rd53aDataProcessor]:   -> Processor thread #2 started!
[14:52:33:736][  info  ][Rd53aDataProcessor]:   -> Processor thread #3 started!
[14:52:33:736][  info  ][  scanConsole  ]: ########
[14:52:33:736][  info  ][  scanConsole  ]: # Scan #
[14:52:33:736][  info  ][  scanConsole  ]: ########
[14:52:33:736][  info  ][  scanConsole  ]: Starting scan!
[14:52:33:736][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0013396 on Rx 4
[14:52:33:740][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0013396] MON MUX_V: 3 Bias 0 1273 -> 0.265353 V Bias 1 1668 -> 0.344587 V, Temperature 0.0792347 -> -6.4447 C
[14:52:33:744][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0013396] MON MUX_V: 5 Bias 0 1240 -> 0.258733 V Bias 1 1626 -> 0.336162 V, Temperature 0.0774293 -> -7.66141 C
[14:52:33:748][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0013396] MON MUX_V: 7 Bias 0 1215 -> 0.253718 V Bias 1 1611 -> 0.333154 V, Temperature 0.0794353 -> -7.59256 C
[14:52:33:752][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0013396] MON MUX_V: 15 Bias 0 1223 -> 0.255323 V Bias 1 1625 -> 0.335962 V, Temperature 0.0806388 -> -6.59805 C
[14:52:33:786][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0013428 on Rx 5
[14:52:33:791][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0013428] MON MUX_V: 3 Bias 0 1252 -> 0.263487 V Bias 1 1628 -> 0.339657 V, Temperature 0.0761705 -> -10.8711 C
[14:52:33:795][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0013428] MON MUX_V: 5 Bias 0 1208 -> 0.254573 V Bias 1 1596 -> 0.333175 V, Temperature 0.0786015 -> -6.90631 C
[14:52:33:799][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0013428] MON MUX_V: 7 Bias 0 1209 -> 0.254776 V Bias 1 1596 -> 0.333175 V, Temperature 0.0783989 -> -9.43243 C
[14:52:33:803][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0013428] MON MUX_V: 15 Bias 0 1196 -> 0.252142 V Bias 1 1587 -> 0.331351 V, Temperature 0.0792092 -> -7.32059 C
[14:52:33:838][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0013381 on Rx 6
[14:52:33:843][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0013381] MON MUX_V: 3 Bias 0 1246 -> 0.268393 V Bias 1 1624 -> 0.344333 V, Temperature 0.0759395 -> -9.47601 C
[14:52:33:847][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0013381] MON MUX_V: 5 Bias 0 1207 -> 0.260558 V Bias 1 1602 -> 0.339913 V, Temperature 0.0793548 -> -8.86371 C
[14:52:33:851][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0013381] MON MUX_V: 7 Bias 0 1193 -> 0.257746 V Bias 1 1585 -> 0.336498 V, Temperature 0.0787521 -> -9.07452 C
[14:52:33:855][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0013381] MON MUX_V: 15 Bias 0 1166 -> 0.252321 V Bias 1 1564 -> 0.332279 V, Temperature 0.0799575 -> -10.431 C
[14:52:33:890][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0013349 on Rx 7
[14:52:33:894][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0013349] MON MUX_V: 3 Bias 0 1291 -> 0.26606 V Bias 1 1673 -> 0.34289 V, Temperature 0.0768296 -> -7.31415 C
[14:52:33:898][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0013349] MON MUX_V: 5 Bias 0 1255 -> 0.25882 V Bias 1 1641 -> 0.336454 V, Temperature 0.0776341 -> -7.04105 C
[14:52:33:902][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0013349] MON MUX_V: 7 Bias 0 1237 -> 0.2552 V Bias 1 1629 -> 0.33404 V, Temperature 0.0788409 -> -6.02533 C
[14:52:33:906][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0013349] MON MUX_V: 15 Bias 0 1244 -> 0.256608 V Bias 1 1634 -> 0.335046 V, Temperature 0.0784386 -> -5.53391 C
[14:52:33:941][  info  ][  scanConsole  ]: Scan done!
[14:52:33:941][  info  ][  scanConsole  ]: Waiting for processors to finish ...
[14:52:33:942][  info  ][  scanConsole  ]: Processor done, waiting for histogrammer ...
[14:52:34:342][  info  ][HistogramAlgorithm]: Histogrammer done!
[14:52:34:342][  info  ][HistogramAlgorithm]: Histogrammer done!
[14:52:34:342][  info  ][HistogramAlgorithm]: Histogrammer done!
[14:52:34:342][  info  ][HistogramAlgorithm]: Histogrammer done!
[14:52:34:342][  info  ][  scanConsole  ]: Processor done, waiting for analysis ...
[14:52:34:742][  info  ][AnalysisAlgorithm]: Analysis done!
[14:52:34:742][  info  ][AnalysisAlgorithm]: Analysis done!
[14:52:34:742][  info  ][AnalysisAlgorithm]: Analysis done!
[14:52:34:742][  info  ][AnalysisAlgorithm]: Analysis done!
[14:52:34:743][  info  ][  scanConsole  ]: All done!
[14:52:34:743][  info  ][  scanConsole  ]: ##########
[14:52:34:743][  info  ][  scanConsole  ]: # Timing #
[14:52:34:743][  info  ][  scanConsole  ]: ##########
[14:52:34:743][  info  ][  scanConsole  ]: -> Configuration: 121 ms
[14:52:34:743][  info  ][  scanConsole  ]: -> Scan:          205 ms
[14:52:34:743][  info  ][  scanConsole  ]: -> Processing:    0 ms
[14:52:34:743][  info  ][  scanConsole  ]: -> Analysis:      800 ms
[14:52:34:743][  info  ][  scanConsole  ]: ###########
[14:52:34:743][  info  ][  scanConsole  ]: # Cleanup #
[14:52:34:743][  info  ][  scanConsole  ]: ###########
[14:52:34:751][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0013396 to configs/KEKQ17/20UPGFC0013396.json
[14:52:34:921][  info  ][  scanConsole  ]: -> Storing output results of FE 4
[14:52:34:921][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0013396, this usually means that the chip did not send any data at all.
[14:52:34:922][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0013428 to configs/KEKQ17/20UPGFC0013428.json
[14:52:35:088][  info  ][  scanConsole  ]: -> Storing output results of FE 5
[14:52:35:088][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0013428, this usually means that the chip did not send any data at all.
[14:52:35:089][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0013381 to configs/KEKQ17/20UPGFC0013381.json
[14:52:35:252][  info  ][  scanConsole  ]: -> Storing output results of FE 6
[14:52:35:252][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0013381, this usually means that the chip did not send any data at all.
[14:52:35:253][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0013349 to configs/KEKQ17/20UPGFC0013349.json
[14:52:35:417][  info  ][  scanConsole  ]: -> Storing output results of FE 7
[14:52:35:417][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0013349, this usually means that the chip did not send any data at all.
[14:52:35:419][  info  ][  scanConsole  ]: Finishing run: 1814
20UPGFC0013349.json.after
20UPGFC0013349.json.before
20UPGFC0013381.json.after
20UPGFC0013381.json.before
20UPGFC0013396.json.after
20UPGFC0013396.json.before
20UPGFC0013428.json.after
20UPGFC0013428.json.before
reg_readtemp.json
scanLog.json

