[14:42:44:597][  info  ][               ]: #####################################
[14:42:44:598][  info  ][               ]: # Welcome to the YARR Scan Console! #
[14:42:44:598][  info  ][               ]: #####################################
[14:42:44:598][  info  ][               ]: -> Parsing command line parameters ...
[14:42:44:598][  info  ][               ]: Configuring logger ...
[14:42:44:600][  info  ][  scanConsole  ]: Scan Type/Config configs/scans/rd53a/reg_readtemp.json
[14:42:44:600][  info  ][  scanConsole  ]: Connectivity:
[14:42:44:600][  info  ][  scanConsole  ]:     configs/KEKQ17/connectivity.json
[14:42:44:600][  info  ][  scanConsole  ]: Target ToT: -1
[14:42:44:600][  info  ][  scanConsole  ]: Target Charge: -1
[14:42:44:600][  info  ][  scanConsole  ]: Output Plots: true
[14:42:44:600][  info  ][  scanConsole  ]: Output Directory: ./data/001812_reg_readtemp/
[14:42:44:604][  info  ][  scanConsole  ]: Timestamp: 2022-08-25_14:42:44
[14:42:44:604][  info  ][  scanConsole  ]: Run Number: 1812
[14:42:44:604][  info  ][  scanConsole  ]: #################
[14:42:44:604][  info  ][  scanConsole  ]: # Init Hardware #
[14:42:44:604][  info  ][  scanConsole  ]: #################
[14:42:44:604][  info  ][  scanConsole  ]: -> Opening controller config: configs/controller/specCfg-rd53a-16x1.json
[14:42:44:605][  info  ][  ScanHelper   ]: Loading controller ...
[14:42:44:605][  info  ][  ScanHelper   ]: Found controller of type: spec
[14:42:44:605][  info  ][  ScanHelper   ]: ... loading controler config:
[14:42:44:605][  info  ][  ScanHelper   ]: ~~~ {
[14:42:44:605][  info  ][  ScanHelper   ]: ~~~     "cmdPeriod": 6.250000073038109e-9,
[14:42:44:605][  info  ][  ScanHelper   ]: ~~~     "idle": {
[14:42:44:605][  info  ][  ScanHelper   ]: ~~~         "word": 1768515945
[14:42:44:605][  info  ][  ScanHelper   ]: ~~~     },
[14:42:44:605][  info  ][  ScanHelper   ]: ~~~     "pulse": {
[14:42:44:605][  info  ][  ScanHelper   ]: ~~~         "interval": 500,
[14:42:44:605][  info  ][  ScanHelper   ]: ~~~         "word": 1549575846
[14:42:44:605][  info  ][  ScanHelper   ]: ~~~     },
[14:42:44:605][  info  ][  ScanHelper   ]: ~~~     "rxActiveLanes": 1,
[14:42:44:605][  info  ][  ScanHelper   ]: ~~~     "rxPolarity": 0,
[14:42:44:605][  info  ][  ScanHelper   ]: ~~~     "specNum": 0,
[14:42:44:605][  info  ][  ScanHelper   ]: ~~~     "spiConfig": 541200,
[14:42:44:605][  info  ][  ScanHelper   ]: ~~~     "sync": {
[14:42:44:605][  info  ][  ScanHelper   ]: ~~~         "interval": 32,
[14:42:44:605][  info  ][  ScanHelper   ]: ~~~         "word": 2172551550
[14:42:44:605][  info  ][  ScanHelper   ]: ~~~     },
[14:42:44:605][  info  ][  ScanHelper   ]: ~~~     "txPolarity": 0
[14:42:44:605][  info  ][  ScanHelper   ]: ~~~ }
[14:42:44:605][  info  ][    SpecCom    ]: Opening SPEC with id #0
[14:42:44:605][  info  ][    SpecCom    ]: Mapping BARs ...
[14:42:44:605][  info  ][    SpecCom    ]: ... Mapped BAR0 at 0x7f3b781f0000 with size 1048576
[14:42:44:605][warning ][    SpecCom    ]: ... BAR4 not mapped (Mmap failed)
[14:42:44:605][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[14:42:44:605][  info  ][    SpecCom    ]: Firmware Version: 0x4d9ff6d
[14:42:44:605][  info  ][    SpecCom    ]: Firmware Identifier: 0x4030232
[14:42:44:605][  info  ][    SpecCom    ]: FPGA card: PLDA XpressK7 325
[14:42:44:605][  info  ][    SpecCom    ]: FE Chip Type: RD53A/B
[14:42:44:605][  info  ][    SpecCom    ]: FMC Card Type: Ohio Card (Display Port)
[14:42:44:605][  info  ][    SpecCom    ]: RX Speed: 640Mbps
[14:42:44:605][  info  ][    SpecCom    ]: Channel Configuration: 16x1
[14:42:44:605][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[14:42:44:605][  info  ][    SpecCom    ]: Flushing buffers ...
[14:42:44:605][  info  ][    SpecCom    ]: Init success!
[14:42:44:605][  info  ][  scanConsole  ]: #######################
[14:42:44:605][  info  ][  scanConsole  ]: ##  Loading Configs  ##
[14:42:44:605][  info  ][  scanConsole  ]: #######################
[14:42:44:605][  info  ][  scanConsole  ]: Opening global config: configs/KEKQ17/connectivity.json
[14:42:44:605][  info  ][  ScanHelper   ]: Chip type: RD53A
[14:42:44:605][  info  ][  ScanHelper   ]: Chip count 4
[14:42:44:605][  info  ][  ScanHelper   ]: Loading chip #0
[14:42:44:606][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(4)
[14:42:44:606][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ17/20UPGFC0013396.json
[14:42:44:786][  info  ][  ScanHelper   ]: Loading chip #1
[14:42:44:786][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(5)
[14:42:44:786][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ17/20UPGFC0013428.json
[14:42:44:966][  info  ][  ScanHelper   ]: Loading chip #2
[14:42:44:966][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(6)
[14:42:44:966][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ17/20UPGFC0013381.json
[14:42:45:142][  info  ][  ScanHelper   ]: Loading chip #3
[14:42:45:142][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(7)
[14:42:45:142][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ17/20UPGFC0013349.json
[14:42:45:321][  info  ][  scanConsole  ]: #################
[14:42:45:321][  info  ][  scanConsole  ]: # Configure FEs #
[14:42:45:321][  info  ][  scanConsole  ]: #################
[14:42:45:321][  info  ][  scanConsole  ]: Configuring 20UPGFC0013396
[14:42:45:351][  info  ][  scanConsole  ]: Configuring 20UPGFC0013428
[14:42:45:382][  info  ][  scanConsole  ]: Configuring 20UPGFC0013381
[14:42:45:412][  info  ][  scanConsole  ]: Configuring 20UPGFC0013349
[14:42:45:443][  info  ][  scanConsole  ]: Sent configuration to all FEs in 121 ms!
[14:42:45:444][  info  ][  scanConsole  ]: Checking com 20UPGFC0013396
[14:42:45:444][  info  ][    SpecRx     ]: Active Rx channels: 0x10
[14:42:45:444][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[14:42:45:444][  info  ][    SpecRx     ]: Rx Status 0xf0
[14:42:45:444][  info  ][    SpecRx     ]: Number of lanes: 1
[14:42:45:444][  info  ][    SpecRx     ]: Channel 4 Lane 0 synchronized!
[14:42:45:454][  info  ][  scanConsole  ]: ... success!
[14:42:45:454][  info  ][  scanConsole  ]: Checking com 20UPGFC0013428
[14:42:45:454][  info  ][    SpecRx     ]: Active Rx channels: 0x20
[14:42:45:454][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[14:42:45:454][  info  ][    SpecRx     ]: Rx Status 0xf0
[14:42:45:454][  info  ][    SpecRx     ]: Number of lanes: 1
[14:42:45:454][  info  ][    SpecRx     ]: Channel 5 Lane 0 synchronized!
[14:42:45:465][  info  ][  scanConsole  ]: ... success!
[14:42:45:465][  info  ][  scanConsole  ]: Checking com 20UPGFC0013381
[14:42:45:465][  info  ][    SpecRx     ]: Active Rx channels: 0x40
[14:42:45:465][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[14:42:45:465][  info  ][    SpecRx     ]: Rx Status 0xf0
[14:42:45:465][  info  ][    SpecRx     ]: Number of lanes: 1
[14:42:45:465][  info  ][    SpecRx     ]: Channel 6 Lane 0 synchronized!
[14:42:45:475][  info  ][  scanConsole  ]: ... success!
[14:42:45:475][  info  ][  scanConsole  ]: Checking com 20UPGFC0013349
[14:42:45:475][  info  ][    SpecRx     ]: Active Rx channels: 0x80
[14:42:45:475][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[14:42:45:475][  info  ][    SpecRx     ]: Rx Status 0xf0
[14:42:45:475][  info  ][    SpecRx     ]: Number of lanes: 1
[14:42:45:475][  info  ][    SpecRx     ]: Channel 7 Lane 0 synchronized!
[14:42:45:485][  info  ][  scanConsole  ]: ... success!
[14:42:45:485][  info  ][  scanConsole  ]: Enabling Tx channels
[14:42:45:485][  info  ][  scanConsole  ]: Enabling Tx channel 1
[14:42:45:485][  info  ][  scanConsole  ]: Enabling Tx channel 1
[14:42:45:486][  info  ][  scanConsole  ]: Enabling Tx channel 1
[14:42:45:486][  info  ][  scanConsole  ]: Enabling Tx channel 1
[14:42:45:486][  info  ][  scanConsole  ]: Enabling Rx channels
[14:42:45:486][  info  ][  scanConsole  ]: Enabling Rx channel 4
[14:42:45:486][  info  ][  scanConsole  ]: Enabling Rx channel 5
[14:42:45:486][  info  ][  scanConsole  ]: Enabling Rx channel 6
[14:42:45:486][  info  ][  scanConsole  ]: Enabling Rx channel 7
[14:42:45:486][  info  ][  scanConsole  ]: ##############
[14:42:45:486][  info  ][  scanConsole  ]: # Setup Scan #
[14:42:45:486][  info  ][  scanConsole  ]: ##############
[14:42:45:486][  info  ][  scanConsole  ]: Found Scan config, constructing scan ...
[14:42:45:486][  info  ][  ScanFactory  ]: Loading Scan:
[14:42:45:486][  info  ][  ScanFactory  ]:   Name: AnalogScan
[14:42:45:486][  info  ][  ScanFactory  ]:   Number of Loops: 3
[14:42:45:486][  info  ][  ScanFactory  ]:   Loading Loop #0
[14:42:45:486][  info  ][  ScanFactory  ]:    Type: Rd53aReadRegLoop
[14:42:45:486][  info  ][  ScanFactory  ]:    Loading loop config ... 
[14:42:45:486][  info  ][  ScanFactory  ]: ~~~ {
[14:42:45:486][  info  ][  ScanFactory  ]: ~~~     "CurMux": [],
[14:42:45:486][  info  ][  ScanFactory  ]: ~~~     "EnblRingOsc": 0,
[14:42:45:486][  info  ][  ScanFactory  ]: ~~~     "Registers": [],
[14:42:45:486][  info  ][  ScanFactory  ]: ~~~     "RingOscDur": 9,
[14:42:45:486][  info  ][  ScanFactory  ]: ~~~     "RingOscRep": 10,
[14:42:45:486][  info  ][  ScanFactory  ]: ~~~     "VoltMux": [3, 5, 7, 15]
[14:42:45:486][  info  ][  ScanFactory  ]: ~~~ }
[14:42:45:486][  info  ][  ScanFactory  ]:   Loading Loop #1
[14:42:45:486][  info  ][  ScanFactory  ]:    Type: Rd53aTriggerLoop
[14:42:45:486][  info  ][  ScanFactory  ]:    Loading loop config ... 
[14:42:45:486][  info  ][  ScanFactory  ]: ~~~ {
[14:42:45:486][  info  ][  ScanFactory  ]: ~~~     "count": 1,
[14:42:45:486][  info  ][  ScanFactory  ]: ~~~     "delay": 48,
[14:42:45:486][  info  ][  ScanFactory  ]: ~~~     "extTrig": false,
[14:42:45:486][  info  ][  ScanFactory  ]: ~~~     "frequency": 30000.0,
[14:42:45:486][  info  ][  ScanFactory  ]: ~~~     "noInject": false,
[14:42:45:486][  info  ][  ScanFactory  ]: ~~~     "sendEcr": false,
[14:42:45:486][  info  ][  ScanFactory  ]: ~~~     "time": 0.0,
[14:42:45:486][  info  ][  ScanFactory  ]: ~~~     "trigMultiplier": 16
[14:42:45:486][  info  ][  ScanFactory  ]: ~~~ }
[14:42:45:486][  info  ][  ScanFactory  ]:   Loading Loop #2
[14:42:45:486][  info  ][  ScanFactory  ]:    Type: StdDataLoop
[14:42:45:486][warning ][  ScanFactory  ]: ~~~ Config empty.
[14:42:45:486][  info  ][ScanBuildHistogrammers]: Loading histogrammer ...
[14:42:45:486][  info  ][ScanBuildHistogrammers]: ... done!
[14:42:45:486][  info  ][ScanBuildAnalyses]: Loading analyses ...
[14:42:45:486][  info  ][  scanConsole  ]: Running pre scan!
[14:42:45:486][  info  ][  ScanFactory  ]: Entering pre scan phase ...
[14:42:45:486][  info  ][     Rd53a     ]: Write named register: GlobalPulseRt -> 16384
[14:42:45:486][  info  ][     Rd53a     ]: Write named register: InjEnDig -> 0
[14:42:45:486][  info  ][     Rd53a     ]: Write named register: InjVcalDiff -> 0
[14:42:45:486][  info  ][     Rd53a     ]: Write named register: InjVcalHigh -> 3500
[14:42:45:486][  info  ][     Rd53a     ]: Write named register: InjVcalMed -> 500
[14:42:45:486][  info  ][     Rd53a     ]: Write named register: LatencyConfig -> 48
[14:42:45:486][  info  ][  scanConsole  ]: Starting histogrammer and analysis threads:
[14:42:45:486][  info  ][  scanConsole  ]:  .. started threads of Fe 4
[14:42:45:486][  info  ][  scanConsole  ]:  .. started threads of Fe 5
[14:42:45:486][  info  ][  scanConsole  ]:  .. started threads of Fe 6
[14:42:45:486][  info  ][  scanConsole  ]:  .. started threads of Fe 7
[14:42:45:486][  info  ][Rd53aDataProcessor]:   -> Processor thread #0 started!
[14:42:45:486][  info  ][Rd53aDataProcessor]:   -> Processor thread #1 started!
[14:42:45:486][  info  ][Rd53aDataProcessor]:   -> Processor thread #2 started!
[14:42:45:486][  info  ][Rd53aDataProcessor]:   -> Processor thread #3 started!
[14:42:45:486][  info  ][  scanConsole  ]: ########
[14:42:45:486][  info  ][  scanConsole  ]: # Scan #
[14:42:45:486][  info  ][  scanConsole  ]: ########
[14:42:45:486][  info  ][  scanConsole  ]: Starting scan!
[14:42:45:487][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0013396 on Rx 4
[14:42:45:490][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0013396] MON MUX_V: 3 Bias 0 1272 -> 0.265152 V Bias 1 1665 -> 0.343986 V, Temperature 0.0788335 -> -7.79517 C
[14:42:45:494][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0013396] MON MUX_V: 5 Bias 0 1240 -> 0.258733 V Bias 1 1626 -> 0.336162 V, Temperature 0.0774293 -> -7.66141 C
[14:42:45:498][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0013396] MON MUX_V: 7 Bias 0 1214 -> 0.253518 V Bias 1 1610 -> 0.332953 V, Temperature 0.0794353 -> -7.59256 C
[14:42:45:502][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0013396] MON MUX_V: 15 Bias 0 1222 -> 0.255122 V Bias 1 1626 -> 0.336162 V, Temperature 0.08104 -> -5.27188 C
[14:42:45:536][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0013428 on Rx 5
[14:42:45:541][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0013428] MON MUX_V: 3 Bias 0 1252 -> 0.263487 V Bias 1 1628 -> 0.339657 V, Temperature 0.0761705 -> -10.8711 C
[14:42:45:545][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0013428] MON MUX_V: 5 Bias 0 1208 -> 0.254573 V Bias 1 1596 -> 0.333175 V, Temperature 0.0786015 -> -6.90631 C
[14:42:45:549][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0013428] MON MUX_V: 7 Bias 0 1209 -> 0.254776 V Bias 1 1595 -> 0.332972 V, Temperature 0.0781963 -> -10.1139 C
[14:42:45:553][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0013428] MON MUX_V: 15 Bias 0 1196 -> 0.252142 V Bias 1 1587 -> 0.331351 V, Temperature 0.0792092 -> -7.32059 C
[14:42:45:588][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0013381 on Rx 6
[14:42:45:592][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0013381] MON MUX_V: 3 Bias 0 1245 -> 0.268192 V Bias 1 1624 -> 0.344333 V, Temperature 0.0761404 -> -8.77847 C
[14:42:45:597][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0013381] MON MUX_V: 5 Bias 0 1207 -> 0.260558 V Bias 1 1600 -> 0.339511 V, Temperature 0.078953 -> -10.2019 C
[14:42:45:601][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0013381] MON MUX_V: 7 Bias 0 1191 -> 0.257344 V Bias 1 1585 -> 0.336498 V, Temperature 0.0791539 -> -7.72729 C
[14:42:45:605][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0013381] MON MUX_V: 15 Bias 0 1166 -> 0.252321 V Bias 1 1563 -> 0.332078 V, Temperature 0.0797566 -> -11.091 C
[14:42:45:639][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0013349 on Rx 7
[14:42:45:644][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0013349] MON MUX_V: 3 Bias 0 1290 -> 0.265859 V Bias 1 1674 -> 0.343091 V, Temperature 0.0772319 -> -5.92236 C
[14:42:45:648][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0013349] MON MUX_V: 5 Bias 0 1255 -> 0.25882 V Bias 1 1641 -> 0.336454 V, Temperature 0.0776341 -> -7.04105 C
[14:42:45:652][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0013349] MON MUX_V: 7 Bias 0 1236 -> 0.254999 V Bias 1 1629 -> 0.33404 V, Temperature 0.079042 -> -5.34384 C
[14:42:45:656][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0013349] MON MUX_V: 15 Bias 0 1244 -> 0.256608 V Bias 1 1633 -> 0.334845 V, Temperature 0.0782375 -> -6.22015 C
[14:42:45:692][  info  ][  scanConsole  ]: Scan done!
[14:42:45:693][  info  ][  scanConsole  ]: Waiting for processors to finish ...
[14:42:45:693][  info  ][  scanConsole  ]: Processor done, waiting for histogrammer ...
[14:42:46:093][  info  ][HistogramAlgorithm]: Histogrammer done!
[14:42:46:093][  info  ][HistogramAlgorithm]: Histogrammer done!
[14:42:46:093][  info  ][HistogramAlgorithm]: Histogrammer done!
[14:42:46:093][  info  ][HistogramAlgorithm]: Histogrammer done!
[14:42:46:093][  info  ][  scanConsole  ]: Processor done, waiting for analysis ...
[14:42:46:494][  info  ][AnalysisAlgorithm]: Analysis done!
[14:42:46:494][  info  ][AnalysisAlgorithm]: Analysis done!
[14:42:46:494][  info  ][AnalysisAlgorithm]: Analysis done!
[14:42:46:494][  info  ][AnalysisAlgorithm]: Analysis done!
[14:42:46:494][  info  ][  scanConsole  ]: All done!
[14:42:46:494][  info  ][  scanConsole  ]: ##########
[14:42:46:494][  info  ][  scanConsole  ]: # Timing #
[14:42:46:494][  info  ][  scanConsole  ]: ##########
[14:42:46:494][  info  ][  scanConsole  ]: -> Configuration: 121 ms
[14:42:46:494][  info  ][  scanConsole  ]: -> Scan:          206 ms
[14:42:46:494][  info  ][  scanConsole  ]: -> Processing:    0 ms
[14:42:46:494][  info  ][  scanConsole  ]: -> Analysis:      800 ms
[14:42:46:494][  info  ][  scanConsole  ]: ###########
[14:42:46:494][  info  ][  scanConsole  ]: # Cleanup #
[14:42:46:494][  info  ][  scanConsole  ]: ###########
[14:42:46:502][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0013396 to configs/KEKQ17/20UPGFC0013396.json
[14:42:46:676][  info  ][  scanConsole  ]: -> Storing output results of FE 4
[14:42:46:676][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0013396, this usually means that the chip did not send any data at all.
[14:42:46:678][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0013428 to configs/KEKQ17/20UPGFC0013428.json
[14:42:46:853][  info  ][  scanConsole  ]: -> Storing output results of FE 5
[14:42:46:853][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0013428, this usually means that the chip did not send any data at all.
[14:42:46:855][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0013381 to configs/KEKQ17/20UPGFC0013381.json
[14:42:47:047][  info  ][  scanConsole  ]: -> Storing output results of FE 6
[14:42:47:047][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0013381, this usually means that the chip did not send any data at all.
[14:42:47:049][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0013349 to configs/KEKQ17/20UPGFC0013349.json
[14:42:47:241][  info  ][  scanConsole  ]: -> Storing output results of FE 7
[14:42:47:241][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0013349, this usually means that the chip did not send any data at all.
[14:42:47:243][  info  ][  scanConsole  ]: Finishing run: 1812
20UPGFC0013349.json.after
20UPGFC0013349.json.before
20UPGFC0013381.json.after
20UPGFC0013381.json.before
20UPGFC0013396.json.after
20UPGFC0013396.json.before
20UPGFC0013428.json.after
20UPGFC0013428.json.before
reg_readtemp.json
scanLog.json

