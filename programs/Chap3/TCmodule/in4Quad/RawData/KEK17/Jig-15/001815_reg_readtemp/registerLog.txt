[15:01:28:370][  info  ][               ]: #####################################
[15:01:28:370][  info  ][               ]: # Welcome to the YARR Scan Console! #
[15:01:28:370][  info  ][               ]: #####################################
[15:01:28:370][  info  ][               ]: -> Parsing command line parameters ...
[15:01:28:370][  info  ][               ]: Configuring logger ...
[15:01:28:373][  info  ][  scanConsole  ]: Scan Type/Config configs/scans/rd53a/reg_readtemp.json
[15:01:28:373][  info  ][  scanConsole  ]: Connectivity:
[15:01:28:373][  info  ][  scanConsole  ]:     configs/KEKQ17/connectivity.json
[15:01:28:373][  info  ][  scanConsole  ]: Target ToT: -1
[15:01:28:373][  info  ][  scanConsole  ]: Target Charge: -1
[15:01:28:373][  info  ][  scanConsole  ]: Output Plots: true
[15:01:28:373][  info  ][  scanConsole  ]: Output Directory: ./data/001815_reg_readtemp/
[15:01:28:378][  info  ][  scanConsole  ]: Timestamp: 2022-08-25_15:01:28
[15:01:28:378][  info  ][  scanConsole  ]: Run Number: 1815
[15:01:28:378][  info  ][  scanConsole  ]: #################
[15:01:28:378][  info  ][  scanConsole  ]: # Init Hardware #
[15:01:28:378][  info  ][  scanConsole  ]: #################
[15:01:28:378][  info  ][  scanConsole  ]: -> Opening controller config: configs/controller/specCfg-rd53a-16x1.json
[15:01:28:378][  info  ][  ScanHelper   ]: Loading controller ...
[15:01:28:378][  info  ][  ScanHelper   ]: Found controller of type: spec
[15:01:28:378][  info  ][  ScanHelper   ]: ... loading controler config:
[15:01:28:378][  info  ][  ScanHelper   ]: ~~~ {
[15:01:28:378][  info  ][  ScanHelper   ]: ~~~     "cmdPeriod": 6.250000073038109e-9,
[15:01:28:378][  info  ][  ScanHelper   ]: ~~~     "idle": {
[15:01:28:378][  info  ][  ScanHelper   ]: ~~~         "word": 1768515945
[15:01:28:378][  info  ][  ScanHelper   ]: ~~~     },
[15:01:28:378][  info  ][  ScanHelper   ]: ~~~     "pulse": {
[15:01:28:378][  info  ][  ScanHelper   ]: ~~~         "interval": 500,
[15:01:28:378][  info  ][  ScanHelper   ]: ~~~         "word": 1549575846
[15:01:28:378][  info  ][  ScanHelper   ]: ~~~     },
[15:01:28:378][  info  ][  ScanHelper   ]: ~~~     "rxActiveLanes": 1,
[15:01:28:378][  info  ][  ScanHelper   ]: ~~~     "rxPolarity": 0,
[15:01:28:378][  info  ][  ScanHelper   ]: ~~~     "specNum": 0,
[15:01:28:379][  info  ][  ScanHelper   ]: ~~~     "spiConfig": 541200,
[15:01:28:379][  info  ][  ScanHelper   ]: ~~~     "sync": {
[15:01:28:379][  info  ][  ScanHelper   ]: ~~~         "interval": 32,
[15:01:28:379][  info  ][  ScanHelper   ]: ~~~         "word": 2172551550
[15:01:28:379][  info  ][  ScanHelper   ]: ~~~     },
[15:01:28:379][  info  ][  ScanHelper   ]: ~~~     "txPolarity": 0
[15:01:28:379][  info  ][  ScanHelper   ]: ~~~ }
[15:01:28:379][  info  ][    SpecCom    ]: Opening SPEC with id #0
[15:01:28:379][  info  ][    SpecCom    ]: Mapping BARs ...
[15:01:28:379][  info  ][    SpecCom    ]: ... Mapped BAR0 at 0x7fd27115d000 with size 1048576
[15:01:28:379][warning ][    SpecCom    ]: ... BAR4 not mapped (Mmap failed)
[15:01:28:379][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[15:01:28:379][  info  ][    SpecCom    ]: Firmware Version: 0x4d9ff6d
[15:01:28:379][  info  ][    SpecCom    ]: Firmware Identifier: 0x4030232
[15:01:28:379][  info  ][    SpecCom    ]: FPGA card: PLDA XpressK7 325
[15:01:28:379][  info  ][    SpecCom    ]: FE Chip Type: RD53A/B
[15:01:28:379][  info  ][    SpecCom    ]: FMC Card Type: Ohio Card (Display Port)
[15:01:28:379][  info  ][    SpecCom    ]: RX Speed: 640Mbps
[15:01:28:379][  info  ][    SpecCom    ]: Channel Configuration: 16x1
[15:01:28:379][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[15:01:28:379][  info  ][    SpecCom    ]: Flushing buffers ...
[15:01:28:379][  info  ][    SpecCom    ]: Init success!
[15:01:28:379][  info  ][  scanConsole  ]: #######################
[15:01:28:379][  info  ][  scanConsole  ]: ##  Loading Configs  ##
[15:01:28:379][  info  ][  scanConsole  ]: #######################
[15:01:28:379][  info  ][  scanConsole  ]: Opening global config: configs/KEKQ17/connectivity.json
[15:01:28:379][  info  ][  ScanHelper   ]: Chip type: RD53A
[15:01:28:379][  info  ][  ScanHelper   ]: Chip count 4
[15:01:28:379][  info  ][  ScanHelper   ]: Loading chip #0
[15:01:28:380][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(4)
[15:01:28:380][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ17/20UPGFC0013396.json
[15:01:28:560][  info  ][  ScanHelper   ]: Loading chip #1
[15:01:28:560][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(5)
[15:01:28:560][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ17/20UPGFC0013428.json
[15:01:28:735][  info  ][  ScanHelper   ]: Loading chip #2
[15:01:28:736][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(6)
[15:01:28:736][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ17/20UPGFC0013381.json
[15:01:28:915][  info  ][  ScanHelper   ]: Loading chip #3
[15:01:28:915][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(7)
[15:01:28:915][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ17/20UPGFC0013349.json
[15:01:29:095][  info  ][  scanConsole  ]: #################
[15:01:29:095][  info  ][  scanConsole  ]: # Configure FEs #
[15:01:29:095][  info  ][  scanConsole  ]: #################
[15:01:29:095][  info  ][  scanConsole  ]: Configuring 20UPGFC0013396
[15:01:29:126][  info  ][  scanConsole  ]: Configuring 20UPGFC0013428
[15:01:29:156][  info  ][  scanConsole  ]: Configuring 20UPGFC0013381
[15:01:29:186][  info  ][  scanConsole  ]: Configuring 20UPGFC0013349
[15:01:29:229][  info  ][  scanConsole  ]: Sent configuration to all FEs in 134 ms!
[15:01:29:230][  info  ][  scanConsole  ]: Checking com 20UPGFC0013396
[15:01:29:230][  info  ][    SpecRx     ]: Active Rx channels: 0x10
[15:01:29:230][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[15:01:29:230][  info  ][    SpecRx     ]: Rx Status 0xf0
[15:01:29:230][  info  ][    SpecRx     ]: Number of lanes: 1
[15:01:29:230][  info  ][    SpecRx     ]: Channel 4 Lane 0 synchronized!
[15:01:29:241][  info  ][  scanConsole  ]: ... success!
[15:01:29:241][  info  ][  scanConsole  ]: Checking com 20UPGFC0013428
[15:01:29:241][  info  ][    SpecRx     ]: Active Rx channels: 0x20
[15:01:29:241][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[15:01:29:241][  info  ][    SpecRx     ]: Rx Status 0xf0
[15:01:29:241][  info  ][    SpecRx     ]: Number of lanes: 1
[15:01:29:241][  info  ][    SpecRx     ]: Channel 5 Lane 0 synchronized!
[15:01:29:251][  info  ][  scanConsole  ]: ... success!
[15:01:29:251][  info  ][  scanConsole  ]: Checking com 20UPGFC0013381
[15:01:29:251][  info  ][    SpecRx     ]: Active Rx channels: 0x40
[15:01:29:251][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[15:01:29:251][  info  ][    SpecRx     ]: Rx Status 0xf0
[15:01:29:251][  info  ][    SpecRx     ]: Number of lanes: 1
[15:01:29:251][  info  ][    SpecRx     ]: Channel 6 Lane 0 synchronized!
[15:01:29:261][  info  ][  scanConsole  ]: ... success!
[15:01:29:261][  info  ][  scanConsole  ]: Checking com 20UPGFC0013349
[15:01:29:261][  info  ][    SpecRx     ]: Active Rx channels: 0x80
[15:01:29:261][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[15:01:29:261][  info  ][    SpecRx     ]: Rx Status 0xf0
[15:01:29:262][  info  ][    SpecRx     ]: Number of lanes: 1
[15:01:29:262][  info  ][    SpecRx     ]: Channel 7 Lane 0 synchronized!
[15:01:29:272][  info  ][  scanConsole  ]: ... success!
[15:01:29:272][  info  ][  scanConsole  ]: Enabling Tx channels
[15:01:29:272][  info  ][  scanConsole  ]: Enabling Tx channel 1
[15:01:29:272][  info  ][  scanConsole  ]: Enabling Tx channel 1
[15:01:29:272][  info  ][  scanConsole  ]: Enabling Tx channel 1
[15:01:29:272][  info  ][  scanConsole  ]: Enabling Tx channel 1
[15:01:29:272][  info  ][  scanConsole  ]: Enabling Rx channels
[15:01:29:272][  info  ][  scanConsole  ]: Enabling Rx channel 4
[15:01:29:272][  info  ][  scanConsole  ]: Enabling Rx channel 5
[15:01:29:272][  info  ][  scanConsole  ]: Enabling Rx channel 6
[15:01:29:272][  info  ][  scanConsole  ]: Enabling Rx channel 7
[15:01:29:272][  info  ][  scanConsole  ]: ##############
[15:01:29:272][  info  ][  scanConsole  ]: # Setup Scan #
[15:01:29:272][  info  ][  scanConsole  ]: ##############
[15:01:29:273][  info  ][  scanConsole  ]: Found Scan config, constructing scan ...
[15:01:29:273][  info  ][  ScanFactory  ]: Loading Scan:
[15:01:29:273][  info  ][  ScanFactory  ]:   Name: AnalogScan
[15:01:29:273][  info  ][  ScanFactory  ]:   Number of Loops: 3
[15:01:29:273][  info  ][  ScanFactory  ]:   Loading Loop #0
[15:01:29:273][  info  ][  ScanFactory  ]:    Type: Rd53aReadRegLoop
[15:01:29:273][  info  ][  ScanFactory  ]:    Loading loop config ... 
[15:01:29:274][  info  ][  ScanFactory  ]: ~~~ {
[15:01:29:274][  info  ][  ScanFactory  ]: ~~~     "CurMux": [],
[15:01:29:274][  info  ][  ScanFactory  ]: ~~~     "EnblRingOsc": 0,
[15:01:29:274][  info  ][  ScanFactory  ]: ~~~     "Registers": [],
[15:01:29:274][  info  ][  ScanFactory  ]: ~~~     "RingOscDur": 9,
[15:01:29:274][  info  ][  ScanFactory  ]: ~~~     "RingOscRep": 10,
[15:01:29:274][  info  ][  ScanFactory  ]: ~~~     "VoltMux": [3, 5, 7, 15]
[15:01:29:274][  info  ][  ScanFactory  ]: ~~~ }
[15:01:29:274][  info  ][  ScanFactory  ]:   Loading Loop #1
[15:01:29:274][  info  ][  ScanFactory  ]:    Type: Rd53aTriggerLoop
[15:01:29:274][  info  ][  ScanFactory  ]:    Loading loop config ... 
[15:01:29:274][  info  ][  ScanFactory  ]: ~~~ {
[15:01:29:274][  info  ][  ScanFactory  ]: ~~~     "count": 1,
[15:01:29:274][  info  ][  ScanFactory  ]: ~~~     "delay": 48,
[15:01:29:274][  info  ][  ScanFactory  ]: ~~~     "extTrig": false,
[15:01:29:274][  info  ][  ScanFactory  ]: ~~~     "frequency": 30000.0,
[15:01:29:274][  info  ][  ScanFactory  ]: ~~~     "noInject": false,
[15:01:29:274][  info  ][  ScanFactory  ]: ~~~     "sendEcr": false,
[15:01:29:274][  info  ][  ScanFactory  ]: ~~~     "time": 0.0,
[15:01:29:274][  info  ][  ScanFactory  ]: ~~~     "trigMultiplier": 16
[15:01:29:274][  info  ][  ScanFactory  ]: ~~~ }
[15:01:29:274][  info  ][  ScanFactory  ]:   Loading Loop #2
[15:01:29:274][  info  ][  ScanFactory  ]:    Type: StdDataLoop
[15:01:29:274][warning ][  ScanFactory  ]: ~~~ Config empty.
[15:01:29:274][  info  ][ScanBuildHistogrammers]: Loading histogrammer ...
[15:01:29:275][  info  ][ScanBuildHistogrammers]: ... done!
[15:01:29:275][  info  ][ScanBuildAnalyses]: Loading analyses ...
[15:01:29:275][  info  ][  scanConsole  ]: Running pre scan!
[15:01:29:275][  info  ][  ScanFactory  ]: Entering pre scan phase ...
[15:01:29:275][  info  ][     Rd53a     ]: Write named register: GlobalPulseRt -> 16384
[15:01:29:275][  info  ][     Rd53a     ]: Write named register: InjEnDig -> 0
[15:01:29:275][  info  ][     Rd53a     ]: Write named register: InjVcalDiff -> 0
[15:01:29:275][  info  ][     Rd53a     ]: Write named register: InjVcalHigh -> 3500
[15:01:29:275][  info  ][     Rd53a     ]: Write named register: InjVcalMed -> 500
[15:01:29:275][  info  ][     Rd53a     ]: Write named register: LatencyConfig -> 48
[15:01:29:275][  info  ][  scanConsole  ]: Starting histogrammer and analysis threads:
[15:01:29:275][  info  ][  scanConsole  ]:  .. started threads of Fe 4
[15:01:29:276][  info  ][  scanConsole  ]:  .. started threads of Fe 5
[15:01:29:276][  info  ][  scanConsole  ]:  .. started threads of Fe 6
[15:01:29:276][  info  ][  scanConsole  ]:  .. started threads of Fe 7
[15:01:29:276][  info  ][Rd53aDataProcessor]:   -> Processor thread #0 started!
[15:01:29:276][  info  ][Rd53aDataProcessor]:   -> Processor thread #1 started!
[15:01:29:277][  info  ][Rd53aDataProcessor]:   -> Processor thread #2 started!
[15:01:29:277][  info  ][Rd53aDataProcessor]:   -> Processor thread #3 started!
[15:01:29:277][  info  ][  scanConsole  ]: ########
[15:01:29:277][  info  ][  scanConsole  ]: # Scan #
[15:01:29:277][  info  ][  scanConsole  ]: ########
[15:01:29:277][  info  ][  scanConsole  ]: Starting scan!
[15:01:29:277][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0013396 on Rx 4
[15:01:29:281][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0013396] MON MUX_V: 3 Bias 0 1273 -> 0.265353 V Bias 1 1665 -> 0.343986 V, Temperature 0.0786329 -> -8.47037 C
[15:01:29:285][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0013396] MON MUX_V: 5 Bias 0 1240 -> 0.258733 V Bias 1 1626 -> 0.336162 V, Temperature 0.0774293 -> -7.66141 C
[15:01:29:289][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0013396] MON MUX_V: 7 Bias 0 1215 -> 0.253718 V Bias 1 1610 -> 0.332953 V, Temperature 0.0792347 -> -8.26315 C
[15:01:29:293][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0013396] MON MUX_V: 15 Bias 0 1223 -> 0.255323 V Bias 1 1625 -> 0.335962 V, Temperature 0.0806388 -> -6.59805 C
[15:01:29:328][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0013428 on Rx 5
[15:01:29:332][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0013428] MON MUX_V: 3 Bias 0 1251 -> 0.263284 V Bias 1 1628 -> 0.339657 V, Temperature 0.076373 -> -10.1736 C
[15:01:29:337][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0013428] MON MUX_V: 5 Bias 0 1208 -> 0.254573 V Bias 1 1596 -> 0.333175 V, Temperature 0.0786015 -> -6.90631 C
[15:01:29:341][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0013428] MON MUX_V: 7 Bias 0 1209 -> 0.254776 V Bias 1 1595 -> 0.332972 V, Temperature 0.0781963 -> -10.1139 C
[15:01:29:345][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0013428] MON MUX_V: 15 Bias 0 1196 -> 0.252142 V Bias 1 1587 -> 0.331351 V, Temperature 0.0792092 -> -7.32059 C
[15:01:29:380][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0013381 on Rx 6
[15:01:29:384][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0013381] MON MUX_V: 3 Bias 0 1244 -> 0.267992 V Bias 1 1623 -> 0.344132 V, Temperature 0.0761404 -> -8.77838 C
[15:01:29:388][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0013381] MON MUX_V: 5 Bias 0 1206 -> 0.260357 V Bias 1 1600 -> 0.339511 V, Temperature 0.0791539 -> -9.53281 C
[15:01:29:392][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0013381] MON MUX_V: 7 Bias 0 1191 -> 0.257344 V Bias 1 1584 -> 0.336297 V, Temperature 0.0789529 -> -8.40106 C
[15:01:29:396][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0013381] MON MUX_V: 15 Bias 0 1165 -> 0.252121 V Bias 1 1564 -> 0.332279 V, Temperature 0.0801584 -> -9.7709 C
[15:01:29:432][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0013349 on Rx 7
[15:01:29:436][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0013349] MON MUX_V: 3 Bias 0 1291 -> 0.26606 V Bias 1 1673 -> 0.34289 V, Temperature 0.0768296 -> -7.31415 C
[15:01:29:440][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0013349] MON MUX_V: 5 Bias 0 1255 -> 0.25882 V Bias 1 1640 -> 0.336253 V, Temperature 0.077433 -> -7.73047 C
[15:01:29:444][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0013349] MON MUX_V: 7 Bias 0 1236 -> 0.254999 V Bias 1 1630 -> 0.334242 V, Temperature 0.0792431 -> -4.66238 C
[15:01:29:448][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0013349] MON MUX_V: 15 Bias 0 1244 -> 0.256608 V Bias 1 1632 -> 0.334644 V, Temperature 0.0780364 -> -6.90628 C
[15:01:29:485][  info  ][  scanConsole  ]: Scan done!
[15:01:29:485][  info  ][  scanConsole  ]: Waiting for processors to finish ...
[15:01:29:486][  info  ][  scanConsole  ]: Processor done, waiting for histogrammer ...
[15:01:29:885][  info  ][HistogramAlgorithm]: Histogrammer done!
[15:01:29:885][  info  ][HistogramAlgorithm]: Histogrammer done!
[15:01:29:885][  info  ][HistogramAlgorithm]: Histogrammer done!
[15:01:29:885][  info  ][HistogramAlgorithm]: Histogrammer done!
[15:01:29:886][  info  ][  scanConsole  ]: Processor done, waiting for analysis ...
[15:01:30:286][  info  ][AnalysisAlgorithm]: Analysis done!
[15:01:30:286][  info  ][AnalysisAlgorithm]: Analysis done!
[15:01:30:286][  info  ][AnalysisAlgorithm]: Analysis done!
[15:01:30:286][  info  ][AnalysisAlgorithm]: Analysis done!
[15:01:30:286][  info  ][  scanConsole  ]: All done!
[15:01:30:286][  info  ][  scanConsole  ]: ##########
[15:01:30:286][  info  ][  scanConsole  ]: # Timing #
[15:01:30:286][  info  ][  scanConsole  ]: ##########
[15:01:30:286][  info  ][  scanConsole  ]: -> Configuration: 134 ms
[15:01:30:286][  info  ][  scanConsole  ]: -> Scan:          208 ms
[15:01:30:286][  info  ][  scanConsole  ]: -> Processing:    0 ms
[15:01:30:286][  info  ][  scanConsole  ]: -> Analysis:      800 ms
[15:01:30:286][  info  ][  scanConsole  ]: ###########
[15:01:30:286][  info  ][  scanConsole  ]: # Cleanup #
[15:01:30:286][  info  ][  scanConsole  ]: ###########
[15:01:30:289][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0013396 to configs/KEKQ17/20UPGFC0013396.json
[15:01:30:487][  info  ][  scanConsole  ]: -> Storing output results of FE 4
[15:01:30:488][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0013396, this usually means that the chip did not send any data at all.
[15:01:30:489][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0013428 to configs/KEKQ17/20UPGFC0013428.json
[15:01:30:655][  info  ][  scanConsole  ]: -> Storing output results of FE 5
[15:01:30:655][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0013428, this usually means that the chip did not send any data at all.
[15:01:30:656][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0013381 to configs/KEKQ17/20UPGFC0013381.json
[15:01:30:819][  info  ][  scanConsole  ]: -> Storing output results of FE 6
[15:01:30:819][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0013381, this usually means that the chip did not send any data at all.
[15:01:30:820][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0013349 to configs/KEKQ17/20UPGFC0013349.json
[15:01:30:983][  info  ][  scanConsole  ]: -> Storing output results of FE 7
[15:01:30:983][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0013349, this usually means that the chip did not send any data at all.
[15:01:30:985][  info  ][  scanConsole  ]: Finishing run: 1815
20UPGFC0013349.json.after
20UPGFC0013349.json.before
20UPGFC0013381.json.after
20UPGFC0013381.json.before
20UPGFC0013396.json.after
20UPGFC0013396.json.before
20UPGFC0013428.json.after
20UPGFC0013428.json.before
reg_readtemp.json
scanLog.json

