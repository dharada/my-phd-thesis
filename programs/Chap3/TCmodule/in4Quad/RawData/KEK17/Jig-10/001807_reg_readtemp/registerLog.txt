[14:16:47:290][  info  ][               ]: #####################################
[14:16:47:290][  info  ][               ]: # Welcome to the YARR Scan Console! #
[14:16:47:290][  info  ][               ]: #####################################
[14:16:47:290][  info  ][               ]: -> Parsing command line parameters ...
[14:16:47:290][  info  ][               ]: Configuring logger ...
[14:16:47:294][  info  ][  scanConsole  ]: Scan Type/Config configs/scans/rd53a/reg_readtemp.json
[14:16:47:294][  info  ][  scanConsole  ]: Connectivity:
[14:16:47:294][  info  ][  scanConsole  ]:     configs/KEKQ17/connectivity.json
[14:16:47:294][  info  ][  scanConsole  ]: Target ToT: -1
[14:16:47:294][  info  ][  scanConsole  ]: Target Charge: -1
[14:16:47:294][  info  ][  scanConsole  ]: Output Plots: true
[14:16:47:294][  info  ][  scanConsole  ]: Output Directory: ./data/001807_reg_readtemp/
[14:16:47:304][  info  ][  scanConsole  ]: Timestamp: 2022-08-25_14:16:47
[14:16:47:304][  info  ][  scanConsole  ]: Run Number: 1807
[14:16:47:304][  info  ][  scanConsole  ]: #################
[14:16:47:304][  info  ][  scanConsole  ]: # Init Hardware #
[14:16:47:304][  info  ][  scanConsole  ]: #################
[14:16:47:304][  info  ][  scanConsole  ]: -> Opening controller config: configs/controller/specCfg-rd53a-16x1.json
[14:16:47:304][  info  ][  ScanHelper   ]: Loading controller ...
[14:16:47:304][  info  ][  ScanHelper   ]: Found controller of type: spec
[14:16:47:304][  info  ][  ScanHelper   ]: ... loading controler config:
[14:16:47:304][  info  ][  ScanHelper   ]: ~~~ {
[14:16:47:304][  info  ][  ScanHelper   ]: ~~~     "cmdPeriod": 6.250000073038109e-9,
[14:16:47:304][  info  ][  ScanHelper   ]: ~~~     "idle": {
[14:16:47:304][  info  ][  ScanHelper   ]: ~~~         "word": 1768515945
[14:16:47:304][  info  ][  ScanHelper   ]: ~~~     },
[14:16:47:304][  info  ][  ScanHelper   ]: ~~~     "pulse": {
[14:16:47:304][  info  ][  ScanHelper   ]: ~~~         "interval": 500,
[14:16:47:304][  info  ][  ScanHelper   ]: ~~~         "word": 1549575846
[14:16:47:305][  info  ][  ScanHelper   ]: ~~~     },
[14:16:47:305][  info  ][  ScanHelper   ]: ~~~     "rxActiveLanes": 1,
[14:16:47:305][  info  ][  ScanHelper   ]: ~~~     "rxPolarity": 0,
[14:16:47:305][  info  ][  ScanHelper   ]: ~~~     "specNum": 0,
[14:16:47:305][  info  ][  ScanHelper   ]: ~~~     "spiConfig": 541200,
[14:16:47:305][  info  ][  ScanHelper   ]: ~~~     "sync": {
[14:16:47:305][  info  ][  ScanHelper   ]: ~~~         "interval": 32,
[14:16:47:305][  info  ][  ScanHelper   ]: ~~~         "word": 2172551550
[14:16:47:305][  info  ][  ScanHelper   ]: ~~~     },
[14:16:47:305][  info  ][  ScanHelper   ]: ~~~     "txPolarity": 0
[14:16:47:305][  info  ][  ScanHelper   ]: ~~~ }
[14:16:47:305][  info  ][    SpecCom    ]: Opening SPEC with id #0
[14:16:47:305][  info  ][    SpecCom    ]: Mapping BARs ...
[14:16:47:305][  info  ][    SpecCom    ]: ... Mapped BAR0 at 0x7f188fee3000 with size 1048576
[14:16:47:305][warning ][    SpecCom    ]: ... BAR4 not mapped (Mmap failed)
[14:16:47:305][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[14:16:47:305][  info  ][    SpecCom    ]: Firmware Version: 0x4d9ff6d
[14:16:47:305][  info  ][    SpecCom    ]: Firmware Identifier: 0x4030232
[14:16:47:305][  info  ][    SpecCom    ]: FPGA card: PLDA XpressK7 325
[14:16:47:305][  info  ][    SpecCom    ]: FE Chip Type: RD53A/B
[14:16:47:305][  info  ][    SpecCom    ]: FMC Card Type: Ohio Card (Display Port)
[14:16:47:305][  info  ][    SpecCom    ]: RX Speed: 640Mbps
[14:16:47:305][  info  ][    SpecCom    ]: Channel Configuration: 16x1
[14:16:47:305][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[14:16:47:305][  info  ][    SpecCom    ]: Flushing buffers ...
[14:16:47:305][  info  ][    SpecCom    ]: Init success!
[14:16:47:305][  info  ][  scanConsole  ]: #######################
[14:16:47:305][  info  ][  scanConsole  ]: ##  Loading Configs  ##
[14:16:47:305][  info  ][  scanConsole  ]: #######################
[14:16:47:305][  info  ][  scanConsole  ]: Opening global config: configs/KEKQ17/connectivity.json
[14:16:47:305][  info  ][  ScanHelper   ]: Chip type: RD53A
[14:16:47:305][  info  ][  ScanHelper   ]: Chip count 4
[14:16:47:305][  info  ][  ScanHelper   ]: Loading chip #0
[14:16:47:307][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(4)
[14:16:47:307][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ17/20UPGFC0013396.json
[14:16:47:501][  info  ][  ScanHelper   ]: Loading chip #1
[14:16:47:502][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(5)
[14:16:47:502][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ17/20UPGFC0013428.json
[14:16:47:676][  info  ][  ScanHelper   ]: Loading chip #2
[14:16:47:677][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(6)
[14:16:47:677][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ17/20UPGFC0013381.json
[14:16:47:857][  info  ][  ScanHelper   ]: Loading chip #3
[14:16:47:858][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(7)
[14:16:47:858][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ17/20UPGFC0013349.json
[14:16:48:056][  info  ][  scanConsole  ]: #################
[14:16:48:056][  info  ][  scanConsole  ]: # Configure FEs #
[14:16:48:056][  info  ][  scanConsole  ]: #################
[14:16:48:056][  info  ][  scanConsole  ]: Configuring 20UPGFC0013396
[14:16:48:087][  info  ][  scanConsole  ]: Configuring 20UPGFC0013428
[14:16:48:117][  info  ][  scanConsole  ]: Configuring 20UPGFC0013381
[14:16:48:154][  info  ][  scanConsole  ]: Configuring 20UPGFC0013349
[14:16:48:196][  info  ][  scanConsole  ]: Sent configuration to all FEs in 140 ms!
[14:16:48:198][  info  ][  scanConsole  ]: Checking com 20UPGFC0013396
[14:16:48:198][  info  ][    SpecRx     ]: Active Rx channels: 0x10
[14:16:48:198][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[14:16:48:198][  info  ][    SpecRx     ]: Rx Status 0xf0
[14:16:48:198][  info  ][    SpecRx     ]: Number of lanes: 1
[14:16:48:198][  info  ][    SpecRx     ]: Channel 4 Lane 0 synchronized!
[14:16:48:208][  info  ][  scanConsole  ]: ... success!
[14:16:48:208][  info  ][  scanConsole  ]: Checking com 20UPGFC0013428
[14:16:48:208][  info  ][    SpecRx     ]: Active Rx channels: 0x20
[14:16:48:208][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[14:16:48:208][  info  ][    SpecRx     ]: Rx Status 0xf0
[14:16:48:208][  info  ][    SpecRx     ]: Number of lanes: 1
[14:16:48:208][  info  ][    SpecRx     ]: Channel 5 Lane 0 synchronized!
[14:16:48:219][  info  ][  scanConsole  ]: ... success!
[14:16:48:219][  info  ][  scanConsole  ]: Checking com 20UPGFC0013381
[14:16:48:219][  info  ][    SpecRx     ]: Active Rx channels: 0x40
[14:16:48:219][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[14:16:48:219][  info  ][    SpecRx     ]: Rx Status 0xf0
[14:16:48:219][  info  ][    SpecRx     ]: Number of lanes: 1
[14:16:48:219][  info  ][    SpecRx     ]: Channel 6 Lane 0 synchronized!
[14:16:48:229][  info  ][  scanConsole  ]: ... success!
[14:16:48:229][  info  ][  scanConsole  ]: Checking com 20UPGFC0013349
[14:16:48:229][  info  ][    SpecRx     ]: Active Rx channels: 0x80
[14:16:48:229][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[14:16:48:229][  info  ][    SpecRx     ]: Rx Status 0xf0
[14:16:48:230][  info  ][    SpecRx     ]: Number of lanes: 1
[14:16:48:230][  info  ][    SpecRx     ]: Channel 7 Lane 0 synchronized!
[14:16:48:240][  info  ][  scanConsole  ]: ... success!
[14:16:48:240][  info  ][  scanConsole  ]: Enabling Tx channels
[14:16:48:240][  info  ][  scanConsole  ]: Enabling Tx channel 1
[14:16:48:240][  info  ][  scanConsole  ]: Enabling Tx channel 1
[14:16:48:240][  info  ][  scanConsole  ]: Enabling Tx channel 1
[14:16:48:240][  info  ][  scanConsole  ]: Enabling Tx channel 1
[14:16:48:240][  info  ][  scanConsole  ]: Enabling Rx channels
[14:16:48:240][  info  ][  scanConsole  ]: Enabling Rx channel 4
[14:16:48:240][  info  ][  scanConsole  ]: Enabling Rx channel 5
[14:16:48:240][  info  ][  scanConsole  ]: Enabling Rx channel 6
[14:16:48:240][  info  ][  scanConsole  ]: Enabling Rx channel 7
[14:16:48:240][  info  ][  scanConsole  ]: ##############
[14:16:48:240][  info  ][  scanConsole  ]: # Setup Scan #
[14:16:48:240][  info  ][  scanConsole  ]: ##############
[14:16:48:241][  info  ][  scanConsole  ]: Found Scan config, constructing scan ...
[14:16:48:241][  info  ][  ScanFactory  ]: Loading Scan:
[14:16:48:241][  info  ][  ScanFactory  ]:   Name: AnalogScan
[14:16:48:241][  info  ][  ScanFactory  ]:   Number of Loops: 3
[14:16:48:241][  info  ][  ScanFactory  ]:   Loading Loop #0
[14:16:48:241][  info  ][  ScanFactory  ]:    Type: Rd53aReadRegLoop
[14:16:48:241][  info  ][  ScanFactory  ]:    Loading loop config ... 
[14:16:48:241][  info  ][  ScanFactory  ]: ~~~ {
[14:16:48:241][  info  ][  ScanFactory  ]: ~~~     "CurMux": [],
[14:16:48:241][  info  ][  ScanFactory  ]: ~~~     "EnblRingOsc": 0,
[14:16:48:241][  info  ][  ScanFactory  ]: ~~~     "Registers": [],
[14:16:48:241][  info  ][  ScanFactory  ]: ~~~     "RingOscDur": 9,
[14:16:48:241][  info  ][  ScanFactory  ]: ~~~     "RingOscRep": 10,
[14:16:48:241][  info  ][  ScanFactory  ]: ~~~     "VoltMux": [3, 5, 7, 15]
[14:16:48:241][  info  ][  ScanFactory  ]: ~~~ }
[14:16:48:242][  info  ][  ScanFactory  ]:   Loading Loop #1
[14:16:48:242][  info  ][  ScanFactory  ]:    Type: Rd53aTriggerLoop
[14:16:48:242][  info  ][  ScanFactory  ]:    Loading loop config ... 
[14:16:48:242][  info  ][  ScanFactory  ]: ~~~ {
[14:16:48:242][  info  ][  ScanFactory  ]: ~~~     "count": 1,
[14:16:48:242][  info  ][  ScanFactory  ]: ~~~     "delay": 48,
[14:16:48:242][  info  ][  ScanFactory  ]: ~~~     "extTrig": false,
[14:16:48:242][  info  ][  ScanFactory  ]: ~~~     "frequency": 30000.0,
[14:16:48:242][  info  ][  ScanFactory  ]: ~~~     "noInject": false,
[14:16:48:242][  info  ][  ScanFactory  ]: ~~~     "sendEcr": false,
[14:16:48:242][  info  ][  ScanFactory  ]: ~~~     "time": 0.0,
[14:16:48:242][  info  ][  ScanFactory  ]: ~~~     "trigMultiplier": 16
[14:16:48:242][  info  ][  ScanFactory  ]: ~~~ }
[14:16:48:242][  info  ][  ScanFactory  ]:   Loading Loop #2
[14:16:48:242][  info  ][  ScanFactory  ]:    Type: StdDataLoop
[14:16:48:242][warning ][  ScanFactory  ]: ~~~ Config empty.
[14:16:48:242][  info  ][ScanBuildHistogrammers]: Loading histogrammer ...
[14:16:48:242][  info  ][ScanBuildHistogrammers]: ... done!
[14:16:48:242][  info  ][ScanBuildAnalyses]: Loading analyses ...
[14:16:48:243][  info  ][  scanConsole  ]: Running pre scan!
[14:16:48:243][  info  ][  ScanFactory  ]: Entering pre scan phase ...
[14:16:48:243][  info  ][     Rd53a     ]: Write named register: GlobalPulseRt -> 16384
[14:16:48:243][  info  ][     Rd53a     ]: Write named register: InjEnDig -> 0
[14:16:48:243][  info  ][     Rd53a     ]: Write named register: InjVcalDiff -> 0
[14:16:48:243][  info  ][     Rd53a     ]: Write named register: InjVcalHigh -> 3500
[14:16:48:243][  info  ][     Rd53a     ]: Write named register: InjVcalMed -> 500
[14:16:48:243][  info  ][     Rd53a     ]: Write named register: LatencyConfig -> 48
[14:16:48:243][  info  ][  scanConsole  ]: Starting histogrammer and analysis threads:
[14:16:48:243][  info  ][  scanConsole  ]:  .. started threads of Fe 4
[14:16:48:243][  info  ][  scanConsole  ]:  .. started threads of Fe 5
[14:16:48:243][  info  ][  scanConsole  ]:  .. started threads of Fe 6
[14:16:48:243][  info  ][  scanConsole  ]:  .. started threads of Fe 7
[14:16:48:244][  info  ][Rd53aDataProcessor]:   -> Processor thread #0 started!
[14:16:48:244][  info  ][Rd53aDataProcessor]:   -> Processor thread #1 started!
[14:16:48:244][  info  ][Rd53aDataProcessor]:   -> Processor thread #2 started!
[14:16:48:244][  info  ][Rd53aDataProcessor]:   -> Processor thread #3 started!
[14:16:48:244][  info  ][  scanConsole  ]: ########
[14:16:48:244][  info  ][  scanConsole  ]: # Scan #
[14:16:48:244][  info  ][  scanConsole  ]: ########
[14:16:48:244][  info  ][  scanConsole  ]: Starting scan!
[14:16:48:244][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0013396 on Rx 4
[14:16:48:249][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0013396] MON MUX_V: 3 Bias 0 1247 -> 0.260137 V Bias 1 1647 -> 0.340375 V, Temperature 0.0802377 -> -3.0686 C
[14:16:48:253][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0013396] MON MUX_V: 5 Bias 0 1214 -> 0.253518 V Bias 1 1605 -> 0.33195 V, Temperature 0.0784323 -> -4.22235 C
[14:16:48:257][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0013396] MON MUX_V: 7 Bias 0 1188 -> 0.248302 V Bias 1 1591 -> 0.329142 V, Temperature 0.0808395 -> -2.89832 C
[14:16:48:261][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0013396] MON MUX_V: 15 Bias 0 1198 -> 0.250308 V Bias 1 1606 -> 0.332151 V, Temperature 0.0818424 -> -2.61954 C
[14:16:48:296][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0013428 on Rx 5
[14:16:48:300][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0013428] MON MUX_V: 3 Bias 0 1228 -> 0.258625 V Bias 1 1610 -> 0.336011 V, Temperature 0.077386 -> -6.68576 C
[14:16:48:304][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0013428] MON MUX_V: 5 Bias 0 1183 -> 0.249509 V Bias 1 1578 -> 0.329528 V, Temperature 0.0800195 -> -2.10309 C
[14:16:48:308][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0013428] MON MUX_V: 7 Bias 0 1184 -> 0.249711 V Bias 1 1578 -> 0.329528 V, Temperature 0.0798169 -> -4.66245 C
[14:16:48:312][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0013428] MON MUX_V: 15 Bias 0 1170 -> 0.246875 V Bias 1 1569 -> 0.327705 V, Temperature 0.0808299 -> -1.88156 C
[14:16:48:347][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0013381 on Rx 6
[14:16:48:351][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0013381] MON MUX_V: 3 Bias 0 1220 -> 0.26317 V Bias 1 1603 -> 0.340114 V, Temperature 0.076944 -> -5.98828 C
[14:16:48:356][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0013381] MON MUX_V: 5 Bias 0 1180 -> 0.255134 V Bias 1 1581 -> 0.335694 V, Temperature 0.0805601 -> -4.84937 C
[14:16:48:360][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0013381] MON MUX_V: 7 Bias 0 1166 -> 0.252321 V Bias 1 1564 -> 0.332279 V, Temperature 0.0799575 -> -5.03265 C
[14:16:48:364][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0013381] MON MUX_V: 15 Bias 0 1139 -> 0.246897 V Bias 1 1543 -> 0.32806 V, Temperature 0.0811628 -> -6.47052 C
[14:16:48:399][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0013349 on Rx 7
[14:16:48:403][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0013349] MON MUX_V: 3 Bias 0 1263 -> 0.260429 V Bias 1 1653 -> 0.338867 V, Temperature 0.0784386 -> -1.74701 C
[14:16:48:407][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0013349] MON MUX_V: 5 Bias 0 1226 -> 0.252987 V Bias 1 1620 -> 0.33223 V, Temperature 0.0792431 -> -1.52582 C
[14:16:48:411][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0013349] MON MUX_V: 7 Bias 0 1210 -> 0.249769 V Bias 1 1607 -> 0.329616 V, Temperature 0.0798465 -> -2.6181 C
[14:16:48:415][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0013349] MON MUX_V: 15 Bias 0 1216 -> 0.250976 V Bias 1 1612 -> 0.330621 V, Temperature 0.0796454 -> -1.41684 C
[14:16:48:451][  info  ][  scanConsole  ]: Scan done!
[14:16:48:451][  info  ][  scanConsole  ]: Waiting for processors to finish ...
[14:16:48:452][  info  ][  scanConsole  ]: Processor done, waiting for histogrammer ...
[14:16:48:852][  info  ][HistogramAlgorithm]: Histogrammer done!
[14:16:48:852][  info  ][HistogramAlgorithm]: Histogrammer done!
[14:16:48:852][  info  ][HistogramAlgorithm]: Histogrammer done!
[14:16:48:852][  info  ][HistogramAlgorithm]: Histogrammer done!
[14:16:48:852][  info  ][  scanConsole  ]: Processor done, waiting for analysis ...
[14:16:49:252][  info  ][AnalysisAlgorithm]: Analysis done!
[14:16:49:252][  info  ][AnalysisAlgorithm]: Analysis done!
[14:16:49:252][  info  ][AnalysisAlgorithm]: Analysis done!
[14:16:49:252][  info  ][AnalysisAlgorithm]: Analysis done!
[14:16:49:253][  info  ][  scanConsole  ]: All done!
[14:16:49:253][  info  ][  scanConsole  ]: ##########
[14:16:49:253][  info  ][  scanConsole  ]: # Timing #
[14:16:49:253][  info  ][  scanConsole  ]: ##########
[14:16:49:253][  info  ][  scanConsole  ]: -> Configuration: 140 ms
[14:16:49:253][  info  ][  scanConsole  ]: -> Scan:          207 ms
[14:16:49:253][  info  ][  scanConsole  ]: -> Processing:    0 ms
[14:16:49:253][  info  ][  scanConsole  ]: -> Analysis:      800 ms
[14:16:49:253][  info  ][  scanConsole  ]: ###########
[14:16:49:253][  info  ][  scanConsole  ]: # Cleanup #
[14:16:49:253][  info  ][  scanConsole  ]: ###########
[14:16:49:261][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0013396 to configs/KEKQ17/20UPGFC0013396.json
[14:16:49:441][  info  ][  scanConsole  ]: -> Storing output results of FE 4
[14:16:49:441][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0013396, this usually means that the chip did not send any data at all.
[14:16:49:442][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0013428 to configs/KEKQ17/20UPGFC0013428.json
[14:16:49:608][  info  ][  scanConsole  ]: -> Storing output results of FE 5
[14:16:49:608][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0013428, this usually means that the chip did not send any data at all.
[14:16:49:609][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0013381 to configs/KEKQ17/20UPGFC0013381.json
[14:16:49:777][  info  ][  scanConsole  ]: -> Storing output results of FE 6
[14:16:49:777][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0013381, this usually means that the chip did not send any data at all.
[14:16:49:778][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0013349 to configs/KEKQ17/20UPGFC0013349.json
[14:16:49:946][  info  ][  scanConsole  ]: -> Storing output results of FE 7
[14:16:49:946][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0013349, this usually means that the chip did not send any data at all.
[14:16:49:948][  info  ][  scanConsole  ]: Finishing run: 1807
20UPGFC0013349.json.after
20UPGFC0013349.json.before
20UPGFC0013381.json.after
20UPGFC0013381.json.before
20UPGFC0013396.json.after
20UPGFC0013396.json.before
20UPGFC0013428.json.after
20UPGFC0013428.json.before
reg_readtemp.json
scanLog.json

