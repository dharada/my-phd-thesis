[13:44:09:868][  info  ][               ]: #####################################
[13:44:09:868][  info  ][               ]: # Welcome to the YARR Scan Console! #
[13:44:09:868][  info  ][               ]: #####################################
[13:44:09:868][  info  ][               ]: -> Parsing command line parameters ...
[13:44:09:868][  info  ][               ]: Configuring logger ...
[13:44:09:871][  info  ][  scanConsole  ]: Scan Type/Config configs/scans/rd53a/reg_readtemp.json
[13:44:09:871][  info  ][  scanConsole  ]: Connectivity:
[13:44:09:871][  info  ][  scanConsole  ]:     configs/KEKQ17/connectivity.json
[13:44:09:871][  info  ][  scanConsole  ]: Target ToT: -1
[13:44:09:871][  info  ][  scanConsole  ]: Target Charge: -1
[13:44:09:871][  info  ][  scanConsole  ]: Output Plots: true
[13:44:09:871][  info  ][  scanConsole  ]: Output Directory: ./data/001801_reg_readtemp/
[13:44:09:876][  info  ][  scanConsole  ]: Timestamp: 2022-08-25_13:44:09
[13:44:09:876][  info  ][  scanConsole  ]: Run Number: 1801
[13:44:09:876][  info  ][  scanConsole  ]: #################
[13:44:09:876][  info  ][  scanConsole  ]: # Init Hardware #
[13:44:09:876][  info  ][  scanConsole  ]: #################
[13:44:09:876][  info  ][  scanConsole  ]: -> Opening controller config: configs/controller/specCfg-rd53a-16x1.json
[13:44:09:876][  info  ][  ScanHelper   ]: Loading controller ...
[13:44:09:876][  info  ][  ScanHelper   ]: Found controller of type: spec
[13:44:09:876][  info  ][  ScanHelper   ]: ... loading controler config:
[13:44:09:876][  info  ][  ScanHelper   ]: ~~~ {
[13:44:09:876][  info  ][  ScanHelper   ]: ~~~     "cmdPeriod": 6.250000073038109e-9,
[13:44:09:876][  info  ][  ScanHelper   ]: ~~~     "idle": {
[13:44:09:876][  info  ][  ScanHelper   ]: ~~~         "word": 1768515945
[13:44:09:876][  info  ][  ScanHelper   ]: ~~~     },
[13:44:09:876][  info  ][  ScanHelper   ]: ~~~     "pulse": {
[13:44:09:876][  info  ][  ScanHelper   ]: ~~~         "interval": 500,
[13:44:09:876][  info  ][  ScanHelper   ]: ~~~         "word": 1549575846
[13:44:09:876][  info  ][  ScanHelper   ]: ~~~     },
[13:44:09:876][  info  ][  ScanHelper   ]: ~~~     "rxActiveLanes": 1,
[13:44:09:876][  info  ][  ScanHelper   ]: ~~~     "rxPolarity": 0,
[13:44:09:876][  info  ][  ScanHelper   ]: ~~~     "specNum": 0,
[13:44:09:876][  info  ][  ScanHelper   ]: ~~~     "spiConfig": 541200,
[13:44:09:876][  info  ][  ScanHelper   ]: ~~~     "sync": {
[13:44:09:876][  info  ][  ScanHelper   ]: ~~~         "interval": 32,
[13:44:09:876][  info  ][  ScanHelper   ]: ~~~         "word": 2172551550
[13:44:09:876][  info  ][  ScanHelper   ]: ~~~     },
[13:44:09:876][  info  ][  ScanHelper   ]: ~~~     "txPolarity": 0
[13:44:09:876][  info  ][  ScanHelper   ]: ~~~ }
[13:44:09:876][  info  ][    SpecCom    ]: Opening SPEC with id #0
[13:44:09:877][  info  ][    SpecCom    ]: Mapping BARs ...
[13:44:09:877][  info  ][    SpecCom    ]: ... Mapped BAR0 at 0x7f5496545000 with size 1048576
[13:44:09:877][warning ][    SpecCom    ]: ... BAR4 not mapped (Mmap failed)
[13:44:09:877][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[13:44:09:877][  info  ][    SpecCom    ]: Firmware Version: 0x4d9ff6d
[13:44:09:877][  info  ][    SpecCom    ]: Firmware Identifier: 0x4030232
[13:44:09:877][  info  ][    SpecCom    ]: FPGA card: PLDA XpressK7 325
[13:44:09:877][  info  ][    SpecCom    ]: FE Chip Type: RD53A/B
[13:44:09:877][  info  ][    SpecCom    ]: FMC Card Type: Ohio Card (Display Port)
[13:44:09:877][  info  ][    SpecCom    ]: RX Speed: 640Mbps
[13:44:09:877][  info  ][    SpecCom    ]: Channel Configuration: 16x1
[13:44:09:877][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[13:44:09:877][  info  ][    SpecCom    ]: Flushing buffers ...
[13:44:09:877][  info  ][    SpecCom    ]: Init success!
[13:44:09:877][  info  ][  scanConsole  ]: #######################
[13:44:09:877][  info  ][  scanConsole  ]: ##  Loading Configs  ##
[13:44:09:877][  info  ][  scanConsole  ]: #######################
[13:44:09:877][  info  ][  scanConsole  ]: Opening global config: configs/KEKQ17/connectivity.json
[13:44:09:877][  info  ][  ScanHelper   ]: Chip type: RD53A
[13:44:09:877][  info  ][  ScanHelper   ]: Chip count 4
[13:44:09:877][  info  ][  ScanHelper   ]: Loading chip #0
[13:44:09:878][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(4)
[13:44:09:878][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ17/20UPGFC0013396.json
[13:44:10:054][  info  ][  ScanHelper   ]: Loading chip #1
[13:44:10:054][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(5)
[13:44:10:054][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ17/20UPGFC0013428.json
[13:44:10:230][  info  ][  ScanHelper   ]: Loading chip #2
[13:44:10:230][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(6)
[13:44:10:230][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ17/20UPGFC0013381.json
[13:44:10:405][  info  ][  ScanHelper   ]: Loading chip #3
[13:44:10:406][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(7)
[13:44:10:406][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ17/20UPGFC0013349.json
[13:44:10:587][  info  ][  scanConsole  ]: #################
[13:44:10:587][  info  ][  scanConsole  ]: # Configure FEs #
[13:44:10:587][  info  ][  scanConsole  ]: #################
[13:44:10:587][  info  ][  scanConsole  ]: Configuring 20UPGFC0013396
[13:44:10:618][  info  ][  scanConsole  ]: Configuring 20UPGFC0013428
[13:44:10:662][  info  ][  scanConsole  ]: Configuring 20UPGFC0013381
[13:44:10:696][  info  ][  scanConsole  ]: Configuring 20UPGFC0013349
[13:44:10:733][  info  ][  scanConsole  ]: Sent configuration to all FEs in 145 ms!
[13:44:10:734][  info  ][  scanConsole  ]: Checking com 20UPGFC0013396
[13:44:10:734][  info  ][    SpecRx     ]: Active Rx channels: 0x10
[13:44:10:734][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[13:44:10:734][  info  ][    SpecRx     ]: Rx Status 0xf0
[13:44:10:734][  info  ][    SpecRx     ]: Number of lanes: 1
[13:44:10:734][  info  ][    SpecRx     ]: Channel 4 Lane 0 synchronized!
[13:44:10:744][  info  ][  scanConsole  ]: ... success!
[13:44:10:744][  info  ][  scanConsole  ]: Checking com 20UPGFC0013428
[13:44:10:744][  info  ][    SpecRx     ]: Active Rx channels: 0x20
[13:44:10:744][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[13:44:10:745][  info  ][    SpecRx     ]: Rx Status 0xf0
[13:44:10:745][  info  ][    SpecRx     ]: Number of lanes: 1
[13:44:10:745][  info  ][    SpecRx     ]: Channel 5 Lane 0 synchronized!
[13:44:10:755][  info  ][  scanConsole  ]: ... success!
[13:44:10:755][  info  ][  scanConsole  ]: Checking com 20UPGFC0013381
[13:44:10:755][  info  ][    SpecRx     ]: Active Rx channels: 0x40
[13:44:10:755][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[13:44:10:755][  info  ][    SpecRx     ]: Rx Status 0xf0
[13:44:10:755][  info  ][    SpecRx     ]: Number of lanes: 1
[13:44:10:755][  info  ][    SpecRx     ]: Channel 6 Lane 0 synchronized!
[13:44:10:766][  info  ][  scanConsole  ]: ... success!
[13:44:10:766][  info  ][  scanConsole  ]: Checking com 20UPGFC0013349
[13:44:10:766][  info  ][    SpecRx     ]: Active Rx channels: 0x80
[13:44:10:766][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[13:44:10:766][  info  ][    SpecRx     ]: Rx Status 0xf0
[13:44:10:766][  info  ][    SpecRx     ]: Number of lanes: 1
[13:44:10:766][  info  ][    SpecRx     ]: Channel 7 Lane 0 synchronized!
[13:44:10:776][  info  ][  scanConsole  ]: ... success!
[13:44:10:776][  info  ][  scanConsole  ]: Enabling Tx channels
[13:44:10:776][  info  ][  scanConsole  ]: Enabling Tx channel 1
[13:44:10:776][  info  ][  scanConsole  ]: Enabling Tx channel 1
[13:44:10:776][  info  ][  scanConsole  ]: Enabling Tx channel 1
[13:44:10:776][  info  ][  scanConsole  ]: Enabling Tx channel 1
[13:44:10:776][  info  ][  scanConsole  ]: Enabling Rx channels
[13:44:10:776][  info  ][  scanConsole  ]: Enabling Rx channel 4
[13:44:10:776][  info  ][  scanConsole  ]: Enabling Rx channel 5
[13:44:10:777][  info  ][  scanConsole  ]: Enabling Rx channel 6
[13:44:10:777][  info  ][  scanConsole  ]: Enabling Rx channel 7
[13:44:10:777][  info  ][  scanConsole  ]: ##############
[13:44:10:777][  info  ][  scanConsole  ]: # Setup Scan #
[13:44:10:777][  info  ][  scanConsole  ]: ##############
[13:44:10:777][  info  ][  scanConsole  ]: Found Scan config, constructing scan ...
[13:44:10:777][  info  ][  ScanFactory  ]: Loading Scan:
[13:44:10:777][  info  ][  ScanFactory  ]:   Name: AnalogScan
[13:44:10:777][  info  ][  ScanFactory  ]:   Number of Loops: 3
[13:44:10:777][  info  ][  ScanFactory  ]:   Loading Loop #0
[13:44:10:777][  info  ][  ScanFactory  ]:    Type: Rd53aReadRegLoop
[13:44:10:777][  info  ][  ScanFactory  ]:    Loading loop config ... 
[13:44:10:778][  info  ][  ScanFactory  ]: ~~~ {
[13:44:10:778][  info  ][  ScanFactory  ]: ~~~     "CurMux": [],
[13:44:10:778][  info  ][  ScanFactory  ]: ~~~     "EnblRingOsc": 0,
[13:44:10:778][  info  ][  ScanFactory  ]: ~~~     "Registers": [],
[13:44:10:778][  info  ][  ScanFactory  ]: ~~~     "RingOscDur": 9,
[13:44:10:778][  info  ][  ScanFactory  ]: ~~~     "RingOscRep": 10,
[13:44:10:778][  info  ][  ScanFactory  ]: ~~~     "VoltMux": [3, 5, 7, 15]
[13:44:10:778][  info  ][  ScanFactory  ]: ~~~ }
[13:44:10:778][  info  ][  ScanFactory  ]:   Loading Loop #1
[13:44:10:778][  info  ][  ScanFactory  ]:    Type: Rd53aTriggerLoop
[13:44:10:778][  info  ][  ScanFactory  ]:    Loading loop config ... 
[13:44:10:778][  info  ][  ScanFactory  ]: ~~~ {
[13:44:10:778][  info  ][  ScanFactory  ]: ~~~     "count": 1,
[13:44:10:778][  info  ][  ScanFactory  ]: ~~~     "delay": 48,
[13:44:10:778][  info  ][  ScanFactory  ]: ~~~     "extTrig": false,
[13:44:10:778][  info  ][  ScanFactory  ]: ~~~     "frequency": 30000.0,
[13:44:10:778][  info  ][  ScanFactory  ]: ~~~     "noInject": false,
[13:44:10:778][  info  ][  ScanFactory  ]: ~~~     "sendEcr": false,
[13:44:10:778][  info  ][  ScanFactory  ]: ~~~     "time": 0.0,
[13:44:10:778][  info  ][  ScanFactory  ]: ~~~     "trigMultiplier": 16
[13:44:10:778][  info  ][  ScanFactory  ]: ~~~ }
[13:44:10:778][  info  ][  ScanFactory  ]:   Loading Loop #2
[13:44:10:778][  info  ][  ScanFactory  ]:    Type: StdDataLoop
[13:44:10:778][warning ][  ScanFactory  ]: ~~~ Config empty.
[13:44:10:778][  info  ][ScanBuildHistogrammers]: Loading histogrammer ...
[13:44:10:779][  info  ][ScanBuildHistogrammers]: ... done!
[13:44:10:779][  info  ][ScanBuildAnalyses]: Loading analyses ...
[13:44:10:779][  info  ][  scanConsole  ]: Running pre scan!
[13:44:10:779][  info  ][  ScanFactory  ]: Entering pre scan phase ...
[13:44:10:779][  info  ][     Rd53a     ]: Write named register: GlobalPulseRt -> 16384
[13:44:10:779][  info  ][     Rd53a     ]: Write named register: InjEnDig -> 0
[13:44:10:779][  info  ][     Rd53a     ]: Write named register: InjVcalDiff -> 0
[13:44:10:779][  info  ][     Rd53a     ]: Write named register: InjVcalHigh -> 3500
[13:44:10:779][  info  ][     Rd53a     ]: Write named register: InjVcalMed -> 500
[13:44:10:779][  info  ][     Rd53a     ]: Write named register: LatencyConfig -> 48
[13:44:10:779][  info  ][  scanConsole  ]: Starting histogrammer and analysis threads:
[13:44:10:780][  info  ][  scanConsole  ]:  .. started threads of Fe 4
[13:44:10:780][  info  ][  scanConsole  ]:  .. started threads of Fe 5
[13:44:10:780][  info  ][  scanConsole  ]:  .. started threads of Fe 6
[13:44:10:780][  info  ][  scanConsole  ]:  .. started threads of Fe 7
[13:44:10:781][  info  ][Rd53aDataProcessor]:   -> Processor thread #0 started!
[13:44:10:781][  info  ][Rd53aDataProcessor]:   -> Processor thread #1 started!
[13:44:10:781][  info  ][Rd53aDataProcessor]:   -> Processor thread #2 started!
[13:44:10:781][  info  ][Rd53aDataProcessor]:   -> Processor thread #3 started!
[13:44:10:781][  info  ][  scanConsole  ]: ########
[13:44:10:781][  info  ][  scanConsole  ]: # Scan #
[13:44:10:781][  info  ][  scanConsole  ]: ########
[13:44:10:781][  info  ][  scanConsole  ]: Starting scan!
[13:44:10:781][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0013396 on Rx 4
[13:44:10:785][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0013396] MON MUX_V: 3 Bias 0 1214 -> 0.253518 V Bias 1 1621 -> 0.335159 V, Temperature 0.0816418 -> 1.65775 C
[13:44:10:789][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0013396] MON MUX_V: 5 Bias 0 1180 -> 0.246697 V Bias 1 1580 -> 0.326935 V, Temperature 0.0802376 -> 1.96771 C
[13:44:10:794][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0013396] MON MUX_V: 7 Bias 0 1154 -> 0.241482 V Bias 1 1564 -> 0.323726 V, Temperature 0.0822436 -> 1.79581 C
[13:44:10:798][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0013396] MON MUX_V: 15 Bias 0 1163 -> 0.243287 V Bias 1 1579 -> 0.326735 V, Temperature 0.0834471 -> 2.68488 C
[13:44:10:833][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0013428 on Rx 5
[13:44:10:837][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0013428] MON MUX_V: 3 Bias 0 1196 -> 0.252142 V Bias 1 1587 -> 0.331351 V, Temperature 0.0792092 -> -0.407776 C
[13:44:10:842][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0013428] MON MUX_V: 5 Bias 0 1150 -> 0.242823 V Bias 1 1552 -> 0.324261 V, Temperature 0.0814376 -> 2.70038 C
[13:44:10:846][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0013428] MON MUX_V: 7 Bias 0 1151 -> 0.243026 V Bias 1 1553 -> 0.324464 V, Temperature 0.0814376 -> 0.789185 C
[13:44:10:850][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0013428] MON MUX_V: 15 Bias 0 1138 -> 0.240393 V Bias 1 1545 -> 0.322843 V, Temperature 0.0824505 -> 3.55734 C
[13:44:10:885][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0013381 on Rx 6
[13:44:10:889][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0013381] MON MUX_V: 3 Bias 0 1188 -> 0.256741 V Bias 1 1580 -> 0.335493 V, Temperature 0.078752 -> 0.289551 C
[13:44:10:894][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0013381] MON MUX_V: 5 Bias 0 1149 -> 0.248906 V Bias 1 1556 -> 0.330672 V, Temperature 0.0817655 -> -0.8349 C
[13:44:10:898][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0013381] MON MUX_V: 7 Bias 0 1132 -> 0.245491 V Bias 1 1541 -> 0.327658 V, Temperature 0.0821673 -> 2.37753 C
[13:44:10:902][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0013381] MON MUX_V: 15 Bias 0 1107 -> 0.240469 V Bias 1 1520 -> 0.323439 V, Temperature 0.0829709 -> -0.529449 C
[13:44:10:937][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0013349 on Rx 7
[13:44:10:941][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0013349] MON MUX_V: 3 Bias 0 1229 -> 0.253591 V Bias 1 1625 -> 0.333236 V, Temperature 0.0796454 -> 2.42847 C
[13:44:10:945][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0013349] MON MUX_V: 5 Bias 0 1192 -> 0.246149 V Bias 1 1592 -> 0.326599 V, Temperature 0.0804499 -> 2.61066 C
[13:44:10:950][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0013349] MON MUX_V: 7 Bias 0 1174 -> 0.242529 V Bias 1 1580 -> 0.324185 V, Temperature 0.0816566 -> 3.51492 C
[13:44:10:954][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0013349] MON MUX_V: 15 Bias 0 1180 -> 0.243736 V Bias 1 1583 -> 0.324789 V, Temperature 0.0810533 -> 3.3866 C
[13:44:10:990][  info  ][  scanConsole  ]: Scan done!
[13:44:10:990][  info  ][  scanConsole  ]: Waiting for processors to finish ...
[13:44:10:991][  info  ][  scanConsole  ]: Processor done, waiting for histogrammer ...
[13:44:11:391][  info  ][HistogramAlgorithm]: Histogrammer done!
[13:44:11:391][  info  ][HistogramAlgorithm]: Histogrammer done!
[13:44:11:391][  info  ][HistogramAlgorithm]: Histogrammer done!
[13:44:11:391][  info  ][HistogramAlgorithm]: Histogrammer done!
[13:44:11:391][  info  ][  scanConsole  ]: Processor done, waiting for analysis ...
[13:44:11:791][  info  ][AnalysisAlgorithm]: Analysis done!
[13:44:11:791][  info  ][AnalysisAlgorithm]: Analysis done!
[13:44:11:791][  info  ][AnalysisAlgorithm]: Analysis done!
[13:44:11:791][  info  ][AnalysisAlgorithm]: Analysis done!
[13:44:11:791][  info  ][  scanConsole  ]: All done!
[13:44:11:791][  info  ][  scanConsole  ]: ##########
[13:44:11:791][  info  ][  scanConsole  ]: # Timing #
[13:44:11:791][  info  ][  scanConsole  ]: ##########
[13:44:11:791][  info  ][  scanConsole  ]: -> Configuration: 145 ms
[13:44:11:791][  info  ][  scanConsole  ]: -> Scan:          209 ms
[13:44:11:791][  info  ][  scanConsole  ]: -> Processing:    0 ms
[13:44:11:791][  info  ][  scanConsole  ]: -> Analysis:      800 ms
[13:44:11:791][  info  ][  scanConsole  ]: ###########
[13:44:11:791][  info  ][  scanConsole  ]: # Cleanup #
[13:44:11:791][  info  ][  scanConsole  ]: ###########
[13:44:11:794][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0013396 to configs/KEKQ17/20UPGFC0013396.json
[13:44:11:985][  info  ][  scanConsole  ]: -> Storing output results of FE 4
[13:44:11:985][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0013396, this usually means that the chip did not send any data at all.
[13:44:11:987][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0013428 to configs/KEKQ17/20UPGFC0013428.json
[13:44:12:155][  info  ][  scanConsole  ]: -> Storing output results of FE 5
[13:44:12:155][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0013428, this usually means that the chip did not send any data at all.
[13:44:12:157][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0013381 to configs/KEKQ17/20UPGFC0013381.json
[13:44:12:323][  info  ][  scanConsole  ]: -> Storing output results of FE 6
[13:44:12:324][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0013381, this usually means that the chip did not send any data at all.
[13:44:12:325][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0013349 to configs/KEKQ17/20UPGFC0013349.json
[13:44:12:488][  info  ][  scanConsole  ]: -> Storing output results of FE 7
[13:44:12:488][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0013349, this usually means that the chip did not send any data at all.
[13:44:12:489][  info  ][  scanConsole  ]: Finishing run: 1801
20UPGFC0013349.json.after
20UPGFC0013349.json.before
20UPGFC0013381.json.after
20UPGFC0013381.json.before
20UPGFC0013396.json.after
20UPGFC0013396.json.before
20UPGFC0013428.json.after
20UPGFC0013428.json.before
reg_readtemp.json
scanLog.json
