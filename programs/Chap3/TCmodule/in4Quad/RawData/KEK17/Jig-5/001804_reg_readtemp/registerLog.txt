[13:49:35:939][  info  ][               ]: #####################################
[13:49:35:939][  info  ][               ]: # Welcome to the YARR Scan Console! #
[13:49:35:939][  info  ][               ]: #####################################
[13:49:35:939][  info  ][               ]: -> Parsing command line parameters ...
[13:49:35:939][  info  ][               ]: Configuring logger ...
[13:49:35:941][  info  ][  scanConsole  ]: Scan Type/Config configs/scans/rd53a/reg_readtemp.json
[13:49:35:941][  info  ][  scanConsole  ]: Connectivity:
[13:49:35:941][  info  ][  scanConsole  ]:     configs/KEKQ17/connectivity.json
[13:49:35:941][  info  ][  scanConsole  ]: Target ToT: -1
[13:49:35:941][  info  ][  scanConsole  ]: Target Charge: -1
[13:49:35:941][  info  ][  scanConsole  ]: Output Plots: true
[13:49:35:941][  info  ][  scanConsole  ]: Output Directory: ./data/001804_reg_readtemp/
[13:49:35:945][  info  ][  scanConsole  ]: Timestamp: 2022-08-25_13:49:35
[13:49:35:945][  info  ][  scanConsole  ]: Run Number: 1804
[13:49:35:945][  info  ][  scanConsole  ]: #################
[13:49:35:945][  info  ][  scanConsole  ]: # Init Hardware #
[13:49:35:946][  info  ][  scanConsole  ]: #################
[13:49:35:946][  info  ][  scanConsole  ]: -> Opening controller config: configs/controller/specCfg-rd53a-16x1.json
[13:49:35:946][  info  ][  ScanHelper   ]: Loading controller ...
[13:49:35:946][  info  ][  ScanHelper   ]: Found controller of type: spec
[13:49:35:946][  info  ][  ScanHelper   ]: ... loading controler config:
[13:49:35:946][  info  ][  ScanHelper   ]: ~~~ {
[13:49:35:946][  info  ][  ScanHelper   ]: ~~~     "cmdPeriod": 6.250000073038109e-9,
[13:49:35:946][  info  ][  ScanHelper   ]: ~~~     "idle": {
[13:49:35:946][  info  ][  ScanHelper   ]: ~~~         "word": 1768515945
[13:49:35:946][  info  ][  ScanHelper   ]: ~~~     },
[13:49:35:946][  info  ][  ScanHelper   ]: ~~~     "pulse": {
[13:49:35:946][  info  ][  ScanHelper   ]: ~~~         "interval": 500,
[13:49:35:946][  info  ][  ScanHelper   ]: ~~~         "word": 1549575846
[13:49:35:946][  info  ][  ScanHelper   ]: ~~~     },
[13:49:35:946][  info  ][  ScanHelper   ]: ~~~     "rxActiveLanes": 1,
[13:49:35:946][  info  ][  ScanHelper   ]: ~~~     "rxPolarity": 0,
[13:49:35:946][  info  ][  ScanHelper   ]: ~~~     "specNum": 0,
[13:49:35:946][  info  ][  ScanHelper   ]: ~~~     "spiConfig": 541200,
[13:49:35:946][  info  ][  ScanHelper   ]: ~~~     "sync": {
[13:49:35:946][  info  ][  ScanHelper   ]: ~~~         "interval": 32,
[13:49:35:946][  info  ][  ScanHelper   ]: ~~~         "word": 2172551550
[13:49:35:946][  info  ][  ScanHelper   ]: ~~~     },
[13:49:35:946][  info  ][  ScanHelper   ]: ~~~     "txPolarity": 0
[13:49:35:946][  info  ][  ScanHelper   ]: ~~~ }
[13:49:35:946][  info  ][    SpecCom    ]: Opening SPEC with id #0
[13:49:35:946][  info  ][    SpecCom    ]: Mapping BARs ...
[13:49:35:946][  info  ][    SpecCom    ]: ... Mapped BAR0 at 0x7f3df81b3000 with size 1048576
[13:49:35:946][warning ][    SpecCom    ]: ... BAR4 not mapped (Mmap failed)
[13:49:35:946][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[13:49:35:946][  info  ][    SpecCom    ]: Firmware Version: 0x4d9ff6d
[13:49:35:946][  info  ][    SpecCom    ]: Firmware Identifier: 0x4030232
[13:49:35:946][  info  ][    SpecCom    ]: FPGA card: PLDA XpressK7 325
[13:49:35:946][  info  ][    SpecCom    ]: FE Chip Type: RD53A/B
[13:49:35:946][  info  ][    SpecCom    ]: FMC Card Type: Ohio Card (Display Port)
[13:49:35:946][  info  ][    SpecCom    ]: RX Speed: 640Mbps
[13:49:35:946][  info  ][    SpecCom    ]: Channel Configuration: 16x1
[13:49:35:946][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[13:49:35:946][  info  ][    SpecCom    ]: Flushing buffers ...
[13:49:35:946][  info  ][    SpecCom    ]: Init success!
[13:49:35:946][  info  ][  scanConsole  ]: #######################
[13:49:35:946][  info  ][  scanConsole  ]: ##  Loading Configs  ##
[13:49:35:946][  info  ][  scanConsole  ]: #######################
[13:49:35:946][  info  ][  scanConsole  ]: Opening global config: configs/KEKQ17/connectivity.json
[13:49:35:946][  info  ][  ScanHelper   ]: Chip type: RD53A
[13:49:35:946][  info  ][  ScanHelper   ]: Chip count 4
[13:49:35:946][  info  ][  ScanHelper   ]: Loading chip #0
[13:49:35:947][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(4)
[13:49:35:947][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ17/20UPGFC0013396.json
[13:49:36:121][  info  ][  ScanHelper   ]: Loading chip #1
[13:49:36:122][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(5)
[13:49:36:122][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ17/20UPGFC0013428.json
[13:49:36:299][  info  ][  ScanHelper   ]: Loading chip #2
[13:49:36:299][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(6)
[13:49:36:299][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ17/20UPGFC0013381.json
[13:49:36:474][  info  ][  ScanHelper   ]: Loading chip #3
[13:49:36:474][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(7)
[13:49:36:474][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ17/20UPGFC0013349.json
[13:49:36:670][  info  ][  scanConsole  ]: #################
[13:49:36:670][  info  ][  scanConsole  ]: # Configure FEs #
[13:49:36:670][  info  ][  scanConsole  ]: #################
[13:49:36:670][  info  ][  scanConsole  ]: Configuring 20UPGFC0013396
[13:49:36:701][  info  ][  scanConsole  ]: Configuring 20UPGFC0013428
[13:49:36:732][  info  ][  scanConsole  ]: Configuring 20UPGFC0013381
[13:49:36:763][  info  ][  scanConsole  ]: Configuring 20UPGFC0013349
[13:49:36:794][  info  ][  scanConsole  ]: Sent configuration to all FEs in 123 ms!
[13:49:36:795][  info  ][  scanConsole  ]: Checking com 20UPGFC0013396
[13:49:36:795][  info  ][    SpecRx     ]: Active Rx channels: 0x10
[13:49:36:795][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[13:49:36:795][  info  ][    SpecRx     ]: Rx Status 0xf0
[13:49:36:795][  info  ][    SpecRx     ]: Number of lanes: 1
[13:49:36:795][  info  ][    SpecRx     ]: Channel 4 Lane 0 synchronized!
[13:49:36:805][  info  ][  scanConsole  ]: ... success!
[13:49:36:805][  info  ][  scanConsole  ]: Checking com 20UPGFC0013428
[13:49:36:805][  info  ][    SpecRx     ]: Active Rx channels: 0x20
[13:49:36:805][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[13:49:36:805][  info  ][    SpecRx     ]: Rx Status 0xf0
[13:49:36:805][  info  ][    SpecRx     ]: Number of lanes: 1
[13:49:36:805][  info  ][    SpecRx     ]: Channel 5 Lane 0 synchronized!
[13:49:36:816][  info  ][  scanConsole  ]: ... success!
[13:49:36:816][  info  ][  scanConsole  ]: Checking com 20UPGFC0013381
[13:49:36:816][  info  ][    SpecRx     ]: Active Rx channels: 0x40
[13:49:36:816][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[13:49:36:816][  info  ][    SpecRx     ]: Rx Status 0xf0
[13:49:36:816][  info  ][    SpecRx     ]: Number of lanes: 1
[13:49:36:816][  info  ][    SpecRx     ]: Channel 6 Lane 0 synchronized!
[13:49:36:826][  info  ][  scanConsole  ]: ... success!
[13:49:36:826][  info  ][  scanConsole  ]: Checking com 20UPGFC0013349
[13:49:36:826][  info  ][    SpecRx     ]: Active Rx channels: 0x80
[13:49:36:826][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[13:49:36:826][  info  ][    SpecRx     ]: Rx Status 0xf0
[13:49:36:826][  info  ][    SpecRx     ]: Number of lanes: 1
[13:49:36:826][  info  ][    SpecRx     ]: Channel 7 Lane 0 synchronized!
[13:49:36:836][  info  ][  scanConsole  ]: ... success!
[13:49:36:836][  info  ][  scanConsole  ]: Enabling Tx channels
[13:49:36:836][  info  ][  scanConsole  ]: Enabling Tx channel 1
[13:49:36:836][  info  ][  scanConsole  ]: Enabling Tx channel 1
[13:49:36:836][  info  ][  scanConsole  ]: Enabling Tx channel 1
[13:49:36:836][  info  ][  scanConsole  ]: Enabling Tx channel 1
[13:49:36:836][  info  ][  scanConsole  ]: Enabling Rx channels
[13:49:36:836][  info  ][  scanConsole  ]: Enabling Rx channel 4
[13:49:36:836][  info  ][  scanConsole  ]: Enabling Rx channel 5
[13:49:36:836][  info  ][  scanConsole  ]: Enabling Rx channel 6
[13:49:36:836][  info  ][  scanConsole  ]: Enabling Rx channel 7
[13:49:36:836][  info  ][  scanConsole  ]: ##############
[13:49:36:836][  info  ][  scanConsole  ]: # Setup Scan #
[13:49:36:836][  info  ][  scanConsole  ]: ##############
[13:49:36:836][  info  ][  scanConsole  ]: Found Scan config, constructing scan ...
[13:49:36:837][  info  ][  ScanFactory  ]: Loading Scan:
[13:49:36:837][  info  ][  ScanFactory  ]:   Name: AnalogScan
[13:49:36:837][  info  ][  ScanFactory  ]:   Number of Loops: 3
[13:49:36:837][  info  ][  ScanFactory  ]:   Loading Loop #0
[13:49:36:837][  info  ][  ScanFactory  ]:    Type: Rd53aReadRegLoop
[13:49:36:837][  info  ][  ScanFactory  ]:    Loading loop config ... 
[13:49:36:837][  info  ][  ScanFactory  ]: ~~~ {
[13:49:36:837][  info  ][  ScanFactory  ]: ~~~     "CurMux": [],
[13:49:36:837][  info  ][  ScanFactory  ]: ~~~     "EnblRingOsc": 0,
[13:49:36:837][  info  ][  ScanFactory  ]: ~~~     "Registers": [],
[13:49:36:837][  info  ][  ScanFactory  ]: ~~~     "RingOscDur": 9,
[13:49:36:837][  info  ][  ScanFactory  ]: ~~~     "RingOscRep": 10,
[13:49:36:837][  info  ][  ScanFactory  ]: ~~~     "VoltMux": [3, 5, 7, 15]
[13:49:36:837][  info  ][  ScanFactory  ]: ~~~ }
[13:49:36:837][  info  ][  ScanFactory  ]:   Loading Loop #1
[13:49:36:837][  info  ][  ScanFactory  ]:    Type: Rd53aTriggerLoop
[13:49:36:837][  info  ][  ScanFactory  ]:    Loading loop config ... 
[13:49:36:837][  info  ][  ScanFactory  ]: ~~~ {
[13:49:36:837][  info  ][  ScanFactory  ]: ~~~     "count": 1,
[13:49:36:837][  info  ][  ScanFactory  ]: ~~~     "delay": 48,
[13:49:36:837][  info  ][  ScanFactory  ]: ~~~     "extTrig": false,
[13:49:36:837][  info  ][  ScanFactory  ]: ~~~     "frequency": 30000.0,
[13:49:36:837][  info  ][  ScanFactory  ]: ~~~     "noInject": false,
[13:49:36:837][  info  ][  ScanFactory  ]: ~~~     "sendEcr": false,
[13:49:36:837][  info  ][  ScanFactory  ]: ~~~     "time": 0.0,
[13:49:36:837][  info  ][  ScanFactory  ]: ~~~     "trigMultiplier": 16
[13:49:36:837][  info  ][  ScanFactory  ]: ~~~ }
[13:49:36:837][  info  ][  ScanFactory  ]:   Loading Loop #2
[13:49:36:837][  info  ][  ScanFactory  ]:    Type: StdDataLoop
[13:49:36:837][warning ][  ScanFactory  ]: ~~~ Config empty.
[13:49:36:837][  info  ][ScanBuildHistogrammers]: Loading histogrammer ...
[13:49:36:837][  info  ][ScanBuildHistogrammers]: ... done!
[13:49:36:837][  info  ][ScanBuildAnalyses]: Loading analyses ...
[13:49:36:837][  info  ][  scanConsole  ]: Running pre scan!
[13:49:36:837][  info  ][  ScanFactory  ]: Entering pre scan phase ...
[13:49:36:837][  info  ][     Rd53a     ]: Write named register: GlobalPulseRt -> 16384
[13:49:36:837][  info  ][     Rd53a     ]: Write named register: InjEnDig -> 0
[13:49:36:837][  info  ][     Rd53a     ]: Write named register: InjVcalDiff -> 0
[13:49:36:837][  info  ][     Rd53a     ]: Write named register: InjVcalHigh -> 3500
[13:49:36:837][  info  ][     Rd53a     ]: Write named register: InjVcalMed -> 500
[13:49:36:837][  info  ][     Rd53a     ]: Write named register: LatencyConfig -> 48
[13:49:36:837][  info  ][  scanConsole  ]: Starting histogrammer and analysis threads:
[13:49:36:837][  info  ][  scanConsole  ]:  .. started threads of Fe 4
[13:49:36:837][  info  ][  scanConsole  ]:  .. started threads of Fe 5
[13:49:36:837][  info  ][  scanConsole  ]:  .. started threads of Fe 6
[13:49:36:837][  info  ][  scanConsole  ]:  .. started threads of Fe 7
[13:49:36:837][  info  ][Rd53aDataProcessor]:   -> Processor thread #0 started!
[13:49:36:837][  info  ][Rd53aDataProcessor]:   -> Processor thread #1 started!
[13:49:36:837][  info  ][Rd53aDataProcessor]:   -> Processor thread #2 started!
[13:49:36:837][  info  ][Rd53aDataProcessor]:   -> Processor thread #3 started!
[13:49:36:837][  info  ][  scanConsole  ]: ########
[13:49:36:837][  info  ][  scanConsole  ]: # Scan #
[13:49:36:837][  info  ][  scanConsole  ]: ########
[13:49:36:837][  info  ][  scanConsole  ]: Starting scan!
[13:49:36:837][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0013396 on Rx 4
[13:49:36:841][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0013396] MON MUX_V: 3 Bias 0 1214 -> 0.253518 V Bias 1 1622 -> 0.33536 V, Temperature 0.0818424 -> 2.33295 C
[13:49:36:845][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0013396] MON MUX_V: 5 Bias 0 1180 -> 0.246697 V Bias 1 1580 -> 0.326935 V, Temperature 0.0802376 -> 1.96771 C
[13:49:36:849][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0013396] MON MUX_V: 7 Bias 0 1155 -> 0.241683 V Bias 1 1564 -> 0.323726 V, Temperature 0.082043 -> 1.12518 C
[13:49:36:853][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0013396] MON MUX_V: 15 Bias 0 1163 -> 0.243287 V Bias 1 1580 -> 0.326935 V, Temperature 0.0836477 -> 3.34796 C
[13:49:36:888][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0013428 on Rx 5
[13:49:36:892][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0013428] MON MUX_V: 3 Bias 0 1196 -> 0.252142 V Bias 1 1587 -> 0.331351 V, Temperature 0.0792092 -> -0.407776 C
[13:49:36:897][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0013428] MON MUX_V: 5 Bias 0 1151 -> 0.243026 V Bias 1 1553 -> 0.324464 V, Temperature 0.0814376 -> 2.70038 C
[13:49:36:901][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0013428] MON MUX_V: 7 Bias 0 1151 -> 0.243026 V Bias 1 1553 -> 0.324464 V, Temperature 0.0814376 -> 0.789185 C
[13:49:36:905][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0013428] MON MUX_V: 15 Bias 0 1139 -> 0.240595 V Bias 1 1545 -> 0.322843 V, Temperature 0.0822479 -> 2.87753 C
[13:49:36:940][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0013381 on Rx 6
[13:49:36:944][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0013381] MON MUX_V: 3 Bias 0 1188 -> 0.256741 V Bias 1 1581 -> 0.335694 V, Temperature 0.0789529 -> 0.987183 C
[13:49:36:948][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0013381] MON MUX_V: 5 Bias 0 1149 -> 0.248906 V Bias 1 1557 -> 0.330873 V, Temperature 0.0819665 -> -0.16571 C
[13:49:36:953][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0013381] MON MUX_V: 7 Bias 0 1134 -> 0.245893 V Bias 1 1541 -> 0.327658 V, Temperature 0.0817655 -> 1.03024 C
[13:49:36:957][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0013381] MON MUX_V: 15 Bias 0 1108 -> 0.240669 V Bias 1 1521 -> 0.32364 V, Temperature 0.0829709 -> -0.529572 C
[13:49:36:992][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0013349 on Rx 7
[13:49:36:996][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0013349] MON MUX_V: 3 Bias 0 1229 -> 0.253591 V Bias 1 1626 -> 0.333437 V, Temperature 0.0798465 -> 3.12439 C
[13:49:37:000][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0013349] MON MUX_V: 5 Bias 0 1193 -> 0.24635 V Bias 1 1591 -> 0.326398 V, Temperature 0.0800476 -> 1.23184 C
[13:49:37:005][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0013349] MON MUX_V: 7 Bias 0 1175 -> 0.24273 V Bias 1 1581 -> 0.324387 V, Temperature 0.0816566 -> 3.51495 C
[13:49:37:009][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0013349] MON MUX_V: 15 Bias 0 1180 -> 0.243736 V Bias 1 1584 -> 0.32499 V, Temperature 0.0812544 -> 4.07272 C
[13:49:37:045][  info  ][  scanConsole  ]: Scan done!
[13:49:37:045][  info  ][  scanConsole  ]: Waiting for processors to finish ...
[13:49:37:046][  info  ][  scanConsole  ]: Processor done, waiting for histogrammer ...
[13:49:37:446][  info  ][HistogramAlgorithm]: Histogrammer done!
[13:49:37:446][  info  ][HistogramAlgorithm]: Histogrammer done!
[13:49:37:446][  info  ][HistogramAlgorithm]: Histogrammer done!
[13:49:37:446][  info  ][HistogramAlgorithm]: Histogrammer done!
[13:49:37:446][  info  ][  scanConsole  ]: Processor done, waiting for analysis ...
[13:49:37:846][  info  ][AnalysisAlgorithm]: Analysis done!
[13:49:37:846][  info  ][AnalysisAlgorithm]: Analysis done!
[13:49:37:846][  info  ][AnalysisAlgorithm]: Analysis done!
[13:49:37:846][  info  ][AnalysisAlgorithm]: Analysis done!
[13:49:37:847][  info  ][  scanConsole  ]: All done!
[13:49:37:847][  info  ][  scanConsole  ]: ##########
[13:49:37:847][  info  ][  scanConsole  ]: # Timing #
[13:49:37:847][  info  ][  scanConsole  ]: ##########
[13:49:37:847][  info  ][  scanConsole  ]: -> Configuration: 123 ms
[13:49:37:847][  info  ][  scanConsole  ]: -> Scan:          207 ms
[13:49:37:847][  info  ][  scanConsole  ]: -> Processing:    0 ms
[13:49:37:847][  info  ][  scanConsole  ]: -> Analysis:      800 ms
[13:49:37:847][  info  ][  scanConsole  ]: ###########
[13:49:37:847][  info  ][  scanConsole  ]: # Cleanup #
[13:49:37:847][  info  ][  scanConsole  ]: ###########
[13:49:37:855][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0013396 to configs/KEKQ17/20UPGFC0013396.json
[13:49:38:029][  info  ][  scanConsole  ]: -> Storing output results of FE 4
[13:49:38:029][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0013396, this usually means that the chip did not send any data at all.
[13:49:38:030][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0013428 to configs/KEKQ17/20UPGFC0013428.json
[13:49:38:192][  info  ][  scanConsole  ]: -> Storing output results of FE 5
[13:49:38:192][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0013428, this usually means that the chip did not send any data at all.
[13:49:38:193][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0013381 to configs/KEKQ17/20UPGFC0013381.json
[13:49:38:357][  info  ][  scanConsole  ]: -> Storing output results of FE 6
[13:49:38:357][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0013381, this usually means that the chip did not send any data at all.
[13:49:38:358][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0013349 to configs/KEKQ17/20UPGFC0013349.json
[13:49:38:521][  info  ][  scanConsole  ]: -> Storing output results of FE 7
[13:49:38:521][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0013349, this usually means that the chip did not send any data at all.
[13:49:38:522][  info  ][  scanConsole  ]: Finishing run: 1804
20UPGFC0013349.json.after
20UPGFC0013349.json.before
20UPGFC0013381.json.after
20UPGFC0013381.json.before
20UPGFC0013396.json.after
20UPGFC0013396.json.before
20UPGFC0013428.json.after
20UPGFC0013428.json.before
reg_readtemp.json
scanLog.json

