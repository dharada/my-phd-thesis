[16:31:50:157][  info  ][               ]: #####################################
[16:31:50:157][  info  ][               ]: # Welcome to the YARR Scan Console! #
[16:31:50:157][  info  ][               ]: #####################################
[16:31:50:157][  info  ][               ]: -> Parsing command line parameters ...
[16:31:50:157][  info  ][               ]: Configuring logger ...
[16:31:50:160][  info  ][  scanConsole  ]: Scan Type/Config configs/scans/rd53a/reg_readtemp.json
[16:31:50:160][  info  ][  scanConsole  ]: Connectivity:
[16:31:50:160][  info  ][  scanConsole  ]:     configs/CERNQ5/example_rd53a_setup.json
[16:31:50:160][  info  ][  scanConsole  ]: Target ToT: -1
[16:31:50:160][  info  ][  scanConsole  ]: Target Charge: -1
[16:31:50:160][  info  ][  scanConsole  ]: Output Plots: false
[16:31:50:160][  info  ][  scanConsole  ]: Output Directory: ./data/001474_reg_readtemp/
[16:31:50:164][  info  ][  scanConsole  ]: Timestamp: 2022-08-19_16:31:50
[16:31:50:164][  info  ][  scanConsole  ]: Run Number: 1474
[16:31:50:164][  info  ][  scanConsole  ]: #################
[16:31:50:164][  info  ][  scanConsole  ]: # Init Hardware #
[16:31:50:164][  info  ][  scanConsole  ]: #################
[16:31:50:164][  info  ][  scanConsole  ]: -> Opening controller config: configs/controller/specCfg-rd53a-16x1.json
[16:31:50:164][  info  ][  ScanHelper   ]: Loading controller ...
[16:31:50:164][  info  ][  ScanHelper   ]: Found controller of type: spec
[16:31:50:164][  info  ][  ScanHelper   ]: ... loading controler config:
[16:31:50:164][  info  ][  ScanHelper   ]: ~~~ {
[16:31:50:164][  info  ][  ScanHelper   ]: ~~~     "cmdPeriod": 6.250000073038109e-9,
[16:31:50:164][  info  ][  ScanHelper   ]: ~~~     "idle": {
[16:31:50:164][  info  ][  ScanHelper   ]: ~~~         "word": 1768515945
[16:31:50:164][  info  ][  ScanHelper   ]: ~~~     },
[16:31:50:164][  info  ][  ScanHelper   ]: ~~~     "pulse": {
[16:31:50:164][  info  ][  ScanHelper   ]: ~~~         "interval": 500,
[16:31:50:164][  info  ][  ScanHelper   ]: ~~~         "word": 1549575846
[16:31:50:164][  info  ][  ScanHelper   ]: ~~~     },
[16:31:50:164][  info  ][  ScanHelper   ]: ~~~     "rxActiveLanes": 1,
[16:31:50:164][  info  ][  ScanHelper   ]: ~~~     "rxPolarity": 0,
[16:31:50:164][  info  ][  ScanHelper   ]: ~~~     "specNum": 0,
[16:31:50:164][  info  ][  ScanHelper   ]: ~~~     "spiConfig": 541200,
[16:31:50:164][  info  ][  ScanHelper   ]: ~~~     "sync": {
[16:31:50:164][  info  ][  ScanHelper   ]: ~~~         "interval": 32,
[16:31:50:164][  info  ][  ScanHelper   ]: ~~~         "word": 2172551550
[16:31:50:164][  info  ][  ScanHelper   ]: ~~~     },
[16:31:50:164][  info  ][  ScanHelper   ]: ~~~     "txPolarity": 0
[16:31:50:164][  info  ][  ScanHelper   ]: ~~~ }
[16:31:50:164][  info  ][    SpecCom    ]: Opening SPEC with id #0
[16:31:50:165][  info  ][    SpecCom    ]: Mapping BARs ...
[16:31:50:165][  info  ][    SpecCom    ]: ... Mapped BAR0 at 0x7fd47572a000 with size 1048576
[16:31:50:165][warning ][    SpecCom    ]: ... BAR4 not mapped (Mmap failed)
[16:31:50:165][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[16:31:50:165][  info  ][    SpecCom    ]: Firmware Version: 0x4d9ff6d
[16:31:50:165][  info  ][    SpecCom    ]: Firmware Identifier: 0x4030232
[16:31:50:165][  info  ][    SpecCom    ]: FPGA card: PLDA XpressK7 325
[16:31:50:165][  info  ][    SpecCom    ]: FE Chip Type: RD53A/B
[16:31:50:165][  info  ][    SpecCom    ]: FMC Card Type: Ohio Card (Display Port)
[16:31:50:165][  info  ][    SpecCom    ]: RX Speed: 640Mbps
[16:31:50:165][  info  ][    SpecCom    ]: Channel Configuration: 16x1
[16:31:50:165][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[16:31:50:165][  info  ][    SpecCom    ]: Flushing buffers ...
[16:31:50:165][  info  ][    SpecCom    ]: Init success!
[16:31:50:165][  info  ][  scanConsole  ]: #######################
[16:31:50:165][  info  ][  scanConsole  ]: ##  Loading Configs  ##
[16:31:50:165][  info  ][  scanConsole  ]: #######################
[16:31:50:165][  info  ][  scanConsole  ]: Opening global config: configs/CERNQ5/example_rd53a_setup.json
[16:31:50:165][  info  ][  ScanHelper   ]: Chip type: RD53A
[16:31:50:165][  info  ][  ScanHelper   ]: Chip count 4
[16:31:50:165][  info  ][  ScanHelper   ]: Loading chip #0
[16:31:50:166][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(4)
[16:31:50:166][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ5/20UPGFC0010068.json
[16:31:50:344][  info  ][  ScanHelper   ]: Loading chip #1
[16:31:50:345][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(5)
[16:31:50:345][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ5/20UPGFC0010100.json
[16:31:50:519][  info  ][  ScanHelper   ]: Loading chip #2
[16:31:50:519][warning ][  ScanHelper   ]:  ... chip not enabled, skipping!
[16:31:50:519][  info  ][  ScanHelper   ]: Loading chip #3
[16:31:50:520][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(7)
[16:31:50:520][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ5/20UPGFC0010084.json
[16:31:50:697][  info  ][  scanConsole  ]: #################
[16:31:50:697][  info  ][  scanConsole  ]: # Configure FEs #
[16:31:50:697][  info  ][  scanConsole  ]: #################
[16:31:50:697][  info  ][  scanConsole  ]: Configuring 20UPGFC0010068
[16:31:50:728][  info  ][  scanConsole  ]: Configuring 20UPGFC0010100
[16:31:50:758][  info  ][  scanConsole  ]: Configuring 20UPGFC0010084
[16:31:50:788][  info  ][  scanConsole  ]: Sent configuration to all FEs in 90 ms!
[16:31:50:789][  info  ][  scanConsole  ]: Checking com 20UPGFC0010068
[16:31:50:789][  info  ][    SpecRx     ]: Active Rx channels: 0x10
[16:31:50:789][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[16:31:50:789][  info  ][    SpecRx     ]: Rx Status 0xb0
[16:31:50:789][  info  ][    SpecRx     ]: Number of lanes: 1
[16:31:50:789][  info  ][    SpecRx     ]: Channel 4 Lane 0 synchronized!
[16:31:50:800][  info  ][  scanConsole  ]: ... success!
[16:31:50:800][  info  ][  scanConsole  ]: Checking com 20UPGFC0010100
[16:31:50:800][  info  ][    SpecRx     ]: Active Rx channels: 0x20
[16:31:50:800][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[16:31:50:800][  info  ][    SpecRx     ]: Rx Status 0xb0
[16:31:50:800][  info  ][    SpecRx     ]: Number of lanes: 1
[16:31:50:800][  info  ][    SpecRx     ]: Channel 5 Lane 0 synchronized!
[16:31:50:810][  info  ][  scanConsole  ]: ... success!
[16:31:50:811][  info  ][  scanConsole  ]: Checking com 20UPGFC0010084
[16:31:50:811][  info  ][    SpecRx     ]: Active Rx channels: 0x80
[16:31:50:811][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[16:31:50:811][  info  ][    SpecRx     ]: Rx Status 0xb0
[16:31:50:811][  info  ][    SpecRx     ]: Number of lanes: 1
[16:31:50:811][  info  ][    SpecRx     ]: Channel 7 Lane 0 synchronized!
[16:31:50:821][  info  ][  scanConsole  ]: ... success!
[16:31:50:821][  info  ][  scanConsole  ]: Enabling Tx channels
[16:31:50:821][  info  ][  scanConsole  ]: Enabling Tx channel 1
[16:31:50:821][  info  ][  scanConsole  ]: Enabling Tx channel 1
[16:31:50:821][  info  ][  scanConsole  ]: Enabling Tx channel 1
[16:31:50:821][  info  ][  scanConsole  ]: Enabling Rx channels
[16:31:50:821][  info  ][  scanConsole  ]: Enabling Rx channel 4
[16:31:50:821][  info  ][  scanConsole  ]: Enabling Rx channel 5
[16:31:50:821][  info  ][  scanConsole  ]: Enabling Rx channel 7
[16:31:50:821][  info  ][  scanConsole  ]: ##############
[16:31:50:821][  info  ][  scanConsole  ]: # Setup Scan #
[16:31:50:821][  info  ][  scanConsole  ]: ##############
[16:31:50:822][  info  ][  scanConsole  ]: Found Scan config, constructing scan ...
[16:31:50:822][  info  ][  ScanFactory  ]: Loading Scan:
[16:31:50:822][  info  ][  ScanFactory  ]:   Name: AnalogScan
[16:31:50:822][  info  ][  ScanFactory  ]:   Number of Loops: 3
[16:31:50:822][  info  ][  ScanFactory  ]:   Loading Loop #0
[16:31:50:822][  info  ][  ScanFactory  ]:    Type: Rd53aReadRegLoop
[16:31:50:822][  info  ][  ScanFactory  ]:    Loading loop config ... 
[16:31:50:822][  info  ][  ScanFactory  ]: ~~~ {
[16:31:50:822][  info  ][  ScanFactory  ]: ~~~     "CurMux": [],
[16:31:50:823][  info  ][  ScanFactory  ]: ~~~     "EnblRingOsc": 0,
[16:31:50:823][  info  ][  ScanFactory  ]: ~~~     "Registers": [],
[16:31:50:823][  info  ][  ScanFactory  ]: ~~~     "RingOscDur": 9,
[16:31:50:823][  info  ][  ScanFactory  ]: ~~~     "RingOscRep": 10,
[16:31:50:823][  info  ][  ScanFactory  ]: ~~~     "VoltMux": [3, 5, 7, 15]
[16:31:50:823][  info  ][  ScanFactory  ]: ~~~ }
[16:31:50:823][  info  ][  ScanFactory  ]:   Loading Loop #1
[16:31:50:823][  info  ][  ScanFactory  ]:    Type: Rd53aTriggerLoop
[16:31:50:823][  info  ][  ScanFactory  ]:    Loading loop config ... 
[16:31:50:823][  info  ][  ScanFactory  ]: ~~~ {
[16:31:50:823][  info  ][  ScanFactory  ]: ~~~     "count": 1,
[16:31:50:823][  info  ][  ScanFactory  ]: ~~~     "delay": 48,
[16:31:50:823][  info  ][  ScanFactory  ]: ~~~     "extTrig": false,
[16:31:50:823][  info  ][  ScanFactory  ]: ~~~     "frequency": 30000.0,
[16:31:50:823][  info  ][  ScanFactory  ]: ~~~     "noInject": false,
[16:31:50:823][  info  ][  ScanFactory  ]: ~~~     "sendEcr": false,
[16:31:50:823][  info  ][  ScanFactory  ]: ~~~     "time": 0.0,
[16:31:50:823][  info  ][  ScanFactory  ]: ~~~     "trigMultiplier": 16
[16:31:50:823][  info  ][  ScanFactory  ]: ~~~ }
[16:31:50:823][  info  ][  ScanFactory  ]:   Loading Loop #2
[16:31:50:823][  info  ][  ScanFactory  ]:    Type: StdDataLoop
[16:31:50:823][warning ][  ScanFactory  ]: ~~~ Config empty.
[16:31:50:823][  info  ][ScanBuildHistogrammers]: Loading histogrammer ...
[16:31:50:823][  info  ][ScanBuildHistogrammers]: ... done!
[16:31:50:824][  info  ][ScanBuildAnalyses]: Loading analyses ...
[16:31:50:824][  info  ][  scanConsole  ]: Running pre scan!
[16:31:50:824][  info  ][  ScanFactory  ]: Entering pre scan phase ...
[16:31:50:824][  info  ][     Rd53a     ]: Write named register: GlobalPulseRt -> 16384
[16:31:50:824][  info  ][     Rd53a     ]: Write named register: InjEnDig -> 0
[16:31:50:824][  info  ][     Rd53a     ]: Write named register: InjVcalDiff -> 0
[16:31:50:824][  info  ][     Rd53a     ]: Write named register: InjVcalHigh -> 3500
[16:31:50:824][  info  ][     Rd53a     ]: Write named register: InjVcalMed -> 500
[16:31:50:824][  info  ][     Rd53a     ]: Write named register: LatencyConfig -> 48
[16:31:50:824][  info  ][  scanConsole  ]: Starting histogrammer and analysis threads:
[16:31:50:824][  info  ][  scanConsole  ]:  .. started threads of Fe 4
[16:31:50:825][  info  ][  scanConsole  ]:  .. started threads of Fe 5
[16:31:50:825][  info  ][  scanConsole  ]:  .. started threads of Fe 7
[16:31:50:825][  info  ][Rd53aDataProcessor]:   -> Processor thread #0 started!
[16:31:50:825][  info  ][Rd53aDataProcessor]:   -> Processor thread #1 started!
[16:31:50:825][  info  ][Rd53aDataProcessor]:   -> Processor thread #2 started!
[16:31:50:825][  info  ][Rd53aDataProcessor]:   -> Processor thread #3 started!
[16:31:50:825][  info  ][  scanConsole  ]: ########
[16:31:50:825][  info  ][  scanConsole  ]: # Scan #
[16:31:50:826][  info  ][  scanConsole  ]: ########
[16:31:50:826][  info  ][  scanConsole  ]: Starting scan!
[16:31:50:826][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0010068 on Rx 4
[16:31:50:830][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0010068] MON MUX_V: 3 Bias 0 1222 -> 0.256789 V Bias 1 1626 -> 0.337526 V, Temperature 0.080737 -> 3.75372 C
[16:31:50:834][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0010068] MON MUX_V: 5 Bias 0 1195 -> 0.251393 V Bias 1 1592 -> 0.330731 V, Temperature 0.0793381 -> -1.47995 C
[16:31:50:838][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0010068] MON MUX_V: 7 Bias 0 1192 -> 0.250793 V Bias 1 1599 -> 0.33213 V, Temperature 0.0813365 -> -2.47693 C
[16:31:50:842][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0010068] MON MUX_V: 15 Bias 0 1184 -> 0.249195 V Bias 1 1600 -> 0.33233 V, Temperature 0.0831351 -> -2.91992 C
[16:31:50:877][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0010100 on Rx 5
[16:31:50:881][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0010100] MON MUX_V: 3 Bias 0 1212 -> 0.264038 V Bias 1 1621 -> 0.34521 V, Temperature 0.0811717 -> 3.95479 C
[16:31:50:885][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0010100] MON MUX_V: 5 Bias 0 1172 -> 0.2561 V Bias 1 1577 -> 0.336478 V, Temperature 0.0803779 -> -2.6499 C
[16:31:50:889][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0010100] MON MUX_V: 7 Bias 0 1157 -> 0.253123 V Bias 1 1573 -> 0.335684 V, Temperature 0.082561 -> -3.40012 C
[16:31:50:893][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0010100] MON MUX_V: 15 Bias 0 1158 -> 0.253321 V Bias 1 1564 -> 0.333898 V, Temperature 0.0805764 -> -4.14993 C
[16:31:50:928][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0010084 on Rx 7
[16:31:50:932][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0010084] MON MUX_V: 3 Bias 0 1234 -> 0.266943 V Bias 1 1631 -> 0.346159 V, Temperature 0.079216 -> 2.97508 C
[16:31:50:936][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0010084] MON MUX_V: 5 Bias 0 1199 -> 0.259959 V Bias 1 1595 -> 0.338976 V, Temperature 0.0790164 -> -2.19998 C
[16:31:50:940][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0010084] MON MUX_V: 7 Bias 0 1200 -> 0.260159 V Bias 1 1592 -> 0.338377 V, Temperature 0.0782183 -> -7.83472 C
[16:31:50:944][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0010084] MON MUX_V: 15 Bias 0 1180 -> 0.256168 V Bias 1 1586 -> 0.33718 V, Temperature 0.0810118 -> -3.16925 C
[16:31:50:980][  info  ][  scanConsole  ]: Scan done!
[16:31:50:980][  info  ][  scanConsole  ]: Waiting for processors to finish ...
[16:31:50:981][  info  ][  scanConsole  ]: Processor done, waiting for histogrammer ...
[16:31:51:381][  info  ][HistogramAlgorithm]: Histogrammer done!
[16:31:51:381][  info  ][HistogramAlgorithm]: Histogrammer done!
[16:31:51:381][  info  ][HistogramAlgorithm]: Histogrammer done!
[16:31:51:381][  info  ][  scanConsole  ]: Processor done, waiting for analysis ...
[16:31:51:781][  info  ][AnalysisAlgorithm]: Analysis done!
[16:31:51:781][  info  ][AnalysisAlgorithm]: Analysis done!
[16:31:51:781][  info  ][AnalysisAlgorithm]: Analysis done!
[16:31:51:781][  info  ][  scanConsole  ]: All done!
[16:31:51:781][  info  ][  scanConsole  ]: ##########
[16:31:51:781][  info  ][  scanConsole  ]: # Timing #
[16:31:51:781][  info  ][  scanConsole  ]: ##########
[16:31:51:781][  info  ][  scanConsole  ]: -> Configuration: 90 ms
[16:31:51:781][  info  ][  scanConsole  ]: -> Scan:          154 ms
[16:31:51:781][  info  ][  scanConsole  ]: -> Processing:    0 ms
[16:31:51:781][  info  ][  scanConsole  ]: -> Analysis:      800 ms
[16:31:51:781][  info  ][  scanConsole  ]: ###########
[16:31:51:781][  info  ][  scanConsole  ]: # Cleanup #
[16:31:51:781][  info  ][  scanConsole  ]: ###########
[16:31:51:783][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0010068 to configs/CERNQ5/20UPGFC0010068.json
[16:31:51:950][  info  ][  scanConsole  ]: -> Storing output results of FE 4
[16:31:51:950][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0010068, this usually means that the chip did not send any data at all.
[16:31:51:951][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0010100 to configs/CERNQ5/20UPGFC0010100.json
[16:31:52:116][  info  ][  scanConsole  ]: -> Storing output results of FE 5
[16:31:52:116][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0010100, this usually means that the chip did not send any data at all.
[16:31:52:118][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0010084 to configs/CERNQ5/20UPGFC0010084.json
[16:31:52:282][  info  ][  scanConsole  ]: -> Storing output results of FE 7
[16:31:52:282][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0010084, this usually means that the chip did not send any data at all.
[16:31:52:284][  info  ][  scanConsole  ]: Finishing run: 1474
[root@jollyroger Yarr]# 
