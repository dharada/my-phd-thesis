[15:37:34:825][  info  ][               ]: #####################################
[15:37:34:825][  info  ][               ]: # Welcome to the YARR Scan Console! #
[15:37:34:825][  info  ][               ]: #####################################
[15:37:34:825][  info  ][               ]: -> Parsing command line parameters ...
[15:37:34:825][  info  ][               ]: Configuring logger ...
[15:37:34:834][  info  ][  scanConsole  ]: Scan Type/Config configs/scans/rd53a/reg_readtemp.json
[15:37:34:834][  info  ][  scanConsole  ]: Connectivity:
[15:37:34:834][  info  ][  scanConsole  ]:     configs/CERNQ5/example_rd53a_setup.json
[15:37:34:834][  info  ][  scanConsole  ]: Target ToT: -1
[15:37:34:834][  info  ][  scanConsole  ]: Target Charge: -1
[15:37:34:834][  info  ][  scanConsole  ]: Output Plots: false
[15:37:34:834][  info  ][  scanConsole  ]: Output Directory: ./data/001466_reg_readtemp/
[15:37:34:852][  info  ][  scanConsole  ]: Timestamp: 2022-08-19_15:37:34
[15:37:34:852][  info  ][  scanConsole  ]: Run Number: 1466
[15:37:34:852][  info  ][  scanConsole  ]: #################
[15:37:34:852][  info  ][  scanConsole  ]: # Init Hardware #
[15:37:34:852][  info  ][  scanConsole  ]: #################
[15:37:34:852][  info  ][  scanConsole  ]: -> Opening controller config: configs/controller/specCfg-rd53a-16x1.json
[15:37:34:853][  info  ][  ScanHelper   ]: Loading controller ...
[15:37:34:853][  info  ][  ScanHelper   ]: Found controller of type: spec
[15:37:34:853][  info  ][  ScanHelper   ]: ... loading controler config:
[15:37:34:853][  info  ][  ScanHelper   ]: ~~~ {
[15:37:34:853][  info  ][  ScanHelper   ]: ~~~     "cmdPeriod": 6.250000073038109e-9,
[15:37:34:853][  info  ][  ScanHelper   ]: ~~~     "idle": {
[15:37:34:853][  info  ][  ScanHelper   ]: ~~~         "word": 1768515945
[15:37:34:853][  info  ][  ScanHelper   ]: ~~~     },
[15:37:34:853][  info  ][  ScanHelper   ]: ~~~     "pulse": {
[15:37:34:853][  info  ][  ScanHelper   ]: ~~~         "interval": 500,
[15:37:34:853][  info  ][  ScanHelper   ]: ~~~         "word": 1549575846
[15:37:34:853][  info  ][  ScanHelper   ]: ~~~     },
[15:37:34:853][  info  ][  ScanHelper   ]: ~~~     "rxActiveLanes": 1,
[15:37:34:853][  info  ][  ScanHelper   ]: ~~~     "rxPolarity": 0,
[15:37:34:853][  info  ][  ScanHelper   ]: ~~~     "specNum": 0,
[15:37:34:853][  info  ][  ScanHelper   ]: ~~~     "spiConfig": 541200,
[15:37:34:853][  info  ][  ScanHelper   ]: ~~~     "sync": {
[15:37:34:853][  info  ][  ScanHelper   ]: ~~~         "interval": 32,
[15:37:34:853][  info  ][  ScanHelper   ]: ~~~         "word": 2172551550
[15:37:34:853][  info  ][  ScanHelper   ]: ~~~     },
[15:37:34:853][  info  ][  ScanHelper   ]: ~~~     "txPolarity": 0
[15:37:34:853][  info  ][  ScanHelper   ]: ~~~ }
[15:37:34:853][  info  ][    SpecCom    ]: Opening SPEC with id #0
[15:37:34:854][  info  ][    SpecCom    ]: Mapping BARs ...
[15:37:34:854][  info  ][    SpecCom    ]: ... Mapped BAR0 at 0x7f6f86e5f000 with size 1048576
[15:37:34:854][warning ][    SpecCom    ]: ... BAR4 not mapped (Mmap failed)
[15:37:34:854][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[15:37:34:854][  info  ][    SpecCom    ]: Firmware Version: 0x4d9ff6d
[15:37:34:854][  info  ][    SpecCom    ]: Firmware Identifier: 0x4030232
[15:37:34:854][  info  ][    SpecCom    ]: FPGA card: PLDA XpressK7 325
[15:37:34:854][  info  ][    SpecCom    ]: FE Chip Type: RD53A/B
[15:37:34:854][  info  ][    SpecCom    ]: FMC Card Type: Ohio Card (Display Port)
[15:37:34:854][  info  ][    SpecCom    ]: RX Speed: 640Mbps
[15:37:34:854][  info  ][    SpecCom    ]: Channel Configuration: 16x1
[15:37:34:854][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[15:37:34:854][  info  ][    SpecCom    ]: Flushing buffers ...
[15:37:34:854][  info  ][    SpecCom    ]: Init success!
[15:37:34:854][  info  ][  scanConsole  ]: #######################
[15:37:34:854][  info  ][  scanConsole  ]: ##  Loading Configs  ##
[15:37:34:854][  info  ][  scanConsole  ]: #######################
[15:37:34:854][  info  ][  scanConsole  ]: Opening global config: configs/CERNQ5/example_rd53a_setup.json
[15:37:34:855][  info  ][  ScanHelper   ]: Chip type: RD53A
[15:37:34:855][  info  ][  ScanHelper   ]: Chip count 4
[15:37:34:855][  info  ][  ScanHelper   ]: Loading chip #0
[15:37:34:858][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(4)
[15:37:34:858][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ5/20UPGFC0010068.json
[15:37:35:060][  info  ][  ScanHelper   ]: Loading chip #1
[15:37:35:061][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(5)
[15:37:35:061][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ5/20UPGFC0010100.json
[15:37:35:238][  info  ][  ScanHelper   ]: Loading chip #2
[15:37:35:238][warning ][  ScanHelper   ]:  ... chip not enabled, skipping!
[15:37:35:238][  info  ][  ScanHelper   ]: Loading chip #3
[15:37:35:239][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(7)
[15:37:35:239][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ5/20UPGFC0010084.json
[15:37:35:418][  info  ][  scanConsole  ]: #################
[15:37:35:418][  info  ][  scanConsole  ]: # Configure FEs #
[15:37:35:418][  info  ][  scanConsole  ]: #################
[15:37:35:418][  info  ][  scanConsole  ]: Configuring 20UPGFC0010068
[15:37:35:449][  info  ][  scanConsole  ]: Configuring 20UPGFC0010100
[15:37:35:479][  info  ][  scanConsole  ]: Configuring 20UPGFC0010084
[15:37:35:509][  info  ][  scanConsole  ]: Sent configuration to all FEs in 90 ms!
[15:37:35:510][  info  ][  scanConsole  ]: Checking com 20UPGFC0010068
[15:37:35:510][  info  ][    SpecRx     ]: Active Rx channels: 0x10
[15:37:35:510][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[15:37:35:510][  info  ][    SpecRx     ]: Rx Status 0xb0
[15:37:35:510][  info  ][    SpecRx     ]: Number of lanes: 1
[15:37:35:510][  info  ][    SpecRx     ]: Channel 4 Lane 0 synchronized!
[15:37:35:521][  info  ][  scanConsole  ]: ... success!
[15:37:35:521][  info  ][  scanConsole  ]: Checking com 20UPGFC0010100
[15:37:35:521][  info  ][    SpecRx     ]: Active Rx channels: 0x20
[15:37:35:521][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[15:37:35:521][  info  ][    SpecRx     ]: Rx Status 0xb0
[15:37:35:521][  info  ][    SpecRx     ]: Number of lanes: 1
[15:37:35:521][  info  ][    SpecRx     ]: Channel 5 Lane 0 synchronized!
[15:37:35:532][  info  ][  scanConsole  ]: ... success!
[15:37:35:532][  info  ][  scanConsole  ]: Checking com 20UPGFC0010084
[15:37:35:532][  info  ][    SpecRx     ]: Active Rx channels: 0x80
[15:37:35:532][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[15:37:35:532][  info  ][    SpecRx     ]: Rx Status 0xb0
[15:37:35:532][  info  ][    SpecRx     ]: Number of lanes: 1
[15:37:35:532][  info  ][    SpecRx     ]: Channel 7 Lane 0 synchronized!
[15:37:35:542][  info  ][  scanConsole  ]: ... success!
[15:37:35:542][  info  ][  scanConsole  ]: Enabling Tx channels
[15:37:35:542][  info  ][  scanConsole  ]: Enabling Tx channel 1
[15:37:35:542][  info  ][  scanConsole  ]: Enabling Tx channel 1
[15:37:35:542][  info  ][  scanConsole  ]: Enabling Tx channel 1
[15:37:35:542][  info  ][  scanConsole  ]: Enabling Rx channels
[15:37:35:542][  info  ][  scanConsole  ]: Enabling Rx channel 4
[15:37:35:542][  info  ][  scanConsole  ]: Enabling Rx channel 5
[15:37:35:542][  info  ][  scanConsole  ]: Enabling Rx channel 7
[15:37:35:543][  info  ][  scanConsole  ]: ##############
[15:37:35:543][  info  ][  scanConsole  ]: # Setup Scan #
[15:37:35:543][  info  ][  scanConsole  ]: ##############
[15:37:35:543][  info  ][  scanConsole  ]: Found Scan config, constructing scan ...
[15:37:35:543][  info  ][  ScanFactory  ]: Loading Scan:
[15:37:35:543][  info  ][  ScanFactory  ]:   Name: AnalogScan
[15:37:35:543][  info  ][  ScanFactory  ]:   Number of Loops: 3
[15:37:35:543][  info  ][  ScanFactory  ]:   Loading Loop #0
[15:37:35:543][  info  ][  ScanFactory  ]:    Type: Rd53aReadRegLoop
[15:37:35:543][  info  ][  ScanFactory  ]:    Loading loop config ... 
[15:37:35:544][  info  ][  ScanFactory  ]: ~~~ {
[15:37:35:544][  info  ][  ScanFactory  ]: ~~~     "CurMux": [],
[15:37:35:544][  info  ][  ScanFactory  ]: ~~~     "EnblRingOsc": 0,
[15:37:35:544][  info  ][  ScanFactory  ]: ~~~     "Registers": [],
[15:37:35:544][  info  ][  ScanFactory  ]: ~~~     "RingOscDur": 9,
[15:37:35:544][  info  ][  ScanFactory  ]: ~~~     "RingOscRep": 10,
[15:37:35:544][  info  ][  ScanFactory  ]: ~~~     "VoltMux": [3, 5, 7, 15]
[15:37:35:544][  info  ][  ScanFactory  ]: ~~~ }
[15:37:35:544][  info  ][  ScanFactory  ]:   Loading Loop #1
[15:37:35:544][  info  ][  ScanFactory  ]:    Type: Rd53aTriggerLoop
[15:37:35:544][  info  ][  ScanFactory  ]:    Loading loop config ... 
[15:37:35:544][  info  ][  ScanFactory  ]: ~~~ {
[15:37:35:544][  info  ][  ScanFactory  ]: ~~~     "count": 1,
[15:37:35:544][  info  ][  ScanFactory  ]: ~~~     "delay": 48,
[15:37:35:544][  info  ][  ScanFactory  ]: ~~~     "extTrig": false,
[15:37:35:544][  info  ][  ScanFactory  ]: ~~~     "frequency": 30000.0,
[15:37:35:544][  info  ][  ScanFactory  ]: ~~~     "noInject": false,
[15:37:35:544][  info  ][  ScanFactory  ]: ~~~     "sendEcr": false,
[15:37:35:544][  info  ][  ScanFactory  ]: ~~~     "time": 0.0,
[15:37:35:544][  info  ][  ScanFactory  ]: ~~~     "trigMultiplier": 16
[15:37:35:544][  info  ][  ScanFactory  ]: ~~~ }
[15:37:35:544][  info  ][  ScanFactory  ]:   Loading Loop #2
[15:37:35:544][  info  ][  ScanFactory  ]:    Type: StdDataLoop
[15:37:35:544][warning ][  ScanFactory  ]: ~~~ Config empty.
[15:37:35:544][  info  ][ScanBuildHistogrammers]: Loading histogrammer ...
[15:37:35:545][  info  ][ScanBuildHistogrammers]: ... done!
[15:37:35:545][  info  ][ScanBuildAnalyses]: Loading analyses ...
[15:37:35:545][  info  ][  scanConsole  ]: Running pre scan!
[15:37:35:545][  info  ][  ScanFactory  ]: Entering pre scan phase ...
[15:37:35:545][  info  ][     Rd53a     ]: Write named register: GlobalPulseRt -> 16384
[15:37:35:545][  info  ][     Rd53a     ]: Write named register: InjEnDig -> 0
[15:37:35:545][  info  ][     Rd53a     ]: Write named register: InjVcalDiff -> 0
[15:37:35:545][  info  ][     Rd53a     ]: Write named register: InjVcalHigh -> 3500
[15:37:35:545][  info  ][     Rd53a     ]: Write named register: InjVcalMed -> 500
[15:37:35:545][  info  ][     Rd53a     ]: Write named register: LatencyConfig -> 48
[15:37:35:545][  info  ][  scanConsole  ]: Starting histogrammer and analysis threads:
[15:37:35:546][  info  ][  scanConsole  ]:  .. started threads of Fe 4
[15:37:35:546][  info  ][  scanConsole  ]:  .. started threads of Fe 5
[15:37:35:546][  info  ][  scanConsole  ]:  .. started threads of Fe 7
[15:37:35:546][  info  ][Rd53aDataProcessor]:   -> Processor thread #0 started!
[15:37:35:546][  info  ][Rd53aDataProcessor]:   -> Processor thread #1 started!
[15:37:35:546][  info  ][Rd53aDataProcessor]:   -> Processor thread #2 started!
[15:37:35:546][  info  ][Rd53aDataProcessor]:   -> Processor thread #3 started!
[15:37:35:546][  info  ][  scanConsole  ]: ########
[15:37:35:546][  info  ][  scanConsole  ]: # Scan #
[15:37:35:546][  info  ][  scanConsole  ]: ########
[15:37:35:546][  info  ][  scanConsole  ]: Starting scan!
[15:37:35:546][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0010068 on Rx 4
[15:37:35:551][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0010068] MON MUX_V: 3 Bias 0 1170 -> 0.246397 V Bias 1 1585 -> 0.329332 V, Temperature 0.0829353 -> 11.3691 C
[15:37:35:555][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0010068] MON MUX_V: 5 Bias 0 1142 -> 0.240801 V Bias 1 1550 -> 0.322338 V, Temperature 0.0815364 -> 6.43997 C
[15:37:35:559][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0010068] MON MUX_V: 7 Bias 0 1137 -> 0.239802 V Bias 1 1556 -> 0.323537 V, Temperature 0.0837347 -> 5.83078 C
[15:37:35:563][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0010068] MON MUX_V: 15 Bias 0 1129 -> 0.238203 V Bias 1 1557 -> 0.323737 V, Temperature 0.0855333 -> 5.72009 C
[15:37:35:598][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0010100 on Rx 5
[15:37:35:602][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0010100] MON MUX_V: 3 Bias 0 1159 -> 0.25352 V Bias 1 1580 -> 0.337073 V, Temperature 0.0835533 -> 10.9226 C
[15:37:35:607][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0010100] MON MUX_V: 5 Bias 0 1118 -> 0.245383 V Bias 1 1535 -> 0.328142 V, Temperature 0.0827594 -> 6.34991 C
[15:37:35:611][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0010100] MON MUX_V: 7 Bias 0 1104 -> 0.242604 V Bias 1 1534 -> 0.327944 V, Temperature 0.0853395 -> 7.10004 C
[15:37:35:615][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0010100] MON MUX_V: 15 Bias 0 1104 -> 0.242604 V Bias 1 1524 -> 0.325959 V, Temperature 0.0833549 -> 6.35007 C
[15:37:35:650][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0010084 on Rx 7
[15:37:35:654][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0010084] MON MUX_V: 3 Bias 0 1181 -> 0.256368 V Bias 1 1592 -> 0.338377 V, Temperature 0.0820095 -> 10.85 C
[15:37:35:658][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0010084] MON MUX_V: 5 Bias 0 1148 -> 0.249783 V Bias 1 1557 -> 0.331393 V, Temperature 0.0816104 -> 7.15994 C
[15:37:35:663][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0010084] MON MUX_V: 7 Bias 0 1147 -> 0.249583 V Bias 1 1553 -> 0.330595 V, Temperature 0.0810118 -> 3.1218 C
[15:37:35:667][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0010084] MON MUX_V: 15 Bias 0 1129 -> 0.245992 V Bias 1 1548 -> 0.329598 V, Temperature 0.0836058 -> 5.83084 C
[15:37:35:703][  info  ][  scanConsole  ]: Scan done!
[15:37:35:703][  info  ][  scanConsole  ]: Waiting for processors to finish ...
[15:37:35:704][  info  ][  scanConsole  ]: Processor done, waiting for histogrammer ...
[15:37:36:104][  info  ][HistogramAlgorithm]: Histogrammer done!
[15:37:36:104][  info  ][HistogramAlgorithm]: Histogrammer done!
[15:37:36:104][  info  ][HistogramAlgorithm]: Histogrammer done!
[15:37:36:104][  info  ][  scanConsole  ]: Processor done, waiting for analysis ...
[15:37:36:504][  info  ][AnalysisAlgorithm]: Analysis done!
[15:37:36:504][  info  ][AnalysisAlgorithm]: Analysis done!
[15:37:36:504][  info  ][AnalysisAlgorithm]: Analysis done!
[15:37:36:505][  info  ][  scanConsole  ]: All done!
[15:37:36:505][  info  ][  scanConsole  ]: ##########
[15:37:36:505][  info  ][  scanConsole  ]: # Timing #
[15:37:36:505][  info  ][  scanConsole  ]: ##########
[15:37:36:505][  info  ][  scanConsole  ]: -> Configuration: 90 ms
[15:37:36:505][  info  ][  scanConsole  ]: -> Scan:          156 ms
[15:37:36:505][  info  ][  scanConsole  ]: -> Processing:    1 ms
[15:37:36:505][  info  ][  scanConsole  ]: -> Analysis:      800 ms
[15:37:36:505][  info  ][  scanConsole  ]: ###########
[15:37:36:505][  info  ][  scanConsole  ]: # Cleanup #
[15:37:36:505][  info  ][  scanConsole  ]: ###########
[15:37:36:513][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0010068 to configs/CERNQ5/20UPGFC0010068.json
[15:37:36:683][  info  ][  scanConsole  ]: -> Storing output results of FE 4
[15:37:36:683][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0010068, this usually means that the chip did not send any data at all.
[15:37:36:685][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0010100 to configs/CERNQ5/20UPGFC0010100.json
[15:37:36:851][  info  ][  scanConsole  ]: -> Storing output results of FE 5
[15:37:36:851][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0010100, this usually means that the chip did not send any data at all.
[15:37:36:852][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0010084 to configs/CERNQ5/20UPGFC0010084.json
[15:37:37:017][  info  ][  scanConsole  ]: -> Storing output results of FE 7
[15:37:37:017][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0010084, this usually means that the chip did not send any data at all.
[15:37:37:018][  info  ][  scanConsole  ]: Finishing run: 1466
[root@jollyroger Yarr]# 
