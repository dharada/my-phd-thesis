[16:56:58:077][  info  ][               ]: #####################################
[16:56:58:077][  info  ][               ]: # Welcome to the YARR Scan Console! #
[16:56:58:077][  info  ][               ]: #####################################
[16:56:58:077][  info  ][               ]: -> Parsing command line parameters ...
[16:56:58:077][  info  ][               ]: Configuring logger ...
[16:56:58:080][  info  ][  scanConsole  ]: Scan Type/Config configs/scans/rd53a/reg_readtemp.json
[16:56:58:080][  info  ][  scanConsole  ]: Connectivity:
[16:56:58:080][  info  ][  scanConsole  ]:     configs/CERNQ5/example_rd53a_setup.json
[16:56:58:080][  info  ][  scanConsole  ]: Target ToT: -1
[16:56:58:080][  info  ][  scanConsole  ]: Target Charge: -1
[16:56:58:080][  info  ][  scanConsole  ]: Output Plots: false
[16:56:58:080][  info  ][  scanConsole  ]: Output Directory: ./data/001482_reg_readtemp/
[16:56:58:087][  info  ][  scanConsole  ]: Timestamp: 2022-08-19_16:56:58
[16:56:58:088][  info  ][  scanConsole  ]: Run Number: 1482
[16:56:58:088][  info  ][  scanConsole  ]: #################
[16:56:58:088][  info  ][  scanConsole  ]: # Init Hardware #
[16:56:58:088][  info  ][  scanConsole  ]: #################
[16:56:58:088][  info  ][  scanConsole  ]: -> Opening controller config: configs/controller/specCfg-rd53a-16x1.json
[16:56:58:088][  info  ][  ScanHelper   ]: Loading controller ...
[16:56:58:088][  info  ][  ScanHelper   ]: Found controller of type: spec
[16:56:58:088][  info  ][  ScanHelper   ]: ... loading controler config:
[16:56:58:088][  info  ][  ScanHelper   ]: ~~~ {
[16:56:58:088][  info  ][  ScanHelper   ]: ~~~     "cmdPeriod": 6.250000073038109e-9,
[16:56:58:088][  info  ][  ScanHelper   ]: ~~~     "idle": {
[16:56:58:088][  info  ][  ScanHelper   ]: ~~~         "word": 1768515945
[16:56:58:088][  info  ][  ScanHelper   ]: ~~~     },
[16:56:58:088][  info  ][  ScanHelper   ]: ~~~     "pulse": {
[16:56:58:088][  info  ][  ScanHelper   ]: ~~~         "interval": 500,
[16:56:58:088][  info  ][  ScanHelper   ]: ~~~         "word": 1549575846
[16:56:58:088][  info  ][  ScanHelper   ]: ~~~     },
[16:56:58:088][  info  ][  ScanHelper   ]: ~~~     "rxActiveLanes": 1,
[16:56:58:088][  info  ][  ScanHelper   ]: ~~~     "rxPolarity": 0,
[16:56:58:088][  info  ][  ScanHelper   ]: ~~~     "specNum": 0,
[16:56:58:088][  info  ][  ScanHelper   ]: ~~~     "spiConfig": 541200,
[16:56:58:088][  info  ][  ScanHelper   ]: ~~~     "sync": {
[16:56:58:088][  info  ][  ScanHelper   ]: ~~~         "interval": 32,
[16:56:58:088][  info  ][  ScanHelper   ]: ~~~         "word": 2172551550
[16:56:58:088][  info  ][  ScanHelper   ]: ~~~     },
[16:56:58:088][  info  ][  ScanHelper   ]: ~~~     "txPolarity": 0
[16:56:58:088][  info  ][  ScanHelper   ]: ~~~ }
[16:56:58:088][  info  ][    SpecCom    ]: Opening SPEC with id #0
[16:56:58:088][  info  ][    SpecCom    ]: Mapping BARs ...
[16:56:58:088][  info  ][    SpecCom    ]: ... Mapped BAR0 at 0x7f4fb1f83000 with size 1048576
[16:56:58:088][warning ][    SpecCom    ]: ... BAR4 not mapped (Mmap failed)
[16:56:58:088][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[16:56:58:088][  info  ][    SpecCom    ]: Firmware Version: 0x4d9ff6d
[16:56:58:088][  info  ][    SpecCom    ]: Firmware Identifier: 0x4030232
[16:56:58:088][  info  ][    SpecCom    ]: FPGA card: PLDA XpressK7 325
[16:56:58:088][  info  ][    SpecCom    ]: FE Chip Type: RD53A/B
[16:56:58:088][  info  ][    SpecCom    ]: FMC Card Type: Ohio Card (Display Port)
[16:56:58:088][  info  ][    SpecCom    ]: RX Speed: 640Mbps
[16:56:58:088][  info  ][    SpecCom    ]: Channel Configuration: 16x1
[16:56:58:088][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[16:56:58:088][  info  ][    SpecCom    ]: Flushing buffers ...
[16:56:58:088][  info  ][    SpecCom    ]: Init success!
[16:56:58:088][  info  ][  scanConsole  ]: #######################
[16:56:58:088][  info  ][  scanConsole  ]: ##  Loading Configs  ##
[16:56:58:088][  info  ][  scanConsole  ]: #######################
[16:56:58:088][  info  ][  scanConsole  ]: Opening global config: configs/CERNQ5/example_rd53a_setup.json
[16:56:58:088][  info  ][  ScanHelper   ]: Chip type: RD53A
[16:56:58:088][  info  ][  ScanHelper   ]: Chip count 4
[16:56:58:088][  info  ][  ScanHelper   ]: Loading chip #0
[16:56:58:089][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(4)
[16:56:58:089][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ5/20UPGFC0010068.json
[16:56:58:288][  info  ][  ScanHelper   ]: Loading chip #1
[16:56:58:288][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(5)
[16:56:58:288][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ5/20UPGFC0010100.json
[16:56:58:488][  info  ][  ScanHelper   ]: Loading chip #2
[16:56:58:488][warning ][  ScanHelper   ]:  ... chip not enabled, skipping!
[16:56:58:488][  info  ][  ScanHelper   ]: Loading chip #3
[16:56:58:489][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(7)
[16:56:58:489][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ5/20UPGFC0010084.json
[16:56:58:673][  info  ][  scanConsole  ]: #################
[16:56:58:673][  info  ][  scanConsole  ]: # Configure FEs #
[16:56:58:673][  info  ][  scanConsole  ]: #################
[16:56:58:673][  info  ][  scanConsole  ]: Configuring 20UPGFC0010068
[16:56:58:703][  info  ][  scanConsole  ]: Configuring 20UPGFC0010100
[16:56:58:734][  info  ][  scanConsole  ]: Configuring 20UPGFC0010084
[16:56:58:783][  info  ][  scanConsole  ]: Sent configuration to all FEs in 109 ms!
[16:56:58:784][  info  ][  scanConsole  ]: Checking com 20UPGFC0010068
[16:56:58:784][  info  ][    SpecRx     ]: Active Rx channels: 0x10
[16:56:58:784][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[16:56:58:784][  info  ][    SpecRx     ]: Rx Status 0xb0
[16:56:58:784][  info  ][    SpecRx     ]: Number of lanes: 1
[16:56:58:784][  info  ][    SpecRx     ]: Channel 4 Lane 0 synchronized!
[16:56:58:795][  info  ][  scanConsole  ]: ... success!
[16:56:58:795][  info  ][  scanConsole  ]: Checking com 20UPGFC0010100
[16:56:58:795][  info  ][    SpecRx     ]: Active Rx channels: 0x20
[16:56:58:795][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[16:56:58:795][  info  ][    SpecRx     ]: Rx Status 0xb0
[16:56:58:795][  info  ][    SpecRx     ]: Number of lanes: 1
[16:56:58:795][  info  ][    SpecRx     ]: Channel 5 Lane 0 synchronized!
[16:56:58:805][  info  ][  scanConsole  ]: ... success!
[16:56:58:805][  info  ][  scanConsole  ]: Checking com 20UPGFC0010084
[16:56:58:805][  info  ][    SpecRx     ]: Active Rx channels: 0x80
[16:56:58:805][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[16:56:58:805][  info  ][    SpecRx     ]: Rx Status 0xb0
[16:56:58:805][  info  ][    SpecRx     ]: Number of lanes: 1
[16:56:58:805][  info  ][    SpecRx     ]: Channel 7 Lane 0 synchronized!
[16:56:58:816][  info  ][  scanConsole  ]: ... success!
[16:56:58:816][  info  ][  scanConsole  ]: Enabling Tx channels
[16:56:58:816][  info  ][  scanConsole  ]: Enabling Tx channel 1
[16:56:58:816][  info  ][  scanConsole  ]: Enabling Tx channel 1
[16:56:58:816][  info  ][  scanConsole  ]: Enabling Tx channel 1
[16:56:58:816][  info  ][  scanConsole  ]: Enabling Rx channels
[16:56:58:816][  info  ][  scanConsole  ]: Enabling Rx channel 4
[16:56:58:816][  info  ][  scanConsole  ]: Enabling Rx channel 5
[16:56:58:816][  info  ][  scanConsole  ]: Enabling Rx channel 7
[16:56:58:816][  info  ][  scanConsole  ]: ##############
[16:56:58:816][  info  ][  scanConsole  ]: # Setup Scan #
[16:56:58:816][  info  ][  scanConsole  ]: ##############
[16:56:58:817][  info  ][  scanConsole  ]: Found Scan config, constructing scan ...
[16:56:58:817][  info  ][  ScanFactory  ]: Loading Scan:
[16:56:58:817][  info  ][  ScanFactory  ]:   Name: AnalogScan
[16:56:58:817][  info  ][  ScanFactory  ]:   Number of Loops: 3
[16:56:58:817][  info  ][  ScanFactory  ]:   Loading Loop #0
[16:56:58:817][  info  ][  ScanFactory  ]:    Type: Rd53aReadRegLoop
[16:56:58:817][  info  ][  ScanFactory  ]:    Loading loop config ... 
[16:56:58:817][  info  ][  ScanFactory  ]: ~~~ {
[16:56:58:817][  info  ][  ScanFactory  ]: ~~~     "CurMux": [],
[16:56:58:817][  info  ][  ScanFactory  ]: ~~~     "EnblRingOsc": 0,
[16:56:58:817][  info  ][  ScanFactory  ]: ~~~     "Registers": [],
[16:56:58:817][  info  ][  ScanFactory  ]: ~~~     "RingOscDur": 9,
[16:56:58:817][  info  ][  ScanFactory  ]: ~~~     "RingOscRep": 10,
[16:56:58:818][  info  ][  ScanFactory  ]: ~~~     "VoltMux": [3, 5, 7, 15]
[16:56:58:818][  info  ][  ScanFactory  ]: ~~~ }
[16:56:58:818][  info  ][  ScanFactory  ]:   Loading Loop #1
[16:56:58:818][  info  ][  ScanFactory  ]:    Type: Rd53aTriggerLoop
[16:56:58:818][  info  ][  ScanFactory  ]:    Loading loop config ... 
[16:56:58:818][  info  ][  ScanFactory  ]: ~~~ {
[16:56:58:818][  info  ][  ScanFactory  ]: ~~~     "count": 1,
[16:56:58:818][  info  ][  ScanFactory  ]: ~~~     "delay": 48,
[16:56:58:818][  info  ][  ScanFactory  ]: ~~~     "extTrig": false,
[16:56:58:818][  info  ][  ScanFactory  ]: ~~~     "frequency": 30000.0,
[16:56:58:818][  info  ][  ScanFactory  ]: ~~~     "noInject": false,
[16:56:58:818][  info  ][  ScanFactory  ]: ~~~     "sendEcr": false,
[16:56:58:818][  info  ][  ScanFactory  ]: ~~~     "time": 0.0,
[16:56:58:818][  info  ][  ScanFactory  ]: ~~~     "trigMultiplier": 16
[16:56:58:818][  info  ][  ScanFactory  ]: ~~~ }
[16:56:58:818][  info  ][  ScanFactory  ]:   Loading Loop #2
[16:56:58:818][  info  ][  ScanFactory  ]:    Type: StdDataLoop
[16:56:58:818][warning ][  ScanFactory  ]: ~~~ Config empty.
[16:56:58:818][  info  ][ScanBuildHistogrammers]: Loading histogrammer ...
[16:56:58:818][  info  ][ScanBuildHistogrammers]: ... done!
[16:56:58:818][  info  ][ScanBuildAnalyses]: Loading analyses ...
[16:56:58:819][  info  ][  scanConsole  ]: Running pre scan!
[16:56:58:819][  info  ][  ScanFactory  ]: Entering pre scan phase ...
[16:56:58:819][  info  ][     Rd53a     ]: Write named register: GlobalPulseRt -> 16384
[16:56:58:819][  info  ][     Rd53a     ]: Write named register: InjEnDig -> 0
[16:56:58:819][  info  ][     Rd53a     ]: Write named register: InjVcalDiff -> 0
[16:56:58:819][  info  ][     Rd53a     ]: Write named register: InjVcalHigh -> 3500
[16:56:58:819][  info  ][     Rd53a     ]: Write named register: InjVcalMed -> 500
[16:56:58:819][  info  ][     Rd53a     ]: Write named register: LatencyConfig -> 48
[16:56:58:819][  info  ][  scanConsole  ]: Starting histogrammer and analysis threads:
[16:56:58:819][  info  ][  scanConsole  ]:  .. started threads of Fe 4
[16:56:58:819][  info  ][  scanConsole  ]:  .. started threads of Fe 5
[16:56:58:820][  info  ][  scanConsole  ]:  .. started threads of Fe 7
[16:56:58:820][  info  ][Rd53aDataProcessor]:   -> Processor thread #0 started!
[16:56:58:820][  info  ][Rd53aDataProcessor]:   -> Processor thread #1 started!
[16:56:58:820][  info  ][Rd53aDataProcessor]:   -> Processor thread #2 started!
[16:56:58:820][  info  ][Rd53aDataProcessor]:   -> Processor thread #3 started!
[16:56:58:820][  info  ][  scanConsole  ]: ########
[16:56:58:820][  info  ][  scanConsole  ]: # Scan #
[16:56:58:820][  info  ][  scanConsole  ]: ########
[16:56:58:820][  info  ][  scanConsole  ]: Starting scan!
[16:56:58:820][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0010068 on Rx 4
[16:56:58:824][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0010068] MON MUX_V: 3 Bias 0 1251 -> 0.262584 V Bias 1 1650 -> 0.342322 V, Temperature 0.0797378 -> 0.292206 C
[16:56:58:829][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0010068] MON MUX_V: 5 Bias 0 1223 -> 0.256989 V Bias 1 1613 -> 0.334928 V, Temperature 0.0779392 -> -6.51999 C
[16:56:58:833][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0010068] MON MUX_V: 7 Bias 0 1220 -> 0.256389 V Bias 1 1620 -> 0.336327 V, Temperature 0.0799376 -> -7.32321 C
[16:56:58:837][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0010068] MON MUX_V: 15 Bias 0 1213 -> 0.25499 V Bias 1 1622 -> 0.336726 V, Temperature 0.0817362 -> -7.96002 C
[16:56:58:872][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0010100 on Rx 5
[16:56:58:876][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0010100] MON MUX_V: 3 Bias 0 1240 -> 0.269595 V Bias 1 1644 -> 0.349775 V, Temperature 0.0801794 -> 1.05162 C
[16:56:58:880][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0010100] MON MUX_V: 5 Bias 0 1198 -> 0.26126 V Bias 1 1599 -> 0.340844 V, Temperature 0.079584 -> -5.65009 C
[16:56:58:884][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0010100] MON MUX_V: 7 Bias 0 1186 -> 0.258878 V Bias 1 1596 -> 0.340248 V, Temperature 0.0813702 -> -7.89999 C
[16:56:58:888][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0010100] MON MUX_V: 15 Bias 0 1185 -> 0.25868 V Bias 1 1587 -> 0.338462 V, Temperature 0.0797825 -> -7.1499 C
[16:56:58:923][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0010084 on Rx 7
[16:56:58:927][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0010084] MON MUX_V: 3 Bias 0 1262 -> 0.27253 V Bias 1 1653 -> 0.350549 V, Temperature 0.0780188 -> -0.399918 C
[16:56:58:932][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0010084] MON MUX_V: 5 Bias 0 1228 -> 0.265746 V Bias 1 1617 -> 0.343366 V, Temperature 0.0776197 -> -7.23996 C
[16:56:58:936][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0010084] MON MUX_V: 7 Bias 0 1227 -> 0.265546 V Bias 1 1613 -> 0.342567 V, Temperature 0.0770211 -> -12.5305 C
[16:56:58:940][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0010084] MON MUX_V: 15 Bias 0 1207 -> 0.261556 V Bias 1 1606 -> 0.341171 V, Temperature 0.079615 -> -8.01547 C
[16:56:58:976][  info  ][  scanConsole  ]: Scan done!
[16:56:58:976][  info  ][  scanConsole  ]: Waiting for processors to finish ...
[16:56:58:977][  info  ][  scanConsole  ]: Processor done, waiting for histogrammer ...
[16:56:59:377][  info  ][HistogramAlgorithm]: Histogrammer done!
[16:56:59:377][  info  ][HistogramAlgorithm]: Histogrammer done!
[16:56:59:377][  info  ][HistogramAlgorithm]: Histogrammer done!
[16:56:59:377][  info  ][  scanConsole  ]: Processor done, waiting for analysis ...
[16:56:59:778][  info  ][AnalysisAlgorithm]: Analysis done!
[16:56:59:778][  info  ][AnalysisAlgorithm]: Analysis done!
[16:56:59:778][  info  ][AnalysisAlgorithm]: Analysis done!
[16:56:59:778][  info  ][  scanConsole  ]: All done!
[16:56:59:778][  info  ][  scanConsole  ]: ##########
[16:56:59:778][  info  ][  scanConsole  ]: # Timing #
[16:56:59:778][  info  ][  scanConsole  ]: ##########
[16:56:59:778][  info  ][  scanConsole  ]: -> Configuration: 109 ms
[16:56:59:778][  info  ][  scanConsole  ]: -> Scan:          156 ms
[16:56:59:778][  info  ][  scanConsole  ]: -> Processing:    0 ms
[16:56:59:778][  info  ][  scanConsole  ]: -> Analysis:      800 ms
[16:56:59:778][  info  ][  scanConsole  ]: ###########
[16:56:59:778][  info  ][  scanConsole  ]: # Cleanup #
[16:56:59:778][  info  ][  scanConsole  ]: ###########
[16:56:59:786][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0010068 to configs/CERNQ5/20UPGFC0010068.json
[16:56:59:994][  info  ][  scanConsole  ]: -> Storing output results of FE 4
[16:56:59:994][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0010068, this usually means that the chip did not send any data at all.
[16:56:59:995][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0010100 to configs/CERNQ5/20UPGFC0010100.json
[16:57:00:162][  info  ][  scanConsole  ]: -> Storing output results of FE 5
[16:57:00:162][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0010100, this usually means that the chip did not send any data at all.
[16:57:00:163][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0010084 to configs/CERNQ5/20UPGFC0010084.json
[16:57:00:329][  info  ][  scanConsole  ]: -> Storing output results of FE 7
[16:57:00:329][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0010084, this usually means that the chip did not send any data at all.
[16:57:00:331][  info  ][  scanConsole  ]: Finishing run: 1482
[root@jollyroger Yarr]# 
