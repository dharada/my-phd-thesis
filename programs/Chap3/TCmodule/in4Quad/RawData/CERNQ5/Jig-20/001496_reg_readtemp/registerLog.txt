[17:31:42:952][  info  ][               ]: #####################################
[17:31:42:952][  info  ][               ]: # Welcome to the YARR Scan Console! #
[17:31:42:952][  info  ][               ]: #####################################
[17:31:42:952][  info  ][               ]: -> Parsing command line parameters ...
[17:31:42:952][  info  ][               ]: Configuring logger ...
[17:31:42:961][  info  ][  scanConsole  ]: Scan Type/Config configs/scans/rd53a/reg_readtemp.json
[17:31:42:961][  info  ][  scanConsole  ]: Connectivity:
[17:31:42:961][  info  ][  scanConsole  ]:     configs/CERNQ5/example_rd53a_setup.json
[17:31:42:961][  info  ][  scanConsole  ]: Target ToT: -1
[17:31:42:961][  info  ][  scanConsole  ]: Target Charge: -1
[17:31:42:961][  info  ][  scanConsole  ]: Output Plots: false
[17:31:42:961][  info  ][  scanConsole  ]: Output Directory: ./data/001496_reg_readtemp/
[17:31:42:980][  info  ][  scanConsole  ]: Timestamp: 2022-08-19_17:31:42
[17:31:42:980][  info  ][  scanConsole  ]: Run Number: 1496
[17:31:42:980][  info  ][  scanConsole  ]: #################
[17:31:42:980][  info  ][  scanConsole  ]: # Init Hardware #
[17:31:42:980][  info  ][  scanConsole  ]: #################
[17:31:42:980][  info  ][  scanConsole  ]: -> Opening controller config: configs/controller/specCfg-rd53a-16x1.json
[17:31:42:980][  info  ][  ScanHelper   ]: Loading controller ...
[17:31:42:981][  info  ][  ScanHelper   ]: Found controller of type: spec
[17:31:42:981][  info  ][  ScanHelper   ]: ... loading controler config:
[17:31:42:981][  info  ][  ScanHelper   ]: ~~~ {
[17:31:42:981][  info  ][  ScanHelper   ]: ~~~     "cmdPeriod": 6.250000073038109e-9,
[17:31:42:981][  info  ][  ScanHelper   ]: ~~~     "idle": {
[17:31:42:981][  info  ][  ScanHelper   ]: ~~~         "word": 1768515945
[17:31:42:981][  info  ][  ScanHelper   ]: ~~~     },
[17:31:42:981][  info  ][  ScanHelper   ]: ~~~     "pulse": {
[17:31:42:981][  info  ][  ScanHelper   ]: ~~~         "interval": 500,
[17:31:42:981][  info  ][  ScanHelper   ]: ~~~         "word": 1549575846
[17:31:42:981][  info  ][  ScanHelper   ]: ~~~     },
[17:31:42:981][  info  ][  ScanHelper   ]: ~~~     "rxActiveLanes": 1,
[17:31:42:981][  info  ][  ScanHelper   ]: ~~~     "rxPolarity": 0,
[17:31:42:981][  info  ][  ScanHelper   ]: ~~~     "specNum": 0,
[17:31:42:981][  info  ][  ScanHelper   ]: ~~~     "spiConfig": 541200,
[17:31:42:981][  info  ][  ScanHelper   ]: ~~~     "sync": {
[17:31:42:981][  info  ][  ScanHelper   ]: ~~~         "interval": 32,
[17:31:42:981][  info  ][  ScanHelper   ]: ~~~         "word": 2172551550
[17:31:42:981][  info  ][  ScanHelper   ]: ~~~     },
[17:31:42:981][  info  ][  ScanHelper   ]: ~~~     "txPolarity": 0
[17:31:42:981][  info  ][  ScanHelper   ]: ~~~ }
[17:31:42:981][  info  ][    SpecCom    ]: Opening SPEC with id #0
[17:31:42:981][  info  ][    SpecCom    ]: Mapping BARs ...
[17:31:42:981][  info  ][    SpecCom    ]: ... Mapped BAR0 at 0x7f9d4bbd2000 with size 1048576
[17:31:42:982][warning ][    SpecCom    ]: ... BAR4 not mapped (Mmap failed)
[17:31:42:982][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[17:31:42:982][  info  ][    SpecCom    ]: Firmware Version: 0x4d9ff6d
[17:31:42:982][  info  ][    SpecCom    ]: Firmware Identifier: 0x4030232
[17:31:42:982][  info  ][    SpecCom    ]: FPGA card: PLDA XpressK7 325
[17:31:42:982][  info  ][    SpecCom    ]: FE Chip Type: RD53A/B
[17:31:42:982][  info  ][    SpecCom    ]: FMC Card Type: Ohio Card (Display Port)
[17:31:42:982][  info  ][    SpecCom    ]: RX Speed: 640Mbps
[17:31:42:982][  info  ][    SpecCom    ]: Channel Configuration: 16x1
[17:31:42:982][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[17:31:42:982][  info  ][    SpecCom    ]: Flushing buffers ...
[17:31:42:982][  info  ][    SpecCom    ]: Init success!
[17:31:42:982][  info  ][  scanConsole  ]: #######################
[17:31:42:982][  info  ][  scanConsole  ]: ##  Loading Configs  ##
[17:31:42:982][  info  ][  scanConsole  ]: #######################
[17:31:42:982][  info  ][  scanConsole  ]: Opening global config: configs/CERNQ5/example_rd53a_setup.json
[17:31:42:983][  info  ][  ScanHelper   ]: Chip type: RD53A
[17:31:42:983][  info  ][  ScanHelper   ]: Chip count 4
[17:31:42:983][  info  ][  ScanHelper   ]: Loading chip #0
[17:31:42:986][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(4)
[17:31:42:986][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ5/20UPGFC0010068.json
[17:31:43:200][  info  ][  ScanHelper   ]: Loading chip #1
[17:31:43:201][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(5)
[17:31:43:201][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ5/20UPGFC0010100.json
[17:31:43:394][  info  ][  ScanHelper   ]: Loading chip #2
[17:31:43:394][warning ][  ScanHelper   ]:  ... chip not enabled, skipping!
[17:31:43:394][  info  ][  ScanHelper   ]: Loading chip #3
[17:31:43:395][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(7)
[17:31:43:395][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ5/20UPGFC0010084.json
[17:31:43:593][  info  ][  scanConsole  ]: #################
[17:31:43:593][  info  ][  scanConsole  ]: # Configure FEs #
[17:31:43:593][  info  ][  scanConsole  ]: #################
[17:31:43:593][  info  ][  scanConsole  ]: Configuring 20UPGFC0010068
[17:31:43:625][  info  ][  scanConsole  ]: Configuring 20UPGFC0010100
[17:31:43:656][  info  ][  scanConsole  ]: Configuring 20UPGFC0010084
[17:31:43:687][  info  ][  scanConsole  ]: Sent configuration to all FEs in 94 ms!
[17:31:43:688][  info  ][  scanConsole  ]: Checking com 20UPGFC0010068
[17:31:43:688][  info  ][    SpecRx     ]: Active Rx channels: 0x10
[17:31:43:688][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[17:31:43:688][  info  ][    SpecRx     ]: Rx Status 0xb0
[17:31:43:688][  info  ][    SpecRx     ]: Number of lanes: 1
[17:31:43:688][  info  ][    SpecRx     ]: Channel 4 Lane 0 synchronized!
[17:31:43:698][  info  ][  scanConsole  ]: ... success!
[17:31:43:698][  info  ][  scanConsole  ]: Checking com 20UPGFC0010100
[17:31:43:698][  info  ][    SpecRx     ]: Active Rx channels: 0x20
[17:31:43:698][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[17:31:43:698][  info  ][    SpecRx     ]: Rx Status 0xb0
[17:31:43:698][  info  ][    SpecRx     ]: Number of lanes: 1
[17:31:43:698][  info  ][    SpecRx     ]: Channel 5 Lane 0 synchronized!
[17:31:43:709][  info  ][  scanConsole  ]: ... success!
[17:31:43:709][  info  ][  scanConsole  ]: Checking com 20UPGFC0010084
[17:31:43:709][  info  ][    SpecRx     ]: Active Rx channels: 0x80
[17:31:43:709][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[17:31:43:709][  info  ][    SpecRx     ]: Rx Status 0xb0
[17:31:43:709][  info  ][    SpecRx     ]: Number of lanes: 1
[17:31:43:709][  info  ][    SpecRx     ]: Channel 7 Lane 0 synchronized!
[17:31:43:720][  info  ][  scanConsole  ]: ... success!
[17:31:43:720][  info  ][  scanConsole  ]: Enabling Tx channels
[17:31:43:720][  info  ][  scanConsole  ]: Enabling Tx channel 1
[17:31:43:720][  info  ][  scanConsole  ]: Enabling Tx channel 1
[17:31:43:720][  info  ][  scanConsole  ]: Enabling Tx channel 1
[17:31:43:720][  info  ][  scanConsole  ]: Enabling Rx channels
[17:31:43:720][  info  ][  scanConsole  ]: Enabling Rx channel 4
[17:31:43:720][  info  ][  scanConsole  ]: Enabling Rx channel 5
[17:31:43:720][  info  ][  scanConsole  ]: Enabling Rx channel 7
[17:31:43:720][  info  ][  scanConsole  ]: ##############
[17:31:43:720][  info  ][  scanConsole  ]: # Setup Scan #
[17:31:43:720][  info  ][  scanConsole  ]: ##############
[17:31:43:720][  info  ][  scanConsole  ]: Found Scan config, constructing scan ...
[17:31:43:721][  info  ][  ScanFactory  ]: Loading Scan:
[17:31:43:721][  info  ][  ScanFactory  ]:   Name: AnalogScan
[17:31:43:721][  info  ][  ScanFactory  ]:   Number of Loops: 3
[17:31:43:721][  info  ][  ScanFactory  ]:   Loading Loop #0
[17:31:43:721][  info  ][  ScanFactory  ]:    Type: Rd53aReadRegLoop
[17:31:43:721][  info  ][  ScanFactory  ]:    Loading loop config ... 
[17:31:43:721][  info  ][  ScanFactory  ]: ~~~ {
[17:31:43:721][  info  ][  ScanFactory  ]: ~~~     "CurMux": [],
[17:31:43:721][  info  ][  ScanFactory  ]: ~~~     "EnblRingOsc": 0,
[17:31:43:721][  info  ][  ScanFactory  ]: ~~~     "Registers": [],
[17:31:43:721][  info  ][  ScanFactory  ]: ~~~     "RingOscDur": 9,
[17:31:43:721][  info  ][  ScanFactory  ]: ~~~     "RingOscRep": 10,
[17:31:43:721][  info  ][  ScanFactory  ]: ~~~     "VoltMux": [3, 5, 7, 15]
[17:31:43:721][  info  ][  ScanFactory  ]: ~~~ }
[17:31:43:721][  info  ][  ScanFactory  ]:   Loading Loop #1
[17:31:43:721][  info  ][  ScanFactory  ]:    Type: Rd53aTriggerLoop
[17:31:43:721][  info  ][  ScanFactory  ]:    Loading loop config ... 
[17:31:43:721][  info  ][  ScanFactory  ]: ~~~ {
[17:31:43:721][  info  ][  ScanFactory  ]: ~~~     "count": 1,
[17:31:43:721][  info  ][  ScanFactory  ]: ~~~     "delay": 48,
[17:31:43:721][  info  ][  ScanFactory  ]: ~~~     "extTrig": false,
[17:31:43:722][  info  ][  ScanFactory  ]: ~~~     "frequency": 30000.0,
[17:31:43:722][  info  ][  ScanFactory  ]: ~~~     "noInject": false,
[17:31:43:722][  info  ][  ScanFactory  ]: ~~~     "sendEcr": false,
[17:31:43:722][  info  ][  ScanFactory  ]: ~~~     "time": 0.0,
[17:31:43:722][  info  ][  ScanFactory  ]: ~~~     "trigMultiplier": 16
[17:31:43:722][  info  ][  ScanFactory  ]: ~~~ }
[17:31:43:722][  info  ][  ScanFactory  ]:   Loading Loop #2
[17:31:43:722][  info  ][  ScanFactory  ]:    Type: StdDataLoop
[17:31:43:722][warning ][  ScanFactory  ]: ~~~ Config empty.
[17:31:43:722][  info  ][ScanBuildHistogrammers]: Loading histogrammer ...
[17:31:43:722][  info  ][ScanBuildHistogrammers]: ... done!
[17:31:43:722][  info  ][ScanBuildAnalyses]: Loading analyses ...
[17:31:43:722][  info  ][  scanConsole  ]: Running pre scan!
[17:31:43:723][  info  ][  ScanFactory  ]: Entering pre scan phase ...
[17:31:43:723][  info  ][     Rd53a     ]: Write named register: GlobalPulseRt -> 16384
[17:31:43:723][  info  ][     Rd53a     ]: Write named register: InjEnDig -> 0
[17:31:43:723][  info  ][     Rd53a     ]: Write named register: InjVcalDiff -> 0
[17:31:43:723][  info  ][     Rd53a     ]: Write named register: InjVcalHigh -> 3500
[17:31:43:723][  info  ][     Rd53a     ]: Write named register: InjVcalMed -> 500
[17:31:43:723][  info  ][     Rd53a     ]: Write named register: LatencyConfig -> 48
[17:31:43:723][  info  ][  scanConsole  ]: Starting histogrammer and analysis threads:
[17:31:43:723][  info  ][  scanConsole  ]:  .. started threads of Fe 4
[17:31:43:723][  info  ][  scanConsole  ]:  .. started threads of Fe 5
[17:31:43:723][  info  ][  scanConsole  ]:  .. started threads of Fe 7
[17:31:43:724][  info  ][Rd53aDataProcessor]:   -> Processor thread #0 started!
[17:31:43:724][  info  ][Rd53aDataProcessor]:   -> Processor thread #1 started!
[17:31:43:724][  info  ][Rd53aDataProcessor]:   -> Processor thread #2 started!
[17:31:43:724][  info  ][Rd53aDataProcessor]:   -> Processor thread #3 started!
[17:31:43:724][  info  ][  scanConsole  ]: ########
[17:31:43:724][  info  ][  scanConsole  ]: # Scan #
[17:31:43:724][  info  ][  scanConsole  ]: ########
[17:31:43:724][  info  ][  scanConsole  ]: Starting scan!
[17:31:43:724][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0010068 on Rx 4
[17:31:43:728][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0010068] MON MUX_V: 3 Bias 0 1283 -> 0.268979 V Bias 1 1673 -> 0.346918 V, Temperature 0.0779392 -> -5.93848 C
[17:31:43:732][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0010068] MON MUX_V: 5 Bias 0 1256 -> 0.263583 V Bias 1 1639 -> 0.340124 V, Temperature 0.0765403 -> -11.5599 C
[17:31:43:737][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0010068] MON MUX_V: 7 Bias 0 1253 -> 0.262984 V Bias 1 1645 -> 0.341323 V, Temperature 0.0783389 -> -12.8616 C
[17:31:43:741][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0010068] MON MUX_V: 15 Bias 0 1247 -> 0.261785 V Bias 1 1648 -> 0.341922 V, Temperature 0.0801375 -> -13.72 C
[17:31:43:776][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0010100 on Rx 5
[17:31:43:780][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0010100] MON MUX_V: 3 Bias 0 1273 -> 0.276145 V Bias 1 1668 -> 0.354538 V, Temperature 0.0783933 -> -4.17418 C
[17:31:43:784][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0010100] MON MUX_V: 5 Bias 0 1231 -> 0.267809 V Bias 1 1622 -> 0.345408 V, Temperature 0.0775993 -> -13.1501 C
[17:31:43:789][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0010100] MON MUX_V: 7 Bias 0 1216 -> 0.264832 V Bias 1 1619 -> 0.344813 V, Temperature 0.079981 -> -13.1499 C
[17:31:43:793][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0010100] MON MUX_V: 15 Bias 0 1215 -> 0.264634 V Bias 1 1610 -> 0.343027 V, Temperature 0.0783933 -> -12.4 C
[17:31:43:828][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0010084 on Rx 7
[17:31:43:832][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0010084] MON MUX_V: 3 Bias 0 1290 -> 0.278117 V Bias 1 1675 -> 0.354939 V, Temperature 0.0768215 -> -3.77499 C
[17:31:43:836][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0010084] MON MUX_V: 5 Bias 0 1258 -> 0.271732 V Bias 1 1640 -> 0.347955 V, Temperature 0.0762229 -> -12.28 C
[17:31:43:840][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0010084] MON MUX_V: 7 Bias 0 1258 -> 0.271732 V Bias 1 1637 -> 0.347356 V, Temperature 0.0756243 -> -18.0087 C
[17:31:43:844][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0010084] MON MUX_V: 15 Bias 0 1238 -> 0.267741 V Bias 1 1629 -> 0.34576 V, Temperature 0.0780188 -> -13.5537 C
[17:31:43:880][  info  ][  scanConsole  ]: Scan done!
[17:31:43:880][  info  ][  scanConsole  ]: Waiting for processors to finish ...
[17:31:43:881][  info  ][  scanConsole  ]: Processor done, waiting for histogrammer ...
[17:31:44:281][  info  ][HistogramAlgorithm]: Histogrammer done!
[17:31:44:281][  info  ][HistogramAlgorithm]: Histogrammer done!
[17:31:44:281][  info  ][HistogramAlgorithm]: Histogrammer done!
[17:31:44:281][  info  ][  scanConsole  ]: Processor done, waiting for analysis ...
[17:31:44:682][  info  ][AnalysisAlgorithm]: Analysis done!
[17:31:44:682][  info  ][AnalysisAlgorithm]: Analysis done!
[17:31:44:682][  info  ][AnalysisAlgorithm]: Analysis done!
[17:31:44:682][  info  ][  scanConsole  ]: All done!
[17:31:44:682][  info  ][  scanConsole  ]: ##########
[17:31:44:682][  info  ][  scanConsole  ]: # Timing #
[17:31:44:682][  info  ][  scanConsole  ]: ##########
[17:31:44:682][  info  ][  scanConsole  ]: -> Configuration: 94 ms
[17:31:44:682][  info  ][  scanConsole  ]: -> Scan:          156 ms
[17:31:44:682][  info  ][  scanConsole  ]: -> Processing:    0 ms
[17:31:44:682][  info  ][  scanConsole  ]: -> Analysis:      800 ms
[17:31:44:682][  info  ][  scanConsole  ]: ###########
[17:31:44:682][  info  ][  scanConsole  ]: # Cleanup #
[17:31:44:682][  info  ][  scanConsole  ]: ###########
[17:31:44:691][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0010068 to configs/CERNQ5/20UPGFC0010068.json
[17:31:44:895][  info  ][  scanConsole  ]: -> Storing output results of FE 4
[17:31:44:895][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0010068, this usually means that the chip did not send any data at all.
[17:31:44:896][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0010100 to configs/CERNQ5/20UPGFC0010100.json
[17:31:45:059][  info  ][  scanConsole  ]: -> Storing output results of FE 5
[17:31:45:059][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0010100, this usually means that the chip did not send any data at all.
[17:31:45:060][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0010084 to configs/CERNQ5/20UPGFC0010084.json
[17:31:45:224][  info  ][  scanConsole  ]: -> Storing output results of FE 7
[17:31:45:224][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0010084, this usually means that the chip did not send any data at all.
[17:31:45:225][  info  ][  scanConsole  ]: Finishing run: 1496
[root@jollyroger Yarr]# 
