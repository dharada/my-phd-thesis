[16:02:22:839][  info  ][               ]: #####################################
[16:02:22:839][  info  ][               ]: # Welcome to the YARR Scan Console! #
[16:02:22:839][  info  ][               ]: #####################################
[16:02:22:839][  info  ][               ]: -> Parsing command line parameters ...
[16:02:22:839][  info  ][               ]: Configuring logger ...
[16:02:22:841][  info  ][  scanConsole  ]: Scan Type/Config configs/scans/rd53a/reg_readtemp.json
[16:02:22:841][  info  ][  scanConsole  ]: Connectivity:
[16:02:22:841][  info  ][  scanConsole  ]:     configs/CERNQ5/example_rd53a_setup.json
[16:02:22:841][  info  ][  scanConsole  ]: Target ToT: -1
[16:02:22:841][  info  ][  scanConsole  ]: Target Charge: -1
[16:02:22:841][  info  ][  scanConsole  ]: Output Plots: false
[16:02:22:841][  info  ][  scanConsole  ]: Output Directory: ./data/001472_reg_readtemp/
[16:02:22:846][  info  ][  scanConsole  ]: Timestamp: 2022-08-19_16:02:22
[16:02:22:846][  info  ][  scanConsole  ]: Run Number: 1472
[16:02:22:846][  info  ][  scanConsole  ]: #################
[16:02:22:846][  info  ][  scanConsole  ]: # Init Hardware #
[16:02:22:846][  info  ][  scanConsole  ]: #################
[16:02:22:846][  info  ][  scanConsole  ]: -> Opening controller config: configs/controller/specCfg-rd53a-16x1.json
[16:02:22:846][  info  ][  ScanHelper   ]: Loading controller ...
[16:02:22:846][  info  ][  ScanHelper   ]: Found controller of type: spec
[16:02:22:846][  info  ][  ScanHelper   ]: ... loading controler config:
[16:02:22:846][  info  ][  ScanHelper   ]: ~~~ {
[16:02:22:846][  info  ][  ScanHelper   ]: ~~~     "cmdPeriod": 6.250000073038109e-9,
[16:02:22:846][  info  ][  ScanHelper   ]: ~~~     "idle": {
[16:02:22:846][  info  ][  ScanHelper   ]: ~~~         "word": 1768515945
[16:02:22:846][  info  ][  ScanHelper   ]: ~~~     },
[16:02:22:846][  info  ][  ScanHelper   ]: ~~~     "pulse": {
[16:02:22:846][  info  ][  ScanHelper   ]: ~~~         "interval": 500,
[16:02:22:846][  info  ][  ScanHelper   ]: ~~~         "word": 1549575846
[16:02:22:846][  info  ][  ScanHelper   ]: ~~~     },
[16:02:22:846][  info  ][  ScanHelper   ]: ~~~     "rxActiveLanes": 1,
[16:02:22:846][  info  ][  ScanHelper   ]: ~~~     "rxPolarity": 0,
[16:02:22:846][  info  ][  ScanHelper   ]: ~~~     "specNum": 0,
[16:02:22:846][  info  ][  ScanHelper   ]: ~~~     "spiConfig": 541200,
[16:02:22:846][  info  ][  ScanHelper   ]: ~~~     "sync": {
[16:02:22:846][  info  ][  ScanHelper   ]: ~~~         "interval": 32,
[16:02:22:846][  info  ][  ScanHelper   ]: ~~~         "word": 2172551550
[16:02:22:846][  info  ][  ScanHelper   ]: ~~~     },
[16:02:22:846][  info  ][  ScanHelper   ]: ~~~     "txPolarity": 0
[16:02:22:846][  info  ][  ScanHelper   ]: ~~~ }
[16:02:22:846][  info  ][    SpecCom    ]: Opening SPEC with id #0
[16:02:22:847][  info  ][    SpecCom    ]: Mapping BARs ...
[16:02:22:847][  info  ][    SpecCom    ]: ... Mapped BAR0 at 0x7ff0b8e34000 with size 1048576
[16:02:22:847][warning ][    SpecCom    ]: ... BAR4 not mapped (Mmap failed)
[16:02:22:847][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[16:02:22:847][  info  ][    SpecCom    ]: Firmware Version: 0x4d9ff6d
[16:02:22:847][  info  ][    SpecCom    ]: Firmware Identifier: 0x4030232
[16:02:22:847][  info  ][    SpecCom    ]: FPGA card: PLDA XpressK7 325
[16:02:22:847][  info  ][    SpecCom    ]: FE Chip Type: RD53A/B
[16:02:22:847][  info  ][    SpecCom    ]: FMC Card Type: Ohio Card (Display Port)
[16:02:22:847][  info  ][    SpecCom    ]: RX Speed: 640Mbps
[16:02:22:847][  info  ][    SpecCom    ]: Channel Configuration: 16x1
[16:02:22:847][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[16:02:22:847][  info  ][    SpecCom    ]: Flushing buffers ...
[16:02:22:847][  info  ][    SpecCom    ]: Init success!
[16:02:22:847][  info  ][  scanConsole  ]: #######################
[16:02:22:847][  info  ][  scanConsole  ]: ##  Loading Configs  ##
[16:02:22:847][  info  ][  scanConsole  ]: #######################
[16:02:22:847][  info  ][  scanConsole  ]: Opening global config: configs/CERNQ5/example_rd53a_setup.json
[16:02:22:847][  info  ][  ScanHelper   ]: Chip type: RD53A
[16:02:22:847][  info  ][  ScanHelper   ]: Chip count 4
[16:02:22:847][  info  ][  ScanHelper   ]: Loading chip #0
[16:02:22:848][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(4)
[16:02:22:848][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ5/20UPGFC0010068.json
[16:02:23:046][  info  ][  ScanHelper   ]: Loading chip #1
[16:02:23:047][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(5)
[16:02:23:047][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ5/20UPGFC0010100.json
[16:02:23:246][  info  ][  ScanHelper   ]: Loading chip #2
[16:02:23:246][warning ][  ScanHelper   ]:  ... chip not enabled, skipping!
[16:02:23:246][  info  ][  ScanHelper   ]: Loading chip #3
[16:02:23:246][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(7)
[16:02:23:246][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ5/20UPGFC0010084.json
[16:02:23:435][  info  ][  scanConsole  ]: #################
[16:02:23:435][  info  ][  scanConsole  ]: # Configure FEs #
[16:02:23:435][  info  ][  scanConsole  ]: #################
[16:02:23:435][  info  ][  scanConsole  ]: Configuring 20UPGFC0010068
[16:02:23:465][  info  ][  scanConsole  ]: Configuring 20UPGFC0010100
[16:02:23:496][  info  ][  scanConsole  ]: Configuring 20UPGFC0010084
[16:02:23:545][  info  ][  scanConsole  ]: Sent configuration to all FEs in 109 ms!
[16:02:23:546][  info  ][  scanConsole  ]: Checking com 20UPGFC0010068
[16:02:23:546][  info  ][    SpecRx     ]: Active Rx channels: 0x10
[16:02:23:546][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[16:02:23:546][  info  ][    SpecRx     ]: Rx Status 0xb0
[16:02:23:546][  info  ][    SpecRx     ]: Number of lanes: 1
[16:02:23:546][  info  ][    SpecRx     ]: Channel 4 Lane 0 synchronized!
[16:02:23:557][  info  ][  scanConsole  ]: ... success!
[16:02:23:557][  info  ][  scanConsole  ]: Checking com 20UPGFC0010100
[16:02:23:557][  info  ][    SpecRx     ]: Active Rx channels: 0x20
[16:02:23:557][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[16:02:23:557][  info  ][    SpecRx     ]: Rx Status 0xb0
[16:02:23:557][  info  ][    SpecRx     ]: Number of lanes: 1
[16:02:23:557][  info  ][    SpecRx     ]: Channel 5 Lane 0 synchronized!
[16:02:23:567][  info  ][  scanConsole  ]: ... success!
[16:02:23:567][  info  ][  scanConsole  ]: Checking com 20UPGFC0010084
[16:02:23:567][  info  ][    SpecRx     ]: Active Rx channels: 0x80
[16:02:23:567][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[16:02:23:567][  info  ][    SpecRx     ]: Rx Status 0xb0
[16:02:23:567][  info  ][    SpecRx     ]: Number of lanes: 1
[16:02:23:567][  info  ][    SpecRx     ]: Channel 7 Lane 0 synchronized!
[16:02:23:578][  info  ][  scanConsole  ]: ... success!
[16:02:23:578][  info  ][  scanConsole  ]: Enabling Tx channels
[16:02:23:578][  info  ][  scanConsole  ]: Enabling Tx channel 1
[16:02:23:578][  info  ][  scanConsole  ]: Enabling Tx channel 1
[16:02:23:578][  info  ][  scanConsole  ]: Enabling Tx channel 1
[16:02:23:578][  info  ][  scanConsole  ]: Enabling Rx channels
[16:02:23:578][  info  ][  scanConsole  ]: Enabling Rx channel 4
[16:02:23:578][  info  ][  scanConsole  ]: Enabling Rx channel 5
[16:02:23:578][  info  ][  scanConsole  ]: Enabling Rx channel 7
[16:02:23:578][  info  ][  scanConsole  ]: ##############
[16:02:23:578][  info  ][  scanConsole  ]: # Setup Scan #
[16:02:23:578][  info  ][  scanConsole  ]: ##############
[16:02:23:578][  info  ][  scanConsole  ]: Found Scan config, constructing scan ...
[16:02:23:579][  info  ][  ScanFactory  ]: Loading Scan:
[16:02:23:579][  info  ][  ScanFactory  ]:   Name: AnalogScan
[16:02:23:579][  info  ][  ScanFactory  ]:   Number of Loops: 3
[16:02:23:579][  info  ][  ScanFactory  ]:   Loading Loop #0
[16:02:23:579][  info  ][  ScanFactory  ]:    Type: Rd53aReadRegLoop
[16:02:23:579][  info  ][  ScanFactory  ]:    Loading loop config ... 
[16:02:23:579][  info  ][  ScanFactory  ]: ~~~ {
[16:02:23:579][  info  ][  ScanFactory  ]: ~~~     "CurMux": [],
[16:02:23:579][  info  ][  ScanFactory  ]: ~~~     "EnblRingOsc": 0,
[16:02:23:579][  info  ][  ScanFactory  ]: ~~~     "Registers": [],
[16:02:23:579][  info  ][  ScanFactory  ]: ~~~     "RingOscDur": 9,
[16:02:23:579][  info  ][  ScanFactory  ]: ~~~     "RingOscRep": 10,
[16:02:23:579][  info  ][  ScanFactory  ]: ~~~     "VoltMux": [3, 5, 7, 15]
[16:02:23:579][  info  ][  ScanFactory  ]: ~~~ }
[16:02:23:579][  info  ][  ScanFactory  ]:   Loading Loop #1
[16:02:23:579][  info  ][  ScanFactory  ]:    Type: Rd53aTriggerLoop
[16:02:23:579][  info  ][  ScanFactory  ]:    Loading loop config ... 
[16:02:23:579][  info  ][  ScanFactory  ]: ~~~ {
[16:02:23:580][  info  ][  ScanFactory  ]: ~~~     "count": 1,
[16:02:23:580][  info  ][  ScanFactory  ]: ~~~     "delay": 48,
[16:02:23:580][  info  ][  ScanFactory  ]: ~~~     "extTrig": false,
[16:02:23:580][  info  ][  ScanFactory  ]: ~~~     "frequency": 30000.0,
[16:02:23:580][  info  ][  ScanFactory  ]: ~~~     "noInject": false,
[16:02:23:580][  info  ][  ScanFactory  ]: ~~~     "sendEcr": false,
[16:02:23:580][  info  ][  ScanFactory  ]: ~~~     "time": 0.0,
[16:02:23:580][  info  ][  ScanFactory  ]: ~~~     "trigMultiplier": 16
[16:02:23:580][  info  ][  ScanFactory  ]: ~~~ }
[16:02:23:580][  info  ][  ScanFactory  ]:   Loading Loop #2
[16:02:23:580][  info  ][  ScanFactory  ]:    Type: StdDataLoop
[16:02:23:580][warning ][  ScanFactory  ]: ~~~ Config empty.
[16:02:23:580][  info  ][ScanBuildHistogrammers]: Loading histogrammer ...
[16:02:23:580][  info  ][ScanBuildHistogrammers]: ... done!
[16:02:23:580][  info  ][ScanBuildAnalyses]: Loading analyses ...
[16:02:23:580][  info  ][  scanConsole  ]: Running pre scan!
[16:02:23:580][  info  ][  ScanFactory  ]: Entering pre scan phase ...
[16:02:23:581][  info  ][     Rd53a     ]: Write named register: GlobalPulseRt -> 16384
[16:02:23:581][  info  ][     Rd53a     ]: Write named register: InjEnDig -> 0
[16:02:23:581][  info  ][     Rd53a     ]: Write named register: InjVcalDiff -> 0
[16:02:23:581][  info  ][     Rd53a     ]: Write named register: InjVcalHigh -> 3500
[16:02:23:581][  info  ][     Rd53a     ]: Write named register: InjVcalMed -> 500
[16:02:23:581][  info  ][     Rd53a     ]: Write named register: LatencyConfig -> 48
[16:02:23:581][  info  ][  scanConsole  ]: Starting histogrammer and analysis threads:
[16:02:23:581][  info  ][  scanConsole  ]:  .. started threads of Fe 4
[16:02:23:581][  info  ][  scanConsole  ]:  .. started threads of Fe 5
[16:02:23:581][  info  ][  scanConsole  ]:  .. started threads of Fe 7
[16:02:23:582][  info  ][Rd53aDataProcessor]:   -> Processor thread #0 started!
[16:02:23:582][  info  ][Rd53aDataProcessor]:   -> Processor thread #1 started!
[16:02:23:582][  info  ][Rd53aDataProcessor]:   -> Processor thread #2 started!
[16:02:23:582][  info  ][Rd53aDataProcessor]:   -> Processor thread #3 started!
[16:02:23:582][  info  ][  scanConsole  ]: ########
[16:02:23:582][  info  ][  scanConsole  ]: # Scan #
[16:02:23:582][  info  ][  scanConsole  ]: ########
[16:02:23:582][  info  ][  scanConsole  ]: Starting scan!
[16:02:23:582][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0010068 on Rx 4
[16:02:23:586][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0010068] MON MUX_V: 3 Bias 0 1197 -> 0.251793 V Bias 1 1606 -> 0.333529 V, Temperature 0.0817362 -> 7.21545 C
[16:02:23:591][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0010068] MON MUX_V: 5 Bias 0 1168 -> 0.245997 V Bias 1 1571 -> 0.326534 V, Temperature 0.0805371 -> 2.83997 C
[16:02:23:595][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0010068] MON MUX_V: 7 Bias 0 1163 -> 0.244998 V Bias 1 1577 -> 0.327733 V, Temperature 0.0827354 -> 2.36917 C
[16:02:23:599][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0010068] MON MUX_V: 15 Bias 0 1155 -> 0.243399 V Bias 1 1579 -> 0.328133 V, Temperature 0.0847339 -> 2.83994 C
[16:02:23:634][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0010100 on Rx 5
[16:02:23:638][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0010100] MON MUX_V: 3 Bias 0 1185 -> 0.25868 V Bias 1 1601 -> 0.341241 V, Temperature 0.082561 -> 8.01935 C
[16:02:23:642][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0010100] MON MUX_V: 5 Bias 0 1144 -> 0.250543 V Bias 1 1555 -> 0.332111 V, Temperature 0.0815687 -> 1.84998 C
[16:02:23:646][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0010100] MON MUX_V: 7 Bias 0 1130 -> 0.247764 V Bias 1 1552 -> 0.331516 V, Temperature 0.0837518 -> 1.10001 C
[16:02:23:650][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0010100] MON MUX_V: 15 Bias 0 1131 -> 0.247963 V Bias 1 1544 -> 0.329928 V, Temperature 0.0819656 -> 1.09991 C
[16:02:23:685][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0010084 on Rx 7
[16:02:23:689][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0010084] MON MUX_V: 3 Bias 0 1208 -> 0.261755 V Bias 1 1612 -> 0.342368 V, Temperature 0.0806127 -> 6.91248 C
[16:02:23:694][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0010084] MON MUX_V: 5 Bias 0 1174 -> 0.254971 V Bias 1 1576 -> 0.335185 V, Temperature 0.0802137 -> 2.12003 C
[16:02:23:698][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0010084] MON MUX_V: 7 Bias 0 1173 -> 0.254771 V Bias 1 1573 -> 0.334586 V, Temperature 0.0798146 -> -1.57382 C
[16:02:23:702][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0010084] MON MUX_V: 15 Bias 0 1154 -> 0.25098 V Bias 1 1565 -> 0.33299 V, Temperature 0.0820095 -> 0.292328 C
[16:02:23:738][  info  ][  scanConsole  ]: Scan done!
[16:02:23:738][  info  ][  scanConsole  ]: Waiting for processors to finish ...
[16:02:23:739][  info  ][  scanConsole  ]: Processor done, waiting for histogrammer ...
[16:02:24:138][  info  ][HistogramAlgorithm]: Histogrammer done!
[16:02:24:138][  info  ][HistogramAlgorithm]: Histogrammer done!
[16:02:24:138][  info  ][HistogramAlgorithm]: Histogrammer done!
[16:02:24:139][  info  ][  scanConsole  ]: Processor done, waiting for analysis ...
[16:02:24:539][  info  ][AnalysisAlgorithm]: Analysis done!
[16:02:24:539][  info  ][AnalysisAlgorithm]: Analysis done!
[16:02:24:539][  info  ][AnalysisAlgorithm]: Analysis done!
[16:02:24:539][  info  ][  scanConsole  ]: All done!
[16:02:24:539][  info  ][  scanConsole  ]: ##########
[16:02:24:539][  info  ][  scanConsole  ]: # Timing #
[16:02:24:539][  info  ][  scanConsole  ]: ##########
[16:02:24:539][  info  ][  scanConsole  ]: -> Configuration: 109 ms
[16:02:24:539][  info  ][  scanConsole  ]: -> Scan:          155 ms
[16:02:24:539][  info  ][  scanConsole  ]: -> Processing:    0 ms
[16:02:24:539][  info  ][  scanConsole  ]: -> Analysis:      800 ms
[16:02:24:539][  info  ][  scanConsole  ]: ###########
[16:02:24:539][  info  ][  scanConsole  ]: # Cleanup #
[16:02:24:539][  info  ][  scanConsole  ]: ###########
[16:02:24:547][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0010068 to configs/CERNQ5/20UPGFC0010068.json
[16:02:24:748][  info  ][  scanConsole  ]: -> Storing output results of FE 4
[16:02:24:748][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0010068, this usually means that the chip did not send any data at all.
[16:02:24:749][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0010100 to configs/CERNQ5/20UPGFC0010100.json
[16:02:24:916][  info  ][  scanConsole  ]: -> Storing output results of FE 5
[16:02:24:916][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0010100, this usually means that the chip did not send any data at all.
[16:02:24:917][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0010084 to configs/CERNQ5/20UPGFC0010084.json
[16:02:25:083][  info  ][  scanConsole  ]: -> Storing output results of FE 7
[16:02:25:083][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0010084, this usually means that the chip did not send any data at all.
[16:02:25:084][  info  ][  scanConsole  ]: Finishing run: 1472
[root@jollyroger Yarr]# 
