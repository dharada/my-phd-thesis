[15:58:46:275][  info  ][               ]: #####################################
[15:58:46:275][  info  ][               ]: # Welcome to the YARR Scan Console! #
[15:58:46:275][  info  ][               ]: #####################################
[15:58:46:275][  info  ][               ]: -> Parsing command line parameters ...
[15:58:46:275][  info  ][               ]: Configuring logger ...
[15:58:46:279][  info  ][  scanConsole  ]: Scan Type/Config configs/scans/rd53a/reg_readtemp.json
[15:58:46:279][  info  ][  scanConsole  ]: Connectivity:
[15:58:46:279][  info  ][  scanConsole  ]:     configs/CERNQ5/example_rd53a_setup.json
[15:58:46:279][  info  ][  scanConsole  ]: Target ToT: -1
[15:58:46:279][  info  ][  scanConsole  ]: Target Charge: -1
[15:58:46:279][  info  ][  scanConsole  ]: Output Plots: false
[15:58:46:279][  info  ][  scanConsole  ]: Output Directory: ./data/001471_reg_readtemp/
[15:58:46:285][  info  ][  scanConsole  ]: Timestamp: 2022-08-19_15:58:46
[15:58:46:286][  info  ][  scanConsole  ]: Run Number: 1471
[15:58:46:286][  info  ][  scanConsole  ]: #################
[15:58:46:286][  info  ][  scanConsole  ]: # Init Hardware #
[15:58:46:286][  info  ][  scanConsole  ]: #################
[15:58:46:286][  info  ][  scanConsole  ]: -> Opening controller config: configs/controller/specCfg-rd53a-16x1.json
[15:58:46:286][  info  ][  ScanHelper   ]: Loading controller ...
[15:58:46:286][  info  ][  ScanHelper   ]: Found controller of type: spec
[15:58:46:286][  info  ][  ScanHelper   ]: ... loading controler config:
[15:58:46:286][  info  ][  ScanHelper   ]: ~~~ {
[15:58:46:286][  info  ][  ScanHelper   ]: ~~~     "cmdPeriod": 6.250000073038109e-9,
[15:58:46:286][  info  ][  ScanHelper   ]: ~~~     "idle": {
[15:58:46:286][  info  ][  ScanHelper   ]: ~~~         "word": 1768515945
[15:58:46:286][  info  ][  ScanHelper   ]: ~~~     },
[15:58:46:286][  info  ][  ScanHelper   ]: ~~~     "pulse": {
[15:58:46:286][  info  ][  ScanHelper   ]: ~~~         "interval": 500,
[15:58:46:286][  info  ][  ScanHelper   ]: ~~~         "word": 1549575846
[15:58:46:286][  info  ][  ScanHelper   ]: ~~~     },
[15:58:46:286][  info  ][  ScanHelper   ]: ~~~     "rxActiveLanes": 1,
[15:58:46:286][  info  ][  ScanHelper   ]: ~~~     "rxPolarity": 0,
[15:58:46:286][  info  ][  ScanHelper   ]: ~~~     "specNum": 0,
[15:58:46:286][  info  ][  ScanHelper   ]: ~~~     "spiConfig": 541200,
[15:58:46:286][  info  ][  ScanHelper   ]: ~~~     "sync": {
[15:58:46:286][  info  ][  ScanHelper   ]: ~~~         "interval": 32,
[15:58:46:286][  info  ][  ScanHelper   ]: ~~~         "word": 2172551550
[15:58:46:286][  info  ][  ScanHelper   ]: ~~~     },
[15:58:46:286][  info  ][  ScanHelper   ]: ~~~     "txPolarity": 0
[15:58:46:286][  info  ][  ScanHelper   ]: ~~~ }
[15:58:46:286][  info  ][    SpecCom    ]: Opening SPEC with id #0
[15:58:46:286][  info  ][    SpecCom    ]: Mapping BARs ...
[15:58:46:286][  info  ][    SpecCom    ]: ... Mapped BAR0 at 0x7febdbdb6000 with size 1048576
[15:58:46:286][warning ][    SpecCom    ]: ... BAR4 not mapped (Mmap failed)
[15:58:46:286][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[15:58:46:286][  info  ][    SpecCom    ]: Firmware Version: 0x4d9ff6d
[15:58:46:286][  info  ][    SpecCom    ]: Firmware Identifier: 0x4030232
[15:58:46:286][  info  ][    SpecCom    ]: FPGA card: PLDA XpressK7 325
[15:58:46:286][  info  ][    SpecCom    ]: FE Chip Type: RD53A/B
[15:58:46:286][  info  ][    SpecCom    ]: FMC Card Type: Ohio Card (Display Port)
[15:58:46:286][  info  ][    SpecCom    ]: RX Speed: 640Mbps
[15:58:46:286][  info  ][    SpecCom    ]: Channel Configuration: 16x1
[15:58:46:286][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[15:58:46:286][  info  ][    SpecCom    ]: Flushing buffers ...
[15:58:46:286][  info  ][    SpecCom    ]: Init success!
[15:58:46:286][  info  ][  scanConsole  ]: #######################
[15:58:46:286][  info  ][  scanConsole  ]: ##  Loading Configs  ##
[15:58:46:286][  info  ][  scanConsole  ]: #######################
[15:58:46:286][  info  ][  scanConsole  ]: Opening global config: configs/CERNQ5/example_rd53a_setup.json
[15:58:46:286][  info  ][  ScanHelper   ]: Chip type: RD53A
[15:58:46:286][  info  ][  ScanHelper   ]: Chip count 4
[15:58:46:286][  info  ][  ScanHelper   ]: Loading chip #0
[15:58:46:288][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(4)
[15:58:46:288][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ5/20UPGFC0010068.json
[15:58:46:475][  info  ][  ScanHelper   ]: Loading chip #1
[15:58:46:476][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(5)
[15:58:46:476][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ5/20UPGFC0010100.json
[15:58:46:649][  info  ][  ScanHelper   ]: Loading chip #2
[15:58:46:649][warning ][  ScanHelper   ]:  ... chip not enabled, skipping!
[15:58:46:649][  info  ][  ScanHelper   ]: Loading chip #3
[15:58:46:650][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(7)
[15:58:46:650][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ5/20UPGFC0010084.json
[15:58:46:828][  info  ][  scanConsole  ]: #################
[15:58:46:828][  info  ][  scanConsole  ]: # Configure FEs #
[15:58:46:828][  info  ][  scanConsole  ]: #################
[15:58:46:828][  info  ][  scanConsole  ]: Configuring 20UPGFC0010068
[15:58:46:859][  info  ][  scanConsole  ]: Configuring 20UPGFC0010100
[15:58:46:890][  info  ][  scanConsole  ]: Configuring 20UPGFC0010084
[15:58:46:920][  info  ][  scanConsole  ]: Sent configuration to all FEs in 91 ms!
[15:58:46:921][  info  ][  scanConsole  ]: Checking com 20UPGFC0010068
[15:58:46:921][  info  ][    SpecRx     ]: Active Rx channels: 0x10
[15:58:46:921][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[15:58:46:921][  info  ][    SpecRx     ]: Rx Status 0xb0
[15:58:46:921][  info  ][    SpecRx     ]: Number of lanes: 1
[15:58:46:921][  info  ][    SpecRx     ]: Channel 4 Lane 0 synchronized!
[15:58:46:931][  info  ][  scanConsole  ]: ... success!
[15:58:46:931][  info  ][  scanConsole  ]: Checking com 20UPGFC0010100
[15:58:46:931][  info  ][    SpecRx     ]: Active Rx channels: 0x20
[15:58:46:931][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[15:58:46:931][  info  ][    SpecRx     ]: Rx Status 0xb0
[15:58:46:931][  info  ][    SpecRx     ]: Number of lanes: 1
[15:58:46:931][  info  ][    SpecRx     ]: Channel 5 Lane 0 synchronized!
[15:58:46:942][  info  ][  scanConsole  ]: ... success!
[15:58:46:942][  info  ][  scanConsole  ]: Checking com 20UPGFC0010084
[15:58:46:942][  info  ][    SpecRx     ]: Active Rx channels: 0x80
[15:58:46:942][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[15:58:46:942][  info  ][    SpecRx     ]: Rx Status 0xb0
[15:58:46:942][  info  ][    SpecRx     ]: Number of lanes: 1
[15:58:46:942][  info  ][    SpecRx     ]: Channel 7 Lane 0 synchronized!
[15:58:46:952][  info  ][  scanConsole  ]: ... success!
[15:58:46:952][  info  ][  scanConsole  ]: Enabling Tx channels
[15:58:46:952][  info  ][  scanConsole  ]: Enabling Tx channel 1
[15:58:46:952][  info  ][  scanConsole  ]: Enabling Tx channel 1
[15:58:46:952][  info  ][  scanConsole  ]: Enabling Tx channel 1
[15:58:46:952][  info  ][  scanConsole  ]: Enabling Rx channels
[15:58:46:952][  info  ][  scanConsole  ]: Enabling Rx channel 4
[15:58:46:952][  info  ][  scanConsole  ]: Enabling Rx channel 5
[15:58:46:953][  info  ][  scanConsole  ]: Enabling Rx channel 7
[15:58:46:953][  info  ][  scanConsole  ]: ##############
[15:58:46:953][  info  ][  scanConsole  ]: # Setup Scan #
[15:58:46:953][  info  ][  scanConsole  ]: ##############
[15:58:46:953][  info  ][  scanConsole  ]: Found Scan config, constructing scan ...
[15:58:46:953][  info  ][  ScanFactory  ]: Loading Scan:
[15:58:46:953][  info  ][  ScanFactory  ]:   Name: AnalogScan
[15:58:46:953][  info  ][  ScanFactory  ]:   Number of Loops: 3
[15:58:46:953][  info  ][  ScanFactory  ]:   Loading Loop #0
[15:58:46:953][  info  ][  ScanFactory  ]:    Type: Rd53aReadRegLoop
[15:58:46:953][  info  ][  ScanFactory  ]:    Loading loop config ... 
[15:58:46:954][  info  ][  ScanFactory  ]: ~~~ {
[15:58:46:954][  info  ][  ScanFactory  ]: ~~~     "CurMux": [],
[15:58:46:954][  info  ][  ScanFactory  ]: ~~~     "EnblRingOsc": 0,
[15:58:46:954][  info  ][  ScanFactory  ]: ~~~     "Registers": [],
[15:58:46:954][  info  ][  ScanFactory  ]: ~~~     "RingOscDur": 9,
[15:58:46:954][  info  ][  ScanFactory  ]: ~~~     "RingOscRep": 10,
[15:58:46:954][  info  ][  ScanFactory  ]: ~~~     "VoltMux": [3, 5, 7, 15]
[15:58:46:954][  info  ][  ScanFactory  ]: ~~~ }
[15:58:46:954][  info  ][  ScanFactory  ]:   Loading Loop #1
[15:58:46:954][  info  ][  ScanFactory  ]:    Type: Rd53aTriggerLoop
[15:58:46:954][  info  ][  ScanFactory  ]:    Loading loop config ... 
[15:58:46:954][  info  ][  ScanFactory  ]: ~~~ {
[15:58:46:954][  info  ][  ScanFactory  ]: ~~~     "count": 1,
[15:58:46:954][  info  ][  ScanFactory  ]: ~~~     "delay": 48,
[15:58:46:954][  info  ][  ScanFactory  ]: ~~~     "extTrig": false,
[15:58:46:954][  info  ][  ScanFactory  ]: ~~~     "frequency": 30000.0,
[15:58:46:954][  info  ][  ScanFactory  ]: ~~~     "noInject": false,
[15:58:46:954][  info  ][  ScanFactory  ]: ~~~     "sendEcr": false,
[15:58:46:954][  info  ][  ScanFactory  ]: ~~~     "time": 0.0,
[15:58:46:954][  info  ][  ScanFactory  ]: ~~~     "trigMultiplier": 16
[15:58:46:954][  info  ][  ScanFactory  ]: ~~~ }
[15:58:46:954][  info  ][  ScanFactory  ]:   Loading Loop #2
[15:58:46:954][  info  ][  ScanFactory  ]:    Type: StdDataLoop
[15:58:46:954][warning ][  ScanFactory  ]: ~~~ Config empty.
[15:58:46:954][  info  ][ScanBuildHistogrammers]: Loading histogrammer ...
[15:58:46:955][  info  ][ScanBuildHistogrammers]: ... done!
[15:58:46:955][  info  ][ScanBuildAnalyses]: Loading analyses ...
[15:58:46:955][  info  ][  scanConsole  ]: Running pre scan!
[15:58:46:955][  info  ][  ScanFactory  ]: Entering pre scan phase ...
[15:58:46:955][  info  ][     Rd53a     ]: Write named register: GlobalPulseRt -> 16384
[15:58:46:955][  info  ][     Rd53a     ]: Write named register: InjEnDig -> 0
[15:58:46:955][  info  ][     Rd53a     ]: Write named register: InjVcalDiff -> 0
[15:58:46:955][  info  ][     Rd53a     ]: Write named register: InjVcalHigh -> 3500
[15:58:46:955][  info  ][     Rd53a     ]: Write named register: InjVcalMed -> 500
[15:58:46:955][  info  ][     Rd53a     ]: Write named register: LatencyConfig -> 48
[15:58:46:955][  info  ][  scanConsole  ]: Starting histogrammer and analysis threads:
[15:58:46:956][  info  ][  scanConsole  ]:  .. started threads of Fe 4
[15:58:46:956][  info  ][  scanConsole  ]:  .. started threads of Fe 5
[15:58:46:956][  info  ][  scanConsole  ]:  .. started threads of Fe 7
[15:58:46:956][  info  ][Rd53aDataProcessor]:   -> Processor thread #0 started!
[15:58:46:956][  info  ][Rd53aDataProcessor]:   -> Processor thread #1 started!
[15:58:46:956][  info  ][Rd53aDataProcessor]:   -> Processor thread #2 started!
[15:58:46:956][  info  ][Rd53aDataProcessor]:   -> Processor thread #3 started!
[15:58:46:956][  info  ][  scanConsole  ]: ########
[15:58:46:956][  info  ][  scanConsole  ]: # Scan #
[15:58:46:956][  info  ][  scanConsole  ]: ########
[15:58:46:956][  info  ][  scanConsole  ]: Starting scan!
[15:58:46:956][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0010068 on Rx 4
[15:58:46:961][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0010068] MON MUX_V: 3 Bias 0 1196 -> 0.251593 V Bias 1 1606 -> 0.333529 V, Temperature 0.0819361 -> 7.90778 C
[15:58:46:965][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0010068] MON MUX_V: 5 Bias 0 1168 -> 0.245997 V Bias 1 1571 -> 0.326534 V, Temperature 0.0805371 -> 2.83997 C
[15:58:46:969][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0010068] MON MUX_V: 7 Bias 0 1164 -> 0.245198 V Bias 1 1577 -> 0.327733 V, Temperature 0.0825356 -> 1.67688 C
[15:58:46:973][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0010068] MON MUX_V: 15 Bias 0 1155 -> 0.243399 V Bias 1 1578 -> 0.327933 V, Temperature 0.084534 -> 2.12 C
[15:58:47:007][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0010100 on Rx 5
[15:58:47:012][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0010100] MON MUX_V: 3 Bias 0 1187 -> 0.259077 V Bias 1 1601 -> 0.341241 V, Temperature 0.082164 -> 6.85803 C
[15:58:47:016][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0010100] MON MUX_V: 5 Bias 0 1143 -> 0.250344 V Bias 1 1556 -> 0.33231 V, Temperature 0.0819656 -> 3.35001 C
[15:58:47:020][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0010100] MON MUX_V: 7 Bias 0 1130 -> 0.247764 V Bias 1 1553 -> 0.331714 V, Temperature 0.0839503 -> 1.85007 C
[15:58:47:024][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0010100] MON MUX_V: 15 Bias 0 1130 -> 0.247764 V Bias 1 1543 -> 0.32973 V, Temperature 0.0819656 -> 1.10001 C
[15:58:47:059][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0010084 on Rx 7
[15:58:47:063][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0010084] MON MUX_V: 3 Bias 0 1206 -> 0.261356 V Bias 1 1611 -> 0.342168 V, Temperature 0.0808123 -> 7.47504 C
[15:58:47:067][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0010084] MON MUX_V: 5 Bias 0 1174 -> 0.254971 V Bias 1 1577 -> 0.335384 V, Temperature 0.0804132 -> 2.84009 C
[15:58:47:071][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0010084] MON MUX_V: 7 Bias 0 1172 -> 0.254572 V Bias 1 1571 -> 0.334187 V, Temperature 0.0796151 -> -2.35651 C
[15:58:47:075][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0010084] MON MUX_V: 15 Bias 0 1154 -> 0.25098 V Bias 1 1567 -> 0.333389 V, Temperature 0.0824085 -> 1.67688 C
[15:58:47:111][  info  ][  scanConsole  ]: Scan done!
[15:58:47:111][  info  ][  scanConsole  ]: Waiting for processors to finish ...
[15:58:47:112][  info  ][  scanConsole  ]: Processor done, waiting for histogrammer ...
[15:58:47:511][  info  ][HistogramAlgorithm]: Histogrammer done!
[15:58:47:511][  info  ][HistogramAlgorithm]: Histogrammer done!
[15:58:47:511][  info  ][HistogramAlgorithm]: Histogrammer done!
[15:58:47:512][  info  ][  scanConsole  ]: Processor done, waiting for analysis ...
[15:58:47:912][  info  ][AnalysisAlgorithm]: Analysis done!
[15:58:47:912][  info  ][AnalysisAlgorithm]: Analysis done!
[15:58:47:912][  info  ][AnalysisAlgorithm]: Analysis done!
[15:58:47:912][  info  ][  scanConsole  ]: All done!
[15:58:47:912][  info  ][  scanConsole  ]: ##########
[15:58:47:912][  info  ][  scanConsole  ]: # Timing #
[15:58:47:912][  info  ][  scanConsole  ]: ##########
[15:58:47:912][  info  ][  scanConsole  ]: -> Configuration: 91 ms
[15:58:47:912][  info  ][  scanConsole  ]: -> Scan:          154 ms
[15:58:47:912][  info  ][  scanConsole  ]: -> Processing:    0 ms
[15:58:47:912][  info  ][  scanConsole  ]: -> Analysis:      800 ms
[15:58:47:912][  info  ][  scanConsole  ]: ###########
[15:58:47:912][  info  ][  scanConsole  ]: # Cleanup #
[15:58:47:912][  info  ][  scanConsole  ]: ###########
[15:58:47:914][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0010068 to configs/CERNQ5/20UPGFC0010068.json
[15:58:48:095][  info  ][  scanConsole  ]: -> Storing output results of FE 4
[15:58:48:095][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0010068, this usually means that the chip did not send any data at all.
[15:58:48:097][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0010100 to configs/CERNQ5/20UPGFC0010100.json
[15:58:48:279][  info  ][  scanConsole  ]: -> Storing output results of FE 5
[15:58:48:279][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0010100, this usually means that the chip did not send any data at all.
[15:58:48:280][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0010084 to configs/CERNQ5/20UPGFC0010084.json
[15:58:48:444][  info  ][  scanConsole  ]: -> Storing output results of FE 7
[15:58:48:444][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0010084, this usually means that the chip did not send any data at all.
[15:58:48:445][  info  ][  scanConsole  ]: Finishing run: 1471
[root@jollyroger Yarr]# 
