[15:54:30:631][  info  ][               ]: #####################################
[15:54:30:631][  info  ][               ]: # Welcome to the YARR Scan Console! #
[15:54:30:631][  info  ][               ]: #####################################
[15:54:30:631][  info  ][               ]: -> Parsing command line parameters ...
[15:54:30:631][  info  ][               ]: Configuring logger ...
[15:54:30:641][  info  ][  scanConsole  ]: Scan Type/Config configs/scans/rd53a/reg_readtemp.json
[15:54:30:641][  info  ][  scanConsole  ]: Connectivity:
[15:54:30:641][  info  ][  scanConsole  ]:     configs/CERNQ5/example_rd53a_setup.json
[15:54:30:641][  info  ][  scanConsole  ]: Target ToT: -1
[15:54:30:641][  info  ][  scanConsole  ]: Target Charge: -1
[15:54:30:641][  info  ][  scanConsole  ]: Output Plots: false
[15:54:30:641][  info  ][  scanConsole  ]: Output Directory: ./data/001469_reg_readtemp/
[15:54:30:659][  info  ][  scanConsole  ]: Timestamp: 2022-08-19_15:54:30
[15:54:30:659][  info  ][  scanConsole  ]: Run Number: 1469
[15:54:30:659][  info  ][  scanConsole  ]: #################
[15:54:30:659][  info  ][  scanConsole  ]: # Init Hardware #
[15:54:30:659][  info  ][  scanConsole  ]: #################
[15:54:30:659][  info  ][  scanConsole  ]: -> Opening controller config: configs/controller/specCfg-rd53a-16x1.json
[15:54:30:660][  info  ][  ScanHelper   ]: Loading controller ...
[15:54:30:660][  info  ][  ScanHelper   ]: Found controller of type: spec
[15:54:30:660][  info  ][  ScanHelper   ]: ... loading controler config:
[15:54:30:660][  info  ][  ScanHelper   ]: ~~~ {
[15:54:30:660][  info  ][  ScanHelper   ]: ~~~     "cmdPeriod": 6.250000073038109e-9,
[15:54:30:660][  info  ][  ScanHelper   ]: ~~~     "idle": {
[15:54:30:660][  info  ][  ScanHelper   ]: ~~~         "word": 1768515945
[15:54:30:660][  info  ][  ScanHelper   ]: ~~~     },
[15:54:30:660][  info  ][  ScanHelper   ]: ~~~     "pulse": {
[15:54:30:660][  info  ][  ScanHelper   ]: ~~~         "interval": 500,
[15:54:30:660][  info  ][  ScanHelper   ]: ~~~         "word": 1549575846
[15:54:30:660][  info  ][  ScanHelper   ]: ~~~     },
[15:54:30:660][  info  ][  ScanHelper   ]: ~~~     "rxActiveLanes": 1,
[15:54:30:660][  info  ][  ScanHelper   ]: ~~~     "rxPolarity": 0,
[15:54:30:660][  info  ][  ScanHelper   ]: ~~~     "specNum": 0,
[15:54:30:660][  info  ][  ScanHelper   ]: ~~~     "spiConfig": 541200,
[15:54:30:660][  info  ][  ScanHelper   ]: ~~~     "sync": {
[15:54:30:660][  info  ][  ScanHelper   ]: ~~~         "interval": 32,
[15:54:30:660][  info  ][  ScanHelper   ]: ~~~         "word": 2172551550
[15:54:30:660][  info  ][  ScanHelper   ]: ~~~     },
[15:54:30:660][  info  ][  ScanHelper   ]: ~~~     "txPolarity": 0
[15:54:30:660][  info  ][  ScanHelper   ]: ~~~ }
[15:54:30:661][  info  ][    SpecCom    ]: Opening SPEC with id #0
[15:54:30:661][  info  ][    SpecCom    ]: Mapping BARs ...
[15:54:30:661][  info  ][    SpecCom    ]: ... Mapped BAR0 at 0x7ffaa9b0e000 with size 1048576
[15:54:30:661][warning ][    SpecCom    ]: ... BAR4 not mapped (Mmap failed)
[15:54:30:661][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[15:54:30:661][  info  ][    SpecCom    ]: Firmware Version: 0x4d9ff6d
[15:54:30:661][  info  ][    SpecCom    ]: Firmware Identifier: 0x4030232
[15:54:30:661][  info  ][    SpecCom    ]: FPGA card: PLDA XpressK7 325
[15:54:30:661][  info  ][    SpecCom    ]: FE Chip Type: RD53A/B
[15:54:30:661][  info  ][    SpecCom    ]: FMC Card Type: Ohio Card (Display Port)
[15:54:30:661][  info  ][    SpecCom    ]: RX Speed: 640Mbps
[15:54:30:661][  info  ][    SpecCom    ]: Channel Configuration: 16x1
[15:54:30:661][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[15:54:30:661][  info  ][    SpecCom    ]: Flushing buffers ...
[15:54:30:661][  info  ][    SpecCom    ]: Init success!
[15:54:30:662][  info  ][  scanConsole  ]: #######################
[15:54:30:662][  info  ][  scanConsole  ]: ##  Loading Configs  ##
[15:54:30:662][  info  ][  scanConsole  ]: #######################
[15:54:30:662][  info  ][  scanConsole  ]: Opening global config: configs/CERNQ5/example_rd53a_setup.json
[15:54:30:662][  info  ][  ScanHelper   ]: Chip type: RD53A
[15:54:30:662][  info  ][  ScanHelper   ]: Chip count 4
[15:54:30:662][  info  ][  ScanHelper   ]: Loading chip #0
[15:54:30:665][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(4)
[15:54:30:665][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ5/20UPGFC0010068.json
[15:54:30:875][  info  ][  ScanHelper   ]: Loading chip #1
[15:54:30:875][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(5)
[15:54:30:875][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ5/20UPGFC0010100.json
[15:54:31:048][  info  ][  ScanHelper   ]: Loading chip #2
[15:54:31:048][warning ][  ScanHelper   ]:  ... chip not enabled, skipping!
[15:54:31:048][  info  ][  ScanHelper   ]: Loading chip #3
[15:54:31:049][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(7)
[15:54:31:049][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ5/20UPGFC0010084.json
[15:54:31:229][  info  ][  scanConsole  ]: #################
[15:54:31:229][  info  ][  scanConsole  ]: # Configure FEs #
[15:54:31:229][  info  ][  scanConsole  ]: #################
[15:54:31:229][  info  ][  scanConsole  ]: Configuring 20UPGFC0010068
[15:54:31:260][  info  ][  scanConsole  ]: Configuring 20UPGFC0010100
[15:54:31:290][  info  ][  scanConsole  ]: Configuring 20UPGFC0010084
[15:54:31:336][  info  ][  scanConsole  ]: Sent configuration to all FEs in 107 ms!
[15:54:31:337][  info  ][  scanConsole  ]: Checking com 20UPGFC0010068
[15:54:31:337][  info  ][    SpecRx     ]: Active Rx channels: 0x10
[15:54:31:337][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[15:54:31:338][  info  ][    SpecRx     ]: Rx Status 0xb0
[15:54:31:338][  info  ][    SpecRx     ]: Number of lanes: 1
[15:54:31:338][  info  ][    SpecRx     ]: Channel 4 Lane 0 synchronized!
[15:54:31:348][  info  ][  scanConsole  ]: ... success!
[15:54:31:348][  info  ][  scanConsole  ]: Checking com 20UPGFC0010100
[15:54:31:348][  info  ][    SpecRx     ]: Active Rx channels: 0x20
[15:54:31:348][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[15:54:31:348][  info  ][    SpecRx     ]: Rx Status 0xb0
[15:54:31:348][  info  ][    SpecRx     ]: Number of lanes: 1
[15:54:31:348][  info  ][    SpecRx     ]: Channel 5 Lane 0 synchronized!
[15:54:31:359][  info  ][  scanConsole  ]: ... success!
[15:54:31:359][  info  ][  scanConsole  ]: Checking com 20UPGFC0010084
[15:54:31:359][  info  ][    SpecRx     ]: Active Rx channels: 0x80
[15:54:31:359][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[15:54:31:359][  info  ][    SpecRx     ]: Rx Status 0xb0
[15:54:31:359][  info  ][    SpecRx     ]: Number of lanes: 1
[15:54:31:359][  info  ][    SpecRx     ]: Channel 7 Lane 0 synchronized!
[15:54:31:369][  info  ][  scanConsole  ]: ... success!
[15:54:31:369][  info  ][  scanConsole  ]: Enabling Tx channels
[15:54:31:369][  info  ][  scanConsole  ]: Enabling Tx channel 1
[15:54:31:369][  info  ][  scanConsole  ]: Enabling Tx channel 1
[15:54:31:369][  info  ][  scanConsole  ]: Enabling Tx channel 1
[15:54:31:369][  info  ][  scanConsole  ]: Enabling Rx channels
[15:54:31:369][  info  ][  scanConsole  ]: Enabling Rx channel 4
[15:54:31:369][  info  ][  scanConsole  ]: Enabling Rx channel 5
[15:54:31:369][  info  ][  scanConsole  ]: Enabling Rx channel 7
[15:54:31:369][  info  ][  scanConsole  ]: ##############
[15:54:31:369][  info  ][  scanConsole  ]: # Setup Scan #
[15:54:31:369][  info  ][  scanConsole  ]: ##############
[15:54:31:370][  info  ][  scanConsole  ]: Found Scan config, constructing scan ...
[15:54:31:370][  info  ][  ScanFactory  ]: Loading Scan:
[15:54:31:370][  info  ][  ScanFactory  ]:   Name: AnalogScan
[15:54:31:370][  info  ][  ScanFactory  ]:   Number of Loops: 3
[15:54:31:370][  info  ][  ScanFactory  ]:   Loading Loop #0
[15:54:31:370][  info  ][  ScanFactory  ]:    Type: Rd53aReadRegLoop
[15:54:31:370][  info  ][  ScanFactory  ]:    Loading loop config ... 
[15:54:31:370][  info  ][  ScanFactory  ]: ~~~ {
[15:54:31:370][  info  ][  ScanFactory  ]: ~~~     "CurMux": [],
[15:54:31:370][  info  ][  ScanFactory  ]: ~~~     "EnblRingOsc": 0,
[15:54:31:370][  info  ][  ScanFactory  ]: ~~~     "Registers": [],
[15:54:31:370][  info  ][  ScanFactory  ]: ~~~     "RingOscDur": 9,
[15:54:31:370][  info  ][  ScanFactory  ]: ~~~     "RingOscRep": 10,
[15:54:31:370][  info  ][  ScanFactory  ]: ~~~     "VoltMux": [3, 5, 7, 15]
[15:54:31:370][  info  ][  ScanFactory  ]: ~~~ }
[15:54:31:371][  info  ][  ScanFactory  ]:   Loading Loop #1
[15:54:31:371][  info  ][  ScanFactory  ]:    Type: Rd53aTriggerLoop
[15:54:31:371][  info  ][  ScanFactory  ]:    Loading loop config ... 
[15:54:31:371][  info  ][  ScanFactory  ]: ~~~ {
[15:54:31:371][  info  ][  ScanFactory  ]: ~~~     "count": 1,
[15:54:31:371][  info  ][  ScanFactory  ]: ~~~     "delay": 48,
[15:54:31:371][  info  ][  ScanFactory  ]: ~~~     "extTrig": false,
[15:54:31:371][  info  ][  ScanFactory  ]: ~~~     "frequency": 30000.0,
[15:54:31:371][  info  ][  ScanFactory  ]: ~~~     "noInject": false,
[15:54:31:371][  info  ][  ScanFactory  ]: ~~~     "sendEcr": false,
[15:54:31:371][  info  ][  ScanFactory  ]: ~~~     "time": 0.0,
[15:54:31:371][  info  ][  ScanFactory  ]: ~~~     "trigMultiplier": 16
[15:54:31:371][  info  ][  ScanFactory  ]: ~~~ }
[15:54:31:371][  info  ][  ScanFactory  ]:   Loading Loop #2
[15:54:31:371][  info  ][  ScanFactory  ]:    Type: StdDataLoop
[15:54:31:371][warning ][  ScanFactory  ]: ~~~ Config empty.
[15:54:31:371][  info  ][ScanBuildHistogrammers]: Loading histogrammer ...
[15:54:31:371][  info  ][ScanBuildHistogrammers]: ... done!
[15:54:31:371][  info  ][ScanBuildAnalyses]: Loading analyses ...
[15:54:31:372][  info  ][  scanConsole  ]: Running pre scan!
[15:54:31:372][  info  ][  ScanFactory  ]: Entering pre scan phase ...
[15:54:31:372][  info  ][     Rd53a     ]: Write named register: GlobalPulseRt -> 16384
[15:54:31:372][  info  ][     Rd53a     ]: Write named register: InjEnDig -> 0
[15:54:31:372][  info  ][     Rd53a     ]: Write named register: InjVcalDiff -> 0
[15:54:31:372][  info  ][     Rd53a     ]: Write named register: InjVcalHigh -> 3500
[15:54:31:372][  info  ][     Rd53a     ]: Write named register: InjVcalMed -> 500
[15:54:31:372][  info  ][     Rd53a     ]: Write named register: LatencyConfig -> 48
[15:54:31:372][  info  ][  scanConsole  ]: Starting histogrammer and analysis threads:
[15:54:31:372][  info  ][  scanConsole  ]:  .. started threads of Fe 4
[15:54:31:372][  info  ][  scanConsole  ]:  .. started threads of Fe 5
[15:54:31:372][  info  ][  scanConsole  ]:  .. started threads of Fe 7
[15:54:31:372][  info  ][Rd53aDataProcessor]:   -> Processor thread #0 started!
[15:54:31:372][  info  ][Rd53aDataProcessor]:   -> Processor thread #1 started!
[15:54:31:373][  info  ][Rd53aDataProcessor]:   -> Processor thread #2 started!
[15:54:31:373][  info  ][Rd53aDataProcessor]:   -> Processor thread #3 started!
[15:54:31:373][  info  ][  scanConsole  ]: ########
[15:54:31:373][  info  ][  scanConsole  ]: # Scan #
[15:54:31:373][  info  ][  scanConsole  ]: ########
[15:54:31:373][  info  ][  scanConsole  ]: Starting scan!
[15:54:31:373][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0010068 on Rx 4
[15:54:31:377][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0010068] MON MUX_V: 3 Bias 0 1195 -> 0.251393 V Bias 1 1605 -> 0.333329 V, Temperature 0.0819361 -> 7.90768 C
[15:54:31:381][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0010068] MON MUX_V: 5 Bias 0 1166 -> 0.245598 V Bias 1 1569 -> 0.326135 V, Temperature 0.0805371 -> 2.83997 C
[15:54:31:386][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0010068] MON MUX_V: 7 Bias 0 1162 -> 0.244798 V Bias 1 1576 -> 0.327534 V, Temperature 0.0827354 -> 2.36926 C
[15:54:31:390][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0010068] MON MUX_V: 15 Bias 0 1153 -> 0.243 V Bias 1 1577 -> 0.327733 V, Temperature 0.0847339 -> 2.84 C
[15:54:31:424][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0010100 on Rx 5
[15:54:31:429][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0010100] MON MUX_V: 3 Bias 0 1184 -> 0.258481 V Bias 1 1600 -> 0.341042 V, Temperature 0.082561 -> 8.01935 C
[15:54:31:433][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0010100] MON MUX_V: 5 Bias 0 1144 -> 0.250543 V Bias 1 1555 -> 0.332111 V, Temperature 0.0815687 -> 1.84998 C
[15:54:31:437][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0010100] MON MUX_V: 7 Bias 0 1130 -> 0.247764 V Bias 1 1552 -> 0.331516 V, Temperature 0.0837518 -> 1.10001 C
[15:54:31:441][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0010100] MON MUX_V: 15 Bias 0 1128 -> 0.247367 V Bias 1 1543 -> 0.32973 V, Temperature 0.0823625 -> 2.60001 C
[15:54:31:476][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0010084 on Rx 7
[15:54:31:480][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0010084] MON MUX_V: 3 Bias 0 1206 -> 0.261356 V Bias 1 1611 -> 0.342168 V, Temperature 0.0808123 -> 7.47504 C
[15:54:31:485][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0010084] MON MUX_V: 5 Bias 0 1172 -> 0.254572 V Bias 1 1575 -> 0.334985 V, Temperature 0.0804132 -> 2.84009 C
[15:54:31:489][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0010084] MON MUX_V: 7 Bias 0 1173 -> 0.254771 V Bias 1 1572 -> 0.334386 V, Temperature 0.0796151 -> -2.35638 C
[15:54:31:493][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0010084] MON MUX_V: 15 Bias 0 1153 -> 0.250781 V Bias 1 1565 -> 0.33299 V, Temperature 0.0822091 -> 0.984711 C
[15:54:31:530][  info  ][  scanConsole  ]: Scan done!
[15:54:31:530][  info  ][  scanConsole  ]: Waiting for processors to finish ...
[15:54:31:531][  info  ][  scanConsole  ]: Processor done, waiting for histogrammer ...
[15:54:31:930][  info  ][HistogramAlgorithm]: Histogrammer done!
[15:54:31:930][  info  ][HistogramAlgorithm]: Histogrammer done!
[15:54:31:930][  info  ][HistogramAlgorithm]: Histogrammer done!
[15:54:31:930][  info  ][  scanConsole  ]: Processor done, waiting for analysis ...
[15:54:32:331][  info  ][AnalysisAlgorithm]: Analysis done!
[15:54:32:331][  info  ][AnalysisAlgorithm]: Analysis done!
[15:54:32:331][  info  ][AnalysisAlgorithm]: Analysis done!
[15:54:32:331][  info  ][  scanConsole  ]: All done!
[15:54:32:331][  info  ][  scanConsole  ]: ##########
[15:54:32:331][  info  ][  scanConsole  ]: # Timing #
[15:54:32:331][  info  ][  scanConsole  ]: ##########
[15:54:32:331][  info  ][  scanConsole  ]: -> Configuration: 107 ms
[15:54:32:331][  info  ][  scanConsole  ]: -> Scan:          156 ms
[15:54:32:331][  info  ][  scanConsole  ]: -> Processing:    0 ms
[15:54:32:331][  info  ][  scanConsole  ]: -> Analysis:      800 ms
[15:54:32:331][  info  ][  scanConsole  ]: ###########
[15:54:32:331][  info  ][  scanConsole  ]: # Cleanup #
[15:54:32:331][  info  ][  scanConsole  ]: ###########
[15:54:32:339][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0010068 to configs/CERNQ5/20UPGFC0010068.json
[15:54:32:507][  info  ][  scanConsole  ]: -> Storing output results of FE 4
[15:54:32:507][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0010068, this usually means that the chip did not send any data at all.
[15:54:32:508][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0010100 to configs/CERNQ5/20UPGFC0010100.json
[15:54:32:674][  info  ][  scanConsole  ]: -> Storing output results of FE 5
[15:54:32:674][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0010100, this usually means that the chip did not send any data at all.
[15:54:32:675][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0010084 to configs/CERNQ5/20UPGFC0010084.json
[15:54:32:853][  info  ][  scanConsole  ]: -> Storing output results of FE 7
[15:54:32:853][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0010084, this usually means that the chip did not send any data at all.
[15:54:32:855][  info  ][  scanConsole  ]: Finishing run: 1469
[root@jollyroger Yarr]# 
