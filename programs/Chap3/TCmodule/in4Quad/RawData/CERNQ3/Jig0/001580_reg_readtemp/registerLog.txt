[16:18:31:064][  info  ][               ]: #####################################
[16:18:31:064][  info  ][               ]: # Welcome to the YARR Scan Console! #
[16:18:31:064][  info  ][               ]: #####################################
[16:18:31:064][  info  ][               ]: -> Parsing command line parameters ...
[16:18:31:064][  info  ][               ]: Configuring logger ...
[16:18:31:067][  info  ][  scanConsole  ]: Scan Type/Config configs/scans/rd53a/reg_readtemp.json
[16:18:31:067][  info  ][  scanConsole  ]: Connectivity:
[16:18:31:067][  info  ][  scanConsole  ]:     configs/CERNQ3/connectivity.json
[16:18:31:067][  info  ][  scanConsole  ]: Target ToT: -1
[16:18:31:067][  info  ][  scanConsole  ]: Target Charge: -1
[16:18:31:067][  info  ][  scanConsole  ]: Output Plots: true
[16:18:31:067][  info  ][  scanConsole  ]: Output Directory: ./data/001580_reg_readtemp/
[16:18:31:071][  info  ][  scanConsole  ]: Timestamp: 2022-08-22_16:18:31
[16:18:31:071][  info  ][  scanConsole  ]: Run Number: 1580
[16:18:31:071][  info  ][  scanConsole  ]: #################
[16:18:31:071][  info  ][  scanConsole  ]: # Init Hardware #
[16:18:31:071][  info  ][  scanConsole  ]: #################
[16:18:31:071][  info  ][  scanConsole  ]: -> Opening controller config: configs/controller/specCfg-rd53a-16x1.json
[16:18:31:071][  info  ][  ScanHelper   ]: Loading controller ...
[16:18:31:071][  info  ][  ScanHelper   ]: Found controller of type: spec
[16:18:31:071][  info  ][  ScanHelper   ]: ... loading controler config:
[16:18:31:071][  info  ][  ScanHelper   ]: ~~~ {
[16:18:31:071][  info  ][  ScanHelper   ]: ~~~     "cmdPeriod": 6.250000073038109e-9,
[16:18:31:071][  info  ][  ScanHelper   ]: ~~~     "idle": {
[16:18:31:071][  info  ][  ScanHelper   ]: ~~~         "word": 1768515945
[16:18:31:071][  info  ][  ScanHelper   ]: ~~~     },
[16:18:31:071][  info  ][  ScanHelper   ]: ~~~     "pulse": {
[16:18:31:071][  info  ][  ScanHelper   ]: ~~~         "interval": 500,
[16:18:31:071][  info  ][  ScanHelper   ]: ~~~         "word": 1549575846
[16:18:31:071][  info  ][  ScanHelper   ]: ~~~     },
[16:18:31:071][  info  ][  ScanHelper   ]: ~~~     "rxActiveLanes": 1,
[16:18:31:071][  info  ][  ScanHelper   ]: ~~~     "rxPolarity": 0,
[16:18:31:071][  info  ][  ScanHelper   ]: ~~~     "specNum": 0,
[16:18:31:071][  info  ][  ScanHelper   ]: ~~~     "spiConfig": 541200,
[16:18:31:071][  info  ][  ScanHelper   ]: ~~~     "sync": {
[16:18:31:071][  info  ][  ScanHelper   ]: ~~~         "interval": 32,
[16:18:31:071][  info  ][  ScanHelper   ]: ~~~         "word": 2172551550
[16:18:31:071][  info  ][  ScanHelper   ]: ~~~     },
[16:18:31:071][  info  ][  ScanHelper   ]: ~~~     "txPolarity": 0
[16:18:31:071][  info  ][  ScanHelper   ]: ~~~ }
[16:18:31:071][  info  ][    SpecCom    ]: Opening SPEC with id #0
[16:18:31:072][  info  ][    SpecCom    ]: Mapping BARs ...
[16:18:31:072][  info  ][    SpecCom    ]: ... Mapped BAR0 at 0x7f10e0a46000 with size 1048576
[16:18:31:072][warning ][    SpecCom    ]: ... BAR4 not mapped (Mmap failed)
[16:18:31:072][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[16:18:31:072][  info  ][    SpecCom    ]: Firmware Version: 0x4d9ff6d
[16:18:31:072][  info  ][    SpecCom    ]: Firmware Identifier: 0x4030232
[16:18:31:072][  info  ][    SpecCom    ]: FPGA card: PLDA XpressK7 325
[16:18:31:072][  info  ][    SpecCom    ]: FE Chip Type: RD53A/B
[16:18:31:072][  info  ][    SpecCom    ]: FMC Card Type: Ohio Card (Display Port)
[16:18:31:072][  info  ][    SpecCom    ]: RX Speed: 640Mbps
[16:18:31:072][  info  ][    SpecCom    ]: Channel Configuration: 16x1
[16:18:31:072][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[16:18:31:072][  info  ][    SpecCom    ]: Flushing buffers ...
[16:18:31:072][  info  ][    SpecCom    ]: Init success!
[16:18:31:072][  info  ][  scanConsole  ]: #######################
[16:18:31:072][  info  ][  scanConsole  ]: ##  Loading Configs  ##
[16:18:31:072][  info  ][  scanConsole  ]: #######################
[16:18:31:072][  info  ][  scanConsole  ]: Opening global config: configs/CERNQ3/connectivity.json
[16:18:31:072][  info  ][  ScanHelper   ]: Chip type: RD53A
[16:18:31:072][  info  ][  ScanHelper   ]: Chip count 4
[16:18:31:072][  info  ][  ScanHelper   ]: Loading chip #0
[16:18:31:073][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(4)
[16:18:31:073][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ3/20UPGFC0012890.json
[16:18:31:247][  info  ][  ScanHelper   ]: Loading chip #1
[16:18:31:247][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(5)
[16:18:31:247][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ3/20UPGFC0012922.json
[16:18:31:420][  info  ][  ScanHelper   ]: Loading chip #2
[16:18:31:421][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(6)
[16:18:31:421][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ3/20UPGFC0012954.json
[16:18:31:592][  info  ][  ScanHelper   ]: Loading chip #3
[16:18:31:593][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(7)
[16:18:31:593][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ3/20UPGFC0012906.json
[16:18:31:765][  info  ][  scanConsole  ]: #################
[16:18:31:765][  info  ][  scanConsole  ]: # Configure FEs #
[16:18:31:765][  info  ][  scanConsole  ]: #################
[16:18:31:765][  info  ][  scanConsole  ]: Configuring 20UPGFC0012890
[16:18:31:796][  info  ][  scanConsole  ]: Configuring 20UPGFC0012922
[16:18:31:827][  info  ][  scanConsole  ]: Configuring 20UPGFC0012954
[16:18:31:860][  info  ][  scanConsole  ]: Configuring 20UPGFC0012906
[16:18:31:908][  info  ][  scanConsole  ]: Sent configuration to all FEs in 143 ms!
[16:18:31:909][  info  ][  scanConsole  ]: Checking com 20UPGFC0012890
[16:18:31:909][  info  ][    SpecRx     ]: Active Rx channels: 0x10
[16:18:31:909][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[16:18:31:909][  info  ][    SpecRx     ]: Rx Status 0xf0
[16:18:31:909][  info  ][    SpecRx     ]: Number of lanes: 1
[16:18:31:909][  info  ][    SpecRx     ]: Channel 4 Lane 0 synchronized!
[16:18:31:920][  info  ][  scanConsole  ]: ... success!
[16:18:31:920][  info  ][  scanConsole  ]: Checking com 20UPGFC0012922
[16:18:31:920][  info  ][    SpecRx     ]: Active Rx channels: 0x20
[16:18:31:920][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[16:18:31:920][  info  ][    SpecRx     ]: Rx Status 0xf0
[16:18:31:920][  info  ][    SpecRx     ]: Number of lanes: 1
[16:18:31:920][  info  ][    SpecRx     ]: Channel 5 Lane 0 synchronized!
[16:18:31:930][  info  ][  scanConsole  ]: ... success!
[16:18:31:930][  info  ][  scanConsole  ]: Checking com 20UPGFC0012954
[16:18:31:930][  info  ][    SpecRx     ]: Active Rx channels: 0x40
[16:18:31:930][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[16:18:31:930][  info  ][    SpecRx     ]: Rx Status 0xf0
[16:18:31:931][  info  ][    SpecRx     ]: Number of lanes: 1
[16:18:31:931][  info  ][    SpecRx     ]: Channel 6 Lane 0 synchronized!
[16:18:31:941][  info  ][  scanConsole  ]: ... success!
[16:18:31:941][  info  ][  scanConsole  ]: Checking com 20UPGFC0012906
[16:18:31:941][  info  ][    SpecRx     ]: Active Rx channels: 0x80
[16:18:31:941][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[16:18:31:941][  info  ][    SpecRx     ]: Rx Status 0xf0
[16:18:31:941][  info  ][    SpecRx     ]: Number of lanes: 1
[16:18:31:941][  info  ][    SpecRx     ]: Channel 7 Lane 0 synchronized!
[16:18:31:951][  info  ][  scanConsole  ]: ... success!
[16:18:31:951][  info  ][  scanConsole  ]: Enabling Tx channels
[16:18:31:952][  info  ][  scanConsole  ]: Enabling Tx channel 1
[16:18:31:952][  info  ][  scanConsole  ]: Enabling Tx channel 1
[16:18:31:952][  info  ][  scanConsole  ]: Enabling Tx channel 1
[16:18:31:952][  info  ][  scanConsole  ]: Enabling Tx channel 1
[16:18:31:952][  info  ][  scanConsole  ]: Enabling Rx channels
[16:18:31:952][  info  ][  scanConsole  ]: Enabling Rx channel 4
[16:18:31:952][  info  ][  scanConsole  ]: Enabling Rx channel 5
[16:18:31:952][  info  ][  scanConsole  ]: Enabling Rx channel 6
[16:18:31:952][  info  ][  scanConsole  ]: Enabling Rx channel 7
[16:18:31:952][  info  ][  scanConsole  ]: ##############
[16:18:31:952][  info  ][  scanConsole  ]: # Setup Scan #
[16:18:31:952][  info  ][  scanConsole  ]: ##############
[16:18:31:952][  info  ][  scanConsole  ]: Found Scan config, constructing scan ...
[16:18:31:953][  info  ][  ScanFactory  ]: Loading Scan:
[16:18:31:953][  info  ][  ScanFactory  ]:   Name: AnalogScan
[16:18:31:953][  info  ][  ScanFactory  ]:   Number of Loops: 3
[16:18:31:953][  info  ][  ScanFactory  ]:   Loading Loop #0
[16:18:31:953][  info  ][  ScanFactory  ]:    Type: Rd53aReadRegLoop
[16:18:31:953][  info  ][  ScanFactory  ]:    Loading loop config ... 
[16:18:31:953][  info  ][  ScanFactory  ]: ~~~ {
[16:18:31:953][  info  ][  ScanFactory  ]: ~~~     "CurMux": [],
[16:18:31:953][  info  ][  ScanFactory  ]: ~~~     "EnblRingOsc": 0,
[16:18:31:953][  info  ][  ScanFactory  ]: ~~~     "Registers": [],
[16:18:31:953][  info  ][  ScanFactory  ]: ~~~     "RingOscDur": 9,
[16:18:31:953][  info  ][  ScanFactory  ]: ~~~     "RingOscRep": 10,
[16:18:31:953][  info  ][  ScanFactory  ]: ~~~     "VoltMux": [3, 5, 7, 15]
[16:18:31:953][  info  ][  ScanFactory  ]: ~~~ }
[16:18:31:953][  info  ][  ScanFactory  ]:   Loading Loop #1
[16:18:31:953][  info  ][  ScanFactory  ]:    Type: Rd53aTriggerLoop
[16:18:31:953][  info  ][  ScanFactory  ]:    Loading loop config ... 
[16:18:31:953][  info  ][  ScanFactory  ]: ~~~ {
[16:18:31:953][  info  ][  ScanFactory  ]: ~~~     "count": 1,
[16:18:31:953][  info  ][  ScanFactory  ]: ~~~     "delay": 48,
[16:18:31:953][  info  ][  ScanFactory  ]: ~~~     "extTrig": false,
[16:18:31:953][  info  ][  ScanFactory  ]: ~~~     "frequency": 30000.0,
[16:18:31:953][  info  ][  ScanFactory  ]: ~~~     "noInject": false,
[16:18:31:953][  info  ][  ScanFactory  ]: ~~~     "sendEcr": false,
[16:18:31:953][  info  ][  ScanFactory  ]: ~~~     "time": 0.0,
[16:18:31:953][  info  ][  ScanFactory  ]: ~~~     "trigMultiplier": 16
[16:18:31:953][  info  ][  ScanFactory  ]: ~~~ }
[16:18:31:953][  info  ][  ScanFactory  ]:   Loading Loop #2
[16:18:31:953][  info  ][  ScanFactory  ]:    Type: StdDataLoop
[16:18:31:954][warning ][  ScanFactory  ]: ~~~ Config empty.
[16:18:31:954][  info  ][ScanBuildHistogrammers]: Loading histogrammer ...
[16:18:31:954][  info  ][ScanBuildHistogrammers]: ... done!
[16:18:31:954][  info  ][ScanBuildAnalyses]: Loading analyses ...
[16:18:31:954][  info  ][  scanConsole  ]: Running pre scan!
[16:18:31:954][  info  ][  ScanFactory  ]: Entering pre scan phase ...
[16:18:31:954][  info  ][     Rd53a     ]: Write named register: GlobalPulseRt -> 16384
[16:18:31:954][  info  ][     Rd53a     ]: Write named register: InjEnDig -> 0
[16:18:31:954][  info  ][     Rd53a     ]: Write named register: InjVcalDiff -> 0
[16:18:31:954][  info  ][     Rd53a     ]: Write named register: InjVcalHigh -> 3500
[16:18:31:955][  info  ][     Rd53a     ]: Write named register: InjVcalMed -> 500
[16:18:31:955][  info  ][     Rd53a     ]: Write named register: LatencyConfig -> 48
[16:18:31:955][  info  ][  scanConsole  ]: Starting histogrammer and analysis threads:
[16:18:31:955][  info  ][  scanConsole  ]:  .. started threads of Fe 4
[16:18:31:955][  info  ][  scanConsole  ]:  .. started threads of Fe 5
[16:18:31:955][  info  ][  scanConsole  ]:  .. started threads of Fe 6
[16:18:31:955][  info  ][  scanConsole  ]:  .. started threads of Fe 7
[16:18:31:956][  info  ][Rd53aDataProcessor]:   -> Processor thread #0 started!
[16:18:31:956][  info  ][Rd53aDataProcessor]:   -> Processor thread #1 started!
[16:18:31:956][  info  ][Rd53aDataProcessor]:   -> Processor thread #2 started!
[16:18:31:956][  info  ][Rd53aDataProcessor]:   -> Processor thread #3 started!
[16:18:31:956][  info  ][  scanConsole  ]: ########
[16:18:31:956][  info  ][  scanConsole  ]: # Scan #
[16:18:31:956][  info  ][  scanConsole  ]: ########
[16:18:31:956][  info  ][  scanConsole  ]: Starting scan!
[16:18:31:956][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0012890 on Rx 4
[16:18:31:960][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0012890] MON MUX_V: 3 Bias 0 1192 -> 0.252009 V Bias 1 1597 -> 0.333362 V, Temperature 0.0813537 -> 8.13638 C
[16:18:31:964][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0012890] MON MUX_V: 5 Bias 0 1131 -> 0.239755 V Bias 1 1545 -> 0.322917 V, Temperature 0.0831615 -> -7.55728 C
[16:18:31:969][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0012890] MON MUX_V: 7 Bias 0 1127 -> 0.238952 V Bias 1 1547 -> 0.323319 V, Temperature 0.0843668 -> -7.84665 C
[16:18:31:973][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0012890] MON MUX_V: 15 Bias 0 1113 -> 0.23614 V Bias 1 1528 -> 0.319502 V, Temperature 0.0833625 -> -13.0691 C
[16:18:32:008][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0012922 on Rx 5
[16:18:32:012][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0012922] MON MUX_V: 3 Bias 0 1182 -> 0.2484 V Bias 1 1591 -> 0.3302 V, Temperature 0.0818 -> 9.38004 C
[16:18:32:016][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0012922] MON MUX_V: 5 Bias 0 1135 -> 0.239 V Bias 1 1555 -> 0.323 V, Temperature 0.084 -> -2.61102 C
[16:18:32:020][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0012922] MON MUX_V: 7 Bias 0 1129 -> 0.2378 V Bias 1 1548 -> 0.3216 V, Temperature 0.0838 -> -5.64108 C
[16:18:32:024][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0012922] MON MUX_V: 15 Bias 0 1123 -> 0.2366 V Bias 1 1545 -> 0.321 V, Temperature 0.0844 -> -4.74704 C
[16:18:32:059][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0012954 on Rx 6
[16:18:32:063][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0012954] MON MUX_V: 3 Bias 0 1189 -> 0.248667 V Bias 1 1594 -> 0.330426 V, Temperature 0.0817582 -> 5.47144 C
[16:18:32:067][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0012954] MON MUX_V: 5 Bias 0 1148 -> 0.240391 V Bias 1 1554 -> 0.322351 V, Temperature 0.0819601 -> -8.85999 C
[16:18:32:072][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0012954] MON MUX_V: 7 Bias 0 1122 -> 0.235142 V Bias 1 1540 -> 0.319524 V, Temperature 0.0843825 -> -5.14441 C
[16:18:32:076][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0012954] MON MUX_V: 15 Bias 0 1125 -> 0.235748 V Bias 1 1541 -> 0.319726 V, Temperature 0.0839788 -> -8.85999 C
[16:18:32:111][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0012906 on Rx 7
[16:18:32:115][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0012906] MON MUX_V: 3 Bias 0 1186 -> 0.25001 V Bias 1 1589 -> 0.332201 V, Temperature 0.0821913 -> 7.97701 C
[16:18:32:119][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0012906] MON MUX_V: 5 Bias 0 1143 -> 0.24124 V Bias 1 1544 -> 0.323024 V, Temperature 0.0817834 -> 0.536346 C
[16:18:32:123][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0012906] MON MUX_V: 7 Bias 0 1144 -> 0.241444 V Bias 1 1552 -> 0.324655 V, Temperature 0.083211 -> -1.53641 C
[16:18:32:127][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0012906] MON MUX_V: 15 Bias 0 1117 -> 0.235938 V Bias 1 1531 -> 0.320372 V, Temperature 0.0844347 -> -1.76672 C
[16:18:32:163][  info  ][  scanConsole  ]: Scan done!
[16:18:32:163][  info  ][  scanConsole  ]: Waiting for processors to finish ...
[16:18:32:164][  info  ][  scanConsole  ]: Processor done, waiting for histogrammer ...
[16:18:32:564][  info  ][HistogramAlgorithm]: Histogrammer done!
[16:18:32:564][  info  ][HistogramAlgorithm]: Histogrammer done!
[16:18:32:564][  info  ][HistogramAlgorithm]: Histogrammer done!
[16:18:32:564][  info  ][HistogramAlgorithm]: Histogrammer done!
[16:18:32:564][  info  ][  scanConsole  ]: Processor done, waiting for analysis ...
[16:18:32:964][  info  ][AnalysisAlgorithm]: Analysis done!
[16:18:32:964][  info  ][AnalysisAlgorithm]: Analysis done!
[16:18:32:964][  info  ][AnalysisAlgorithm]: Analysis done!
[16:18:32:964][  info  ][AnalysisAlgorithm]: Analysis done!
[16:18:32:965][  info  ][  scanConsole  ]: All done!
[16:18:32:965][  info  ][  scanConsole  ]: ##########
[16:18:32:965][  info  ][  scanConsole  ]: # Timing #
[16:18:32:965][  info  ][  scanConsole  ]: ##########
[16:18:32:965][  info  ][  scanConsole  ]: -> Configuration: 143 ms
[16:18:32:965][  info  ][  scanConsole  ]: -> Scan:          207 ms
[16:18:32:965][  info  ][  scanConsole  ]: -> Processing:    0 ms
[16:18:32:965][  info  ][  scanConsole  ]: -> Analysis:      800 ms
[16:18:32:965][  info  ][  scanConsole  ]: ###########
[16:18:32:965][  info  ][  scanConsole  ]: # Cleanup #
[16:18:32:965][  info  ][  scanConsole  ]: ###########
[16:18:32:973][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0012890 to configs/CERNQ3/20UPGFC0012890.json
[16:18:33:143][  info  ][  scanConsole  ]: -> Storing output results of FE 4
[16:18:33:143][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0012890, this usually means that the chip did not send any data at all.
[16:18:33:144][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0012922 to configs/CERNQ3/20UPGFC0012922.json
[16:18:33:307][  info  ][  scanConsole  ]: -> Storing output results of FE 5
[16:18:33:307][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0012922, this usually means that the chip did not send any data at all.
[16:18:33:308][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0012954 to configs/CERNQ3/20UPGFC0012954.json
[16:18:33:471][  info  ][  scanConsole  ]: -> Storing output results of FE 6
[16:18:33:471][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0012954, this usually means that the chip did not send any data at all.
[16:18:33:472][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0012906 to configs/CERNQ3/20UPGFC0012906.json
[16:18:33:635][  info  ][  scanConsole  ]: -> Storing output results of FE 7
[16:18:33:635][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0012906, this usually means that the chip did not send any data at all.
[16:18:33:636][  info  ][  scanConsole  ]: Finishing run: 1580
20UPGFC0012890.json.after
20UPGFC0012890.json.before
20UPGFC0012906.json.after
20UPGFC0012906.json.before
20UPGFC0012922.json.after
20UPGFC0012922.json.before
20UPGFC0012954.json.after
20UPGFC0012954.json.before
reg_readtemp.json
scanLog.json

