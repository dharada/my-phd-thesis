[18:02:56:659][  info  ][               ]: #####################################
[18:02:56:659][  info  ][               ]: # Welcome to the YARR Scan Console! #
[18:02:56:659][  info  ][               ]: #####################################
[18:02:56:659][  info  ][               ]: -> Parsing command line parameters ...
[18:02:56:659][  info  ][               ]: Configuring logger ...
[18:02:56:661][  info  ][  scanConsole  ]: Scan Type/Config configs/scans/rd53a/reg_readtemp.json
[18:02:56:661][  info  ][  scanConsole  ]: Connectivity:
[18:02:56:661][  info  ][  scanConsole  ]:     configs/CERNQ3/connectivity.json
[18:02:56:661][  info  ][  scanConsole  ]: Target ToT: -1
[18:02:56:661][  info  ][  scanConsole  ]: Target Charge: -1
[18:02:56:661][  info  ][  scanConsole  ]: Output Plots: true
[18:02:56:661][  info  ][  scanConsole  ]: Output Directory: ./data/001595_reg_readtemp/
[18:02:56:666][  info  ][  scanConsole  ]: Timestamp: 2022-08-22_18:02:56
[18:02:56:666][  info  ][  scanConsole  ]: Run Number: 1595
[18:02:56:666][  info  ][  scanConsole  ]: #################
[18:02:56:666][  info  ][  scanConsole  ]: # Init Hardware #
[18:02:56:666][  info  ][  scanConsole  ]: #################
[18:02:56:666][  info  ][  scanConsole  ]: -> Opening controller config: configs/controller/specCfg-rd53a-16x1.json
[18:02:56:666][  info  ][  ScanHelper   ]: Loading controller ...
[18:02:56:666][  info  ][  ScanHelper   ]: Found controller of type: spec
[18:02:56:666][  info  ][  ScanHelper   ]: ... loading controler config:
[18:02:56:666][  info  ][  ScanHelper   ]: ~~~ {
[18:02:56:666][  info  ][  ScanHelper   ]: ~~~     "cmdPeriod": 6.250000073038109e-9,
[18:02:56:666][  info  ][  ScanHelper   ]: ~~~     "idle": {
[18:02:56:666][  info  ][  ScanHelper   ]: ~~~         "word": 1768515945
[18:02:56:666][  info  ][  ScanHelper   ]: ~~~     },
[18:02:56:666][  info  ][  ScanHelper   ]: ~~~     "pulse": {
[18:02:56:666][  info  ][  ScanHelper   ]: ~~~         "interval": 500,
[18:02:56:666][  info  ][  ScanHelper   ]: ~~~         "word": 1549575846
[18:02:56:666][  info  ][  ScanHelper   ]: ~~~     },
[18:02:56:666][  info  ][  ScanHelper   ]: ~~~     "rxActiveLanes": 1,
[18:02:56:666][  info  ][  ScanHelper   ]: ~~~     "rxPolarity": 0,
[18:02:56:666][  info  ][  ScanHelper   ]: ~~~     "specNum": 0,
[18:02:56:666][  info  ][  ScanHelper   ]: ~~~     "spiConfig": 541200,
[18:02:56:666][  info  ][  ScanHelper   ]: ~~~     "sync": {
[18:02:56:666][  info  ][  ScanHelper   ]: ~~~         "interval": 32,
[18:02:56:666][  info  ][  ScanHelper   ]: ~~~         "word": 2172551550
[18:02:56:666][  info  ][  ScanHelper   ]: ~~~     },
[18:02:56:666][  info  ][  ScanHelper   ]: ~~~     "txPolarity": 0
[18:02:56:666][  info  ][  ScanHelper   ]: ~~~ }
[18:02:56:666][  info  ][    SpecCom    ]: Opening SPEC with id #0
[18:02:56:666][  info  ][    SpecCom    ]: Mapping BARs ...
[18:02:56:666][  info  ][    SpecCom    ]: ... Mapped BAR0 at 0x7f2d0dd3d000 with size 1048576
[18:02:56:666][warning ][    SpecCom    ]: ... BAR4 not mapped (Mmap failed)
[18:02:56:666][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[18:02:56:666][  info  ][    SpecCom    ]: Firmware Version: 0x4d9ff6d
[18:02:56:666][  info  ][    SpecCom    ]: Firmware Identifier: 0x4030232
[18:02:56:666][  info  ][    SpecCom    ]: FPGA card: PLDA XpressK7 325
[18:02:56:666][  info  ][    SpecCom    ]: FE Chip Type: RD53A/B
[18:02:56:666][  info  ][    SpecCom    ]: FMC Card Type: Ohio Card (Display Port)
[18:02:56:667][  info  ][    SpecCom    ]: RX Speed: 640Mbps
[18:02:56:667][  info  ][    SpecCom    ]: Channel Configuration: 16x1
[18:02:56:667][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[18:02:56:667][  info  ][    SpecCom    ]: Flushing buffers ...
[18:02:56:667][  info  ][    SpecCom    ]: Init success!
[18:02:56:667][  info  ][  scanConsole  ]: #######################
[18:02:56:667][  info  ][  scanConsole  ]: ##  Loading Configs  ##
[18:02:56:667][  info  ][  scanConsole  ]: #######################
[18:02:56:667][  info  ][  scanConsole  ]: Opening global config: configs/CERNQ3/connectivity.json
[18:02:56:667][  info  ][  ScanHelper   ]: Chip type: RD53A
[18:02:56:667][  info  ][  ScanHelper   ]: Chip count 4
[18:02:56:667][  info  ][  ScanHelper   ]: Loading chip #0
[18:02:56:667][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(4)
[18:02:56:667][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ3/20UPGFC0012890.json
[18:02:56:843][  info  ][  ScanHelper   ]: Loading chip #1
[18:02:56:843][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(5)
[18:02:56:843][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ3/20UPGFC0012922.json
[18:02:57:014][  info  ][  ScanHelper   ]: Loading chip #2
[18:02:57:014][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(6)
[18:02:57:014][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ3/20UPGFC0012954.json
[18:02:57:187][  info  ][  ScanHelper   ]: Loading chip #3
[18:02:57:188][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(7)
[18:02:57:188][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ3/20UPGFC0012906.json
[18:02:57:363][  info  ][  scanConsole  ]: #################
[18:02:57:363][  info  ][  scanConsole  ]: # Configure FEs #
[18:02:57:363][  info  ][  scanConsole  ]: #################
[18:02:57:363][  info  ][  scanConsole  ]: Configuring 20UPGFC0012890
[18:02:57:394][  info  ][  scanConsole  ]: Configuring 20UPGFC0012922
[18:02:57:437][  info  ][  scanConsole  ]: Configuring 20UPGFC0012954
[18:02:57:471][  info  ][  scanConsole  ]: Configuring 20UPGFC0012906
[18:02:57:505][  info  ][  scanConsole  ]: Sent configuration to all FEs in 141 ms!
[18:02:57:506][  info  ][  scanConsole  ]: Checking com 20UPGFC0012890
[18:02:57:506][  info  ][    SpecRx     ]: Active Rx channels: 0x10
[18:02:57:506][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[18:02:57:506][  info  ][    SpecRx     ]: Rx Status 0xf0
[18:02:57:506][  info  ][    SpecRx     ]: Number of lanes: 1
[18:02:57:506][  info  ][    SpecRx     ]: Channel 4 Lane 0 synchronized!
[18:02:57:517][  info  ][  scanConsole  ]: ... success!
[18:02:57:517][  info  ][  scanConsole  ]: Checking com 20UPGFC0012922
[18:02:57:517][  info  ][    SpecRx     ]: Active Rx channels: 0x20
[18:02:57:517][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[18:02:57:517][  info  ][    SpecRx     ]: Rx Status 0xf0
[18:02:57:517][  info  ][    SpecRx     ]: Number of lanes: 1
[18:02:57:517][  info  ][    SpecRx     ]: Channel 5 Lane 0 synchronized!
[18:02:57:527][  info  ][  scanConsole  ]: ... success!
[18:02:57:527][  info  ][  scanConsole  ]: Checking com 20UPGFC0012954
[18:02:57:527][  info  ][    SpecRx     ]: Active Rx channels: 0x40
[18:02:57:527][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[18:02:57:527][  info  ][    SpecRx     ]: Rx Status 0xf0
[18:02:57:527][  info  ][    SpecRx     ]: Number of lanes: 1
[18:02:57:527][  info  ][    SpecRx     ]: Channel 6 Lane 0 synchronized!
[18:02:57:538][  info  ][  scanConsole  ]: ... success!
[18:02:57:538][  info  ][  scanConsole  ]: Checking com 20UPGFC0012906
[18:02:57:538][  info  ][    SpecRx     ]: Active Rx channels: 0x80
[18:02:57:538][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[18:02:57:538][  info  ][    SpecRx     ]: Rx Status 0xf0
[18:02:57:538][  info  ][    SpecRx     ]: Number of lanes: 1
[18:02:57:538][  info  ][    SpecRx     ]: Channel 7 Lane 0 synchronized!
[18:02:57:549][  info  ][  scanConsole  ]: ... success!
[18:02:57:549][  info  ][  scanConsole  ]: Enabling Tx channels
[18:02:57:549][  info  ][  scanConsole  ]: Enabling Tx channel 1
[18:02:57:549][  info  ][  scanConsole  ]: Enabling Tx channel 1
[18:02:57:549][  info  ][  scanConsole  ]: Enabling Tx channel 1
[18:02:57:549][  info  ][  scanConsole  ]: Enabling Tx channel 1
[18:02:57:549][  info  ][  scanConsole  ]: Enabling Rx channels
[18:02:57:549][  info  ][  scanConsole  ]: Enabling Rx channel 4
[18:02:57:549][  info  ][  scanConsole  ]: Enabling Rx channel 5
[18:02:57:549][  info  ][  scanConsole  ]: Enabling Rx channel 6
[18:02:57:549][  info  ][  scanConsole  ]: Enabling Rx channel 7
[18:02:57:549][  info  ][  scanConsole  ]: ##############
[18:02:57:549][  info  ][  scanConsole  ]: # Setup Scan #
[18:02:57:549][  info  ][  scanConsole  ]: ##############
[18:02:57:549][  info  ][  scanConsole  ]: Found Scan config, constructing scan ...
[18:02:57:550][  info  ][  ScanFactory  ]: Loading Scan:
[18:02:57:550][  info  ][  ScanFactory  ]:   Name: AnalogScan
[18:02:57:550][  info  ][  ScanFactory  ]:   Number of Loops: 3
[18:02:57:550][  info  ][  ScanFactory  ]:   Loading Loop #0
[18:02:57:550][  info  ][  ScanFactory  ]:    Type: Rd53aReadRegLoop
[18:02:57:550][  info  ][  ScanFactory  ]:    Loading loop config ... 
[18:02:57:550][  info  ][  ScanFactory  ]: ~~~ {
[18:02:57:550][  info  ][  ScanFactory  ]: ~~~     "CurMux": [],
[18:02:57:550][  info  ][  ScanFactory  ]: ~~~     "EnblRingOsc": 0,
[18:02:57:550][  info  ][  ScanFactory  ]: ~~~     "Registers": [],
[18:02:57:550][  info  ][  ScanFactory  ]: ~~~     "RingOscDur": 9,
[18:02:57:550][  info  ][  ScanFactory  ]: ~~~     "RingOscRep": 10,
[18:02:57:550][  info  ][  ScanFactory  ]: ~~~     "VoltMux": [3, 5, 7, 15]
[18:02:57:550][  info  ][  ScanFactory  ]: ~~~ }
[18:02:57:550][  info  ][  ScanFactory  ]:   Loading Loop #1
[18:02:57:550][  info  ][  ScanFactory  ]:    Type: Rd53aTriggerLoop
[18:02:57:550][  info  ][  ScanFactory  ]:    Loading loop config ... 
[18:02:57:550][  info  ][  ScanFactory  ]: ~~~ {
[18:02:57:550][  info  ][  ScanFactory  ]: ~~~     "count": 1,
[18:02:57:550][  info  ][  ScanFactory  ]: ~~~     "delay": 48,
[18:02:57:550][  info  ][  ScanFactory  ]: ~~~     "extTrig": false,
[18:02:57:550][  info  ][  ScanFactory  ]: ~~~     "frequency": 30000.0,
[18:02:57:550][  info  ][  ScanFactory  ]: ~~~     "noInject": false,
[18:02:57:550][  info  ][  ScanFactory  ]: ~~~     "sendEcr": false,
[18:02:57:550][  info  ][  ScanFactory  ]: ~~~     "time": 0.0,
[18:02:57:550][  info  ][  ScanFactory  ]: ~~~     "trigMultiplier": 16
[18:02:57:550][  info  ][  ScanFactory  ]: ~~~ }
[18:02:57:550][  info  ][  ScanFactory  ]:   Loading Loop #2
[18:02:57:550][  info  ][  ScanFactory  ]:    Type: StdDataLoop
[18:02:57:550][warning ][  ScanFactory  ]: ~~~ Config empty.
[18:02:57:551][  info  ][ScanBuildHistogrammers]: Loading histogrammer ...
[18:02:57:551][  info  ][ScanBuildHistogrammers]: ... done!
[18:02:57:551][  info  ][ScanBuildAnalyses]: Loading analyses ...
[18:02:57:551][  info  ][  scanConsole  ]: Running pre scan!
[18:02:57:551][  info  ][  ScanFactory  ]: Entering pre scan phase ...
[18:02:57:551][  info  ][     Rd53a     ]: Write named register: GlobalPulseRt -> 16384
[18:02:57:551][  info  ][     Rd53a     ]: Write named register: InjEnDig -> 0
[18:02:57:551][  info  ][     Rd53a     ]: Write named register: InjVcalDiff -> 0
[18:02:57:551][  info  ][     Rd53a     ]: Write named register: InjVcalHigh -> 3500
[18:02:57:551][  info  ][     Rd53a     ]: Write named register: InjVcalMed -> 500
[18:02:57:551][  info  ][     Rd53a     ]: Write named register: LatencyConfig -> 48
[18:02:57:552][  info  ][  scanConsole  ]: Starting histogrammer and analysis threads:
[18:02:57:552][  info  ][  scanConsole  ]:  .. started threads of Fe 4
[18:02:57:552][  info  ][  scanConsole  ]:  .. started threads of Fe 5
[18:02:57:552][  info  ][  scanConsole  ]:  .. started threads of Fe 6
[18:02:57:552][  info  ][  scanConsole  ]:  .. started threads of Fe 7
[18:02:57:553][  info  ][Rd53aDataProcessor]:   -> Processor thread #0 started!
[18:02:57:553][  info  ][Rd53aDataProcessor]:   -> Processor thread #1 started!
[18:02:57:553][  info  ][Rd53aDataProcessor]:   -> Processor thread #2 started!
[18:02:57:553][  info  ][Rd53aDataProcessor]:   -> Processor thread #3 started!
[18:02:57:553][  info  ][  scanConsole  ]: ########
[18:02:57:553][  info  ][  scanConsole  ]: # Scan #
[18:02:57:553][  info  ][  scanConsole  ]: ########
[18:02:57:553][  info  ][  scanConsole  ]: Starting scan!
[18:02:57:553][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0012890 on Rx 4
[18:02:57:557][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0012890] MON MUX_V: 3 Bias 0 1257 -> 0.265065 V Bias 1 1647 -> 0.343406 V, Temperature 0.0783406 -> -2.22726 C
[18:02:57:561][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0012890] MON MUX_V: 5 Bias 0 1194 -> 0.25241 V Bias 1 1594 -> 0.33276 V, Temperature 0.0803494 -> -22.7571 C
[18:02:57:565][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0012890] MON MUX_V: 7 Bias 0 1192 -> 0.252009 V Bias 1 1597 -> 0.333362 V, Temperature 0.0813537 -> -23.0466 C
[18:02:57:569][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0012890] MON MUX_V: 15 Bias 0 1177 -> 0.248996 V Bias 1 1578 -> 0.329546 V, Temperature 0.0805502 -> -29.4386 C
[18:02:57:604][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0012922 on Rx 5
[18:02:57:608][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0012922] MON MUX_V: 3 Bias 0 1245 -> 0.261 V Bias 1 1638 -> 0.3396 V, Temperature 0.0786 -> -0.348038 C
[18:02:57:612][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0012922] MON MUX_V: 5 Bias 0 1198 -> 0.2516 V Bias 1 1604 -> 0.3328 V, Temperature 0.0812 -> -14.4332 C
[18:02:57:616][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0012922] MON MUX_V: 7 Bias 0 1193 -> 0.2506 V Bias 1 1597 -> 0.3314 V, Temperature 0.0808 -> -19.0529 C
[18:02:57:620][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0012922] MON MUX_V: 15 Bias 0 1187 -> 0.2494 V Bias 1 1594 -> 0.3308 V, Temperature 0.0814 -> -18.1587 C
[18:02:57:655][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0012954 on Rx 6
[18:02:57:659][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0012954] MON MUX_V: 3 Bias 0 1251 -> 0.261183 V Bias 1 1641 -> 0.339914 V, Temperature 0.0787301 -> -5.38562 C
[18:02:57:663][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0012954] MON MUX_V: 5 Bias 0 1211 -> 0.253109 V Bias 1 1603 -> 0.332242 V, Temperature 0.0791339 -> -23.0466 C
[18:02:57:668][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0012954] MON MUX_V: 7 Bias 0 1186 -> 0.248062 V Bias 1 1589 -> 0.329416 V, Temperature 0.0813545 -> -17.811 C
[18:02:57:672][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0012954] MON MUX_V: 15 Bias 0 1189 -> 0.248667 V Bias 1 1588 -> 0.329214 V, Temperature 0.080547 -> -26.0865 C
[18:02:57:706][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0012906 on Rx 7
[18:02:57:711][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0012906] MON MUX_V: 3 Bias 0 1243 -> 0.261635 V Bias 1 1631 -> 0.340767 V, Temperature 0.0791321 -> -0.792328 C
[18:02:57:715][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0012906] MON MUX_V: 5 Bias 0 1202 -> 0.253273 V Bias 1 1586 -> 0.331589 V, Temperature 0.0783163 -> -11.2091 C
[18:02:57:719][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0012906] MON MUX_V: 7 Bias 0 1203 -> 0.253477 V Bias 1 1595 -> 0.333425 V, Temperature 0.0799479 -> -12.5909 C
[18:02:57:723][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0012906] MON MUX_V: 15 Bias 0 1177 -> 0.248174 V Bias 1 1576 -> 0.32955 V, Temperature 0.0813755 -> -12.6237 C
[18:02:57:759][  info  ][  scanConsole  ]: Scan done!
[18:02:57:759][  info  ][  scanConsole  ]: Waiting for processors to finish ...
[18:02:57:760][  info  ][  scanConsole  ]: Processor done, waiting for histogrammer ...
[18:02:58:160][  info  ][HistogramAlgorithm]: Histogrammer done!
[18:02:58:160][  info  ][HistogramAlgorithm]: Histogrammer done!
[18:02:58:160][  info  ][HistogramAlgorithm]: Histogrammer done!
[18:02:58:160][  info  ][HistogramAlgorithm]: Histogrammer done!
[18:02:58:160][  info  ][  scanConsole  ]: Processor done, waiting for analysis ...
[18:02:58:560][  info  ][AnalysisAlgorithm]: Analysis done!
[18:02:58:560][  info  ][AnalysisAlgorithm]: Analysis done!
[18:02:58:561][  info  ][AnalysisAlgorithm]: Analysis done!
[18:02:58:560][  info  ][AnalysisAlgorithm]: Analysis done!
[18:02:58:561][  info  ][  scanConsole  ]: All done!
[18:02:58:561][  info  ][  scanConsole  ]: ##########
[18:02:58:561][  info  ][  scanConsole  ]: # Timing #
[18:02:58:561][  info  ][  scanConsole  ]: ##########
[18:02:58:561][  info  ][  scanConsole  ]: -> Configuration: 141 ms
[18:02:58:561][  info  ][  scanConsole  ]: -> Scan:          206 ms
[18:02:58:561][  info  ][  scanConsole  ]: -> Processing:    0 ms
[18:02:58:561][  info  ][  scanConsole  ]: -> Analysis:      800 ms
[18:02:58:561][  info  ][  scanConsole  ]: ###########
[18:02:58:561][  info  ][  scanConsole  ]: # Cleanup #
[18:02:58:561][  info  ][  scanConsole  ]: ###########
[18:02:58:569][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0012890 to configs/CERNQ3/20UPGFC0012890.json
[18:02:58:745][  info  ][  scanConsole  ]: -> Storing output results of FE 4
[18:02:58:745][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0012890, this usually means that the chip did not send any data at all.
[18:02:58:747][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0012922 to configs/CERNQ3/20UPGFC0012922.json
[18:02:58:916][  info  ][  scanConsole  ]: -> Storing output results of FE 5
[18:02:58:916][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0012922, this usually means that the chip did not send any data at all.
[18:02:58:918][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0012954 to configs/CERNQ3/20UPGFC0012954.json
[18:02:59:088][  info  ][  scanConsole  ]: -> Storing output results of FE 6
[18:02:59:088][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0012954, this usually means that the chip did not send any data at all.
[18:02:59:089][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0012906 to configs/CERNQ3/20UPGFC0012906.json
[18:02:59:258][  info  ][  scanConsole  ]: -> Storing output results of FE 7
[18:02:59:258][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0012906, this usually means that the chip did not send any data at all.
[18:02:59:259][  info  ][  scanConsole  ]: Finishing run: 1595
20UPGFC0012890.json.after
20UPGFC0012890.json.before
20UPGFC0012906.json.after
20UPGFC0012906.json.before
20UPGFC0012922.json.after
20UPGFC0012922.json.before
20UPGFC0012954.json.after
20UPGFC0012954.json.before
reg_readtemp.json
scanLog.json

