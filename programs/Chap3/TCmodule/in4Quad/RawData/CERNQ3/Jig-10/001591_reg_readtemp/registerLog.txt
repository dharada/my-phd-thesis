[17:51:49:573][  info  ][               ]: #####################################
[17:51:49:573][  info  ][               ]: # Welcome to the YARR Scan Console! #
[17:51:49:573][  info  ][               ]: #####################################
[17:51:49:573][  info  ][               ]: -> Parsing command line parameters ...
[17:51:49:574][  info  ][               ]: Configuring logger ...
[17:51:49:576][  info  ][  scanConsole  ]: Scan Type/Config configs/scans/rd53a/reg_readtemp.json
[17:51:49:576][  info  ][  scanConsole  ]: Connectivity:
[17:51:49:576][  info  ][  scanConsole  ]:     configs/CERNQ3/connectivity.json
[17:51:49:576][  info  ][  scanConsole  ]: Target ToT: -1
[17:51:49:576][  info  ][  scanConsole  ]: Target Charge: -1
[17:51:49:576][  info  ][  scanConsole  ]: Output Plots: true
[17:51:49:576][  info  ][  scanConsole  ]: Output Directory: ./data/001591_reg_readtemp/
[17:51:49:581][  info  ][  scanConsole  ]: Timestamp: 2022-08-22_17:51:49
[17:51:49:581][  info  ][  scanConsole  ]: Run Number: 1591
[17:51:49:581][  info  ][  scanConsole  ]: #################
[17:51:49:581][  info  ][  scanConsole  ]: # Init Hardware #
[17:51:49:581][  info  ][  scanConsole  ]: #################
[17:51:49:581][  info  ][  scanConsole  ]: -> Opening controller config: configs/controller/specCfg-rd53a-16x1.json
[17:51:49:581][  info  ][  ScanHelper   ]: Loading controller ...
[17:51:49:581][  info  ][  ScanHelper   ]: Found controller of type: spec
[17:51:49:581][  info  ][  ScanHelper   ]: ... loading controler config:
[17:51:49:581][  info  ][  ScanHelper   ]: ~~~ {
[17:51:49:581][  info  ][  ScanHelper   ]: ~~~     "cmdPeriod": 6.250000073038109e-9,
[17:51:49:581][  info  ][  ScanHelper   ]: ~~~     "idle": {
[17:51:49:581][  info  ][  ScanHelper   ]: ~~~         "word": 1768515945
[17:51:49:581][  info  ][  ScanHelper   ]: ~~~     },
[17:51:49:581][  info  ][  ScanHelper   ]: ~~~     "pulse": {
[17:51:49:581][  info  ][  ScanHelper   ]: ~~~         "interval": 500,
[17:51:49:581][  info  ][  ScanHelper   ]: ~~~         "word": 1549575846
[17:51:49:581][  info  ][  ScanHelper   ]: ~~~     },
[17:51:49:581][  info  ][  ScanHelper   ]: ~~~     "rxActiveLanes": 1,
[17:51:49:581][  info  ][  ScanHelper   ]: ~~~     "rxPolarity": 0,
[17:51:49:581][  info  ][  ScanHelper   ]: ~~~     "specNum": 0,
[17:51:49:581][  info  ][  ScanHelper   ]: ~~~     "spiConfig": 541200,
[17:51:49:581][  info  ][  ScanHelper   ]: ~~~     "sync": {
[17:51:49:581][  info  ][  ScanHelper   ]: ~~~         "interval": 32,
[17:51:49:581][  info  ][  ScanHelper   ]: ~~~         "word": 2172551550
[17:51:49:581][  info  ][  ScanHelper   ]: ~~~     },
[17:51:49:581][  info  ][  ScanHelper   ]: ~~~     "txPolarity": 0
[17:51:49:581][  info  ][  ScanHelper   ]: ~~~ }
[17:51:49:581][  info  ][    SpecCom    ]: Opening SPEC with id #0
[17:51:49:582][  info  ][    SpecCom    ]: Mapping BARs ...
[17:51:49:582][  info  ][    SpecCom    ]: ... Mapped BAR0 at 0x7fe75eebf000 with size 1048576
[17:51:49:582][warning ][    SpecCom    ]: ... BAR4 not mapped (Mmap failed)
[17:51:49:582][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[17:51:49:582][  info  ][    SpecCom    ]: Firmware Version: 0x4d9ff6d
[17:51:49:582][  info  ][    SpecCom    ]: Firmware Identifier: 0x4030232
[17:51:49:582][  info  ][    SpecCom    ]: FPGA card: PLDA XpressK7 325
[17:51:49:582][  info  ][    SpecCom    ]: FE Chip Type: RD53A/B
[17:51:49:582][  info  ][    SpecCom    ]: FMC Card Type: Ohio Card (Display Port)
[17:51:49:582][  info  ][    SpecCom    ]: RX Speed: 640Mbps
[17:51:49:582][  info  ][    SpecCom    ]: Channel Configuration: 16x1
[17:51:49:582][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[17:51:49:582][  info  ][    SpecCom    ]: Flushing buffers ...
[17:51:49:582][  info  ][    SpecCom    ]: Init success!
[17:51:49:582][  info  ][  scanConsole  ]: #######################
[17:51:49:582][  info  ][  scanConsole  ]: ##  Loading Configs  ##
[17:51:49:582][  info  ][  scanConsole  ]: #######################
[17:51:49:582][  info  ][  scanConsole  ]: Opening global config: configs/CERNQ3/connectivity.json
[17:51:49:582][  info  ][  ScanHelper   ]: Chip type: RD53A
[17:51:49:582][  info  ][  ScanHelper   ]: Chip count 4
[17:51:49:582][  info  ][  ScanHelper   ]: Loading chip #0
[17:51:49:583][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(4)
[17:51:49:583][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ3/20UPGFC0012890.json
[17:51:49:773][  info  ][  ScanHelper   ]: Loading chip #1
[17:51:49:774][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(5)
[17:51:49:774][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ3/20UPGFC0012922.json
[17:51:49:961][  info  ][  ScanHelper   ]: Loading chip #2
[17:51:49:961][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(6)
[17:51:49:961][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ3/20UPGFC0012954.json
[17:51:50:150][  info  ][  ScanHelper   ]: Loading chip #3
[17:51:50:151][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(7)
[17:51:50:151][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ3/20UPGFC0012906.json
[17:51:50:342][  info  ][  scanConsole  ]: #################
[17:51:50:342][  info  ][  scanConsole  ]: # Configure FEs #
[17:51:50:342][  info  ][  scanConsole  ]: #################
[17:51:50:342][  info  ][  scanConsole  ]: Configuring 20UPGFC0012890
[17:51:50:372][  info  ][  scanConsole  ]: Configuring 20UPGFC0012922
[17:51:50:416][  info  ][  scanConsole  ]: Configuring 20UPGFC0012954
[17:51:50:459][  info  ][  scanConsole  ]: Configuring 20UPGFC0012906
[17:51:50:493][  info  ][  scanConsole  ]: Sent configuration to all FEs in 151 ms!
[17:51:50:494][  info  ][  scanConsole  ]: Checking com 20UPGFC0012890
[17:51:50:494][  info  ][    SpecRx     ]: Active Rx channels: 0x10
[17:51:50:494][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[17:51:50:494][  info  ][    SpecRx     ]: Rx Status 0xf0
[17:51:50:494][  info  ][    SpecRx     ]: Number of lanes: 1
[17:51:50:494][  info  ][    SpecRx     ]: Channel 4 Lane 0 synchronized!
[17:51:50:505][  info  ][  scanConsole  ]: ... success!
[17:51:50:505][  info  ][  scanConsole  ]: Checking com 20UPGFC0012922
[17:51:50:505][  info  ][    SpecRx     ]: Active Rx channels: 0x20
[17:51:50:505][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[17:51:50:505][  info  ][    SpecRx     ]: Rx Status 0xf0
[17:51:50:505][  info  ][    SpecRx     ]: Number of lanes: 1
[17:51:50:505][  info  ][    SpecRx     ]: Channel 5 Lane 0 synchronized!
[17:51:50:515][  info  ][  scanConsole  ]: ... success!
[17:51:50:515][  info  ][  scanConsole  ]: Checking com 20UPGFC0012954
[17:51:50:515][  info  ][    SpecRx     ]: Active Rx channels: 0x40
[17:51:50:515][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[17:51:50:515][  info  ][    SpecRx     ]: Rx Status 0xf0
[17:51:50:515][  info  ][    SpecRx     ]: Number of lanes: 1
[17:51:50:515][  info  ][    SpecRx     ]: Channel 6 Lane 0 synchronized!
[17:51:50:526][  info  ][  scanConsole  ]: ... success!
[17:51:50:526][  info  ][  scanConsole  ]: Checking com 20UPGFC0012906
[17:51:50:526][  info  ][    SpecRx     ]: Active Rx channels: 0x80
[17:51:50:526][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[17:51:50:526][  info  ][    SpecRx     ]: Rx Status 0xf0
[17:51:50:526][  info  ][    SpecRx     ]: Number of lanes: 1
[17:51:50:526][  info  ][    SpecRx     ]: Channel 7 Lane 0 synchronized!
[17:51:50:536][  info  ][  scanConsole  ]: ... success!
[17:51:50:536][  info  ][  scanConsole  ]: Enabling Tx channels
[17:51:50:536][  info  ][  scanConsole  ]: Enabling Tx channel 1
[17:51:50:536][  info  ][  scanConsole  ]: Enabling Tx channel 1
[17:51:50:536][  info  ][  scanConsole  ]: Enabling Tx channel 1
[17:51:50:536][  info  ][  scanConsole  ]: Enabling Tx channel 1
[17:51:50:536][  info  ][  scanConsole  ]: Enabling Rx channels
[17:51:50:536][  info  ][  scanConsole  ]: Enabling Rx channel 4
[17:51:50:536][  info  ][  scanConsole  ]: Enabling Rx channel 5
[17:51:50:536][  info  ][  scanConsole  ]: Enabling Rx channel 6
[17:51:50:536][  info  ][  scanConsole  ]: Enabling Rx channel 7
[17:51:50:536][  info  ][  scanConsole  ]: ##############
[17:51:50:537][  info  ][  scanConsole  ]: # Setup Scan #
[17:51:50:537][  info  ][  scanConsole  ]: ##############
[17:51:50:537][  info  ][  scanConsole  ]: Found Scan config, constructing scan ...
[17:51:50:537][  info  ][  ScanFactory  ]: Loading Scan:
[17:51:50:537][  info  ][  ScanFactory  ]:   Name: AnalogScan
[17:51:50:537][  info  ][  ScanFactory  ]:   Number of Loops: 3
[17:51:50:537][  info  ][  ScanFactory  ]:   Loading Loop #0
[17:51:50:537][  info  ][  ScanFactory  ]:    Type: Rd53aReadRegLoop
[17:51:50:538][  info  ][  ScanFactory  ]:    Loading loop config ... 
[17:51:50:538][  info  ][  ScanFactory  ]: ~~~ {
[17:51:50:538][  info  ][  ScanFactory  ]: ~~~     "CurMux": [],
[17:51:50:538][  info  ][  ScanFactory  ]: ~~~     "EnblRingOsc": 0,
[17:51:50:538][  info  ][  ScanFactory  ]: ~~~     "Registers": [],
[17:51:50:538][  info  ][  ScanFactory  ]: ~~~     "RingOscDur": 9,
[17:51:50:538][  info  ][  ScanFactory  ]: ~~~     "RingOscRep": 10,
[17:51:50:538][  info  ][  ScanFactory  ]: ~~~     "VoltMux": [3, 5, 7, 15]
[17:51:50:538][  info  ][  ScanFactory  ]: ~~~ }
[17:51:50:538][  info  ][  ScanFactory  ]:   Loading Loop #1
[17:51:50:538][  info  ][  ScanFactory  ]:    Type: Rd53aTriggerLoop
[17:51:50:538][  info  ][  ScanFactory  ]:    Loading loop config ... 
[17:51:50:538][  info  ][  ScanFactory  ]: ~~~ {
[17:51:50:538][  info  ][  ScanFactory  ]: ~~~     "count": 1,
[17:51:50:538][  info  ][  ScanFactory  ]: ~~~     "delay": 48,
[17:51:50:538][  info  ][  ScanFactory  ]: ~~~     "extTrig": false,
[17:51:50:538][  info  ][  ScanFactory  ]: ~~~     "frequency": 30000.0,
[17:51:50:538][  info  ][  ScanFactory  ]: ~~~     "noInject": false,
[17:51:50:538][  info  ][  ScanFactory  ]: ~~~     "sendEcr": false,
[17:51:50:538][  info  ][  ScanFactory  ]: ~~~     "time": 0.0,
[17:51:50:538][  info  ][  ScanFactory  ]: ~~~     "trigMultiplier": 16
[17:51:50:538][  info  ][  ScanFactory  ]: ~~~ }
[17:51:50:538][  info  ][  ScanFactory  ]:   Loading Loop #2
[17:51:50:538][  info  ][  ScanFactory  ]:    Type: StdDataLoop
[17:51:50:538][warning ][  ScanFactory  ]: ~~~ Config empty.
[17:51:50:538][  info  ][ScanBuildHistogrammers]: Loading histogrammer ...
[17:51:50:539][  info  ][ScanBuildHistogrammers]: ... done!
[17:51:50:539][  info  ][ScanBuildAnalyses]: Loading analyses ...
[17:51:50:539][  info  ][  scanConsole  ]: Running pre scan!
[17:51:50:539][  info  ][  ScanFactory  ]: Entering pre scan phase ...
[17:51:50:539][  info  ][     Rd53a     ]: Write named register: GlobalPulseRt -> 16384
[17:51:50:539][  info  ][     Rd53a     ]: Write named register: InjEnDig -> 0
[17:51:50:539][  info  ][     Rd53a     ]: Write named register: InjVcalDiff -> 0
[17:51:50:539][  info  ][     Rd53a     ]: Write named register: InjVcalHigh -> 3500
[17:51:50:539][  info  ][     Rd53a     ]: Write named register: InjVcalMed -> 500
[17:51:50:539][  info  ][     Rd53a     ]: Write named register: LatencyConfig -> 48
[17:51:50:539][  info  ][  scanConsole  ]: Starting histogrammer and analysis threads:
[17:51:50:540][  info  ][  scanConsole  ]:  .. started threads of Fe 4
[17:51:50:540][  info  ][  scanConsole  ]:  .. started threads of Fe 5
[17:51:50:540][  info  ][  scanConsole  ]:  .. started threads of Fe 6
[17:51:50:540][  info  ][  scanConsole  ]:  .. started threads of Fe 7
[17:51:50:541][  info  ][Rd53aDataProcessor]:   -> Processor thread #0 started!
[17:51:50:541][  info  ][Rd53aDataProcessor]:   -> Processor thread #1 started!
[17:51:50:541][  info  ][Rd53aDataProcessor]:   -> Processor thread #2 started!
[17:51:50:541][  info  ][Rd53aDataProcessor]:   -> Processor thread #3 started!
[17:51:50:541][  info  ][  scanConsole  ]: ########
[17:51:50:541][  info  ][  scanConsole  ]: # Scan #
[17:51:50:541][  info  ][  scanConsole  ]: ########
[17:51:50:541][  info  ][  scanConsole  ]: Starting scan!
[17:51:50:541][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0012890 on Rx 4
[17:51:50:545][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0012890] MON MUX_V: 3 Bias 0 1257 -> 0.265065 V Bias 1 1649 -> 0.343808 V, Temperature 0.0787424 -> -0.845367 C
[17:51:50:549][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0012890] MON MUX_V: 5 Bias 0 1195 -> 0.252611 V Bias 1 1593 -> 0.332559 V, Temperature 0.0799476 -> -24.9286 C
[17:51:50:554][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0012890] MON MUX_V: 7 Bias 0 1193 -> 0.25221 V Bias 1 1597 -> 0.333362 V, Temperature 0.0811529 -> -24.0599 C
[17:51:50:558][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0012890] MON MUX_V: 15 Bias 0 1176 -> 0.248795 V Bias 1 1578 -> 0.329546 V, Temperature 0.0807511 -> -28.2693 C
[17:51:50:593][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0012922 on Rx 5
[17:51:50:597][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0012922] MON MUX_V: 3 Bias 0 1245 -> 0.261 V Bias 1 1639 -> 0.3398 V, Temperature 0.0788 -> 0.260056 C
[17:51:50:601][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0012922] MON MUX_V: 5 Bias 0 1199 -> 0.2518 V Bias 1 1605 -> 0.333 V, Temperature 0.0812 -> -14.4333 C
[17:51:50:605][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0012922] MON MUX_V: 7 Bias 0 1194 -> 0.2508 V Bias 1 1598 -> 0.3316 V, Temperature 0.0808 -> -19.0529 C
[17:51:50:610][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0012922] MON MUX_V: 15 Bias 0 1187 -> 0.2494 V Bias 1 1595 -> 0.331 V, Temperature 0.0816 -> -17.2647 C
[17:51:50:644][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0012954 on Rx 6
[17:51:50:649][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0012954] MON MUX_V: 3 Bias 0 1251 -> 0.261183 V Bias 1 1641 -> 0.339914 V, Temperature 0.0787301 -> -5.38562 C
[17:51:50:653][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0012954] MON MUX_V: 5 Bias 0 1211 -> 0.253109 V Bias 1 1603 -> 0.332242 V, Temperature 0.0791339 -> -23.0466 C
[17:51:50:657][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0012954] MON MUX_V: 7 Bias 0 1187 -> 0.248264 V Bias 1 1590 -> 0.329618 V, Temperature 0.0813544 -> -17.8111 C
[17:51:50:661][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0012954] MON MUX_V: 15 Bias 0 1190 -> 0.248869 V Bias 1 1589 -> 0.329416 V, Temperature 0.080547 -> -26.0867 C
[17:51:50:696][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0012906 on Rx 7
[17:51:50:700][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0012906] MON MUX_V: 3 Bias 0 1242 -> 0.261431 V Bias 1 1632 -> 0.340971 V, Temperature 0.07954 -> 0.376923 C
[17:51:50:705][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0012906] MON MUX_V: 5 Bias 0 1202 -> 0.253273 V Bias 1 1587 -> 0.331793 V, Temperature 0.0785202 -> -10.5182 C
[17:51:50:709][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0012906] MON MUX_V: 7 Bias 0 1202 -> 0.253273 V Bias 1 1595 -> 0.333425 V, Temperature 0.0801518 -> -11.9 C
[17:51:50:713][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0012906] MON MUX_V: 15 Bias 0 1177 -> 0.248174 V Bias 1 1575 -> 0.329346 V, Temperature 0.0811715 -> -13.3476 C
[17:51:50:750][  info  ][  scanConsole  ]: Scan done!
[17:51:50:750][  info  ][  scanConsole  ]: Waiting for processors to finish ...
[17:51:50:751][  info  ][  scanConsole  ]: Processor done, waiting for histogrammer ...
[17:51:51:150][  info  ][HistogramAlgorithm]: Histogrammer done!
[17:51:51:150][  info  ][HistogramAlgorithm]: Histogrammer done!
[17:51:51:150][  info  ][HistogramAlgorithm]: Histogrammer done!
[17:51:51:150][  info  ][HistogramAlgorithm]: Histogrammer done!
[17:51:51:151][  info  ][  scanConsole  ]: Processor done, waiting for analysis ...
[17:51:51:551][  info  ][AnalysisAlgorithm]: Analysis done!
[17:51:51:551][  info  ][AnalysisAlgorithm]: Analysis done!
[17:51:51:551][  info  ][AnalysisAlgorithm]: Analysis done!
[17:51:51:551][  info  ][AnalysisAlgorithm]: Analysis done!
[17:51:51:551][  info  ][  scanConsole  ]: All done!
[17:51:51:551][  info  ][  scanConsole  ]: ##########
[17:51:51:551][  info  ][  scanConsole  ]: # Timing #
[17:51:51:551][  info  ][  scanConsole  ]: ##########
[17:51:51:551][  info  ][  scanConsole  ]: -> Configuration: 151 ms
[17:51:51:551][  info  ][  scanConsole  ]: -> Scan:          208 ms
[17:51:51:551][  info  ][  scanConsole  ]: -> Processing:    0 ms
[17:51:51:551][  info  ][  scanConsole  ]: -> Analysis:      800 ms
[17:51:51:551][  info  ][  scanConsole  ]: ###########
[17:51:51:552][  info  ][  scanConsole  ]: # Cleanup #
[17:51:51:552][  info  ][  scanConsole  ]: ###########
[17:51:51:560][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0012890 to configs/CERNQ3/20UPGFC0012890.json
[17:51:51:770][  info  ][  scanConsole  ]: -> Storing output results of FE 4
[17:51:51:770][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0012890, this usually means that the chip did not send any data at all.
[17:51:51:771][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0012922 to configs/CERNQ3/20UPGFC0012922.json
[17:51:51:938][  info  ][  scanConsole  ]: -> Storing output results of FE 5
[17:51:51:938][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0012922, this usually means that the chip did not send any data at all.
[17:51:51:939][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0012954 to configs/CERNQ3/20UPGFC0012954.json
[17:51:52:106][  info  ][  scanConsole  ]: -> Storing output results of FE 6
[17:51:52:106][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0012954, this usually means that the chip did not send any data at all.
[17:51:52:108][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0012906 to configs/CERNQ3/20UPGFC0012906.json
[17:51:52:278][  info  ][  scanConsole  ]: -> Storing output results of FE 7
[17:51:52:278][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0012906, this usually means that the chip did not send any data at all.
[17:51:52:279][  info  ][  scanConsole  ]: Finishing run: 1591
20UPGFC0012890.json.after
20UPGFC0012890.json.before
20UPGFC0012906.json.after
20UPGFC0012906.json.before
20UPGFC0012922.json.after
20UPGFC0012922.json.before
20UPGFC0012954.json.after
20UPGFC0012954.json.before
reg_readtemp.json
scanLog.json

