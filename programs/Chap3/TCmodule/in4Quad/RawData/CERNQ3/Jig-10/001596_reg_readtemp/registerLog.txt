[18:03:48:077][  info  ][               ]: #####################################
[18:03:48:077][  info  ][               ]: # Welcome to the YARR Scan Console! #
[18:03:48:077][  info  ][               ]: #####################################
[18:03:48:077][  info  ][               ]: -> Parsing command line parameters ...
[18:03:48:077][  info  ][               ]: Configuring logger ...
[18:03:48:080][  info  ][  scanConsole  ]: Scan Type/Config configs/scans/rd53a/reg_readtemp.json
[18:03:48:080][  info  ][  scanConsole  ]: Connectivity:
[18:03:48:080][  info  ][  scanConsole  ]:     configs/CERNQ3/connectivity.json
[18:03:48:080][  info  ][  scanConsole  ]: Target ToT: -1
[18:03:48:080][  info  ][  scanConsole  ]: Target Charge: -1
[18:03:48:080][  info  ][  scanConsole  ]: Output Plots: true
[18:03:48:080][  info  ][  scanConsole  ]: Output Directory: ./data/001596_reg_readtemp/
[18:03:48:085][  info  ][  scanConsole  ]: Timestamp: 2022-08-22_18:03:48
[18:03:48:085][  info  ][  scanConsole  ]: Run Number: 1596
[18:03:48:085][  info  ][  scanConsole  ]: #################
[18:03:48:085][  info  ][  scanConsole  ]: # Init Hardware #
[18:03:48:085][  info  ][  scanConsole  ]: #################
[18:03:48:085][  info  ][  scanConsole  ]: -> Opening controller config: configs/controller/specCfg-rd53a-16x1.json
[18:03:48:085][  info  ][  ScanHelper   ]: Loading controller ...
[18:03:48:085][  info  ][  ScanHelper   ]: Found controller of type: spec
[18:03:48:085][  info  ][  ScanHelper   ]: ... loading controler config:
[18:03:48:085][  info  ][  ScanHelper   ]: ~~~ {
[18:03:48:085][  info  ][  ScanHelper   ]: ~~~     "cmdPeriod": 6.250000073038109e-9,
[18:03:48:085][  info  ][  ScanHelper   ]: ~~~     "idle": {
[18:03:48:085][  info  ][  ScanHelper   ]: ~~~         "word": 1768515945
[18:03:48:085][  info  ][  ScanHelper   ]: ~~~     },
[18:03:48:085][  info  ][  ScanHelper   ]: ~~~     "pulse": {
[18:03:48:085][  info  ][  ScanHelper   ]: ~~~         "interval": 500,
[18:03:48:085][  info  ][  ScanHelper   ]: ~~~         "word": 1549575846
[18:03:48:085][  info  ][  ScanHelper   ]: ~~~     },
[18:03:48:085][  info  ][  ScanHelper   ]: ~~~     "rxActiveLanes": 1,
[18:03:48:085][  info  ][  ScanHelper   ]: ~~~     "rxPolarity": 0,
[18:03:48:085][  info  ][  ScanHelper   ]: ~~~     "specNum": 0,
[18:03:48:085][  info  ][  ScanHelper   ]: ~~~     "spiConfig": 541200,
[18:03:48:085][  info  ][  ScanHelper   ]: ~~~     "sync": {
[18:03:48:085][  info  ][  ScanHelper   ]: ~~~         "interval": 32,
[18:03:48:085][  info  ][  ScanHelper   ]: ~~~         "word": 2172551550
[18:03:48:085][  info  ][  ScanHelper   ]: ~~~     },
[18:03:48:085][  info  ][  ScanHelper   ]: ~~~     "txPolarity": 0
[18:03:48:085][  info  ][  ScanHelper   ]: ~~~ }
[18:03:48:085][  info  ][    SpecCom    ]: Opening SPEC with id #0
[18:03:48:085][  info  ][    SpecCom    ]: Mapping BARs ...
[18:03:48:085][  info  ][    SpecCom    ]: ... Mapped BAR0 at 0x7f01d3b4a000 with size 1048576
[18:03:48:085][warning ][    SpecCom    ]: ... BAR4 not mapped (Mmap failed)
[18:03:48:085][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[18:03:48:085][  info  ][    SpecCom    ]: Firmware Version: 0x4d9ff6d
[18:03:48:085][  info  ][    SpecCom    ]: Firmware Identifier: 0x4030232
[18:03:48:085][  info  ][    SpecCom    ]: FPGA card: PLDA XpressK7 325
[18:03:48:085][  info  ][    SpecCom    ]: FE Chip Type: RD53A/B
[18:03:48:085][  info  ][    SpecCom    ]: FMC Card Type: Ohio Card (Display Port)
[18:03:48:085][  info  ][    SpecCom    ]: RX Speed: 640Mbps
[18:03:48:085][  info  ][    SpecCom    ]: Channel Configuration: 16x1
[18:03:48:085][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[18:03:48:085][  info  ][    SpecCom    ]: Flushing buffers ...
[18:03:48:085][  info  ][    SpecCom    ]: Init success!
[18:03:48:085][  info  ][  scanConsole  ]: #######################
[18:03:48:085][  info  ][  scanConsole  ]: ##  Loading Configs  ##
[18:03:48:085][  info  ][  scanConsole  ]: #######################
[18:03:48:085][  info  ][  scanConsole  ]: Opening global config: configs/CERNQ3/connectivity.json
[18:03:48:085][  info  ][  ScanHelper   ]: Chip type: RD53A
[18:03:48:085][  info  ][  ScanHelper   ]: Chip count 4
[18:03:48:085][  info  ][  ScanHelper   ]: Loading chip #0
[18:03:48:086][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(4)
[18:03:48:086][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ3/20UPGFC0012890.json
[18:03:48:262][  info  ][  ScanHelper   ]: Loading chip #1
[18:03:48:263][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(5)
[18:03:48:263][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ3/20UPGFC0012922.json
[18:03:48:440][  info  ][  ScanHelper   ]: Loading chip #2
[18:03:48:440][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(6)
[18:03:48:440][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ3/20UPGFC0012954.json
[18:03:48:613][  info  ][  ScanHelper   ]: Loading chip #3
[18:03:48:614][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(7)
[18:03:48:614][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ3/20UPGFC0012906.json
[18:03:48:791][  info  ][  scanConsole  ]: #################
[18:03:48:791][  info  ][  scanConsole  ]: # Configure FEs #
[18:03:48:791][  info  ][  scanConsole  ]: #################
[18:03:48:791][  info  ][  scanConsole  ]: Configuring 20UPGFC0012890
[18:03:48:822][  info  ][  scanConsole  ]: Configuring 20UPGFC0012922
[18:03:48:854][  info  ][  scanConsole  ]: Configuring 20UPGFC0012954
[18:03:48:884][  info  ][  scanConsole  ]: Configuring 20UPGFC0012906
[18:03:48:915][  info  ][  scanConsole  ]: Sent configuration to all FEs in 123 ms!
[18:03:48:916][  info  ][  scanConsole  ]: Checking com 20UPGFC0012890
[18:03:48:916][  info  ][    SpecRx     ]: Active Rx channels: 0x10
[18:03:48:916][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[18:03:48:916][  info  ][    SpecRx     ]: Rx Status 0xf0
[18:03:48:916][  info  ][    SpecRx     ]: Number of lanes: 1
[18:03:48:916][  info  ][    SpecRx     ]: Channel 4 Lane 0 synchronized!
[18:03:48:926][  info  ][  scanConsole  ]: ... success!
[18:03:48:926][  info  ][  scanConsole  ]: Checking com 20UPGFC0012922
[18:03:48:926][  info  ][    SpecRx     ]: Active Rx channels: 0x20
[18:03:48:926][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[18:03:48:926][  info  ][    SpecRx     ]: Rx Status 0xf0
[18:03:48:926][  info  ][    SpecRx     ]: Number of lanes: 1
[18:03:48:926][  info  ][    SpecRx     ]: Channel 5 Lane 0 synchronized!
[18:03:48:937][  info  ][  scanConsole  ]: ... success!
[18:03:48:937][  info  ][  scanConsole  ]: Checking com 20UPGFC0012954
[18:03:48:937][  info  ][    SpecRx     ]: Active Rx channels: 0x40
[18:03:48:937][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[18:03:48:937][  info  ][    SpecRx     ]: Rx Status 0xf0
[18:03:48:937][  info  ][    SpecRx     ]: Number of lanes: 1
[18:03:48:937][  info  ][    SpecRx     ]: Channel 6 Lane 0 synchronized!
[18:03:48:947][  info  ][  scanConsole  ]: ... success!
[18:03:48:947][  info  ][  scanConsole  ]: Checking com 20UPGFC0012906
[18:03:48:947][  info  ][    SpecRx     ]: Active Rx channels: 0x80
[18:03:48:947][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[18:03:48:948][  info  ][    SpecRx     ]: Rx Status 0xf0
[18:03:48:948][  info  ][    SpecRx     ]: Number of lanes: 1
[18:03:48:948][  info  ][    SpecRx     ]: Channel 7 Lane 0 synchronized!
[18:03:48:958][  info  ][  scanConsole  ]: ... success!
[18:03:48:958][  info  ][  scanConsole  ]: Enabling Tx channels
[18:03:48:958][  info  ][  scanConsole  ]: Enabling Tx channel 1
[18:03:48:958][  info  ][  scanConsole  ]: Enabling Tx channel 1
[18:03:48:958][  info  ][  scanConsole  ]: Enabling Tx channel 1
[18:03:48:958][  info  ][  scanConsole  ]: Enabling Tx channel 1
[18:03:48:958][  info  ][  scanConsole  ]: Enabling Rx channels
[18:03:48:958][  info  ][  scanConsole  ]: Enabling Rx channel 4
[18:03:48:958][  info  ][  scanConsole  ]: Enabling Rx channel 5
[18:03:48:958][  info  ][  scanConsole  ]: Enabling Rx channel 6
[18:03:48:958][  info  ][  scanConsole  ]: Enabling Rx channel 7
[18:03:48:958][  info  ][  scanConsole  ]: ##############
[18:03:48:958][  info  ][  scanConsole  ]: # Setup Scan #
[18:03:48:958][  info  ][  scanConsole  ]: ##############
[18:03:48:959][  info  ][  scanConsole  ]: Found Scan config, constructing scan ...
[18:03:48:959][  info  ][  ScanFactory  ]: Loading Scan:
[18:03:48:959][  info  ][  ScanFactory  ]:   Name: AnalogScan
[18:03:48:959][  info  ][  ScanFactory  ]:   Number of Loops: 3
[18:03:48:959][  info  ][  ScanFactory  ]:   Loading Loop #0
[18:03:48:959][  info  ][  ScanFactory  ]:    Type: Rd53aReadRegLoop
[18:03:48:959][  info  ][  ScanFactory  ]:    Loading loop config ... 
[18:03:48:959][  info  ][  ScanFactory  ]: ~~~ {
[18:03:48:959][  info  ][  ScanFactory  ]: ~~~     "CurMux": [],
[18:03:48:959][  info  ][  ScanFactory  ]: ~~~     "EnblRingOsc": 0,
[18:03:48:960][  info  ][  ScanFactory  ]: ~~~     "Registers": [],
[18:03:48:960][  info  ][  ScanFactory  ]: ~~~     "RingOscDur": 9,
[18:03:48:960][  info  ][  ScanFactory  ]: ~~~     "RingOscRep": 10,
[18:03:48:960][  info  ][  ScanFactory  ]: ~~~     "VoltMux": [3, 5, 7, 15]
[18:03:48:960][  info  ][  ScanFactory  ]: ~~~ }
[18:03:48:960][  info  ][  ScanFactory  ]:   Loading Loop #1
[18:03:48:960][  info  ][  ScanFactory  ]:    Type: Rd53aTriggerLoop
[18:03:48:960][  info  ][  ScanFactory  ]:    Loading loop config ... 
[18:03:48:960][  info  ][  ScanFactory  ]: ~~~ {
[18:03:48:960][  info  ][  ScanFactory  ]: ~~~     "count": 1,
[18:03:48:960][  info  ][  ScanFactory  ]: ~~~     "delay": 48,
[18:03:48:960][  info  ][  ScanFactory  ]: ~~~     "extTrig": false,
[18:03:48:960][  info  ][  ScanFactory  ]: ~~~     "frequency": 30000.0,
[18:03:48:960][  info  ][  ScanFactory  ]: ~~~     "noInject": false,
[18:03:48:960][  info  ][  ScanFactory  ]: ~~~     "sendEcr": false,
[18:03:48:960][  info  ][  ScanFactory  ]: ~~~     "time": 0.0,
[18:03:48:960][  info  ][  ScanFactory  ]: ~~~     "trigMultiplier": 16
[18:03:48:960][  info  ][  ScanFactory  ]: ~~~ }
[18:03:48:960][  info  ][  ScanFactory  ]:   Loading Loop #2
[18:03:48:960][  info  ][  ScanFactory  ]:    Type: StdDataLoop
[18:03:48:960][warning ][  ScanFactory  ]: ~~~ Config empty.
[18:03:48:960][  info  ][ScanBuildHistogrammers]: Loading histogrammer ...
[18:03:48:960][  info  ][ScanBuildHistogrammers]: ... done!
[18:03:48:960][  info  ][ScanBuildAnalyses]: Loading analyses ...
[18:03:48:961][  info  ][  scanConsole  ]: Running pre scan!
[18:03:48:961][  info  ][  ScanFactory  ]: Entering pre scan phase ...
[18:03:48:961][  info  ][     Rd53a     ]: Write named register: GlobalPulseRt -> 16384
[18:03:48:961][  info  ][     Rd53a     ]: Write named register: InjEnDig -> 0
[18:03:48:961][  info  ][     Rd53a     ]: Write named register: InjVcalDiff -> 0
[18:03:48:961][  info  ][     Rd53a     ]: Write named register: InjVcalHigh -> 3500
[18:03:48:961][  info  ][     Rd53a     ]: Write named register: InjVcalMed -> 500
[18:03:48:961][  info  ][     Rd53a     ]: Write named register: LatencyConfig -> 48
[18:03:48:961][  info  ][  scanConsole  ]: Starting histogrammer and analysis threads:
[18:03:48:961][  info  ][  scanConsole  ]:  .. started threads of Fe 4
[18:03:48:961][  info  ][  scanConsole  ]:  .. started threads of Fe 5
[18:03:48:961][  info  ][  scanConsole  ]:  .. started threads of Fe 6
[18:03:48:962][  info  ][  scanConsole  ]:  .. started threads of Fe 7
[18:03:48:962][  info  ][Rd53aDataProcessor]:   -> Processor thread #0 started!
[18:03:48:962][  info  ][Rd53aDataProcessor]:   -> Processor thread #1 started!
[18:03:48:962][  info  ][Rd53aDataProcessor]:   -> Processor thread #2 started!
[18:03:48:962][  info  ][Rd53aDataProcessor]:   -> Processor thread #3 started!
[18:03:48:962][  info  ][  scanConsole  ]: ########
[18:03:48:962][  info  ][  scanConsole  ]: # Scan #
[18:03:48:962][  info  ][  scanConsole  ]: ########
[18:03:48:962][  info  ][  scanConsole  ]: Starting scan!
[18:03:48:962][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0012890 on Rx 4
[18:03:48:967][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0012890] MON MUX_V: 3 Bias 0 1257 -> 0.265065 V Bias 1 1646 -> 0.343205 V, Temperature 0.0781398 -> -2.91815 C
[18:03:48:971][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0012890] MON MUX_V: 5 Bias 0 1195 -> 0.252611 V Bias 1 1594 -> 0.33276 V, Temperature 0.0801485 -> -23.8429 C
[18:03:48:975][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0012890] MON MUX_V: 7 Bias 0 1192 -> 0.252009 V Bias 1 1597 -> 0.333362 V, Temperature 0.0813537 -> -23.0466 C
[18:03:48:980][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0012890] MON MUX_V: 15 Bias 0 1177 -> 0.248996 V Bias 1 1576 -> 0.329144 V, Temperature 0.0801485 -> -31.777 C
[18:03:49:014][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0012922 on Rx 5
[18:03:49:019][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0012922] MON MUX_V: 3 Bias 0 1245 -> 0.261 V Bias 1 1639 -> 0.3398 V, Temperature 0.0788 -> 0.260056 C
[18:03:49:023][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0012922] MON MUX_V: 5 Bias 0 1198 -> 0.2516 V Bias 1 1605 -> 0.333 V, Temperature 0.0814 -> -13.5889 C
[18:03:49:027][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0012922] MON MUX_V: 7 Bias 0 1193 -> 0.2506 V Bias 1 1597 -> 0.3314 V, Temperature 0.0808 -> -19.0529 C
[18:03:49:032][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0012922] MON MUX_V: 15 Bias 0 1187 -> 0.2494 V Bias 1 1593 -> 0.3306 V, Temperature 0.0812 -> -19.053 C
[18:03:49:067][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0012954 on Rx 6
[18:03:49:071][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0012954] MON MUX_V: 3 Bias 0 1251 -> 0.261183 V Bias 1 1641 -> 0.339914 V, Temperature 0.0787301 -> -5.38562 C
[18:03:49:075][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0012954] MON MUX_V: 5 Bias 0 1211 -> 0.253109 V Bias 1 1603 -> 0.332242 V, Temperature 0.0791339 -> -23.0466 C
[18:03:49:079][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0012954] MON MUX_V: 7 Bias 0 1186 -> 0.248062 V Bias 1 1590 -> 0.329618 V, Temperature 0.0815563 -> -16.9666 C
[18:03:49:084][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0012954] MON MUX_V: 15 Bias 0 1190 -> 0.248869 V Bias 1 1589 -> 0.329416 V, Temperature 0.080547 -> -26.0867 C
[18:03:49:119][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0012906 on Rx 7
[18:03:49:123][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0012906] MON MUX_V: 3 Bias 0 1242 -> 0.261431 V Bias 1 1630 -> 0.340563 V, Temperature 0.0791321 -> -0.792328 C
[18:03:49:127][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0012906] MON MUX_V: 5 Bias 0 1202 -> 0.253273 V Bias 1 1587 -> 0.331793 V, Temperature 0.0785202 -> -10.5182 C
[18:03:49:131][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0012906] MON MUX_V: 7 Bias 0 1203 -> 0.253477 V Bias 1 1595 -> 0.333425 V, Temperature 0.0799479 -> -12.5909 C
[18:03:49:135][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0012906] MON MUX_V: 15 Bias 0 1176 -> 0.247971 V Bias 1 1575 -> 0.329346 V, Temperature 0.0813755 -> -12.6238 C
[18:03:49:172][  info  ][  scanConsole  ]: Scan done!
[18:03:49:172][  info  ][  scanConsole  ]: Waiting for processors to finish ...
[18:03:49:172][  info  ][  scanConsole  ]: Processor done, waiting for histogrammer ...
[18:03:49:572][  info  ][HistogramAlgorithm]: Histogrammer done!
[18:03:49:572][  info  ][HistogramAlgorithm]: Histogrammer done!
[18:03:49:572][  info  ][HistogramAlgorithm]: Histogrammer done!
[18:03:49:572][  info  ][HistogramAlgorithm]: Histogrammer done!
[18:03:49:572][  info  ][  scanConsole  ]: Processor done, waiting for analysis ...
[18:03:49:973][  info  ][AnalysisAlgorithm]: Analysis done!
[18:03:49:973][  info  ][AnalysisAlgorithm]: Analysis done!
[18:03:49:973][  info  ][AnalysisAlgorithm]: Analysis done!
[18:03:49:973][  info  ][AnalysisAlgorithm]: Analysis done!
[18:03:49:973][  info  ][  scanConsole  ]: All done!
[18:03:49:973][  info  ][  scanConsole  ]: ##########
[18:03:49:973][  info  ][  scanConsole  ]: # Timing #
[18:03:49:973][  info  ][  scanConsole  ]: ##########
[18:03:49:973][  info  ][  scanConsole  ]: -> Configuration: 123 ms
[18:03:49:973][  info  ][  scanConsole  ]: -> Scan:          209 ms
[18:03:49:973][  info  ][  scanConsole  ]: -> Processing:    0 ms
[18:03:49:973][  info  ][  scanConsole  ]: -> Analysis:      800 ms
[18:03:49:973][  info  ][  scanConsole  ]: ###########
[18:03:49:973][  info  ][  scanConsole  ]: # Cleanup #
[18:03:49:973][  info  ][  scanConsole  ]: ###########
[18:03:49:976][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0012890 to configs/CERNQ3/20UPGFC0012890.json
[18:03:50:159][  info  ][  scanConsole  ]: -> Storing output results of FE 4
[18:03:50:159][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0012890, this usually means that the chip did not send any data at all.
[18:03:50:161][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0012922 to configs/CERNQ3/20UPGFC0012922.json
[18:03:50:332][  info  ][  scanConsole  ]: -> Storing output results of FE 5
[18:03:50:332][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0012922, this usually means that the chip did not send any data at all.
[18:03:50:333][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0012954 to configs/CERNQ3/20UPGFC0012954.json
[18:03:50:498][  info  ][  scanConsole  ]: -> Storing output results of FE 6
[18:03:50:498][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0012954, this usually means that the chip did not send any data at all.
[18:03:50:499][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0012906 to configs/CERNQ3/20UPGFC0012906.json
[18:03:50:664][  info  ][  scanConsole  ]: -> Storing output results of FE 7
[18:03:50:664][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0012906, this usually means that the chip did not send any data at all.
[18:03:50:665][  info  ][  scanConsole  ]: Finishing run: 1596
20UPGFC0012890.json.after
20UPGFC0012890.json.before
20UPGFC0012906.json.after
20UPGFC0012906.json.before
20UPGFC0012922.json.after
20UPGFC0012922.json.before
20UPGFC0012954.json.after
20UPGFC0012954.json.before
reg_readtemp.json
scanLog.json

