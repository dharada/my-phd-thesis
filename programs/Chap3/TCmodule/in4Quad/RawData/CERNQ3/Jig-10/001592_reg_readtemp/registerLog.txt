[17:55:27:945][  info  ][               ]: #####################################
[17:55:27:945][  info  ][               ]: # Welcome to the YARR Scan Console! #
[17:55:27:945][  info  ][               ]: #####################################
[17:55:27:945][  info  ][               ]: -> Parsing command line parameters ...
[17:55:27:945][  info  ][               ]: Configuring logger ...
[17:55:27:948][  info  ][  scanConsole  ]: Scan Type/Config configs/scans/rd53a/reg_readtemp.json
[17:55:27:948][  info  ][  scanConsole  ]: Connectivity:
[17:55:27:948][  info  ][  scanConsole  ]:     configs/CERNQ3/connectivity.json
[17:55:27:948][  info  ][  scanConsole  ]: Target ToT: -1
[17:55:27:948][  info  ][  scanConsole  ]: Target Charge: -1
[17:55:27:948][  info  ][  scanConsole  ]: Output Plots: true
[17:55:27:948][  info  ][  scanConsole  ]: Output Directory: ./data/001592_reg_readtemp/
[17:55:27:953][  info  ][  scanConsole  ]: Timestamp: 2022-08-22_17:55:27
[17:55:27:953][  info  ][  scanConsole  ]: Run Number: 1592
[17:55:27:953][  info  ][  scanConsole  ]: #################
[17:55:27:953][  info  ][  scanConsole  ]: # Init Hardware #
[17:55:27:953][  info  ][  scanConsole  ]: #################
[17:55:27:953][  info  ][  scanConsole  ]: -> Opening controller config: configs/controller/specCfg-rd53a-16x1.json
[17:55:27:953][  info  ][  ScanHelper   ]: Loading controller ...
[17:55:27:953][  info  ][  ScanHelper   ]: Found controller of type: spec
[17:55:27:953][  info  ][  ScanHelper   ]: ... loading controler config:
[17:55:27:953][  info  ][  ScanHelper   ]: ~~~ {
[17:55:27:953][  info  ][  ScanHelper   ]: ~~~     "cmdPeriod": 6.250000073038109e-9,
[17:55:27:953][  info  ][  ScanHelper   ]: ~~~     "idle": {
[17:55:27:953][  info  ][  ScanHelper   ]: ~~~         "word": 1768515945
[17:55:27:953][  info  ][  ScanHelper   ]: ~~~     },
[17:55:27:953][  info  ][  ScanHelper   ]: ~~~     "pulse": {
[17:55:27:953][  info  ][  ScanHelper   ]: ~~~         "interval": 500,
[17:55:27:953][  info  ][  ScanHelper   ]: ~~~         "word": 1549575846
[17:55:27:953][  info  ][  ScanHelper   ]: ~~~     },
[17:55:27:953][  info  ][  ScanHelper   ]: ~~~     "rxActiveLanes": 1,
[17:55:27:953][  info  ][  ScanHelper   ]: ~~~     "rxPolarity": 0,
[17:55:27:953][  info  ][  ScanHelper   ]: ~~~     "specNum": 0,
[17:55:27:953][  info  ][  ScanHelper   ]: ~~~     "spiConfig": 541200,
[17:55:27:953][  info  ][  ScanHelper   ]: ~~~     "sync": {
[17:55:27:953][  info  ][  ScanHelper   ]: ~~~         "interval": 32,
[17:55:27:953][  info  ][  ScanHelper   ]: ~~~         "word": 2172551550
[17:55:27:953][  info  ][  ScanHelper   ]: ~~~     },
[17:55:27:953][  info  ][  ScanHelper   ]: ~~~     "txPolarity": 0
[17:55:27:953][  info  ][  ScanHelper   ]: ~~~ }
[17:55:27:953][  info  ][    SpecCom    ]: Opening SPEC with id #0
[17:55:27:953][  info  ][    SpecCom    ]: Mapping BARs ...
[17:55:27:953][  info  ][    SpecCom    ]: ... Mapped BAR0 at 0x7f1dca314000 with size 1048576
[17:55:27:953][warning ][    SpecCom    ]: ... BAR4 not mapped (Mmap failed)
[17:55:27:953][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[17:55:27:953][  info  ][    SpecCom    ]: Firmware Version: 0x4d9ff6d
[17:55:27:953][  info  ][    SpecCom    ]: Firmware Identifier: 0x4030232
[17:55:27:953][  info  ][    SpecCom    ]: FPGA card: PLDA XpressK7 325
[17:55:27:953][  info  ][    SpecCom    ]: FE Chip Type: RD53A/B
[17:55:27:953][  info  ][    SpecCom    ]: FMC Card Type: Ohio Card (Display Port)
[17:55:27:953][  info  ][    SpecCom    ]: RX Speed: 640Mbps
[17:55:27:953][  info  ][    SpecCom    ]: Channel Configuration: 16x1
[17:55:27:953][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[17:55:27:953][  info  ][    SpecCom    ]: Flushing buffers ...
[17:55:27:953][  info  ][    SpecCom    ]: Init success!
[17:55:27:953][  info  ][  scanConsole  ]: #######################
[17:55:27:953][  info  ][  scanConsole  ]: ##  Loading Configs  ##
[17:55:27:953][  info  ][  scanConsole  ]: #######################
[17:55:27:953][  info  ][  scanConsole  ]: Opening global config: configs/CERNQ3/connectivity.json
[17:55:27:953][  info  ][  ScanHelper   ]: Chip type: RD53A
[17:55:27:953][  info  ][  ScanHelper   ]: Chip count 4
[17:55:27:953][  info  ][  ScanHelper   ]: Loading chip #0
[17:55:27:954][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(4)
[17:55:27:954][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ3/20UPGFC0012890.json
[17:55:28:127][  info  ][  ScanHelper   ]: Loading chip #1
[17:55:28:128][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(5)
[17:55:28:128][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ3/20UPGFC0012922.json
[17:55:28:299][  info  ][  ScanHelper   ]: Loading chip #2
[17:55:28:299][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(6)
[17:55:28:299][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ3/20UPGFC0012954.json
[17:55:28:469][  info  ][  ScanHelper   ]: Loading chip #3
[17:55:28:470][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(7)
[17:55:28:470][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ3/20UPGFC0012906.json
[17:55:28:644][  info  ][  scanConsole  ]: #################
[17:55:28:644][  info  ][  scanConsole  ]: # Configure FEs #
[17:55:28:644][  info  ][  scanConsole  ]: #################
[17:55:28:644][  info  ][  scanConsole  ]: Configuring 20UPGFC0012890
[17:55:28:674][  info  ][  scanConsole  ]: Configuring 20UPGFC0012922
[17:55:28:705][  info  ][  scanConsole  ]: Configuring 20UPGFC0012954
[17:55:28:735][  info  ][  scanConsole  ]: Configuring 20UPGFC0012906
[17:55:28:765][  info  ][  scanConsole  ]: Sent configuration to all FEs in 120 ms!
[17:55:28:766][  info  ][  scanConsole  ]: Checking com 20UPGFC0012890
[17:55:28:766][  info  ][    SpecRx     ]: Active Rx channels: 0x10
[17:55:28:766][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[17:55:28:766][  info  ][    SpecRx     ]: Rx Status 0xf0
[17:55:28:766][  info  ][    SpecRx     ]: Number of lanes: 1
[17:55:28:766][  info  ][    SpecRx     ]: Channel 4 Lane 0 synchronized!
[17:55:28:776][  info  ][  scanConsole  ]: ... success!
[17:55:28:776][  info  ][  scanConsole  ]: Checking com 20UPGFC0012922
[17:55:28:776][  info  ][    SpecRx     ]: Active Rx channels: 0x20
[17:55:28:776][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[17:55:28:776][  info  ][    SpecRx     ]: Rx Status 0xf0
[17:55:28:776][  info  ][    SpecRx     ]: Number of lanes: 1
[17:55:28:776][  info  ][    SpecRx     ]: Channel 5 Lane 0 synchronized!
[17:55:28:787][  info  ][  scanConsole  ]: ... success!
[17:55:28:787][  info  ][  scanConsole  ]: Checking com 20UPGFC0012954
[17:55:28:787][  info  ][    SpecRx     ]: Active Rx channels: 0x40
[17:55:28:787][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[17:55:28:787][  info  ][    SpecRx     ]: Rx Status 0xf0
[17:55:28:787][  info  ][    SpecRx     ]: Number of lanes: 1
[17:55:28:787][  info  ][    SpecRx     ]: Channel 6 Lane 0 synchronized!
[17:55:28:797][  info  ][  scanConsole  ]: ... success!
[17:55:28:797][  info  ][  scanConsole  ]: Checking com 20UPGFC0012906
[17:55:28:797][  info  ][    SpecRx     ]: Active Rx channels: 0x80
[17:55:28:797][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[17:55:28:797][  info  ][    SpecRx     ]: Rx Status 0xf0
[17:55:28:797][  info  ][    SpecRx     ]: Number of lanes: 1
[17:55:28:798][  info  ][    SpecRx     ]: Channel 7 Lane 0 synchronized!
[17:55:28:808][  info  ][  scanConsole  ]: ... success!
[17:55:28:808][  info  ][  scanConsole  ]: Enabling Tx channels
[17:55:28:808][  info  ][  scanConsole  ]: Enabling Tx channel 1
[17:55:28:808][  info  ][  scanConsole  ]: Enabling Tx channel 1
[17:55:28:808][  info  ][  scanConsole  ]: Enabling Tx channel 1
[17:55:28:808][  info  ][  scanConsole  ]: Enabling Tx channel 1
[17:55:28:808][  info  ][  scanConsole  ]: Enabling Rx channels
[17:55:28:808][  info  ][  scanConsole  ]: Enabling Rx channel 4
[17:55:28:808][  info  ][  scanConsole  ]: Enabling Rx channel 5
[17:55:28:808][  info  ][  scanConsole  ]: Enabling Rx channel 6
[17:55:28:808][  info  ][  scanConsole  ]: Enabling Rx channel 7
[17:55:28:808][  info  ][  scanConsole  ]: ##############
[17:55:28:808][  info  ][  scanConsole  ]: # Setup Scan #
[17:55:28:808][  info  ][  scanConsole  ]: ##############
[17:55:28:809][  info  ][  scanConsole  ]: Found Scan config, constructing scan ...
[17:55:28:809][  info  ][  ScanFactory  ]: Loading Scan:
[17:55:28:809][  info  ][  ScanFactory  ]:   Name: AnalogScan
[17:55:28:809][  info  ][  ScanFactory  ]:   Number of Loops: 3
[17:55:28:809][  info  ][  ScanFactory  ]:   Loading Loop #0
[17:55:28:809][  info  ][  ScanFactory  ]:    Type: Rd53aReadRegLoop
[17:55:28:809][  info  ][  ScanFactory  ]:    Loading loop config ... 
[17:55:28:809][  info  ][  ScanFactory  ]: ~~~ {
[17:55:28:809][  info  ][  ScanFactory  ]: ~~~     "CurMux": [],
[17:55:28:809][  info  ][  ScanFactory  ]: ~~~     "EnblRingOsc": 0,
[17:55:28:810][  info  ][  ScanFactory  ]: ~~~     "Registers": [],
[17:55:28:810][  info  ][  ScanFactory  ]: ~~~     "RingOscDur": 9,
[17:55:28:810][  info  ][  ScanFactory  ]: ~~~     "RingOscRep": 10,
[17:55:28:810][  info  ][  ScanFactory  ]: ~~~     "VoltMux": [3, 5, 7, 15]
[17:55:28:810][  info  ][  ScanFactory  ]: ~~~ }
[17:55:28:810][  info  ][  ScanFactory  ]:   Loading Loop #1
[17:55:28:810][  info  ][  ScanFactory  ]:    Type: Rd53aTriggerLoop
[17:55:28:810][  info  ][  ScanFactory  ]:    Loading loop config ... 
[17:55:28:810][  info  ][  ScanFactory  ]: ~~~ {
[17:55:28:810][  info  ][  ScanFactory  ]: ~~~     "count": 1,
[17:55:28:810][  info  ][  ScanFactory  ]: ~~~     "delay": 48,
[17:55:28:810][  info  ][  ScanFactory  ]: ~~~     "extTrig": false,
[17:55:28:810][  info  ][  ScanFactory  ]: ~~~     "frequency": 30000.0,
[17:55:28:810][  info  ][  ScanFactory  ]: ~~~     "noInject": false,
[17:55:28:810][  info  ][  ScanFactory  ]: ~~~     "sendEcr": false,
[17:55:28:810][  info  ][  ScanFactory  ]: ~~~     "time": 0.0,
[17:55:28:810][  info  ][  ScanFactory  ]: ~~~     "trigMultiplier": 16
[17:55:28:810][  info  ][  ScanFactory  ]: ~~~ }
[17:55:28:810][  info  ][  ScanFactory  ]:   Loading Loop #2
[17:55:28:810][  info  ][  ScanFactory  ]:    Type: StdDataLoop
[17:55:28:810][warning ][  ScanFactory  ]: ~~~ Config empty.
[17:55:28:810][  info  ][ScanBuildHistogrammers]: Loading histogrammer ...
[17:55:28:810][  info  ][ScanBuildHistogrammers]: ... done!
[17:55:28:810][  info  ][ScanBuildAnalyses]: Loading analyses ...
[17:55:28:811][  info  ][  scanConsole  ]: Running pre scan!
[17:55:28:811][  info  ][  ScanFactory  ]: Entering pre scan phase ...
[17:55:28:811][  info  ][     Rd53a     ]: Write named register: GlobalPulseRt -> 16384
[17:55:28:811][  info  ][     Rd53a     ]: Write named register: InjEnDig -> 0
[17:55:28:811][  info  ][     Rd53a     ]: Write named register: InjVcalDiff -> 0
[17:55:28:811][  info  ][     Rd53a     ]: Write named register: InjVcalHigh -> 3500
[17:55:28:811][  info  ][     Rd53a     ]: Write named register: InjVcalMed -> 500
[17:55:28:811][  info  ][     Rd53a     ]: Write named register: LatencyConfig -> 48
[17:55:28:811][  info  ][  scanConsole  ]: Starting histogrammer and analysis threads:
[17:55:28:811][  info  ][  scanConsole  ]:  .. started threads of Fe 4
[17:55:28:811][  info  ][  scanConsole  ]:  .. started threads of Fe 5
[17:55:28:812][  info  ][  scanConsole  ]:  .. started threads of Fe 6
[17:55:28:812][  info  ][  scanConsole  ]:  .. started threads of Fe 7
[17:55:28:812][  info  ][Rd53aDataProcessor]:   -> Processor thread #0 started!
[17:55:28:812][  info  ][Rd53aDataProcessor]:   -> Processor thread #1 started!
[17:55:28:812][  info  ][Rd53aDataProcessor]:   -> Processor thread #2 started!
[17:55:28:812][  info  ][Rd53aDataProcessor]:   -> Processor thread #3 started!
[17:55:28:812][  info  ][  scanConsole  ]: ########
[17:55:28:812][  info  ][  scanConsole  ]: # Scan #
[17:55:28:812][  info  ][  scanConsole  ]: ########
[17:55:28:812][  info  ][  scanConsole  ]: Starting scan!
[17:55:28:812][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0012890 on Rx 4
[17:55:28:816][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0012890] MON MUX_V: 3 Bias 0 1257 -> 0.265065 V Bias 1 1648 -> 0.343607 V, Temperature 0.0785415 -> -1.53625 C
[17:55:28:821][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0012890] MON MUX_V: 5 Bias 0 1195 -> 0.252611 V Bias 1 1595 -> 0.332961 V, Temperature 0.0803493 -> -22.7573 C
[17:55:28:825][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0012890] MON MUX_V: 7 Bias 0 1192 -> 0.252009 V Bias 1 1597 -> 0.333362 V, Temperature 0.0813537 -> -23.0466 C
[17:55:28:830][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0012890] MON MUX_V: 15 Bias 0 1177 -> 0.248996 V Bias 1 1578 -> 0.329546 V, Temperature 0.0805502 -> -29.4386 C
[17:55:28:864][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0012922 on Rx 5
[17:55:28:869][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0012922] MON MUX_V: 3 Bias 0 1244 -> 0.2608 V Bias 1 1640 -> 0.34 V, Temperature 0.0792 -> 1.47609 C
[17:55:28:873][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0012922] MON MUX_V: 5 Bias 0 1199 -> 0.2518 V Bias 1 1604 -> 0.3328 V, Temperature 0.081 -> -15.2776 C
[17:55:28:877][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0012922] MON MUX_V: 7 Bias 0 1194 -> 0.2508 V Bias 1 1598 -> 0.3316 V, Temperature 0.0808 -> -19.0529 C
[17:55:28:881][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0012922] MON MUX_V: 15 Bias 0 1188 -> 0.2496 V Bias 1 1595 -> 0.331 V, Temperature 0.0814 -> -18.1588 C
[17:55:28:916][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0012954 on Rx 6
[17:55:28:920][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0012954] MON MUX_V: 3 Bias 0 1251 -> 0.261183 V Bias 1 1641 -> 0.339914 V, Temperature 0.0787301 -> -5.38562 C
[17:55:28:924][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0012954] MON MUX_V: 5 Bias 0 1212 -> 0.25331 V Bias 1 1602 -> 0.332041 V, Temperature 0.0787301 -> -25.0732 C
[17:55:28:929][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0012954] MON MUX_V: 7 Bias 0 1187 -> 0.248264 V Bias 1 1590 -> 0.329618 V, Temperature 0.0813544 -> -17.8111 C
[17:55:28:933][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0012954] MON MUX_V: 15 Bias 0 1190 -> 0.248869 V Bias 1 1590 -> 0.329618 V, Temperature 0.0807488 -> -25.0734 C
[17:55:28:968][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0012906 on Rx 7
[17:55:28:972][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0012906] MON MUX_V: 3 Bias 0 1243 -> 0.261635 V Bias 1 1632 -> 0.340971 V, Temperature 0.079336 -> -0.207657 C
[17:55:28:976][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0012906] MON MUX_V: 5 Bias 0 1202 -> 0.253273 V Bias 1 1587 -> 0.331793 V, Temperature 0.0785202 -> -10.5182 C
[17:55:28:981][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0012906] MON MUX_V: 7 Bias 0 1203 -> 0.253477 V Bias 1 1595 -> 0.333425 V, Temperature 0.0799479 -> -12.5909 C
[17:55:28:985][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0012906] MON MUX_V: 15 Bias 0 1176 -> 0.247971 V Bias 1 1576 -> 0.32955 V, Temperature 0.0815795 -> -11.8999 C
[17:55:29:021][  info  ][  scanConsole  ]: Scan done!
[17:55:29:022][  info  ][  scanConsole  ]: Waiting for processors to finish ...
[17:55:29:022][  info  ][  scanConsole  ]: Processor done, waiting for histogrammer ...
[17:55:29:422][  info  ][HistogramAlgorithm]: Histogrammer done!
[17:55:29:422][  info  ][HistogramAlgorithm]: Histogrammer done!
[17:55:29:422][  info  ][HistogramAlgorithm]: Histogrammer done!
[17:55:29:422][  info  ][HistogramAlgorithm]: Histogrammer done!
[17:55:29:422][  info  ][  scanConsole  ]: Processor done, waiting for analysis ...
[17:55:29:822][  info  ][AnalysisAlgorithm]: Analysis done!
[17:55:29:822][  info  ][AnalysisAlgorithm]: Analysis done!
[17:55:29:822][  info  ][AnalysisAlgorithm]: Analysis done!
[17:55:29:822][  info  ][AnalysisAlgorithm]: Analysis done!
[17:55:29:823][  info  ][  scanConsole  ]: All done!
[17:55:29:823][  info  ][  scanConsole  ]: ##########
[17:55:29:823][  info  ][  scanConsole  ]: # Timing #
[17:55:29:823][  info  ][  scanConsole  ]: ##########
[17:55:29:823][  info  ][  scanConsole  ]: -> Configuration: 120 ms
[17:55:29:823][  info  ][  scanConsole  ]: -> Scan:          209 ms
[17:55:29:823][  info  ][  scanConsole  ]: -> Processing:    0 ms
[17:55:29:823][  info  ][  scanConsole  ]: -> Analysis:      800 ms
[17:55:29:823][  info  ][  scanConsole  ]: ###########
[17:55:29:823][  info  ][  scanConsole  ]: # Cleanup #
[17:55:29:823][  info  ][  scanConsole  ]: ###########
[17:55:29:826][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0012890 to configs/CERNQ3/20UPGFC0012890.json
[17:55:30:010][  info  ][  scanConsole  ]: -> Storing output results of FE 4
[17:55:30:010][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0012890, this usually means that the chip did not send any data at all.
[17:55:30:012][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0012922 to configs/CERNQ3/20UPGFC0012922.json
[17:55:30:189][  info  ][  scanConsole  ]: -> Storing output results of FE 5
[17:55:30:189][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0012922, this usually means that the chip did not send any data at all.
[17:55:30:192][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0012954 to configs/CERNQ3/20UPGFC0012954.json
[17:55:30:361][  info  ][  scanConsole  ]: -> Storing output results of FE 6
[17:55:30:361][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0012954, this usually means that the chip did not send any data at all.
[17:55:30:362][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0012906 to configs/CERNQ3/20UPGFC0012906.json
[17:55:30:527][  info  ][  scanConsole  ]: -> Storing output results of FE 7
[17:55:30:527][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0012906, this usually means that the chip did not send any data at all.
[17:55:30:529][  info  ][  scanConsole  ]: Finishing run: 1592
20UPGFC0012890.json.after
20UPGFC0012890.json.before
20UPGFC0012906.json.after
20UPGFC0012906.json.before
20UPGFC0012922.json.after
20UPGFC0012922.json.before
20UPGFC0012954.json.after
20UPGFC0012954.json.before
reg_readtemp.json
scanLog.json

