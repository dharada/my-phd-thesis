[15:51:28:158][  info  ][               ]: #####################################
[15:51:28:158][  info  ][               ]: # Welcome to the YARR Scan Console! #
[15:51:28:158][  info  ][               ]: #####################################
[15:51:28:158][  info  ][               ]: -> Parsing command line parameters ...
[15:51:28:158][  info  ][               ]: Configuring logger ...
[15:51:28:167][  info  ][  scanConsole  ]: Scan Type/Config configs/scans/rd53a/reg_readtemp.json
[15:51:28:167][  info  ][  scanConsole  ]: Connectivity:
[15:51:28:167][  info  ][  scanConsole  ]:     configs/CERNQ6/example_rd53a_setup.json
[15:51:28:167][  info  ][  scanConsole  ]: Target ToT: -1
[15:51:28:167][  info  ][  scanConsole  ]: Target Charge: -1
[15:51:28:167][  info  ][  scanConsole  ]: Output Plots: false
[15:51:28:167][  info  ][  scanConsole  ]: Output Directory: ./data/001511_reg_readtemp/
[15:51:28:185][  info  ][  scanConsole  ]: Timestamp: 2022-08-20_15:51:28
[15:51:28:185][  info  ][  scanConsole  ]: Run Number: 1511
[15:51:28:185][  info  ][  scanConsole  ]: #################
[15:51:28:185][  info  ][  scanConsole  ]: # Init Hardware #
[15:51:28:185][  info  ][  scanConsole  ]: #################
[15:51:28:186][  info  ][  scanConsole  ]: -> Opening controller config: configs/controller/specCfg-rd53a-16x1.json
[15:51:28:186][  info  ][  ScanHelper   ]: Loading controller ...
[15:51:28:186][  info  ][  ScanHelper   ]: Found controller of type: spec
[15:51:28:186][  info  ][  ScanHelper   ]: ... loading controler config:
[15:51:28:186][  info  ][  ScanHelper   ]: ~~~ {
[15:51:28:186][  info  ][  ScanHelper   ]: ~~~     "cmdPeriod": 6.250000073038109e-9,
[15:51:28:186][  info  ][  ScanHelper   ]: ~~~     "idle": {
[15:51:28:186][  info  ][  ScanHelper   ]: ~~~         "word": 1768515945
[15:51:28:186][  info  ][  ScanHelper   ]: ~~~     },
[15:51:28:186][  info  ][  ScanHelper   ]: ~~~     "pulse": {
[15:51:28:186][  info  ][  ScanHelper   ]: ~~~         "interval": 500,
[15:51:28:186][  info  ][  ScanHelper   ]: ~~~         "word": 1549575846
[15:51:28:186][  info  ][  ScanHelper   ]: ~~~     },
[15:51:28:186][  info  ][  ScanHelper   ]: ~~~     "rxActiveLanes": 1,
[15:51:28:186][  info  ][  ScanHelper   ]: ~~~     "rxPolarity": 0,
[15:51:28:186][  info  ][  ScanHelper   ]: ~~~     "specNum": 0,
[15:51:28:186][  info  ][  ScanHelper   ]: ~~~     "spiConfig": 541200,
[15:51:28:186][  info  ][  ScanHelper   ]: ~~~     "sync": {
[15:51:28:187][  info  ][  ScanHelper   ]: ~~~         "interval": 32,
[15:51:28:187][  info  ][  ScanHelper   ]: ~~~         "word": 2172551550
[15:51:28:187][  info  ][  ScanHelper   ]: ~~~     },
[15:51:28:187][  info  ][  ScanHelper   ]: ~~~     "txPolarity": 0
[15:51:28:187][  info  ][  ScanHelper   ]: ~~~ }
[15:51:28:187][  info  ][    SpecCom    ]: Opening SPEC with id #0
[15:51:28:187][  info  ][    SpecCom    ]: Mapping BARs ...
[15:51:28:187][  info  ][    SpecCom    ]: ... Mapped BAR0 at 0x7f766ff58000 with size 1048576
[15:51:28:187][warning ][    SpecCom    ]: ... BAR4 not mapped (Mmap failed)
[15:51:28:187][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[15:51:28:187][  info  ][    SpecCom    ]: Firmware Version: 0x4d9ff6d
[15:51:28:187][  info  ][    SpecCom    ]: Firmware Identifier: 0x4030232
[15:51:28:187][  info  ][    SpecCom    ]: FPGA card: PLDA XpressK7 325
[15:51:28:187][  info  ][    SpecCom    ]: FE Chip Type: RD53A/B
[15:51:28:187][  info  ][    SpecCom    ]: FMC Card Type: Ohio Card (Display Port)
[15:51:28:187][  info  ][    SpecCom    ]: RX Speed: 640Mbps
[15:51:28:187][  info  ][    SpecCom    ]: Channel Configuration: 16x1
[15:51:28:187][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[15:51:28:188][  info  ][    SpecCom    ]: Flushing buffers ...
[15:51:28:188][  info  ][    SpecCom    ]: Init success!
[15:51:28:188][  info  ][  scanConsole  ]: #######################
[15:51:28:188][  info  ][  scanConsole  ]: ##  Loading Configs  ##
[15:51:28:188][  info  ][  scanConsole  ]: #######################
[15:51:28:188][  info  ][  scanConsole  ]: Opening global config: configs/CERNQ6/example_rd53a_setup.json
[15:51:28:188][  info  ][  ScanHelper   ]: Chip type: RD53A
[15:51:28:188][  info  ][  ScanHelper   ]: Chip count 4
[15:51:28:188][  info  ][  ScanHelper   ]: Loading chip #0
[15:51:28:191][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(4)
[15:51:28:191][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ6/20UPGFC0010055.json
[15:51:28:401][  info  ][  ScanHelper   ]: Loading chip #1
[15:51:28:401][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(5)
[15:51:28:401][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ6/20UPGFC0010103.json
[15:51:28:573][  info  ][  ScanHelper   ]: Loading chip #2
[15:51:28:574][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(6)
[15:51:28:574][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ6/20UPGFC0010119.json
[15:51:28:752][  info  ][  ScanHelper   ]: Loading chip #3
[15:51:28:753][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(7)
[15:51:28:753][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ6/20UPGFC0010087.json
[15:51:28:951][  info  ][  scanConsole  ]: #################
[15:51:28:951][  info  ][  scanConsole  ]: # Configure FEs #
[15:51:28:951][  info  ][  scanConsole  ]: #################
[15:51:28:951][  info  ][  scanConsole  ]: Configuring 20UPGFC0010055
[15:51:28:982][  info  ][  scanConsole  ]: Configuring 20UPGFC0010103
[15:51:29:013][  info  ][  scanConsole  ]: Configuring 20UPGFC0010119
[15:51:29:046][  info  ][  scanConsole  ]: Configuring 20UPGFC0010087
[15:51:29:077][  info  ][  scanConsole  ]: Sent configuration to all FEs in 125 ms!
[15:51:29:078][  info  ][  scanConsole  ]: Checking com 20UPGFC0010055
[15:51:29:078][  info  ][    SpecRx     ]: Active Rx channels: 0x10
[15:51:29:078][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[15:51:29:078][  info  ][    SpecRx     ]: Rx Status 0xf0
[15:51:29:078][  info  ][    SpecRx     ]: Number of lanes: 1
[15:51:29:078][  info  ][    SpecRx     ]: Channel 4 Lane 0 synchronized!
[15:51:29:088][  info  ][  scanConsole  ]: ... success!
[15:51:29:088][  info  ][  scanConsole  ]: Checking com 20UPGFC0010103
[15:51:29:088][  info  ][    SpecRx     ]: Active Rx channels: 0x20
[15:51:29:088][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[15:51:29:088][  info  ][    SpecRx     ]: Rx Status 0xf0
[15:51:29:088][  info  ][    SpecRx     ]: Number of lanes: 1
[15:51:29:088][  info  ][    SpecRx     ]: Channel 5 Lane 0 synchronized!
[15:51:29:098][  info  ][  scanConsole  ]: ... success!
[15:51:29:098][  info  ][  scanConsole  ]: Checking com 20UPGFC0010119
[15:51:29:098][  info  ][    SpecRx     ]: Active Rx channels: 0x40
[15:51:29:098][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[15:51:29:098][  info  ][    SpecRx     ]: Rx Status 0xf0
[15:51:29:098][  info  ][    SpecRx     ]: Number of lanes: 1
[15:51:29:098][  info  ][    SpecRx     ]: Channel 6 Lane 0 synchronized!
[15:51:29:109][  info  ][  scanConsole  ]: ... success!
[15:51:29:109][  info  ][  scanConsole  ]: Checking com 20UPGFC0010087
[15:51:29:109][  info  ][    SpecRx     ]: Active Rx channels: 0x80
[15:51:29:109][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[15:51:29:109][  info  ][    SpecRx     ]: Rx Status 0xf0
[15:51:29:109][  info  ][    SpecRx     ]: Number of lanes: 1
[15:51:29:109][  info  ][    SpecRx     ]: Channel 7 Lane 0 synchronized!
[15:51:29:119][  info  ][  scanConsole  ]: ... success!
[15:51:29:119][  info  ][  scanConsole  ]: Enabling Tx channels
[15:51:29:119][  info  ][  scanConsole  ]: Enabling Tx channel 1
[15:51:29:119][  info  ][  scanConsole  ]: Enabling Tx channel 1
[15:51:29:119][  info  ][  scanConsole  ]: Enabling Tx channel 1
[15:51:29:119][  info  ][  scanConsole  ]: Enabling Tx channel 1
[15:51:29:119][  info  ][  scanConsole  ]: Enabling Rx channels
[15:51:29:119][  info  ][  scanConsole  ]: Enabling Rx channel 4
[15:51:29:119][  info  ][  scanConsole  ]: Enabling Rx channel 5
[15:51:29:119][  info  ][  scanConsole  ]: Enabling Rx channel 6
[15:51:29:119][  info  ][  scanConsole  ]: Enabling Rx channel 7
[15:51:29:119][  info  ][  scanConsole  ]: ##############
[15:51:29:119][  info  ][  scanConsole  ]: # Setup Scan #
[15:51:29:119][  info  ][  scanConsole  ]: ##############
[15:51:29:119][  info  ][  scanConsole  ]: Found Scan config, constructing scan ...
[15:51:29:119][  info  ][  ScanFactory  ]: Loading Scan:
[15:51:29:119][  info  ][  ScanFactory  ]:   Name: AnalogScan
[15:51:29:119][  info  ][  ScanFactory  ]:   Number of Loops: 3
[15:51:29:119][  info  ][  ScanFactory  ]:   Loading Loop #0
[15:51:29:119][  info  ][  ScanFactory  ]:    Type: Rd53aReadRegLoop
[15:51:29:119][  info  ][  ScanFactory  ]:    Loading loop config ... 
[15:51:29:119][  info  ][  ScanFactory  ]: ~~~ {
[15:51:29:119][  info  ][  ScanFactory  ]: ~~~     "CurMux": [],
[15:51:29:119][  info  ][  ScanFactory  ]: ~~~     "EnblRingOsc": 0,
[15:51:29:119][  info  ][  ScanFactory  ]: ~~~     "Registers": [],
[15:51:29:119][  info  ][  ScanFactory  ]: ~~~     "RingOscDur": 9,
[15:51:29:119][  info  ][  ScanFactory  ]: ~~~     "RingOscRep": 10,
[15:51:29:119][  info  ][  ScanFactory  ]: ~~~     "VoltMux": [3, 5, 7, 15]
[15:51:29:119][  info  ][  ScanFactory  ]: ~~~ }
[15:51:29:119][  info  ][  ScanFactory  ]:   Loading Loop #1
[15:51:29:119][  info  ][  ScanFactory  ]:    Type: Rd53aTriggerLoop
[15:51:29:119][  info  ][  ScanFactory  ]:    Loading loop config ... 
[15:51:29:119][  info  ][  ScanFactory  ]: ~~~ {
[15:51:29:119][  info  ][  ScanFactory  ]: ~~~     "count": 1,
[15:51:29:119][  info  ][  ScanFactory  ]: ~~~     "delay": 48,
[15:51:29:119][  info  ][  ScanFactory  ]: ~~~     "extTrig": false,
[15:51:29:119][  info  ][  ScanFactory  ]: ~~~     "frequency": 30000.0,
[15:51:29:119][  info  ][  ScanFactory  ]: ~~~     "noInject": false,
[15:51:29:119][  info  ][  ScanFactory  ]: ~~~     "sendEcr": false,
[15:51:29:119][  info  ][  ScanFactory  ]: ~~~     "time": 0.0,
[15:51:29:119][  info  ][  ScanFactory  ]: ~~~     "trigMultiplier": 16
[15:51:29:119][  info  ][  ScanFactory  ]: ~~~ }
[15:51:29:119][  info  ][  ScanFactory  ]:   Loading Loop #2
[15:51:29:119][  info  ][  ScanFactory  ]:    Type: StdDataLoop
[15:51:29:119][warning ][  ScanFactory  ]: ~~~ Config empty.
[15:51:29:119][  info  ][ScanBuildHistogrammers]: Loading histogrammer ...
[15:51:29:119][  info  ][ScanBuildHistogrammers]: ... done!
[15:51:29:119][  info  ][ScanBuildAnalyses]: Loading analyses ...
[15:51:29:120][  info  ][  scanConsole  ]: Running pre scan!
[15:51:29:120][  info  ][  ScanFactory  ]: Entering pre scan phase ...
[15:51:29:120][  info  ][     Rd53a     ]: Write named register: GlobalPulseRt -> 16384
[15:51:29:120][  info  ][     Rd53a     ]: Write named register: InjEnDig -> 0
[15:51:29:120][  info  ][     Rd53a     ]: Write named register: InjVcalDiff -> 0
[15:51:29:120][  info  ][     Rd53a     ]: Write named register: InjVcalHigh -> 3500
[15:51:29:120][  info  ][     Rd53a     ]: Write named register: InjVcalMed -> 500
[15:51:29:120][  info  ][     Rd53a     ]: Write named register: LatencyConfig -> 48
[15:51:29:120][  info  ][  scanConsole  ]: Starting histogrammer and analysis threads:
[15:51:29:120][  info  ][  scanConsole  ]:  .. started threads of Fe 4
[15:51:29:120][  info  ][  scanConsole  ]:  .. started threads of Fe 5
[15:51:29:120][  info  ][  scanConsole  ]:  .. started threads of Fe 6
[15:51:29:120][  info  ][  scanConsole  ]:  .. started threads of Fe 7
[15:51:29:120][  info  ][Rd53aDataProcessor]:   -> Processor thread #0 started!
[15:51:29:120][  info  ][Rd53aDataProcessor]:   -> Processor thread #1 started!
[15:51:29:120][  info  ][Rd53aDataProcessor]:   -> Processor thread #2 started!
[15:51:29:120][  info  ][Rd53aDataProcessor]:   -> Processor thread #3 started!
[15:51:29:120][  info  ][  scanConsole  ]: ########
[15:51:29:120][  info  ][  scanConsole  ]: # Scan #
[15:51:29:120][  info  ][  scanConsole  ]: ########
[15:51:29:120][  info  ][  scanConsole  ]: Starting scan!
[15:51:29:120][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0010055 on Rx 4
[15:51:29:124][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0010055] MON MUX_V: 3 Bias 0 999 -> 0.224574 V Bias 1 1455 -> 0.315269 V, Temperature 0.0906954 -> 37.1375 C
[15:51:29:128][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0010055] MON MUX_V: 5 Bias 0 975 -> 0.2198 V Bias 1 1432 -> 0.310694 V, Temperature 0.0908943 -> 38.621 C
[15:51:29:132][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0010055] MON MUX_V: 7 Bias 0 959 -> 0.216618 V Bias 1 1417 -> 0.307711 V, Temperature 0.0910932 -> 40.3579 C
[15:51:29:136][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0010055] MON MUX_V: 15 Bias 0 942 -> 0.213237 V Bias 1 1405 -> 0.305324 V, Temperature 0.0920876 -> 40.1167 C
[15:51:29:169][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0010103 on Rx 5
[15:51:29:173][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0010103] MON MUX_V: 3 Bias 0 1006 -> 0.223415 V Bias 1 1462 -> 0.314305 V, Temperature 0.0908895 -> 34.9222 C
[15:51:29:177][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0010103] MON MUX_V: 5 Bias 0 950 -> 0.212253 V Bias 1 1410 -> 0.30394 V, Temperature 0.0916868 -> 36.78 C
[15:51:29:180][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0010103] MON MUX_V: 7 Bias 0 931 -> 0.208466 V Bias 1 1403 -> 0.302545 V, Temperature 0.0940786 -> 37.825 C
[15:51:29:184][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0010103] MON MUX_V: 15 Bias 0 944 -> 0.211057 V Bias 1 1409 -> 0.303741 V, Temperature 0.0926834 -> 39.6783 C
[15:51:29:218][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0010119 on Rx 6
[15:51:29:222][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0010119] MON MUX_V: 3 Bias 0 990 -> 0.220501 V Bias 1 1454 -> 0.312118 V, Temperature 0.0916172 -> 35.1808 C
[15:51:29:226][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0010119] MON MUX_V: 5 Bias 0 968 -> 0.216157 V Bias 1 1432 -> 0.307774 V, Temperature 0.0916172 -> 41.7882 C
[15:51:29:229][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0010119] MON MUX_V: 7 Bias 0 946 -> 0.211813 V Bias 1 1416 -> 0.304615 V, Temperature 0.0928019 -> 40.358 C
[15:51:29:233][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0010119] MON MUX_V: 15 Bias 0 938 -> 0.210233 V Bias 1 1407 -> 0.302838 V, Temperature 0.0926044 -> 42.8666 C
[15:51:29:267][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0010087 on Rx 7
[15:51:29:271][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0010087] MON MUX_V: 3 Bias 0 1012 -> 0.224138 V Bias 1 1465 -> 0.314036 V, Temperature 0.0898979 -> 36.1444 C
[15:51:29:275][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0010087] MON MUX_V: 5 Bias 0 979 -> 0.217589 V Bias 1 1442 -> 0.309472 V, Temperature 0.0918824 -> 37.6286 C
[15:51:29:279][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0010087] MON MUX_V: 7 Bias 0 962 -> 0.214216 V Bias 1 1434 -> 0.307884 V, Temperature 0.0936684 -> 39.2 C
[15:51:29:283][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0010087] MON MUX_V: 15 Bias 0 968 -> 0.215406 V Bias 1 1435 -> 0.308083 V, Temperature 0.0926762 -> 36.0913 C
[15:51:29:320][  info  ][  scanConsole  ]: Scan done!
[15:51:29:320][  info  ][  scanConsole  ]: Waiting for processors to finish ...
[15:51:29:321][  info  ][  scanConsole  ]: Processor done, waiting for histogrammer ...
[15:51:29:720][  info  ][HistogramAlgorithm]: Histogrammer done!
[15:51:29:720][  info  ][HistogramAlgorithm]: Histogrammer done!
[15:51:29:720][  info  ][HistogramAlgorithm]: Histogrammer done!
[15:51:29:720][  info  ][HistogramAlgorithm]: Histogrammer done!
[15:51:29:721][  info  ][  scanConsole  ]: Processor done, waiting for analysis ...
[15:51:30:121][  info  ][AnalysisAlgorithm]: Analysis done!
[15:51:30:121][  info  ][AnalysisAlgorithm]: Analysis done!
[15:51:30:121][  info  ][AnalysisAlgorithm]: Analysis done!
[15:51:30:121][  info  ][AnalysisAlgorithm]: Analysis done!
[15:51:30:121][  info  ][  scanConsole  ]: All done!
[15:51:30:121][  info  ][  scanConsole  ]: ##########
[15:51:30:121][  info  ][  scanConsole  ]: # Timing #
[15:51:30:121][  info  ][  scanConsole  ]: ##########
[15:51:30:121][  info  ][  scanConsole  ]: -> Configuration: 125 ms
[15:51:30:121][  info  ][  scanConsole  ]: -> Scan:          199 ms
[15:51:30:121][  info  ][  scanConsole  ]: -> Processing:    0 ms
[15:51:30:121][  info  ][  scanConsole  ]: -> Analysis:      800 ms
[15:51:30:121][  info  ][  scanConsole  ]: ###########
[15:51:30:121][  info  ][  scanConsole  ]: # Cleanup #
[15:51:30:121][  info  ][  scanConsole  ]: ###########
[15:51:30:130][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0010055 to configs/CERNQ6/20UPGFC0010055.json
[15:51:30:337][  info  ][  scanConsole  ]: -> Storing output results of FE 4
[15:51:30:337][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0010055, this usually means that the chip did not send any data at all.
[15:51:30:338][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0010103 to configs/CERNQ6/20UPGFC0010103.json
[15:51:30:503][  info  ][  scanConsole  ]: -> Storing output results of FE 5
[15:51:30:503][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0010103, this usually means that the chip did not send any data at all.
[15:51:30:504][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0010119 to configs/CERNQ6/20UPGFC0010119.json
[15:51:30:669][  info  ][  scanConsole  ]: -> Storing output results of FE 6
[15:51:30:669][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0010119, this usually means that the chip did not send any data at all.
[15:51:30:671][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0010087 to configs/CERNQ6/20UPGFC0010087.json
[15:51:30:837][  info  ][  scanConsole  ]: -> Storing output results of FE 7
[15:51:30:837][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0010087, this usually means that the chip did not send any data at all.
[15:51:30:838][  info  ][  scanConsole  ]: Finishing run: 1511
[root@jollyroger Yarr]# 
