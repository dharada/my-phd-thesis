[18:06:41:336][  info  ][               ]: #####################################
[18:06:41:336][  info  ][               ]: # Welcome to the YARR Scan Console! #
[18:06:41:336][  info  ][               ]: #####################################
[18:06:41:336][  info  ][               ]: -> Parsing command line parameters ...
[18:06:41:336][  info  ][               ]: Configuring logger ...
[18:06:41:342][  info  ][  scanConsole  ]: Scan Type/Config configs/scans/rd53a/reg_readtemp.json
[18:06:41:342][  info  ][  scanConsole  ]: Connectivity:
[18:06:41:342][  info  ][  scanConsole  ]:     configs/CERNQ6/example_rd53a_setup.json
[18:06:41:342][  info  ][  scanConsole  ]: Target ToT: -1
[18:06:41:342][  info  ][  scanConsole  ]: Target Charge: -1
[18:06:41:342][  info  ][  scanConsole  ]: Output Plots: false
[18:06:41:342][  info  ][  scanConsole  ]: Output Directory: ./data/001543_reg_readtemp/
[18:06:41:347][  info  ][  scanConsole  ]: Timestamp: 2022-08-20_18:06:41
[18:06:41:347][  info  ][  scanConsole  ]: Run Number: 1543
[18:06:41:347][  info  ][  scanConsole  ]: #################
[18:06:41:347][  info  ][  scanConsole  ]: # Init Hardware #
[18:06:41:347][  info  ][  scanConsole  ]: #################
[18:06:41:347][  info  ][  scanConsole  ]: -> Opening controller config: configs/controller/specCfg-rd53a-16x1.json
[18:06:41:347][  info  ][  ScanHelper   ]: Loading controller ...
[18:06:41:347][  info  ][  ScanHelper   ]: Found controller of type: spec
[18:06:41:347][  info  ][  ScanHelper   ]: ... loading controler config:
[18:06:41:347][  info  ][  ScanHelper   ]: ~~~ {
[18:06:41:347][  info  ][  ScanHelper   ]: ~~~     "cmdPeriod": 6.250000073038109e-9,
[18:06:41:347][  info  ][  ScanHelper   ]: ~~~     "idle": {
[18:06:41:347][  info  ][  ScanHelper   ]: ~~~         "word": 1768515945
[18:06:41:347][  info  ][  ScanHelper   ]: ~~~     },
[18:06:41:347][  info  ][  ScanHelper   ]: ~~~     "pulse": {
[18:06:41:347][  info  ][  ScanHelper   ]: ~~~         "interval": 500,
[18:06:41:347][  info  ][  ScanHelper   ]: ~~~         "word": 1549575846
[18:06:41:347][  info  ][  ScanHelper   ]: ~~~     },
[18:06:41:347][  info  ][  ScanHelper   ]: ~~~     "rxActiveLanes": 1,
[18:06:41:347][  info  ][  ScanHelper   ]: ~~~     "rxPolarity": 0,
[18:06:41:347][  info  ][  ScanHelper   ]: ~~~     "specNum": 0,
[18:06:41:347][  info  ][  ScanHelper   ]: ~~~     "spiConfig": 541200,
[18:06:41:347][  info  ][  ScanHelper   ]: ~~~     "sync": {
[18:06:41:347][  info  ][  ScanHelper   ]: ~~~         "interval": 32,
[18:06:41:347][  info  ][  ScanHelper   ]: ~~~         "word": 2172551550
[18:06:41:347][  info  ][  ScanHelper   ]: ~~~     },
[18:06:41:347][  info  ][  ScanHelper   ]: ~~~     "txPolarity": 0
[18:06:41:347][  info  ][  ScanHelper   ]: ~~~ }
[18:06:41:347][  info  ][    SpecCom    ]: Opening SPEC with id #0
[18:06:41:347][  info  ][    SpecCom    ]: Mapping BARs ...
[18:06:41:347][  info  ][    SpecCom    ]: ... Mapped BAR0 at 0x7f6eee233000 with size 1048576
[18:06:41:347][warning ][    SpecCom    ]: ... BAR4 not mapped (Mmap failed)
[18:06:41:347][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[18:06:41:347][  info  ][    SpecCom    ]: Firmware Version: 0x4d9ff6d
[18:06:41:347][  info  ][    SpecCom    ]: Firmware Identifier: 0x4030232
[18:06:41:347][  info  ][    SpecCom    ]: FPGA card: PLDA XpressK7 325
[18:06:41:347][  info  ][    SpecCom    ]: FE Chip Type: RD53A/B
[18:06:41:347][  info  ][    SpecCom    ]: FMC Card Type: Ohio Card (Display Port)
[18:06:41:347][  info  ][    SpecCom    ]: RX Speed: 640Mbps
[18:06:41:347][  info  ][    SpecCom    ]: Channel Configuration: 16x1
[18:06:41:347][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[18:06:41:347][  info  ][    SpecCom    ]: Flushing buffers ...
[18:06:41:347][  info  ][    SpecCom    ]: Init success!
[18:06:41:347][  info  ][  scanConsole  ]: #######################
[18:06:41:347][  info  ][  scanConsole  ]: ##  Loading Configs  ##
[18:06:41:347][  info  ][  scanConsole  ]: #######################
[18:06:41:347][  info  ][  scanConsole  ]: Opening global config: configs/CERNQ6/example_rd53a_setup.json
[18:06:41:347][  info  ][  ScanHelper   ]: Chip type: RD53A
[18:06:41:347][  info  ][  ScanHelper   ]: Chip count 4
[18:06:41:347][  info  ][  ScanHelper   ]: Loading chip #0
[18:06:41:348][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(4)
[18:06:41:348][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ6/20UPGFC0010055.json
[18:06:41:524][  info  ][  ScanHelper   ]: Loading chip #1
[18:06:41:525][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(5)
[18:06:41:525][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ6/20UPGFC0010103.json
[18:06:41:698][  info  ][  ScanHelper   ]: Loading chip #2
[18:06:41:699][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(6)
[18:06:41:699][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ6/20UPGFC0010119.json
[18:06:41:873][  info  ][  ScanHelper   ]: Loading chip #3
[18:06:41:873][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(7)
[18:06:41:873][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ6/20UPGFC0010087.json
[18:06:42:050][  info  ][  scanConsole  ]: #################
[18:06:42:050][  info  ][  scanConsole  ]: # Configure FEs #
[18:06:42:050][  info  ][  scanConsole  ]: #################
[18:06:42:050][  info  ][  scanConsole  ]: Configuring 20UPGFC0010055
[18:06:42:081][  info  ][  scanConsole  ]: Configuring 20UPGFC0010103
[18:06:42:111][  info  ][  scanConsole  ]: Configuring 20UPGFC0010119
[18:06:42:143][  info  ][  scanConsole  ]: Configuring 20UPGFC0010087
[18:06:42:191][  info  ][  scanConsole  ]: Sent configuration to all FEs in 140 ms!
[18:06:42:192][  info  ][  scanConsole  ]: Checking com 20UPGFC0010055
[18:06:42:192][  info  ][    SpecRx     ]: Active Rx channels: 0x10
[18:06:42:192][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[18:06:42:192][  info  ][    SpecRx     ]: Rx Status 0xf0
[18:06:42:192][  info  ][    SpecRx     ]: Number of lanes: 1
[18:06:42:192][  info  ][    SpecRx     ]: Channel 4 Lane 0 synchronized!
[18:06:42:203][  info  ][  scanConsole  ]: ... success!
[18:06:42:203][  info  ][  scanConsole  ]: Checking com 20UPGFC0010103
[18:06:42:203][  info  ][    SpecRx     ]: Active Rx channels: 0x20
[18:06:42:203][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[18:06:42:203][  info  ][    SpecRx     ]: Rx Status 0xf0
[18:06:42:203][  info  ][    SpecRx     ]: Number of lanes: 1
[18:06:42:203][  info  ][    SpecRx     ]: Channel 5 Lane 0 synchronized!
[18:06:42:213][  info  ][  scanConsole  ]: ... success!
[18:06:42:213][  info  ][  scanConsole  ]: Checking com 20UPGFC0010119
[18:06:42:213][  info  ][    SpecRx     ]: Active Rx channels: 0x40
[18:06:42:213][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[18:06:42:213][  info  ][    SpecRx     ]: Rx Status 0xf0
[18:06:42:213][  info  ][    SpecRx     ]: Number of lanes: 1
[18:06:42:213][  info  ][    SpecRx     ]: Channel 6 Lane 0 synchronized!
[18:06:42:224][  info  ][  scanConsole  ]: ... success!
[18:06:42:224][  info  ][  scanConsole  ]: Checking com 20UPGFC0010087
[18:06:42:224][  info  ][    SpecRx     ]: Active Rx channels: 0x80
[18:06:42:224][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[18:06:42:224][  info  ][    SpecRx     ]: Rx Status 0xf0
[18:06:42:224][  info  ][    SpecRx     ]: Number of lanes: 1
[18:06:42:224][  info  ][    SpecRx     ]: Channel 7 Lane 0 synchronized!
[18:06:42:234][  info  ][  scanConsole  ]: ... success!
[18:06:42:234][  info  ][  scanConsole  ]: Enabling Tx channels
[18:06:42:234][  info  ][  scanConsole  ]: Enabling Tx channel 1
[18:06:42:234][  info  ][  scanConsole  ]: Enabling Tx channel 1
[18:06:42:235][  info  ][  scanConsole  ]: Enabling Tx channel 1
[18:06:42:235][  info  ][  scanConsole  ]: Enabling Tx channel 1
[18:06:42:235][  info  ][  scanConsole  ]: Enabling Rx channels
[18:06:42:235][  info  ][  scanConsole  ]: Enabling Rx channel 4
[18:06:42:235][  info  ][  scanConsole  ]: Enabling Rx channel 5
[18:06:42:235][  info  ][  scanConsole  ]: Enabling Rx channel 6
[18:06:42:235][  info  ][  scanConsole  ]: Enabling Rx channel 7
[18:06:42:235][  info  ][  scanConsole  ]: ##############
[18:06:42:235][  info  ][  scanConsole  ]: # Setup Scan #
[18:06:42:235][  info  ][  scanConsole  ]: ##############
[18:06:42:235][  info  ][  scanConsole  ]: Found Scan config, constructing scan ...
[18:06:42:235][  info  ][  ScanFactory  ]: Loading Scan:
[18:06:42:236][  info  ][  ScanFactory  ]:   Name: AnalogScan
[18:06:42:236][  info  ][  ScanFactory  ]:   Number of Loops: 3
[18:06:42:236][  info  ][  ScanFactory  ]:   Loading Loop #0
[18:06:42:236][  info  ][  ScanFactory  ]:    Type: Rd53aReadRegLoop
[18:06:42:236][  info  ][  ScanFactory  ]:    Loading loop config ... 
[18:06:42:236][  info  ][  ScanFactory  ]: ~~~ {
[18:06:42:236][  info  ][  ScanFactory  ]: ~~~     "CurMux": [],
[18:06:42:236][  info  ][  ScanFactory  ]: ~~~     "EnblRingOsc": 0,
[18:06:42:236][  info  ][  ScanFactory  ]: ~~~     "Registers": [],
[18:06:42:236][  info  ][  ScanFactory  ]: ~~~     "RingOscDur": 9,
[18:06:42:236][  info  ][  ScanFactory  ]: ~~~     "RingOscRep": 10,
[18:06:42:236][  info  ][  ScanFactory  ]: ~~~     "VoltMux": [3, 5, 7, 15]
[18:06:42:236][  info  ][  ScanFactory  ]: ~~~ }
[18:06:42:236][  info  ][  ScanFactory  ]:   Loading Loop #1
[18:06:42:236][  info  ][  ScanFactory  ]:    Type: Rd53aTriggerLoop
[18:06:42:236][  info  ][  ScanFactory  ]:    Loading loop config ... 
[18:06:42:236][  info  ][  ScanFactory  ]: ~~~ {
[18:06:42:236][  info  ][  ScanFactory  ]: ~~~     "count": 1,
[18:06:42:236][  info  ][  ScanFactory  ]: ~~~     "delay": 48,
[18:06:42:236][  info  ][  ScanFactory  ]: ~~~     "extTrig": false,
[18:06:42:236][  info  ][  ScanFactory  ]: ~~~     "frequency": 30000.0,
[18:06:42:236][  info  ][  ScanFactory  ]: ~~~     "noInject": false,
[18:06:42:236][  info  ][  ScanFactory  ]: ~~~     "sendEcr": false,
[18:06:42:236][  info  ][  ScanFactory  ]: ~~~     "time": 0.0,
[18:06:42:236][  info  ][  ScanFactory  ]: ~~~     "trigMultiplier": 16
[18:06:42:236][  info  ][  ScanFactory  ]: ~~~ }
[18:06:42:236][  info  ][  ScanFactory  ]:   Loading Loop #2
[18:06:42:236][  info  ][  ScanFactory  ]:    Type: StdDataLoop
[18:06:42:236][warning ][  ScanFactory  ]: ~~~ Config empty.
[18:06:42:236][  info  ][ScanBuildHistogrammers]: Loading histogrammer ...
[18:06:42:237][  info  ][ScanBuildHistogrammers]: ... done!
[18:06:42:237][  info  ][ScanBuildAnalyses]: Loading analyses ...
[18:06:42:237][  info  ][  scanConsole  ]: Running pre scan!
[18:06:42:237][  info  ][  ScanFactory  ]: Entering pre scan phase ...
[18:06:42:237][  info  ][     Rd53a     ]: Write named register: GlobalPulseRt -> 16384
[18:06:42:237][  info  ][     Rd53a     ]: Write named register: InjEnDig -> 0
[18:06:42:237][  info  ][     Rd53a     ]: Write named register: InjVcalDiff -> 0
[18:06:42:237][  info  ][     Rd53a     ]: Write named register: InjVcalHigh -> 3500
[18:06:42:237][  info  ][     Rd53a     ]: Write named register: InjVcalMed -> 500
[18:06:42:237][  info  ][     Rd53a     ]: Write named register: LatencyConfig -> 48
[18:06:42:237][  info  ][  scanConsole  ]: Starting histogrammer and analysis threads:
[18:06:42:238][  info  ][  scanConsole  ]:  .. started threads of Fe 4
[18:06:42:238][  info  ][  scanConsole  ]:  .. started threads of Fe 5
[18:06:42:238][  info  ][  scanConsole  ]:  .. started threads of Fe 6
[18:06:42:238][  info  ][  scanConsole  ]:  .. started threads of Fe 7
[18:06:42:239][  info  ][Rd53aDataProcessor]:   -> Processor thread #0 started!
[18:06:42:239][  info  ][Rd53aDataProcessor]:   -> Processor thread #1 started!
[18:06:42:239][  info  ][Rd53aDataProcessor]:   -> Processor thread #2 started!
[18:06:42:239][  info  ][Rd53aDataProcessor]:   -> Processor thread #3 started!
[18:06:42:239][  info  ][  scanConsole  ]: ########
[18:06:42:239][  info  ][  scanConsole  ]: # Scan #
[18:06:42:239][  info  ][  scanConsole  ]: ########
[18:06:42:239][  info  ][  scanConsole  ]: Starting scan!
[18:06:42:239][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0010055 on Rx 4
[18:06:42:243][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0010055] MON MUX_V: 3 Bias 0 1317 -> 0.287822 V Bias 1 1687 -> 0.361412 V, Temperature 0.0735905 -> -21.9874 C
[18:06:42:247][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0010055] MON MUX_V: 5 Bias 0 1297 -> 0.283844 V Bias 1 1666 -> 0.357235 V, Temperature 0.0733916 -> -37.8001 C
[18:06:42:251][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0010055] MON MUX_V: 7 Bias 0 1284 -> 0.281258 V Bias 1 1653 -> 0.35465 V, Temperature 0.0733916 -> -36.9315 C
[18:06:42:255][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0010055] MON MUX_V: 15 Bias 0 1263 -> 0.277081 V Bias 1 1641 -> 0.352263 V, Temperature 0.0751817 -> -37.8001 C
[18:06:42:290][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0010103 on Rx 5
[18:06:42:294][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0010103] MON MUX_V: 3 Bias 0 1336 -> 0.28919 V Bias 1 1710 -> 0.363736 V, Temperature 0.0745453 -> -15.1889 C
[18:06:42:299][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0010103] MON MUX_V: 5 Bias 0 1287 -> 0.279424 V Bias 1 1663 -> 0.354368 V, Temperature 0.074944 -> -18.6601 C
[18:06:42:303][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0010103] MON MUX_V: 7 Bias 0 1267 -> 0.275437 V Bias 1 1656 -> 0.352972 V, Temperature 0.0775351 -> -19.2375 C
[18:06:42:307][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0010103] MON MUX_V: 15 Bias 0 1286 -> 0.279224 V Bias 1 1665 -> 0.354766 V, Temperature 0.075542 -> -22.0172 C
[18:06:42:342][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0010119 on Rx 6
[18:06:42:346][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0010119] MON MUX_V: 3 Bias 0 1320 -> 0.285659 V Bias 1 1700 -> 0.360691 V, Temperature 0.0750313 -> -18.1269 C
[18:06:42:350][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0010119] MON MUX_V: 5 Bias 0 1307 -> 0.283092 V Bias 1 1685 -> 0.357729 V, Temperature 0.0746364 -> -41.6823 C
[18:06:42:355][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0010119] MON MUX_V: 7 Bias 0 1290 -> 0.279736 V Bias 1 1673 -> 0.355359 V, Temperature 0.0756237 -> -35.1947 C
[18:06:42:359][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0010119] MON MUX_V: 15 Bias 0 1280 -> 0.277761 V Bias 1 1660 -> 0.352793 V, Temperature 0.0750313 -> -38.7167 C
[18:06:42:394][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0010087 on Rx 7
[18:06:42:398][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0010087] MON MUX_V: 3 Bias 0 1339 -> 0.289031 V Bias 1 1707 -> 0.362061 V, Temperature 0.0730296 -> -15.8001 C
[18:06:42:402][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0010087] MON MUX_V: 5 Bias 0 1307 -> 0.282681 V Bias 1 1689 -> 0.358489 V, Temperature 0.0758079 -> -26.0143 C
[18:06:42:407][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0010087] MON MUX_V: 7 Bias 0 1295 -> 0.2803 V Bias 1 1681 -> 0.356901 V, Temperature 0.0766017 -> -28.3714 C
[18:06:42:411][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0010087] MON MUX_V: 15 Bias 0 1296 -> 0.280498 V Bias 1 1679 -> 0.356504 V, Temperature 0.0760064 -> -24.1696 C
[18:06:42:447][  info  ][  scanConsole  ]: Scan done!
[18:06:42:447][  info  ][  scanConsole  ]: Waiting for processors to finish ...
[18:06:42:448][  info  ][  scanConsole  ]: Processor done, waiting for histogrammer ...
[18:06:42:848][  info  ][HistogramAlgorithm]: Histogrammer done!
[18:06:42:848][  info  ][HistogramAlgorithm]: Histogrammer done!
[18:06:42:848][  info  ][HistogramAlgorithm]: Histogrammer done!
[18:06:42:848][  info  ][HistogramAlgorithm]: Histogrammer done!
[18:06:42:848][  info  ][  scanConsole  ]: Processor done, waiting for analysis ...
[18:06:43:248][  info  ][AnalysisAlgorithm]: Analysis done!
[18:06:43:249][  info  ][AnalysisAlgorithm]: Analysis done!
[18:06:43:249][  info  ][AnalysisAlgorithm]: Analysis done!
[18:06:43:249][  info  ][AnalysisAlgorithm]: Analysis done!
[18:06:43:249][  info  ][  scanConsole  ]: All done!
[18:06:43:249][  info  ][  scanConsole  ]: ##########
[18:06:43:249][  info  ][  scanConsole  ]: # Timing #
[18:06:43:249][  info  ][  scanConsole  ]: ##########
[18:06:43:249][  info  ][  scanConsole  ]: -> Configuration: 140 ms
[18:06:43:249][  info  ][  scanConsole  ]: -> Scan:          208 ms
[18:06:43:249][  info  ][  scanConsole  ]: -> Processing:    1 ms
[18:06:43:249][  info  ][  scanConsole  ]: -> Analysis:      800 ms
[18:06:43:249][  info  ][  scanConsole  ]: ###########
[18:06:43:249][  info  ][  scanConsole  ]: # Cleanup #
[18:06:43:249][  info  ][  scanConsole  ]: ###########
[18:06:43:257][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0010055 to configs/CERNQ6/20UPGFC0010055.json
[18:06:43:459][  info  ][  scanConsole  ]: -> Storing output results of FE 4
[18:06:43:459][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0010055, this usually means that the chip did not send any data at all.
[18:06:43:461][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0010103 to configs/CERNQ6/20UPGFC0010103.json
[18:06:43:624][  info  ][  scanConsole  ]: -> Storing output results of FE 5
[18:06:43:624][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0010103, this usually means that the chip did not send any data at all.
[18:06:43:625][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0010119 to configs/CERNQ6/20UPGFC0010119.json
[18:06:43:789][  info  ][  scanConsole  ]: -> Storing output results of FE 6
[18:06:43:789][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0010119, this usually means that the chip did not send any data at all.
[18:06:43:790][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0010087 to configs/CERNQ6/20UPGFC0010087.json
[18:06:43:954][  info  ][  scanConsole  ]: -> Storing output results of FE 7
[18:06:43:954][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0010087, this usually means that the chip did not send any data at all.
[18:06:43:955][  info  ][  scanConsole  ]: Finishing run: 1543
[root@jollyroger Yarr]# 
