[18:14:14:759][  info  ][               ]: #####################################
[18:14:14:759][  info  ][               ]: # Welcome to the YARR Scan Console! #
[18:14:14:759][  info  ][               ]: #####################################
[18:14:14:759][  info  ][               ]: -> Parsing command line parameters ...
[18:14:14:759][  info  ][               ]: Configuring logger ...
[18:14:14:762][  info  ][  scanConsole  ]: Scan Type/Config configs/scans/rd53a/reg_readtemp.json
[18:14:14:762][  info  ][  scanConsole  ]: Connectivity:
[18:14:14:762][  info  ][  scanConsole  ]:     configs/CERNQ6/example_rd53a_setup.json
[18:14:14:762][  info  ][  scanConsole  ]: Target ToT: -1
[18:14:14:762][  info  ][  scanConsole  ]: Target Charge: -1
[18:14:14:762][  info  ][  scanConsole  ]: Output Plots: false
[18:14:14:762][  info  ][  scanConsole  ]: Output Directory: ./data/001547_reg_readtemp/
[18:14:14:767][  info  ][  scanConsole  ]: Timestamp: 2022-08-20_18:14:14
[18:14:14:767][  info  ][  scanConsole  ]: Run Number: 1547
[18:14:14:767][  info  ][  scanConsole  ]: #################
[18:14:14:767][  info  ][  scanConsole  ]: # Init Hardware #
[18:14:14:767][  info  ][  scanConsole  ]: #################
[18:14:14:767][  info  ][  scanConsole  ]: -> Opening controller config: configs/controller/specCfg-rd53a-16x1.json
[18:14:14:767][  info  ][  ScanHelper   ]: Loading controller ...
[18:14:14:767][  info  ][  ScanHelper   ]: Found controller of type: spec
[18:14:14:767][  info  ][  ScanHelper   ]: ... loading controler config:
[18:14:14:767][  info  ][  ScanHelper   ]: ~~~ {
[18:14:14:767][  info  ][  ScanHelper   ]: ~~~     "cmdPeriod": 6.250000073038109e-9,
[18:14:14:767][  info  ][  ScanHelper   ]: ~~~     "idle": {
[18:14:14:767][  info  ][  ScanHelper   ]: ~~~         "word": 1768515945
[18:14:14:767][  info  ][  ScanHelper   ]: ~~~     },
[18:14:14:767][  info  ][  ScanHelper   ]: ~~~     "pulse": {
[18:14:14:767][  info  ][  ScanHelper   ]: ~~~         "interval": 500,
[18:14:14:767][  info  ][  ScanHelper   ]: ~~~         "word": 1549575846
[18:14:14:767][  info  ][  ScanHelper   ]: ~~~     },
[18:14:14:767][  info  ][  ScanHelper   ]: ~~~     "rxActiveLanes": 1,
[18:14:14:767][  info  ][  ScanHelper   ]: ~~~     "rxPolarity": 0,
[18:14:14:767][  info  ][  ScanHelper   ]: ~~~     "specNum": 0,
[18:14:14:767][  info  ][  ScanHelper   ]: ~~~     "spiConfig": 541200,
[18:14:14:767][  info  ][  ScanHelper   ]: ~~~     "sync": {
[18:14:14:767][  info  ][  ScanHelper   ]: ~~~         "interval": 32,
[18:14:14:767][  info  ][  ScanHelper   ]: ~~~         "word": 2172551550
[18:14:14:767][  info  ][  ScanHelper   ]: ~~~     },
[18:14:14:767][  info  ][  ScanHelper   ]: ~~~     "txPolarity": 0
[18:14:14:767][  info  ][  ScanHelper   ]: ~~~ }
[18:14:14:767][  info  ][    SpecCom    ]: Opening SPEC with id #0
[18:14:14:767][  info  ][    SpecCom    ]: Mapping BARs ...
[18:14:14:767][  info  ][    SpecCom    ]: ... Mapped BAR0 at 0x7f1786740000 with size 1048576
[18:14:14:767][warning ][    SpecCom    ]: ... BAR4 not mapped (Mmap failed)
[18:14:14:767][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[18:14:14:767][  info  ][    SpecCom    ]: Firmware Version: 0x4d9ff6d
[18:14:14:767][  info  ][    SpecCom    ]: Firmware Identifier: 0x4030232
[18:14:14:767][  info  ][    SpecCom    ]: FPGA card: PLDA XpressK7 325
[18:14:14:767][  info  ][    SpecCom    ]: FE Chip Type: RD53A/B
[18:14:14:767][  info  ][    SpecCom    ]: FMC Card Type: Ohio Card (Display Port)
[18:14:14:767][  info  ][    SpecCom    ]: RX Speed: 640Mbps
[18:14:14:767][  info  ][    SpecCom    ]: Channel Configuration: 16x1
[18:14:14:767][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[18:14:14:767][  info  ][    SpecCom    ]: Flushing buffers ...
[18:14:14:767][  info  ][    SpecCom    ]: Init success!
[18:14:14:767][  info  ][  scanConsole  ]: #######################
[18:14:14:767][  info  ][  scanConsole  ]: ##  Loading Configs  ##
[18:14:14:767][  info  ][  scanConsole  ]: #######################
[18:14:14:767][  info  ][  scanConsole  ]: Opening global config: configs/CERNQ6/example_rd53a_setup.json
[18:14:14:767][  info  ][  ScanHelper   ]: Chip type: RD53A
[18:14:14:767][  info  ][  ScanHelper   ]: Chip count 4
[18:14:14:767][  info  ][  ScanHelper   ]: Loading chip #0
[18:14:14:768][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(4)
[18:14:14:768][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ6/20UPGFC0010055.json
[18:14:14:953][  info  ][  ScanHelper   ]: Loading chip #1
[18:14:14:954][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(5)
[18:14:14:954][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ6/20UPGFC0010103.json
[18:14:15:127][  info  ][  ScanHelper   ]: Loading chip #2
[18:14:15:127][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(6)
[18:14:15:127][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ6/20UPGFC0010119.json
[18:14:15:304][  info  ][  ScanHelper   ]: Loading chip #3
[18:14:15:305][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(7)
[18:14:15:305][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ6/20UPGFC0010087.json
[18:14:15:482][  info  ][  scanConsole  ]: #################
[18:14:15:482][  info  ][  scanConsole  ]: # Configure FEs #
[18:14:15:482][  info  ][  scanConsole  ]: #################
[18:14:15:482][  info  ][  scanConsole  ]: Configuring 20UPGFC0010055
[18:14:15:512][  info  ][  scanConsole  ]: Configuring 20UPGFC0010103
[18:14:15:542][  info  ][  scanConsole  ]: Configuring 20UPGFC0010119
[18:14:15:572][  info  ][  scanConsole  ]: Configuring 20UPGFC0010087
[18:14:15:603][  info  ][  scanConsole  ]: Sent configuration to all FEs in 121 ms!
[18:14:15:604][  info  ][  scanConsole  ]: Checking com 20UPGFC0010055
[18:14:15:604][  info  ][    SpecRx     ]: Active Rx channels: 0x10
[18:14:15:604][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[18:14:15:604][  info  ][    SpecRx     ]: Rx Status 0xf0
[18:14:15:604][  info  ][    SpecRx     ]: Number of lanes: 1
[18:14:15:604][  info  ][    SpecRx     ]: Channel 4 Lane 0 synchronized!
[18:14:15:614][  info  ][  scanConsole  ]: ... success!
[18:14:15:614][  info  ][  scanConsole  ]: Checking com 20UPGFC0010103
[18:14:15:614][  info  ][    SpecRx     ]: Active Rx channels: 0x20
[18:14:15:614][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[18:14:15:614][  info  ][    SpecRx     ]: Rx Status 0xf0
[18:14:15:614][  info  ][    SpecRx     ]: Number of lanes: 1
[18:14:15:614][  info  ][    SpecRx     ]: Channel 5 Lane 0 synchronized!
[18:14:15:625][  info  ][  scanConsole  ]: ... success!
[18:14:15:625][  info  ][  scanConsole  ]: Checking com 20UPGFC0010119
[18:14:15:625][  info  ][    SpecRx     ]: Active Rx channels: 0x40
[18:14:15:625][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[18:14:15:625][  info  ][    SpecRx     ]: Rx Status 0xf0
[18:14:15:625][  info  ][    SpecRx     ]: Number of lanes: 1
[18:14:15:625][  info  ][    SpecRx     ]: Channel 6 Lane 0 synchronized!
[18:14:15:635][  info  ][  scanConsole  ]: ... success!
[18:14:15:635][  info  ][  scanConsole  ]: Checking com 20UPGFC0010087
[18:14:15:635][  info  ][    SpecRx     ]: Active Rx channels: 0x80
[18:14:15:636][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[18:14:15:636][  info  ][    SpecRx     ]: Rx Status 0xf0
[18:14:15:636][  info  ][    SpecRx     ]: Number of lanes: 1
[18:14:15:636][  info  ][    SpecRx     ]: Channel 7 Lane 0 synchronized!
[18:14:15:646][  info  ][  scanConsole  ]: ... success!
[18:14:15:646][  info  ][  scanConsole  ]: Enabling Tx channels
[18:14:15:646][  info  ][  scanConsole  ]: Enabling Tx channel 1
[18:14:15:646][  info  ][  scanConsole  ]: Enabling Tx channel 1
[18:14:15:646][  info  ][  scanConsole  ]: Enabling Tx channel 1
[18:14:15:646][  info  ][  scanConsole  ]: Enabling Tx channel 1
[18:14:15:646][  info  ][  scanConsole  ]: Enabling Rx channels
[18:14:15:646][  info  ][  scanConsole  ]: Enabling Rx channel 4
[18:14:15:646][  info  ][  scanConsole  ]: Enabling Rx channel 5
[18:14:15:646][  info  ][  scanConsole  ]: Enabling Rx channel 6
[18:14:15:646][  info  ][  scanConsole  ]: Enabling Rx channel 7
[18:14:15:646][  info  ][  scanConsole  ]: ##############
[18:14:15:646][  info  ][  scanConsole  ]: # Setup Scan #
[18:14:15:646][  info  ][  scanConsole  ]: ##############
[18:14:15:647][  info  ][  scanConsole  ]: Found Scan config, constructing scan ...
[18:14:15:647][  info  ][  ScanFactory  ]: Loading Scan:
[18:14:15:647][  info  ][  ScanFactory  ]:   Name: AnalogScan
[18:14:15:647][  info  ][  ScanFactory  ]:   Number of Loops: 3
[18:14:15:647][  info  ][  ScanFactory  ]:   Loading Loop #0
[18:14:15:647][  info  ][  ScanFactory  ]:    Type: Rd53aReadRegLoop
[18:14:15:647][  info  ][  ScanFactory  ]:    Loading loop config ... 
[18:14:15:647][  info  ][  ScanFactory  ]: ~~~ {
[18:14:15:647][  info  ][  ScanFactory  ]: ~~~     "CurMux": [],
[18:14:15:648][  info  ][  ScanFactory  ]: ~~~     "EnblRingOsc": 0,
[18:14:15:648][  info  ][  ScanFactory  ]: ~~~     "Registers": [],
[18:14:15:648][  info  ][  ScanFactory  ]: ~~~     "RingOscDur": 9,
[18:14:15:648][  info  ][  ScanFactory  ]: ~~~     "RingOscRep": 10,
[18:14:15:648][  info  ][  ScanFactory  ]: ~~~     "VoltMux": [3, 5, 7, 15]
[18:14:15:648][  info  ][  ScanFactory  ]: ~~~ }
[18:14:15:648][  info  ][  ScanFactory  ]:   Loading Loop #1
[18:14:15:648][  info  ][  ScanFactory  ]:    Type: Rd53aTriggerLoop
[18:14:15:648][  info  ][  ScanFactory  ]:    Loading loop config ... 
[18:14:15:648][  info  ][  ScanFactory  ]: ~~~ {
[18:14:15:648][  info  ][  ScanFactory  ]: ~~~     "count": 1,
[18:14:15:648][  info  ][  ScanFactory  ]: ~~~     "delay": 48,
[18:14:15:648][  info  ][  ScanFactory  ]: ~~~     "extTrig": false,
[18:14:15:648][  info  ][  ScanFactory  ]: ~~~     "frequency": 30000.0,
[18:14:15:648][  info  ][  ScanFactory  ]: ~~~     "noInject": false,
[18:14:15:648][  info  ][  ScanFactory  ]: ~~~     "sendEcr": false,
[18:14:15:648][  info  ][  ScanFactory  ]: ~~~     "time": 0.0,
[18:14:15:648][  info  ][  ScanFactory  ]: ~~~     "trigMultiplier": 16
[18:14:15:648][  info  ][  ScanFactory  ]: ~~~ }
[18:14:15:648][  info  ][  ScanFactory  ]:   Loading Loop #2
[18:14:15:648][  info  ][  ScanFactory  ]:    Type: StdDataLoop
[18:14:15:648][warning ][  ScanFactory  ]: ~~~ Config empty.
[18:14:15:648][  info  ][ScanBuildHistogrammers]: Loading histogrammer ...
[18:14:15:648][  info  ][ScanBuildHistogrammers]: ... done!
[18:14:15:649][  info  ][ScanBuildAnalyses]: Loading analyses ...
[18:14:15:649][  info  ][  scanConsole  ]: Running pre scan!
[18:14:15:649][  info  ][  ScanFactory  ]: Entering pre scan phase ...
[18:14:15:649][  info  ][     Rd53a     ]: Write named register: GlobalPulseRt -> 16384
[18:14:15:649][  info  ][     Rd53a     ]: Write named register: InjEnDig -> 0
[18:14:15:649][  info  ][     Rd53a     ]: Write named register: InjVcalDiff -> 0
[18:14:15:649][  info  ][     Rd53a     ]: Write named register: InjVcalHigh -> 3500
[18:14:15:649][  info  ][     Rd53a     ]: Write named register: InjVcalMed -> 500
[18:14:15:649][  info  ][     Rd53a     ]: Write named register: LatencyConfig -> 48
[18:14:15:649][  info  ][  scanConsole  ]: Starting histogrammer and analysis threads:
[18:14:15:649][  info  ][  scanConsole  ]:  .. started threads of Fe 4
[18:14:15:650][  info  ][  scanConsole  ]:  .. started threads of Fe 5
[18:14:15:650][  info  ][  scanConsole  ]:  .. started threads of Fe 6
[18:14:15:650][  info  ][  scanConsole  ]:  .. started threads of Fe 7
[18:14:15:650][  info  ][Rd53aDataProcessor]:   -> Processor thread #0 started!
[18:14:15:650][  info  ][Rd53aDataProcessor]:   -> Processor thread #1 started!
[18:14:15:650][  info  ][Rd53aDataProcessor]:   -> Processor thread #2 started!
[18:14:15:650][  info  ][Rd53aDataProcessor]:   -> Processor thread #3 started!
[18:14:15:650][  info  ][  scanConsole  ]: ########
[18:14:15:650][  info  ][  scanConsole  ]: # Scan #
[18:14:15:650][  info  ][  scanConsole  ]: ########
[18:14:15:650][  info  ][  scanConsole  ]: Starting scan!
[18:14:15:650][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0010055 on Rx 4
[18:14:15:655][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0010055] MON MUX_V: 3 Bias 0 1319 -> 0.288219 V Bias 1 1687 -> 0.361412 V, Temperature 0.0731927 -> -23.3625 C
[18:14:15:659][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0010055] MON MUX_V: 5 Bias 0 1297 -> 0.283844 V Bias 1 1667 -> 0.357434 V, Temperature 0.0735905 -> -36.9315 C
[18:14:15:663][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0010055] MON MUX_V: 7 Bias 0 1284 -> 0.281258 V Bias 1 1654 -> 0.354849 V, Temperature 0.0735905 -> -36.0632 C
[18:14:15:667][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0010055] MON MUX_V: 15 Bias 0 1264 -> 0.27728 V Bias 1 1641 -> 0.352263 V, Temperature 0.0749828 -> -38.7166 C
[18:14:15:701][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0010103 on Rx 5
[18:14:15:706][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0010103] MON MUX_V: 3 Bias 0 1338 -> 0.289589 V Bias 1 1711 -> 0.363935 V, Temperature 0.074346 -> -15.8 C
[18:14:15:710][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0010103] MON MUX_V: 5 Bias 0 1288 -> 0.279623 V Bias 1 1663 -> 0.354368 V, Temperature 0.0747447 -> -19.32 C
[18:14:15:714][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0010103] MON MUX_V: 7 Bias 0 1268 -> 0.275637 V Bias 1 1656 -> 0.352972 V, Temperature 0.0773358 -> -19.925 C
[18:14:15:718][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0010103] MON MUX_V: 15 Bias 0 1284 -> 0.278826 V Bias 1 1667 -> 0.355165 V, Temperature 0.0763392 -> -19.1478 C
[18:14:15:753][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0010119 on Rx 6
[18:14:15:757][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0010119] MON MUX_V: 3 Bias 0 1319 -> 0.285462 V Bias 1 1701 -> 0.360888 V, Temperature 0.0754263 -> -16.8576 C
[18:14:15:761][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0010119] MON MUX_V: 5 Bias 0 1307 -> 0.283092 V Bias 1 1685 -> 0.357729 V, Temperature 0.0746364 -> -41.6823 C
[18:14:15:765][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0010119] MON MUX_V: 7 Bias 0 1290 -> 0.279736 V Bias 1 1673 -> 0.355359 V, Temperature 0.0756237 -> -35.1947 C
[18:14:15:769][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0010119] MON MUX_V: 15 Bias 0 1279 -> 0.277564 V Bias 1 1661 -> 0.35299 V, Temperature 0.0754262 -> -36.8834 C
[18:14:15:804][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0010087 on Rx 7
[18:14:15:808][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0010087] MON MUX_V: 3 Bias 0 1342 -> 0.289627 V Bias 1 1708 -> 0.36226 V, Temperature 0.0726327 -> -17.0222 C
[18:14:15:812][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0010087] MON MUX_V: 5 Bias 0 1307 -> 0.282681 V Bias 1 1689 -> 0.358489 V, Temperature 0.0758079 -> -26.0143 C
[18:14:15:816][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0010087] MON MUX_V: 7 Bias 0 1296 -> 0.280498 V Bias 1 1683 -> 0.357298 V, Temperature 0.0768002 -> -27.5858 C
[18:14:15:821][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0010087] MON MUX_V: 15 Bias 0 1296 -> 0.280498 V Bias 1 1681 -> 0.356901 V, Temperature 0.0764033 -> -22.7347 C
[18:14:15:857][  info  ][  scanConsole  ]: Scan done!
[18:14:15:857][  info  ][  scanConsole  ]: Waiting for processors to finish ...
[18:14:15:858][  info  ][  scanConsole  ]: Processor done, waiting for histogrammer ...
[18:14:16:257][  info  ][HistogramAlgorithm]: Histogrammer done!
[18:14:16:257][  info  ][HistogramAlgorithm]: Histogrammer done!
[18:14:16:257][  info  ][HistogramAlgorithm]: Histogrammer done!
[18:14:16:257][  info  ][HistogramAlgorithm]: Histogrammer done!
[18:14:16:258][  info  ][  scanConsole  ]: Processor done, waiting for analysis ...
[18:14:16:658][  info  ][AnalysisAlgorithm]: Analysis done!
[18:14:16:658][  info  ][AnalysisAlgorithm]: Analysis done!
[18:14:16:658][  info  ][AnalysisAlgorithm]: Analysis done!
[18:14:16:658][  info  ][AnalysisAlgorithm]: Analysis done!
[18:14:16:658][  info  ][  scanConsole  ]: All done!
[18:14:16:658][  info  ][  scanConsole  ]: ##########
[18:14:16:658][  info  ][  scanConsole  ]: # Timing #
[18:14:16:659][  info  ][  scanConsole  ]: ##########
[18:14:16:659][  info  ][  scanConsole  ]: -> Configuration: 121 ms
[18:14:16:659][  info  ][  scanConsole  ]: -> Scan:          206 ms
[18:14:16:659][  info  ][  scanConsole  ]: -> Processing:    1 ms
[18:14:16:659][  info  ][  scanConsole  ]: -> Analysis:      800 ms
[18:14:16:659][  info  ][  scanConsole  ]: ###########
[18:14:16:659][  info  ][  scanConsole  ]: # Cleanup #
[18:14:16:659][  info  ][  scanConsole  ]: ###########
[18:14:16:667][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0010055 to configs/CERNQ6/20UPGFC0010055.json
[18:14:16:872][  info  ][  scanConsole  ]: -> Storing output results of FE 4
[18:14:16:872][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0010055, this usually means that the chip did not send any data at all.
[18:14:16:873][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0010103 to configs/CERNQ6/20UPGFC0010103.json
[18:14:17:039][  info  ][  scanConsole  ]: -> Storing output results of FE 5
[18:14:17:039][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0010103, this usually means that the chip did not send any data at all.
[18:14:17:040][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0010119 to configs/CERNQ6/20UPGFC0010119.json
[18:14:17:209][  info  ][  scanConsole  ]: -> Storing output results of FE 6
[18:14:17:209][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0010119, this usually means that the chip did not send any data at all.
[18:14:17:211][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0010087 to configs/CERNQ6/20UPGFC0010087.json
[18:14:17:378][  info  ][  scanConsole  ]: -> Storing output results of FE 7
[18:14:17:378][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0010087, this usually means that the chip did not send any data at all.
[18:14:17:379][  info  ][  scanConsole  ]: Finishing run: 1547
[root@jollyroger Yarr]# 
