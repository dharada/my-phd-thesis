[17:18:55:359][  info  ][               ]: #####################################
[17:18:55:359][  info  ][               ]: # Welcome to the YARR Scan Console! #
[17:18:55:359][  info  ][               ]: #####################################
[17:18:55:359][  info  ][               ]: -> Parsing command line parameters ...
[17:18:55:359][  info  ][               ]: Configuring logger ...
[17:18:55:361][  info  ][  scanConsole  ]: Scan Type/Config configs/scans/rd53a/reg_readtemp.json
[17:18:55:361][  info  ][  scanConsole  ]: Connectivity:
[17:18:55:361][  info  ][  scanConsole  ]:     configs/CERNQ6/example_rd53a_setup.json
[17:18:55:361][  info  ][  scanConsole  ]: Target ToT: -1
[17:18:55:361][  info  ][  scanConsole  ]: Target Charge: -1
[17:18:55:361][  info  ][  scanConsole  ]: Output Plots: false
[17:18:55:362][  info  ][  scanConsole  ]: Output Directory: ./data/001532_reg_readtemp/
[17:18:55:367][  info  ][  scanConsole  ]: Timestamp: 2022-08-20_17:18:55
[17:18:55:367][  info  ][  scanConsole  ]: Run Number: 1532
[17:18:55:367][  info  ][  scanConsole  ]: #################
[17:18:55:367][  info  ][  scanConsole  ]: # Init Hardware #
[17:18:55:367][  info  ][  scanConsole  ]: #################
[17:18:55:367][  info  ][  scanConsole  ]: -> Opening controller config: configs/controller/specCfg-rd53a-16x1.json
[17:18:55:367][  info  ][  ScanHelper   ]: Loading controller ...
[17:18:55:367][  info  ][  ScanHelper   ]: Found controller of type: spec
[17:18:55:367][  info  ][  ScanHelper   ]: ... loading controler config:
[17:18:55:367][  info  ][  ScanHelper   ]: ~~~ {
[17:18:55:367][  info  ][  ScanHelper   ]: ~~~     "cmdPeriod": 6.250000073038109e-9,
[17:18:55:367][  info  ][  ScanHelper   ]: ~~~     "idle": {
[17:18:55:367][  info  ][  ScanHelper   ]: ~~~         "word": 1768515945
[17:18:55:367][  info  ][  ScanHelper   ]: ~~~     },
[17:18:55:367][  info  ][  ScanHelper   ]: ~~~     "pulse": {
[17:18:55:367][  info  ][  ScanHelper   ]: ~~~         "interval": 500,
[17:18:55:367][  info  ][  ScanHelper   ]: ~~~         "word": 1549575846
[17:18:55:367][  info  ][  ScanHelper   ]: ~~~     },
[17:18:55:367][  info  ][  ScanHelper   ]: ~~~     "rxActiveLanes": 1,
[17:18:55:367][  info  ][  ScanHelper   ]: ~~~     "rxPolarity": 0,
[17:18:55:367][  info  ][  ScanHelper   ]: ~~~     "specNum": 0,
[17:18:55:367][  info  ][  ScanHelper   ]: ~~~     "spiConfig": 541200,
[17:18:55:367][  info  ][  ScanHelper   ]: ~~~     "sync": {
[17:18:55:367][  info  ][  ScanHelper   ]: ~~~         "interval": 32,
[17:18:55:367][  info  ][  ScanHelper   ]: ~~~         "word": 2172551550
[17:18:55:367][  info  ][  ScanHelper   ]: ~~~     },
[17:18:55:367][  info  ][  ScanHelper   ]: ~~~     "txPolarity": 0
[17:18:55:367][  info  ][  ScanHelper   ]: ~~~ }
[17:18:55:367][  info  ][    SpecCom    ]: Opening SPEC with id #0
[17:18:55:367][  info  ][    SpecCom    ]: Mapping BARs ...
[17:18:55:367][  info  ][    SpecCom    ]: ... Mapped BAR0 at 0x7fc883fa5000 with size 1048576
[17:18:55:367][warning ][    SpecCom    ]: ... BAR4 not mapped (Mmap failed)
[17:18:55:367][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[17:18:55:367][  info  ][    SpecCom    ]: Firmware Version: 0x4d9ff6d
[17:18:55:367][  info  ][    SpecCom    ]: Firmware Identifier: 0x4030232
[17:18:55:367][  info  ][    SpecCom    ]: FPGA card: PLDA XpressK7 325
[17:18:55:367][  info  ][    SpecCom    ]: FE Chip Type: RD53A/B
[17:18:55:367][  info  ][    SpecCom    ]: FMC Card Type: Ohio Card (Display Port)
[17:18:55:367][  info  ][    SpecCom    ]: RX Speed: 640Mbps
[17:18:55:367][  info  ][    SpecCom    ]: Channel Configuration: 16x1
[17:18:55:367][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[17:18:55:367][  info  ][    SpecCom    ]: Flushing buffers ...
[17:18:55:367][  info  ][    SpecCom    ]: Init success!
[17:18:55:367][  info  ][  scanConsole  ]: #######################
[17:18:55:367][  info  ][  scanConsole  ]: ##  Loading Configs  ##
[17:18:55:367][  info  ][  scanConsole  ]: #######################
[17:18:55:367][  info  ][  scanConsole  ]: Opening global config: configs/CERNQ6/example_rd53a_setup.json
[17:18:55:367][  info  ][  ScanHelper   ]: Chip type: RD53A
[17:18:55:368][  info  ][  ScanHelper   ]: Chip count 4
[17:18:55:368][  info  ][  ScanHelper   ]: Loading chip #0
[17:18:55:368][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(4)
[17:18:55:368][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ6/20UPGFC0010055.json
[17:18:55:546][  info  ][  ScanHelper   ]: Loading chip #1
[17:18:55:547][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(5)
[17:18:55:547][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ6/20UPGFC0010103.json
[17:18:55:717][  info  ][  ScanHelper   ]: Loading chip #2
[17:18:55:717][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(6)
[17:18:55:718][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ6/20UPGFC0010119.json
[17:18:55:892][  info  ][  ScanHelper   ]: Loading chip #3
[17:18:55:892][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(7)
[17:18:55:892][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ6/20UPGFC0010087.json
[17:18:56:068][  info  ][  scanConsole  ]: #################
[17:18:56:068][  info  ][  scanConsole  ]: # Configure FEs #
[17:18:56:068][  info  ][  scanConsole  ]: #################
[17:18:56:068][  info  ][  scanConsole  ]: Configuring 20UPGFC0010055
[17:18:56:099][  info  ][  scanConsole  ]: Configuring 20UPGFC0010103
[17:18:56:129][  info  ][  scanConsole  ]: Configuring 20UPGFC0010119
[17:18:56:159][  info  ][  scanConsole  ]: Configuring 20UPGFC0010087
[17:18:56:203][  info  ][  scanConsole  ]: Sent configuration to all FEs in 134 ms!
[17:18:56:204][  info  ][  scanConsole  ]: Checking com 20UPGFC0010055
[17:18:56:204][  info  ][    SpecRx     ]: Active Rx channels: 0x10
[17:18:56:204][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[17:18:56:204][  info  ][    SpecRx     ]: Rx Status 0xf0
[17:18:56:204][  info  ][    SpecRx     ]: Number of lanes: 1
[17:18:56:204][  info  ][    SpecRx     ]: Channel 4 Lane 0 synchronized!
[17:18:56:214][  info  ][  scanConsole  ]: ... success!
[17:18:56:214][  info  ][  scanConsole  ]: Checking com 20UPGFC0010103
[17:18:56:214][  info  ][    SpecRx     ]: Active Rx channels: 0x20
[17:18:56:215][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[17:18:56:215][  info  ][    SpecRx     ]: Rx Status 0xf0
[17:18:56:215][  info  ][    SpecRx     ]: Number of lanes: 1
[17:18:56:215][  info  ][    SpecRx     ]: Channel 5 Lane 0 synchronized!
[17:18:56:225][  info  ][  scanConsole  ]: ... success!
[17:18:56:225][  info  ][  scanConsole  ]: Checking com 20UPGFC0010119
[17:18:56:225][  info  ][    SpecRx     ]: Active Rx channels: 0x40
[17:18:56:225][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[17:18:56:225][  info  ][    SpecRx     ]: Rx Status 0xf0
[17:18:56:225][  info  ][    SpecRx     ]: Number of lanes: 1
[17:18:56:225][  info  ][    SpecRx     ]: Channel 6 Lane 0 synchronized!
[17:18:56:236][  info  ][  scanConsole  ]: ... success!
[17:18:56:236][  info  ][  scanConsole  ]: Checking com 20UPGFC0010087
[17:18:56:236][  info  ][    SpecRx     ]: Active Rx channels: 0x80
[17:18:56:236][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[17:18:56:236][  info  ][    SpecRx     ]: Rx Status 0xf0
[17:18:56:236][  info  ][    SpecRx     ]: Number of lanes: 1
[17:18:56:236][  info  ][    SpecRx     ]: Channel 7 Lane 0 synchronized!
[17:18:56:246][  info  ][  scanConsole  ]: ... success!
[17:18:56:246][  info  ][  scanConsole  ]: Enabling Tx channels
[17:18:56:246][  info  ][  scanConsole  ]: Enabling Tx channel 1
[17:18:56:246][  info  ][  scanConsole  ]: Enabling Tx channel 1
[17:18:56:246][  info  ][  scanConsole  ]: Enabling Tx channel 1
[17:18:56:246][  info  ][  scanConsole  ]: Enabling Tx channel 1
[17:18:56:246][  info  ][  scanConsole  ]: Enabling Rx channels
[17:18:56:246][  info  ][  scanConsole  ]: Enabling Rx channel 4
[17:18:56:247][  info  ][  scanConsole  ]: Enabling Rx channel 5
[17:18:56:247][  info  ][  scanConsole  ]: Enabling Rx channel 6
[17:18:56:247][  info  ][  scanConsole  ]: Enabling Rx channel 7
[17:18:56:247][  info  ][  scanConsole  ]: ##############
[17:18:56:247][  info  ][  scanConsole  ]: # Setup Scan #
[17:18:56:247][  info  ][  scanConsole  ]: ##############
[17:18:56:247][  info  ][  scanConsole  ]: Found Scan config, constructing scan ...
[17:18:56:247][  info  ][  ScanFactory  ]: Loading Scan:
[17:18:56:247][  info  ][  ScanFactory  ]:   Name: AnalogScan
[17:18:56:247][  info  ][  ScanFactory  ]:   Number of Loops: 3
[17:18:56:248][  info  ][  ScanFactory  ]:   Loading Loop #0
[17:18:56:248][  info  ][  ScanFactory  ]:    Type: Rd53aReadRegLoop
[17:18:56:248][  info  ][  ScanFactory  ]:    Loading loop config ... 
[17:18:56:248][  info  ][  ScanFactory  ]: ~~~ {
[17:18:56:248][  info  ][  ScanFactory  ]: ~~~     "CurMux": [],
[17:18:56:248][  info  ][  ScanFactory  ]: ~~~     "EnblRingOsc": 0,
[17:18:56:248][  info  ][  ScanFactory  ]: ~~~     "Registers": [],
[17:18:56:248][  info  ][  ScanFactory  ]: ~~~     "RingOscDur": 9,
[17:18:56:248][  info  ][  ScanFactory  ]: ~~~     "RingOscRep": 10,
[17:18:56:248][  info  ][  ScanFactory  ]: ~~~     "VoltMux": [3, 5, 7, 15]
[17:18:56:248][  info  ][  ScanFactory  ]: ~~~ }
[17:18:56:248][  info  ][  ScanFactory  ]:   Loading Loop #1
[17:18:56:248][  info  ][  ScanFactory  ]:    Type: Rd53aTriggerLoop
[17:18:56:248][  info  ][  ScanFactory  ]:    Loading loop config ... 
[17:18:56:248][  info  ][  ScanFactory  ]: ~~~ {
[17:18:56:248][  info  ][  ScanFactory  ]: ~~~     "count": 1,
[17:18:56:248][  info  ][  ScanFactory  ]: ~~~     "delay": 48,
[17:18:56:248][  info  ][  ScanFactory  ]: ~~~     "extTrig": false,
[17:18:56:248][  info  ][  ScanFactory  ]: ~~~     "frequency": 30000.0,
[17:18:56:248][  info  ][  ScanFactory  ]: ~~~     "noInject": false,
[17:18:56:248][  info  ][  ScanFactory  ]: ~~~     "sendEcr": false,
[17:18:56:248][  info  ][  ScanFactory  ]: ~~~     "time": 0.0,
[17:18:56:248][  info  ][  ScanFactory  ]: ~~~     "trigMultiplier": 16
[17:18:56:248][  info  ][  ScanFactory  ]: ~~~ }
[17:18:56:248][  info  ][  ScanFactory  ]:   Loading Loop #2
[17:18:56:248][  info  ][  ScanFactory  ]:    Type: StdDataLoop
[17:18:56:248][warning ][  ScanFactory  ]: ~~~ Config empty.
[17:18:56:248][  info  ][ScanBuildHistogrammers]: Loading histogrammer ...
[17:18:56:249][  info  ][ScanBuildHistogrammers]: ... done!
[17:18:56:249][  info  ][ScanBuildAnalyses]: Loading analyses ...
[17:18:56:249][  info  ][  scanConsole  ]: Running pre scan!
[17:18:56:249][  info  ][  ScanFactory  ]: Entering pre scan phase ...
[17:18:56:249][  info  ][     Rd53a     ]: Write named register: GlobalPulseRt -> 16384
[17:18:56:249][  info  ][     Rd53a     ]: Write named register: InjEnDig -> 0
[17:18:56:249][  info  ][     Rd53a     ]: Write named register: InjVcalDiff -> 0
[17:18:56:249][  info  ][     Rd53a     ]: Write named register: InjVcalHigh -> 3500
[17:18:56:249][  info  ][     Rd53a     ]: Write named register: InjVcalMed -> 500
[17:18:56:249][  info  ][     Rd53a     ]: Write named register: LatencyConfig -> 48
[17:18:56:249][  info  ][  scanConsole  ]: Starting histogrammer and analysis threads:
[17:18:56:250][  info  ][  scanConsole  ]:  .. started threads of Fe 4
[17:18:56:250][  info  ][  scanConsole  ]:  .. started threads of Fe 5
[17:18:56:250][  info  ][  scanConsole  ]:  .. started threads of Fe 6
[17:18:56:250][  info  ][  scanConsole  ]:  .. started threads of Fe 7
[17:18:56:250][  info  ][Rd53aDataProcessor]:   -> Processor thread #0 started!
[17:18:56:250][  info  ][Rd53aDataProcessor]:   -> Processor thread #1 started!
[17:18:56:250][  info  ][Rd53aDataProcessor]:   -> Processor thread #2 started!
[17:18:56:250][  info  ][Rd53aDataProcessor]:   -> Processor thread #3 started!
[17:18:56:250][  info  ][  scanConsole  ]: ########
[17:18:56:251][  info  ][  scanConsole  ]: # Scan #
[17:18:56:251][  info  ][  scanConsole  ]: ########
[17:18:56:251][  info  ][  scanConsole  ]: Starting scan!
[17:18:56:251][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0010055 on Rx 4
[17:18:56:255][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0010055] MON MUX_V: 3 Bias 0 1268 -> 0.278076 V Bias 1 1652 -> 0.354451 V, Temperature 0.076375 -> -12.3625 C
[17:18:56:259][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0010055] MON MUX_V: 5 Bias 0 1245 -> 0.273501 V Bias 1 1628 -> 0.349678 V, Temperature 0.0761762 -> -25.642 C
[17:18:56:263][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0010055] MON MUX_V: 7 Bias 0 1232 -> 0.270916 V Bias 1 1616 -> 0.347291 V, Temperature 0.076375 -> -23.9053 C
[17:18:56:268][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0010055] MON MUX_V: 15 Bias 0 1211 -> 0.266739 V Bias 1 1603 -> 0.344705 V, Temperature 0.0779662 -> -24.9668 C
[17:18:56:302][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0010103 on Rx 5
[17:18:56:307][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0010103] MON MUX_V: 3 Bias 0 1282 -> 0.278427 V Bias 1 1670 -> 0.355763 V, Temperature 0.0773358 -> -6.63344 C
[17:18:56:311][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0010103] MON MUX_V: 5 Bias 0 1231 -> 0.268262 V Bias 1 1622 -> 0.346196 V, Temperature 0.0779337 -> -8.76013 C
[17:18:56:315][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0010103] MON MUX_V: 7 Bias 0 1212 -> 0.264475 V Bias 1 1614 -> 0.344601 V, Temperature 0.0801263 -> -10.3 C
[17:18:56:320][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0010103] MON MUX_V: 15 Bias 0 1229 -> 0.267863 V Bias 1 1625 -> 0.346794 V, Temperature 0.0789304 -> -9.82166 C
0[17:18:56:354][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0010119 on Rx 6
[17:18:56:359][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0010119] MON MUX_V: 3 Bias 0 1265 -> 0.274799 V Bias 1 1660 -> 0.352793 V, Temperature 0.0779931 -> -8.60767 C
[17:18:56:363][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0010119] MON MUX_V: 5 Bias 0 1251 -> 0.272035 V Bias 1 1643 -> 0.349436 V, Temperature 0.0774007 -> -28.0941 C
[17:18:56:367][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0010119] MON MUX_V: 7 Bias 0 1233 -> 0.268481 V Bias 1 1631 -> 0.347067 V, Temperature 0.0785854 -> -22.1684 C
[17:18:56:371][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0010119] MON MUX_V: 15 Bias 0 1223 -> 0.266507 V Bias 1 1619 -> 0.344697 V, Temperature 0.0781905 -> -24.05 C
[17:18:56:406][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0010087 on Rx 7
[17:18:56:411][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0010087] MON MUX_V: 3 Bias 0 1287 -> 0.278712 V Bias 1 1668 -> 0.354322 V, Temperature 0.0756095 -> -7.85548 C
[17:18:56:415][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0010087] MON MUX_V: 5 Bias 0 1254 -> 0.272163 V Bias 1 1649 -> 0.350551 V, Temperature 0.0783878 -> -15.8 C
[17:18:56:419][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0010087] MON MUX_V: 7 Bias 0 1242 -> 0.269782 V Bias 1 1642 -> 0.349162 V, Temperature 0.07938 -> -17.3714 C
[17:18:56:424][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0010087] MON MUX_V: 15 Bias 0 1242 -> 0.269782 V Bias 1 1640 -> 0.348765 V, Temperature 0.0789831 -> -13.4087 C
[17:18:56:460][  info  ][  scanConsole  ]: Scan done!
[17:18:56:460][  info  ][  scanConsole  ]: Waiting for processors to finish ...
[17:18:56:461][  info  ][  scanConsole  ]: Processor done, waiting for histogrammer ...
[17:18:56:861][  info  ][HistogramAlgorithm]: Histogrammer done!
[17:18:56:861][  info  ][HistogramAlgorithm]: Histogrammer done!
[17:18:56:861][  info  ][HistogramAlgorithm]: Histogrammer done!
[17:18:56:861][  info  ][HistogramAlgorithm]: Histogrammer done!
[17:18:56:861][  info  ][  scanConsole  ]: Processor done, waiting for analysis ...
[17:18:57:261][  info  ][AnalysisAlgorithm]: Analysis done!
[17:18:57:261][  info  ][AnalysisAlgorithm]: Analysis done!
[17:18:57:261][  info  ][AnalysisAlgorithm]: Analysis done!
[17:18:57:261][  info  ][AnalysisAlgorithm]: Analysis done!
[17:18:57:262][  info  ][  scanConsole  ]: All done!
[17:18:57:262][  info  ][  scanConsole  ]: ##########
[17:18:57:262][  info  ][  scanConsole  ]: # Timing #
[17:18:57:262][  info  ][  scanConsole  ]: ##########
[17:18:57:262][  info  ][  scanConsole  ]: -> Configuration: 134 ms
[17:18:57:262][  info  ][  scanConsole  ]: -> Scan:          209 ms
[17:18:57:262][  info  ][  scanConsole  ]: -> Processing:    1 ms
[17:18:57:262][  info  ][  scanConsole  ]: -> Analysis:      800 ms
[17:18:57:262][  info  ][  scanConsole  ]: ###########
[17:18:57:262][  info  ][  scanConsole  ]: # Cleanup #
[17:18:57:262][  info  ][  scanConsole  ]: ###########
[17:18:57:270][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0010055 to configs/CERNQ6/20UPGFC0010055.json
[17:18:57:475][  info  ][  scanConsole  ]: -> Storing output results of FE 4
[17:18:57:475][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0010055, this usually means that the chip did not send any data at all.
[17:18:57:476][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0010103 to configs/CERNQ6/20UPGFC0010103.json
[17:18:57:642][  info  ][  scanConsole  ]: -> Storing output results of FE 5
[17:18:57:642][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0010103, this usually means that the chip did not send any data at all.
[17:18:57:643][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0010119 to configs/CERNQ6/20UPGFC0010119.json
[17:18:57:808][  info  ][  scanConsole  ]: -> Storing output results of FE 6
[17:18:57:808][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0010119, this usually means that the chip did not send any data at all.
[17:18:57:810][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0010087 to configs/CERNQ6/20UPGFC0010087.json
[17:18:57:979][  info  ][  scanConsole  ]: -> Storing output results of FE 7
[17:18:57:979][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0010087, this usually means that the chip did not send any data at all.
[17:18:57:980][  info  ][  scanConsole  ]: Finishing run: 1532
[root@jollyroger Yarr]# 
