[17:25:23:040][  info  ][               ]: #####################################
[17:25:23:041][  info  ][               ]: # Welcome to the YARR Scan Console! #
[17:25:23:041][  info  ][               ]: #####################################
[17:25:23:041][  info  ][               ]: -> Parsing command line parameters ...
[17:25:23:041][  info  ][               ]: Configuring logger ...
[17:25:23:050][  info  ][  scanConsole  ]: Scan Type/Config configs/scans/rd53a/reg_readtemp.json
[17:25:23:050][  info  ][  scanConsole  ]: Connectivity:
[17:25:23:050][  info  ][  scanConsole  ]:     configs/CERNQ6/example_rd53a_setup.json
[17:25:23:050][  info  ][  scanConsole  ]: Target ToT: -1
[17:25:23:050][  info  ][  scanConsole  ]: Target Charge: -1
[17:25:23:050][  info  ][  scanConsole  ]: Output Plots: false
[17:25:23:050][  info  ][  scanConsole  ]: Output Directory: ./data/001536_reg_readtemp/
[17:25:23:069][  info  ][  scanConsole  ]: Timestamp: 2022-08-20_17:25:23
[17:25:23:069][  info  ][  scanConsole  ]: Run Number: 1536
[17:25:23:069][  info  ][  scanConsole  ]: #################
[17:25:23:069][  info  ][  scanConsole  ]: # Init Hardware #
[17:25:23:069][  info  ][  scanConsole  ]: #################
[17:25:23:069][  info  ][  scanConsole  ]: -> Opening controller config: configs/controller/specCfg-rd53a-16x1.json
[17:25:23:069][  info  ][  ScanHelper   ]: Loading controller ...
[17:25:23:069][  info  ][  ScanHelper   ]: Found controller of type: spec
[17:25:23:069][  info  ][  ScanHelper   ]: ... loading controler config:
[17:25:23:069][  info  ][  ScanHelper   ]: ~~~ {
[17:25:23:069][  info  ][  ScanHelper   ]: ~~~     "cmdPeriod": 6.250000073038109e-9,
[17:25:23:069][  info  ][  ScanHelper   ]: ~~~     "idle": {
[17:25:23:069][  info  ][  ScanHelper   ]: ~~~         "word": 1768515945
[17:25:23:069][  info  ][  ScanHelper   ]: ~~~     },
[17:25:23:069][  info  ][  ScanHelper   ]: ~~~     "pulse": {
[17:25:23:070][  info  ][  ScanHelper   ]: ~~~         "interval": 500,
[17:25:23:070][  info  ][  ScanHelper   ]: ~~~         "word": 1549575846
[17:25:23:070][  info  ][  ScanHelper   ]: ~~~     },
[17:25:23:070][  info  ][  ScanHelper   ]: ~~~     "rxActiveLanes": 1,
[17:25:23:070][  info  ][  ScanHelper   ]: ~~~     "rxPolarity": 0,
[17:25:23:070][  info  ][  ScanHelper   ]: ~~~     "specNum": 0,
[17:25:23:070][  info  ][  ScanHelper   ]: ~~~     "spiConfig": 541200,
[17:25:23:070][  info  ][  ScanHelper   ]: ~~~     "sync": {
[17:25:23:070][  info  ][  ScanHelper   ]: ~~~         "interval": 32,
[17:25:23:070][  info  ][  ScanHelper   ]: ~~~         "word": 2172551550
[17:25:23:070][  info  ][  ScanHelper   ]: ~~~     },
[17:25:23:070][  info  ][  ScanHelper   ]: ~~~     "txPolarity": 0
[17:25:23:070][  info  ][  ScanHelper   ]: ~~~ }
[17:25:23:070][  info  ][    SpecCom    ]: Opening SPEC with id #0
[17:25:23:070][  info  ][    SpecCom    ]: Mapping BARs ...
[17:25:23:070][  info  ][    SpecCom    ]: ... Mapped BAR0 at 0x7fa1ed8b6000 with size 1048576
[17:25:23:070][warning ][    SpecCom    ]: ... BAR4 not mapped (Mmap failed)
[17:25:23:071][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[17:25:23:071][  info  ][    SpecCom    ]: Firmware Version: 0x4d9ff6d
[17:25:23:071][  info  ][    SpecCom    ]: Firmware Identifier: 0x4030232
[17:25:23:071][  info  ][    SpecCom    ]: FPGA card: PLDA XpressK7 325
[17:25:23:071][  info  ][    SpecCom    ]: FE Chip Type: RD53A/B
[17:25:23:071][  info  ][    SpecCom    ]: FMC Card Type: Ohio Card (Display Port)
[17:25:23:071][  info  ][    SpecCom    ]: RX Speed: 640Mbps
[17:25:23:071][  info  ][    SpecCom    ]: Channel Configuration: 16x1
[17:25:23:071][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[17:25:23:071][  info  ][    SpecCom    ]: Flushing buffers ...
[17:25:23:071][  info  ][    SpecCom    ]: Init success!
[17:25:23:071][  info  ][  scanConsole  ]: #######################
[17:25:23:071][  info  ][  scanConsole  ]: ##  Loading Configs  ##
[17:25:23:071][  info  ][  scanConsole  ]: #######################
[17:25:23:071][  info  ][  scanConsole  ]: Opening global config: configs/CERNQ6/example_rd53a_setup.json
[17:25:23:071][  info  ][  ScanHelper   ]: Chip type: RD53A
[17:25:23:071][  info  ][  ScanHelper   ]: Chip count 4
[17:25:23:071][  info  ][  ScanHelper   ]: Loading chip #0
[17:25:23:075][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(4)
[17:25:23:075][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ6/20UPGFC0010055.json
[17:25:23:278][  info  ][  ScanHelper   ]: Loading chip #1
[17:25:23:279][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(5)
[17:25:23:279][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ6/20UPGFC0010103.json
[17:25:23:450][  info  ][  ScanHelper   ]: Loading chip #2
[17:25:23:451][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(6)
[17:25:23:451][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ6/20UPGFC0010119.json
[17:25:23:626][  info  ][  ScanHelper   ]: Loading chip #3
[17:25:23:627][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(7)
[17:25:23:627][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ6/20UPGFC0010087.json
[17:25:23:804][  info  ][  scanConsole  ]: #################
[17:25:23:804][  info  ][  scanConsole  ]: # Configure FEs #
[17:25:23:804][  info  ][  scanConsole  ]: #################
[17:25:23:804][  info  ][  scanConsole  ]: Configuring 20UPGFC0010055
[17:25:23:835][  info  ][  scanConsole  ]: Configuring 20UPGFC0010103
[17:25:23:865][  info  ][  scanConsole  ]: Configuring 20UPGFC0010119
[17:25:23:906][  info  ][  scanConsole  ]: Configuring 20UPGFC0010087
[17:25:23:936][  info  ][  scanConsole  ]: Sent configuration to all FEs in 131 ms!
[17:25:23:937][  info  ][  scanConsole  ]: Checking com 20UPGFC0010055
[17:25:23:937][  info  ][    SpecRx     ]: Active Rx channels: 0x10
[17:25:23:937][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[17:25:23:937][  info  ][    SpecRx     ]: Rx Status 0xf0
[17:25:23:937][  info  ][    SpecRx     ]: Number of lanes: 1
[17:25:23:937][  info  ][    SpecRx     ]: Channel 4 Lane 0 synchronized!
[17:25:23:948][  info  ][  scanConsole  ]: ... success!
[17:25:23:948][  info  ][  scanConsole  ]: Checking com 20UPGFC0010103
[17:25:23:948][  info  ][    SpecRx     ]: Active Rx channels: 0x20
[17:25:23:948][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[17:25:23:948][  info  ][    SpecRx     ]: Rx Status 0xf0
[17:25:23:948][  info  ][    SpecRx     ]: Number of lanes: 1
[17:25:23:948][  info  ][    SpecRx     ]: Channel 5 Lane 0 synchronized!
[17:25:23:958][  info  ][  scanConsole  ]: ... success!
[17:25:23:958][  info  ][  scanConsole  ]: Checking com 20UPGFC0010119
[17:25:23:958][  info  ][    SpecRx     ]: Active Rx channels: 0x40
[17:25:23:958][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[17:25:23:958][  info  ][    SpecRx     ]: Rx Status 0xf0
[17:25:23:958][  info  ][    SpecRx     ]: Number of lanes: 1
[17:25:23:958][  info  ][    SpecRx     ]: Channel 6 Lane 0 synchronized!
[17:25:23:969][  info  ][  scanConsole  ]: ... success!
[17:25:23:969][  info  ][  scanConsole  ]: Checking com 20UPGFC0010087
[17:25:23:969][  info  ][    SpecRx     ]: Active Rx channels: 0x80
[17:25:23:969][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[17:25:23:969][  info  ][    SpecRx     ]: Rx Status 0xf0
[17:25:23:969][  info  ][    SpecRx     ]: Number of lanes: 1
[17:25:23:969][  info  ][    SpecRx     ]: Channel 7 Lane 0 synchronized!
[17:25:23:979][  info  ][  scanConsole  ]: ... success!
[17:25:23:979][  info  ][  scanConsole  ]: Enabling Tx channels
[17:25:23:980][  info  ][  scanConsole  ]: Enabling Tx channel 1
[17:25:23:980][  info  ][  scanConsole  ]: Enabling Tx channel 1
[17:25:23:980][  info  ][  scanConsole  ]: Enabling Tx channel 1
[17:25:23:980][  info  ][  scanConsole  ]: Enabling Tx channel 1
[17:25:23:980][  info  ][  scanConsole  ]: Enabling Rx channels
[17:25:23:980][  info  ][  scanConsole  ]: Enabling Rx channel 4
[17:25:23:980][  info  ][  scanConsole  ]: Enabling Rx channel 5
[17:25:23:980][  info  ][  scanConsole  ]: Enabling Rx channel 6
[17:25:23:980][  info  ][  scanConsole  ]: Enabling Rx channel 7
[17:25:23:980][  info  ][  scanConsole  ]: ##############
[17:25:23:980][  info  ][  scanConsole  ]: # Setup Scan #
[17:25:23:980][  info  ][  scanConsole  ]: ##############
[17:25:23:980][  info  ][  scanConsole  ]: Found Scan config, constructing scan ...
[17:25:23:981][  info  ][  ScanFactory  ]: Loading Scan:
[17:25:23:981][  info  ][  ScanFactory  ]:   Name: AnalogScan
[17:25:23:981][  info  ][  ScanFactory  ]:   Number of Loops: 3
[17:25:23:981][  info  ][  ScanFactory  ]:   Loading Loop #0
[17:25:23:981][  info  ][  ScanFactory  ]:    Type: Rd53aReadRegLoop
[17:25:23:981][  info  ][  ScanFactory  ]:    Loading loop config ... 
[17:25:23:981][  info  ][  ScanFactory  ]: ~~~ {
[17:25:23:981][  info  ][  ScanFactory  ]: ~~~     "CurMux": [],
[17:25:23:981][  info  ][  ScanFactory  ]: ~~~     "EnblRingOsc": 0,
[17:25:23:981][  info  ][  ScanFactory  ]: ~~~     "Registers": [],
[17:25:23:981][  info  ][  ScanFactory  ]: ~~~     "RingOscDur": 9,
[17:25:23:981][  info  ][  ScanFactory  ]: ~~~     "RingOscRep": 10,
[17:25:23:981][  info  ][  ScanFactory  ]: ~~~     "VoltMux": [3, 5, 7, 15]
[17:25:23:981][  info  ][  ScanFactory  ]: ~~~ }
[17:25:23:981][  info  ][  ScanFactory  ]:   Loading Loop #1
[17:25:23:981][  info  ][  ScanFactory  ]:    Type: Rd53aTriggerLoop
[17:25:23:981][  info  ][  ScanFactory  ]:    Loading loop config ... 
[17:25:23:981][  info  ][  ScanFactory  ]: ~~~ {
[17:25:23:981][  info  ][  ScanFactory  ]: ~~~     "count": 1,
[17:25:23:981][  info  ][  ScanFactory  ]: ~~~     "delay": 48,
[17:25:23:981][  info  ][  ScanFactory  ]: ~~~     "extTrig": false,
[17:25:23:981][  info  ][  ScanFactory  ]: ~~~     "frequency": 30000.0,
[17:25:23:981][  info  ][  ScanFactory  ]: ~~~     "noInject": false,
[17:25:23:981][  info  ][  ScanFactory  ]: ~~~     "sendEcr": false,
[17:25:23:981][  info  ][  ScanFactory  ]: ~~~     "time": 0.0,
[17:25:23:981][  info  ][  ScanFactory  ]: ~~~     "trigMultiplier": 16
[17:25:23:981][  info  ][  ScanFactory  ]: ~~~ }
[17:25:23:981][  info  ][  ScanFactory  ]:   Loading Loop #2
[17:25:23:982][  info  ][  ScanFactory  ]:    Type: StdDataLoop
[17:25:23:982][warning ][  ScanFactory  ]: ~~~ Config empty.
[17:25:23:982][  info  ][ScanBuildHistogrammers]: Loading histogrammer ...
[17:25:23:982][  info  ][ScanBuildHistogrammers]: ... done!
[17:25:23:982][  info  ][ScanBuildAnalyses]: Loading analyses ...
[17:25:23:982][  info  ][  scanConsole  ]: Running pre scan!
[17:25:23:982][  info  ][  ScanFactory  ]: Entering pre scan phase ...
[17:25:23:982][  info  ][     Rd53a     ]: Write named register: GlobalPulseRt -> 16384
[17:25:23:982][  info  ][     Rd53a     ]: Write named register: InjEnDig -> 0
[17:25:23:982][  info  ][     Rd53a     ]: Write named register: InjVcalDiff -> 0
[17:25:23:982][  info  ][     Rd53a     ]: Write named register: InjVcalHigh -> 3500
[17:25:23:982][  info  ][     Rd53a     ]: Write named register: InjVcalMed -> 500
[17:25:23:983][  info  ][     Rd53a     ]: Write named register: LatencyConfig -> 48
[17:25:23:983][  info  ][  scanConsole  ]: Starting histogrammer and analysis threads:
[17:25:23:983][  info  ][  scanConsole  ]:  .. started threads of Fe 4
[17:25:23:983][  info  ][  scanConsole  ]:  .. started threads of Fe 5
[17:25:23:983][  info  ][  scanConsole  ]:  .. started threads of Fe 6
[17:25:23:983][  info  ][  scanConsole  ]:  .. started threads of Fe 7
[17:25:23:983][  info  ][Rd53aDataProcessor]:   -> Processor thread #0 started!
[17:25:23:983][  info  ][Rd53aDataProcessor]:   -> Processor thread #1 started!
[17:25:23:984][  info  ][Rd53aDataProcessor]:   -> Processor thread #2 started!
[17:25:23:984][  info  ][Rd53aDataProcessor]:   -> Processor thread #3 started!
[17:25:23:984][  info  ][  scanConsole  ]: ########
[17:25:23:984][  info  ][  scanConsole  ]: # Scan #
[17:25:23:984][  info  ][  scanConsole  ]: ########
[17:25:23:984][  info  ][  scanConsole  ]: Starting scan!
[17:25:23:984][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0010055 on Rx 4
[17:25:23:988][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0010055] MON MUX_V: 3 Bias 0 1267 -> 0.277877 V Bias 1 1652 -> 0.354451 V, Temperature 0.0765739 -> -11.675 C
[17:25:23:992][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0010055] MON MUX_V: 5 Bias 0 1246 -> 0.2737 V Bias 1 1629 -> 0.349876 V, Temperature 0.0761761 -> -25.6422 C
[17:25:23:996][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0010055] MON MUX_V: 7 Bias 0 1232 -> 0.270916 V Bias 1 1616 -> 0.347291 V, Temperature 0.076375 -> -23.9053 C
[17:25:24:000][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0010055] MON MUX_V: 15 Bias 0 1211 -> 0.266739 V Bias 1 1604 -> 0.344904 V, Temperature 0.0781651 -> -24.05 C
[17:25:24:035][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0010103 on Rx 5
[17:25:24:039][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0010103] MON MUX_V: 3 Bias 0 1283 -> 0.278626 V Bias 1 1670 -> 0.355763 V, Temperature 0.0771365 -> -7.24454 C
[17:25:24:043][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0010103] MON MUX_V: 5 Bias 0 1232 -> 0.268461 V Bias 1 1623 -> 0.346395 V, Temperature 0.0779338 -> -8.75995 C
[17:25:24:047][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0010103] MON MUX_V: 7 Bias 0 1212 -> 0.264475 V Bias 1 1615 -> 0.3448 V, Temperature 0.0803256 -> -9.61246 C
[17:25:24:051][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0010103] MON MUX_V: 15 Bias 0 1230 -> 0.268063 V Bias 1 1624 -> 0.346594 V, Temperature 0.0785317 -> -11.2566 C
[17:25:24:086][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0010119 on Rx 6
[17:25:24:090][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0010119] MON MUX_V: 3 Bias 0 1265 -> 0.274799 V Bias 1 1659 -> 0.352595 V, Temperature 0.0777957 -> -9.24225 C
[17:25:24:094][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0010119] MON MUX_V: 5 Bias 0 1252 -> 0.272233 V Bias 1 1643 -> 0.349436 V, Temperature 0.0772033 -> -29.0646 C
[17:25:24:099][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0010119] MON MUX_V: 7 Bias 0 1232 -> 0.268284 V Bias 1 1631 -> 0.347067 V, Temperature 0.0787829 -> -21.3 C
[17:25:24:103][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0010119] MON MUX_V: 15 Bias 0 1224 -> 0.266704 V Bias 1 1618 -> 0.3445 V, Temperature 0.0777956 -> -25.8834 C
[17:25:24:138][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0010087 on Rx 7
[17:25:24:142][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0010087] MON MUX_V: 3 Bias 0 1287 -> 0.278712 V Bias 1 1668 -> 0.354322 V, Temperature 0.0756095 -> -7.85548 C
[17:25:24:146][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0010087] MON MUX_V: 5 Bias 0 1255 -> 0.272362 V Bias 1 1649 -> 0.350551 V, Temperature 0.0781893 -> -16.5857 C
[17:25:24:150][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0010087] MON MUX_V: 7 Bias 0 1243 -> 0.26998 V Bias 1 1642 -> 0.349162 V, Temperature 0.0791816 -> -18.1571 C
[17:25:24:154][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0010087] MON MUX_V: 15 Bias 0 1244 -> 0.270179 V Bias 1 1641 -> 0.348963 V, Temperature 0.0787847 -> -14.1261 C
[17:25:24:190][  info  ][  scanConsole  ]: Scan done!
[17:25:24:190][  info  ][  scanConsole  ]: Waiting for processors to finish ...
[17:25:24:191][  info  ][  scanConsole  ]: Processor done, waiting for histogrammer ...
[17:25:24:591][  info  ][HistogramAlgorithm]: Histogrammer done!
[17:25:24:591][  info  ][HistogramAlgorithm]: Histogrammer done!
[17:25:24:591][  info  ][HistogramAlgorithm]: Histogrammer done!
[17:25:24:591][  info  ][HistogramAlgorithm]: Histogrammer done!
[17:25:24:591][  info  ][  scanConsole  ]: Processor done, waiting for analysis ...
[17:25:24:991][  info  ][AnalysisAlgorithm]: Analysis done!
[17:25:24:991][  info  ][AnalysisAlgorithm]: Analysis done!
[17:25:24:991][  info  ][AnalysisAlgorithm]: Analysis done!
[17:25:24:991][  info  ][AnalysisAlgorithm]: Analysis done!
[17:25:24:992][  info  ][  scanConsole  ]: All done!
[17:25:24:992][  info  ][  scanConsole  ]: ##########
[17:25:24:992][  info  ][  scanConsole  ]: # Timing #
[17:25:24:992][  info  ][  scanConsole  ]: ##########
[17:25:24:992][  info  ][  scanConsole  ]: -> Configuration: 131 ms
[17:25:24:992][  info  ][  scanConsole  ]: -> Scan:          206 ms
[17:25:24:992][  info  ][  scanConsole  ]: -> Processing:    0 ms
[17:25:24:992][  info  ][  scanConsole  ]: -> Analysis:      800 ms
[17:25:24:992][  info  ][  scanConsole  ]: ###########
[17:25:24:992][  info  ][  scanConsole  ]: # Cleanup #
[17:25:24:992][  info  ][  scanConsole  ]: ###########
[17:25:25:000][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0010055 to configs/CERNQ6/20UPGFC0010055.json
[17:25:25:206][  info  ][  scanConsole  ]: -> Storing output results of FE 4
[17:25:25:206][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0010055, this usually means that the chip did not send any data at all.
[17:25:25:207][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0010103 to configs/CERNQ6/20UPGFC0010103.json
[17:25:25:370][  info  ][  scanConsole  ]: -> Storing output results of FE 5
[17:25:25:370][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0010103, this usually means that the chip did not send any data at all.
[17:25:25:371][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0010119 to configs/CERNQ6/20UPGFC0010119.json
[17:25:25:536][  info  ][  scanConsole  ]: -> Storing output results of FE 6
[17:25:25:536][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0010119, this usually means that the chip did not send any data at all.
[17:25:25:537][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0010087 to configs/CERNQ6/20UPGFC0010087.json
[17:25:25:700][  info  ][  scanConsole  ]: -> Storing output results of FE 7
[17:25:25:700][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0010087, this usually means that the chip did not send any data at all.
[17:25:25:701][  info  ][  scanConsole  ]: Finishing run: 1536
[root@jollyroger Yarr]# 
