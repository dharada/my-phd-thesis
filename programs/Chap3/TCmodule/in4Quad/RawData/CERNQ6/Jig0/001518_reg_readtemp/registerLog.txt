[16:14:15:953][  info  ][               ]: #####################################
[16:14:15:953][  info  ][               ]: # Welcome to the YARR Scan Console! #
[16:14:15:953][  info  ][               ]: #####################################
[16:14:15:953][  info  ][               ]: -> Parsing command line parameters ...
[16:14:15:953][  info  ][               ]: Configuring logger ...
[16:14:15:962][  info  ][  scanConsole  ]: Scan Type/Config configs/scans/rd53a/reg_readtemp.json
[16:14:15:962][  info  ][  scanConsole  ]: Connectivity:
[16:14:15:962][  info  ][  scanConsole  ]:     configs/CERNQ6/example_rd53a_setup.json
[16:14:15:962][  info  ][  scanConsole  ]: Target ToT: -1
[16:14:15:962][  info  ][  scanConsole  ]: Target Charge: -1
[16:14:15:962][  info  ][  scanConsole  ]: Output Plots: false
[16:14:15:962][  info  ][  scanConsole  ]: Output Directory: ./data/001518_reg_readtemp/
[16:14:15:981][  info  ][  scanConsole  ]: Timestamp: 2022-08-20_16:14:15
[16:14:15:981][  info  ][  scanConsole  ]: Run Number: 1518
[16:14:15:981][  info  ][  scanConsole  ]: #################
[16:14:15:981][  info  ][  scanConsole  ]: # Init Hardware #
[16:14:15:981][  info  ][  scanConsole  ]: #################
[16:14:15:981][  info  ][  scanConsole  ]: -> Opening controller config: configs/controller/specCfg-rd53a-16x1.json
[16:14:15:982][  info  ][  ScanHelper   ]: Loading controller ...
[16:14:15:982][  info  ][  ScanHelper   ]: Found controller of type: spec
[16:14:15:982][  info  ][  ScanHelper   ]: ... loading controler config:
[16:14:15:982][  info  ][  ScanHelper   ]: ~~~ {
[16:14:15:982][  info  ][  ScanHelper   ]: ~~~     "cmdPeriod": 6.250000073038109e-9,
[16:14:15:982][  info  ][  ScanHelper   ]: ~~~     "idle": {
[16:14:15:982][  info  ][  ScanHelper   ]: ~~~         "word": 1768515945
[16:14:15:982][  info  ][  ScanHelper   ]: ~~~     },
[16:14:15:982][  info  ][  ScanHelper   ]: ~~~     "pulse": {
[16:14:15:982][  info  ][  ScanHelper   ]: ~~~         "interval": 500,
[16:14:15:982][  info  ][  ScanHelper   ]: ~~~         "word": 1549575846
[16:14:15:982][  info  ][  ScanHelper   ]: ~~~     },
[16:14:15:982][  info  ][  ScanHelper   ]: ~~~     "rxActiveLanes": 1,
[16:14:15:982][  info  ][  ScanHelper   ]: ~~~     "rxPolarity": 0,
[16:14:15:982][  info  ][  ScanHelper   ]: ~~~     "specNum": 0,
[16:14:15:982][  info  ][  ScanHelper   ]: ~~~     "spiConfig": 541200,
[16:14:15:982][  info  ][  ScanHelper   ]: ~~~     "sync": {
[16:14:15:982][  info  ][  ScanHelper   ]: ~~~         "interval": 32,
[16:14:15:982][  info  ][  ScanHelper   ]: ~~~         "word": 2172551550
[16:14:15:982][  info  ][  ScanHelper   ]: ~~~     },
[16:14:15:982][  info  ][  ScanHelper   ]: ~~~     "txPolarity": 0
[16:14:15:982][  info  ][  ScanHelper   ]: ~~~ }
[16:14:15:982][  info  ][    SpecCom    ]: Opening SPEC with id #0
[16:14:15:982][  info  ][    SpecCom    ]: Mapping BARs ...
[16:14:15:982][  info  ][    SpecCom    ]: ... Mapped BAR0 at 0x7f4ce7625000 with size 1048576
[16:14:15:982][warning ][    SpecCom    ]: ... BAR4 not mapped (Mmap failed)
[16:14:15:983][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[16:14:15:983][  info  ][    SpecCom    ]: Firmware Version: 0x4d9ff6d
[16:14:15:983][  info  ][    SpecCom    ]: Firmware Identifier: 0x4030232
[16:14:15:983][  info  ][    SpecCom    ]: FPGA card: PLDA XpressK7 325
[16:14:15:983][  info  ][    SpecCom    ]: FE Chip Type: RD53A/B
[16:14:15:983][  info  ][    SpecCom    ]: FMC Card Type: Ohio Card (Display Port)
[16:14:15:983][  info  ][    SpecCom    ]: RX Speed: 640Mbps
[16:14:15:983][  info  ][    SpecCom    ]: Channel Configuration: 16x1
[16:14:15:983][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[16:14:15:983][  info  ][    SpecCom    ]: Flushing buffers ...
[16:14:15:983][  info  ][    SpecCom    ]: Init success!
[16:14:15:983][  info  ][  scanConsole  ]: #######################
[16:14:15:983][  info  ][  scanConsole  ]: ##  Loading Configs  ##
[16:14:15:983][  info  ][  scanConsole  ]: #######################
[16:14:15:983][  info  ][  scanConsole  ]: Opening global config: configs/CERNQ6/example_rd53a_setup.json
[16:14:15:983][  info  ][  ScanHelper   ]: Chip type: RD53A
[16:14:15:983][  info  ][  ScanHelper   ]: Chip count 4
[16:14:15:983][  info  ][  ScanHelper   ]: Loading chip #0
[16:14:15:986][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(4)
[16:14:15:986][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ6/20UPGFC0010055.json
[16:14:16:191][  info  ][  ScanHelper   ]: Loading chip #1
[16:14:16:191][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(5)
[16:14:16:191][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ6/20UPGFC0010103.json
[16:14:16:366][  info  ][  ScanHelper   ]: Loading chip #2
[16:14:16:366][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(6)
[16:14:16:366][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ6/20UPGFC0010119.json
[16:14:16:541][  info  ][  ScanHelper   ]: Loading chip #3
[16:14:16:541][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(7)
[16:14:16:541][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ6/20UPGFC0010087.json
[16:14:16:716][  info  ][  scanConsole  ]: #################
[16:14:16:716][  info  ][  scanConsole  ]: # Configure FEs #
[16:14:16:716][  info  ][  scanConsole  ]: #################
[16:14:16:716][  info  ][  scanConsole  ]: Configuring 20UPGFC0010055
[16:14:16:747][  info  ][  scanConsole  ]: Configuring 20UPGFC0010103
[16:14:16:791][  info  ][  scanConsole  ]: Configuring 20UPGFC0010119
[16:14:16:825][  info  ][  scanConsole  ]: Configuring 20UPGFC0010087
[16:14:16:858][  info  ][  scanConsole  ]: Sent configuration to all FEs in 142 ms!
[16:14:16:859][  info  ][  scanConsole  ]: Checking com 20UPGFC0010055
[16:14:16:859][  info  ][    SpecRx     ]: Active Rx channels: 0x10
[16:14:16:859][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[16:14:16:859][  info  ][    SpecRx     ]: Rx Status 0xf0
[16:14:16:859][  info  ][    SpecRx     ]: Number of lanes: 1
[16:14:16:859][  info  ][    SpecRx     ]: Channel 4 Lane 0 synchronized!
[16:14:16:870][  info  ][  scanConsole  ]: ... success!
[16:14:16:870][  info  ][  scanConsole  ]: Checking com 20UPGFC0010103
[16:14:16:870][  info  ][    SpecRx     ]: Active Rx channels: 0x20
[16:14:16:870][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[16:14:16:870][  info  ][    SpecRx     ]: Rx Status 0xf0
[16:14:16:870][  info  ][    SpecRx     ]: Number of lanes: 1
[16:14:16:870][  info  ][    SpecRx     ]: Channel 5 Lane 0 synchronized!
[16:14:16:880][  info  ][  scanConsole  ]: ... success!
[16:14:16:881][  info  ][  scanConsole  ]: Checking com 20UPGFC0010119
[16:14:16:881][  info  ][    SpecRx     ]: Active Rx channels: 0x40
[16:14:16:881][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[16:14:16:881][  info  ][    SpecRx     ]: Rx Status 0xf0
[16:14:16:881][  info  ][    SpecRx     ]: Number of lanes: 1
[16:14:16:881][  info  ][    SpecRx     ]: Channel 6 Lane 0 synchronized!
[16:14:16:891][  info  ][  scanConsole  ]: ... success!
[16:14:16:891][  info  ][  scanConsole  ]: Checking com 20UPGFC0010087
[16:14:16:891][  info  ][    SpecRx     ]: Active Rx channels: 0x80
[16:14:16:891][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[16:14:16:891][  info  ][    SpecRx     ]: Rx Status 0xf0
[16:14:16:891][  info  ][    SpecRx     ]: Number of lanes: 1
[16:14:16:891][  info  ][    SpecRx     ]: Channel 7 Lane 0 synchronized!
[16:14:16:902][  info  ][  scanConsole  ]: ... success!
[16:14:16:902][  info  ][  scanConsole  ]: Enabling Tx channels
[16:14:16:902][  info  ][  scanConsole  ]: Enabling Tx channel 1
[16:14:16:902][  info  ][  scanConsole  ]: Enabling Tx channel 1
[16:14:16:902][  info  ][  scanConsole  ]: Enabling Tx channel 1
[16:14:16:902][  info  ][  scanConsole  ]: Enabling Tx channel 1
[16:14:16:902][  info  ][  scanConsole  ]: Enabling Rx channels
[16:14:16:902][  info  ][  scanConsole  ]: Enabling Rx channel 4
[16:14:16:902][  info  ][  scanConsole  ]: Enabling Rx channel 5
[16:14:16:902][  info  ][  scanConsole  ]: Enabling Rx channel 6
[16:14:16:902][  info  ][  scanConsole  ]: Enabling Rx channel 7
[16:14:16:902][  info  ][  scanConsole  ]: ##############
[16:14:16:902][  info  ][  scanConsole  ]: # Setup Scan #
[16:14:16:902][  info  ][  scanConsole  ]: ##############
[16:14:16:902][  info  ][  scanConsole  ]: Found Scan config, constructing scan ...
[16:14:16:902][  info  ][  ScanFactory  ]: Loading Scan:
[16:14:16:902][  info  ][  ScanFactory  ]:   Name: AnalogScan
[16:14:16:902][  info  ][  ScanFactory  ]:   Number of Loops: 3
[16:14:16:902][  info  ][  ScanFactory  ]:   Loading Loop #0
[16:14:16:902][  info  ][  ScanFactory  ]:    Type: Rd53aReadRegLoop
[16:14:16:902][  info  ][  ScanFactory  ]:    Loading loop config ... 
[16:14:16:902][  info  ][  ScanFactory  ]: ~~~ {
[16:14:16:902][  info  ][  ScanFactory  ]: ~~~     "CurMux": [],
[16:14:16:902][  info  ][  ScanFactory  ]: ~~~     "EnblRingOsc": 0,
[16:14:16:902][  info  ][  ScanFactory  ]: ~~~     "Registers": [],
[16:14:16:902][  info  ][  ScanFactory  ]: ~~~     "RingOscDur": 9,
[16:14:16:902][  info  ][  ScanFactory  ]: ~~~     "RingOscRep": 10,
[16:14:16:902][  info  ][  ScanFactory  ]: ~~~     "VoltMux": [3, 5, 7, 15]
[16:14:16:902][  info  ][  ScanFactory  ]: ~~~ }
[16:14:16:902][  info  ][  ScanFactory  ]:   Loading Loop #1
[16:14:16:902][  info  ][  ScanFactory  ]:    Type: Rd53aTriggerLoop
[16:14:16:902][  info  ][  ScanFactory  ]:    Loading loop config ... 
[16:14:16:902][  info  ][  ScanFactory  ]: ~~~ {
[16:14:16:902][  info  ][  ScanFactory  ]: ~~~     "count": 1,
[16:14:16:902][  info  ][  ScanFactory  ]: ~~~     "delay": 48,
[16:14:16:902][  info  ][  ScanFactory  ]: ~~~     "extTrig": false,
[16:14:16:902][  info  ][  ScanFactory  ]: ~~~     "frequency": 30000.0,
[16:14:16:902][  info  ][  ScanFactory  ]: ~~~     "noInject": false,
[16:14:16:902][  info  ][  ScanFactory  ]: ~~~     "sendEcr": false,
[16:14:16:902][  info  ][  ScanFactory  ]: ~~~     "time": 0.0,
[16:14:16:902][  info  ][  ScanFactory  ]: ~~~     "trigMultiplier": 16
[16:14:16:902][  info  ][  ScanFactory  ]: ~~~ }
[16:14:16:902][  info  ][  ScanFactory  ]:   Loading Loop #2
[16:14:16:902][  info  ][  ScanFactory  ]:    Type: StdDataLoop
[16:14:16:902][warning ][  ScanFactory  ]: ~~~ Config empty.
[16:14:16:902][  info  ][ScanBuildHistogrammers]: Loading histogrammer ...
[16:14:16:902][  info  ][ScanBuildHistogrammers]: ... done!
[16:14:16:902][  info  ][ScanBuildAnalyses]: Loading analyses ...
[16:14:16:902][  info  ][  scanConsole  ]: Running pre scan!
[16:14:16:902][  info  ][  ScanFactory  ]: Entering pre scan phase ...
[16:14:16:902][  info  ][     Rd53a     ]: Write named register: GlobalPulseRt -> 16384
[16:14:16:902][  info  ][     Rd53a     ]: Write named register: InjEnDig -> 0
[16:14:16:902][  info  ][     Rd53a     ]: Write named register: InjVcalDiff -> 0
[16:14:16:902][  info  ][     Rd53a     ]: Write named register: InjVcalHigh -> 3500
[16:14:16:902][  info  ][     Rd53a     ]: Write named register: InjVcalMed -> 500
[16:14:16:902][  info  ][     Rd53a     ]: Write named register: LatencyConfig -> 48
[16:14:16:902][  info  ][  scanConsole  ]: Starting histogrammer and analysis threads:
[16:14:16:903][  info  ][  scanConsole  ]:  .. started threads of Fe 4
[16:14:16:903][  info  ][  scanConsole  ]:  .. started threads of Fe 5
[16:14:16:903][  info  ][  scanConsole  ]:  .. started threads of Fe 6
[16:14:16:903][  info  ][  scanConsole  ]:  .. started threads of Fe 7
[16:14:16:903][  info  ][Rd53aDataProcessor]:   -> Processor thread #0 started!
[16:14:16:903][  info  ][Rd53aDataProcessor]:   -> Processor thread #1 started!
[16:14:16:903][  info  ][Rd53aDataProcessor]:   -> Processor thread #2 started!
[16:14:16:903][  info  ][Rd53aDataProcessor]:   -> Processor thread #3 started!
[16:14:16:903][  info  ][  scanConsole  ]: ########
[16:14:16:903][  info  ][  scanConsole  ]: # Scan #
[16:14:16:903][  info  ][  scanConsole  ]: ########
[16:14:16:903][  info  ][  scanConsole  ]: Starting scan!
[16:14:16:903][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0010055 on Rx 4
[16:14:16:907][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0010055] MON MUX_V: 3 Bias 0 1188 -> 0.262164 V Bias 1 1594 -> 0.342915 V, Temperature 0.0807506 -> 2.76236 C
[16:14:16:911][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0010055] MON MUX_V: 5 Bias 0 1165 -> 0.25759 V Bias 1 1571 -> 0.338341 V, Temperature 0.0807507 -> -5.6684 C
[16:14:16:915][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0010055] MON MUX_V: 7 Bias 0 1151 -> 0.254805 V Bias 1 1557 -> 0.335556 V, Temperature 0.0807507 -> -4.79996 C
[16:14:16:919][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0010055] MON MUX_V: 15 Bias 0 1133 -> 0.251225 V Bias 1 1545 -> 0.333169 V, Temperature 0.081944 -> -6.63336 C
[16:14:16:954][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0010103 on Rx 5
[16:14:16:958][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0010103] MON MUX_V: 3 Bias 0 1201 -> 0.262282 V Bias 1 1609 -> 0.343604 V, Temperature 0.0813222 -> 5.58899 C
[16:14:16:962][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0010103] MON MUX_V: 5 Bias 0 1149 -> 0.251918 V Bias 1 1560 -> 0.333838 V, Temperature 0.0819201 -> 4.43997 C
[16:14:16:967][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0010103] MON MUX_V: 7 Bias 0 1130 -> 0.248131 V Bias 1 1553 -> 0.332443 V, Temperature 0.0843119 -> 4.13739 C
[16:14:16:971][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0010103] MON MUX_V: 15 Bias 0 1146 -> 0.25132 V Bias 1 1561 -> 0.334037 V, Temperature 0.0827174 -> 3.80869 C
[16:14:17:006][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0010119 on Rx 6
[16:14:17:010][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0010119] MON MUX_V: 3 Bias 0 1184 -> 0.258806 V Bias 1 1600 -> 0.340946 V, Temperature 0.0821396 -> 4.71933 C
[16:14:17:014][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0010119] MON MUX_V: 5 Bias 0 1168 -> 0.255647 V Bias 1 1582 -> 0.337391 V, Temperature 0.0817446 -> -6.74124 C
[16:14:17:018][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0010119] MON MUX_V: 7 Bias 0 1148 -> 0.251698 V Bias 1 1569 -> 0.334825 V, Temperature 0.0831268 -> -2.19458 C
[16:14:17:023][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0010119] MON MUX_V: 15 Bias 0 1140 -> 0.250118 V Bias 1 1557 -> 0.332455 V, Temperature 0.082337 -> -4.79993 C
[16:14:17:057][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0010087 on Rx 7
[16:14:17:062][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0010087] MON MUX_V: 3 Bias 0 1205 -> 0.262439 V Bias 1 1608 -> 0.342414 V, Temperature 0.0799754 -> 5.58884 C
[16:14:17:066][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0010087] MON MUX_V: 5 Bias 0 1172 -> 0.25589 V Bias 1 1587 -> 0.338247 V, Temperature 0.0823568 -> -0.0857239 C
[16:14:17:070][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0010087] MON MUX_V: 7 Bias 0 1159 -> 0.25331 V Bias 1 1580 -> 0.336858 V, Temperature 0.0835475 -> -0.871368 C
[16:14:17:075][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0010087] MON MUX_V: 15 Bias 0 1162 -> 0.253906 V Bias 1 1580 -> 0.336858 V, Temperature 0.0829521 -> 0.939148 C
[16:14:17:111][  info  ][  scanConsole  ]: Scan done!
[16:14:17:111][  info  ][  scanConsole  ]: Waiting for processors to finish ...
[16:14:17:111][  info  ][  scanConsole  ]: Processor done, waiting for histogrammer ...
[16:14:17:511][  info  ][HistogramAlgorithm]: Histogrammer done!
[16:14:17:511][  info  ][HistogramAlgorithm]: Histogrammer done!
[16:14:17:511][  info  ][HistogramAlgorithm]: Histogrammer done!
[16:14:17:511][  info  ][HistogramAlgorithm]: Histogrammer done!
[16:14:17:512][  info  ][  scanConsole  ]: Processor done, waiting for analysis ...
[16:14:17:912][  info  ][AnalysisAlgorithm]: Analysis done!
[16:14:17:912][  info  ][AnalysisAlgorithm]: Analysis done!
[16:14:17:912][  info  ][AnalysisAlgorithm]: Analysis done!
[16:14:17:912][  info  ][AnalysisAlgorithm]: Analysis done!
[16:14:17:912][  info  ][  scanConsole  ]: All done!
[16:14:17:912][  info  ][  scanConsole  ]: ##########
[16:14:17:912][  info  ][  scanConsole  ]: # Timing #
[16:14:17:912][  info  ][  scanConsole  ]: ##########
[16:14:17:912][  info  ][  scanConsole  ]: -> Configuration: 142 ms
[16:14:17:912][  info  ][  scanConsole  ]: -> Scan:          207 ms
[16:14:17:912][  info  ][  scanConsole  ]: -> Processing:    0 ms
[16:14:17:912][  info  ][  scanConsole  ]: -> Analysis:      800 ms
[16:14:17:912][  info  ][  scanConsole  ]: ###########
[16:14:17:912][  info  ][  scanConsole  ]: # Cleanup #
[16:14:17:912][  info  ][  scanConsole  ]: ###########
[16:14:17:921][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0010055 to configs/CERNQ6/20UPGFC0010055.json
[16:14:18:089][  info  ][  scanConsole  ]: -> Storing output results of FE 4
[16:14:18:089][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0010055, this usually means that the chip did not send any data at all.
[16:14:18:090][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0010103 to configs/CERNQ6/20UPGFC0010103.json
[16:14:18:255][  info  ][  scanConsole  ]: -> Storing output results of FE 5
[16:14:18:255][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0010103, this usually means that the chip did not send any data at all.
[16:14:18:256][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0010119 to configs/CERNQ6/20UPGFC0010119.json
[16:14:18:423][  info  ][  scanConsole  ]: -> Storing output results of FE 6
[16:14:18:423][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0010119, this usually means that the chip did not send any data at all.
[16:14:18:424][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0010087 to configs/CERNQ6/20UPGFC0010087.json
[16:14:18:593][  info  ][  scanConsole  ]: -> Storing output results of FE 7
[16:14:18:593][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0010087, this usually means that the chip did not send any data at all.
[16:14:18:595][  info  ][  scanConsole  ]: Finishing run: 1518
[root@jollyroger Yarr]# 
