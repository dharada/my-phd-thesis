[16:06:59:148][  info  ][               ]: #####################################
[16:06:59:148][  info  ][               ]: # Welcome to the YARR Scan Console! #
[16:06:59:148][  info  ][               ]: #####################################
[16:06:59:148][  info  ][               ]: -> Parsing command line parameters ...
[16:06:59:148][  info  ][               ]: Configuring logger ...
[16:06:59:151][  info  ][  scanConsole  ]: Scan Type/Config configs/scans/rd53a/reg_readtemp.json
[16:06:59:151][  info  ][  scanConsole  ]: Connectivity:
[16:06:59:151][  info  ][  scanConsole  ]:     configs/CERNQ6/example_rd53a_setup.json
[16:06:59:151][  info  ][  scanConsole  ]: Target ToT: -1
[16:06:59:151][  info  ][  scanConsole  ]: Target Charge: -1
[16:06:59:151][  info  ][  scanConsole  ]: Output Plots: false
[16:06:59:151][  info  ][  scanConsole  ]: Output Directory: ./data/001514_reg_readtemp/
[16:06:59:156][  info  ][  scanConsole  ]: Timestamp: 2022-08-20_16:06:59
[16:06:59:156][  info  ][  scanConsole  ]: Run Number: 1514
[16:06:59:156][  info  ][  scanConsole  ]: #################
[16:06:59:156][  info  ][  scanConsole  ]: # Init Hardware #
[16:06:59:156][  info  ][  scanConsole  ]: #################
[16:06:59:156][  info  ][  scanConsole  ]: -> Opening controller config: configs/controller/specCfg-rd53a-16x1.json
[16:06:59:156][  info  ][  ScanHelper   ]: Loading controller ...
[16:06:59:156][  info  ][  ScanHelper   ]: Found controller of type: spec
[16:06:59:156][  info  ][  ScanHelper   ]: ... loading controler config:
[16:06:59:156][  info  ][  ScanHelper   ]: ~~~ {
[16:06:59:156][  info  ][  ScanHelper   ]: ~~~     "cmdPeriod": 6.250000073038109e-9,
[16:06:59:156][  info  ][  ScanHelper   ]: ~~~     "idle": {
[16:06:59:156][  info  ][  ScanHelper   ]: ~~~         "word": 1768515945
[16:06:59:156][  info  ][  ScanHelper   ]: ~~~     },
[16:06:59:156][  info  ][  ScanHelper   ]: ~~~     "pulse": {
[16:06:59:156][  info  ][  ScanHelper   ]: ~~~         "interval": 500,
[16:06:59:156][  info  ][  ScanHelper   ]: ~~~         "word": 1549575846
[16:06:59:156][  info  ][  ScanHelper   ]: ~~~     },
[16:06:59:156][  info  ][  ScanHelper   ]: ~~~     "rxActiveLanes": 1,
[16:06:59:156][  info  ][  ScanHelper   ]: ~~~     "rxPolarity": 0,
[16:06:59:156][  info  ][  ScanHelper   ]: ~~~     "specNum": 0,
[16:06:59:156][  info  ][  ScanHelper   ]: ~~~     "spiConfig": 541200,
[16:06:59:156][  info  ][  ScanHelper   ]: ~~~     "sync": {
[16:06:59:156][  info  ][  ScanHelper   ]: ~~~         "interval": 32,
[16:06:59:156][  info  ][  ScanHelper   ]: ~~~         "word": 2172551550
[16:06:59:156][  info  ][  ScanHelper   ]: ~~~     },
[16:06:59:156][  info  ][  ScanHelper   ]: ~~~     "txPolarity": 0
[16:06:59:156][  info  ][  ScanHelper   ]: ~~~ }
[16:06:59:156][  info  ][    SpecCom    ]: Opening SPEC with id #0
[16:06:59:156][  info  ][    SpecCom    ]: Mapping BARs ...
[16:06:59:156][  info  ][    SpecCom    ]: ... Mapped BAR0 at 0x7f9193d74000 with size 1048576
[16:06:59:156][warning ][    SpecCom    ]: ... BAR4 not mapped (Mmap failed)
[16:06:59:157][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[16:06:59:157][  info  ][    SpecCom    ]: Firmware Version: 0x4d9ff6d
[16:06:59:157][  info  ][    SpecCom    ]: Firmware Identifier: 0x4030232
[16:06:59:157][  info  ][    SpecCom    ]: FPGA card: PLDA XpressK7 325
[16:06:59:157][  info  ][    SpecCom    ]: FE Chip Type: RD53A/B
[16:06:59:157][  info  ][    SpecCom    ]: FMC Card Type: Ohio Card (Display Port)
[16:06:59:157][  info  ][    SpecCom    ]: RX Speed: 640Mbps
[16:06:59:157][  info  ][    SpecCom    ]: Channel Configuration: 16x1
[16:06:59:157][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[16:06:59:157][  info  ][    SpecCom    ]: Flushing buffers ...
[16:06:59:157][  info  ][    SpecCom    ]: Init success!
[16:06:59:157][  info  ][  scanConsole  ]: #######################
[16:06:59:157][  info  ][  scanConsole  ]: ##  Loading Configs  ##
[16:06:59:157][  info  ][  scanConsole  ]: #######################
[16:06:59:157][  info  ][  scanConsole  ]: Opening global config: configs/CERNQ6/example_rd53a_setup.json
[16:06:59:157][  info  ][  ScanHelper   ]: Chip type: RD53A
[16:06:59:157][  info  ][  ScanHelper   ]: Chip count 4
[16:06:59:157][  info  ][  ScanHelper   ]: Loading chip #0
[16:06:59:158][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(4)
[16:06:59:158][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ6/20UPGFC0010055.json
[16:06:59:358][  info  ][  ScanHelper   ]: Loading chip #1
[16:06:59:358][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(5)
[16:06:59:358][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ6/20UPGFC0010103.json
[16:06:59:541][  info  ][  ScanHelper   ]: Loading chip #2
[16:06:59:541][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(6)
[16:06:59:542][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ6/20UPGFC0010119.json
[16:06:59:716][  info  ][  ScanHelper   ]: Loading chip #3
[16:06:59:716][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(7)
[16:06:59:717][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ6/20UPGFC0010087.json
[16:06:59:893][  info  ][  scanConsole  ]: #################
[16:06:59:893][  info  ][  scanConsole  ]: # Configure FEs #
[16:06:59:893][  info  ][  scanConsole  ]: #################
[16:06:59:893][  info  ][  scanConsole  ]: Configuring 20UPGFC0010055
[16:06:59:923][  info  ][  scanConsole  ]: Configuring 20UPGFC0010103
[16:06:59:954][  info  ][  scanConsole  ]: Configuring 20UPGFC0010119
[16:06:59:984][  info  ][  scanConsole  ]: Configuring 20UPGFC0010087
[16:07:00:017][  info  ][  scanConsole  ]: Sent configuration to all FEs in 123 ms!
[16:07:00:018][  info  ][  scanConsole  ]: Checking com 20UPGFC0010055
[16:07:00:018][  info  ][    SpecRx     ]: Active Rx channels: 0x10
[16:07:00:018][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[16:07:00:018][  info  ][    SpecRx     ]: Rx Status 0xf0
[16:07:00:018][  info  ][    SpecRx     ]: Number of lanes: 1
[16:07:00:018][  info  ][    SpecRx     ]: Channel 4 Lane 0 synchronized!
[16:07:00:028][  info  ][  scanConsole  ]: ... success!
[16:07:00:028][  info  ][  scanConsole  ]: Checking com 20UPGFC0010103
[16:07:00:028][  info  ][    SpecRx     ]: Active Rx channels: 0x20
[16:07:00:028][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[16:07:00:028][  info  ][    SpecRx     ]: Rx Status 0xf0
[16:07:00:028][  info  ][    SpecRx     ]: Number of lanes: 1
[16:07:00:028][  info  ][    SpecRx     ]: Channel 5 Lane 0 synchronized!
[16:07:00:039][  info  ][  scanConsole  ]: ... success!
[16:07:00:039][  info  ][  scanConsole  ]: Checking com 20UPGFC0010119
[16:07:00:039][  info  ][    SpecRx     ]: Active Rx channels: 0x40
[16:07:00:039][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[16:07:00:039][  info  ][    SpecRx     ]: Rx Status 0xf0
[16:07:00:039][  info  ][    SpecRx     ]: Number of lanes: 1
[16:07:00:039][  info  ][    SpecRx     ]: Channel 6 Lane 0 synchronized!
[16:07:00:049][  info  ][  scanConsole  ]: ... success!
[16:07:00:050][  info  ][  scanConsole  ]: Checking com 20UPGFC0010087
[16:07:00:050][  info  ][    SpecRx     ]: Active Rx channels: 0x80
[16:07:00:050][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[16:07:00:050][  info  ][    SpecRx     ]: Rx Status 0xf0
[16:07:00:050][  info  ][    SpecRx     ]: Number of lanes: 1
[16:07:00:050][  info  ][    SpecRx     ]: Channel 7 Lane 0 synchronized!
[16:07:00:060][  info  ][  scanConsole  ]: ... success!
[16:07:00:060][  info  ][  scanConsole  ]: Enabling Tx channels
[16:07:00:060][  info  ][  scanConsole  ]: Enabling Tx channel 1
[16:07:00:060][  info  ][  scanConsole  ]: Enabling Tx channel 1
[16:07:00:060][  info  ][  scanConsole  ]: Enabling Tx channel 1
[16:07:00:060][  info  ][  scanConsole  ]: Enabling Tx channel 1
[16:07:00:060][  info  ][  scanConsole  ]: Enabling Rx channels
[16:07:00:060][  info  ][  scanConsole  ]: Enabling Rx channel 4
[16:07:00:060][  info  ][  scanConsole  ]: Enabling Rx channel 5
[16:07:00:060][  info  ][  scanConsole  ]: Enabling Rx channel 6
[16:07:00:060][  info  ][  scanConsole  ]: Enabling Rx channel 7
[16:07:00:060][  info  ][  scanConsole  ]: ##############
[16:07:00:060][  info  ][  scanConsole  ]: # Setup Scan #
[16:07:00:060][  info  ][  scanConsole  ]: ##############
[16:07:00:061][  info  ][  scanConsole  ]: Found Scan config, constructing scan ...
[16:07:00:061][  info  ][  ScanFactory  ]: Loading Scan:
[16:07:00:061][  info  ][  ScanFactory  ]:   Name: AnalogScan
[16:07:00:061][  info  ][  ScanFactory  ]:   Number of Loops: 3
[16:07:00:061][  info  ][  ScanFactory  ]:   Loading Loop #0
[16:07:00:061][  info  ][  ScanFactory  ]:    Type: Rd53aReadRegLoop
[16:07:00:061][  info  ][  ScanFactory  ]:    Loading loop config ... 
[16:07:00:061][  info  ][  ScanFactory  ]: ~~~ {
[16:07:00:061][  info  ][  ScanFactory  ]: ~~~     "CurMux": [],
[16:07:00:062][  info  ][  ScanFactory  ]: ~~~     "EnblRingOsc": 0,
[16:07:00:062][  info  ][  ScanFactory  ]: ~~~     "Registers": [],
[16:07:00:062][  info  ][  ScanFactory  ]: ~~~     "RingOscDur": 9,
[16:07:00:062][  info  ][  ScanFactory  ]: ~~~     "RingOscRep": 10,
[16:07:00:062][  info  ][  ScanFactory  ]: ~~~     "VoltMux": [3, 5, 7, 15]
[16:07:00:062][  info  ][  ScanFactory  ]: ~~~ }
[16:07:00:062][  info  ][  ScanFactory  ]:   Loading Loop #1
[16:07:00:062][  info  ][  ScanFactory  ]:    Type: Rd53aTriggerLoop
[16:07:00:062][  info  ][  ScanFactory  ]:    Loading loop config ... 
[16:07:00:062][  info  ][  ScanFactory  ]: ~~~ {
[16:07:00:062][  info  ][  ScanFactory  ]: ~~~     "count": 1,
[16:07:00:062][  info  ][  ScanFactory  ]: ~~~     "delay": 48,
[16:07:00:062][  info  ][  ScanFactory  ]: ~~~     "extTrig": false,
[16:07:00:062][  info  ][  ScanFactory  ]: ~~~     "frequency": 30000.0,
[16:07:00:062][  info  ][  ScanFactory  ]: ~~~     "noInject": false,
[16:07:00:062][  info  ][  ScanFactory  ]: ~~~     "sendEcr": false,
[16:07:00:062][  info  ][  ScanFactory  ]: ~~~     "time": 0.0,
[16:07:00:062][  info  ][  ScanFactory  ]: ~~~     "trigMultiplier": 16
[16:07:00:062][  info  ][  ScanFactory  ]: ~~~ }
[16:07:00:062][  info  ][  ScanFactory  ]:   Loading Loop #2
[16:07:00:062][  info  ][  ScanFactory  ]:    Type: StdDataLoop
[16:07:00:062][warning ][  ScanFactory  ]: ~~~ Config empty.
[16:07:00:062][  info  ][ScanBuildHistogrammers]: Loading histogrammer ...
[16:07:00:062][  info  ][ScanBuildHistogrammers]: ... done!
[16:07:00:062][  info  ][ScanBuildAnalyses]: Loading analyses ...
[16:07:00:063][  info  ][  scanConsole  ]: Running pre scan!
[16:07:00:063][  info  ][  ScanFactory  ]: Entering pre scan phase ...
[16:07:00:063][  info  ][     Rd53a     ]: Write named register: GlobalPulseRt -> 16384
[16:07:00:063][  info  ][     Rd53a     ]: Write named register: InjEnDig -> 0
[16:07:00:063][  info  ][     Rd53a     ]: Write named register: InjVcalDiff -> 0
[16:07:00:063][  info  ][     Rd53a     ]: Write named register: InjVcalHigh -> 3500
[16:07:00:063][  info  ][     Rd53a     ]: Write named register: InjVcalMed -> 500
[16:07:00:063][  info  ][     Rd53a     ]: Write named register: LatencyConfig -> 48
[16:07:00:063][  info  ][  scanConsole  ]: Starting histogrammer and analysis threads:
[16:07:00:063][  info  ][  scanConsole  ]:  .. started threads of Fe 4
[16:07:00:063][  info  ][  scanConsole  ]:  .. started threads of Fe 5
[16:07:00:064][  info  ][  scanConsole  ]:  .. started threads of Fe 6
[16:07:00:064][  info  ][  scanConsole  ]:  .. started threads of Fe 7
[16:07:00:064][  info  ][Rd53aDataProcessor]:   -> Processor thread #0 started!
[16:07:00:064][  info  ][Rd53aDataProcessor]:   -> Processor thread #1 started!
[16:07:00:064][  info  ][Rd53aDataProcessor]:   -> Processor thread #2 started!
[16:07:00:064][  info  ][Rd53aDataProcessor]:   -> Processor thread #3 started!
[16:07:00:064][  info  ][  scanConsole  ]: ########
[16:07:00:064][  info  ][  scanConsole  ]: # Scan #
[16:07:00:064][  info  ][  scanConsole  ]: ########
[16:07:00:064][  info  ][  scanConsole  ]: Starting scan!
[16:07:00:064][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0010055 on Rx 4
[16:07:00:069][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0010055] MON MUX_V: 3 Bias 0 1187 -> 0.261966 V Bias 1 1594 -> 0.342915 V, Temperature 0.0809495 -> 3.44986 C
[16:07:00:073][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0010055] MON MUX_V: 5 Bias 0 1164 -> 0.257391 V Bias 1 1571 -> 0.338341 V, Temperature 0.0809496 -> -4.80008 C
[16:07:00:077][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0010055] MON MUX_V: 7 Bias 0 1151 -> 0.254805 V Bias 1 1557 -> 0.335556 V, Temperature 0.0807507 -> -4.79996 C
[16:07:00:081][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0010055] MON MUX_V: 15 Bias 0 1132 -> 0.251026 V Bias 1 1544 -> 0.33297 V, Temperature 0.081944 -> -6.63336 C
[16:07:00:116][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0010103 on Rx 5
[16:07:00:120][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0010103] MON MUX_V: 3 Bias 0 1200 -> 0.262083 V Bias 1 1609 -> 0.343604 V, Temperature 0.0815215 -> 6.2001 C
[16:07:00:124][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0010103] MON MUX_V: 5 Bias 0 1148 -> 0.251718 V Bias 1 1558 -> 0.333439 V, Temperature 0.0817208 -> 3.77997 C
[16:07:00:128][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0010103] MON MUX_V: 7 Bias 0 1128 -> 0.247732 V Bias 1 1552 -> 0.332243 V, Temperature 0.0845113 -> 4.82501 C
[16:07:00:132][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0010103] MON MUX_V: 15 Bias 0 1144 -> 0.250921 V Bias 1 1561 -> 0.334037 V, Temperature 0.0831161 -> 5.24347 C
[16:07:00:167][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0010119 on Rx 6
[16:07:00:171][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0010119] MON MUX_V: 3 Bias 0 1183 -> 0.258609 V Bias 1 1599 -> 0.340748 V, Temperature 0.0821396 -> 4.71933 C
[16:07:00:176][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0010119] MON MUX_V: 5 Bias 0 1166 -> 0.255252 V Bias 1 1580 -> 0.336997 V, Temperature 0.0817446 -> -6.74124 C
[16:07:00:180][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0010119] MON MUX_V: 7 Bias 0 1147 -> 0.2515 V Bias 1 1568 -> 0.334627 V, Temperature 0.0831268 -> -2.19482 C
[16:07:00:184][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0010119] MON MUX_V: 15 Bias 0 1139 -> 0.249921 V Bias 1 1555 -> 0.33206 V, Temperature 0.0821396 -> -5.71655 C
[16:07:00:219][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0010087 on Rx 7
[16:07:00:223][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0010087] MON MUX_V: 3 Bias 0 1205 -> 0.262439 V Bias 1 1607 -> 0.342216 V, Temperature 0.0797769 -> 4.9778 C
[16:07:00:227][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0010087] MON MUX_V: 5 Bias 0 1171 -> 0.255692 V Bias 1 1585 -> 0.33785 V, Temperature 0.0821583 -> -0.87146 C
[16:07:00:231][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0010087] MON MUX_V: 7 Bias 0 1157 -> 0.252914 V Bias 1 1579 -> 0.336659 V, Temperature 0.083746 -> -0.0856323 C
[16:07:00:236][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0010087] MON MUX_V: 15 Bias 0 1160 -> 0.253509 V Bias 1 1578 -> 0.336461 V, Temperature 0.0829521 -> 0.939148 C
[16:07:00:272][  info  ][  scanConsole  ]: Scan done!
[16:07:00:272][  info  ][  scanConsole  ]: Waiting for processors to finish ...
[16:07:00:273][  info  ][  scanConsole  ]: Processor done, waiting for histogrammer ...
[16:07:00:672][  info  ][HistogramAlgorithm]: Histogrammer done!
[16:07:00:672][  info  ][HistogramAlgorithm]: Histogrammer done!
[16:07:00:672][  info  ][HistogramAlgorithm]: Histogrammer done!
[16:07:00:672][  info  ][HistogramAlgorithm]: Histogrammer done!
[16:07:00:673][  info  ][  scanConsole  ]: Processor done, waiting for analysis ...
[16:07:01:073][  info  ][AnalysisAlgorithm]: Analysis done!
[16:07:01:073][  info  ][AnalysisAlgorithm]: Analysis done!
[16:07:01:073][  info  ][AnalysisAlgorithm]: Analysis done!
[16:07:01:073][  info  ][AnalysisAlgorithm]: Analysis done!
[16:07:01:073][  info  ][  scanConsole  ]: All done!
[16:07:01:073][  info  ][  scanConsole  ]: ##########
[16:07:01:073][  info  ][  scanConsole  ]: # Timing #
[16:07:01:073][  info  ][  scanConsole  ]: ##########
[16:07:01:073][  info  ][  scanConsole  ]: -> Configuration: 123 ms
[16:07:01:073][  info  ][  scanConsole  ]: -> Scan:          207 ms
[16:07:01:073][  info  ][  scanConsole  ]: -> Processing:    0 ms
[16:07:01:073][  info  ][  scanConsole  ]: -> Analysis:      800 ms
[16:07:01:073][  info  ][  scanConsole  ]: ###########
[16:07:01:073][  info  ][  scanConsole  ]: # Cleanup #
[16:07:01:073][  info  ][  scanConsole  ]: ###########
[16:07:01:077][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0010055 to configs/CERNQ6/20UPGFC0010055.json
[16:07:01:267][  info  ][  scanConsole  ]: -> Storing output results of FE 4
[16:07:01:267][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0010055, this usually means that the chip did not send any data at all.
[16:07:01:269][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0010103 to configs/CERNQ6/20UPGFC0010103.json
[16:07:01:435][  info  ][  scanConsole  ]: -> Storing output results of FE 5
[16:07:01:435][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0010103, this usually means that the chip did not send any data at all.
[16:07:01:436][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0010119 to configs/CERNQ6/20UPGFC0010119.json
[16:07:01:601][  info  ][  scanConsole  ]: -> Storing output results of FE 6
[16:07:01:601][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0010119, this usually means that the chip did not send any data at all.
[16:07:01:602][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0010087 to configs/CERNQ6/20UPGFC0010087.json
[16:07:01:769][  info  ][  scanConsole  ]: -> Storing output results of FE 7
[16:07:01:769][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0010087, this usually means that the chip did not send any data at all.
[16:07:01:770][  info  ][  scanConsole  ]: Finishing run: 1514
[root@jollyroger Yarr]# 
