[16:11:07:332][  info  ][               ]: #####################################
[16:11:07:332][  info  ][               ]: # Welcome to the YARR Scan Console! #
[16:11:07:332][  info  ][               ]: #####################################
[16:11:07:332][  info  ][               ]: -> Parsing command line parameters ...
[16:11:07:332][  info  ][               ]: Configuring logger ...
[16:11:07:341][  info  ][  scanConsole  ]: Scan Type/Config configs/scans/rd53a/reg_readtemp.json
[16:11:07:342][  info  ][  scanConsole  ]: Connectivity:
[16:11:07:342][  info  ][  scanConsole  ]:     configs/CERNQ6/example_rd53a_setup.json
[16:11:07:342][  info  ][  scanConsole  ]: Target ToT: -1
[16:11:07:342][  info  ][  scanConsole  ]: Target Charge: -1
[16:11:07:342][  info  ][  scanConsole  ]: Output Plots: false
[16:11:07:342][  info  ][  scanConsole  ]: Output Directory: ./data/001516_reg_readtemp/
[16:11:07:360][  info  ][  scanConsole  ]: Timestamp: 2022-08-20_16:11:07
[16:11:07:360][  info  ][  scanConsole  ]: Run Number: 1516
[16:11:07:360][  info  ][  scanConsole  ]: #################
[16:11:07:361][  info  ][  scanConsole  ]: # Init Hardware #
[16:11:07:361][  info  ][  scanConsole  ]: #################
[16:11:07:361][  info  ][  scanConsole  ]: -> Opening controller config: configs/controller/specCfg-rd53a-16x1.json
[16:11:07:361][  info  ][  ScanHelper   ]: Loading controller ...
[16:11:07:361][  info  ][  ScanHelper   ]: Found controller of type: spec
[16:11:07:361][  info  ][  ScanHelper   ]: ... loading controler config:
[16:11:07:361][  info  ][  ScanHelper   ]: ~~~ {
[16:11:07:361][  info  ][  ScanHelper   ]: ~~~     "cmdPeriod": 6.250000073038109e-9,
[16:11:07:361][  info  ][  ScanHelper   ]: ~~~     "idle": {
[16:11:07:361][  info  ][  ScanHelper   ]: ~~~         "word": 1768515945
[16:11:07:361][  info  ][  ScanHelper   ]: ~~~     },
[16:11:07:361][  info  ][  ScanHelper   ]: ~~~     "pulse": {
[16:11:07:361][  info  ][  ScanHelper   ]: ~~~         "interval": 500,
[16:11:07:361][  info  ][  ScanHelper   ]: ~~~         "word": 1549575846
[16:11:07:361][  info  ][  ScanHelper   ]: ~~~     },
[16:11:07:361][  info  ][  ScanHelper   ]: ~~~     "rxActiveLanes": 1,
[16:11:07:361][  info  ][  ScanHelper   ]: ~~~     "rxPolarity": 0,
[16:11:07:361][  info  ][  ScanHelper   ]: ~~~     "specNum": 0,
[16:11:07:361][  info  ][  ScanHelper   ]: ~~~     "spiConfig": 541200,
[16:11:07:361][  info  ][  ScanHelper   ]: ~~~     "sync": {
[16:11:07:361][  info  ][  ScanHelper   ]: ~~~         "interval": 32,
[16:11:07:362][  info  ][  ScanHelper   ]: ~~~         "word": 2172551550
[16:11:07:362][  info  ][  ScanHelper   ]: ~~~     },
[16:11:07:362][  info  ][  ScanHelper   ]: ~~~     "txPolarity": 0
[16:11:07:362][  info  ][  ScanHelper   ]: ~~~ }
[16:11:07:362][  info  ][    SpecCom    ]: Opening SPEC with id #0
[16:11:07:363][  info  ][    SpecCom    ]: Mapping BARs ...
[16:11:07:363][  info  ][    SpecCom    ]: ... Mapped BAR0 at 0x7f444fc04000 with size 1048576
[16:11:07:363][warning ][    SpecCom    ]: ... BAR4 not mapped (Mmap failed)
[16:11:07:364][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[16:11:07:364][  info  ][    SpecCom    ]: Firmware Version: 0x4d9ff6d
[16:11:07:364][  info  ][    SpecCom    ]: Firmware Identifier: 0x4030232
[16:11:07:364][  info  ][    SpecCom    ]: FPGA card: PLDA XpressK7 325
[16:11:07:364][  info  ][    SpecCom    ]: FE Chip Type: RD53A/B
[16:11:07:364][  info  ][    SpecCom    ]: FMC Card Type: Ohio Card (Display Port)
[16:11:07:364][  info  ][    SpecCom    ]: RX Speed: 640Mbps
[16:11:07:364][  info  ][    SpecCom    ]: Channel Configuration: 16x1
[16:11:07:364][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[16:11:07:364][  info  ][    SpecCom    ]: Flushing buffers ...
[16:11:07:364][  info  ][    SpecCom    ]: Init success!
[16:11:07:364][  info  ][  scanConsole  ]: #######################
[16:11:07:364][  info  ][  scanConsole  ]: ##  Loading Configs  ##
[16:11:07:364][  info  ][  scanConsole  ]: #######################
[16:11:07:364][  info  ][  scanConsole  ]: Opening global config: configs/CERNQ6/example_rd53a_setup.json
[16:11:07:364][  info  ][  ScanHelper   ]: Chip type: RD53A
[16:11:07:364][  info  ][  ScanHelper   ]: Chip count 4
[16:11:07:364][  info  ][  ScanHelper   ]: Loading chip #0
[16:11:07:368][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(4)
[16:11:07:368][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ6/20UPGFC0010055.json
[16:11:07:578][  info  ][  ScanHelper   ]: Loading chip #1
[16:11:07:578][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(5)
[16:11:07:578][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ6/20UPGFC0010103.json
[16:11:07:752][  info  ][  ScanHelper   ]: Loading chip #2
[16:11:07:752][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(6)
[16:11:07:752][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ6/20UPGFC0010119.json
[16:11:07:928][  info  ][  ScanHelper   ]: Loading chip #3
[16:11:07:928][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(7)
[16:11:07:928][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ6/20UPGFC0010087.json
[16:11:08:106][  info  ][  scanConsole  ]: #################
[16:11:08:106][  info  ][  scanConsole  ]: # Configure FEs #
[16:11:08:106][  info  ][  scanConsole  ]: #################
[16:11:08:106][  info  ][  scanConsole  ]: Configuring 20UPGFC0010055
[16:11:08:137][  info  ][  scanConsole  ]: Configuring 20UPGFC0010103
[16:11:08:167][  info  ][  scanConsole  ]: Configuring 20UPGFC0010119
[16:11:08:197][  info  ][  scanConsole  ]: Configuring 20UPGFC0010087
[16:11:08:228][  info  ][  scanConsole  ]: Sent configuration to all FEs in 121 ms!
[16:11:08:229][  info  ][  scanConsole  ]: Checking com 20UPGFC0010055
[16:11:08:229][  info  ][    SpecRx     ]: Active Rx channels: 0x10
[16:11:08:229][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[16:11:08:229][  info  ][    SpecRx     ]: Rx Status 0xf0
[16:11:08:229][  info  ][    SpecRx     ]: Number of lanes: 1
[16:11:08:229][  info  ][    SpecRx     ]: Channel 4 Lane 0 synchronized!
[16:11:08:239][  info  ][  scanConsole  ]: ... success!
[16:11:08:239][  info  ][  scanConsole  ]: Checking com 20UPGFC0010103
[16:11:08:239][  info  ][    SpecRx     ]: Active Rx channels: 0x20
[16:11:08:239][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[16:11:08:239][  info  ][    SpecRx     ]: Rx Status 0xf0
[16:11:08:239][  info  ][    SpecRx     ]: Number of lanes: 1
[16:11:08:239][  info  ][    SpecRx     ]: Channel 5 Lane 0 synchronized!
[16:11:08:250][  info  ][  scanConsole  ]: ... success!
[16:11:08:250][  info  ][  scanConsole  ]: Checking com 20UPGFC0010119
[16:11:08:250][  info  ][    SpecRx     ]: Active Rx channels: 0x40
[16:11:08:250][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[16:11:08:250][  info  ][    SpecRx     ]: Rx Status 0xf0
[16:11:08:250][  info  ][    SpecRx     ]: Number of lanes: 1
[16:11:08:250][  info  ][    SpecRx     ]: Channel 6 Lane 0 synchronized!
[16:11:08:260][  info  ][  scanConsole  ]: ... success!
[16:11:08:260][  info  ][  scanConsole  ]: Checking com 20UPGFC0010087
[16:11:08:260][  info  ][    SpecRx     ]: Active Rx channels: 0x80
[16:11:08:261][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[16:11:08:261][  info  ][    SpecRx     ]: Rx Status 0xf0
[16:11:08:261][  info  ][    SpecRx     ]: Number of lanes: 1
[16:11:08:261][  info  ][    SpecRx     ]: Channel 7 Lane 0 synchronized!
[16:11:08:271][  info  ][  scanConsole  ]: ... success!
[16:11:08:271][  info  ][  scanConsole  ]: Enabling Tx channels
[16:11:08:271][  info  ][  scanConsole  ]: Enabling Tx channel 1
[16:11:08:271][  info  ][  scanConsole  ]: Enabling Tx channel 1
[16:11:08:271][  info  ][  scanConsole  ]: Enabling Tx channel 1
[16:11:08:271][  info  ][  scanConsole  ]: Enabling Tx channel 1
[16:11:08:271][  info  ][  scanConsole  ]: Enabling Rx channels
[16:11:08:271][  info  ][  scanConsole  ]: Enabling Rx channel 4
[16:11:08:271][  info  ][  scanConsole  ]: Enabling Rx channel 5
[16:11:08:271][  info  ][  scanConsole  ]: Enabling Rx channel 6
[16:11:08:271][  info  ][  scanConsole  ]: Enabling Rx channel 7
[16:11:08:271][  info  ][  scanConsole  ]: ##############
[16:11:08:271][  info  ][  scanConsole  ]: # Setup Scan #
[16:11:08:271][  info  ][  scanConsole  ]: ##############
[16:11:08:272][  info  ][  scanConsole  ]: Found Scan config, constructing scan ...
[16:11:08:272][  info  ][  ScanFactory  ]: Loading Scan:
[16:11:08:272][  info  ][  ScanFactory  ]:   Name: AnalogScan
[16:11:08:272][  info  ][  ScanFactory  ]:   Number of Loops: 3
[16:11:08:272][  info  ][  ScanFactory  ]:   Loading Loop #0
[16:11:08:272][  info  ][  ScanFactory  ]:    Type: Rd53aReadRegLoop
[16:11:08:272][  info  ][  ScanFactory  ]:    Loading loop config ... 
[16:11:08:272][  info  ][  ScanFactory  ]: ~~~ {
[16:11:08:273][  info  ][  ScanFactory  ]: ~~~     "CurMux": [],
[16:11:08:273][  info  ][  ScanFactory  ]: ~~~     "EnblRingOsc": 0,
[16:11:08:273][  info  ][  ScanFactory  ]: ~~~     "Registers": [],
[16:11:08:273][  info  ][  ScanFactory  ]: ~~~     "RingOscDur": 9,
[16:11:08:273][  info  ][  ScanFactory  ]: ~~~     "RingOscRep": 10,
[16:11:08:273][  info  ][  ScanFactory  ]: ~~~     "VoltMux": [3, 5, 7, 15]
[16:11:08:273][  info  ][  ScanFactory  ]: ~~~ }
[16:11:08:273][  info  ][  ScanFactory  ]:   Loading Loop #1
[16:11:08:273][  info  ][  ScanFactory  ]:    Type: Rd53aTriggerLoop
[16:11:08:273][  info  ][  ScanFactory  ]:    Loading loop config ... 
[16:11:08:273][  info  ][  ScanFactory  ]: ~~~ {
[16:11:08:273][  info  ][  ScanFactory  ]: ~~~     "count": 1,
[16:11:08:273][  info  ][  ScanFactory  ]: ~~~     "delay": 48,
[16:11:08:273][  info  ][  ScanFactory  ]: ~~~     "extTrig": false,
[16:11:08:273][  info  ][  ScanFactory  ]: ~~~     "frequency": 30000.0,
[16:11:08:273][  info  ][  ScanFactory  ]: ~~~     "noInject": false,
[16:11:08:273][  info  ][  ScanFactory  ]: ~~~     "sendEcr": false,
[16:11:08:273][  info  ][  ScanFactory  ]: ~~~     "time": 0.0,
[16:11:08:273][  info  ][  ScanFactory  ]: ~~~     "trigMultiplier": 16
[16:11:08:273][  info  ][  ScanFactory  ]: ~~~ }
[16:11:08:273][  info  ][  ScanFactory  ]:   Loading Loop #2
[16:11:08:273][  info  ][  ScanFactory  ]:    Type: StdDataLoop
[16:11:08:273][warning ][  ScanFactory  ]: ~~~ Config empty.
[16:11:08:273][  info  ][ScanBuildHistogrammers]: Loading histogrammer ...
[16:11:08:273][  info  ][ScanBuildHistogrammers]: ... done!
[16:11:08:274][  info  ][ScanBuildAnalyses]: Loading analyses ...
[16:11:08:274][  info  ][  scanConsole  ]: Running pre scan!
[16:11:08:274][  info  ][  ScanFactory  ]: Entering pre scan phase ...
[16:11:08:274][  info  ][     Rd53a     ]: Write named register: GlobalPulseRt -> 16384
[16:11:08:274][  info  ][     Rd53a     ]: Write named register: InjEnDig -> 0
[16:11:08:274][  info  ][     Rd53a     ]: Write named register: InjVcalDiff -> 0
[16:11:08:274][  info  ][     Rd53a     ]: Write named register: InjVcalHigh -> 3500
[16:11:08:274][  info  ][     Rd53a     ]: Write named register: InjVcalMed -> 500
[16:11:08:274][  info  ][     Rd53a     ]: Write named register: LatencyConfig -> 48
[16:11:08:274][  info  ][  scanConsole  ]: Starting histogrammer and analysis threads:
[16:11:08:274][  info  ][  scanConsole  ]:  .. started threads of Fe 4
[16:11:08:275][  info  ][  scanConsole  ]:  .. started threads of Fe 5
[16:11:08:275][  info  ][  scanConsole  ]:  .. started threads of Fe 6
[16:11:08:275][  info  ][  scanConsole  ]:  .. started threads of Fe 7
[16:11:08:275][  info  ][Rd53aDataProcessor]:   -> Processor thread #0 started!
[16:11:08:275][  info  ][Rd53aDataProcessor]:   -> Processor thread #1 started!
[16:11:08:275][  info  ][Rd53aDataProcessor]:   -> Processor thread #2 started!
[16:11:08:275][  info  ][Rd53aDataProcessor]:   -> Processor thread #3 started!
[16:11:08:275][  info  ][  scanConsole  ]: ########
[16:11:08:275][  info  ][  scanConsole  ]: # Scan #
[16:11:08:275][  info  ][  scanConsole  ]: ########
[16:11:08:275][  info  ][  scanConsole  ]: Starting scan!
[16:11:08:275][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0010055 on Rx 4
[16:11:08:280][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0010055] MON MUX_V: 3 Bias 0 1187 -> 0.261966 V Bias 1 1593 -> 0.342716 V, Temperature 0.0807507 -> 2.76245 C
[16:11:08:284][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0010055] MON MUX_V: 5 Bias 0 1165 -> 0.25759 V Bias 1 1571 -> 0.338341 V, Temperature 0.0807507 -> -5.6684 C
[16:11:08:288][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0010055] MON MUX_V: 7 Bias 0 1151 -> 0.254805 V Bias 1 1557 -> 0.335556 V, Temperature 0.0807507 -> -4.79996 C
[16:11:08:292][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0010055] MON MUX_V: 15 Bias 0 1132 -> 0.251026 V Bias 1 1545 -> 0.333169 V, Temperature 0.0821429 -> -5.71664 C
[16:11:08:327][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0010103 on Rx 5
[16:11:08:332][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0010103] MON MUX_V: 3 Bias 0 1199 -> 0.261884 V Bias 1 1609 -> 0.343604 V, Temperature 0.0817208 -> 6.81111 C
[16:11:08:336][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0010103] MON MUX_V: 5 Bias 0 1148 -> 0.251718 V Bias 1 1560 -> 0.333838 V, Temperature 0.0821195 -> 5.09998 C
[16:11:08:340][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0010103] MON MUX_V: 7 Bias 0 1129 -> 0.247931 V Bias 1 1552 -> 0.332243 V, Temperature 0.084312 -> 4.13754 C
[16:11:08:345][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0010103] MON MUX_V: 15 Bias 0 1146 -> 0.25132 V Bias 1 1561 -> 0.334037 V, Temperature 0.0827174 -> 3.80869 C
[16:11:08:379][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0010119 on Rx 6
[16:11:08:383][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0010119] MON MUX_V: 3 Bias 0 1184 -> 0.258806 V Bias 1 1599 -> 0.340748 V, Temperature 0.0819421 -> 4.08475 C
[16:11:08:388][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0010119] MON MUX_V: 5 Bias 0 1167 -> 0.255449 V Bias 1 1580 -> 0.336997 V, Temperature 0.0815472 -> -7.71179 C
[16:11:08:392][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0010119] MON MUX_V: 7 Bias 0 1148 -> 0.251698 V Bias 1 1567 -> 0.33443 V, Temperature 0.0827319 -> -3.93158 C
[16:11:08:396][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0010119] MON MUX_V: 15 Bias 0 1139 -> 0.249921 V Bias 1 1556 -> 0.332258 V, Temperature 0.082337 -> -4.79993 C
[16:11:08:431][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0010087 on Rx 7
[16:11:08:436][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0010087] MON MUX_V: 3 Bias 0 1206 -> 0.262638 V Bias 1 1608 -> 0.342414 V, Temperature 0.0797769 -> 4.97771 C
[16:11:08:440][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0010087] MON MUX_V: 5 Bias 0 1172 -> 0.25589 V Bias 1 1587 -> 0.338247 V, Temperature 0.0823568 -> -0.0857239 C
[16:11:08:444][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0010087] MON MUX_V: 7 Bias 0 1158 -> 0.253112 V Bias 1 1579 -> 0.336659 V, Temperature 0.0835475 -> -0.871368 C
[16:11:08:448][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0010087] MON MUX_V: 15 Bias 0 1162 -> 0.253906 V Bias 1 1579 -> 0.336659 V, Temperature 0.0827537 -> 0.221771 C
[16:11:08:484][  info  ][  scanConsole  ]: Scan done!
[16:11:08:484][  info  ][  scanConsole  ]: Waiting for processors to finish ...
[16:11:08:485][  info  ][  scanConsole  ]: Processor done, waiting for histogrammer ...
[16:11:08:885][  info  ][HistogramAlgorithm]: Histogrammer done!
[16:11:08:885][  info  ][HistogramAlgorithm]: Histogrammer done!
[16:11:08:885][  info  ][HistogramAlgorithm]: Histogrammer done!
[16:11:08:885][  info  ][HistogramAlgorithm]: Histogrammer done!
[16:11:08:885][  info  ][  scanConsole  ]: Processor done, waiting for analysis ...
[16:11:09:285][  info  ][AnalysisAlgorithm]: Analysis done!
[16:11:09:285][  info  ][AnalysisAlgorithm]: Analysis done!
[16:11:09:285][  info  ][AnalysisAlgorithm]: Analysis done!
[16:11:09:285][  info  ][AnalysisAlgorithm]: Analysis done!
[16:11:09:285][  info  ][  scanConsole  ]: All done!
[16:11:09:285][  info  ][  scanConsole  ]: ##########
[16:11:09:285][  info  ][  scanConsole  ]: # Timing #
[16:11:09:285][  info  ][  scanConsole  ]: ##########
[16:11:09:285][  info  ][  scanConsole  ]: -> Configuration: 121 ms
[16:11:09:285][  info  ][  scanConsole  ]: -> Scan:          209 ms
[16:11:09:285][  info  ][  scanConsole  ]: -> Processing:    0 ms
[16:11:09:285][  info  ][  scanConsole  ]: -> Analysis:      800 ms
[16:11:09:285][  info  ][  scanConsole  ]: ###########
[16:11:09:285][  info  ][  scanConsole  ]: # Cleanup #
[16:11:09:285][  info  ][  scanConsole  ]: ###########
[16:11:09:288][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0010055 to configs/CERNQ6/20UPGFC0010055.json
[16:11:09:468][  info  ][  scanConsole  ]: -> Storing output results of FE 4
[16:11:09:468][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0010055, this usually means that the chip did not send any data at all.
[16:11:09:469][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0010103 to configs/CERNQ6/20UPGFC0010103.json
[16:11:09:634][  info  ][  scanConsole  ]: -> Storing output results of FE 5
[16:11:09:634][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0010103, this usually means that the chip did not send any data at all.
[16:11:09:636][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0010119 to configs/CERNQ6/20UPGFC0010119.json
[16:11:09:800][  info  ][  scanConsole  ]: -> Storing output results of FE 6
[16:11:09:800][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0010119, this usually means that the chip did not send any data at all.
[16:11:09:801][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0010087 to configs/CERNQ6/20UPGFC0010087.json
[16:11:09:969][  info  ][  scanConsole  ]: -> Storing output results of FE 7
[16:11:09:969][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0010087, this usually means that the chip did not send any data at all.
[16:11:09:970][  info  ][  scanConsole  ]: Finishing run: 1516
[root@jollyroger Yarr]# 
