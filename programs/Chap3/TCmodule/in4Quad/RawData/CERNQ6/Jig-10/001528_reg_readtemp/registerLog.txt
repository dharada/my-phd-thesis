[17:00:13:318][  info  ][               ]: #####################################
[17:00:13:318][  info  ][               ]: # Welcome to the YARR Scan Console! #
[17:00:13:318][  info  ][               ]: #####################################
[17:00:13:318][  info  ][               ]: -> Parsing command line parameters ...
[17:00:13:318][  info  ][               ]: Configuring logger ...
[17:00:13:321][  info  ][  scanConsole  ]: Scan Type/Config configs/scans/rd53a/reg_readtemp.json
[17:00:13:321][  info  ][  scanConsole  ]: Connectivity:
[17:00:13:321][  info  ][  scanConsole  ]:     configs/CERNQ6/example_rd53a_setup.json
[17:00:13:321][  info  ][  scanConsole  ]: Target ToT: -1
[17:00:13:321][  info  ][  scanConsole  ]: Target Charge: -1
[17:00:13:321][  info  ][  scanConsole  ]: Output Plots: false
[17:00:13:321][  info  ][  scanConsole  ]: Output Directory: ./data/001528_reg_readtemp/
[17:00:13:324][  info  ][  scanConsole  ]: Timestamp: 2022-08-20_17:00:13
[17:00:13:324][  info  ][  scanConsole  ]: Run Number: 1528
[17:00:13:324][  info  ][  scanConsole  ]: #################
[17:00:13:324][  info  ][  scanConsole  ]: # Init Hardware #
[17:00:13:324][  info  ][  scanConsole  ]: #################
[17:00:13:324][  info  ][  scanConsole  ]: -> Opening controller config: configs/controller/specCfg-rd53a-16x1.json
[17:00:13:325][  info  ][  ScanHelper   ]: Loading controller ...
[17:00:13:325][  info  ][  ScanHelper   ]: Found controller of type: spec
[17:00:13:325][  info  ][  ScanHelper   ]: ... loading controler config:
[17:00:13:325][  info  ][  ScanHelper   ]: ~~~ {
[17:00:13:325][  info  ][  ScanHelper   ]: ~~~     "cmdPeriod": 6.250000073038109e-9,
[17:00:13:325][  info  ][  ScanHelper   ]: ~~~     "idle": {
[17:00:13:325][  info  ][  ScanHelper   ]: ~~~         "word": 1768515945
[17:00:13:325][  info  ][  ScanHelper   ]: ~~~     },
[17:00:13:325][  info  ][  ScanHelper   ]: ~~~     "pulse": {
[17:00:13:325][  info  ][  ScanHelper   ]: ~~~         "interval": 500,
[17:00:13:325][  info  ][  ScanHelper   ]: ~~~         "word": 1549575846
[17:00:13:325][  info  ][  ScanHelper   ]: ~~~     },
[17:00:13:325][  info  ][  ScanHelper   ]: ~~~     "rxActiveLanes": 1,
[17:00:13:325][  info  ][  ScanHelper   ]: ~~~     "rxPolarity": 0,
[17:00:13:325][  info  ][  ScanHelper   ]: ~~~     "specNum": 0,
[17:00:13:325][  info  ][  ScanHelper   ]: ~~~     "spiConfig": 541200,
[17:00:13:325][  info  ][  ScanHelper   ]: ~~~     "sync": {
[17:00:13:325][  info  ][  ScanHelper   ]: ~~~         "interval": 32,
[17:00:13:325][  info  ][  ScanHelper   ]: ~~~         "word": 2172551550
[17:00:13:325][  info  ][  ScanHelper   ]: ~~~     },
[17:00:13:325][  info  ][  ScanHelper   ]: ~~~     "txPolarity": 0
[17:00:13:325][  info  ][  ScanHelper   ]: ~~~ }
[17:00:13:325][  info  ][    SpecCom    ]: Opening SPEC with id #0
[17:00:13:325][  info  ][    SpecCom    ]: Mapping BARs ...
[17:00:13:325][  info  ][    SpecCom    ]: ... Mapped BAR0 at 0x7f2cb7c48000 with size 1048576
[17:00:13:325][warning ][    SpecCom    ]: ... BAR4 not mapped (Mmap failed)
[17:00:13:325][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[17:00:13:325][  info  ][    SpecCom    ]: Firmware Version: 0x4d9ff6d
[17:00:13:325][  info  ][    SpecCom    ]: Firmware Identifier: 0x4030232
[17:00:13:325][  info  ][    SpecCom    ]: FPGA card: PLDA XpressK7 325
[17:00:13:325][  info  ][    SpecCom    ]: FE Chip Type: RD53A/B
[17:00:13:325][  info  ][    SpecCom    ]: FMC Card Type: Ohio Card (Display Port)
[17:00:13:325][  info  ][    SpecCom    ]: RX Speed: 640Mbps
[17:00:13:325][  info  ][    SpecCom    ]: Channel Configuration: 16x1
[17:00:13:325][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[17:00:13:325][  info  ][    SpecCom    ]: Flushing buffers ...
[17:00:13:325][  info  ][    SpecCom    ]: Init success!
[17:00:13:325][  info  ][  scanConsole  ]: #######################
[17:00:13:325][  info  ][  scanConsole  ]: ##  Loading Configs  ##
[17:00:13:325][  info  ][  scanConsole  ]: #######################
[17:00:13:325][  info  ][  scanConsole  ]: Opening global config: configs/CERNQ6/example_rd53a_setup.json
[17:00:13:325][  info  ][  ScanHelper   ]: Chip type: RD53A
[17:00:13:325][  info  ][  ScanHelper   ]: Chip count 4
[17:00:13:325][  info  ][  ScanHelper   ]: Loading chip #0
[17:00:13:326][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(4)
[17:00:13:326][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ6/20UPGFC0010055.json
[17:00:13:502][  info  ][  ScanHelper   ]: Loading chip #1
[17:00:13:503][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(5)
[17:00:13:503][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ6/20UPGFC0010103.json
[17:00:13:676][  info  ][  ScanHelper   ]: Loading chip #2
[17:00:13:676][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(6)
[17:00:13:676][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ6/20UPGFC0010119.json
[17:00:13:858][  info  ][  ScanHelper   ]: Loading chip #3
[17:00:13:858][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(7)
[17:00:13:858][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ6/20UPGFC0010087.json
[17:00:14:038][  info  ][  scanConsole  ]: #################
[17:00:14:038][  info  ][  scanConsole  ]: # Configure FEs #
[17:00:14:038][  info  ][  scanConsole  ]: #################
[17:00:14:038][  info  ][  scanConsole  ]: Configuring 20UPGFC0010055
[17:00:14:069][  info  ][  scanConsole  ]: Configuring 20UPGFC0010103
[17:00:14:100][  info  ][  scanConsole  ]: Configuring 20UPGFC0010119
[17:00:14:130][  info  ][  scanConsole  ]: Configuring 20UPGFC0010087
[17:00:14:160][  info  ][  scanConsole  ]: Sent configuration to all FEs in 121 ms!
[17:00:14:161][  info  ][  scanConsole  ]: Checking com 20UPGFC0010055
[17:00:14:161][  info  ][    SpecRx     ]: Active Rx channels: 0x10
[17:00:14:161][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[17:00:14:161][  info  ][    SpecRx     ]: Rx Status 0xf0
[17:00:14:161][  info  ][    SpecRx     ]: Number of lanes: 1
[17:00:14:161][  info  ][    SpecRx     ]: Channel 4 Lane 0 synchronized!
[17:00:14:172][  info  ][  scanConsole  ]: ... success!
[17:00:14:172][  info  ][  scanConsole  ]: Checking com 20UPGFC0010103
[17:00:14:172][  info  ][    SpecRx     ]: Active Rx channels: 0x20
[17:00:14:172][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[17:00:14:172][  info  ][    SpecRx     ]: Rx Status 0xf0
[17:00:14:172][  info  ][    SpecRx     ]: Number of lanes: 1
[17:00:14:172][  info  ][    SpecRx     ]: Channel 5 Lane 0 synchronized!
[17:00:14:182][  info  ][  scanConsole  ]: ... success!
[17:00:14:182][  info  ][  scanConsole  ]: Checking com 20UPGFC0010119
[17:00:14:182][  info  ][    SpecRx     ]: Active Rx channels: 0x40
[17:00:14:183][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[17:00:14:183][  info  ][    SpecRx     ]: Rx Status 0xf0
[17:00:14:183][  info  ][    SpecRx     ]: Number of lanes: 1
[17:00:14:183][  info  ][    SpecRx     ]: Channel 6 Lane 0 synchronized!
[17:00:14:193][  info  ][  scanConsole  ]: ... success!
[17:00:14:193][  info  ][  scanConsole  ]: Checking com 20UPGFC0010087
[17:00:14:193][  info  ][    SpecRx     ]: Active Rx channels: 0x80
[17:00:14:193][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[17:00:14:193][  info  ][    SpecRx     ]: Rx Status 0xf0
[17:00:14:193][  info  ][    SpecRx     ]: Number of lanes: 1
[17:00:14:193][  info  ][    SpecRx     ]: Channel 7 Lane 0 synchronized!
[17:00:14:204][  info  ][  scanConsole  ]: ... success!
[17:00:14:204][  info  ][  scanConsole  ]: Enabling Tx channels
[17:00:14:204][  info  ][  scanConsole  ]: Enabling Tx channel 1
[17:00:14:204][  info  ][  scanConsole  ]: Enabling Tx channel 1
[17:00:14:204][  info  ][  scanConsole  ]: Enabling Tx channel 1
[17:00:14:204][  info  ][  scanConsole  ]: Enabling Tx channel 1
[17:00:14:204][  info  ][  scanConsole  ]: Enabling Rx channels
[17:00:14:204][  info  ][  scanConsole  ]: Enabling Rx channel 4
[17:00:14:204][  info  ][  scanConsole  ]: Enabling Rx channel 5
[17:00:14:204][  info  ][  scanConsole  ]: Enabling Rx channel 6
[17:00:14:204][  info  ][  scanConsole  ]: Enabling Rx channel 7
[17:00:14:204][  info  ][  scanConsole  ]: ##############
[17:00:14:204][  info  ][  scanConsole  ]: # Setup Scan #
[17:00:14:204][  info  ][  scanConsole  ]: ##############
[17:00:14:204][  info  ][  scanConsole  ]: Found Scan config, constructing scan ...
[17:00:14:205][  info  ][  ScanFactory  ]: Loading Scan:
[17:00:14:205][  info  ][  ScanFactory  ]:   Name: AnalogScan
[17:00:14:205][  info  ][  ScanFactory  ]:   Number of Loops: 3
[17:00:14:205][  info  ][  ScanFactory  ]:   Loading Loop #0
[17:00:14:205][  info  ][  ScanFactory  ]:    Type: Rd53aReadRegLoop
[17:00:14:205][  info  ][  ScanFactory  ]:    Loading loop config ... 
[17:00:14:205][  info  ][  ScanFactory  ]: ~~~ {
[17:00:14:205][  info  ][  ScanFactory  ]: ~~~     "CurMux": [],
[17:00:14:205][  info  ][  ScanFactory  ]: ~~~     "EnblRingOsc": 0,
[17:00:14:205][  info  ][  ScanFactory  ]: ~~~     "Registers": [],
[17:00:14:205][  info  ][  ScanFactory  ]: ~~~     "RingOscDur": 9,
[17:00:14:205][  info  ][  ScanFactory  ]: ~~~     "RingOscRep": 10,
[17:00:14:205][  info  ][  ScanFactory  ]: ~~~     "VoltMux": [3, 5, 7, 15]
[17:00:14:205][  info  ][  ScanFactory  ]: ~~~ }
[17:00:14:205][  info  ][  ScanFactory  ]:   Loading Loop #1
[17:00:14:205][  info  ][  ScanFactory  ]:    Type: Rd53aTriggerLoop
[17:00:14:205][  info  ][  ScanFactory  ]:    Loading loop config ... 
[17:00:14:205][  info  ][  ScanFactory  ]: ~~~ {
[17:00:14:205][  info  ][  ScanFactory  ]: ~~~     "count": 1,
[17:00:14:205][  info  ][  ScanFactory  ]: ~~~     "delay": 48,
[17:00:14:206][  info  ][  ScanFactory  ]: ~~~     "extTrig": false,
[17:00:14:206][  info  ][  ScanFactory  ]: ~~~     "frequency": 30000.0,
[17:00:14:206][  info  ][  ScanFactory  ]: ~~~     "noInject": false,
[17:00:14:206][  info  ][  ScanFactory  ]: ~~~     "sendEcr": false,
[17:00:14:206][  info  ][  ScanFactory  ]: ~~~     "time": 0.0,
[17:00:14:206][  info  ][  ScanFactory  ]: ~~~     "trigMultiplier": 16
[17:00:14:206][  info  ][  ScanFactory  ]: ~~~ }
[17:00:14:206][  info  ][  ScanFactory  ]:   Loading Loop #2
[17:00:14:206][  info  ][  ScanFactory  ]:    Type: StdDataLoop
[17:00:14:206][warning ][  ScanFactory  ]: ~~~ Config empty.
[17:00:14:206][  info  ][ScanBuildHistogrammers]: Loading histogrammer ...
[17:00:14:206][  info  ][ScanBuildHistogrammers]: ... done!
[17:00:14:206][  info  ][ScanBuildAnalyses]: Loading analyses ...
[17:00:14:206][  info  ][  scanConsole  ]: Running pre scan!
[17:00:14:206][  info  ][  ScanFactory  ]: Entering pre scan phase ...
[17:00:14:207][  info  ][     Rd53a     ]: Write named register: GlobalPulseRt -> 16384
[17:00:14:207][  info  ][     Rd53a     ]: Write named register: InjEnDig -> 0
[17:00:14:207][  info  ][     Rd53a     ]: Write named register: InjVcalDiff -> 0
[17:00:14:207][  info  ][     Rd53a     ]: Write named register: InjVcalHigh -> 3500
[17:00:14:207][  info  ][     Rd53a     ]: Write named register: InjVcalMed -> 500
[17:00:14:207][  info  ][     Rd53a     ]: Write named register: LatencyConfig -> 48
[17:00:14:207][  info  ][  scanConsole  ]: Starting histogrammer and analysis threads:
[17:00:14:207][  info  ][  scanConsole  ]:  .. started threads of Fe 4
[17:00:14:207][  info  ][  scanConsole  ]:  .. started threads of Fe 5
[17:00:14:207][  info  ][  scanConsole  ]:  .. started threads of Fe 6
[17:00:14:207][  info  ][  scanConsole  ]:  .. started threads of Fe 7
[17:00:14:208][  info  ][Rd53aDataProcessor]:   -> Processor thread #0 started!
[17:00:14:208][  info  ][Rd53aDataProcessor]:   -> Processor thread #1 started!
[17:00:14:208][  info  ][Rd53aDataProcessor]:   -> Processor thread #2 started!
[17:00:14:208][  info  ][Rd53aDataProcessor]:   -> Processor thread #3 started!
[17:00:14:208][  info  ][  scanConsole  ]: ########
[17:00:14:208][  info  ][  scanConsole  ]: # Scan #
[17:00:14:208][  info  ][  scanConsole  ]: ########
[17:00:14:208][  info  ][  scanConsole  ]: Starting scan!
[17:00:14:208][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0010055 on Rx 4
[17:00:14:212][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0010055] MON MUX_V: 3 Bias 0 1240 -> 0.272507 V Bias 1 1632 -> 0.350473 V, Temperature 0.0779662 -> -6.86249 C
[17:00:14:216][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0010055] MON MUX_V: 5 Bias 0 1218 -> 0.268131 V Bias 1 1609 -> 0.345899 V, Temperature 0.0777673 -> -18.6948 C
[17:00:14:221][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0010055] MON MUX_V: 7 Bias 0 1204 -> 0.265347 V Bias 1 1595 -> 0.343114 V, Temperature 0.0777673 -> -17.8262 C
[17:00:14:225][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0010055] MON MUX_V: 15 Bias 0 1183 -> 0.26117 V Bias 1 1583 -> 0.340727 V, Temperature 0.0795574 -> -17.6332 C
[17:00:14:259][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0010103 on Rx 5
[17:00:14:264][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0010103] MON MUX_V: 3 Bias 0 1254 -> 0.272846 V Bias 1 1649 -> 0.351577 V, Temperature 0.078731 -> -2.35559 C
[17:00:14:268][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0010103] MON MUX_V: 5 Bias 0 1203 -> 0.262681 V Bias 1 1601 -> 0.34201 V, Temperature 0.079329 -> -4.14017 C
[17:00:14:272][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0010103] MON MUX_V: 7 Bias 0 1184 -> 0.258894 V Bias 1 1593 -> 0.340415 V, Temperature 0.0815215 -> -5.48749 C
[17:00:14:277][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0010103] MON MUX_V: 15 Bias 0 1199 -> 0.261884 V Bias 1 1602 -> 0.342209 V, Temperature 0.0803256 -> -4.80008 C
[17:00:14:311][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0010119 on Rx 6
[17:00:14:315][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0010119] MON MUX_V: 3 Bias 0 1238 -> 0.269468 V Bias 1 1640 -> 0.348844 V, Temperature 0.0793753 -> -4.1653 C
[17:00:14:319][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0010119] MON MUX_V: 5 Bias 0 1223 -> 0.266507 V Bias 1 1623 -> 0.345487 V, Temperature 0.0789803 -> -20.3295 C
[17:00:14:323][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0010119] MON MUX_V: 7 Bias 0 1205 -> 0.262952 V Bias 1 1611 -> 0.343118 V, Temperature 0.0801651 -> -15.221 C
[17:00:14:327][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0010119] MON MUX_V: 15 Bias 0 1195 -> 0.260978 V Bias 1 1598 -> 0.340551 V, Temperature 0.0795727 -> -17.6333 C
[17:00:14:361][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0010087 on Rx 7
[17:00:14:365][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0010087] MON MUX_V: 3 Bias 0 1259 -> 0.273155 V Bias 1 1649 -> 0.350551 V, Temperature 0.0773955 -> -2.35558 C
[17:00:14:369][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0010087] MON MUX_V: 5 Bias 0 1226 -> 0.266607 V Bias 1 1628 -> 0.346384 V, Temperature 0.0797769 -> -10.3 C
[17:00:14:372][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0010087] MON MUX_V: 7 Bias 0 1213 -> 0.264027 V Bias 1 1621 -> 0.344994 V, Temperature 0.0809677 -> -11.0856 C
[17:00:14:376][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0010087] MON MUX_V: 15 Bias 0 1215 -> 0.264424 V Bias 1 1619 -> 0.344597 V, Temperature 0.0801738 -> -9.1044 C
[17:00:14:412][  info  ][  scanConsole  ]: Scan done!
[17:00:14:412][  info  ][  scanConsole  ]: Waiting for processors to finish ...
[17:00:14:412][  info  ][  scanConsole  ]: Processor done, waiting for histogrammer ...
[17:00:14:812][  info  ][HistogramAlgorithm]: Histogrammer done!
[17:00:14:812][  info  ][HistogramAlgorithm]: Histogrammer done!
[17:00:14:812][  info  ][HistogramAlgorithm]: Histogrammer done!
[17:00:14:812][  info  ][HistogramAlgorithm]: Histogrammer done!
[17:00:14:812][  info  ][  scanConsole  ]: Processor done, waiting for analysis ...
[17:00:15:212][  info  ][AnalysisAlgorithm]: Analysis done!
[17:00:15:212][  info  ][AnalysisAlgorithm]: Analysis done!
[17:00:15:212][  info  ][AnalysisAlgorithm]: Analysis done!
[17:00:15:212][  info  ][AnalysisAlgorithm]: Analysis done!
[17:00:15:213][  info  ][  scanConsole  ]: All done!
[17:00:15:213][  info  ][  scanConsole  ]: ##########
[17:00:15:213][  info  ][  scanConsole  ]: # Timing #
[17:00:15:213][  info  ][  scanConsole  ]: ##########
[17:00:15:213][  info  ][  scanConsole  ]: -> Configuration: 121 ms
[17:00:15:213][  info  ][  scanConsole  ]: -> Scan:          203 ms
[17:00:15:213][  info  ][  scanConsole  ]: -> Processing:    0 ms
[17:00:15:213][  info  ][  scanConsole  ]: -> Analysis:      800 ms
[17:00:15:213][  info  ][  scanConsole  ]: ###########
[17:00:15:213][  info  ][  scanConsole  ]: # Cleanup #
[17:00:15:213][  info  ][  scanConsole  ]: ###########
[17:00:15:221][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0010055 to configs/CERNQ6/20UPGFC0010055.json
[17:00:15:423][  info  ][  scanConsole  ]: -> Storing output results of FE 4
[17:00:15:423][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0010055, this usually means that the chip did not send any data at all.
[17:00:15:424][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0010103 to configs/CERNQ6/20UPGFC0010103.json
[17:00:15:591][  info  ][  scanConsole  ]: -> Storing output results of FE 5
[17:00:15:591][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0010103, this usually means that the chip did not send any data at all.
[17:00:15:592][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0010119 to configs/CERNQ6/20UPGFC0010119.json
[17:00:15:757][  info  ][  scanConsole  ]: -> Storing output results of FE 6
[17:00:15:757][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0010119, this usually means that the chip did not send any data at all.
[17:00:15:759][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0010087 to configs/CERNQ6/20UPGFC0010087.json
[17:00:15:926][  info  ][  scanConsole  ]: -> Storing output results of FE 7
[17:00:15:926][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0010087, this usually means that the chip did not send any data at all.
[17:00:15:927][  info  ][  scanConsole  ]: Finishing run: 1528
[root@jollyroger Yarr]# 
