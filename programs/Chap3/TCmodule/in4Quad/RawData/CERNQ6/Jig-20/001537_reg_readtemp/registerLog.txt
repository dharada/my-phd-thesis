[17:40:48:382][  info  ][               ]: #####################################
[17:40:48:382][  info  ][               ]: # Welcome to the YARR Scan Console! #
[17:40:48:382][  info  ][               ]: #####################################
[17:40:48:382][  info  ][               ]: -> Parsing command line parameters ...
[17:40:48:382][  info  ][               ]: Configuring logger ...
[17:40:48:385][  info  ][  scanConsole  ]: Scan Type/Config configs/scans/rd53a/reg_readtemp.json
[17:40:48:385][  info  ][  scanConsole  ]: Connectivity:
[17:40:48:385][  info  ][  scanConsole  ]:     configs/CERNQ6/example_rd53a_setup.json
[17:40:48:385][  info  ][  scanConsole  ]: Target ToT: -1
[17:40:48:385][  info  ][  scanConsole  ]: Target Charge: -1
[17:40:48:385][  info  ][  scanConsole  ]: Output Plots: false
[17:40:48:385][  info  ][  scanConsole  ]: Output Directory: ./data/001537_reg_readtemp/
[17:40:48:389][  info  ][  scanConsole  ]: Timestamp: 2022-08-20_17:40:48
[17:40:48:389][  info  ][  scanConsole  ]: Run Number: 1537
[17:40:48:389][  info  ][  scanConsole  ]: #################
[17:40:48:389][  info  ][  scanConsole  ]: # Init Hardware #
[17:40:48:389][  info  ][  scanConsole  ]: #################
[17:40:48:389][  info  ][  scanConsole  ]: -> Opening controller config: configs/controller/specCfg-rd53a-16x1.json
[17:40:48:389][  info  ][  ScanHelper   ]: Loading controller ...
[17:40:48:389][  info  ][  ScanHelper   ]: Found controller of type: spec
[17:40:48:389][  info  ][  ScanHelper   ]: ... loading controler config:
[17:40:48:389][  info  ][  ScanHelper   ]: ~~~ {
[17:40:48:389][  info  ][  ScanHelper   ]: ~~~     "cmdPeriod": 6.250000073038109e-9,
[17:40:48:389][  info  ][  ScanHelper   ]: ~~~     "idle": {
[17:40:48:389][  info  ][  ScanHelper   ]: ~~~         "word": 1768515945
[17:40:48:389][  info  ][  ScanHelper   ]: ~~~     },
[17:40:48:389][  info  ][  ScanHelper   ]: ~~~     "pulse": {
[17:40:48:389][  info  ][  ScanHelper   ]: ~~~         "interval": 500,
[17:40:48:389][  info  ][  ScanHelper   ]: ~~~         "word": 1549575846
[17:40:48:389][  info  ][  ScanHelper   ]: ~~~     },
[17:40:48:389][  info  ][  ScanHelper   ]: ~~~     "rxActiveLanes": 1,
[17:40:48:389][  info  ][  ScanHelper   ]: ~~~     "rxPolarity": 0,
[17:40:48:389][  info  ][  ScanHelper   ]: ~~~     "specNum": 0,
[17:40:48:389][  info  ][  ScanHelper   ]: ~~~     "spiConfig": 541200,
[17:40:48:389][  info  ][  ScanHelper   ]: ~~~     "sync": {
[17:40:48:389][  info  ][  ScanHelper   ]: ~~~         "interval": 32,
[17:40:48:389][  info  ][  ScanHelper   ]: ~~~         "word": 2172551550
[17:40:48:389][  info  ][  ScanHelper   ]: ~~~     },
[17:40:48:389][  info  ][  ScanHelper   ]: ~~~     "txPolarity": 0
[17:40:48:389][  info  ][  ScanHelper   ]: ~~~ }
[17:40:48:389][  info  ][    SpecCom    ]: Opening SPEC with id #0
[17:40:48:390][  info  ][    SpecCom    ]: Mapping BARs ...
[17:40:48:390][  info  ][    SpecCom    ]: ... Mapped BAR0 at 0x7f535f46f000 with size 1048576
[17:40:48:390][warning ][    SpecCom    ]: ... BAR4 not mapped (Mmap failed)
[17:40:48:390][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[17:40:48:390][  info  ][    SpecCom    ]: Firmware Version: 0x4d9ff6d
[17:40:48:390][  info  ][    SpecCom    ]: Firmware Identifier: 0x4030232
[17:40:48:390][  info  ][    SpecCom    ]: FPGA card: PLDA XpressK7 325
[17:40:48:390][  info  ][    SpecCom    ]: FE Chip Type: RD53A/B
[17:40:48:390][  info  ][    SpecCom    ]: FMC Card Type: Ohio Card (Display Port)
[17:40:48:390][  info  ][    SpecCom    ]: RX Speed: 640Mbps
[17:40:48:390][  info  ][    SpecCom    ]: Channel Configuration: 16x1
[17:40:48:390][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[17:40:48:390][  info  ][    SpecCom    ]: Flushing buffers ...
[17:40:48:390][  info  ][    SpecCom    ]: Init success!
[17:40:48:390][  info  ][  scanConsole  ]: #######################
[17:40:48:390][  info  ][  scanConsole  ]: ##  Loading Configs  ##
[17:40:48:390][  info  ][  scanConsole  ]: #######################
[17:40:48:390][  info  ][  scanConsole  ]: Opening global config: configs/CERNQ6/example_rd53a_setup.json
[17:40:48:390][  info  ][  ScanHelper   ]: Chip type: RD53A
[17:40:48:390][  info  ][  ScanHelper   ]: Chip count 4
[17:40:48:390][  info  ][  ScanHelper   ]: Loading chip #0
[17:40:48:391][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(4)
[17:40:48:391][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ6/20UPGFC0010055.json
[17:40:48:567][  info  ][  ScanHelper   ]: Loading chip #1
[17:40:48:568][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(5)
[17:40:48:568][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ6/20UPGFC0010103.json
[17:40:48:739][  info  ][  ScanHelper   ]: Loading chip #2
[17:40:48:740][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(6)
[17:40:48:740][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ6/20UPGFC0010119.json
[17:40:48:918][  info  ][  ScanHelper   ]: Loading chip #3
[17:40:48:918][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(7)
[17:40:48:918][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ6/20UPGFC0010087.json
[17:40:49:099][  info  ][  scanConsole  ]: #################
[17:40:49:099][  info  ][  scanConsole  ]: # Configure FEs #
[17:40:49:099][  info  ][  scanConsole  ]: #################
[17:40:49:099][  info  ][  scanConsole  ]: Configuring 20UPGFC0010055
[17:40:49:130][  info  ][  scanConsole  ]: Configuring 20UPGFC0010103
[17:40:49:160][  info  ][  scanConsole  ]: Configuring 20UPGFC0010119
[17:40:49:191][  info  ][  scanConsole  ]: Configuring 20UPGFC0010087
[17:40:49:226][  info  ][  scanConsole  ]: Sent configuration to all FEs in 127 ms!
[17:40:49:227][  info  ][  scanConsole  ]: Checking com 20UPGFC0010055
[17:40:49:227][  info  ][    SpecRx     ]: Active Rx channels: 0x10
[17:40:49:228][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[17:40:49:228][  info  ][    SpecRx     ]: Rx Status 0xf0
[17:40:49:228][  info  ][    SpecRx     ]: Number of lanes: 1
[17:40:49:228][  info  ][    SpecRx     ]: Channel 4 Lane 0 synchronized!
[17:40:49:238][  info  ][  scanConsole  ]: ... success!
[17:40:49:238][  info  ][  scanConsole  ]: Checking com 20UPGFC0010103
[17:40:49:238][  info  ][    SpecRx     ]: Active Rx channels: 0x20
[17:40:49:238][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[17:40:49:238][  info  ][    SpecRx     ]: Rx Status 0xf0
[17:40:49:238][  info  ][    SpecRx     ]: Number of lanes: 1
[17:40:49:238][  info  ][    SpecRx     ]: Channel 5 Lane 0 synchronized!
[17:40:49:249][  info  ][  scanConsole  ]: ... success!
[17:40:49:249][  info  ][  scanConsole  ]: Checking com 20UPGFC0010119
[17:40:49:249][  info  ][    SpecRx     ]: Active Rx channels: 0x40
[17:40:49:249][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[17:40:49:249][  info  ][    SpecRx     ]: Rx Status 0xf0
[17:40:49:249][  info  ][    SpecRx     ]: Number of lanes: 1
[17:40:49:249][  info  ][    SpecRx     ]: Channel 6 Lane 0 synchronized!
[17:40:49:259][  info  ][  scanConsole  ]: ... success!
[17:40:49:259][  info  ][  scanConsole  ]: Checking com 20UPGFC0010087
[17:40:49:259][  info  ][    SpecRx     ]: Active Rx channels: 0x80
[17:40:49:259][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[17:40:49:259][  info  ][    SpecRx     ]: Rx Status 0xf0
[17:40:49:259][  info  ][    SpecRx     ]: Number of lanes: 1
[17:40:49:259][  info  ][    SpecRx     ]: Channel 7 Lane 0 synchronized!
[17:40:49:270][  info  ][  scanConsole  ]: ... success!
[17:40:49:270][  info  ][  scanConsole  ]: Enabling Tx channels
[17:40:49:270][  info  ][  scanConsole  ]: Enabling Tx channel 1
[17:40:49:270][  info  ][  scanConsole  ]: Enabling Tx channel 1
[17:40:49:270][  info  ][  scanConsole  ]: Enabling Tx channel 1
[17:40:49:270][  info  ][  scanConsole  ]: Enabling Tx channel 1
[17:40:49:270][  info  ][  scanConsole  ]: Enabling Rx channels
[17:40:49:270][  info  ][  scanConsole  ]: Enabling Rx channel 4
[17:40:49:270][  info  ][  scanConsole  ]: Enabling Rx channel 5
[17:40:49:270][  info  ][  scanConsole  ]: Enabling Rx channel 6
[17:40:49:270][  info  ][  scanConsole  ]: Enabling Rx channel 7
[17:40:49:270][  info  ][  scanConsole  ]: ##############
[17:40:49:270][  info  ][  scanConsole  ]: # Setup Scan #
[17:40:49:270][  info  ][  scanConsole  ]: ##############
[17:40:49:270][  info  ][  scanConsole  ]: Found Scan config, constructing scan ...
[17:40:49:271][  info  ][  ScanFactory  ]: Loading Scan:
[17:40:49:271][  info  ][  ScanFactory  ]:   Name: AnalogScan
[17:40:49:271][  info  ][  ScanFactory  ]:   Number of Loops: 3
[17:40:49:271][  info  ][  ScanFactory  ]:   Loading Loop #0
[17:40:49:271][  info  ][  ScanFactory  ]:    Type: Rd53aReadRegLoop
[17:40:49:271][  info  ][  ScanFactory  ]:    Loading loop config ... 
[17:40:49:271][  info  ][  ScanFactory  ]: ~~~ {
[17:40:49:271][  info  ][  ScanFactory  ]: ~~~     "CurMux": [],
[17:40:49:271][  info  ][  ScanFactory  ]: ~~~     "EnblRingOsc": 0,
[17:40:49:271][  info  ][  ScanFactory  ]: ~~~     "Registers": [],
[17:40:49:271][  info  ][  ScanFactory  ]: ~~~     "RingOscDur": 9,
[17:40:49:271][  info  ][  ScanFactory  ]: ~~~     "RingOscRep": 10,
[17:40:49:271][  info  ][  ScanFactory  ]: ~~~     "VoltMux": [3, 5, 7, 15]
[17:40:49:271][  info  ][  ScanFactory  ]: ~~~ }
[17:40:49:271][  info  ][  ScanFactory  ]:   Loading Loop #1
[17:40:49:271][  info  ][  ScanFactory  ]:    Type: Rd53aTriggerLoop
[17:40:49:271][  info  ][  ScanFactory  ]:    Loading loop config ... 
[17:40:49:271][  info  ][  ScanFactory  ]: ~~~ {
[17:40:49:271][  info  ][  ScanFactory  ]: ~~~     "count": 1,
[17:40:49:271][  info  ][  ScanFactory  ]: ~~~     "delay": 48,
[17:40:49:271][  info  ][  ScanFactory  ]: ~~~     "extTrig": false,
[17:40:49:271][  info  ][  ScanFactory  ]: ~~~     "frequency": 30000.0,
[17:40:49:271][  info  ][  ScanFactory  ]: ~~~     "noInject": false,
[17:40:49:272][  info  ][  ScanFactory  ]: ~~~     "sendEcr": false,
[17:40:49:272][  info  ][  ScanFactory  ]: ~~~     "time": 0.0,
[17:40:49:272][  info  ][  ScanFactory  ]: ~~~     "trigMultiplier": 16
[17:40:49:272][  info  ][  ScanFactory  ]: ~~~ }
[17:40:49:272][  info  ][  ScanFactory  ]:   Loading Loop #2
[17:40:49:272][  info  ][  ScanFactory  ]:    Type: StdDataLoop
[17:40:49:272][warning ][  ScanFactory  ]: ~~~ Config empty.
[17:40:49:272][  info  ][ScanBuildHistogrammers]: Loading histogrammer ...
[17:40:49:272][  info  ][ScanBuildHistogrammers]: ... done!
[17:40:49:272][  info  ][ScanBuildAnalyses]: Loading analyses ...
[17:40:49:272][  info  ][  scanConsole  ]: Running pre scan!
[17:40:49:272][  info  ][  ScanFactory  ]: Entering pre scan phase ...
[17:40:49:272][  info  ][     Rd53a     ]: Write named register: GlobalPulseRt -> 16384
[17:40:49:273][  info  ][     Rd53a     ]: Write named register: InjEnDig -> 0
[17:40:49:273][  info  ][     Rd53a     ]: Write named register: InjVcalDiff -> 0
[17:40:49:273][  info  ][     Rd53a     ]: Write named register: InjVcalHigh -> 3500
[17:40:49:273][  info  ][     Rd53a     ]: Write named register: InjVcalMed -> 500
[17:40:49:273][  info  ][     Rd53a     ]: Write named register: LatencyConfig -> 48
[17:40:49:273][  info  ][  scanConsole  ]: Starting histogrammer and analysis threads:
[17:40:49:273][  info  ][  scanConsole  ]:  .. started threads of Fe 4
[17:40:49:273][  info  ][  scanConsole  ]:  .. started threads of Fe 5
[17:40:49:273][  info  ][  scanConsole  ]:  .. started threads of Fe 6
[17:40:49:273][  info  ][  scanConsole  ]:  .. started threads of Fe 7
[17:40:49:274][  info  ][Rd53aDataProcessor]:   -> Processor thread #0 started!
[17:40:49:274][  info  ][Rd53aDataProcessor]:   -> Processor thread #1 started!
[17:40:49:274][  info  ][Rd53aDataProcessor]:   -> Processor thread #2 started!
[17:40:49:274][  info  ][Rd53aDataProcessor]:   -> Processor thread #3 started!
[17:40:49:274][  info  ][  scanConsole  ]: ########
[17:40:49:274][  info  ][  scanConsole  ]: # Scan #
[17:40:49:274][  info  ][  scanConsole  ]: ########
[17:40:49:274][  info  ][  scanConsole  ]: Starting scan!
[17:40:49:274][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0010055 on Rx 4
[17:40:49:278][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0010055] MON MUX_V: 3 Bias 0 1291 -> 0.28265 V Bias 1 1669 -> 0.357832 V, Temperature 0.0751817 -> -16.4874 C
[17:40:49:282][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0010055] MON MUX_V: 5 Bias 0 1270 -> 0.278474 V Bias 1 1647 -> 0.353456 V, Temperature 0.0749827 -> -30.8529 C
[17:40:49:286][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0010055] MON MUX_V: 7 Bias 0 1257 -> 0.275888 V Bias 1 1634 -> 0.350871 V, Temperature 0.0749828 -> -29.9842 C
[17:40:49:290][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0010055] MON MUX_V: 15 Bias 0 1236 -> 0.271711 V Bias 1 1622 -> 0.348484 V, Temperature 0.0767728 -> -30.4666 C
[17:40:49:323][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0010103 on Rx 5
[17:40:49:327][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0010103] MON MUX_V: 3 Bias 0 1308 -> 0.283609 V Bias 1 1689 -> 0.35955 V, Temperature 0.0759406 -> -10.911 C
[17:40:49:331][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0010103] MON MUX_V: 5 Bias 0 1257 -> 0.273444 V Bias 1 1642 -> 0.350182 V, Temperature 0.0767379 -> -12.72 C
[17:40:49:335][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0010103] MON MUX_V: 7 Bias 0 1239 -> 0.269856 V Bias 1 1634 -> 0.348587 V, Temperature 0.078731 -> -15.1125 C
[17:40:49:339][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0010103] MON MUX_V: 15 Bias 0 1257 -> 0.273444 V Bias 1 1644 -> 0.350581 V, Temperature 0.0771365 -> -16.2782 C
[17:40:49:373][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0010119 on Rx 6
[17:40:49:377][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0010119] MON MUX_V: 3 Bias 0 1291 -> 0.279933 V Bias 1 1680 -> 0.356742 V, Temperature 0.0768084 -> -12.4153 C
[17:40:49:380][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0010119] MON MUX_V: 5 Bias 0 1279 -> 0.277564 V Bias 1 1663 -> 0.353385 V, Temperature 0.0758211 -> -35.859 C
[17:40:49:384][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0010119] MON MUX_V: 7 Bias 0 1260 -> 0.273812 V Bias 1 1651 -> 0.351016 V, Temperature 0.0772033 -> -28.2474 C
[17:40:49:388][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0010119] MON MUX_V: 15 Bias 0 1251 -> 0.272035 V Bias 1 1639 -> 0.348646 V, Temperature 0.0766109 -> -31.3835 C
[17:40:49:422][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0010087 on Rx 7
[17:40:49:426][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0010087] MON MUX_V: 3 Bias 0 1312 -> 0.283673 V Bias 1 1688 -> 0.358291 V, Temperature 0.0746172 -> -10.9111 C
[17:40:49:430][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0010087] MON MUX_V: 5 Bias 0 1280 -> 0.277323 V Bias 1 1668 -> 0.354322 V, Temperature 0.0769986 -> -21.3 C
[17:40:49:434][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0010087] MON MUX_V: 7 Bias 0 1269 -> 0.27514 V Bias 1 1662 -> 0.353131 V, Temperature 0.0779909 -> -22.8715 C
[17:40:49:438][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0010087] MON MUX_V: 15 Bias 0 1270 -> 0.275338 V Bias 1 1660 -> 0.352734 V, Temperature 0.0773955 -> -19.1478 C
[17:40:49:473][  info  ][  scanConsole  ]: Scan done!
[17:40:49:473][  info  ][  scanConsole  ]: Waiting for processors to finish ...
[17:40:49:474][  info  ][  scanConsole  ]: Processor done, waiting for histogrammer ...
[17:40:49:873][  info  ][HistogramAlgorithm]: Histogrammer done!
[17:40:49:873][  info  ][HistogramAlgorithm]: Histogrammer done!
[17:40:49:873][  info  ][HistogramAlgorithm]: Histogrammer done!
[17:40:49:873][  info  ][HistogramAlgorithm]: Histogrammer done!
[17:40:49:873][  info  ][  scanConsole  ]: Processor done, waiting for analysis ...
[17:40:50:273][  info  ][AnalysisAlgorithm]: Analysis done!
[17:40:50:273][  info  ][AnalysisAlgorithm]: Analysis done!
[17:40:50:274][  info  ][AnalysisAlgorithm]: Analysis done!
[17:40:50:274][  info  ][AnalysisAlgorithm]: Analysis done!
[17:40:50:274][  info  ][  scanConsole  ]: All done!
[17:40:50:274][  info  ][  scanConsole  ]: ##########
[17:40:50:274][  info  ][  scanConsole  ]: # Timing #
[17:40:50:274][  info  ][  scanConsole  ]: ##########
[17:40:50:274][  info  ][  scanConsole  ]: -> Configuration: 127 ms
[17:40:50:274][  info  ][  scanConsole  ]: -> Scan:          198 ms
[17:40:50:274][  info  ][  scanConsole  ]: -> Processing:    0 ms
[17:40:50:274][  info  ][  scanConsole  ]: -> Analysis:      800 ms
[17:40:50:274][  info  ][  scanConsole  ]: ###########
[17:40:50:274][  info  ][  scanConsole  ]: # Cleanup #
[17:40:50:274][  info  ][  scanConsole  ]: ###########
[17:40:50:283][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0010055 to configs/CERNQ6/20UPGFC0010055.json
[17:40:50:489][  info  ][  scanConsole  ]: -> Storing output results of FE 4
[17:40:50:489][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0010055, this usually means that the chip did not send any data at all.
[17:40:50:490][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0010103 to configs/CERNQ6/20UPGFC0010103.json
[17:40:50:658][  info  ][  scanConsole  ]: -> Storing output results of FE 5
[17:40:50:658][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0010103, this usually means that the chip did not send any data at all.
[17:40:50:659][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0010119 to configs/CERNQ6/20UPGFC0010119.json
[17:40:50:827][  info  ][  scanConsole  ]: -> Storing output results of FE 6
[17:40:50:827][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0010119, this usually means that the chip did not send any data at all.
[17:40:50:828][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0010087 to configs/CERNQ6/20UPGFC0010087.json
[17:40:50:994][  info  ][  scanConsole  ]: -> Storing output results of FE 7
[17:40:50:994][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0010087, this usually means that the chip did not send any data at all.
[17:40:50:996][  info  ][  scanConsole  ]: Finishing run: 1537
[root@jollyroger Yarr]# 
