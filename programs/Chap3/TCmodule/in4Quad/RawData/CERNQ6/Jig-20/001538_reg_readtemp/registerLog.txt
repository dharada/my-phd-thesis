[17:41:11:808][  info  ][               ]: #####################################
[17:41:11:808][  info  ][               ]: # Welcome to the YARR Scan Console! #
[17:41:11:808][  info  ][               ]: #####################################
[17:41:11:808][  info  ][               ]: -> Parsing command line parameters ...
[17:41:11:808][  info  ][               ]: Configuring logger ...
[17:41:11:817][  info  ][  scanConsole  ]: Scan Type/Config configs/scans/rd53a/reg_readtemp.json
[17:41:11:817][  info  ][  scanConsole  ]: Connectivity:
[17:41:11:817][  info  ][  scanConsole  ]:     configs/CERNQ6/example_rd53a_setup.json
[17:41:11:817][  info  ][  scanConsole  ]: Target ToT: -1
[17:41:11:817][  info  ][  scanConsole  ]: Target Charge: -1
[17:41:11:817][  info  ][  scanConsole  ]: Output Plots: false
[17:41:11:817][  info  ][  scanConsole  ]: Output Directory: ./data/001538_reg_readtemp/
[17:41:11:835][  info  ][  scanConsole  ]: Timestamp: 2022-08-20_17:41:11
[17:41:11:835][  info  ][  scanConsole  ]: Run Number: 1538
[17:41:11:835][  info  ][  scanConsole  ]: #################
[17:41:11:835][  info  ][  scanConsole  ]: # Init Hardware #
[17:41:11:835][  info  ][  scanConsole  ]: #################
[17:41:11:835][  info  ][  scanConsole  ]: -> Opening controller config: configs/controller/specCfg-rd53a-16x1.json
[17:41:11:835][  info  ][  ScanHelper   ]: Loading controller ...
[17:41:11:835][  info  ][  ScanHelper   ]: Found controller of type: spec
[17:41:11:835][  info  ][  ScanHelper   ]: ... loading controler config:
[17:41:11:836][  info  ][  ScanHelper   ]: ~~~ {
[17:41:11:836][  info  ][  ScanHelper   ]: ~~~     "cmdPeriod": 6.250000073038109e-9,
[17:41:11:836][  info  ][  ScanHelper   ]: ~~~     "idle": {
[17:41:11:836][  info  ][  ScanHelper   ]: ~~~         "word": 1768515945
[17:41:11:836][  info  ][  ScanHelper   ]: ~~~     },
[17:41:11:836][  info  ][  ScanHelper   ]: ~~~     "pulse": {
[17:41:11:836][  info  ][  ScanHelper   ]: ~~~         "interval": 500,
[17:41:11:836][  info  ][  ScanHelper   ]: ~~~         "word": 1549575846
[17:41:11:836][  info  ][  ScanHelper   ]: ~~~     },
[17:41:11:836][  info  ][  ScanHelper   ]: ~~~     "rxActiveLanes": 1,
[17:41:11:836][  info  ][  ScanHelper   ]: ~~~     "rxPolarity": 0,
[17:41:11:836][  info  ][  ScanHelper   ]: ~~~     "specNum": 0,
[17:41:11:836][  info  ][  ScanHelper   ]: ~~~     "spiConfig": 541200,
[17:41:11:836][  info  ][  ScanHelper   ]: ~~~     "sync": {
[17:41:11:836][  info  ][  ScanHelper   ]: ~~~         "interval": 32,
[17:41:11:836][  info  ][  ScanHelper   ]: ~~~         "word": 2172551550
[17:41:11:836][  info  ][  ScanHelper   ]: ~~~     },
[17:41:11:836][  info  ][  ScanHelper   ]: ~~~     "txPolarity": 0
[17:41:11:836][  info  ][  ScanHelper   ]: ~~~ }
[17:41:11:836][  info  ][    SpecCom    ]: Opening SPEC with id #0
[17:41:11:836][  info  ][    SpecCom    ]: Mapping BARs ...
[17:41:11:836][  info  ][    SpecCom    ]: ... Mapped BAR0 at 0x7f479a5f4000 with size 1048576
[17:41:11:836][warning ][    SpecCom    ]: ... BAR4 not mapped (Mmap failed)
[17:41:11:837][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[17:41:11:837][  info  ][    SpecCom    ]: Firmware Version: 0x4d9ff6d
[17:41:11:837][  info  ][    SpecCom    ]: Firmware Identifier: 0x4030232
[17:41:11:837][  info  ][    SpecCom    ]: FPGA card: PLDA XpressK7 325
[17:41:11:837][  info  ][    SpecCom    ]: FE Chip Type: RD53A/B
[17:41:11:837][  info  ][    SpecCom    ]: FMC Card Type: Ohio Card (Display Port)
[17:41:11:837][  info  ][    SpecCom    ]: RX Speed: 640Mbps
[17:41:11:837][  info  ][    SpecCom    ]: Channel Configuration: 16x1
[17:41:11:837][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[17:41:11:837][  info  ][    SpecCom    ]: Flushing buffers ...
[17:41:11:837][  info  ][    SpecCom    ]: Init success!
[17:41:11:837][  info  ][  scanConsole  ]: #######################
[17:41:11:837][  info  ][  scanConsole  ]: ##  Loading Configs  ##
[17:41:11:837][  info  ][  scanConsole  ]: #######################
[17:41:11:837][  info  ][  scanConsole  ]: Opening global config: configs/CERNQ6/example_rd53a_setup.json
[17:41:11:837][  info  ][  ScanHelper   ]: Chip type: RD53A
[17:41:11:837][  info  ][  ScanHelper   ]: Chip count 4
[17:41:11:837][  info  ][  ScanHelper   ]: Loading chip #0
[17:41:11:841][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(4)
[17:41:11:841][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ6/20UPGFC0010055.json
[17:41:12:045][  info  ][  ScanHelper   ]: Loading chip #1
[17:41:12:046][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(5)
[17:41:12:046][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ6/20UPGFC0010103.json
[17:41:12:223][  info  ][  ScanHelper   ]: Loading chip #2
[17:41:12:223][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(6)
[17:41:12:223][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ6/20UPGFC0010119.json
[17:41:12:401][  info  ][  ScanHelper   ]: Loading chip #3
[17:41:12:401][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(7)
[17:41:12:401][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ6/20UPGFC0010087.json
[17:41:12:578][  info  ][  scanConsole  ]: #################
[17:41:12:578][  info  ][  scanConsole  ]: # Configure FEs #
[17:41:12:578][  info  ][  scanConsole  ]: #################
[17:41:12:578][  info  ][  scanConsole  ]: Configuring 20UPGFC0010055
[17:41:12:608][  info  ][  scanConsole  ]: Configuring 20UPGFC0010103
[17:41:12:638][  info  ][  scanConsole  ]: Configuring 20UPGFC0010119
[17:41:12:668][  info  ][  scanConsole  ]: Configuring 20UPGFC0010087
[17:41:12:698][  info  ][  scanConsole  ]: Sent configuration to all FEs in 120 ms!
[17:41:12:699][  info  ][  scanConsole  ]: Checking com 20UPGFC0010055
[17:41:12:699][  info  ][    SpecRx     ]: Active Rx channels: 0x10
[17:41:12:699][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[17:41:12:699][  info  ][    SpecRx     ]: Rx Status 0xf0
[17:41:12:699][  info  ][    SpecRx     ]: Number of lanes: 1
[17:41:12:699][  info  ][    SpecRx     ]: Channel 4 Lane 0 synchronized!
[17:41:12:710][  info  ][  scanConsole  ]: ... success!
[17:41:12:710][  info  ][  scanConsole  ]: Checking com 20UPGFC0010103
[17:41:12:710][  info  ][    SpecRx     ]: Active Rx channels: 0x20
[17:41:12:710][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[17:41:12:710][  info  ][    SpecRx     ]: Rx Status 0xf0
[17:41:12:710][  info  ][    SpecRx     ]: Number of lanes: 1
[17:41:12:710][  info  ][    SpecRx     ]: Channel 5 Lane 0 synchronized!
[17:41:12:720][  info  ][  scanConsole  ]: ... success!
[17:41:12:720][  info  ][  scanConsole  ]: Checking com 20UPGFC0010119
[17:41:12:720][  info  ][    SpecRx     ]: Active Rx channels: 0x40
[17:41:12:720][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[17:41:12:720][  info  ][    SpecRx     ]: Rx Status 0xf0
[17:41:12:720][  info  ][    SpecRx     ]: Number of lanes: 1
[17:41:12:720][  info  ][    SpecRx     ]: Channel 6 Lane 0 synchronized!
[17:41:12:730][  info  ][  scanConsole  ]: ... success!
[17:41:12:731][  info  ][  scanConsole  ]: Checking com 20UPGFC0010087
[17:41:12:731][  info  ][    SpecRx     ]: Active Rx channels: 0x80
[17:41:12:731][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[17:41:12:731][  info  ][    SpecRx     ]: Rx Status 0xf0
[17:41:12:731][  info  ][    SpecRx     ]: Number of lanes: 1
[17:41:12:731][  info  ][    SpecRx     ]: Channel 7 Lane 0 synchronized!
[17:41:12:741][  info  ][  scanConsole  ]: ... success!
[17:41:12:741][  info  ][  scanConsole  ]: Enabling Tx channels
[17:41:12:741][  info  ][  scanConsole  ]: Enabling Tx channel 1
[17:41:12:741][  info  ][  scanConsole  ]: Enabling Tx channel 1
[17:41:12:741][  info  ][  scanConsole  ]: Enabling Tx channel 1
[17:41:12:741][  info  ][  scanConsole  ]: Enabling Tx channel 1
[17:41:12:741][  info  ][  scanConsole  ]: Enabling Rx channels
[17:41:12:741][  info  ][  scanConsole  ]: Enabling Rx channel 4
[17:41:12:741][  info  ][  scanConsole  ]: Enabling Rx channel 5
[17:41:12:741][  info  ][  scanConsole  ]: Enabling Rx channel 6
[17:41:12:741][  info  ][  scanConsole  ]: Enabling Rx channel 7
[17:41:12:741][  info  ][  scanConsole  ]: ##############
[17:41:12:741][  info  ][  scanConsole  ]: # Setup Scan #
[17:41:12:741][  info  ][  scanConsole  ]: ##############
[17:41:12:742][  info  ][  scanConsole  ]: Found Scan config, constructing scan ...
[17:41:12:742][  info  ][  ScanFactory  ]: Loading Scan:
[17:41:12:742][  info  ][  ScanFactory  ]:   Name: AnalogScan
[17:41:12:742][  info  ][  ScanFactory  ]:   Number of Loops: 3
[17:41:12:742][  info  ][  ScanFactory  ]:   Loading Loop #0
[17:41:12:742][  info  ][  ScanFactory  ]:    Type: Rd53aReadRegLoop
[17:41:12:742][  info  ][  ScanFactory  ]:    Loading loop config ... 
[17:41:12:743][  info  ][  ScanFactory  ]: ~~~ {
[17:41:12:743][  info  ][  ScanFactory  ]: ~~~     "CurMux": [],
[17:41:12:743][  info  ][  ScanFactory  ]: ~~~     "EnblRingOsc": 0,
[17:41:12:743][  info  ][  ScanFactory  ]: ~~~     "Registers": [],
[17:41:12:743][  info  ][  ScanFactory  ]: ~~~     "RingOscDur": 9,
[17:41:12:743][  info  ][  ScanFactory  ]: ~~~     "RingOscRep": 10,
[17:41:12:743][  info  ][  ScanFactory  ]: ~~~     "VoltMux": [3, 5, 7, 15]
[17:41:12:743][  info  ][  ScanFactory  ]: ~~~ }
[17:41:12:743][  info  ][  ScanFactory  ]:   Loading Loop #1
[17:41:12:743][  info  ][  ScanFactory  ]:    Type: Rd53aTriggerLoop
[17:41:12:743][  info  ][  ScanFactory  ]:    Loading loop config ... 
[17:41:12:743][  info  ][  ScanFactory  ]: ~~~ {
[17:41:12:743][  info  ][  ScanFactory  ]: ~~~     "count": 1,
[17:41:12:743][  info  ][  ScanFactory  ]: ~~~     "delay": 48,
[17:41:12:743][  info  ][  ScanFactory  ]: ~~~     "extTrig": false,
[17:41:12:743][  info  ][  ScanFactory  ]: ~~~     "frequency": 30000.0,
[17:41:12:743][  info  ][  ScanFactory  ]: ~~~     "noInject": false,
[17:41:12:743][  info  ][  ScanFactory  ]: ~~~     "sendEcr": false,
[17:41:12:743][  info  ][  ScanFactory  ]: ~~~     "time": 0.0,
[17:41:12:743][  info  ][  ScanFactory  ]: ~~~     "trigMultiplier": 16
[17:41:12:743][  info  ][  ScanFactory  ]: ~~~ }
[17:41:12:743][  info  ][  ScanFactory  ]:   Loading Loop #2
[17:41:12:743][  info  ][  ScanFactory  ]:    Type: StdDataLoop
[17:41:12:743][warning ][  ScanFactory  ]: ~~~ Config empty.
[17:41:12:743][  info  ][ScanBuildHistogrammers]: Loading histogrammer ...
[17:41:12:744][  info  ][ScanBuildHistogrammers]: ... done!
[17:41:12:744][  info  ][ScanBuildAnalyses]: Loading analyses ...
[17:41:12:744][  info  ][  scanConsole  ]: Running pre scan!
[17:41:12:744][  info  ][  ScanFactory  ]: Entering pre scan phase ...
[17:41:12:744][  info  ][     Rd53a     ]: Write named register: GlobalPulseRt -> 16384
[17:41:12:744][  info  ][     Rd53a     ]: Write named register: InjEnDig -> 0
[17:41:12:744][  info  ][     Rd53a     ]: Write named register: InjVcalDiff -> 0
[17:41:12:744][  info  ][     Rd53a     ]: Write named register: InjVcalHigh -> 3500
[17:41:12:744][  info  ][     Rd53a     ]: Write named register: InjVcalMed -> 500
[17:41:12:744][  info  ][     Rd53a     ]: Write named register: LatencyConfig -> 48
[17:41:12:744][  info  ][  scanConsole  ]: Starting histogrammer and analysis threads:
[17:41:12:744][  info  ][  scanConsole  ]:  .. started threads of Fe 4
[17:41:12:745][  info  ][  scanConsole  ]:  .. started threads of Fe 5
[17:41:12:745][  info  ][  scanConsole  ]:  .. started threads of Fe 6
[17:41:12:745][  info  ][  scanConsole  ]:  .. started threads of Fe 7
[17:41:12:745][  info  ][Rd53aDataProcessor]:   -> Processor thread #0 started!
[17:41:12:745][  info  ][Rd53aDataProcessor]:   -> Processor thread #1 started!
[17:41:12:745][  info  ][Rd53aDataProcessor]:   -> Processor thread #2 started!
[17:41:12:745][  info  ][Rd53aDataProcessor]:   -> Processor thread #3 started!
[17:41:12:745][  info  ][  scanConsole  ]: ########
[17:41:12:745][  info  ][  scanConsole  ]: # Scan #
[17:41:12:745][  info  ][  scanConsole  ]: ########
[17:41:12:745][  info  ][  scanConsole  ]: Starting scan!
[17:41:12:745][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0010055 on Rx 4
[17:41:12:750][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0010055] MON MUX_V: 3 Bias 0 1292 -> 0.282849 V Bias 1 1670 -> 0.358031 V, Temperature 0.0751817 -> -16.4875 C
[17:41:12:754][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0010055] MON MUX_V: 5 Bias 0 1270 -> 0.278474 V Bias 1 1648 -> 0.353655 V, Temperature 0.0751816 -> -29.9844 C
[17:41:12:758][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0010055] MON MUX_V: 7 Bias 0 1257 -> 0.275888 V Bias 1 1635 -> 0.35107 V, Temperature 0.0751817 -> -29.1159 C
[17:41:12:762][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0010055] MON MUX_V: 15 Bias 0 1237 -> 0.27191 V Bias 1 1622 -> 0.348484 V, Temperature 0.0765739 -> -31.3833 C
[17:41:12:797][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0010103 on Rx 5
[17:41:12:801][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0010103] MON MUX_V: 3 Bias 0 1309 -> 0.283809 V Bias 1 1691 -> 0.359949 V, Temperature 0.0761399 -> -10.3 C
[17:41:12:805][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0010103] MON MUX_V: 5 Bias 0 1258 -> 0.273643 V Bias 1 1643 -> 0.350381 V, Temperature 0.0767378 -> -12.7201 C
[17:41:12:810][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0010103] MON MUX_V: 7 Bias 0 1239 -> 0.269856 V Bias 1 1634 -> 0.348587 V, Temperature 0.078731 -> -15.1125 C
[17:41:12:814][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0010103] MON MUX_V: 15 Bias 0 1257 -> 0.273444 V Bias 1 1644 -> 0.350581 V, Temperature 0.0771365 -> -16.2782 C
[17:41:12:849][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0010119 on Rx 6
[17:41:12:853][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0010119] MON MUX_V: 3 Bias 0 1292 -> 0.280131 V Bias 1 1679 -> 0.356544 V, Temperature 0.0764135 -> -13.6846 C
[17:41:12:857][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0010119] MON MUX_V: 5 Bias 0 1279 -> 0.277564 V Bias 1 1663 -> 0.353385 V, Temperature 0.0758211 -> -35.859 C
[17:41:12:862][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0010119] MON MUX_V: 7 Bias 0 1261 -> 0.27401 V Bias 1 1651 -> 0.351016 V, Temperature 0.0770058 -> -29.1158 C
[17:41:12:866][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0010119] MON MUX_V: 15 Bias 0 1252 -> 0.272233 V Bias 1 1639 -> 0.348646 V, Temperature 0.0764135 -> -32.3001 C
[17:41:12:901][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0010087 on Rx 7
[17:41:12:905][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0010087] MON MUX_V: 3 Bias 0 1313 -> 0.283872 V Bias 1 1687 -> 0.358092 V, Temperature 0.0742203 -> -12.1333 C
[17:41:12:909][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0010087] MON MUX_V: 5 Bias 0 1280 -> 0.277323 V Bias 1 1668 -> 0.354322 V, Temperature 0.0769986 -> -21.3 C
[17:41:12:913][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0010087] MON MUX_V: 7 Bias 0 1268 -> 0.274941 V Bias 1 1663 -> 0.353329 V, Temperature 0.0783878 -> -21.3 C
[17:41:12:917][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0010087] MON MUX_V: 15 Bias 0 1269 -> 0.27514 V Bias 1 1659 -> 0.352535 V, Temperature 0.0773955 -> -19.1478 C
[17:41:12:954][  info  ][  scanConsole  ]: Scan done!
[17:41:12:954][  info  ][  scanConsole  ]: Waiting for processors to finish ...
[17:41:12:955][  info  ][  scanConsole  ]: Processor done, waiting for histogrammer ...
[17:41:13:354][  info  ][HistogramAlgorithm]: Histogrammer done!
[17:41:13:354][  info  ][HistogramAlgorithm]: Histogrammer done!
[17:41:13:355][  info  ][HistogramAlgorithm]: Histogrammer done!
[17:41:13:355][  info  ][HistogramAlgorithm]: Histogrammer done!
[17:41:13:355][  info  ][  scanConsole  ]: Processor done, waiting for analysis ...
[17:41:13:755][  info  ][AnalysisAlgorithm]: Analysis done!
[17:41:13:755][  info  ][AnalysisAlgorithm]: Analysis done!
[17:41:13:755][  info  ][AnalysisAlgorithm]: Analysis done!
[17:41:13:755][  info  ][AnalysisAlgorithm]: Analysis done!
[17:41:13:756][  info  ][  scanConsole  ]: All done!
[17:41:13:756][  info  ][  scanConsole  ]: ##########
[17:41:13:756][  info  ][  scanConsole  ]: # Timing #
[17:41:13:756][  info  ][  scanConsole  ]: ##########
[17:41:13:756][  info  ][  scanConsole  ]: -> Configuration: 120 ms
[17:41:13:756][  info  ][  scanConsole  ]: -> Scan:          208 ms
[17:41:13:756][  info  ][  scanConsole  ]: -> Processing:    1 ms
[17:41:13:756][  info  ][  scanConsole  ]: -> Analysis:      800 ms
[17:41:13:756][  info  ][  scanConsole  ]: ###########
[17:41:13:756][  info  ][  scanConsole  ]: # Cleanup #
[17:41:13:756][  info  ][  scanConsole  ]: ###########
[17:41:13:764][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0010055 to configs/CERNQ6/20UPGFC0010055.json
[17:41:13:976][  info  ][  scanConsole  ]: -> Storing output results of FE 4
[17:41:13:976][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0010055, this usually means that the chip did not send any data at all.
[17:41:13:978][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0010103 to configs/CERNQ6/20UPGFC0010103.json
[17:41:14:148][  info  ][  scanConsole  ]: -> Storing output results of FE 5
[17:41:14:149][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0010103, this usually means that the chip did not send any data at all.
[17:41:14:150][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0010119 to configs/CERNQ6/20UPGFC0010119.json
[17:41:14:329][  info  ][  scanConsole  ]: -> Storing output results of FE 6
[17:41:14:329][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0010119, this usually means that the chip did not send any data at all.
[17:41:14:330][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0010087 to configs/CERNQ6/20UPGFC0010087.json
[17:41:14:520][  info  ][  scanConsole  ]: -> Storing output results of FE 7
[17:41:14:520][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0010087, this usually means that the chip did not send any data at all.
[17:41:14:523][  info  ][  scanConsole  ]: Finishing run: 1538
[root@jollyroger Yarr]# 
