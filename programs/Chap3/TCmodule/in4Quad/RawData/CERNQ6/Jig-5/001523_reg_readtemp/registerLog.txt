[16:41:10:108][  info  ][               ]: #####################################
[16:41:10:108][  info  ][               ]: # Welcome to the YARR Scan Console! #
[16:41:10:108][  info  ][               ]: #####################################
[16:41:10:108][  info  ][               ]: -> Parsing command line parameters ...
[16:41:10:108][  info  ][               ]: Configuring logger ...
[16:41:10:117][  info  ][  scanConsole  ]: Scan Type/Config configs/scans/rd53a/reg_readtemp.json
[16:41:10:117][  info  ][  scanConsole  ]: Connectivity:
[16:41:10:117][  info  ][  scanConsole  ]:     configs/CERNQ6/example_rd53a_setup.json
[16:41:10:117][  info  ][  scanConsole  ]: Target ToT: -1
[16:41:10:117][  info  ][  scanConsole  ]: Target Charge: -1
[16:41:10:117][  info  ][  scanConsole  ]: Output Plots: false
[16:41:10:117][  info  ][  scanConsole  ]: Output Directory: ./data/001523_reg_readtemp/
[16:41:10:136][  info  ][  scanConsole  ]: Timestamp: 2022-08-20_16:41:10
[16:41:10:136][  info  ][  scanConsole  ]: Run Number: 1523
[16:41:10:136][  info  ][  scanConsole  ]: #################
[16:41:10:136][  info  ][  scanConsole  ]: # Init Hardware #
[16:41:10:136][  info  ][  scanConsole  ]: #################
[16:41:10:136][  info  ][  scanConsole  ]: -> Opening controller config: configs/controller/specCfg-rd53a-16x1.json
[16:41:10:136][  info  ][  ScanHelper   ]: Loading controller ...
[16:41:10:136][  info  ][  ScanHelper   ]: Found controller of type: spec
[16:41:10:136][  info  ][  ScanHelper   ]: ... loading controler config:
[16:41:10:137][  info  ][  ScanHelper   ]: ~~~ {
[16:41:10:137][  info  ][  ScanHelper   ]: ~~~     "cmdPeriod": 6.250000073038109e-9,
[16:41:10:137][  info  ][  ScanHelper   ]: ~~~     "idle": {
[16:41:10:137][  info  ][  ScanHelper   ]: ~~~         "word": 1768515945
[16:41:10:137][  info  ][  ScanHelper   ]: ~~~     },
[16:41:10:137][  info  ][  ScanHelper   ]: ~~~     "pulse": {
[16:41:10:137][  info  ][  ScanHelper   ]: ~~~         "interval": 500,
[16:41:10:137][  info  ][  ScanHelper   ]: ~~~         "word": 1549575846
[16:41:10:137][  info  ][  ScanHelper   ]: ~~~     },
[16:41:10:137][  info  ][  ScanHelper   ]: ~~~     "rxActiveLanes": 1,
[16:41:10:137][  info  ][  ScanHelper   ]: ~~~     "rxPolarity": 0,
[16:41:10:137][  info  ][  ScanHelper   ]: ~~~     "specNum": 0,
[16:41:10:137][  info  ][  ScanHelper   ]: ~~~     "spiConfig": 541200,
[16:41:10:137][  info  ][  ScanHelper   ]: ~~~     "sync": {
[16:41:10:137][  info  ][  ScanHelper   ]: ~~~         "interval": 32,
[16:41:10:137][  info  ][  ScanHelper   ]: ~~~         "word": 2172551550
[16:41:10:137][  info  ][  ScanHelper   ]: ~~~     },
[16:41:10:137][  info  ][  ScanHelper   ]: ~~~     "txPolarity": 0
[16:41:10:137][  info  ][  ScanHelper   ]: ~~~ }
[16:41:10:137][  info  ][    SpecCom    ]: Opening SPEC with id #0
[16:41:10:138][  info  ][    SpecCom    ]: Mapping BARs ...
[16:41:10:138][  info  ][    SpecCom    ]: ... Mapped BAR0 at 0x7f4c9988e000 with size 1048576
[16:41:10:138][warning ][    SpecCom    ]: ... BAR4 not mapped (Mmap failed)
[16:41:10:138][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[16:41:10:138][  info  ][    SpecCom    ]: Firmware Version: 0x4d9ff6d
[16:41:10:138][  info  ][    SpecCom    ]: Firmware Identifier: 0x4030232
[16:41:10:139][  info  ][    SpecCom    ]: FPGA card: PLDA XpressK7 325
[16:41:10:139][  info  ][    SpecCom    ]: FE Chip Type: RD53A/B
[16:41:10:139][  info  ][    SpecCom    ]: FMC Card Type: Ohio Card (Display Port)
[16:41:10:139][  info  ][    SpecCom    ]: RX Speed: 640Mbps
[16:41:10:139][  info  ][    SpecCom    ]: Channel Configuration: 16x1
[16:41:10:139][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[16:41:10:139][  info  ][    SpecCom    ]: Flushing buffers ...
[16:41:10:139][  info  ][    SpecCom    ]: Init success!
[16:41:10:139][  info  ][  scanConsole  ]: #######################
[16:41:10:139][  info  ][  scanConsole  ]: ##  Loading Configs  ##
[16:41:10:139][  info  ][  scanConsole  ]: #######################
[16:41:10:139][  info  ][  scanConsole  ]: Opening global config: configs/CERNQ6/example_rd53a_setup.json
[16:41:10:139][  info  ][  ScanHelper   ]: Chip type: RD53A
[16:41:10:139][  info  ][  ScanHelper   ]: Chip count 4
[16:41:10:139][  info  ][  ScanHelper   ]: Loading chip #0
[16:41:10:142][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(4)
[16:41:10:143][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ6/20UPGFC0010055.json
[16:41:10:347][  info  ][  ScanHelper   ]: Loading chip #1
[16:41:10:347][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(5)
[16:41:10:347][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ6/20UPGFC0010103.json
[16:41:10:520][  info  ][  ScanHelper   ]: Loading chip #2
[16:41:10:520][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(6)
[16:41:10:520][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ6/20UPGFC0010119.json
[16:41:10:696][  info  ][  ScanHelper   ]: Loading chip #3
[16:41:10:697][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(7)
[16:41:10:697][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ6/20UPGFC0010087.json
[16:41:10:874][  info  ][  scanConsole  ]: #################
[16:41:10:874][  info  ][  scanConsole  ]: # Configure FEs #
[16:41:10:874][  info  ][  scanConsole  ]: #################
[16:41:10:874][  info  ][  scanConsole  ]: Configuring 20UPGFC0010055
[16:41:10:905][  info  ][  scanConsole  ]: Configuring 20UPGFC0010103
[16:41:10:941][  info  ][  scanConsole  ]: Configuring 20UPGFC0010119
[16:41:10:977][  info  ][  scanConsole  ]: Configuring 20UPGFC0010087
[16:41:11:014][  info  ][  scanConsole  ]: Sent configuration to all FEs in 139 ms!
[16:41:11:015][  info  ][  scanConsole  ]: Checking com 20UPGFC0010055
[16:41:11:015][  info  ][    SpecRx     ]: Active Rx channels: 0x10
[16:41:11:015][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[16:41:11:015][  info  ][    SpecRx     ]: Rx Status 0xf0
[16:41:11:015][  info  ][    SpecRx     ]: Number of lanes: 1
[16:41:11:015][  info  ][    SpecRx     ]: Channel 4 Lane 0 synchronized!
[16:41:11:026][  info  ][  scanConsole  ]: ... success!
[16:41:11:026][  info  ][  scanConsole  ]: Checking com 20UPGFC0010103
[16:41:11:026][  info  ][    SpecRx     ]: Active Rx channels: 0x20
[16:41:11:026][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[16:41:11:026][  info  ][    SpecRx     ]: Rx Status 0xf0
[16:41:11:026][  info  ][    SpecRx     ]: Number of lanes: 1
[16:41:11:026][  info  ][    SpecRx     ]: Channel 5 Lane 0 synchronized!
[16:41:11:036][  info  ][  scanConsole  ]: ... success!
[16:41:11:036][  info  ][  scanConsole  ]: Checking com 20UPGFC0010119
[16:41:11:036][  info  ][    SpecRx     ]: Active Rx channels: 0x40
[16:41:11:036][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[16:41:11:036][  info  ][    SpecRx     ]: Rx Status 0xf0
[16:41:11:036][  info  ][    SpecRx     ]: Number of lanes: 1
[16:41:11:036][  info  ][    SpecRx     ]: Channel 6 Lane 0 synchronized!
[16:41:11:047][  info  ][  scanConsole  ]: ... success!
[16:41:11:047][  info  ][  scanConsole  ]: Checking com 20UPGFC0010087
[16:41:11:047][  info  ][    SpecRx     ]: Active Rx channels: 0x80
[16:41:11:047][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[16:41:11:047][  info  ][    SpecRx     ]: Rx Status 0xf0
[16:41:11:047][  info  ][    SpecRx     ]: Number of lanes: 1
[16:41:11:047][  info  ][    SpecRx     ]: Channel 7 Lane 0 synchronized!
[16:41:11:057][  info  ][  scanConsole  ]: ... success!
[16:41:11:057][  info  ][  scanConsole  ]: Enabling Tx channels
[16:41:11:057][  info  ][  scanConsole  ]: Enabling Tx channel 1
[16:41:11:057][  info  ][  scanConsole  ]: Enabling Tx channel 1
[16:41:11:057][  info  ][  scanConsole  ]: Enabling Tx channel 1
[16:41:11:057][  info  ][  scanConsole  ]: Enabling Tx channel 1
[16:41:11:057][  info  ][  scanConsole  ]: Enabling Rx channels
[16:41:11:058][  info  ][  scanConsole  ]: Enabling Rx channel 4
[16:41:11:058][  info  ][  scanConsole  ]: Enabling Rx channel 5
[16:41:11:058][  info  ][  scanConsole  ]: Enabling Rx channel 6
[16:41:11:058][  info  ][  scanConsole  ]: Enabling Rx channel 7
[16:41:11:058][  info  ][  scanConsole  ]: ##############
[16:41:11:058][  info  ][  scanConsole  ]: # Setup Scan #
[16:41:11:058][  info  ][  scanConsole  ]: ##############
[16:41:11:058][  info  ][  scanConsole  ]: Found Scan config, constructing scan ...
[16:41:11:058][  info  ][  ScanFactory  ]: Loading Scan:
[16:41:11:058][  info  ][  ScanFactory  ]:   Name: AnalogScan
[16:41:11:058][  info  ][  ScanFactory  ]:   Number of Loops: 3
[16:41:11:058][  info  ][  ScanFactory  ]:   Loading Loop #0
[16:41:11:058][  info  ][  ScanFactory  ]:    Type: Rd53aReadRegLoop
[16:41:11:059][  info  ][  ScanFactory  ]:    Loading loop config ... 
[16:41:11:059][  info  ][  ScanFactory  ]: ~~~ {
[16:41:11:059][  info  ][  ScanFactory  ]: ~~~     "CurMux": [],
[16:41:11:059][  info  ][  ScanFactory  ]: ~~~     "EnblRingOsc": 0,
[16:41:11:059][  info  ][  ScanFactory  ]: ~~~     "Registers": [],
[16:41:11:059][  info  ][  ScanFactory  ]: ~~~     "RingOscDur": 9,
[16:41:11:059][  info  ][  ScanFactory  ]: ~~~     "RingOscRep": 10,
[16:41:11:059][  info  ][  ScanFactory  ]: ~~~     "VoltMux": [3, 5, 7, 15]
[16:41:11:059][  info  ][  ScanFactory  ]: ~~~ }
[16:41:11:059][  info  ][  ScanFactory  ]:   Loading Loop #1
[16:41:11:059][  info  ][  ScanFactory  ]:    Type: Rd53aTriggerLoop
[16:41:11:059][  info  ][  ScanFactory  ]:    Loading loop config ... 
[16:41:11:059][  info  ][  ScanFactory  ]: ~~~ {
[16:41:11:059][  info  ][  ScanFactory  ]: ~~~     "count": 1,
[16:41:11:059][  info  ][  ScanFactory  ]: ~~~     "delay": 48,
[16:41:11:059][  info  ][  ScanFactory  ]: ~~~     "extTrig": false,
[16:41:11:059][  info  ][  ScanFactory  ]: ~~~     "frequency": 30000.0,
[16:41:11:059][  info  ][  ScanFactory  ]: ~~~     "noInject": false,
[16:41:11:059][  info  ][  ScanFactory  ]: ~~~     "sendEcr": false,
[16:41:11:059][  info  ][  ScanFactory  ]: ~~~     "time": 0.0,
[16:41:11:059][  info  ][  ScanFactory  ]: ~~~     "trigMultiplier": 16
[16:41:11:059][  info  ][  ScanFactory  ]: ~~~ }
[16:41:11:059][  info  ][  ScanFactory  ]:   Loading Loop #2
[16:41:11:059][  info  ][  ScanFactory  ]:    Type: StdDataLoop
[16:41:11:059][warning ][  ScanFactory  ]: ~~~ Config empty.
[16:41:11:059][  info  ][ScanBuildHistogrammers]: Loading histogrammer ...
[16:41:11:060][  info  ][ScanBuildHistogrammers]: ... done!
[16:41:11:060][  info  ][ScanBuildAnalyses]: Loading analyses ...
[16:41:11:060][  info  ][  scanConsole  ]: Running pre scan!
[16:41:11:060][  info  ][  ScanFactory  ]: Entering pre scan phase ...
[16:41:11:060][  info  ][     Rd53a     ]: Write named register: GlobalPulseRt -> 16384
[16:41:11:060][  info  ][     Rd53a     ]: Write named register: InjEnDig -> 0
[16:41:11:060][  info  ][     Rd53a     ]: Write named register: InjVcalDiff -> 0
[16:41:11:060][  info  ][     Rd53a     ]: Write named register: InjVcalHigh -> 3500
[16:41:11:060][  info  ][     Rd53a     ]: Write named register: InjVcalMed -> 500
[16:41:11:060][  info  ][     Rd53a     ]: Write named register: LatencyConfig -> 48
[16:41:11:060][  info  ][  scanConsole  ]: Starting histogrammer and analysis threads:
[16:41:11:061][  info  ][  scanConsole  ]:  .. started threads of Fe 4
[16:41:11:061][  info  ][  scanConsole  ]:  .. started threads of Fe 5
[16:41:11:061][  info  ][  scanConsole  ]:  .. started threads of Fe 6
[16:41:11:061][  info  ][  scanConsole  ]:  .. started threads of Fe 7
[16:41:11:061][  info  ][Rd53aDataProcessor]:   -> Processor thread #0 started!
[16:41:11:062][  info  ][Rd53aDataProcessor]:   -> Processor thread #1 started!
[16:41:11:062][  info  ][Rd53aDataProcessor]:   -> Processor thread #2 started!
[16:41:11:062][  info  ][Rd53aDataProcessor]:   -> Processor thread #3 started!
[16:41:11:062][  info  ][  scanConsole  ]: ########
[16:41:11:062][  info  ][  scanConsole  ]: # Scan #
[16:41:11:062][  info  ][  scanConsole  ]: ########
[16:41:11:062][  info  ][  scanConsole  ]: Starting scan!
[16:41:11:062][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0010055 on Rx 4
[16:41:11:066][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0010055] MON MUX_V: 3 Bias 0 1212 -> 0.266938 V Bias 1 1611 -> 0.346296 V, Temperature 0.0793584 -> -2.05002 C
[16:41:11:070][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0010055] MON MUX_V: 5 Bias 0 1190 -> 0.262562 V Bias 1 1589 -> 0.341921 V, Temperature 0.0793584 -> -11.7474 C
[16:41:11:074][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0010055] MON MUX_V: 7 Bias 0 1176 -> 0.259778 V Bias 1 1575 -> 0.339136 V, Temperature 0.0793584 -> -10.879 C
[16:41:11:078][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0010055] MON MUX_V: 15 Bias 0 1157 -> 0.255999 V Bias 1 1563 -> 0.336749 V, Temperature 0.0807507 -> -12.1333 C
[16:41:11:113][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0010103 on Rx 5
[16:41:11:117][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0010103] MON MUX_V: 3 Bias 0 1226 -> 0.267265 V Bias 1 1628 -> 0.347391 V, Temperature 0.0801263 -> 1.92226 C
[16:41:11:121][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0010103] MON MUX_V: 5 Bias 0 1175 -> 0.2571 V Bias 1 1579 -> 0.337625 V, Temperature 0.0805249 -> -0.180084 C
[16:41:11:125][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0010103] MON MUX_V: 7 Bias 0 1155 -> 0.253114 V Bias 1 1572 -> 0.33623 V, Temperature 0.0831161 -> 0.0125732 C
[16:41:11:129][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0010103] MON MUX_V: 15 Bias 0 1172 -> 0.256502 V Bias 1 1581 -> 0.338023 V, Temperature 0.0815215 -> -0.495758 C
[16:41:11:164][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0010119 on Rx 6
[16:41:11:168][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0010119] MON MUX_V: 3 Bias 0 1211 -> 0.264137 V Bias 1 1620 -> 0.344895 V, Temperature 0.0807574 -> 0.276978 C
[16:41:11:172][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0010119] MON MUX_V: 5 Bias 0 1194 -> 0.260781 V Bias 1 1600 -> 0.340946 V, Temperature 0.080165 -> -14.506 C
[16:41:11:176][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0010119] MON MUX_V: 7 Bias 0 1175 -> 0.257029 V Bias 1 1588 -> 0.338576 V, Temperature 0.0815472 -> -9.14209 C
[16:41:11:181][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0010119] MON MUX_V: 15 Bias 0 1167 -> 0.255449 V Bias 1 1576 -> 0.336207 V, Temperature 0.0807574 -> -12.1333 C
[16:41:11:215][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0010087 on Rx 7
[16:41:11:220][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0010087] MON MUX_V: 3 Bias 0 1231 -> 0.267599 V Bias 1 1627 -> 0.346185 V, Temperature 0.0785863 -> 1.31116 C
[16:41:11:224][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0010087] MON MUX_V: 5 Bias 0 1198 -> 0.26105 V Bias 1 1607 -> 0.342216 V, Temperature 0.0811661 -> -4.79987 C
[16:41:11:228][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0010087] MON MUX_V: 7 Bias 0 1186 -> 0.258669 V Bias 1 1599 -> 0.340628 V, Temperature 0.0819599 -> -7.15707 C
[16:41:11:232][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0010087] MON MUX_V: 15 Bias 0 1187 -> 0.258867 V Bias 1 1598 -> 0.34043 V, Temperature 0.081563 -> -4.08255 C
[16:41:11:268][  info  ][  scanConsole  ]: Scan done!
[16:41:11:268][  info  ][  scanConsole  ]: Waiting for processors to finish ...
[16:41:11:269][  info  ][  scanConsole  ]: Processor done, waiting for histogrammer ...
[16:41:11:669][  info  ][HistogramAlgorithm]: Histogrammer done!
[16:41:11:669][  info  ][HistogramAlgorithm]: Histogrammer done!
[16:41:11:669][  info  ][HistogramAlgorithm]: Histogrammer done!
[16:41:11:669][  info  ][HistogramAlgorithm]: Histogrammer done!
[16:41:11:669][  info  ][  scanConsole  ]: Processor done, waiting for analysis ...
[16:41:12:069][  info  ][AnalysisAlgorithm]: Analysis done!
[16:41:12:069][  info  ][AnalysisAlgorithm]: Analysis done!
[16:41:12:069][  info  ][AnalysisAlgorithm]: Analysis done!
[16:41:12:069][  info  ][AnalysisAlgorithm]: Analysis done!
[16:41:12:070][  info  ][  scanConsole  ]: All done!
[16:41:12:070][  info  ][  scanConsole  ]: ##########
[16:41:12:070][  info  ][  scanConsole  ]: # Timing #
[16:41:12:070][  info  ][  scanConsole  ]: ##########
[16:41:12:070][  info  ][  scanConsole  ]: -> Configuration: 139 ms
[16:41:12:070][  info  ][  scanConsole  ]: -> Scan:          206 ms
[16:41:12:070][  info  ][  scanConsole  ]: -> Processing:    0 ms
[16:41:12:070][  info  ][  scanConsole  ]: -> Analysis:      800 ms
[16:41:12:070][  info  ][  scanConsole  ]: ###########
[16:41:12:070][  info  ][  scanConsole  ]: # Cleanup #
[16:41:12:070][  info  ][  scanConsole  ]: ###########
[16:41:12:078][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0010055 to configs/CERNQ6/20UPGFC0010055.json
[16:41:12:263][  info  ][  scanConsole  ]: -> Storing output results of FE 4
[16:41:12:263][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0010055, this usually means that the chip did not send any data at all.
[16:41:12:264][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0010103 to configs/CERNQ6/20UPGFC0010103.json
[16:41:12:427][  info  ][  scanConsole  ]: -> Storing output results of FE 5
[16:41:12:427][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0010103, this usually means that the chip did not send any data at all.
[16:41:12:428][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0010119 to configs/CERNQ6/20UPGFC0010119.json
[16:41:12:596][  info  ][  scanConsole  ]: -> Storing output results of FE 6
[16:41:12:596][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0010119, this usually means that the chip did not send any data at all.
[16:41:12:597][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0010087 to configs/CERNQ6/20UPGFC0010087.json
[16:41:12:764][  info  ][  scanConsole  ]: -> Storing output results of FE 7
[16:41:12:764][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0010087, this usually means that the chip did not send any data at all.
[16:41:12:766][  info  ][  scanConsole  ]: Finishing run: 1523
[root@jollyroger Yarr]# 
