[16:41:25:073][  info  ][               ]: #####################################
[16:41:25:073][  info  ][               ]: # Welcome to the YARR Scan Console! #
[16:41:25:073][  info  ][               ]: #####################################
[16:41:25:073][  info  ][               ]: -> Parsing command line parameters ...
[16:41:25:074][  info  ][               ]: Configuring logger ...
[16:41:25:082][  info  ][  scanConsole  ]: Scan Type/Config configs/scans/rd53a/reg_readtemp.json
[16:41:25:082][  info  ][  scanConsole  ]: Connectivity:
[16:41:25:082][  info  ][  scanConsole  ]:     configs/CERNQ6/example_rd53a_setup.json
[16:41:25:082][  info  ][  scanConsole  ]: Target ToT: -1
[16:41:25:083][  info  ][  scanConsole  ]: Target Charge: -1
[16:41:25:083][  info  ][  scanConsole  ]: Output Plots: false
[16:41:25:083][  info  ][  scanConsole  ]: Output Directory: ./data/001524_reg_readtemp/
[16:41:25:101][  info  ][  scanConsole  ]: Timestamp: 2022-08-20_16:41:25
[16:41:25:101][  info  ][  scanConsole  ]: Run Number: 1524
[16:41:25:101][  info  ][  scanConsole  ]: #################
[16:41:25:101][  info  ][  scanConsole  ]: # Init Hardware #
[16:41:25:101][  info  ][  scanConsole  ]: #################
[16:41:25:101][  info  ][  scanConsole  ]: -> Opening controller config: configs/controller/specCfg-rd53a-16x1.json
[16:41:25:102][  info  ][  ScanHelper   ]: Loading controller ...
[16:41:25:102][  info  ][  ScanHelper   ]: Found controller of type: spec
[16:41:25:102][  info  ][  ScanHelper   ]: ... loading controler config:
[16:41:25:102][  info  ][  ScanHelper   ]: ~~~ {
[16:41:25:102][  info  ][  ScanHelper   ]: ~~~     "cmdPeriod": 6.250000073038109e-9,
[16:41:25:102][  info  ][  ScanHelper   ]: ~~~     "idle": {
[16:41:25:102][  info  ][  ScanHelper   ]: ~~~         "word": 1768515945
[16:41:25:102][  info  ][  ScanHelper   ]: ~~~     },
[16:41:25:102][  info  ][  ScanHelper   ]: ~~~     "pulse": {
[16:41:25:102][  info  ][  ScanHelper   ]: ~~~         "interval": 500,
[16:41:25:102][  info  ][  ScanHelper   ]: ~~~         "word": 1549575846
[16:41:25:102][  info  ][  ScanHelper   ]: ~~~     },
[16:41:25:102][  info  ][  ScanHelper   ]: ~~~     "rxActiveLanes": 1,
[16:41:25:102][  info  ][  ScanHelper   ]: ~~~     "rxPolarity": 0,
[16:41:25:102][  info  ][  ScanHelper   ]: ~~~     "specNum": 0,
[16:41:25:102][  info  ][  ScanHelper   ]: ~~~     "spiConfig": 541200,
[16:41:25:102][  info  ][  ScanHelper   ]: ~~~     "sync": {
[16:41:25:102][  info  ][  ScanHelper   ]: ~~~         "interval": 32,
[16:41:25:102][  info  ][  ScanHelper   ]: ~~~         "word": 2172551550
[16:41:25:102][  info  ][  ScanHelper   ]: ~~~     },
[16:41:25:102][  info  ][  ScanHelper   ]: ~~~     "txPolarity": 0
[16:41:25:102][  info  ][  ScanHelper   ]: ~~~ }
[16:41:25:102][  info  ][    SpecCom    ]: Opening SPEC with id #0
[16:41:25:103][  info  ][    SpecCom    ]: Mapping BARs ...
[16:41:25:103][  info  ][    SpecCom    ]: ... Mapped BAR0 at 0x7fc2ebd98000 with size 1048576
[16:41:25:103][warning ][    SpecCom    ]: ... BAR4 not mapped (Mmap failed)
[16:41:25:103][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[16:41:25:103][  info  ][    SpecCom    ]: Firmware Version: 0x4d9ff6d
[16:41:25:103][  info  ][    SpecCom    ]: Firmware Identifier: 0x4030232
[16:41:25:103][  info  ][    SpecCom    ]: FPGA card: PLDA XpressK7 325
[16:41:25:103][  info  ][    SpecCom    ]: FE Chip Type: RD53A/B
[16:41:25:103][  info  ][    SpecCom    ]: FMC Card Type: Ohio Card (Display Port)
[16:41:25:103][  info  ][    SpecCom    ]: RX Speed: 640Mbps
[16:41:25:103][  info  ][    SpecCom    ]: Channel Configuration: 16x1
[16:41:25:103][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[16:41:25:103][  info  ][    SpecCom    ]: Flushing buffers ...
[16:41:25:103][  info  ][    SpecCom    ]: Init success!
[16:41:25:103][  info  ][  scanConsole  ]: #######################
[16:41:25:103][  info  ][  scanConsole  ]: ##  Loading Configs  ##
[16:41:25:103][  info  ][  scanConsole  ]: #######################
[16:41:25:103][  info  ][  scanConsole  ]: Opening global config: configs/CERNQ6/example_rd53a_setup.json
[16:41:25:104][  info  ][  ScanHelper   ]: Chip type: RD53A
[16:41:25:104][  info  ][  ScanHelper   ]: Chip count 4
[16:41:25:104][  info  ][  ScanHelper   ]: Loading chip #0
[16:41:25:107][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(4)
[16:41:25:107][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ6/20UPGFC0010055.json
[16:41:25:309][  info  ][  ScanHelper   ]: Loading chip #1
[16:41:25:309][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(5)
[16:41:25:309][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ6/20UPGFC0010103.json
[16:41:25:482][  info  ][  ScanHelper   ]: Loading chip #2
[16:41:25:483][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(6)
[16:41:25:483][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ6/20UPGFC0010119.json
[16:41:25:658][  info  ][  ScanHelper   ]: Loading chip #3
[16:41:25:659][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(7)
[16:41:25:659][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ6/20UPGFC0010087.json
[16:41:25:833][  info  ][  scanConsole  ]: #################
[16:41:25:833][  info  ][  scanConsole  ]: # Configure FEs #
[16:41:25:833][  info  ][  scanConsole  ]: #################
[16:41:25:833][  info  ][  scanConsole  ]: Configuring 20UPGFC0010055
[16:41:25:863][  info  ][  scanConsole  ]: Configuring 20UPGFC0010103
[16:41:25:911][  info  ][  scanConsole  ]: Configuring 20UPGFC0010119
[16:41:25:959][  info  ][  scanConsole  ]: Configuring 20UPGFC0010087
[16:41:26:003][  info  ][  scanConsole  ]: Sent configuration to all FEs in 170 ms!
[16:41:26:004][  info  ][  scanConsole  ]: Checking com 20UPGFC0010055
[16:41:26:004][  info  ][    SpecRx     ]: Active Rx channels: 0x10
[16:41:26:004][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[16:41:26:004][  info  ][    SpecRx     ]: Rx Status 0xf0
[16:41:26:004][  info  ][    SpecRx     ]: Number of lanes: 1
[16:41:26:004][  info  ][    SpecRx     ]: Channel 4 Lane 0 synchronized!
[16:41:26:015][  info  ][  scanConsole  ]: ... success!
[16:41:26:015][  info  ][  scanConsole  ]: Checking com 20UPGFC0010103
[16:41:26:015][  info  ][    SpecRx     ]: Active Rx channels: 0x20
[16:41:26:015][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[16:41:26:015][  info  ][    SpecRx     ]: Rx Status 0xf0
[16:41:26:015][  info  ][    SpecRx     ]: Number of lanes: 1
[16:41:26:015][  info  ][    SpecRx     ]: Channel 5 Lane 0 synchronized!
[16:41:26:025][  info  ][  scanConsole  ]: ... success!
[16:41:26:025][  info  ][  scanConsole  ]: Checking com 20UPGFC0010119
[16:41:26:025][  info  ][    SpecRx     ]: Active Rx channels: 0x40
[16:41:26:025][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[16:41:26:025][  info  ][    SpecRx     ]: Rx Status 0xf0
[16:41:26:025][  info  ][    SpecRx     ]: Number of lanes: 1
[16:41:26:025][  info  ][    SpecRx     ]: Channel 6 Lane 0 synchronized!
[16:41:26:036][  info  ][  scanConsole  ]: ... success!
[16:41:26:036][  info  ][  scanConsole  ]: Checking com 20UPGFC0010087
[16:41:26:036][  info  ][    SpecRx     ]: Active Rx channels: 0x80
[16:41:26:036][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[16:41:26:036][  info  ][    SpecRx     ]: Rx Status 0xf0
[16:41:26:036][  info  ][    SpecRx     ]: Number of lanes: 1
[16:41:26:036][  info  ][    SpecRx     ]: Channel 7 Lane 0 synchronized!
[16:41:26:046][  info  ][  scanConsole  ]: ... success!
[16:41:26:046][  info  ][  scanConsole  ]: Enabling Tx channels
[16:41:26:046][  info  ][  scanConsole  ]: Enabling Tx channel 1
[16:41:26:046][  info  ][  scanConsole  ]: Enabling Tx channel 1
[16:41:26:046][  info  ][  scanConsole  ]: Enabling Tx channel 1
[16:41:26:046][  info  ][  scanConsole  ]: Enabling Tx channel 1
[16:41:26:046][  info  ][  scanConsole  ]: Enabling Rx channels
[16:41:26:046][  info  ][  scanConsole  ]: Enabling Rx channel 4
[16:41:26:046][  info  ][  scanConsole  ]: Enabling Rx channel 5
[16:41:26:047][  info  ][  scanConsole  ]: Enabling Rx channel 6
[16:41:26:047][  info  ][  scanConsole  ]: Enabling Rx channel 7
[16:41:26:047][  info  ][  scanConsole  ]: ##############
[16:41:26:047][  info  ][  scanConsole  ]: # Setup Scan #
[16:41:26:047][  info  ][  scanConsole  ]: ##############
[16:41:26:047][  info  ][  scanConsole  ]: Found Scan config, constructing scan ...
[16:41:26:047][  info  ][  ScanFactory  ]: Loading Scan:
[16:41:26:047][  info  ][  ScanFactory  ]:   Name: AnalogScan
[16:41:26:047][  info  ][  ScanFactory  ]:   Number of Loops: 3
[16:41:26:048][  info  ][  ScanFactory  ]:   Loading Loop #0
[16:41:26:048][  info  ][  ScanFactory  ]:    Type: Rd53aReadRegLoop
[16:41:26:048][  info  ][  ScanFactory  ]:    Loading loop config ... 
[16:41:26:048][  info  ][  ScanFactory  ]: ~~~ {
[16:41:26:048][  info  ][  ScanFactory  ]: ~~~     "CurMux": [],
[16:41:26:048][  info  ][  ScanFactory  ]: ~~~     "EnblRingOsc": 0,
[16:41:26:048][  info  ][  ScanFactory  ]: ~~~     "Registers": [],
[16:41:26:048][  info  ][  ScanFactory  ]: ~~~     "RingOscDur": 9,
[16:41:26:048][  info  ][  ScanFactory  ]: ~~~     "RingOscRep": 10,
[16:41:26:048][  info  ][  ScanFactory  ]: ~~~     "VoltMux": [3, 5, 7, 15]
[16:41:26:048][  info  ][  ScanFactory  ]: ~~~ }
[16:41:26:048][  info  ][  ScanFactory  ]:   Loading Loop #1
[16:41:26:048][  info  ][  ScanFactory  ]:    Type: Rd53aTriggerLoop
[16:41:26:048][  info  ][  ScanFactory  ]:    Loading loop config ... 
[16:41:26:048][  info  ][  ScanFactory  ]: ~~~ {
[16:41:26:048][  info  ][  ScanFactory  ]: ~~~     "count": 1,
[16:41:26:048][  info  ][  ScanFactory  ]: ~~~     "delay": 48,
[16:41:26:048][  info  ][  ScanFactory  ]: ~~~     "extTrig": false,
[16:41:26:048][  info  ][  ScanFactory  ]: ~~~     "frequency": 30000.0,
[16:41:26:048][  info  ][  ScanFactory  ]: ~~~     "noInject": false,
[16:41:26:048][  info  ][  ScanFactory  ]: ~~~     "sendEcr": false,
[16:41:26:048][  info  ][  ScanFactory  ]: ~~~     "time": 0.0,
[16:41:26:048][  info  ][  ScanFactory  ]: ~~~     "trigMultiplier": 16
[16:41:26:048][  info  ][  ScanFactory  ]: ~~~ }
[16:41:26:048][  info  ][  ScanFactory  ]:   Loading Loop #2
[16:41:26:048][  info  ][  ScanFactory  ]:    Type: StdDataLoop
[16:41:26:048][warning ][  ScanFactory  ]: ~~~ Config empty.
[16:41:26:048][  info  ][ScanBuildHistogrammers]: Loading histogrammer ...
[16:41:26:049][  info  ][ScanBuildHistogrammers]: ... done!
[16:41:26:049][  info  ][ScanBuildAnalyses]: Loading analyses ...
[16:41:26:049][  info  ][  scanConsole  ]: Running pre scan!
[16:41:26:049][  info  ][  ScanFactory  ]: Entering pre scan phase ...
[16:41:26:049][  info  ][     Rd53a     ]: Write named register: GlobalPulseRt -> 16384
[16:41:26:049][  info  ][     Rd53a     ]: Write named register: InjEnDig -> 0
[16:41:26:049][  info  ][     Rd53a     ]: Write named register: InjVcalDiff -> 0
[16:41:26:049][  info  ][     Rd53a     ]: Write named register: InjVcalHigh -> 3500
[16:41:26:049][  info  ][     Rd53a     ]: Write named register: InjVcalMed -> 500
[16:41:26:049][  info  ][     Rd53a     ]: Write named register: LatencyConfig -> 48
[16:41:26:049][  info  ][  scanConsole  ]: Starting histogrammer and analysis threads:
[16:41:26:049][  info  ][  scanConsole  ]:  .. started threads of Fe 4
[16:41:26:050][  info  ][  scanConsole  ]:  .. started threads of Fe 5
[16:41:26:050][  info  ][  scanConsole  ]:  .. started threads of Fe 6
[16:41:26:050][  info  ][  scanConsole  ]:  .. started threads of Fe 7
[16:41:26:050][  info  ][Rd53aDataProcessor]:   -> Processor thread #0 started!
[16:41:26:050][  info  ][Rd53aDataProcessor]:   -> Processor thread #1 started!
[16:41:26:050][  info  ][Rd53aDataProcessor]:   -> Processor thread #2 started!
[16:41:26:050][  info  ][Rd53aDataProcessor]:   -> Processor thread #3 started!
[16:41:26:050][  info  ][  scanConsole  ]: ########
[16:41:26:050][  info  ][  scanConsole  ]: # Scan #
[16:41:26:050][  info  ][  scanConsole  ]: ########
[16:41:26:050][  info  ][  scanConsole  ]: Starting scan!
[16:41:26:050][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0010055 on Rx 4
[16:41:26:055][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0010055] MON MUX_V: 3 Bias 0 1213 -> 0.267137 V Bias 1 1612 -> 0.346495 V, Temperature 0.0793584 -> -2.05002 C
[16:41:26:059][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0010055] MON MUX_V: 5 Bias 0 1190 -> 0.262562 V Bias 1 1589 -> 0.341921 V, Temperature 0.0793584 -> -11.7474 C
[16:41:26:063][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0010055] MON MUX_V: 7 Bias 0 1176 -> 0.259778 V Bias 1 1575 -> 0.339136 V, Temperature 0.0793584 -> -10.879 C
[16:41:26:067][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0010055] MON MUX_V: 15 Bias 0 1156 -> 0.2558 V Bias 1 1563 -> 0.336749 V, Temperature 0.0809496 -> -11.2166 C
[16:41:26:102][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0010103 on Rx 5
[16:41:26:107][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0010103] MON MUX_V: 3 Bias 0 1226 -> 0.267265 V Bias 1 1627 -> 0.347192 V, Temperature 0.079927 -> 1.31114 C
[16:41:26:111][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0010103] MON MUX_V: 5 Bias 0 1174 -> 0.256901 V Bias 1 1579 -> 0.337625 V, Temperature 0.0807242 -> 0.480011 C
[16:41:26:115][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0010103] MON MUX_V: 7 Bias 0 1155 -> 0.253114 V Bias 1 1571 -> 0.33603 V, Temperature 0.0829168 -> -0.674927 C
[16:41:26:120][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0010103] MON MUX_V: 15 Bias 0 1171 -> 0.256303 V Bias 1 1580 -> 0.337824 V, Temperature 0.0815215 -> -0.495758 C
[16:41:26:154][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0010119 on Rx 6
[16:41:26:159][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0010119] MON MUX_V: 3 Bias 0 1210 -> 0.26394 V Bias 1 1619 -> 0.344697 V, Temperature 0.0807574 -> 0.276978 C
[16:41:26:163][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0010119] MON MUX_V: 5 Bias 0 1195 -> 0.260978 V Bias 1 1601 -> 0.341143 V, Temperature 0.0801651 -> -14.5058 C
[16:41:26:167][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0010119] MON MUX_V: 7 Bias 0 1176 -> 0.257226 V Bias 1 1588 -> 0.338576 V, Temperature 0.0813498 -> -10.0105 C
[16:41:26:172][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0010119] MON MUX_V: 15 Bias 0 1166 -> 0.255252 V Bias 1 1577 -> 0.336404 V, Temperature 0.0811523 -> -10.3 C
[16:41:26:206][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0010087 on Rx 7
[16:41:26:211][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0010087] MON MUX_V: 3 Bias 0 1231 -> 0.267599 V Bias 1 1628 -> 0.346384 V, Temperature 0.0787847 -> 1.92229 C
[16:41:26:215][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0010087] MON MUX_V: 5 Bias 0 1197 -> 0.260852 V Bias 1 1606 -> 0.342018 V, Temperature 0.0811661 -> -4.79987 C
[16:41:26:219][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0010087] MON MUX_V: 7 Bias 0 1185 -> 0.25847 V Bias 1 1600 -> 0.340827 V, Temperature 0.0823568 -> -5.58572 C
[16:41:26:223][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0010087] MON MUX_V: 15 Bias 0 1188 -> 0.259065 V Bias 1 1598 -> 0.34043 V, Temperature 0.0813645 -> -4.79996 C
[16:41:26:260][  info  ][  scanConsole  ]: Scan done!
[16:41:26:260][  info  ][  scanConsole  ]: Waiting for processors to finish ...
[16:41:26:261][  info  ][  scanConsole  ]: Processor done, waiting for histogrammer ...
[16:41:26:660][  info  ][HistogramAlgorithm]: Histogrammer done!
[16:41:26:661][  info  ][HistogramAlgorithm]: Histogrammer done!
[16:41:26:660][  info  ][HistogramAlgorithm]: Histogrammer done!
[16:41:26:661][  info  ][HistogramAlgorithm]: Histogrammer done!
[16:41:26:661][  info  ][  scanConsole  ]: Processor done, waiting for analysis ...
[16:41:27:061][  info  ][AnalysisAlgorithm]: Analysis done!
[16:41:27:061][  info  ][AnalysisAlgorithm]: Analysis done!
[16:41:27:061][  info  ][AnalysisAlgorithm]: Analysis done!
[16:41:27:061][  info  ][AnalysisAlgorithm]: Analysis done!
[16:41:27:061][  info  ][  scanConsole  ]: All done!
[16:41:27:061][  info  ][  scanConsole  ]: ##########
[16:41:27:061][  info  ][  scanConsole  ]: # Timing #
[16:41:27:061][  info  ][  scanConsole  ]: ##########
[16:41:27:061][  info  ][  scanConsole  ]: -> Configuration: 170 ms
[16:41:27:061][  info  ][  scanConsole  ]: -> Scan:          209 ms
[16:41:27:061][  info  ][  scanConsole  ]: -> Processing:    1 ms
[16:41:27:061][  info  ][  scanConsole  ]: -> Analysis:      800 ms
[16:41:27:061][  info  ][  scanConsole  ]: ###########
[16:41:27:061][  info  ][  scanConsole  ]: # Cleanup #
[16:41:27:061][  info  ][  scanConsole  ]: ###########
[16:41:27:070][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0010055 to configs/CERNQ6/20UPGFC0010055.json
[16:41:27:281][  info  ][  scanConsole  ]: -> Storing output results of FE 4
[16:41:27:281][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0010055, this usually means that the chip did not send any data at all.
[16:41:27:282][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0010103 to configs/CERNQ6/20UPGFC0010103.json
[16:41:27:445][  info  ][  scanConsole  ]: -> Storing output results of FE 5
[16:41:27:445][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0010103, this usually means that the chip did not send any data at all.
[16:41:27:446][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0010119 to configs/CERNQ6/20UPGFC0010119.json
[16:41:27:610][  info  ][  scanConsole  ]: -> Storing output results of FE 6
[16:41:27:610][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0010119, this usually means that the chip did not send any data at all.
[16:41:27:611][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0010087 to configs/CERNQ6/20UPGFC0010087.json
[16:41:27:774][  info  ][  scanConsole  ]: -> Storing output results of FE 7
[16:41:27:774][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0010087, this usually means that the chip did not send any data at all.
[16:41:27:776][  info  ][  scanConsole  ]: Finishing run: 1524
[root@jollyroger Yarr]# 
