[14:20:55:904][  info  ][               ]: #####################################
[14:20:55:904][  info  ][               ]: # Welcome to the YARR Scan Console! #
[14:20:55:904][  info  ][               ]: #####################################
[14:20:55:904][  info  ][               ]: -> Parsing command line parameters ...
[14:20:55:904][  info  ][               ]: Configuring logger ...
[14:20:55:906][  info  ][  scanConsole  ]: Scan Type/Config configs/scans/rd53a/reg_readtemp.json
[14:20:55:906][  info  ][  scanConsole  ]: Connectivity:
[14:20:55:906][  info  ][  scanConsole  ]:     configs/KEKQ18/connectivity.json
[14:20:55:906][  info  ][  scanConsole  ]: Target ToT: -1
[14:20:55:906][  info  ][  scanConsole  ]: Target Charge: -1
[14:20:55:906][  info  ][  scanConsole  ]: Output Plots: true
[14:20:55:906][  info  ][  scanConsole  ]: Output Directory: ./data/001836_reg_readtemp/
[14:20:55:910][  info  ][  scanConsole  ]: Timestamp: 2022-08-26_14:20:55
[14:20:55:910][  info  ][  scanConsole  ]: Run Number: 1836
[14:20:55:910][  info  ][  scanConsole  ]: #################
[14:20:55:910][  info  ][  scanConsole  ]: # Init Hardware #
[14:20:55:910][  info  ][  scanConsole  ]: #################
[14:20:55:910][  info  ][  scanConsole  ]: -> Opening controller config: configs/controller/specCfg-rd53a-16x1.json
[14:20:55:910][  info  ][  ScanHelper   ]: Loading controller ...
[14:20:55:910][  info  ][  ScanHelper   ]: Found controller of type: spec
[14:20:55:910][  info  ][  ScanHelper   ]: ... loading controler config:
[14:20:55:910][  info  ][  ScanHelper   ]: ~~~ {
[14:20:55:910][  info  ][  ScanHelper   ]: ~~~     "cmdPeriod": 6.250000073038109e-9,
[14:20:55:910][  info  ][  ScanHelper   ]: ~~~     "idle": {
[14:20:55:910][  info  ][  ScanHelper   ]: ~~~         "word": 1768515945
[14:20:55:910][  info  ][  ScanHelper   ]: ~~~     },
[14:20:55:910][  info  ][  ScanHelper   ]: ~~~     "pulse": {
[14:20:55:910][  info  ][  ScanHelper   ]: ~~~         "interval": 500,
[14:20:55:910][  info  ][  ScanHelper   ]: ~~~         "word": 1549575846
[14:20:55:910][  info  ][  ScanHelper   ]: ~~~     },
[14:20:55:910][  info  ][  ScanHelper   ]: ~~~     "rxActiveLanes": 1,
[14:20:55:910][  info  ][  ScanHelper   ]: ~~~     "rxPolarity": 0,
[14:20:55:910][  info  ][  ScanHelper   ]: ~~~     "specNum": 0,
[14:20:55:910][  info  ][  ScanHelper   ]: ~~~     "spiConfig": 541200,
[14:20:55:910][  info  ][  ScanHelper   ]: ~~~     "sync": {
[14:20:55:910][  info  ][  ScanHelper   ]: ~~~         "interval": 32,
[14:20:55:910][  info  ][  ScanHelper   ]: ~~~         "word": 2172551550
[14:20:55:910][  info  ][  ScanHelper   ]: ~~~     },
[14:20:55:910][  info  ][  ScanHelper   ]: ~~~     "txPolarity": 0
[14:20:55:910][  info  ][  ScanHelper   ]: ~~~ }
[14:20:55:910][  info  ][    SpecCom    ]: Opening SPEC with id #0
[14:20:55:910][  info  ][    SpecCom    ]: Mapping BARs ...
[14:20:55:910][  info  ][    SpecCom    ]: ... Mapped BAR0 at 0x7f13e2ac7000 with size 1048576
[14:20:55:910][warning ][    SpecCom    ]: ... BAR4 not mapped (Mmap failed)
[14:20:55:910][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[14:20:55:910][  info  ][    SpecCom    ]: Firmware Version: 0x4d9ff6d
[14:20:55:910][  info  ][    SpecCom    ]: Firmware Identifier: 0x4030232
[14:20:55:910][  info  ][    SpecCom    ]: FPGA card: PLDA XpressK7 325
[14:20:55:910][  info  ][    SpecCom    ]: FE Chip Type: RD53A/B
[14:20:55:910][  info  ][    SpecCom    ]: FMC Card Type: Ohio Card (Display Port)
[14:20:55:910][  info  ][    SpecCom    ]: RX Speed: 640Mbps
[14:20:55:910][  info  ][    SpecCom    ]: Channel Configuration: 16x1
[14:20:55:910][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[14:20:55:910][  info  ][    SpecCom    ]: Flushing buffers ...
[14:20:55:910][  info  ][    SpecCom    ]: Init success!
[14:20:55:910][  info  ][  scanConsole  ]: #######################
[14:20:55:910][  info  ][  scanConsole  ]: ##  Loading Configs  ##
[14:20:55:910][  info  ][  scanConsole  ]: #######################
[14:20:55:910][  info  ][  scanConsole  ]: Opening global config: configs/KEKQ18/connectivity.json
[14:20:55:910][  info  ][  ScanHelper   ]: Chip type: RD53A
[14:20:55:910][  info  ][  ScanHelper   ]: Chip count 4
[14:20:55:910][  info  ][  ScanHelper   ]: Loading chip #0
[14:20:55:911][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(4)
[14:20:55:911][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ18/20UPGFC0009364.json
[14:20:56:088][  info  ][  ScanHelper   ]: Loading chip #1
[14:20:56:088][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(5)
[14:20:56:088][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ18/20UPGFC0009253.json
[14:20:56:261][  info  ][  ScanHelper   ]: Loading chip #2
[14:20:56:262][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(6)
[14:20:56:262][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ18/20UPGFC0009285.json
[14:20:56:440][  info  ][  ScanHelper   ]: Loading chip #3
[14:20:56:441][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(7)
[14:20:56:441][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ18/20UPGFC0009269.json
[14:20:56:619][  info  ][  scanConsole  ]: #################
[14:20:56:619][  info  ][  scanConsole  ]: # Configure FEs #
[14:20:56:619][  info  ][  scanConsole  ]: #################
[14:20:56:619][  info  ][  scanConsole  ]: Configuring 20UPGFC0009364
[14:20:56:651][  info  ][  scanConsole  ]: Configuring 20UPGFC0009253
[14:20:56:681][  info  ][  scanConsole  ]: Configuring 20UPGFC0009285
[14:20:56:712][  info  ][  scanConsole  ]: Configuring 20UPGFC0009269
[14:20:56:752][  info  ][  scanConsole  ]: Sent configuration to all FEs in 133 ms!
[14:20:56:753][  info  ][  scanConsole  ]: Checking com 20UPGFC0009364
[14:20:56:753][  info  ][    SpecRx     ]: Active Rx channels: 0x10
[14:20:56:753][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[14:20:56:753][  info  ][    SpecRx     ]: Rx Status 0xf0
[14:20:56:753][  info  ][    SpecRx     ]: Number of lanes: 1
[14:20:56:753][  info  ][    SpecRx     ]: Channel 4 Lane 0 synchronized!
[14:20:56:764][  info  ][  scanConsole  ]: ... success!
[14:20:56:764][  info  ][  scanConsole  ]: Checking com 20UPGFC0009253
[14:20:56:764][  info  ][    SpecRx     ]: Active Rx channels: 0x20
[14:20:56:764][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[14:20:56:764][  info  ][    SpecRx     ]: Rx Status 0xf0
[14:20:56:764][  info  ][    SpecRx     ]: Number of lanes: 1
[14:20:56:764][  info  ][    SpecRx     ]: Channel 5 Lane 0 synchronized!
[14:20:56:775][  info  ][  scanConsole  ]: ... success!
[14:20:56:775][  info  ][  scanConsole  ]: Checking com 20UPGFC0009285
[14:20:56:775][  info  ][    SpecRx     ]: Active Rx channels: 0x40
[14:20:56:775][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[14:20:56:775][  info  ][    SpecRx     ]: Rx Status 0xf0
[14:20:56:775][  info  ][    SpecRx     ]: Number of lanes: 1
[14:20:56:775][  info  ][    SpecRx     ]: Channel 6 Lane 0 synchronized!
[14:20:56:785][  info  ][  scanConsole  ]: ... success!
[14:20:56:785][  info  ][  scanConsole  ]: Checking com 20UPGFC0009269
[14:20:56:785][  info  ][    SpecRx     ]: Active Rx channels: 0x80
[14:20:56:785][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[14:20:56:785][  info  ][    SpecRx     ]: Rx Status 0xf0
[14:20:56:785][  info  ][    SpecRx     ]: Number of lanes: 1
[14:20:56:785][  info  ][    SpecRx     ]: Channel 7 Lane 0 synchronized!
[14:20:56:796][  info  ][  scanConsole  ]: ... success!
[14:20:56:796][  info  ][  scanConsole  ]: Enabling Tx channels
[14:20:56:796][  info  ][  scanConsole  ]: Enabling Tx channel 1
[14:20:56:796][  info  ][  scanConsole  ]: Enabling Tx channel 1
[14:20:56:796][  info  ][  scanConsole  ]: Enabling Tx channel 1
[14:20:56:796][  info  ][  scanConsole  ]: Enabling Tx channel 1
[14:20:56:796][  info  ][  scanConsole  ]: Enabling Rx channels
[14:20:56:796][  info  ][  scanConsole  ]: Enabling Rx channel 4
[14:20:56:796][  info  ][  scanConsole  ]: Enabling Rx channel 5
[14:20:56:796][  info  ][  scanConsole  ]: Enabling Rx channel 6
[14:20:56:796][  info  ][  scanConsole  ]: Enabling Rx channel 7
[14:20:56:796][  info  ][  scanConsole  ]: ##############
[14:20:56:796][  info  ][  scanConsole  ]: # Setup Scan #
[14:20:56:796][  info  ][  scanConsole  ]: ##############
[14:20:56:796][  info  ][  scanConsole  ]: Found Scan config, constructing scan ...
[14:20:56:797][  info  ][  ScanFactory  ]: Loading Scan:
[14:20:56:797][  info  ][  ScanFactory  ]:   Name: AnalogScan
[14:20:56:797][  info  ][  ScanFactory  ]:   Number of Loops: 3
[14:20:56:797][  info  ][  ScanFactory  ]:   Loading Loop #0
[14:20:56:797][  info  ][  ScanFactory  ]:    Type: Rd53aReadRegLoop
[14:20:56:797][  info  ][  ScanFactory  ]:    Loading loop config ... 
[14:20:56:797][  info  ][  ScanFactory  ]: ~~~ {
[14:20:56:797][  info  ][  ScanFactory  ]: ~~~     "CurMux": [],
[14:20:56:797][  info  ][  ScanFactory  ]: ~~~     "EnblRingOsc": 0,
[14:20:56:797][  info  ][  ScanFactory  ]: ~~~     "Registers": [],
[14:20:56:797][  info  ][  ScanFactory  ]: ~~~     "RingOscDur": 9,
[14:20:56:797][  info  ][  ScanFactory  ]: ~~~     "RingOscRep": 10,
[14:20:56:797][  info  ][  ScanFactory  ]: ~~~     "VoltMux": [3, 5, 7, 15]
[14:20:56:797][  info  ][  ScanFactory  ]: ~~~ }
[14:20:56:797][  info  ][  ScanFactory  ]:   Loading Loop #1
[14:20:56:797][  info  ][  ScanFactory  ]:    Type: Rd53aTriggerLoop
[14:20:56:797][  info  ][  ScanFactory  ]:    Loading loop config ... 
[14:20:56:797][  info  ][  ScanFactory  ]: ~~~ {
[14:20:56:797][  info  ][  ScanFactory  ]: ~~~     "count": 1,
[14:20:56:797][  info  ][  ScanFactory  ]: ~~~     "delay": 48,
[14:20:56:798][  info  ][  ScanFactory  ]: ~~~     "extTrig": false,
[14:20:56:798][  info  ][  ScanFactory  ]: ~~~     "frequency": 30000.0,
[14:20:56:798][  info  ][  ScanFactory  ]: ~~~     "noInject": false,
[14:20:56:798][  info  ][  ScanFactory  ]: ~~~     "sendEcr": false,
[14:20:56:798][  info  ][  ScanFactory  ]: ~~~     "time": 0.0,
[14:20:56:798][  info  ][  ScanFactory  ]: ~~~     "trigMultiplier": 16
[14:20:56:798][  info  ][  ScanFactory  ]: ~~~ }
[14:20:56:798][  info  ][  ScanFactory  ]:   Loading Loop #2
[14:20:56:798][  info  ][  ScanFactory  ]:    Type: StdDataLoop
[14:20:56:798][warning ][  ScanFactory  ]: ~~~ Config empty.
[14:20:56:798][  info  ][ScanBuildHistogrammers]: Loading histogrammer ...
[14:20:56:798][  info  ][ScanBuildHistogrammers]: ... done!
[14:20:56:798][  info  ][ScanBuildAnalyses]: Loading analyses ...
[14:20:56:798][  info  ][  scanConsole  ]: Running pre scan!
[14:20:56:798][  info  ][  ScanFactory  ]: Entering pre scan phase ...
[14:20:56:799][  info  ][     Rd53a     ]: Write named register: GlobalPulseRt -> 16384
[14:20:56:799][  info  ][     Rd53a     ]: Write named register: InjEnDig -> 0
[14:20:56:799][  info  ][     Rd53a     ]: Write named register: InjVcalDiff -> 0
[14:20:56:799][  info  ][     Rd53a     ]: Write named register: InjVcalHigh -> 3500
[14:20:56:799][  info  ][     Rd53a     ]: Write named register: InjVcalMed -> 500
[14:20:56:799][  info  ][     Rd53a     ]: Write named register: LatencyConfig -> 48
[14:20:56:799][  info  ][  scanConsole  ]: Starting histogrammer and analysis threads:
[14:20:56:799][  info  ][  scanConsole  ]:  .. started threads of Fe 4
[14:20:56:799][  info  ][  scanConsole  ]:  .. started threads of Fe 5
[14:20:56:799][  info  ][  scanConsole  ]:  .. started threads of Fe 6
[14:20:56:799][  info  ][  scanConsole  ]:  .. started threads of Fe 7
[14:20:56:800][  info  ][Rd53aDataProcessor]:   -> Processor thread #0 started!
[14:20:56:800][  info  ][Rd53aDataProcessor]:   -> Processor thread #1 started!
[14:20:56:800][  info  ][Rd53aDataProcessor]:   -> Processor thread #2 started!
[14:20:56:800][  info  ][Rd53aDataProcessor]:   -> Processor thread #3 started!
[14:20:56:800][  info  ][  scanConsole  ]: ########
[14:20:56:800][  info  ][  scanConsole  ]: # Scan #
[14:20:56:800][  info  ][  scanConsole  ]: ########
[14:20:56:800][  info  ][  scanConsole  ]: Starting scan!
[14:20:56:800][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0009364 on Rx 4
[14:20:56:804][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0009364] MON MUX_V: 3 Bias 0 1189 -> 0.262174 V Bias 1 1593 -> 0.342427 V, Temperature 0.0802527 -> 7.84282 C
[14:20:56:808][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0009364] MON MUX_V: 5 Bias 0 1154 -> 0.255222 V Bias 1 1560 -> 0.335872 V, Temperature 0.08065 -> 1.30817 C
[14:20:56:812][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0009364] MON MUX_V: 7 Bias 0 1140 -> 0.252441 V Bias 1 1559 -> 0.335673 V, Temperature 0.0832324 -> -1.17941 C
[14:20:56:816][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0009364] MON MUX_V: 15 Bias 0 1135 -> 0.251448 V Bias 1 1554 -> 0.33468 V, Temperature 0.0832324 -> 1.30771 C
[14:20:56:851][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0009253 on Rx 5
[14:20:56:855][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0009253] MON MUX_V: 3 Bias 0 1226 -> 0.267291 V Bias 1 1622 -> 0.345578 V, Temperature 0.0782864 -> 8.88937 C
[14:20:56:859][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0009253] MON MUX_V: 5 Bias 0 1177 -> 0.257604 V Bias 1 1587 -> 0.338659 V, Temperature 0.0810542 -> -1.23758 C
[14:20:56:863][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0009253] MON MUX_V: 7 Bias 0 1158 -> 0.253848 V Bias 1 1575 -> 0.336286 V, Temperature 0.082438 -> 0.451752 C
[14:20:56:867][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0009253] MON MUX_V: 15 Bias 0 1147 -> 0.251674 V Bias 1 1566 -> 0.334507 V, Temperature 0.0828334 -> -1.12759 C
[14:20:56:902][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0009285 on Rx 6
[14:20:56:906][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0009285] MON MUX_V: 3 Bias 0 1222 -> 0.265221 V Bias 1 1632 -> 0.34659 V, Temperature 0.081369 -> 7.0195 C
[14:20:56:910][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0009285] MON MUX_V: 5 Bias 0 1181 -> 0.257084 V Bias 1 1593 -> 0.33885 V, Temperature 0.081766 -> 8.90631 C
[14:20:56:915][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0009285] MON MUX_V: 7 Bias 0 1169 -> 0.254703 V Bias 1 1583 -> 0.336866 V, Temperature 0.0821629 -> 8.4003 C
[14:20:56:919][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0009285] MON MUX_V: 15 Bias 0 1161 -> 0.253115 V Bias 1 1577 -> 0.335675 V, Temperature 0.0825598 -> 10.2569 C
[14:20:56:954][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0009269 on Rx 7
[14:20:56:958][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0009269] MON MUX_V: 3 Bias 0 1228 -> 0.266649 V Bias 1 1631 -> 0.346651 V, Temperature 0.0800022 -> 10.6142 C
[14:20:56:962][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0009269] MON MUX_V: 5 Bias 0 1188 -> 0.258708 V Bias 1 1602 -> 0.340894 V, Temperature 0.0821859 -> 4.28513 C
[14:20:56:966][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0009269] MON MUX_V: 7 Bias 0 1158 -> 0.252753 V Bias 1 1582 -> 0.336924 V, Temperature 0.0841711 -> 2.75156 C
[14:20:56:970][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0009269] MON MUX_V: 15 Bias 0 1180 -> 0.25712 V Bias 1 1595 -> 0.339505 V, Temperature 0.0823844 -> 4.04364 C
[14:20:57:007][  info  ][  scanConsole  ]: Scan done!
[14:20:57:007][  info  ][  scanConsole  ]: Waiting for processors to finish ...
[14:20:57:008][  info  ][  scanConsole  ]: Processor done, waiting for histogrammer ...
[14:20:57:407][  info  ][HistogramAlgorithm]: Histogrammer done!
[14:20:57:407][  info  ][HistogramAlgorithm]: Histogrammer done!
[14:20:57:407][  info  ][HistogramAlgorithm]: Histogrammer done!
[14:20:57:407][  info  ][HistogramAlgorithm]: Histogrammer done!
[14:20:57:408][  info  ][  scanConsole  ]: Processor done, waiting for analysis ...
[14:20:57:808][  info  ][AnalysisAlgorithm]: Analysis done!
[14:20:57:808][  info  ][AnalysisAlgorithm]: Analysis done!
[14:20:57:808][  info  ][AnalysisAlgorithm]: Analysis done!
[14:20:57:808][  info  ][AnalysisAlgorithm]: Analysis done!
[14:20:57:808][  info  ][  scanConsole  ]: All done!
[14:20:57:808][  info  ][  scanConsole  ]: ##########
[14:20:57:808][  info  ][  scanConsole  ]: # Timing #
[14:20:57:808][  info  ][  scanConsole  ]: ##########
[14:20:57:808][  info  ][  scanConsole  ]: -> Configuration: 133 ms
[14:20:57:808][  info  ][  scanConsole  ]: -> Scan:          206 ms
[14:20:57:808][  info  ][  scanConsole  ]: -> Processing:    0 ms
[14:20:57:808][  info  ][  scanConsole  ]: -> Analysis:      800 ms
[14:20:57:808][  info  ][  scanConsole  ]: ###########
[14:20:57:808][  info  ][  scanConsole  ]: # Cleanup #
[14:20:57:808][  info  ][  scanConsole  ]: ###########
[14:20:57:817][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0009364 to configs/KEKQ18/20UPGFC0009364.json
[14:20:58:008][  info  ][  scanConsole  ]: -> Storing output results of FE 4
[14:20:58:008][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0009364, this usually means that the chip did not send any data at all.
[14:20:58:009][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0009253 to configs/KEKQ18/20UPGFC0009253.json
[14:20:58:171][  info  ][  scanConsole  ]: -> Storing output results of FE 5
[14:20:58:171][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0009253, this usually means that the chip did not send any data at all.
[14:20:58:173][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0009285 to configs/KEKQ18/20UPGFC0009285.json
[14:20:58:336][  info  ][  scanConsole  ]: -> Storing output results of FE 6
[14:20:58:336][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0009285, this usually means that the chip did not send any data at all.
[14:20:58:337][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0009269 to configs/KEKQ18/20UPGFC0009269.json
[14:20:58:512][  info  ][  scanConsole  ]: -> Storing output results of FE 7
[14:20:58:512][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0009269, this usually means that the chip did not send any data at all.
[14:20:58:514][  info  ][  scanConsole  ]: Finishing run: 1836
20UPGFC0009253.json.after
20UPGFC0009253.json.before
20UPGFC0009269.json.after
20UPGFC0009269.json.before
20UPGFC0009285.json.after
20UPGFC0009285.json.before
20UPGFC0009364.json.after
20UPGFC0009364.json.before
reg_readtemp.json
scanLog.json

