[13:52:30:971][  info  ][               ]: #####################################
[13:52:30:971][  info  ][               ]: # Welcome to the YARR Scan Console! #
[13:52:30:971][  info  ][               ]: #####################################
[13:52:30:971][  info  ][               ]: -> Parsing command line parameters ...
[13:52:30:971][  info  ][               ]: Configuring logger ...
[13:52:30:973][  info  ][  scanConsole  ]: Scan Type/Config configs/scans/rd53a/reg_readtemp.json
[13:52:30:973][  info  ][  scanConsole  ]: Connectivity:
[13:52:30:973][  info  ][  scanConsole  ]:     configs/KEKQ18/connectivity.json
[13:52:30:973][  info  ][  scanConsole  ]: Target ToT: -1
[13:52:30:973][  info  ][  scanConsole  ]: Target Charge: -1
[13:52:30:973][  info  ][  scanConsole  ]: Output Plots: true
[13:52:30:973][  info  ][  scanConsole  ]: Output Directory: ./data/001832_reg_readtemp/
[13:52:30:979][  info  ][  scanConsole  ]: Timestamp: 2022-08-26_13:52:30
[13:52:30:979][  info  ][  scanConsole  ]: Run Number: 1832
[13:52:30:979][  info  ][  scanConsole  ]: #################
[13:52:30:979][  info  ][  scanConsole  ]: # Init Hardware #
[13:52:30:979][  info  ][  scanConsole  ]: #################
[13:52:30:979][  info  ][  scanConsole  ]: -> Opening controller config: configs/controller/specCfg-rd53a-16x1.json
[13:52:30:979][  info  ][  ScanHelper   ]: Loading controller ...
[13:52:30:979][  info  ][  ScanHelper   ]: Found controller of type: spec
[13:52:30:979][  info  ][  ScanHelper   ]: ... loading controler config:
[13:52:30:979][  info  ][  ScanHelper   ]: ~~~ {
[13:52:30:979][  info  ][  ScanHelper   ]: ~~~     "cmdPeriod": 6.250000073038109e-9,
[13:52:30:979][  info  ][  ScanHelper   ]: ~~~     "idle": {
[13:52:30:979][  info  ][  ScanHelper   ]: ~~~         "word": 1768515945
[13:52:30:979][  info  ][  ScanHelper   ]: ~~~     },
[13:52:30:979][  info  ][  ScanHelper   ]: ~~~     "pulse": {
[13:52:30:979][  info  ][  ScanHelper   ]: ~~~         "interval": 500,
[13:52:30:980][  info  ][  ScanHelper   ]: ~~~         "word": 1549575846
[13:52:30:980][  info  ][  ScanHelper   ]: ~~~     },
[13:52:30:980][  info  ][  ScanHelper   ]: ~~~     "rxActiveLanes": 1,
[13:52:30:980][  info  ][  ScanHelper   ]: ~~~     "rxPolarity": 0,
[13:52:30:980][  info  ][  ScanHelper   ]: ~~~     "specNum": 0,
[13:52:30:980][  info  ][  ScanHelper   ]: ~~~     "spiConfig": 541200,
[13:52:30:980][  info  ][  ScanHelper   ]: ~~~     "sync": {
[13:52:30:980][  info  ][  ScanHelper   ]: ~~~         "interval": 32,
[13:52:30:980][  info  ][  ScanHelper   ]: ~~~         "word": 2172551550
[13:52:30:980][  info  ][  ScanHelper   ]: ~~~     },
[13:52:30:980][  info  ][  ScanHelper   ]: ~~~     "txPolarity": 0
[13:52:30:980][  info  ][  ScanHelper   ]: ~~~ }
[13:52:30:980][  info  ][    SpecCom    ]: Opening SPEC with id #0
[13:52:30:980][  info  ][    SpecCom    ]: Mapping BARs ...
[13:52:30:980][  info  ][    SpecCom    ]: ... Mapped BAR0 at 0x7fcb91f52000 with size 1048576
[13:52:30:980][warning ][    SpecCom    ]: ... BAR4 not mapped (Mmap failed)
[13:52:30:980][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[13:52:30:980][  info  ][    SpecCom    ]: Firmware Version: 0x4d9ff6d
[13:52:30:980][  info  ][    SpecCom    ]: Firmware Identifier: 0x4030232
[13:52:30:980][  info  ][    SpecCom    ]: FPGA card: PLDA XpressK7 325
[13:52:30:980][  info  ][    SpecCom    ]: FE Chip Type: RD53A/B
[13:52:30:980][  info  ][    SpecCom    ]: FMC Card Type: Ohio Card (Display Port)
[13:52:30:980][  info  ][    SpecCom    ]: RX Speed: 640Mbps
[13:52:30:980][  info  ][    SpecCom    ]: Channel Configuration: 16x1
[13:52:30:980][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[13:52:30:980][  info  ][    SpecCom    ]: Flushing buffers ...
[13:52:30:980][  info  ][    SpecCom    ]: Init success!
[13:52:30:980][  info  ][  scanConsole  ]: #######################
[13:52:30:980][  info  ][  scanConsole  ]: ##  Loading Configs  ##
[13:52:30:980][  info  ][  scanConsole  ]: #######################
[13:52:30:980][  info  ][  scanConsole  ]: Opening global config: configs/KEKQ18/connectivity.json
[13:52:30:980][  info  ][  ScanHelper   ]: Chip type: RD53A
[13:52:30:980][  info  ][  ScanHelper   ]: Chip count 4
[13:52:30:980][  info  ][  ScanHelper   ]: Loading chip #0
[13:52:30:981][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(4)
[13:52:30:981][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ18/20UPGFC0009364.json
[13:52:31:158][  info  ][  ScanHelper   ]: Loading chip #1
[13:52:31:159][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(5)
[13:52:31:159][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ18/20UPGFC0009253.json
[13:52:31:334][  info  ][  ScanHelper   ]: Loading chip #2
[13:52:31:335][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(6)
[13:52:31:335][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ18/20UPGFC0009285.json
[13:52:31:510][  info  ][  ScanHelper   ]: Loading chip #3
[13:52:31:511][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(7)
[13:52:31:511][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ18/20UPGFC0009269.json
[13:52:31:689][  info  ][  scanConsole  ]: #################
[13:52:31:689][  info  ][  scanConsole  ]: # Configure FEs #
[13:52:31:689][  info  ][  scanConsole  ]: #################
[13:52:31:689][  info  ][  scanConsole  ]: Configuring 20UPGFC0009364
[13:52:31:720][  info  ][  scanConsole  ]: Configuring 20UPGFC0009253
[13:52:31:750][  info  ][  scanConsole  ]: Configuring 20UPGFC0009285
[13:52:31:781][  info  ][  scanConsole  ]: Configuring 20UPGFC0009269
[13:52:31:811][  info  ][  scanConsole  ]: Sent configuration to all FEs in 121 ms!
[13:52:31:812][  info  ][  scanConsole  ]: Checking com 20UPGFC0009364
[13:52:31:812][  info  ][    SpecRx     ]: Active Rx channels: 0x10
[13:52:31:812][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[13:52:31:812][  info  ][    SpecRx     ]: Rx Status 0xf0
[13:52:31:812][  info  ][    SpecRx     ]: Number of lanes: 1
[13:52:31:812][  info  ][    SpecRx     ]: Channel 4 Lane 0 synchronized!
[13:52:31:822][  info  ][  scanConsole  ]: ... success!
[13:52:31:822][  info  ][  scanConsole  ]: Checking com 20UPGFC0009253
[13:52:31:822][  info  ][    SpecRx     ]: Active Rx channels: 0x20
[13:52:31:822][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[13:52:31:822][  info  ][    SpecRx     ]: Rx Status 0xf0
[13:52:31:822][  info  ][    SpecRx     ]: Number of lanes: 1
[13:52:31:822][  info  ][    SpecRx     ]: Channel 5 Lane 0 synchronized!
[13:52:31:833][  info  ][  scanConsole  ]: ... success!
[13:52:31:833][  info  ][  scanConsole  ]: Checking com 20UPGFC0009285
[13:52:31:833][  info  ][    SpecRx     ]: Active Rx channels: 0x40
[13:52:31:833][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[13:52:31:833][  info  ][    SpecRx     ]: Rx Status 0xf0
[13:52:31:833][  info  ][    SpecRx     ]: Number of lanes: 1
[13:52:31:833][  info  ][    SpecRx     ]: Channel 6 Lane 0 synchronized!
[13:52:31:843][  info  ][  scanConsole  ]: ... success!
[13:52:31:843][  info  ][  scanConsole  ]: Checking com 20UPGFC0009269
[13:52:31:843][  info  ][    SpecRx     ]: Active Rx channels: 0x80
[13:52:31:843][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[13:52:31:843][  info  ][    SpecRx     ]: Rx Status 0xf0
[13:52:31:843][  info  ][    SpecRx     ]: Number of lanes: 1
[13:52:31:843][  info  ][    SpecRx     ]: Channel 7 Lane 0 synchronized!
[13:52:31:853][  info  ][  scanConsole  ]: ... success!
[13:52:31:854][  info  ][  scanConsole  ]: Enabling Tx channels
[13:52:31:854][  info  ][  scanConsole  ]: Enabling Tx channel 1
[13:52:31:854][  info  ][  scanConsole  ]: Enabling Tx channel 1
[13:52:31:854][  info  ][  scanConsole  ]: Enabling Tx channel 1
[13:52:31:854][  info  ][  scanConsole  ]: Enabling Tx channel 1
[13:52:31:854][  info  ][  scanConsole  ]: Enabling Rx channels
[13:52:31:854][  info  ][  scanConsole  ]: Enabling Rx channel 4
[13:52:31:854][  info  ][  scanConsole  ]: Enabling Rx channel 5
[13:52:31:854][  info  ][  scanConsole  ]: Enabling Rx channel 6
[13:52:31:854][  info  ][  scanConsole  ]: Enabling Rx channel 7
[13:52:31:854][  info  ][  scanConsole  ]: ##############
[13:52:31:854][  info  ][  scanConsole  ]: # Setup Scan #
[13:52:31:854][  info  ][  scanConsole  ]: ##############
[13:52:31:854][  info  ][  scanConsole  ]: Found Scan config, constructing scan ...
[13:52:31:855][  info  ][  ScanFactory  ]: Loading Scan:
[13:52:31:855][  info  ][  ScanFactory  ]:   Name: AnalogScan
[13:52:31:855][  info  ][  ScanFactory  ]:   Number of Loops: 3
[13:52:31:855][  info  ][  ScanFactory  ]:   Loading Loop #0
[13:52:31:855][  info  ][  ScanFactory  ]:    Type: Rd53aReadRegLoop
[13:52:31:855][  info  ][  ScanFactory  ]:    Loading loop config ... 
[13:52:31:855][  info  ][  ScanFactory  ]: ~~~ {
[13:52:31:855][  info  ][  ScanFactory  ]: ~~~     "CurMux": [],
[13:52:31:855][  info  ][  ScanFactory  ]: ~~~     "EnblRingOsc": 0,
[13:52:31:855][  info  ][  ScanFactory  ]: ~~~     "Registers": [],
[13:52:31:855][  info  ][  ScanFactory  ]: ~~~     "RingOscDur": 9,
[13:52:31:855][  info  ][  ScanFactory  ]: ~~~     "RingOscRep": 10,
[13:52:31:855][  info  ][  ScanFactory  ]: ~~~     "VoltMux": [3, 5, 7, 15]
[13:52:31:855][  info  ][  ScanFactory  ]: ~~~ }
[13:52:31:855][  info  ][  ScanFactory  ]:   Loading Loop #1
[13:52:31:855][  info  ][  ScanFactory  ]:    Type: Rd53aTriggerLoop
[13:52:31:855][  info  ][  ScanFactory  ]:    Loading loop config ... 
[13:52:31:855][  info  ][  ScanFactory  ]: ~~~ {
[13:52:31:855][  info  ][  ScanFactory  ]: ~~~     "count": 1,
[13:52:31:855][  info  ][  ScanFactory  ]: ~~~     "delay": 48,
[13:52:31:855][  info  ][  ScanFactory  ]: ~~~     "extTrig": false,
[13:52:31:855][  info  ][  ScanFactory  ]: ~~~     "frequency": 30000.0,
[13:52:31:856][  info  ][  ScanFactory  ]: ~~~     "noInject": false,
[13:52:31:856][  info  ][  ScanFactory  ]: ~~~     "sendEcr": false,
[13:52:31:856][  info  ][  ScanFactory  ]: ~~~     "time": 0.0,
[13:52:31:856][  info  ][  ScanFactory  ]: ~~~     "trigMultiplier": 16
[13:52:31:856][  info  ][  ScanFactory  ]: ~~~ }
[13:52:31:856][  info  ][  ScanFactory  ]:   Loading Loop #2
[13:52:31:856][  info  ][  ScanFactory  ]:    Type: StdDataLoop
[13:52:31:856][warning ][  ScanFactory  ]: ~~~ Config empty.
[13:52:31:856][  info  ][ScanBuildHistogrammers]: Loading histogrammer ...
[13:52:31:856][  info  ][ScanBuildHistogrammers]: ... done!
[13:52:31:856][  info  ][ScanBuildAnalyses]: Loading analyses ...
[13:52:31:856][  info  ][  scanConsole  ]: Running pre scan!
[13:52:31:857][  info  ][  ScanFactory  ]: Entering pre scan phase ...
[13:52:31:857][  info  ][     Rd53a     ]: Write named register: GlobalPulseRt -> 16384
[13:52:31:857][  info  ][     Rd53a     ]: Write named register: InjEnDig -> 0
[13:52:31:857][  info  ][     Rd53a     ]: Write named register: InjVcalDiff -> 0
[13:52:31:857][  info  ][     Rd53a     ]: Write named register: InjVcalHigh -> 3500
[13:52:31:857][  info  ][     Rd53a     ]: Write named register: InjVcalMed -> 500
[13:52:31:857][  info  ][     Rd53a     ]: Write named register: LatencyConfig -> 48
[13:52:31:857][  info  ][  scanConsole  ]: Starting histogrammer and analysis threads:
[13:52:31:857][  info  ][  scanConsole  ]:  .. started threads of Fe 4
[13:52:31:857][  info  ][  scanConsole  ]:  .. started threads of Fe 5
[13:52:31:857][  info  ][  scanConsole  ]:  .. started threads of Fe 6
[13:52:31:857][  info  ][  scanConsole  ]:  .. started threads of Fe 7
[13:52:31:858][  info  ][Rd53aDataProcessor]:   -> Processor thread #0 started!
[13:52:31:858][  info  ][Rd53aDataProcessor]:   -> Processor thread #1 started!
[13:52:31:858][  info  ][Rd53aDataProcessor]:   -> Processor thread #2 started!
[13:52:31:858][  info  ][Rd53aDataProcessor]:   -> Processor thread #3 started!
[13:52:31:858][  info  ][  scanConsole  ]: ########
[13:52:31:858][  info  ][  scanConsole  ]: # Scan #
[13:52:31:858][  info  ][  scanConsole  ]: ########
[13:52:31:858][  info  ][  scanConsole  ]: Starting scan!
[13:52:31:858][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0009364 on Rx 4
[13:52:31:862][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0009364] MON MUX_V: 3 Bias 0 996 -> 0.223836 V Bias 1 1448 -> 0.313624 V, Temperature 0.0897877 -> 37.9504 C
[13:52:31:867][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0009364] MON MUX_V: 5 Bias 0 954 -> 0.215493 V Bias 1 1411 -> 0.306274 V, Temperature 0.0907809 -> 42.7062 C
[13:52:31:871][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0009364] MON MUX_V: 7 Bias 0 936 -> 0.211917 V Bias 1 1406 -> 0.305281 V, Temperature 0.0933633 -> 42.8061 C
[13:52:31:875][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0009364] MON MUX_V: 15 Bias 0 931 -> 0.210924 V Bias 1 1399 -> 0.30389 V, Temperature 0.092966 -> 41.0824 C
[13:52:31:909][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0009253 on Rx 5
[13:52:31:914][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0009253] MON MUX_V: 3 Bias 0 1034 -> 0.229334 V Bias 1 1479 -> 0.317308 V, Temperature 0.0879734 -> 37.063 C
[13:52:31:918][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0009253] MON MUX_V: 5 Bias 0 978 -> 0.218263 V Bias 1 1438 -> 0.309202 V, Temperature 0.0909388 -> 44.7602 C
[13:52:31:922][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0009253] MON MUX_V: 7 Bias 0 959 -> 0.214507 V Bias 1 1425 -> 0.306632 V, Temperature 0.0921249 -> 38.0166 C
[13:52:31:926][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0009253] MON MUX_V: 15 Bias 0 950 -> 0.212728 V Bias 1 1419 -> 0.305446 V, Temperature 0.092718 -> 39.4587 C
[13:52:31:961][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0009285 on Rx 6
[13:52:31:965][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0009285] MON MUX_V: 3 Bias 0 1028 -> 0.22672 V Bias 1 1486 -> 0.317615 V, Temperature 0.0908952 -> 40.3877 C
[13:52:31:969][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0009285] MON MUX_V: 5 Bias 0 981 -> 0.217392 V Bias 1 1442 -> 0.308883 V, Temperature 0.0914906 -> 42.1129 C
[13:52:31:974][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0009285] MON MUX_V: 7 Bias 0 967 -> 0.214614 V Bias 1 1431 -> 0.3067 V, Temperature 0.0920859 -> 42.1685 C
[13:52:31:978][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0009285] MON MUX_V: 15 Bias 0 959 -> 0.213026 V Bias 1 1425 -> 0.305509 V, Temperature 0.0924829 -> 43.6292 C
[13:52:32:013][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0009269 on Rx 7
[13:52:32:017][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0009269] MON MUX_V: 3 Bias 0 1034 -> 0.228137 V Bias 1 1486 -> 0.317866 V, Temperature 0.0897295 -> 38.7878 C
[13:52:32:021][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0009269] MON MUX_V: 5 Bias 0 991 -> 0.2196 V Bias 1 1455 -> 0.311712 V, Temperature 0.0921117 -> 42.6166 C
[13:52:32:025][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0009269] MON MUX_V: 7 Bias 0 959 -> 0.213248 V Bias 1 1433 -> 0.307345 V, Temperature 0.0940969 -> 41.083 C
[13:52:32:029][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0009269] MON MUX_V: 15 Bias 0 980 -> 0.217417 V Bias 1 1447 -> 0.310124 V, Temperature 0.0927073 -> 41.8105 C
[13:52:32:066][  info  ][  scanConsole  ]: Scan done!
[13:52:32:066][  info  ][  scanConsole  ]: Waiting for processors to finish ...
[13:52:32:067][  info  ][  scanConsole  ]: Processor done, waiting for histogrammer ...
[13:52:32:466][  info  ][HistogramAlgorithm]: Histogrammer done!
[13:52:32:466][  info  ][HistogramAlgorithm]: Histogrammer done!
[13:52:32:466][  info  ][HistogramAlgorithm]: Histogrammer done!
[13:52:32:466][  info  ][HistogramAlgorithm]: Histogrammer done!
[13:52:32:467][  info  ][  scanConsole  ]: Processor done, waiting for analysis ...
[13:52:32:867][  info  ][AnalysisAlgorithm]: Analysis done!
[13:52:32:867][  info  ][AnalysisAlgorithm]: Analysis done!
[13:52:32:867][  info  ][AnalysisAlgorithm]: Analysis done!
[13:52:32:867][  info  ][AnalysisAlgorithm]: Analysis done!
[13:52:32:867][  info  ][  scanConsole  ]: All done!
[13:52:32:867][  info  ][  scanConsole  ]: ##########
[13:52:32:867][  info  ][  scanConsole  ]: # Timing #
[13:52:32:867][  info  ][  scanConsole  ]: ##########
[13:52:32:867][  info  ][  scanConsole  ]: -> Configuration: 121 ms
[13:52:32:867][  info  ][  scanConsole  ]: -> Scan:          207 ms
[13:52:32:867][  info  ][  scanConsole  ]: -> Processing:    0 ms
[13:52:32:867][  info  ][  scanConsole  ]: -> Analysis:      800 ms
[13:52:32:867][  info  ][  scanConsole  ]: ###########
[13:52:32:867][  info  ][  scanConsole  ]: # Cleanup #
[13:52:32:867][  info  ][  scanConsole  ]: ###########
[13:52:32:875][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0009364 to configs/KEKQ18/20UPGFC0009364.json
[13:52:33:050][  info  ][  scanConsole  ]: -> Storing output results of FE 4
[13:52:33:050][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0009364, this usually means that the chip did not send any data at all.
[13:52:33:052][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0009253 to configs/KEKQ18/20UPGFC0009253.json
[13:52:33:215][  info  ][  scanConsole  ]: -> Storing output results of FE 5
[13:52:33:215][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0009253, this usually means that the chip did not send any data at all.
[13:52:33:216][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0009285 to configs/KEKQ18/20UPGFC0009285.json
[13:52:33:395][  info  ][  scanConsole  ]: -> Storing output results of FE 6
[13:52:33:395][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0009285, this usually means that the chip did not send any data at all.
[13:52:33:396][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0009269 to configs/KEKQ18/20UPGFC0009269.json
[13:52:33:584][  info  ][  scanConsole  ]: -> Storing output results of FE 7
[13:52:33:584][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0009269, this usually means that the chip did not send any data at all.
[13:52:33:586][  info  ][  scanConsole  ]: Finishing run: 1832
20UPGFC0009253.json.after
20UPGFC0009253.json.before
20UPGFC0009269.json.after
20UPGFC0009269.json.before
20UPGFC0009285.json.after
20UPGFC0009285.json.before
20UPGFC0009364.json.after
20UPGFC0009364.json.before
reg_readtemp.json
scanLog.json

