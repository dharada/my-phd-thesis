[15:55:56:404][  info  ][               ]: #####################################
[15:55:56:405][  info  ][               ]: # Welcome to the YARR Scan Console! #
[15:55:56:405][  info  ][               ]: #####################################
[15:55:56:405][  info  ][               ]: -> Parsing command line parameters ...
[15:55:56:405][  info  ][               ]: Configuring logger ...
[15:55:56:414][  info  ][  scanConsole  ]: Scan Type/Config configs/scans/rd53a/reg_readtemp.json
[15:55:56:414][  info  ][  scanConsole  ]: Connectivity:
[15:55:56:414][  info  ][  scanConsole  ]:     configs/KEKQ18/connectivity.json
[15:55:56:414][  info  ][  scanConsole  ]: Target ToT: -1
[15:55:56:414][  info  ][  scanConsole  ]: Target Charge: -1
[15:55:56:414][  info  ][  scanConsole  ]: Output Plots: true
[15:55:56:414][  info  ][  scanConsole  ]: Output Directory: ./data/001853_reg_readtemp/
[15:55:56:433][  info  ][  scanConsole  ]: Timestamp: 2022-08-26_15:55:56
[15:55:56:433][  info  ][  scanConsole  ]: Run Number: 1853
[15:55:56:433][  info  ][  scanConsole  ]: #################
[15:55:56:433][  info  ][  scanConsole  ]: # Init Hardware #
[15:55:56:433][  info  ][  scanConsole  ]: #################
[15:55:56:433][  info  ][  scanConsole  ]: -> Opening controller config: configs/controller/specCfg-rd53a-16x1.json
[15:55:56:433][  info  ][  ScanHelper   ]: Loading controller ...
[15:55:56:433][  info  ][  ScanHelper   ]: Found controller of type: spec
[15:55:56:433][  info  ][  ScanHelper   ]: ... loading controler config:
[15:55:56:434][  info  ][  ScanHelper   ]: ~~~ {
[15:55:56:434][  info  ][  ScanHelper   ]: ~~~     "cmdPeriod": 6.250000073038109e-9,
[15:55:56:434][  info  ][  ScanHelper   ]: ~~~     "idle": {
[15:55:56:434][  info  ][  ScanHelper   ]: ~~~         "word": 1768515945
[15:55:56:434][  info  ][  ScanHelper   ]: ~~~     },
[15:55:56:434][  info  ][  ScanHelper   ]: ~~~     "pulse": {
[15:55:56:434][  info  ][  ScanHelper   ]: ~~~         "interval": 500,
[15:55:56:434][  info  ][  ScanHelper   ]: ~~~         "word": 1549575846
[15:55:56:434][  info  ][  ScanHelper   ]: ~~~     },
[15:55:56:434][  info  ][  ScanHelper   ]: ~~~     "rxActiveLanes": 1,
[15:55:56:434][  info  ][  ScanHelper   ]: ~~~     "rxPolarity": 0,
[15:55:56:434][  info  ][  ScanHelper   ]: ~~~     "specNum": 0,
[15:55:56:434][  info  ][  ScanHelper   ]: ~~~     "spiConfig": 541200,
[15:55:56:434][  info  ][  ScanHelper   ]: ~~~     "sync": {
[15:55:56:434][  info  ][  ScanHelper   ]: ~~~         "interval": 32,
[15:55:56:434][  info  ][  ScanHelper   ]: ~~~         "word": 2172551550
[15:55:56:434][  info  ][  ScanHelper   ]: ~~~     },
[15:55:56:434][  info  ][  ScanHelper   ]: ~~~     "txPolarity": 0
[15:55:56:434][  info  ][  ScanHelper   ]: ~~~ }
[15:55:56:434][  info  ][    SpecCom    ]: Opening SPEC with id #0
[15:55:56:434][  info  ][    SpecCom    ]: Mapping BARs ...
[15:55:56:434][  info  ][    SpecCom    ]: ... Mapped BAR0 at 0x7fbdaad86000 with size 1048576
[15:55:56:435][warning ][    SpecCom    ]: ... BAR4 not mapped (Mmap failed)
[15:55:56:435][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[15:55:56:435][  info  ][    SpecCom    ]: Firmware Version: 0x4d9ff6d
[15:55:56:435][  info  ][    SpecCom    ]: Firmware Identifier: 0x4030232
[15:55:56:435][  info  ][    SpecCom    ]: FPGA card: PLDA XpressK7 325
[15:55:56:435][  info  ][    SpecCom    ]: FE Chip Type: RD53A/B
[15:55:56:435][  info  ][    SpecCom    ]: FMC Card Type: Ohio Card (Display Port)
[15:55:56:435][  info  ][    SpecCom    ]: RX Speed: 640Mbps
[15:55:56:435][  info  ][    SpecCom    ]: Channel Configuration: 16x1
[15:55:56:435][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[15:55:56:435][  info  ][    SpecCom    ]: Flushing buffers ...
[15:55:56:435][  info  ][    SpecCom    ]: Init success!
[15:55:56:435][  info  ][  scanConsole  ]: #######################
[15:55:56:435][  info  ][  scanConsole  ]: ##  Loading Configs  ##
[15:55:56:435][  info  ][  scanConsole  ]: #######################
[15:55:56:435][  info  ][  scanConsole  ]: Opening global config: configs/KEKQ18/connectivity.json
[15:55:56:435][  info  ][  ScanHelper   ]: Chip type: RD53A
[15:55:56:436][  info  ][  ScanHelper   ]: Chip count 4
[15:55:56:436][  info  ][  ScanHelper   ]: Loading chip #0
[15:55:56:439][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(4)
[15:55:56:439][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ18/20UPGFC0009364.json
[15:55:56:640][  info  ][  ScanHelper   ]: Loading chip #1
[15:55:56:640][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(5)
[15:55:56:641][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ18/20UPGFC0009253.json
[15:55:56:814][  info  ][  ScanHelper   ]: Loading chip #2
[15:55:56:815][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(6)
[15:55:56:815][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ18/20UPGFC0009285.json
[15:55:56:997][  info  ][  ScanHelper   ]: Loading chip #3
[15:55:56:998][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(7)
[15:55:56:998][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ18/20UPGFC0009269.json
[15:55:57:197][  info  ][  scanConsole  ]: #################
[15:55:57:197][  info  ][  scanConsole  ]: # Configure FEs #
[15:55:57:197][  info  ][  scanConsole  ]: #################
[15:55:57:197][  info  ][  scanConsole  ]: Configuring 20UPGFC0009364
[15:55:57:228][  info  ][  scanConsole  ]: Configuring 20UPGFC0009253
[15:55:57:266][  info  ][  scanConsole  ]: Configuring 20UPGFC0009285
[15:55:57:313][  info  ][  scanConsole  ]: Configuring 20UPGFC0009269
[15:55:57:361][  info  ][  scanConsole  ]: Sent configuration to all FEs in 163 ms!
[15:55:57:362][  info  ][  scanConsole  ]: Checking com 20UPGFC0009364
[15:55:57:362][  info  ][    SpecRx     ]: Active Rx channels: 0x10
[15:55:57:362][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[15:55:57:362][  info  ][    SpecRx     ]: Rx Status 0xf0
[15:55:57:362][  info  ][    SpecRx     ]: Number of lanes: 1
[15:55:57:362][  info  ][    SpecRx     ]: Channel 4 Lane 0 synchronized!
[15:55:57:373][  info  ][  scanConsole  ]: ... success!
[15:55:57:373][  info  ][  scanConsole  ]: Checking com 20UPGFC0009253
[15:55:57:373][  info  ][    SpecRx     ]: Active Rx channels: 0x20
[15:55:57:373][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[15:55:57:373][  info  ][    SpecRx     ]: Rx Status 0xf0
[15:55:57:373][  info  ][    SpecRx     ]: Number of lanes: 1
[15:55:57:373][  info  ][    SpecRx     ]: Channel 5 Lane 0 synchronized!
[15:55:57:383][  info  ][  scanConsole  ]: ... success!
[15:55:57:383][  info  ][  scanConsole  ]: Checking com 20UPGFC0009285
[15:55:57:383][  info  ][    SpecRx     ]: Active Rx channels: 0x40
[15:55:57:383][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[15:55:57:383][  info  ][    SpecRx     ]: Rx Status 0xf0
[15:55:57:383][  info  ][    SpecRx     ]: Number of lanes: 1
[15:55:57:383][  info  ][    SpecRx     ]: Channel 6 Lane 0 synchronized!
[15:55:57:394][  info  ][  scanConsole  ]: ... success!
[15:55:57:394][  info  ][  scanConsole  ]: Checking com 20UPGFC0009269
[15:55:57:394][  info  ][    SpecRx     ]: Active Rx channels: 0x80
[15:55:57:394][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[15:55:57:394][  info  ][    SpecRx     ]: Rx Status 0xf0
[15:55:57:394][  info  ][    SpecRx     ]: Number of lanes: 1
[15:55:57:394][  info  ][    SpecRx     ]: Channel 7 Lane 0 synchronized!
[15:55:57:404][  info  ][  scanConsole  ]: ... success!
[15:55:57:404][  info  ][  scanConsole  ]: Enabling Tx channels
[15:55:57:404][  info  ][  scanConsole  ]: Enabling Tx channel 1
[15:55:57:404][  info  ][  scanConsole  ]: Enabling Tx channel 1
[15:55:57:404][  info  ][  scanConsole  ]: Enabling Tx channel 1
[15:55:57:405][  info  ][  scanConsole  ]: Enabling Tx channel 1
[15:55:57:405][  info  ][  scanConsole  ]: Enabling Rx channels
[15:55:57:405][  info  ][  scanConsole  ]: Enabling Rx channel 4
[15:55:57:405][  info  ][  scanConsole  ]: Enabling Rx channel 5
[15:55:57:405][  info  ][  scanConsole  ]: Enabling Rx channel 6
[15:55:57:405][  info  ][  scanConsole  ]: Enabling Rx channel 7
[15:55:57:405][  info  ][  scanConsole  ]: ##############
[15:55:57:405][  info  ][  scanConsole  ]: # Setup Scan #
[15:55:57:405][  info  ][  scanConsole  ]: ##############
[15:55:57:405][  info  ][  scanConsole  ]: Found Scan config, constructing scan ...
[15:55:57:406][  info  ][  ScanFactory  ]: Loading Scan:
[15:55:57:406][  info  ][  ScanFactory  ]:   Name: AnalogScan
[15:55:57:406][  info  ][  ScanFactory  ]:   Number of Loops: 3
[15:55:57:406][  info  ][  ScanFactory  ]:   Loading Loop #0
[15:55:57:406][  info  ][  ScanFactory  ]:    Type: Rd53aReadRegLoop
[15:55:57:406][  info  ][  ScanFactory  ]:    Loading loop config ... 
[15:55:57:406][  info  ][  ScanFactory  ]: ~~~ {
[15:55:57:406][  info  ][  ScanFactory  ]: ~~~     "CurMux": [],
[15:55:57:406][  info  ][  ScanFactory  ]: ~~~     "EnblRingOsc": 0,
[15:55:57:406][  info  ][  ScanFactory  ]: ~~~     "Registers": [],
[15:55:57:406][  info  ][  ScanFactory  ]: ~~~     "RingOscDur": 9,
[15:55:57:406][  info  ][  ScanFactory  ]: ~~~     "RingOscRep": 10,
[15:55:57:406][  info  ][  ScanFactory  ]: ~~~     "VoltMux": [3, 5, 7, 15]
[15:55:57:406][  info  ][  ScanFactory  ]: ~~~ }
[15:55:57:406][  info  ][  ScanFactory  ]:   Loading Loop #1
[15:55:57:406][  info  ][  ScanFactory  ]:    Type: Rd53aTriggerLoop
[15:55:57:406][  info  ][  ScanFactory  ]:    Loading loop config ... 
[15:55:57:406][  info  ][  ScanFactory  ]: ~~~ {
[15:55:57:406][  info  ][  ScanFactory  ]: ~~~     "count": 1,
[15:55:57:406][  info  ][  ScanFactory  ]: ~~~     "delay": 48,
[15:55:57:406][  info  ][  ScanFactory  ]: ~~~     "extTrig": false,
[15:55:57:406][  info  ][  ScanFactory  ]: ~~~     "frequency": 30000.0,
[15:55:57:406][  info  ][  ScanFactory  ]: ~~~     "noInject": false,
[15:55:57:406][  info  ][  ScanFactory  ]: ~~~     "sendEcr": false,
[15:55:57:406][  info  ][  ScanFactory  ]: ~~~     "time": 0.0,
[15:55:57:406][  info  ][  ScanFactory  ]: ~~~     "trigMultiplier": 16
[15:55:57:406][  info  ][  ScanFactory  ]: ~~~ }
[15:55:57:406][  info  ][  ScanFactory  ]:   Loading Loop #2
[15:55:57:406][  info  ][  ScanFactory  ]:    Type: StdDataLoop
[15:55:57:406][warning ][  ScanFactory  ]: ~~~ Config empty.
[15:55:57:407][  info  ][ScanBuildHistogrammers]: Loading histogrammer ...
[15:55:57:407][  info  ][ScanBuildHistogrammers]: ... done!
[15:55:57:407][  info  ][ScanBuildAnalyses]: Loading analyses ...
[15:55:57:407][  info  ][  scanConsole  ]: Running pre scan!
[15:55:57:407][  info  ][  ScanFactory  ]: Entering pre scan phase ...
[15:55:57:407][  info  ][     Rd53a     ]: Write named register: GlobalPulseRt -> 16384
[15:55:57:407][  info  ][     Rd53a     ]: Write named register: InjEnDig -> 0
[15:55:57:407][  info  ][     Rd53a     ]: Write named register: InjVcalDiff -> 0
[15:55:57:407][  info  ][     Rd53a     ]: Write named register: InjVcalHigh -> 3500
[15:55:57:408][  info  ][     Rd53a     ]: Write named register: InjVcalMed -> 500
[15:55:57:408][  info  ][     Rd53a     ]: Write named register: LatencyConfig -> 48
[15:55:57:408][  info  ][  scanConsole  ]: Starting histogrammer and analysis threads:
[15:55:57:408][  info  ][  scanConsole  ]:  .. started threads of Fe 4
[15:55:57:408][  info  ][  scanConsole  ]:  .. started threads of Fe 5
[15:55:57:408][  info  ][  scanConsole  ]:  .. started threads of Fe 6
[15:55:57:408][  info  ][  scanConsole  ]:  .. started threads of Fe 7
[15:55:57:408][  info  ][Rd53aDataProcessor]:   -> Processor thread #0 started!
[15:55:57:409][  info  ][Rd53aDataProcessor]:   -> Processor thread #1 started!
[15:55:57:409][  info  ][Rd53aDataProcessor]:   -> Processor thread #2 started!
[15:55:57:409][  info  ][Rd53aDataProcessor]:   -> Processor thread #3 started!
[15:55:57:409][  info  ][  scanConsole  ]: ########
[15:55:57:409][  info  ][  scanConsole  ]: # Scan #
[15:55:57:409][  info  ][  scanConsole  ]: ########
[15:55:57:409][  info  ][  scanConsole  ]: Starting scan!
[15:55:57:409][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0009364 on Rx 4
[15:55:57:413][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0009364] MON MUX_V: 3 Bias 0 1276 -> 0.279457 V Bias 1 1659 -> 0.355538 V, Temperature 0.0760811 -> -5.32948 C
[15:55:57:417][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0009364] MON MUX_V: 5 Bias 0 1244 -> 0.2731 V Bias 1 1628 -> 0.34938 V, Temperature 0.0762798 -> -16.5499 C
[15:55:57:422][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0009364] MON MUX_V: 7 Bias 0 1233 -> 0.270915 V Bias 1 1630 -> 0.349777 V, Temperature 0.0788622 -> -20.1535 C
[15:55:57:426][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0009364] MON MUX_V: 15 Bias 0 1227 -> 0.269723 V Bias 1 1624 -> 0.348585 V, Temperature 0.0788622 -> -16.5502 C
[15:55:57:461][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0009253 on Rx 5
[15:55:57:465][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0009253] MON MUX_V: 3 Bias 0 1312 -> 0.284293 V Bias 1 1686 -> 0.35823 V, Temperature 0.0739372 -> -3.75999 C
[15:55:57:469][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0009253] MON MUX_V: 5 Bias 0 1265 -> 0.275001 V Bias 1 1652 -> 0.351509 V, Temperature 0.0765072 -> -22.3966 C
[15:55:57:473][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0009253] MON MUX_V: 7 Bias 0 1247 -> 0.271443 V Bias 1 1640 -> 0.349136 V, Temperature 0.0776934 -> -17.9473 C
[15:55:57:478][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0009253] MON MUX_V: 15 Bias 0 1234 -> 0.268873 V Bias 1 1632 -> 0.347555 V, Temperature 0.0786818 -> -18.1739 C
[15:55:57:513][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0009285 on Rx 6
[15:55:57:517][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0009285] MON MUX_V: 3 Bias 0 1308 -> 0.282289 V Bias 1 1698 -> 0.359689 V, Temperature 0.0773998 -> -6.88379 C
[15:55:57:521][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0009285] MON MUX_V: 5 Bias 0 1269 -> 0.274549 V Bias 1 1660 -> 0.352147 V, Temperature 0.0775983 -> -5.32516 C
[15:55:57:525][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0009285] MON MUX_V: 7 Bias 0 1260 -> 0.272763 V Bias 1 1651 -> 0.350361 V, Temperature 0.0775983 -> -7.133 C
[15:55:57:529][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0009285] MON MUX_V: 15 Bias 0 1253 -> 0.271373 V Bias 1 1645 -> 0.34917 V, Temperature 0.0777968 -> -5.76172 C
[15:55:57:564][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0009269 on Rx 7
[15:55:57:568][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0009269] MON MUX_V: 3 Bias 0 1312 -> 0.283324 V Bias 1 1694 -> 0.359158 V, Temperature 0.0758334 -> -1.46025 C
[15:55:57:572][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0009269] MON MUX_V: 5 Bias 0 1275 -> 0.275979 V Bias 1 1666 -> 0.353599 V, Temperature 0.07762 -> -13.3473 C
[15:55:57:576][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0009269] MON MUX_V: 7 Bias 0 1246 -> 0.270222 V Bias 1 1648 -> 0.350026 V, Temperature 0.0798037 -> -14.1144 C
[15:55:57:581][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0009269] MON MUX_V: 15 Bias 0 1269 -> 0.274788 V Bias 1 1661 -> 0.352607 V, Temperature 0.0778185 -> -12.6606 C
[15:55:57:616][  info  ][  scanConsole  ]: Scan done!
[15:55:57:616][  info  ][  scanConsole  ]: Waiting for processors to finish ...
[15:55:57:617][  info  ][  scanConsole  ]: Processor done, waiting for histogrammer ...
[15:55:58:017][  info  ][HistogramAlgorithm]: Histogrammer done!
[15:55:58:017][  info  ][HistogramAlgorithm]: Histogrammer done!
[15:55:58:017][  info  ][HistogramAlgorithm]: Histogrammer done!
[15:55:58:017][  info  ][HistogramAlgorithm]: Histogrammer done!
[15:55:58:017][  info  ][  scanConsole  ]: Processor done, waiting for analysis ...
[15:55:58:418][  info  ][AnalysisAlgorithm]: Analysis done!
[15:55:58:418][  info  ][AnalysisAlgorithm]: Analysis done!
[15:55:58:418][  info  ][AnalysisAlgorithm]: Analysis done!
[15:55:58:418][  info  ][AnalysisAlgorithm]: Analysis done!
[15:55:58:418][  info  ][  scanConsole  ]: All done!
[15:55:58:418][  info  ][  scanConsole  ]: ##########
[15:55:58:418][  info  ][  scanConsole  ]: # Timing #
[15:55:58:418][  info  ][  scanConsole  ]: ##########
[15:55:58:418][  info  ][  scanConsole  ]: -> Configuration: 163 ms
[15:55:58:418][  info  ][  scanConsole  ]: -> Scan:          207 ms
[15:55:58:418][  info  ][  scanConsole  ]: -> Processing:    0 ms
[15:55:58:418][  info  ][  scanConsole  ]: -> Analysis:      800 ms
[15:55:58:418][  info  ][  scanConsole  ]: ###########
[15:55:58:418][  info  ][  scanConsole  ]: # Cleanup #
[15:55:58:418][  info  ][  scanConsole  ]: ###########
[15:55:58:427][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0009364 to configs/KEKQ18/20UPGFC0009364.json
[15:55:58:632][  info  ][  scanConsole  ]: -> Storing output results of FE 4
[15:55:58:632][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0009364, this usually means that the chip did not send any data at all.
[15:55:58:633][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0009253 to configs/KEKQ18/20UPGFC0009253.json
[15:55:58:799][  info  ][  scanConsole  ]: -> Storing output results of FE 5
[15:55:58:799][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0009253, this usually means that the chip did not send any data at all.
[15:55:58:801][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0009285 to configs/KEKQ18/20UPGFC0009285.json
[15:55:58:967][  info  ][  scanConsole  ]: -> Storing output results of FE 6
[15:55:58:967][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0009285, this usually means that the chip did not send any data at all.
[15:55:58:969][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0009269 to configs/KEKQ18/20UPGFC0009269.json
[15:55:59:147][  info  ][  scanConsole  ]: -> Storing output results of FE 7
[15:55:59:147][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0009269, this usually means that the chip did not send any data at all.
[15:55:59:148][  info  ][  scanConsole  ]: Finishing run: 1853
20UPGFC0009253.json.after
20UPGFC0009253.json.before
20UPGFC0009269.json.after
20UPGFC0009269.json.before
20UPGFC0009285.json.after
20UPGFC0009285.json.before
20UPGFC0009364.json.after
20UPGFC0009364.json.before
reg_readtemp.json
scanLog.json

