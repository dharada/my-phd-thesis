[16:01:16:567][  info  ][               ]: #####################################
[16:01:16:567][  info  ][               ]: # Welcome to the YARR Scan Console! #
[16:01:16:567][  info  ][               ]: #####################################
[16:01:16:567][  info  ][               ]: -> Parsing command line parameters ...
[16:01:16:567][  info  ][               ]: Configuring logger ...
[16:01:16:570][  info  ][  scanConsole  ]: Scan Type/Config configs/scans/rd53a/reg_readtemp.json
[16:01:16:570][  info  ][  scanConsole  ]: Connectivity:
[16:01:16:570][  info  ][  scanConsole  ]:     configs/KEKQ18/connectivity.json
[16:01:16:570][  info  ][  scanConsole  ]: Target ToT: -1
[16:01:16:570][  info  ][  scanConsole  ]: Target Charge: -1
[16:01:16:570][  info  ][  scanConsole  ]: Output Plots: true
[16:01:16:570][  info  ][  scanConsole  ]: Output Directory: ./data/001856_reg_readtemp/
[16:01:16:574][  info  ][  scanConsole  ]: Timestamp: 2022-08-26_16:01:16
[16:01:16:574][  info  ][  scanConsole  ]: Run Number: 1856
[16:01:16:574][  info  ][  scanConsole  ]: #################
[16:01:16:574][  info  ][  scanConsole  ]: # Init Hardware #
[16:01:16:574][  info  ][  scanConsole  ]: #################
[16:01:16:574][  info  ][  scanConsole  ]: -> Opening controller config: configs/controller/specCfg-rd53a-16x1.json
[16:01:16:574][  info  ][  ScanHelper   ]: Loading controller ...
[16:01:16:574][  info  ][  ScanHelper   ]: Found controller of type: spec
[16:01:16:574][  info  ][  ScanHelper   ]: ... loading controler config:
[16:01:16:574][  info  ][  ScanHelper   ]: ~~~ {
[16:01:16:574][  info  ][  ScanHelper   ]: ~~~     "cmdPeriod": 6.250000073038109e-9,
[16:01:16:574][  info  ][  ScanHelper   ]: ~~~     "idle": {
[16:01:16:574][  info  ][  ScanHelper   ]: ~~~         "word": 1768515945
[16:01:16:574][  info  ][  ScanHelper   ]: ~~~     },
[16:01:16:574][  info  ][  ScanHelper   ]: ~~~     "pulse": {
[16:01:16:574][  info  ][  ScanHelper   ]: ~~~         "interval": 500,
[16:01:16:574][  info  ][  ScanHelper   ]: ~~~         "word": 1549575846
[16:01:16:574][  info  ][  ScanHelper   ]: ~~~     },
[16:01:16:574][  info  ][  ScanHelper   ]: ~~~     "rxActiveLanes": 1,
[16:01:16:574][  info  ][  ScanHelper   ]: ~~~     "rxPolarity": 0,
[16:01:16:574][  info  ][  ScanHelper   ]: ~~~     "specNum": 0,
[16:01:16:574][  info  ][  ScanHelper   ]: ~~~     "spiConfig": 541200,
[16:01:16:574][  info  ][  ScanHelper   ]: ~~~     "sync": {
[16:01:16:574][  info  ][  ScanHelper   ]: ~~~         "interval": 32,
[16:01:16:574][  info  ][  ScanHelper   ]: ~~~         "word": 2172551550
[16:01:16:574][  info  ][  ScanHelper   ]: ~~~     },
[16:01:16:574][  info  ][  ScanHelper   ]: ~~~     "txPolarity": 0
[16:01:16:574][  info  ][  ScanHelper   ]: ~~~ }
[16:01:16:574][  info  ][    SpecCom    ]: Opening SPEC with id #0
[16:01:16:575][  info  ][    SpecCom    ]: Mapping BARs ...
[16:01:16:575][  info  ][    SpecCom    ]: ... Mapped BAR0 at 0x7f79f80cf000 with size 1048576
[16:01:16:575][warning ][    SpecCom    ]: ... BAR4 not mapped (Mmap failed)
[16:01:16:575][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[16:01:16:575][  info  ][    SpecCom    ]: Firmware Version: 0x4d9ff6d
[16:01:16:575][  info  ][    SpecCom    ]: Firmware Identifier: 0x4030232
[16:01:16:575][  info  ][    SpecCom    ]: FPGA card: PLDA XpressK7 325
[16:01:16:575][  info  ][    SpecCom    ]: FE Chip Type: RD53A/B
[16:01:16:575][  info  ][    SpecCom    ]: FMC Card Type: Ohio Card (Display Port)
[16:01:16:575][  info  ][    SpecCom    ]: RX Speed: 640Mbps
[16:01:16:575][  info  ][    SpecCom    ]: Channel Configuration: 16x1
[16:01:16:575][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[16:01:16:575][  info  ][    SpecCom    ]: Flushing buffers ...
[16:01:16:575][  info  ][    SpecCom    ]: Init success!
[16:01:16:575][  info  ][  scanConsole  ]: #######################
[16:01:16:575][  info  ][  scanConsole  ]: ##  Loading Configs  ##
[16:01:16:575][  info  ][  scanConsole  ]: #######################
[16:01:16:575][  info  ][  scanConsole  ]: Opening global config: configs/KEKQ18/connectivity.json
[16:01:16:575][  info  ][  ScanHelper   ]: Chip type: RD53A
[16:01:16:575][  info  ][  ScanHelper   ]: Chip count 4
[16:01:16:575][  info  ][  ScanHelper   ]: Loading chip #0
[16:01:16:575][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(4)
[16:01:16:575][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ18/20UPGFC0009364.json
[16:01:16:754][  info  ][  ScanHelper   ]: Loading chip #1
[16:01:16:754][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(5)
[16:01:16:754][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ18/20UPGFC0009253.json
[16:01:16:930][  info  ][  ScanHelper   ]: Loading chip #2
[16:01:16:930][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(6)
[16:01:16:930][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ18/20UPGFC0009285.json
[16:01:17:104][  info  ][  ScanHelper   ]: Loading chip #3
[16:01:17:104][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(7)
[16:01:17:104][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ18/20UPGFC0009269.json
[16:01:17:279][  info  ][  scanConsole  ]: #################
[16:01:17:279][  info  ][  scanConsole  ]: # Configure FEs #
[16:01:17:279][  info  ][  scanConsole  ]: #################
[16:01:17:279][  info  ][  scanConsole  ]: Configuring 20UPGFC0009364
[16:01:17:310][  info  ][  scanConsole  ]: Configuring 20UPGFC0009253
[16:01:17:340][  info  ][  scanConsole  ]: Configuring 20UPGFC0009285
[16:01:17:371][  info  ][  scanConsole  ]: Configuring 20UPGFC0009269
[16:01:17:407][  info  ][  scanConsole  ]: Sent configuration to all FEs in 128 ms!
[16:01:17:409][  info  ][  scanConsole  ]: Checking com 20UPGFC0009364
[16:01:17:409][  info  ][    SpecRx     ]: Active Rx channels: 0x10
[16:01:17:409][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[16:01:17:409][  info  ][    SpecRx     ]: Rx Status 0xf0
[16:01:17:409][  info  ][    SpecRx     ]: Number of lanes: 1
[16:01:17:409][  info  ][    SpecRx     ]: Channel 4 Lane 0 synchronized!
[16:01:17:419][  info  ][  scanConsole  ]: ... success!
[16:01:17:419][  info  ][  scanConsole  ]: Checking com 20UPGFC0009253
[16:01:17:419][  info  ][    SpecRx     ]: Active Rx channels: 0x20
[16:01:17:419][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[16:01:17:419][  info  ][    SpecRx     ]: Rx Status 0xf0
[16:01:17:419][  info  ][    SpecRx     ]: Number of lanes: 1
[16:01:17:419][  info  ][    SpecRx     ]: Channel 5 Lane 0 synchronized!
[16:01:17:430][  info  ][  scanConsole  ]: ... success!
[16:01:17:430][  info  ][  scanConsole  ]: Checking com 20UPGFC0009285
[16:01:17:430][  info  ][    SpecRx     ]: Active Rx channels: 0x40
[16:01:17:430][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[16:01:17:430][  info  ][    SpecRx     ]: Rx Status 0xf0
[16:01:17:430][  info  ][    SpecRx     ]: Number of lanes: 1
[16:01:17:430][  info  ][    SpecRx     ]: Channel 6 Lane 0 synchronized!
[16:01:17:440][  info  ][  scanConsole  ]: ... success!
[16:01:17:440][  info  ][  scanConsole  ]: Checking com 20UPGFC0009269
[16:01:17:440][  info  ][    SpecRx     ]: Active Rx channels: 0x80
[16:01:17:441][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[16:01:17:441][  info  ][    SpecRx     ]: Rx Status 0xf0
[16:01:17:441][  info  ][    SpecRx     ]: Number of lanes: 1
[16:01:17:441][  info  ][    SpecRx     ]: Channel 7 Lane 0 synchronized!
[16:01:17:451][  info  ][  scanConsole  ]: ... success!
[16:01:17:451][  info  ][  scanConsole  ]: Enabling Tx channels
[16:01:17:451][  info  ][  scanConsole  ]: Enabling Tx channel 1
[16:01:17:451][  info  ][  scanConsole  ]: Enabling Tx channel 1
[16:01:17:451][  info  ][  scanConsole  ]: Enabling Tx channel 1
[16:01:17:451][  info  ][  scanConsole  ]: Enabling Tx channel 1
[16:01:17:451][  info  ][  scanConsole  ]: Enabling Rx channels
[16:01:17:451][  info  ][  scanConsole  ]: Enabling Rx channel 4
[16:01:17:451][  info  ][  scanConsole  ]: Enabling Rx channel 5
[16:01:17:451][  info  ][  scanConsole  ]: Enabling Rx channel 6
[16:01:17:451][  info  ][  scanConsole  ]: Enabling Rx channel 7
[16:01:17:451][  info  ][  scanConsole  ]: ##############
[16:01:17:451][  info  ][  scanConsole  ]: # Setup Scan #
[16:01:17:451][  info  ][  scanConsole  ]: ##############
[16:01:17:452][  info  ][  scanConsole  ]: Found Scan config, constructing scan ...
[16:01:17:452][  info  ][  ScanFactory  ]: Loading Scan:
[16:01:17:452][  info  ][  ScanFactory  ]:   Name: AnalogScan
[16:01:17:452][  info  ][  ScanFactory  ]:   Number of Loops: 3
[16:01:17:452][  info  ][  ScanFactory  ]:   Loading Loop #0
[16:01:17:452][  info  ][  ScanFactory  ]:    Type: Rd53aReadRegLoop
[16:01:17:452][  info  ][  ScanFactory  ]:    Loading loop config ... 
[16:01:17:453][  info  ][  ScanFactory  ]: ~~~ {
[16:01:17:453][  info  ][  ScanFactory  ]: ~~~     "CurMux": [],
[16:01:17:453][  info  ][  ScanFactory  ]: ~~~     "EnblRingOsc": 0,
[16:01:17:453][  info  ][  ScanFactory  ]: ~~~     "Registers": [],
[16:01:17:453][  info  ][  ScanFactory  ]: ~~~     "RingOscDur": 9,
[16:01:17:453][  info  ][  ScanFactory  ]: ~~~     "RingOscRep": 10,
[16:01:17:453][  info  ][  ScanFactory  ]: ~~~     "VoltMux": [3, 5, 7, 15]
[16:01:17:453][  info  ][  ScanFactory  ]: ~~~ }
[16:01:17:453][  info  ][  ScanFactory  ]:   Loading Loop #1
[16:01:17:453][  info  ][  ScanFactory  ]:    Type: Rd53aTriggerLoop
[16:01:17:453][  info  ][  ScanFactory  ]:    Loading loop config ... 
[16:01:17:453][  info  ][  ScanFactory  ]: ~~~ {
[16:01:17:453][  info  ][  ScanFactory  ]: ~~~     "count": 1,
[16:01:17:453][  info  ][  ScanFactory  ]: ~~~     "delay": 48,
[16:01:17:453][  info  ][  ScanFactory  ]: ~~~     "extTrig": false,
[16:01:17:453][  info  ][  ScanFactory  ]: ~~~     "frequency": 30000.0,
[16:01:17:453][  info  ][  ScanFactory  ]: ~~~     "noInject": false,
[16:01:17:453][  info  ][  ScanFactory  ]: ~~~     "sendEcr": false,
[16:01:17:453][  info  ][  ScanFactory  ]: ~~~     "time": 0.0,
[16:01:17:453][  info  ][  ScanFactory  ]: ~~~     "trigMultiplier": 16
[16:01:17:453][  info  ][  ScanFactory  ]: ~~~ }
[16:01:17:453][  info  ][  ScanFactory  ]:   Loading Loop #2
[16:01:17:453][  info  ][  ScanFactory  ]:    Type: StdDataLoop
[16:01:17:453][warning ][  ScanFactory  ]: ~~~ Config empty.
[16:01:17:453][  info  ][ScanBuildHistogrammers]: Loading histogrammer ...
[16:01:17:454][  info  ][ScanBuildHistogrammers]: ... done!
[16:01:17:454][  info  ][ScanBuildAnalyses]: Loading analyses ...
[16:01:17:454][  info  ][  scanConsole  ]: Running pre scan!
[16:01:17:454][  info  ][  ScanFactory  ]: Entering pre scan phase ...
[16:01:17:454][  info  ][     Rd53a     ]: Write named register: GlobalPulseRt -> 16384
[16:01:17:454][  info  ][     Rd53a     ]: Write named register: InjEnDig -> 0
[16:01:17:454][  info  ][     Rd53a     ]: Write named register: InjVcalDiff -> 0
[16:01:17:454][  info  ][     Rd53a     ]: Write named register: InjVcalHigh -> 3500
[16:01:17:454][  info  ][     Rd53a     ]: Write named register: InjVcalMed -> 500
[16:01:17:454][  info  ][     Rd53a     ]: Write named register: LatencyConfig -> 48
[16:01:17:454][  info  ][  scanConsole  ]: Starting histogrammer and analysis threads:
[16:01:17:454][  info  ][  scanConsole  ]:  .. started threads of Fe 4
[16:01:17:455][  info  ][  scanConsole  ]:  .. started threads of Fe 5
[16:01:17:455][  info  ][  scanConsole  ]:  .. started threads of Fe 6
[16:01:17:455][  info  ][  scanConsole  ]:  .. started threads of Fe 7
[16:01:17:455][  info  ][Rd53aDataProcessor]:   -> Processor thread #0 started!
[16:01:17:455][  info  ][Rd53aDataProcessor]:   -> Processor thread #1 started!
[16:01:17:455][  info  ][Rd53aDataProcessor]:   -> Processor thread #2 started!
[16:01:17:455][  info  ][Rd53aDataProcessor]:   -> Processor thread #3 started!
[16:01:17:455][  info  ][  scanConsole  ]: ########
[16:01:17:455][  info  ][  scanConsole  ]: # Scan #
[16:01:17:455][  info  ][  scanConsole  ]: ########
[16:01:17:455][  info  ][  scanConsole  ]: Starting scan!
[16:01:17:455][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0009364 on Rx 4
[16:01:17:460][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0009364] MON MUX_V: 3 Bias 0 1275 -> 0.279258 V Bias 1 1658 -> 0.355339 V, Temperature 0.0760812 -> -5.3293 C
[16:01:17:464][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0009364] MON MUX_V: 5 Bias 0 1244 -> 0.2731 V Bias 1 1629 -> 0.349578 V, Temperature 0.0764785 -> -15.7381 C
[16:01:17:468][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0009364] MON MUX_V: 7 Bias 0 1234 -> 0.271114 V Bias 1 1630 -> 0.349777 V, Temperature 0.0786636 -> -21.0159 C
[16:01:17:472][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0009364] MON MUX_V: 15 Bias 0 1229 -> 0.27012 V Bias 1 1625 -> 0.348784 V, Temperature 0.0786636 -> -17.362 C
[16:01:17:507][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0009253 on Rx 5
[16:01:17:511][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0009253] MON MUX_V: 3 Bias 0 1312 -> 0.284293 V Bias 1 1686 -> 0.35823 V, Temperature 0.0739372 -> -3.75999 C
[16:01:17:516][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0009253] MON MUX_V: 5 Bias 0 1265 -> 0.275001 V Bias 1 1652 -> 0.351509 V, Temperature 0.0765072 -> -22.3966 C
[16:01:17:520][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0009253] MON MUX_V: 7 Bias 0 1248 -> 0.271641 V Bias 1 1640 -> 0.349136 V, Temperature 0.0774957 -> -18.714 C
[16:01:17:524][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0009253] MON MUX_V: 15 Bias 0 1234 -> 0.268873 V Bias 1 1631 -> 0.347357 V, Temperature 0.0784841 -> -18.9855 C
[16:01:17:559][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0009285 on Rx 6
[16:01:17:563][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0009285] MON MUX_V: 3 Bias 0 1310 -> 0.282686 V Bias 1 1700 -> 0.360086 V, Temperature 0.0773998 -> -6.88391 C
[16:01:17:568][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0009285] MON MUX_V: 5 Bias 0 1272 -> 0.275144 V Bias 1 1661 -> 0.352346 V, Temperature 0.0772014 -> -6.68048 C
[16:01:17:572][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0009285] MON MUX_V: 7 Bias 0 1260 -> 0.272763 V Bias 1 1652 -> 0.350559 V, Temperature 0.0777968 -> -6.45764 C
[16:01:17:576][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0009285] MON MUX_V: 15 Bias 0 1253 -> 0.271373 V Bias 1 1646 -> 0.349369 V, Temperature 0.0779952 -> -5.0943 C
[16:01:17:611][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0009269 on Rx 7
[16:01:17:616][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0009269] MON MUX_V: 3 Bias 0 1313 -> 0.283523 V Bias 1 1695 -> 0.359356 V, Temperature 0.0758334 -> -1.46025 C
[16:01:17:620][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0009269] MON MUX_V: 5 Bias 0 1276 -> 0.276178 V Bias 1 1668 -> 0.353996 V, Temperature 0.0778185 -> -12.5807 C
[16:01:17:624][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0009269] MON MUX_V: 7 Bias 0 1248 -> 0.270619 V Bias 1 1649 -> 0.350224 V, Temperature 0.0796052 -> -14.8811 C
[16:01:17:629][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0009269] MON MUX_V: 15 Bias 0 1270 -> 0.274987 V Bias 1 1663 -> 0.353004 V, Temperature 0.0780171 -> -11.9344 C
[16:01:17:665][  info  ][  scanConsole  ]: Scan done!
[16:01:17:665][  info  ][  scanConsole  ]: Waiting for processors to finish ...
[16:01:17:666][  info  ][  scanConsole  ]: Processor done, waiting for histogrammer ...
[16:01:18:065][  info  ][HistogramAlgorithm]: Histogrammer done!
[16:01:18:065][  info  ][HistogramAlgorithm]: Histogrammer done!
[16:01:18:065][  info  ][HistogramAlgorithm]: Histogrammer done!
[16:01:18:065][  info  ][HistogramAlgorithm]: Histogrammer done!
[16:01:18:066][  info  ][  scanConsole  ]: Processor done, waiting for analysis ...
[16:01:18:466][  info  ][AnalysisAlgorithm]: Analysis done!
[16:01:18:466][  info  ][AnalysisAlgorithm]: Analysis done!
[16:01:18:466][  info  ][AnalysisAlgorithm]: Analysis done!
[16:01:18:466][  info  ][AnalysisAlgorithm]: Analysis done!
[16:01:18:467][  info  ][  scanConsole  ]: All done!
[16:01:18:467][  info  ][  scanConsole  ]: ##########
[16:01:18:467][  info  ][  scanConsole  ]: # Timing #
[16:01:18:467][  info  ][  scanConsole  ]: ##########
[16:01:18:467][  info  ][  scanConsole  ]: -> Configuration: 128 ms
[16:01:18:467][  info  ][  scanConsole  ]: -> Scan:          209 ms
[16:01:18:467][  info  ][  scanConsole  ]: -> Processing:    0 ms
[16:01:18:467][  info  ][  scanConsole  ]: -> Analysis:      800 ms
[16:01:18:467][  info  ][  scanConsole  ]: ###########
[16:01:18:467][  info  ][  scanConsole  ]: # Cleanup #
[16:01:18:467][  info  ][  scanConsole  ]: ###########
[16:01:18:475][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0009364 to configs/KEKQ18/20UPGFC0009364.json
[16:01:18:656][  info  ][  scanConsole  ]: -> Storing output results of FE 4
[16:01:18:656][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0009364, this usually means that the chip did not send any data at all.
[16:01:18:657][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0009253 to configs/KEKQ18/20UPGFC0009253.json
[16:01:18:825][  info  ][  scanConsole  ]: -> Storing output results of FE 5
[16:01:18:825][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0009253, this usually means that the chip did not send any data at all.
[16:01:18:827][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0009285 to configs/KEKQ18/20UPGFC0009285.json
[16:01:19:008][  info  ][  scanConsole  ]: -> Storing output results of FE 6
[16:01:19:008][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0009285, this usually means that the chip did not send any data at all.
[16:01:19:010][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0009269 to configs/KEKQ18/20UPGFC0009269.json
[16:01:19:193][  info  ][  scanConsole  ]: -> Storing output results of FE 7
[16:01:19:193][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0009269, this usually means that the chip did not send any data at all.
[16:01:19:195][  info  ][  scanConsole  ]: Finishing run: 1856
20UPGFC0009253.json.after
20UPGFC0009253.json.before
20UPGFC0009269.json.after
20UPGFC0009269.json.before
20UPGFC0009285.json.after
20UPGFC0009285.json.before
20UPGFC0009364.json.after
20UPGFC0009364.json.before
reg_readtemp.json
scanLog.json

