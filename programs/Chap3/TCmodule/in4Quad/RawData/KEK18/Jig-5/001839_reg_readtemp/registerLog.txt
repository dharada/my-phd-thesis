[14:42:49:179][  info  ][               ]: #####################################
[14:42:49:179][  info  ][               ]: # Welcome to the YARR Scan Console! #
[14:42:49:179][  info  ][               ]: #####################################
[14:42:49:179][  info  ][               ]: -> Parsing command line parameters ...
[14:42:49:179][  info  ][               ]: Configuring logger ...
[14:42:49:181][  info  ][  scanConsole  ]: Scan Type/Config configs/scans/rd53a/reg_readtemp.json
[14:42:49:181][  info  ][  scanConsole  ]: Connectivity:
[14:42:49:181][  info  ][  scanConsole  ]:     configs/KEKQ18/connectivity.json
[14:42:49:181][  info  ][  scanConsole  ]: Target ToT: -1
[14:42:49:181][  info  ][  scanConsole  ]: Target Charge: -1
[14:42:49:181][  info  ][  scanConsole  ]: Output Plots: true
[14:42:49:181][  info  ][  scanConsole  ]: Output Directory: ./data/001839_reg_readtemp/
[14:42:49:185][  info  ][  scanConsole  ]: Timestamp: 2022-08-26_14:42:49
[14:42:49:185][  info  ][  scanConsole  ]: Run Number: 1839
[14:42:49:185][  info  ][  scanConsole  ]: #################
[14:42:49:185][  info  ][  scanConsole  ]: # Init Hardware #
[14:42:49:185][  info  ][  scanConsole  ]: #################
[14:42:49:185][  info  ][  scanConsole  ]: -> Opening controller config: configs/controller/specCfg-rd53a-16x1.json
[14:42:49:185][  info  ][  ScanHelper   ]: Loading controller ...
[14:42:49:185][  info  ][  ScanHelper   ]: Found controller of type: spec
[14:42:49:185][  info  ][  ScanHelper   ]: ... loading controler config:
[14:42:49:185][  info  ][  ScanHelper   ]: ~~~ {
[14:42:49:185][  info  ][  ScanHelper   ]: ~~~     "cmdPeriod": 6.250000073038109e-9,
[14:42:49:185][  info  ][  ScanHelper   ]: ~~~     "idle": {
[14:42:49:185][  info  ][  ScanHelper   ]: ~~~         "word": 1768515945
[14:42:49:186][  info  ][  ScanHelper   ]: ~~~     },
[14:42:49:186][  info  ][  ScanHelper   ]: ~~~     "pulse": {
[14:42:49:186][  info  ][  ScanHelper   ]: ~~~         "interval": 500,
[14:42:49:186][  info  ][  ScanHelper   ]: ~~~         "word": 1549575846
[14:42:49:186][  info  ][  ScanHelper   ]: ~~~     },
[14:42:49:186][  info  ][  ScanHelper   ]: ~~~     "rxActiveLanes": 1,
[14:42:49:186][  info  ][  ScanHelper   ]: ~~~     "rxPolarity": 0,
[14:42:49:186][  info  ][  ScanHelper   ]: ~~~     "specNum": 0,
[14:42:49:186][  info  ][  ScanHelper   ]: ~~~     "spiConfig": 541200,
[14:42:49:186][  info  ][  ScanHelper   ]: ~~~     "sync": {
[14:42:49:186][  info  ][  ScanHelper   ]: ~~~         "interval": 32,
[14:42:49:186][  info  ][  ScanHelper   ]: ~~~         "word": 2172551550
[14:42:49:186][  info  ][  ScanHelper   ]: ~~~     },
[14:42:49:186][  info  ][  ScanHelper   ]: ~~~     "txPolarity": 0
[14:42:49:186][  info  ][  ScanHelper   ]: ~~~ }
[14:42:49:186][  info  ][    SpecCom    ]: Opening SPEC with id #0
[14:42:49:186][  info  ][    SpecCom    ]: Mapping BARs ...
[14:42:49:186][  info  ][    SpecCom    ]: ... Mapped BAR0 at 0x7f464c79a000 with size 1048576
[14:42:49:186][warning ][    SpecCom    ]: ... BAR4 not mapped (Mmap failed)
[14:42:49:186][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[14:42:49:186][  info  ][    SpecCom    ]: Firmware Version: 0x4d9ff6d
[14:42:49:186][  info  ][    SpecCom    ]: Firmware Identifier: 0x4030232
[14:42:49:186][  info  ][    SpecCom    ]: FPGA card: PLDA XpressK7 325
[14:42:49:186][  info  ][    SpecCom    ]: FE Chip Type: RD53A/B
[14:42:49:186][  info  ][    SpecCom    ]: FMC Card Type: Ohio Card (Display Port)
[14:42:49:186][  info  ][    SpecCom    ]: RX Speed: 640Mbps
[14:42:49:186][  info  ][    SpecCom    ]: Channel Configuration: 16x1
[14:42:49:186][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[14:42:49:186][  info  ][    SpecCom    ]: Flushing buffers ...
[14:42:49:186][  info  ][    SpecCom    ]: Init success!
[14:42:49:186][  info  ][  scanConsole  ]: #######################
[14:42:49:186][  info  ][  scanConsole  ]: ##  Loading Configs  ##
[14:42:49:186][  info  ][  scanConsole  ]: #######################
[14:42:49:186][  info  ][  scanConsole  ]: Opening global config: configs/KEKQ18/connectivity.json
[14:42:49:186][  info  ][  ScanHelper   ]: Chip type: RD53A
[14:42:49:186][  info  ][  ScanHelper   ]: Chip count 4
[14:42:49:186][  info  ][  ScanHelper   ]: Loading chip #0
[14:42:49:187][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(4)
[14:42:49:187][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ18/20UPGFC0009364.json
[14:42:49:365][  info  ][  ScanHelper   ]: Loading chip #1
[14:42:49:365][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(5)
[14:42:49:365][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ18/20UPGFC0009253.json
[14:42:49:538][  info  ][  ScanHelper   ]: Loading chip #2
[14:42:49:539][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(6)
[14:42:49:539][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ18/20UPGFC0009285.json
[14:42:49:714][  info  ][  ScanHelper   ]: Loading chip #3
[14:42:49:714][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(7)
[14:42:49:714][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ18/20UPGFC0009269.json
[14:42:49:899][  info  ][  scanConsole  ]: #################
[14:42:49:899][  info  ][  scanConsole  ]: # Configure FEs #
[14:42:49:899][  info  ][  scanConsole  ]: #################
[14:42:49:899][  info  ][  scanConsole  ]: Configuring 20UPGFC0009364
[14:42:49:930][  info  ][  scanConsole  ]: Configuring 20UPGFC0009253
[14:42:49:974][  info  ][  scanConsole  ]: Configuring 20UPGFC0009285
[14:42:50:005][  info  ][  scanConsole  ]: Configuring 20UPGFC0009269
[14:42:50:047][  info  ][  scanConsole  ]: Sent configuration to all FEs in 148 ms!
[14:42:50:048][  info  ][  scanConsole  ]: Checking com 20UPGFC0009364
[14:42:50:049][  info  ][    SpecRx     ]: Active Rx channels: 0x10
[14:42:50:049][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[14:42:50:049][  info  ][    SpecRx     ]: Rx Status 0xf0
[14:42:50:049][  info  ][    SpecRx     ]: Number of lanes: 1
[14:42:50:049][  info  ][    SpecRx     ]: Channel 4 Lane 0 synchronized!
[14:42:50:059][  info  ][  scanConsole  ]: ... success!
[14:42:50:059][  info  ][  scanConsole  ]: Checking com 20UPGFC0009253
[14:42:50:059][  info  ][    SpecRx     ]: Active Rx channels: 0x20
[14:42:50:059][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[14:42:50:059][  info  ][    SpecRx     ]: Rx Status 0xf0
[14:42:50:059][  info  ][    SpecRx     ]: Number of lanes: 1
[14:42:50:059][  info  ][    SpecRx     ]: Channel 5 Lane 0 synchronized!
[14:42:50:070][  info  ][  scanConsole  ]: ... success!
[14:42:50:070][  info  ][  scanConsole  ]: Checking com 20UPGFC0009285
[14:42:50:070][  info  ][    SpecRx     ]: Active Rx channels: 0x40
[14:42:50:070][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[14:42:50:070][  info  ][    SpecRx     ]: Rx Status 0xf0
[14:42:50:070][  info  ][    SpecRx     ]: Number of lanes: 1
[14:42:50:070][  info  ][    SpecRx     ]: Channel 6 Lane 0 synchronized!
[14:42:50:080][  info  ][  scanConsole  ]: ... success!
[14:42:50:080][  info  ][  scanConsole  ]: Checking com 20UPGFC0009269
[14:42:50:080][  info  ][    SpecRx     ]: Active Rx channels: 0x80
[14:42:50:080][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[14:42:50:080][  info  ][    SpecRx     ]: Rx Status 0xf0
[14:42:50:080][  info  ][    SpecRx     ]: Number of lanes: 1
[14:42:50:080][  info  ][    SpecRx     ]: Channel 7 Lane 0 synchronized!
[14:42:50:091][  info  ][  scanConsole  ]: ... success!
[14:42:50:091][  info  ][  scanConsole  ]: Enabling Tx channels
[14:42:50:091][  info  ][  scanConsole  ]: Enabling Tx channel 1
[14:42:50:091][  info  ][  scanConsole  ]: Enabling Tx channel 1
[14:42:50:091][  info  ][  scanConsole  ]: Enabling Tx channel 1
[14:42:50:091][  info  ][  scanConsole  ]: Enabling Tx channel 1
[14:42:50:091][  info  ][  scanConsole  ]: Enabling Rx channels
[14:42:50:091][  info  ][  scanConsole  ]: Enabling Rx channel 4
[14:42:50:091][  info  ][  scanConsole  ]: Enabling Rx channel 5
[14:42:50:091][  info  ][  scanConsole  ]: Enabling Rx channel 6
[14:42:50:091][  info  ][  scanConsole  ]: Enabling Rx channel 7
[14:42:50:091][  info  ][  scanConsole  ]: ##############
[14:42:50:091][  info  ][  scanConsole  ]: # Setup Scan #
[14:42:50:091][  info  ][  scanConsole  ]: ##############
[14:42:50:092][  info  ][  scanConsole  ]: Found Scan config, constructing scan ...
[14:42:50:092][  info  ][  ScanFactory  ]: Loading Scan:
[14:42:50:092][  info  ][  ScanFactory  ]:   Name: AnalogScan
[14:42:50:092][  info  ][  ScanFactory  ]:   Number of Loops: 3
[14:42:50:092][  info  ][  ScanFactory  ]:   Loading Loop #0
[14:42:50:092][  info  ][  ScanFactory  ]:    Type: Rd53aReadRegLoop
[14:42:50:092][  info  ][  ScanFactory  ]:    Loading loop config ... 
[14:42:50:092][  info  ][  ScanFactory  ]: ~~~ {
[14:42:50:092][  info  ][  ScanFactory  ]: ~~~     "CurMux": [],
[14:42:50:092][  info  ][  ScanFactory  ]: ~~~     "EnblRingOsc": 0,
[14:42:50:092][  info  ][  ScanFactory  ]: ~~~     "Registers": [],
[14:42:50:092][  info  ][  ScanFactory  ]: ~~~     "RingOscDur": 9,
[14:42:50:092][  info  ][  ScanFactory  ]: ~~~     "RingOscRep": 10,
[14:42:50:092][  info  ][  ScanFactory  ]: ~~~     "VoltMux": [3, 5, 7, 15]
[14:42:50:093][  info  ][  ScanFactory  ]: ~~~ }
[14:42:50:093][  info  ][  ScanFactory  ]:   Loading Loop #1
[14:42:50:093][  info  ][  ScanFactory  ]:    Type: Rd53aTriggerLoop
[14:42:50:093][  info  ][  ScanFactory  ]:    Loading loop config ... 
[14:42:50:093][  info  ][  ScanFactory  ]: ~~~ {
[14:42:50:093][  info  ][  ScanFactory  ]: ~~~     "count": 1,
[14:42:50:093][  info  ][  ScanFactory  ]: ~~~     "delay": 48,
[14:42:50:093][  info  ][  ScanFactory  ]: ~~~     "extTrig": false,
[14:42:50:093][  info  ][  ScanFactory  ]: ~~~     "frequency": 30000.0,
[14:42:50:093][  info  ][  ScanFactory  ]: ~~~     "noInject": false,
[14:42:50:093][  info  ][  ScanFactory  ]: ~~~     "sendEcr": false,
[14:42:50:093][  info  ][  ScanFactory  ]: ~~~     "time": 0.0,
[14:42:50:093][  info  ][  ScanFactory  ]: ~~~     "trigMultiplier": 16
[14:42:50:093][  info  ][  ScanFactory  ]: ~~~ }
[14:42:50:093][  info  ][  ScanFactory  ]:   Loading Loop #2
[14:42:50:093][  info  ][  ScanFactory  ]:    Type: StdDataLoop
[14:42:50:093][warning ][  ScanFactory  ]: ~~~ Config empty.
[14:42:50:093][  info  ][ScanBuildHistogrammers]: Loading histogrammer ...
[14:42:50:093][  info  ][ScanBuildHistogrammers]: ... done!
[14:42:50:093][  info  ][ScanBuildAnalyses]: Loading analyses ...
[14:42:50:094][  info  ][  scanConsole  ]: Running pre scan!
[14:42:50:094][  info  ][  ScanFactory  ]: Entering pre scan phase ...
[14:42:50:094][  info  ][     Rd53a     ]: Write named register: GlobalPulseRt -> 16384
[14:42:50:094][  info  ][     Rd53a     ]: Write named register: InjEnDig -> 0
[14:42:50:094][  info  ][     Rd53a     ]: Write named register: InjVcalDiff -> 0
[14:42:50:094][  info  ][     Rd53a     ]: Write named register: InjVcalHigh -> 3500
[14:42:50:094][  info  ][     Rd53a     ]: Write named register: InjVcalMed -> 500
[14:42:50:094][  info  ][     Rd53a     ]: Write named register: LatencyConfig -> 48
[14:42:50:094][  info  ][  scanConsole  ]: Starting histogrammer and analysis threads:
[14:42:50:094][  info  ][  scanConsole  ]:  .. started threads of Fe 4
[14:42:50:094][  info  ][  scanConsole  ]:  .. started threads of Fe 5
[14:42:50:094][  info  ][  scanConsole  ]:  .. started threads of Fe 6
[14:42:50:095][  info  ][  scanConsole  ]:  .. started threads of Fe 7
[14:42:50:095][  info  ][Rd53aDataProcessor]:   -> Processor thread #0 started!
[14:42:50:095][  info  ][Rd53aDataProcessor]:   -> Processor thread #1 started!
[14:42:50:095][  info  ][Rd53aDataProcessor]:   -> Processor thread #2 started!
[14:42:50:095][  info  ][Rd53aDataProcessor]:   -> Processor thread #3 started!
[14:42:50:095][  info  ][  scanConsole  ]: ########
[14:42:50:095][  info  ][  scanConsole  ]: # Scan #
[14:42:50:095][  info  ][  scanConsole  ]: ########
[14:42:50:095][  info  ][  scanConsole  ]: Starting scan!
[14:42:50:096][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0009364 on Rx 4
[14:42:50:100][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0009364] MON MUX_V: 3 Bias 0 1216 -> 0.267538 V Bias 1 1614 -> 0.346599 V, Temperature 0.0790609 -> 4.07932 C
[14:42:50:104][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0009364] MON MUX_V: 5 Bias 0 1181 -> 0.260585 V Bias 1 1582 -> 0.340242 V, Temperature 0.0796567 -> -2.75058 C
[14:42:50:108][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0009364] MON MUX_V: 7 Bias 0 1170 -> 0.2584 V Bias 1 1583 -> 0.340441 V, Temperature 0.0820405 -> -6.35397 C
[14:42:50:112][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0009364] MON MUX_V: 15 Bias 0 1164 -> 0.257208 V Bias 1 1576 -> 0.33905 V, Temperature 0.0818419 -> -4.37439 C
[14:42:50:147][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0009253 on Rx 5
[14:42:50:152][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0009253] MON MUX_V: 3 Bias 0 1253 -> 0.272629 V Bias 1 1642 -> 0.349532 V, Temperature 0.0769026 -> 4.86459 C
[14:42:50:156][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0009253] MON MUX_V: 5 Bias 0 1204 -> 0.262942 V Bias 1 1608 -> 0.34281 V, Temperature 0.079868 -> -6.75732 C
[14:42:50:160][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0009253] MON MUX_V: 7 Bias 0 1186 -> 0.259384 V Bias 1 1593 -> 0.339845 V, Temperature 0.0804611 -> -7.21457 C
[14:42:50:164][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0009253] MON MUX_V: 15 Bias 0 1174 -> 0.257011 V Bias 1 1588 -> 0.338856 V, Temperature 0.0818449 -> -5.18631 C
[14:42:50:199][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0009285 on Rx 6
[14:42:50:203][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0009285] MON MUX_V: 3 Bias 0 1249 -> 0.27058 V Bias 1 1652 -> 0.350559 V, Temperature 0.0799798 -> 2.15326 C
[14:42:50:207][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0009285] MON MUX_V: 5 Bias 0 1208 -> 0.262443 V Bias 1 1612 -> 0.342621 V, Temperature 0.0801783 -> 3.4848 C
[14:42:50:211][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0009285] MON MUX_V: 7 Bias 0 1197 -> 0.26026 V Bias 1 1604 -> 0.341033 V, Temperature 0.0807736 -> 3.67252 C
[14:42:50:216][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0009285] MON MUX_V: 15 Bias 0 1189 -> 0.258672 V Bias 1 1597 -> 0.339644 V, Temperature 0.0809721 -> 4.9173 C
[14:42:50:250][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0009269 on Rx 7
[14:42:50:254][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0009269] MON MUX_V: 3 Bias 0 1253 -> 0.271612 V Bias 1 1650 -> 0.350423 V, Temperature 0.0788111 -> 7.16429 C
[14:42:50:259][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0009269] MON MUX_V: 5 Bias 0 1215 -> 0.264068 V Bias 1 1621 -> 0.344666 V, Temperature 0.0805977 -> -1.84805 C
[14:42:50:263][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0009269] MON MUX_V: 7 Bias 0 1185 -> 0.258113 V Bias 1 1602 -> 0.340894 V, Temperature 0.0827814 -> -2.61499 C
[14:42:50:267][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0009269] MON MUX_V: 15 Bias 0 1208 -> 0.262679 V Bias 1 1615 -> 0.343475 V, Temperature 0.0807963 -> -1.76651 C
[14:42:50:303][  info  ][  scanConsole  ]: Scan done!
[14:42:50:303][  info  ][  scanConsole  ]: Waiting for processors to finish ...
[14:42:50:304][  info  ][  scanConsole  ]: Processor done, waiting for histogrammer ...
[14:42:50:704][  info  ][HistogramAlgorithm]: Histogrammer done!
[14:42:50:704][  info  ][HistogramAlgorithm]: Histogrammer done!
[14:42:50:704][  info  ][HistogramAlgorithm]: Histogrammer done!
[14:42:50:704][  info  ][HistogramAlgorithm]: Histogrammer done!
[14:42:50:704][  info  ][  scanConsole  ]: Processor done, waiting for analysis ...
[14:42:51:104][  info  ][AnalysisAlgorithm]: Analysis done!
[14:42:51:105][  info  ][AnalysisAlgorithm]: Analysis done!
[14:42:51:104][  info  ][AnalysisAlgorithm]: Analysis done!
[14:42:51:105][  info  ][AnalysisAlgorithm]: Analysis done!
[14:42:51:105][  info  ][  scanConsole  ]: All done!
[14:42:51:105][  info  ][  scanConsole  ]: ##########
[14:42:51:105][  info  ][  scanConsole  ]: # Timing #
[14:42:51:105][  info  ][  scanConsole  ]: ##########
[14:42:51:105][  info  ][  scanConsole  ]: -> Configuration: 148 ms
[14:42:51:105][  info  ][  scanConsole  ]: -> Scan:          207 ms
[14:42:51:105][  info  ][  scanConsole  ]: -> Processing:    1 ms
[14:42:51:105][  info  ][  scanConsole  ]: -> Analysis:      800 ms
[14:42:51:105][  info  ][  scanConsole  ]: ###########
[14:42:51:105][  info  ][  scanConsole  ]: # Cleanup #
[14:42:51:105][  info  ][  scanConsole  ]: ###########
[14:42:51:113][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0009364 to configs/KEKQ18/20UPGFC0009364.json
[14:42:51:288][  info  ][  scanConsole  ]: -> Storing output results of FE 4
[14:42:51:288][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0009364, this usually means that the chip did not send any data at all.
[14:42:51:289][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0009253 to configs/KEKQ18/20UPGFC0009253.json
[14:42:51:452][  info  ][  scanConsole  ]: -> Storing output results of FE 5
[14:42:51:452][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0009253, this usually means that the chip did not send any data at all.
[14:42:51:453][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0009285 to configs/KEKQ18/20UPGFC0009285.json
[14:42:51:618][  info  ][  scanConsole  ]: -> Storing output results of FE 6
[14:42:51:618][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0009285, this usually means that the chip did not send any data at all.
[14:42:51:620][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0009269 to configs/KEKQ18/20UPGFC0009269.json
[14:42:51:786][  info  ][  scanConsole  ]: -> Storing output results of FE 7
[14:42:51:786][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0009269, this usually means that the chip did not send any data at all.
[14:42:51:787][  info  ][  scanConsole  ]: Finishing run: 1839
20UPGFC0009253.json.after
20UPGFC0009253.json.before
20UPGFC0009269.json.after
20UPGFC0009269.json.before
20UPGFC0009285.json.after
20UPGFC0009285.json.before
20UPGFC0009364.json.after
20UPGFC0009364.json.before
reg_readtemp.json
scanLog.json

