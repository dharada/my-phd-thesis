[14:47:45:360][  info  ][               ]: #####################################
[14:47:45:360][  info  ][               ]: # Welcome to the YARR Scan Console! #
[14:47:45:360][  info  ][               ]: #####################################
[14:47:45:360][  info  ][               ]: -> Parsing command line parameters ...
[14:47:45:360][  info  ][               ]: Configuring logger ...
[14:47:45:364][  info  ][  scanConsole  ]: Scan Type/Config configs/scans/rd53a/reg_readtemp.json
[14:47:45:364][  info  ][  scanConsole  ]: Connectivity:
[14:47:45:364][  info  ][  scanConsole  ]:     configs/KEKQ18/connectivity.json
[14:47:45:364][  info  ][  scanConsole  ]: Target ToT: -1
[14:47:45:364][  info  ][  scanConsole  ]: Target Charge: -1
[14:47:45:364][  info  ][  scanConsole  ]: Output Plots: true
[14:47:45:364][  info  ][  scanConsole  ]: Output Directory: ./data/001841_reg_readtemp/
[14:47:45:369][  info  ][  scanConsole  ]: Timestamp: 2022-08-26_14:47:45
[14:47:45:369][  info  ][  scanConsole  ]: Run Number: 1841
[14:47:45:369][  info  ][  scanConsole  ]: #################
[14:47:45:369][  info  ][  scanConsole  ]: # Init Hardware #
[14:47:45:369][  info  ][  scanConsole  ]: #################
[14:47:45:369][  info  ][  scanConsole  ]: -> Opening controller config: configs/controller/specCfg-rd53a-16x1.json
[14:47:45:369][  info  ][  ScanHelper   ]: Loading controller ...
[14:47:45:369][  info  ][  ScanHelper   ]: Found controller of type: spec
[14:47:45:369][  info  ][  ScanHelper   ]: ... loading controler config:
[14:47:45:369][  info  ][  ScanHelper   ]: ~~~ {
[14:47:45:369][  info  ][  ScanHelper   ]: ~~~     "cmdPeriod": 6.250000073038109e-9,
[14:47:45:369][  info  ][  ScanHelper   ]: ~~~     "idle": {
[14:47:45:369][  info  ][  ScanHelper   ]: ~~~         "word": 1768515945
[14:47:45:369][  info  ][  ScanHelper   ]: ~~~     },
[14:47:45:369][  info  ][  ScanHelper   ]: ~~~     "pulse": {
[14:47:45:369][  info  ][  ScanHelper   ]: ~~~         "interval": 500,
[14:47:45:369][  info  ][  ScanHelper   ]: ~~~         "word": 1549575846
[14:47:45:369][  info  ][  ScanHelper   ]: ~~~     },
[14:47:45:369][  info  ][  ScanHelper   ]: ~~~     "rxActiveLanes": 1,
[14:47:45:369][  info  ][  ScanHelper   ]: ~~~     "rxPolarity": 0,
[14:47:45:369][  info  ][  ScanHelper   ]: ~~~     "specNum": 0,
[14:47:45:369][  info  ][  ScanHelper   ]: ~~~     "spiConfig": 541200,
[14:47:45:369][  info  ][  ScanHelper   ]: ~~~     "sync": {
[14:47:45:369][  info  ][  ScanHelper   ]: ~~~         "interval": 32,
[14:47:45:369][  info  ][  ScanHelper   ]: ~~~         "word": 2172551550
[14:47:45:369][  info  ][  ScanHelper   ]: ~~~     },
[14:47:45:369][  info  ][  ScanHelper   ]: ~~~     "txPolarity": 0
[14:47:45:369][  info  ][  ScanHelper   ]: ~~~ }
[14:47:45:369][  info  ][    SpecCom    ]: Opening SPEC with id #0
[14:47:45:369][  info  ][    SpecCom    ]: Mapping BARs ...
[14:47:45:369][  info  ][    SpecCom    ]: ... Mapped BAR0 at 0x7fa114325000 with size 1048576
[14:47:45:369][warning ][    SpecCom    ]: ... BAR4 not mapped (Mmap failed)
[14:47:45:369][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[14:47:45:369][  info  ][    SpecCom    ]: Firmware Version: 0x4d9ff6d
[14:47:45:369][  info  ][    SpecCom    ]: Firmware Identifier: 0x4030232
[14:47:45:369][  info  ][    SpecCom    ]: FPGA card: PLDA XpressK7 325
[14:47:45:369][  info  ][    SpecCom    ]: FE Chip Type: RD53A/B
[14:47:45:369][  info  ][    SpecCom    ]: FMC Card Type: Ohio Card (Display Port)
[14:47:45:369][  info  ][    SpecCom    ]: RX Speed: 640Mbps
[14:47:45:369][  info  ][    SpecCom    ]: Channel Configuration: 16x1
[14:47:45:369][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[14:47:45:369][  info  ][    SpecCom    ]: Flushing buffers ...
[14:47:45:369][  info  ][    SpecCom    ]: Init success!
[14:47:45:369][  info  ][  scanConsole  ]: #######################
[14:47:45:369][  info  ][  scanConsole  ]: ##  Loading Configs  ##
[14:47:45:369][  info  ][  scanConsole  ]: #######################
[14:47:45:369][  info  ][  scanConsole  ]: Opening global config: configs/KEKQ18/connectivity.json
[14:47:45:369][  info  ][  ScanHelper   ]: Chip type: RD53A
[14:47:45:369][  info  ][  ScanHelper   ]: Chip count 4
[14:47:45:369][  info  ][  ScanHelper   ]: Loading chip #0
[14:47:45:370][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(4)
[14:47:45:370][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ18/20UPGFC0009364.json
[14:47:45:546][  info  ][  ScanHelper   ]: Loading chip #1
[14:47:45:546][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(5)
[14:47:45:546][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ18/20UPGFC0009253.json
[14:47:45:718][  info  ][  ScanHelper   ]: Loading chip #2
[14:47:45:719][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(6)
[14:47:45:719][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ18/20UPGFC0009285.json
[14:47:45:891][  info  ][  ScanHelper   ]: Loading chip #3
[14:47:45:891][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(7)
[14:47:45:891][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ18/20UPGFC0009269.json
[14:47:46:066][  info  ][  scanConsole  ]: #################
[14:47:46:066][  info  ][  scanConsole  ]: # Configure FEs #
[14:47:46:066][  info  ][  scanConsole  ]: #################
[14:47:46:066][  info  ][  scanConsole  ]: Configuring 20UPGFC0009364
[14:47:46:097][  info  ][  scanConsole  ]: Configuring 20UPGFC0009253
[14:47:46:127][  info  ][  scanConsole  ]: Configuring 20UPGFC0009285
[14:47:46:158][  info  ][  scanConsole  ]: Configuring 20UPGFC0009269
[14:47:46:188][  info  ][  scanConsole  ]: Sent configuration to all FEs in 122 ms!
[14:47:46:189][  info  ][  scanConsole  ]: Checking com 20UPGFC0009364
[14:47:46:189][  info  ][    SpecRx     ]: Active Rx channels: 0x10
[14:47:46:189][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[14:47:46:189][  info  ][    SpecRx     ]: Rx Status 0xf0
[14:47:46:189][  info  ][    SpecRx     ]: Number of lanes: 1
[14:47:46:189][  info  ][    SpecRx     ]: Channel 4 Lane 0 synchronized!
[14:47:46:199][  info  ][  scanConsole  ]: ... success!
[14:47:46:199][  info  ][  scanConsole  ]: Checking com 20UPGFC0009253
[14:47:46:199][  info  ][    SpecRx     ]: Active Rx channels: 0x20
[14:47:46:199][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[14:47:46:199][  info  ][    SpecRx     ]: Rx Status 0xf0
[14:47:46:199][  info  ][    SpecRx     ]: Number of lanes: 1
[14:47:46:199][  info  ][    SpecRx     ]: Channel 5 Lane 0 synchronized!
[14:47:46:210][  info  ][  scanConsole  ]: ... success!
[14:47:46:210][  info  ][  scanConsole  ]: Checking com 20UPGFC0009285
[14:47:46:210][  info  ][    SpecRx     ]: Active Rx channels: 0x40
[14:47:46:210][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[14:47:46:210][  info  ][    SpecRx     ]: Rx Status 0xf0
[14:47:46:210][  info  ][    SpecRx     ]: Number of lanes: 1
[14:47:46:210][  info  ][    SpecRx     ]: Channel 6 Lane 0 synchronized!
[14:47:46:220][  info  ][  scanConsole  ]: ... success!
[14:47:46:220][  info  ][  scanConsole  ]: Checking com 20UPGFC0009269
[14:47:46:220][  info  ][    SpecRx     ]: Active Rx channels: 0x80
[14:47:46:220][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[14:47:46:220][  info  ][    SpecRx     ]: Rx Status 0xf0
[14:47:46:220][  info  ][    SpecRx     ]: Number of lanes: 1
[14:47:46:220][  info  ][    SpecRx     ]: Channel 7 Lane 0 synchronized!
[14:47:46:230][  info  ][  scanConsole  ]: ... success!
[14:47:46:230][  info  ][  scanConsole  ]: Enabling Tx channels
[14:47:46:230][  info  ][  scanConsole  ]: Enabling Tx channel 1
[14:47:46:230][  info  ][  scanConsole  ]: Enabling Tx channel 1
[14:47:46:230][  info  ][  scanConsole  ]: Enabling Tx channel 1
[14:47:46:230][  info  ][  scanConsole  ]: Enabling Tx channel 1
[14:47:46:230][  info  ][  scanConsole  ]: Enabling Rx channels
[14:47:46:230][  info  ][  scanConsole  ]: Enabling Rx channel 4
[14:47:46:230][  info  ][  scanConsole  ]: Enabling Rx channel 5
[14:47:46:230][  info  ][  scanConsole  ]: Enabling Rx channel 6
[14:47:46:230][  info  ][  scanConsole  ]: Enabling Rx channel 7
[14:47:46:230][  info  ][  scanConsole  ]: ##############
[14:47:46:230][  info  ][  scanConsole  ]: # Setup Scan #
[14:47:46:230][  info  ][  scanConsole  ]: ##############
[14:47:46:231][  info  ][  scanConsole  ]: Found Scan config, constructing scan ...
[14:47:46:231][  info  ][  ScanFactory  ]: Loading Scan:
[14:47:46:231][  info  ][  ScanFactory  ]:   Name: AnalogScan
[14:47:46:231][  info  ][  ScanFactory  ]:   Number of Loops: 3
[14:47:46:231][  info  ][  ScanFactory  ]:   Loading Loop #0
[14:47:46:231][  info  ][  ScanFactory  ]:    Type: Rd53aReadRegLoop
[14:47:46:231][  info  ][  ScanFactory  ]:    Loading loop config ... 
[14:47:46:231][  info  ][  ScanFactory  ]: ~~~ {
[14:47:46:231][  info  ][  ScanFactory  ]: ~~~     "CurMux": [],
[14:47:46:231][  info  ][  ScanFactory  ]: ~~~     "EnblRingOsc": 0,
[14:47:46:231][  info  ][  ScanFactory  ]: ~~~     "Registers": [],
[14:47:46:231][  info  ][  ScanFactory  ]: ~~~     "RingOscDur": 9,
[14:47:46:231][  info  ][  ScanFactory  ]: ~~~     "RingOscRep": 10,
[14:47:46:231][  info  ][  ScanFactory  ]: ~~~     "VoltMux": [3, 5, 7, 15]
[14:47:46:231][  info  ][  ScanFactory  ]: ~~~ }
[14:47:46:231][  info  ][  ScanFactory  ]:   Loading Loop #1
[14:47:46:231][  info  ][  ScanFactory  ]:    Type: Rd53aTriggerLoop
[14:47:46:231][  info  ][  ScanFactory  ]:    Loading loop config ... 
[14:47:46:231][  info  ][  ScanFactory  ]: ~~~ {
[14:47:46:231][  info  ][  ScanFactory  ]: ~~~     "count": 1,
[14:47:46:231][  info  ][  ScanFactory  ]: ~~~     "delay": 48,
[14:47:46:231][  info  ][  ScanFactory  ]: ~~~     "extTrig": false,
[14:47:46:231][  info  ][  ScanFactory  ]: ~~~     "frequency": 30000.0,
[14:47:46:231][  info  ][  ScanFactory  ]: ~~~     "noInject": false,
[14:47:46:231][  info  ][  ScanFactory  ]: ~~~     "sendEcr": false,
[14:47:46:231][  info  ][  ScanFactory  ]: ~~~     "time": 0.0,
[14:47:46:231][  info  ][  ScanFactory  ]: ~~~     "trigMultiplier": 16
[14:47:46:231][  info  ][  ScanFactory  ]: ~~~ }
[14:47:46:231][  info  ][  ScanFactory  ]:   Loading Loop #2
[14:47:46:231][  info  ][  ScanFactory  ]:    Type: StdDataLoop
[14:47:46:231][warning ][  ScanFactory  ]: ~~~ Config empty.
[14:47:46:231][  info  ][ScanBuildHistogrammers]: Loading histogrammer ...
[14:47:46:231][  info  ][ScanBuildHistogrammers]: ... done!
[14:47:46:231][  info  ][ScanBuildAnalyses]: Loading analyses ...
[14:47:46:231][  info  ][  scanConsole  ]: Running pre scan!
[14:47:46:231][  info  ][  ScanFactory  ]: Entering pre scan phase ...
[14:47:46:231][  info  ][     Rd53a     ]: Write named register: GlobalPulseRt -> 16384
[14:47:46:231][  info  ][     Rd53a     ]: Write named register: InjEnDig -> 0
[14:47:46:231][  info  ][     Rd53a     ]: Write named register: InjVcalDiff -> 0
[14:47:46:231][  info  ][     Rd53a     ]: Write named register: InjVcalHigh -> 3500
[14:47:46:231][  info  ][     Rd53a     ]: Write named register: InjVcalMed -> 500
[14:47:46:231][  info  ][     Rd53a     ]: Write named register: LatencyConfig -> 48
[14:47:46:231][  info  ][  scanConsole  ]: Starting histogrammer and analysis threads:
[14:47:46:232][  info  ][  scanConsole  ]:  .. started threads of Fe 4
[14:47:46:232][  info  ][  scanConsole  ]:  .. started threads of Fe 5
[14:47:46:232][  info  ][  scanConsole  ]:  .. started threads of Fe 6
[14:47:46:232][  info  ][  scanConsole  ]:  .. started threads of Fe 7
[14:47:46:232][  info  ][Rd53aDataProcessor]:   -> Processor thread #0 started!
[14:47:46:232][  info  ][Rd53aDataProcessor]:   -> Processor thread #1 started!
[14:47:46:232][  info  ][Rd53aDataProcessor]:   -> Processor thread #2 started!
[14:47:46:232][  info  ][Rd53aDataProcessor]:   -> Processor thread #3 started!
[14:47:46:232][  info  ][  scanConsole  ]: ########
[14:47:46:232][  info  ][  scanConsole  ]: # Scan #
[14:47:46:232][  info  ][  scanConsole  ]: ########
[14:47:46:232][  info  ][  scanConsole  ]: Starting scan!
[14:47:46:232][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0009364 on Rx 4
[14:47:46:236][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0009364] MON MUX_V: 3 Bias 0 1214 -> 0.267141 V Bias 1 1612 -> 0.346201 V, Temperature 0.0790608 -> 4.07922 C
[14:47:46:240][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0009364] MON MUX_V: 5 Bias 0 1179 -> 0.260188 V Bias 1 1580 -> 0.339845 V, Temperature 0.0796568 -> -2.75034 C
[14:47:46:244][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0009364] MON MUX_V: 7 Bias 0 1167 -> 0.257804 V Bias 1 1580 -> 0.339845 V, Temperature 0.0820405 -> -6.3541 C
[14:47:46:248][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0009364] MON MUX_V: 15 Bias 0 1161 -> 0.256612 V Bias 1 1574 -> 0.338653 V, Temperature 0.0820405 -> -3.56271 C
[14:47:46:283][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0009253 on Rx 5
[14:47:46:287][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0009253] MON MUX_V: 3 Bias 0 1250 -> 0.272036 V Bias 1 1640 -> 0.349136 V, Temperature 0.0771003 -> 5.43953 C
[14:47:46:291][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0009253] MON MUX_V: 5 Bias 0 1201 -> 0.262349 V Bias 1 1604 -> 0.342019 V, Temperature 0.0796703 -> -7.67737 C
[14:47:46:295][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0009253] MON MUX_V: 7 Bias 0 1184 -> 0.258988 V Bias 1 1592 -> 0.339647 V, Temperature 0.0806587 -> -6.448 C
[14:47:46:300][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0009253] MON MUX_V: 15 Bias 0 1171 -> 0.256418 V Bias 1 1584 -> 0.338065 V, Temperature 0.0816472 -> -5.99799 C
[14:47:46:334][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0009285 on Rx 6
[14:47:46:339][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0009285] MON MUX_V: 3 Bias 0 1248 -> 0.270381 V Bias 1 1651 -> 0.350361 V, Temperature 0.0799798 -> 2.15338 C
[14:47:46:343][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0009285] MON MUX_V: 5 Bias 0 1206 -> 0.262046 V Bias 1 1613 -> 0.342819 V, Temperature 0.0807737 -> 5.51779 C
[14:47:46:347][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0009285] MON MUX_V: 7 Bias 0 1196 -> 0.260061 V Bias 1 1603 -> 0.340835 V, Temperature 0.0807737 -> 3.67273 C
[14:47:46:351][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0009285] MON MUX_V: 15 Bias 0 1188 -> 0.258474 V Bias 1 1597 -> 0.339644 V, Temperature 0.0811706 -> 5.58472 C
[14:47:46:386][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0009269 on Rx 7
[14:47:46:390][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0009269] MON MUX_V: 3 Bias 0 1252 -> 0.271413 V Bias 1 1649 -> 0.350224 V, Temperature 0.0788111 -> 7.16429 C
[14:47:46:394][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0009269] MON MUX_V: 5 Bias 0 1214 -> 0.26387 V Bias 1 1620 -> 0.344467 V, Temperature 0.0805977 -> -1.84805 C
[14:47:46:398][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0009269] MON MUX_V: 7 Bias 0 1184 -> 0.257914 V Bias 1 1601 -> 0.340696 V, Temperature 0.0827814 -> -2.61499 C
[14:47:46:403][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0009269] MON MUX_V: 15 Bias 0 1206 -> 0.262282 V Bias 1 1615 -> 0.343475 V, Temperature 0.0811933 -> -0.313965 C
[14:47:46:439][  info  ][  scanConsole  ]: Scan done!
[14:47:46:439][  info  ][  scanConsole  ]: Waiting for processors to finish ...
[14:47:46:440][  info  ][  scanConsole  ]: Processor done, waiting for histogrammer ...
[14:47:46:839][  info  ][HistogramAlgorithm]: Histogrammer done!
[14:47:46:839][  info  ][HistogramAlgorithm]: Histogrammer done!
[14:47:46:839][  info  ][HistogramAlgorithm]: Histogrammer done!
[14:47:46:839][  info  ][HistogramAlgorithm]: Histogrammer done!
[14:47:46:840][  info  ][  scanConsole  ]: Processor done, waiting for analysis ...
[14:47:47:240][  info  ][AnalysisAlgorithm]: Analysis done!
[14:47:47:240][  info  ][AnalysisAlgorithm]: Analysis done!
[14:47:47:240][  info  ][AnalysisAlgorithm]: Analysis done!
[14:47:47:240][  info  ][AnalysisAlgorithm]: Analysis done!
[14:47:47:240][  info  ][  scanConsole  ]: All done!
[14:47:47:240][  info  ][  scanConsole  ]: ##########
[14:47:47:240][  info  ][  scanConsole  ]: # Timing #
[14:47:47:240][  info  ][  scanConsole  ]: ##########
[14:47:47:240][  info  ][  scanConsole  ]: -> Configuration: 122 ms
[14:47:47:241][  info  ][  scanConsole  ]: -> Scan:          206 ms
[14:47:47:241][  info  ][  scanConsole  ]: -> Processing:    0 ms
[14:47:47:241][  info  ][  scanConsole  ]: -> Analysis:      800 ms
[14:47:47:241][  info  ][  scanConsole  ]: ###########
[14:47:47:241][  info  ][  scanConsole  ]: # Cleanup #
[14:47:47:241][  info  ][  scanConsole  ]: ###########
[14:47:47:249][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0009364 to configs/KEKQ18/20UPGFC0009364.json
[14:47:47:416][  info  ][  scanConsole  ]: -> Storing output results of FE 4
[14:47:47:416][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0009364, this usually means that the chip did not send any data at all.
[14:47:47:418][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0009253 to configs/KEKQ18/20UPGFC0009253.json
[14:47:47:580][  info  ][  scanConsole  ]: -> Storing output results of FE 5
[14:47:47:580][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0009253, this usually means that the chip did not send any data at all.
[14:47:47:582][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0009285 to configs/KEKQ18/20UPGFC0009285.json
[14:47:47:744][  info  ][  scanConsole  ]: -> Storing output results of FE 6
[14:47:47:744][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0009285, this usually means that the chip did not send any data at all.
[14:47:47:745][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0009269 to configs/KEKQ18/20UPGFC0009269.json
[14:47:47:908][  info  ][  scanConsole  ]: -> Storing output results of FE 7
[14:47:47:908][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0009269, this usually means that the chip did not send any data at all.
[14:47:47:909][  info  ][  scanConsole  ]: Finishing run: 1841
20UPGFC0009253.json.after
20UPGFC0009253.json.before
20UPGFC0009269.json.after
20UPGFC0009269.json.before
20UPGFC0009285.json.after
20UPGFC0009285.json.before
20UPGFC0009364.json.after
20UPGFC0009364.json.before
reg_readtemp.json
scanLog.json

