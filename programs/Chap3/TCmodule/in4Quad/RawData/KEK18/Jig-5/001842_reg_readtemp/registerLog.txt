[14:48:27:470][  info  ][               ]: #####################################
[14:48:27:470][  info  ][               ]: # Welcome to the YARR Scan Console! #
[14:48:27:470][  info  ][               ]: #####################################
[14:48:27:470][  info  ][               ]: -> Parsing command line parameters ...
[14:48:27:470][  info  ][               ]: Configuring logger ...
[14:48:27:472][  info  ][  scanConsole  ]: Scan Type/Config configs/scans/rd53a/reg_readtemp.json
[14:48:27:472][  info  ][  scanConsole  ]: Connectivity:
[14:48:27:472][  info  ][  scanConsole  ]:     configs/KEKQ18/connectivity.json
[14:48:27:472][  info  ][  scanConsole  ]: Target ToT: -1
[14:48:27:472][  info  ][  scanConsole  ]: Target Charge: -1
[14:48:27:472][  info  ][  scanConsole  ]: Output Plots: true
[14:48:27:472][  info  ][  scanConsole  ]: Output Directory: ./data/001842_reg_readtemp/
[14:48:27:476][  info  ][  scanConsole  ]: Timestamp: 2022-08-26_14:48:27
[14:48:27:476][  info  ][  scanConsole  ]: Run Number: 1842
[14:48:27:477][  info  ][  scanConsole  ]: #################
[14:48:27:477][  info  ][  scanConsole  ]: # Init Hardware #
[14:48:27:477][  info  ][  scanConsole  ]: #################
[14:48:27:477][  info  ][  scanConsole  ]: -> Opening controller config: configs/controller/specCfg-rd53a-16x1.json
[14:48:27:477][  info  ][  ScanHelper   ]: Loading controller ...
[14:48:27:477][  info  ][  ScanHelper   ]: Found controller of type: spec
[14:48:27:477][  info  ][  ScanHelper   ]: ... loading controler config:
[14:48:27:477][  info  ][  ScanHelper   ]: ~~~ {
[14:48:27:477][  info  ][  ScanHelper   ]: ~~~     "cmdPeriod": 6.250000073038109e-9,
[14:48:27:477][  info  ][  ScanHelper   ]: ~~~     "idle": {
[14:48:27:477][  info  ][  ScanHelper   ]: ~~~         "word": 1768515945
[14:48:27:477][  info  ][  ScanHelper   ]: ~~~     },
[14:48:27:477][  info  ][  ScanHelper   ]: ~~~     "pulse": {
[14:48:27:477][  info  ][  ScanHelper   ]: ~~~         "interval": 500,
[14:48:27:477][  info  ][  ScanHelper   ]: ~~~         "word": 1549575846
[14:48:27:477][  info  ][  ScanHelper   ]: ~~~     },
[14:48:27:477][  info  ][  ScanHelper   ]: ~~~     "rxActiveLanes": 1,
[14:48:27:477][  info  ][  ScanHelper   ]: ~~~     "rxPolarity": 0,
[14:48:27:477][  info  ][  ScanHelper   ]: ~~~     "specNum": 0,
[14:48:27:477][  info  ][  ScanHelper   ]: ~~~     "spiConfig": 541200,
[14:48:27:477][  info  ][  ScanHelper   ]: ~~~     "sync": {
[14:48:27:477][  info  ][  ScanHelper   ]: ~~~         "interval": 32,
[14:48:27:477][  info  ][  ScanHelper   ]: ~~~         "word": 2172551550
[14:48:27:477][  info  ][  ScanHelper   ]: ~~~     },
[14:48:27:477][  info  ][  ScanHelper   ]: ~~~     "txPolarity": 0
[14:48:27:477][  info  ][  ScanHelper   ]: ~~~ }
[14:48:27:477][  info  ][    SpecCom    ]: Opening SPEC with id #0
[14:48:27:477][  info  ][    SpecCom    ]: Mapping BARs ...
[14:48:27:477][  info  ][    SpecCom    ]: ... Mapped BAR0 at 0x7fc1b861b000 with size 1048576
[14:48:27:477][warning ][    SpecCom    ]: ... BAR4 not mapped (Mmap failed)
[14:48:27:477][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[14:48:27:477][  info  ][    SpecCom    ]: Firmware Version: 0x4d9ff6d
[14:48:27:477][  info  ][    SpecCom    ]: Firmware Identifier: 0x4030232
[14:48:27:477][  info  ][    SpecCom    ]: FPGA card: PLDA XpressK7 325
[14:48:27:477][  info  ][    SpecCom    ]: FE Chip Type: RD53A/B
[14:48:27:477][  info  ][    SpecCom    ]: FMC Card Type: Ohio Card (Display Port)
[14:48:27:477][  info  ][    SpecCom    ]: RX Speed: 640Mbps
[14:48:27:477][  info  ][    SpecCom    ]: Channel Configuration: 16x1
[14:48:27:477][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[14:48:27:477][  info  ][    SpecCom    ]: Flushing buffers ...
[14:48:27:477][  info  ][    SpecCom    ]: Init success!
[14:48:27:477][  info  ][  scanConsole  ]: #######################
[14:48:27:477][  info  ][  scanConsole  ]: ##  Loading Configs  ##
[14:48:27:477][  info  ][  scanConsole  ]: #######################
[14:48:27:477][  info  ][  scanConsole  ]: Opening global config: configs/KEKQ18/connectivity.json
[14:48:27:477][  info  ][  ScanHelper   ]: Chip type: RD53A
[14:48:27:477][  info  ][  ScanHelper   ]: Chip count 4
[14:48:27:477][  info  ][  ScanHelper   ]: Loading chip #0
[14:48:27:478][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(4)
[14:48:27:478][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ18/20UPGFC0009364.json
[14:48:27:654][  info  ][  ScanHelper   ]: Loading chip #1
[14:48:27:654][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(5)
[14:48:27:654][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ18/20UPGFC0009253.json
[14:48:27:827][  info  ][  ScanHelper   ]: Loading chip #2
[14:48:27:827][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(6)
[14:48:27:827][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ18/20UPGFC0009285.json
[14:48:28:001][  info  ][  ScanHelper   ]: Loading chip #3
[14:48:28:001][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(7)
[14:48:28:001][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ18/20UPGFC0009269.json
[14:48:28:178][  info  ][  scanConsole  ]: #################
[14:48:28:178][  info  ][  scanConsole  ]: # Configure FEs #
[14:48:28:178][  info  ][  scanConsole  ]: #################
[14:48:28:178][  info  ][  scanConsole  ]: Configuring 20UPGFC0009364
[14:48:28:209][  info  ][  scanConsole  ]: Configuring 20UPGFC0009253
[14:48:28:242][  info  ][  scanConsole  ]: Configuring 20UPGFC0009285
[14:48:28:290][  info  ][  scanConsole  ]: Configuring 20UPGFC0009269
[14:48:28:333][  info  ][  scanConsole  ]: Sent configuration to all FEs in 154 ms!
[14:48:28:334][  info  ][  scanConsole  ]: Checking com 20UPGFC0009364
[14:48:28:334][  info  ][    SpecRx     ]: Active Rx channels: 0x10
[14:48:28:334][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[14:48:28:334][  info  ][    SpecRx     ]: Rx Status 0xf0
[14:48:28:334][  info  ][    SpecRx     ]: Number of lanes: 1
[14:48:28:334][  info  ][    SpecRx     ]: Channel 4 Lane 0 synchronized!
[14:48:28:344][  info  ][  scanConsole  ]: ... success!
[14:48:28:344][  info  ][  scanConsole  ]: Checking com 20UPGFC0009253
[14:48:28:344][  info  ][    SpecRx     ]: Active Rx channels: 0x20
[14:48:28:344][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[14:48:28:344][  info  ][    SpecRx     ]: Rx Status 0xf0
[14:48:28:344][  info  ][    SpecRx     ]: Number of lanes: 1
[14:48:28:344][  info  ][    SpecRx     ]: Channel 5 Lane 0 synchronized!
[14:48:28:355][  info  ][  scanConsole  ]: ... success!
[14:48:28:355][  info  ][  scanConsole  ]: Checking com 20UPGFC0009285
[14:48:28:355][  info  ][    SpecRx     ]: Active Rx channels: 0x40
[14:48:28:355][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[14:48:28:355][  info  ][    SpecRx     ]: Rx Status 0xf0
[14:48:28:355][  info  ][    SpecRx     ]: Number of lanes: 1
[14:48:28:355][  info  ][    SpecRx     ]: Channel 6 Lane 0 synchronized!
[14:48:28:365][  info  ][  scanConsole  ]: ... success!
[14:48:28:365][  info  ][  scanConsole  ]: Checking com 20UPGFC0009269
[14:48:28:365][  info  ][    SpecRx     ]: Active Rx channels: 0x80
[14:48:28:365][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[14:48:28:365][  info  ][    SpecRx     ]: Rx Status 0xf0
[14:48:28:365][  info  ][    SpecRx     ]: Number of lanes: 1
[14:48:28:366][  info  ][    SpecRx     ]: Channel 7 Lane 0 synchronized!
[14:48:28:376][  info  ][  scanConsole  ]: ... success!
[14:48:28:376][  info  ][  scanConsole  ]: Enabling Tx channels
[14:48:28:376][  info  ][  scanConsole  ]: Enabling Tx channel 1
[14:48:28:376][  info  ][  scanConsole  ]: Enabling Tx channel 1
[14:48:28:376][  info  ][  scanConsole  ]: Enabling Tx channel 1
[14:48:28:376][  info  ][  scanConsole  ]: Enabling Tx channel 1
[14:48:28:376][  info  ][  scanConsole  ]: Enabling Rx channels
[14:48:28:376][  info  ][  scanConsole  ]: Enabling Rx channel 4
[14:48:28:376][  info  ][  scanConsole  ]: Enabling Rx channel 5
[14:48:28:376][  info  ][  scanConsole  ]: Enabling Rx channel 6
[14:48:28:376][  info  ][  scanConsole  ]: Enabling Rx channel 7
[14:48:28:376][  info  ][  scanConsole  ]: ##############
[14:48:28:376][  info  ][  scanConsole  ]: # Setup Scan #
[14:48:28:376][  info  ][  scanConsole  ]: ##############
[14:48:28:377][  info  ][  scanConsole  ]: Found Scan config, constructing scan ...
[14:48:28:377][  info  ][  ScanFactory  ]: Loading Scan:
[14:48:28:377][  info  ][  ScanFactory  ]:   Name: AnalogScan
[14:48:28:377][  info  ][  ScanFactory  ]:   Number of Loops: 3
[14:48:28:377][  info  ][  ScanFactory  ]:   Loading Loop #0
[14:48:28:377][  info  ][  ScanFactory  ]:    Type: Rd53aReadRegLoop
[14:48:28:377][  info  ][  ScanFactory  ]:    Loading loop config ... 
[14:48:28:378][  info  ][  ScanFactory  ]: ~~~ {
[14:48:28:378][  info  ][  ScanFactory  ]: ~~~     "CurMux": [],
[14:48:28:378][  info  ][  ScanFactory  ]: ~~~     "EnblRingOsc": 0,
[14:48:28:378][  info  ][  ScanFactory  ]: ~~~     "Registers": [],
[14:48:28:378][  info  ][  ScanFactory  ]: ~~~     "RingOscDur": 9,
[14:48:28:378][  info  ][  ScanFactory  ]: ~~~     "RingOscRep": 10,
[14:48:28:378][  info  ][  ScanFactory  ]: ~~~     "VoltMux": [3, 5, 7, 15]
[14:48:28:378][  info  ][  ScanFactory  ]: ~~~ }
[14:48:28:378][  info  ][  ScanFactory  ]:   Loading Loop #1
[14:48:28:378][  info  ][  ScanFactory  ]:    Type: Rd53aTriggerLoop
[14:48:28:378][  info  ][  ScanFactory  ]:    Loading loop config ... 
[14:48:28:378][  info  ][  ScanFactory  ]: ~~~ {
[14:48:28:378][  info  ][  ScanFactory  ]: ~~~     "count": 1,
[14:48:28:378][  info  ][  ScanFactory  ]: ~~~     "delay": 48,
[14:48:28:378][  info  ][  ScanFactory  ]: ~~~     "extTrig": false,
[14:48:28:378][  info  ][  ScanFactory  ]: ~~~     "frequency": 30000.0,
[14:48:28:378][  info  ][  ScanFactory  ]: ~~~     "noInject": false,
[14:48:28:378][  info  ][  ScanFactory  ]: ~~~     "sendEcr": false,
[14:48:28:378][  info  ][  ScanFactory  ]: ~~~     "time": 0.0,
[14:48:28:378][  info  ][  ScanFactory  ]: ~~~     "trigMultiplier": 16
[14:48:28:378][  info  ][  ScanFactory  ]: ~~~ }
[14:48:28:378][  info  ][  ScanFactory  ]:   Loading Loop #2
[14:48:28:378][  info  ][  ScanFactory  ]:    Type: StdDataLoop
[14:48:28:378][warning ][  ScanFactory  ]: ~~~ Config empty.
[14:48:28:378][  info  ][ScanBuildHistogrammers]: Loading histogrammer ...
[14:48:28:379][  info  ][ScanBuildHistogrammers]: ... done!
[14:48:28:379][  info  ][ScanBuildAnalyses]: Loading analyses ...
[14:48:28:379][  info  ][  scanConsole  ]: Running pre scan!
[14:48:28:379][  info  ][  ScanFactory  ]: Entering pre scan phase ...
[14:48:28:379][  info  ][     Rd53a     ]: Write named register: GlobalPulseRt -> 16384
[14:48:28:379][  info  ][     Rd53a     ]: Write named register: InjEnDig -> 0
[14:48:28:379][  info  ][     Rd53a     ]: Write named register: InjVcalDiff -> 0
[14:48:28:379][  info  ][     Rd53a     ]: Write named register: InjVcalHigh -> 3500
[14:48:28:379][  info  ][     Rd53a     ]: Write named register: InjVcalMed -> 500
[14:48:28:379][  info  ][     Rd53a     ]: Write named register: LatencyConfig -> 48
[14:48:28:379][  info  ][  scanConsole  ]: Starting histogrammer and analysis threads:
[14:48:28:379][  info  ][  scanConsole  ]:  .. started threads of Fe 4
[14:48:28:380][  info  ][  scanConsole  ]:  .. started threads of Fe 5
[14:48:28:380][  info  ][  scanConsole  ]:  .. started threads of Fe 6
[14:48:28:380][  info  ][  scanConsole  ]:  .. started threads of Fe 7
[14:48:28:380][  info  ][Rd53aDataProcessor]:   -> Processor thread #0 started!
[14:48:28:380][  info  ][Rd53aDataProcessor]:   -> Processor thread #1 started!
[14:48:28:380][  info  ][Rd53aDataProcessor]:   -> Processor thread #2 started!
[14:48:28:380][  info  ][Rd53aDataProcessor]:   -> Processor thread #3 started!
[14:48:28:380][  info  ][  scanConsole  ]: ########
[14:48:28:380][  info  ][  scanConsole  ]: # Scan #
[14:48:28:380][  info  ][  scanConsole  ]: ########
[14:48:28:380][  info  ][  scanConsole  ]: Starting scan!
[14:48:28:380][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0009364 on Rx 4
[14:48:28:385][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0009364] MON MUX_V: 3 Bias 0 1213 -> 0.266942 V Bias 1 1611 -> 0.346003 V, Temperature 0.0790609 -> 4.07932 C
[14:48:28:389][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0009364] MON MUX_V: 5 Bias 0 1180 -> 0.260387 V Bias 1 1579 -> 0.339646 V, Temperature 0.0792595 -> -4.37381 C
[14:48:28:393][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0009364] MON MUX_V: 7 Bias 0 1167 -> 0.257804 V Bias 1 1580 -> 0.339845 V, Temperature 0.0820405 -> -6.3541 C
[14:48:28:397][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0009364] MON MUX_V: 15 Bias 0 1162 -> 0.256811 V Bias 1 1573 -> 0.338454 V, Temperature 0.0816432 -> -5.18616 C
[14:48:28:432][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0009253 on Rx 5
[14:48:28:437][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0009253] MON MUX_V: 3 Bias 0 1251 -> 0.272234 V Bias 1 1640 -> 0.349136 V, Temperature 0.0769026 -> 4.86459 C
[14:48:28:441][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0009253] MON MUX_V: 5 Bias 0 1201 -> 0.262349 V Bias 1 1604 -> 0.342019 V, Temperature 0.0796703 -> -7.67737 C
[14:48:28:445][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0009253] MON MUX_V: 7 Bias 0 1183 -> 0.258791 V Bias 1 1592 -> 0.339647 V, Temperature 0.0808564 -> -5.6813 C
[14:48:28:449][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0009253] MON MUX_V: 15 Bias 0 1172 -> 0.256616 V Bias 1 1585 -> 0.338263 V, Temperature 0.0816472 -> -5.99799 C
[14:48:28:484][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0009285 on Rx 6
[14:48:28:488][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0009285] MON MUX_V: 3 Bias 0 1248 -> 0.270381 V Bias 1 1650 -> 0.350163 V, Temperature 0.0797814 -> 1.45813 C
[14:48:28:492][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0009285] MON MUX_V: 5 Bias 0 1205 -> 0.261847 V Bias 1 1612 -> 0.342621 V, Temperature 0.0807737 -> 5.51779 C
[14:48:28:496][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0009285] MON MUX_V: 7 Bias 0 1195 -> 0.259863 V Bias 1 1602 -> 0.340636 V, Temperature 0.0807737 -> 3.67273 C
[14:48:28:501][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0009285] MON MUX_V: 15 Bias 0 1188 -> 0.258474 V Bias 1 1596 -> 0.339446 V, Temperature 0.0809721 -> 4.9173 C
[14:48:28:535][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0009269 on Rx 7
[14:48:28:540][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0009269] MON MUX_V: 3 Bias 0 1251 -> 0.271215 V Bias 1 1648 -> 0.350026 V, Temperature 0.0788111 -> 7.16429 C
[14:48:28:544][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0009269] MON MUX_V: 5 Bias 0 1213 -> 0.263671 V Bias 1 1620 -> 0.344467 V, Temperature 0.0807963 -> -1.08133 C
[14:48:28:547][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0009269] MON MUX_V: 7 Bias 0 1184 -> 0.257914 V Bias 1 1601 -> 0.340696 V, Temperature 0.0827814 -> -2.61499 C
[14:48:28:551][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0009269] MON MUX_V: 15 Bias 0 1206 -> 0.262282 V Bias 1 1615 -> 0.343475 V, Temperature 0.0811933 -> -0.313965 C
[14:48:28:587][  info  ][  scanConsole  ]: Scan done!
[14:48:28:587][  info  ][  scanConsole  ]: Waiting for processors to finish ...
[14:48:28:588][  info  ][  scanConsole  ]: Processor done, waiting for histogrammer ...
[14:48:28:987][  info  ][HistogramAlgorithm]: Histogrammer done!
[14:48:28:987][  info  ][HistogramAlgorithm]: Histogrammer done!
[14:48:28:987][  info  ][HistogramAlgorithm]: Histogrammer done!
[14:48:28:987][  info  ][HistogramAlgorithm]: Histogrammer done!
[14:48:28:987][  info  ][  scanConsole  ]: Processor done, waiting for analysis ...
[14:48:29:388][  info  ][AnalysisAlgorithm]: Analysis done!
[14:48:29:388][  info  ][AnalysisAlgorithm]: Analysis done!
[14:48:29:388][  info  ][AnalysisAlgorithm]: Analysis done!
[14:48:29:388][  info  ][AnalysisAlgorithm]: Analysis done!
[14:48:29:388][  info  ][  scanConsole  ]: All done!
[14:48:29:388][  info  ][  scanConsole  ]: ##########
[14:48:29:388][  info  ][  scanConsole  ]: # Timing #
[14:48:29:388][  info  ][  scanConsole  ]: ##########
[14:48:29:388][  info  ][  scanConsole  ]: -> Configuration: 154 ms
[14:48:29:388][  info  ][  scanConsole  ]: -> Scan:          206 ms
[14:48:29:388][  info  ][  scanConsole  ]: -> Processing:    0 ms
[14:48:29:388][  info  ][  scanConsole  ]: -> Analysis:      800 ms
[14:48:29:388][  info  ][  scanConsole  ]: ###########
[14:48:29:388][  info  ][  scanConsole  ]: # Cleanup #
[14:48:29:388][  info  ][  scanConsole  ]: ###########
[14:48:29:397][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0009364 to configs/KEKQ18/20UPGFC0009364.json
[14:48:29:572][  info  ][  scanConsole  ]: -> Storing output results of FE 4
[14:48:29:572][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0009364, this usually means that the chip did not send any data at all.
[14:48:29:573][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0009253 to configs/KEKQ18/20UPGFC0009253.json
[14:48:29:736][  info  ][  scanConsole  ]: -> Storing output results of FE 5
[14:48:29:736][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0009253, this usually means that the chip did not send any data at all.
[14:48:29:737][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0009285 to configs/KEKQ18/20UPGFC0009285.json
[14:48:29:902][  info  ][  scanConsole  ]: -> Storing output results of FE 6
[14:48:29:902][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0009285, this usually means that the chip did not send any data at all.
[14:48:29:903][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0009269 to configs/KEKQ18/20UPGFC0009269.json
[14:48:30:066][  info  ][  scanConsole  ]: -> Storing output results of FE 7
[14:48:30:066][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0009269, this usually means that the chip did not send any data at all.
[14:48:30:067][  info  ][  scanConsole  ]: Finishing run: 1842
20UPGFC0009253.json.after
20UPGFC0009253.json.before
20UPGFC0009269.json.after
20UPGFC0009269.json.before
20UPGFC0009285.json.after
20UPGFC0009285.json.before
20UPGFC0009364.json.after
20UPGFC0009364.json.before
reg_readtemp.json
scanLog.json

