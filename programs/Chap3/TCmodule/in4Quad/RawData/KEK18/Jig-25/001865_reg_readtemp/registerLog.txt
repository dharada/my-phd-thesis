[17:00:55:841][  info  ][               ]: #####################################
[17:00:55:841][  info  ][               ]: # Welcome to the YARR Scan Console! #
[17:00:55:841][  info  ][               ]: #####################################
[17:00:55:841][  info  ][               ]: -> Parsing command line parameters ...
[17:00:55:841][  info  ][               ]: Configuring logger ...
[17:00:55:843][  info  ][  scanConsole  ]: Scan Type/Config configs/scans/rd53a/reg_readtemp.json
[17:00:55:843][  info  ][  scanConsole  ]: Connectivity:
[17:00:55:843][  info  ][  scanConsole  ]:     configs/KEKQ17/connectivity.json
[17:00:55:843][  info  ][  scanConsole  ]: Target ToT: -1
[17:00:55:843][  info  ][  scanConsole  ]: Target Charge: -1
[17:00:55:843][  info  ][  scanConsole  ]: Output Plots: true
[17:00:55:843][  info  ][  scanConsole  ]: Output Directory: ./data/001865_reg_readtemp/
[17:00:55:848][  info  ][  scanConsole  ]: Timestamp: 2022-08-26_17:00:55
[17:00:55:848][  info  ][  scanConsole  ]: Run Number: 1865
[17:00:55:848][  info  ][  scanConsole  ]: #################
[17:00:55:848][  info  ][  scanConsole  ]: # Init Hardware #
[17:00:55:848][  info  ][  scanConsole  ]: #################
[17:00:55:848][  info  ][  scanConsole  ]: -> Opening controller config: configs/controller/specCfg-rd53a-16x1.json
[17:00:55:849][  info  ][  ScanHelper   ]: Loading controller ...
[17:00:55:849][  info  ][  ScanHelper   ]: Found controller of type: spec
[17:00:55:849][  info  ][  ScanHelper   ]: ... loading controler config:
[17:00:55:849][  info  ][  ScanHelper   ]: ~~~ {
[17:00:55:849][  info  ][  ScanHelper   ]: ~~~     "cmdPeriod": 6.250000073038109e-9,
[17:00:55:849][  info  ][  ScanHelper   ]: ~~~     "idle": {
[17:00:55:849][  info  ][  ScanHelper   ]: ~~~         "word": 1768515945
[17:00:55:849][  info  ][  ScanHelper   ]: ~~~     },
[17:00:55:849][  info  ][  ScanHelper   ]: ~~~     "pulse": {
[17:00:55:849][  info  ][  ScanHelper   ]: ~~~         "interval": 500,
[17:00:55:849][  info  ][  ScanHelper   ]: ~~~         "word": 1549575846
[17:00:55:849][  info  ][  ScanHelper   ]: ~~~     },
[17:00:55:849][  info  ][  ScanHelper   ]: ~~~     "rxActiveLanes": 1,
[17:00:55:849][  info  ][  ScanHelper   ]: ~~~     "rxPolarity": 0,
[17:00:55:849][  info  ][  ScanHelper   ]: ~~~     "specNum": 0,
[17:00:55:849][  info  ][  ScanHelper   ]: ~~~     "spiConfig": 541200,
[17:00:55:849][  info  ][  ScanHelper   ]: ~~~     "sync": {
[17:00:55:849][  info  ][  ScanHelper   ]: ~~~         "interval": 32,
[17:00:55:849][  info  ][  ScanHelper   ]: ~~~         "word": 2172551550
[17:00:55:849][  info  ][  ScanHelper   ]: ~~~     },
[17:00:55:849][  info  ][  ScanHelper   ]: ~~~     "txPolarity": 0
[17:00:55:849][  info  ][  ScanHelper   ]: ~~~ }
[17:00:55:849][  info  ][    SpecCom    ]: Opening SPEC with id #0
[17:00:55:849][  info  ][    SpecCom    ]: Mapping BARs ...
[17:00:55:849][  info  ][    SpecCom    ]: ... Mapped BAR0 at 0x7faab5e13000 with size 1048576
[17:00:55:849][warning ][    SpecCom    ]: ... BAR4 not mapped (Mmap failed)
[17:00:55:849][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[17:00:55:849][  info  ][    SpecCom    ]: Firmware Version: 0x4d9ff6d
[17:00:55:849][  info  ][    SpecCom    ]: Firmware Identifier: 0x4030232
[17:00:55:849][  info  ][    SpecCom    ]: FPGA card: PLDA XpressK7 325
[17:00:55:849][  info  ][    SpecCom    ]: FE Chip Type: RD53A/B
[17:00:55:849][  info  ][    SpecCom    ]: FMC Card Type: Ohio Card (Display Port)
[17:00:55:849][  info  ][    SpecCom    ]: RX Speed: 640Mbps
[17:00:55:849][  info  ][    SpecCom    ]: Channel Configuration: 16x1
[17:00:55:849][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[17:00:55:849][  info  ][    SpecCom    ]: Flushing buffers ...
[17:00:55:849][  info  ][    SpecCom    ]: Init success!
[17:00:55:849][  info  ][  scanConsole  ]: #######################
[17:00:55:849][  info  ][  scanConsole  ]: ##  Loading Configs  ##
[17:00:55:849][  info  ][  scanConsole  ]: #######################
[17:00:55:849][  info  ][  scanConsole  ]: Opening global config: configs/KEKQ17/connectivity.json
[17:00:55:849][  info  ][  ScanHelper   ]: Chip type: RD53A
[17:00:55:849][  info  ][  ScanHelper   ]: Chip count 4
[17:00:55:849][  info  ][  ScanHelper   ]: Loading chip #0
[17:00:55:850][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(4)
[17:00:55:850][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ17/20UPGFC0013396.json
[17:00:56:027][  info  ][  ScanHelper   ]: Loading chip #1
[17:00:56:028][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(5)
[17:00:56:028][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ17/20UPGFC0013428.json
[17:00:56:202][  info  ][  ScanHelper   ]: Loading chip #2
[17:00:56:203][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(6)
[17:00:56:203][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ17/20UPGFC0013381.json
[17:00:56:377][  info  ][  ScanHelper   ]: Loading chip #3
[17:00:56:378][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(7)
[17:00:56:378][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ17/20UPGFC0013349.json
[17:00:56:559][  info  ][  scanConsole  ]: #################
[17:00:56:559][  info  ][  scanConsole  ]: # Configure FEs #
[17:00:56:559][  info  ][  scanConsole  ]: #################
[17:00:56:559][  info  ][  scanConsole  ]: Configuring 20UPGFC0013396
[17:00:56:590][  info  ][  scanConsole  ]: Configuring 20UPGFC0013428
[17:00:56:620][  info  ][  scanConsole  ]: Configuring 20UPGFC0013381
[17:00:56:650][  info  ][  scanConsole  ]: Configuring 20UPGFC0013349
[17:00:56:680][  info  ][  scanConsole  ]: Sent configuration to all FEs in 120 ms!
[17:00:56:681][  info  ][  scanConsole  ]: Checking com 20UPGFC0013396
[17:00:56:681][  info  ][    SpecRx     ]: Active Rx channels: 0x10
[17:00:56:681][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[17:00:56:681][  info  ][    SpecRx     ]: Rx Status 0xf0
[17:00:56:681][  info  ][    SpecRx     ]: Number of lanes: 1
[17:00:56:681][  info  ][    SpecRx     ]: Channel 4 Lane 0 synchronized!
[17:00:56:691][  info  ][  scanConsole  ]: ... success!
[17:00:56:691][  info  ][  scanConsole  ]: Checking com 20UPGFC0013428
[17:00:56:691][  info  ][    SpecRx     ]: Active Rx channels: 0x20
[17:00:56:691][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[17:00:56:691][  info  ][    SpecRx     ]: Rx Status 0xf0
[17:00:56:691][  info  ][    SpecRx     ]: Number of lanes: 1
[17:00:56:691][  info  ][    SpecRx     ]: Channel 5 Lane 0 synchronized!
[17:00:56:702][  info  ][  scanConsole  ]: ... success!
[17:00:56:702][  info  ][  scanConsole  ]: Checking com 20UPGFC0013381
[17:00:56:702][  info  ][    SpecRx     ]: Active Rx channels: 0x40
[17:00:56:702][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[17:00:56:702][  info  ][    SpecRx     ]: Rx Status 0xf0
[17:00:56:702][  info  ][    SpecRx     ]: Number of lanes: 1
[17:00:56:702][  info  ][    SpecRx     ]: Channel 6 Lane 0 synchronized!
[17:00:56:712][  info  ][  scanConsole  ]: ... success!
[17:00:56:712][  info  ][  scanConsole  ]: Checking com 20UPGFC0013349
[17:00:56:712][  info  ][    SpecRx     ]: Active Rx channels: 0x80
[17:00:56:712][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[17:00:56:712][  info  ][    SpecRx     ]: Rx Status 0xf0
[17:00:56:712][  info  ][    SpecRx     ]: Number of lanes: 1
[17:00:56:712][  info  ][    SpecRx     ]: Channel 7 Lane 0 synchronized!
[17:00:56:723][  info  ][  scanConsole  ]: ... success!
[17:00:56:723][  info  ][  scanConsole  ]: Enabling Tx channels
[17:00:56:723][  info  ][  scanConsole  ]: Enabling Tx channel 1
[17:00:56:723][  info  ][  scanConsole  ]: Enabling Tx channel 1
[17:00:56:723][  info  ][  scanConsole  ]: Enabling Tx channel 1
[17:00:56:723][  info  ][  scanConsole  ]: Enabling Tx channel 1
[17:00:56:723][  info  ][  scanConsole  ]: Enabling Rx channels
[17:00:56:723][  info  ][  scanConsole  ]: Enabling Rx channel 4
[17:00:56:723][  info  ][  scanConsole  ]: Enabling Rx channel 5
[17:00:56:723][  info  ][  scanConsole  ]: Enabling Rx channel 6
[17:00:56:723][  info  ][  scanConsole  ]: Enabling Rx channel 7
[17:00:56:723][  info  ][  scanConsole  ]: ##############
[17:00:56:723][  info  ][  scanConsole  ]: # Setup Scan #
[17:00:56:723][  info  ][  scanConsole  ]: ##############
[17:00:56:724][  info  ][  scanConsole  ]: Found Scan config, constructing scan ...
[17:00:56:724][  info  ][  ScanFactory  ]: Loading Scan:
[17:00:56:724][  info  ][  ScanFactory  ]:   Name: AnalogScan
[17:00:56:724][  info  ][  ScanFactory  ]:   Number of Loops: 3
[17:00:56:724][  info  ][  ScanFactory  ]:   Loading Loop #0
[17:00:56:724][  info  ][  ScanFactory  ]:    Type: Rd53aReadRegLoop
[17:00:56:724][  info  ][  ScanFactory  ]:    Loading loop config ... 
[17:00:56:724][  info  ][  ScanFactory  ]: ~~~ {
[17:00:56:724][  info  ][  ScanFactory  ]: ~~~     "CurMux": [],
[17:00:56:724][  info  ][  ScanFactory  ]: ~~~     "EnblRingOsc": 0,
[17:00:56:724][  info  ][  ScanFactory  ]: ~~~     "Registers": [],
[17:00:56:724][  info  ][  ScanFactory  ]: ~~~     "RingOscDur": 9,
[17:00:56:724][  info  ][  ScanFactory  ]: ~~~     "RingOscRep": 10,
[17:00:56:724][  info  ][  ScanFactory  ]: ~~~     "VoltMux": [3, 5, 7, 15]
[17:00:56:724][  info  ][  ScanFactory  ]: ~~~ }
[17:00:56:724][  info  ][  ScanFactory  ]:   Loading Loop #1
[17:00:56:724][  info  ][  ScanFactory  ]:    Type: Rd53aTriggerLoop
[17:00:56:724][  info  ][  ScanFactory  ]:    Loading loop config ... 
[17:00:56:725][  info  ][  ScanFactory  ]: ~~~ {
[17:00:56:725][  info  ][  ScanFactory  ]: ~~~     "count": 1,
[17:00:56:725][  info  ][  ScanFactory  ]: ~~~     "delay": 48,
[17:00:56:725][  info  ][  ScanFactory  ]: ~~~     "extTrig": false,
[17:00:56:725][  info  ][  ScanFactory  ]: ~~~     "frequency": 30000.0,
[17:00:56:725][  info  ][  ScanFactory  ]: ~~~     "noInject": false,
[17:00:56:725][  info  ][  ScanFactory  ]: ~~~     "sendEcr": false,
[17:00:56:725][  info  ][  ScanFactory  ]: ~~~     "time": 0.0,
[17:00:56:725][  info  ][  ScanFactory  ]: ~~~     "trigMultiplier": 16
[17:00:56:725][  info  ][  ScanFactory  ]: ~~~ }
[17:00:56:725][  info  ][  ScanFactory  ]:   Loading Loop #2
[17:00:56:725][  info  ][  ScanFactory  ]:    Type: StdDataLoop
[17:00:56:725][warning ][  ScanFactory  ]: ~~~ Config empty.
[17:00:56:725][  info  ][ScanBuildHistogrammers]: Loading histogrammer ...
[17:00:56:725][  info  ][ScanBuildHistogrammers]: ... done!
[17:00:56:725][  info  ][ScanBuildAnalyses]: Loading analyses ...
[17:00:56:726][  info  ][  scanConsole  ]: Running pre scan!
[17:00:56:726][  info  ][  ScanFactory  ]: Entering pre scan phase ...
[17:00:56:726][  info  ][     Rd53a     ]: Write named register: GlobalPulseRt -> 16384
[17:00:56:726][  info  ][     Rd53a     ]: Write named register: InjEnDig -> 0
[17:00:56:726][  info  ][     Rd53a     ]: Write named register: InjVcalDiff -> 0
[17:00:56:726][  info  ][     Rd53a     ]: Write named register: InjVcalHigh -> 3500
[17:00:56:726][  info  ][     Rd53a     ]: Write named register: InjVcalMed -> 500
[17:00:56:726][  info  ][     Rd53a     ]: Write named register: LatencyConfig -> 48
[17:00:56:726][  info  ][  scanConsole  ]: Starting histogrammer and analysis threads:
[17:00:56:726][  info  ][  scanConsole  ]:  .. started threads of Fe 4
[17:00:56:726][  info  ][  scanConsole  ]:  .. started threads of Fe 5
[17:00:56:726][  info  ][  scanConsole  ]:  .. started threads of Fe 6
[17:00:56:726][  info  ][  scanConsole  ]:  .. started threads of Fe 7
[17:00:56:727][  info  ][Rd53aDataProcessor]:   -> Processor thread #0 started!
[17:00:56:727][  info  ][Rd53aDataProcessor]:   -> Processor thread #1 started!
[17:00:56:727][  info  ][Rd53aDataProcessor]:   -> Processor thread #2 started!
[17:00:56:727][  info  ][Rd53aDataProcessor]:   -> Processor thread #3 started!
[17:00:56:727][  info  ][  scanConsole  ]: ########
[17:00:56:727][  info  ][  scanConsole  ]: # Scan #
[17:00:56:727][  info  ][  scanConsole  ]: ########
[17:00:56:727][  info  ][  scanConsole  ]: Starting scan!
[17:00:56:727][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0013396 on Rx 4
[17:00:56:732][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0013396] MON MUX_V: 3 Bias 0 1302 -> 0.27117 V Bias 1 1660 -> 0.342983 V, Temperature 0.0718127 -> -31.4273 C
[17:00:56:736][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0013396] MON MUX_V: 5 Bias 0 1272 -> 0.265152 V Bias 1 1631 -> 0.337165 V, Temperature 0.0720133 -> -26.2318 C
[17:00:56:740][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0013396] MON MUX_V: 7 Bias 0 1264 -> 0.263547 V Bias 1 1637 -> 0.338369 V, Temperature 0.0748216 -> -23.0164 C
[17:00:56:744][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0013396] MON MUX_V: 15 Bias 0 1260 -> 0.262745 V Bias 1 1630 -> 0.336965 V, Temperature 0.0742198 -> -27.8161 C
[17:00:56:780][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0013428 on Rx 5
[17:00:56:784][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0013428] MON MUX_V: 3 Bias 0 1368 -> 0.286986 V Bias 1 1727 -> 0.359713 V, Temperature 0.0727266 -> -22.7293 C
[17:00:56:788][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0013428] MON MUX_V: 5 Bias 0 1324 -> 0.278073 V Bias 1 1696 -> 0.353433 V, Temperature 0.0753601 -> -17.8855 C
[17:00:56:792][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0013428] MON MUX_V: 7 Bias 0 1307 -> 0.274629 V Bias 1 1683 -> 0.350799 V, Temperature 0.0761705 -> -16.9283 C
[17:00:56:797][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0013428] MON MUX_V: 15 Bias 0 1292 -> 0.27159 V Bias 1 1673 -> 0.348773 V, Temperature 0.0771834 -> -14.1193 C
[17:00:56:832][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0013381 on Rx 6
[17:00:56:836][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0013381] MON MUX_V: 3 Bias 0 1368 -> 0.292903 V Bias 1 1743 -> 0.36824 V, Temperature 0.0753368 -> -11.5687 C
[17:00:56:840][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0013381] MON MUX_V: 5 Bias 0 1331 -> 0.28547 V Bias 1 1707 -> 0.361007 V, Temperature 0.0755377 -> -21.5762 C
[17:00:56:845][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0013381] MON MUX_V: 7 Bias 0 1322 -> 0.283662 V Bias 1 1698 -> 0.359199 V, Temperature 0.0755377 -> -19.8534 C
[17:00:56:849][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0013381] MON MUX_V: 15 Bias 0 1314 -> 0.282054 V Bias 1 1691 -> 0.357793 V, Temperature 0.0757386 -> -24.2929 C
[17:00:56:884][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0013349 on Rx 7
[17:00:56:888][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0013349] MON MUX_V: 3 Bias 0 1324 -> 0.272697 V Bias 1 1677 -> 0.343694 V, Temperature 0.070997 -> -27.4955 C
[17:00:56:892][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0013349] MON MUX_V: 5 Bias 0 1289 -> 0.265658 V Bias 1 1653 -> 0.338867 V, Temperature 0.0732094 -> -22.2079 C
[17:00:56:897][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0013349] MON MUX_V: 7 Bias 0 1263 -> 0.260429 V Bias 1 1636 -> 0.335448 V, Temperature 0.0750195 -> -18.9726 C
[17:00:56:901][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0013349] MON MUX_V: 15 Bias 0 1284 -> 0.264652 V Bias 1 1649 -> 0.338063 V, Temperature 0.0734105 -> -22.6888 C
[17:00:56:937][  info  ][  scanConsole  ]: Scan done!
[17:00:56:937][  info  ][  scanConsole  ]: Waiting for processors to finish ...
[17:00:56:938][  info  ][  scanConsole  ]: Processor done, waiting for histogrammer ...
[17:00:57:338][  info  ][HistogramAlgorithm]: Histogrammer done!
[17:00:57:338][  info  ][HistogramAlgorithm]: Histogrammer done!
[17:00:57:338][  info  ][HistogramAlgorithm]: Histogrammer done!
[17:00:57:338][  info  ][HistogramAlgorithm]: Histogrammer done!
[17:00:57:338][  info  ][  scanConsole  ]: Processor done, waiting for analysis ...
[17:00:57:738][  info  ][AnalysisAlgorithm]: Analysis done!
[17:00:57:738][  info  ][AnalysisAlgorithm]: Analysis done!
[17:00:57:738][  info  ][AnalysisAlgorithm]: Analysis done!
[17:00:57:738][  info  ][AnalysisAlgorithm]: Analysis done!
[17:00:57:739][  info  ][  scanConsole  ]: All done!
[17:00:57:739][  info  ][  scanConsole  ]: ##########
[17:00:57:739][  info  ][  scanConsole  ]: # Timing #
[17:00:57:739][  info  ][  scanConsole  ]: ##########
[17:00:57:739][  info  ][  scanConsole  ]: -> Configuration: 120 ms
[17:00:57:739][  info  ][  scanConsole  ]: -> Scan:          209 ms
[17:00:57:739][  info  ][  scanConsole  ]: -> Processing:    0 ms
[17:00:57:739][  info  ][  scanConsole  ]: -> Analysis:      800 ms
[17:00:57:739][  info  ][  scanConsole  ]: ###########
[17:00:57:739][  info  ][  scanConsole  ]: # Cleanup #
[17:00:57:739][  info  ][  scanConsole  ]: ###########
[17:00:57:750][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0013396 to configs/KEKQ17/20UPGFC0013396.json
[17:00:57:920][  info  ][  scanConsole  ]: -> Storing output results of FE 4
[17:00:57:920][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0013396, this usually means that the chip did not send any data at all.
[17:00:57:922][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0013428 to configs/KEKQ17/20UPGFC0013428.json
[17:00:58:085][  info  ][  scanConsole  ]: -> Storing output results of FE 5
[17:00:58:085][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0013428, this usually means that the chip did not send any data at all.
[17:00:58:087][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0013381 to configs/KEKQ17/20UPGFC0013381.json
[17:00:58:252][  info  ][  scanConsole  ]: -> Storing output results of FE 6
[17:00:58:252][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0013381, this usually means that the chip did not send any data at all.
[17:00:58:254][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0013349 to configs/KEKQ17/20UPGFC0013349.json
[17:00:58:417][  info  ][  scanConsole  ]: -> Storing output results of FE 7
[17:00:58:417][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0013349, this usually means that the chip did not send any data at all.
[17:00:58:418][  info  ][  scanConsole  ]: Finishing run: 1865
20UPGFC0013349.json.after
20UPGFC0013349.json.before
20UPGFC0013381.json.after
20UPGFC0013381.json.before
20UPGFC0013396.json.after
20UPGFC0013396.json.before
20UPGFC0013428.json.after
20UPGFC0013428.json.before
reg_readtemp.json
scanLog.json

