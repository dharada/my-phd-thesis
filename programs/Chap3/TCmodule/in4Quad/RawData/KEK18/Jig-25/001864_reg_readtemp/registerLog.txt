[16:55:30:400][  info  ][               ]: #####################################
[16:55:30:400][  info  ][               ]: # Welcome to the YARR Scan Console! #
[16:55:30:400][  info  ][               ]: #####################################
[16:55:30:400][  info  ][               ]: -> Parsing command line parameters ...
[16:55:30:400][  info  ][               ]: Configuring logger ...
[16:55:30:402][  info  ][  scanConsole  ]: Scan Type/Config configs/scans/rd53a/reg_readtemp.json
[16:55:30:402][  info  ][  scanConsole  ]: Connectivity:
[16:55:30:403][  info  ][  scanConsole  ]:     configs/KEKQ17/connectivity.json
[16:55:30:403][  info  ][  scanConsole  ]: Target ToT: -1
[16:55:30:403][  info  ][  scanConsole  ]: Target Charge: -1
[16:55:30:403][  info  ][  scanConsole  ]: Output Plots: true
[16:55:30:403][  info  ][  scanConsole  ]: Output Directory: ./data/001864_reg_readtemp/
[16:55:30:408][  info  ][  scanConsole  ]: Timestamp: 2022-08-26_16:55:30
[16:55:30:408][  info  ][  scanConsole  ]: Run Number: 1864
[16:55:30:408][  info  ][  scanConsole  ]: #################
[16:55:30:408][  info  ][  scanConsole  ]: # Init Hardware #
[16:55:30:408][  info  ][  scanConsole  ]: #################
[16:55:30:408][  info  ][  scanConsole  ]: -> Opening controller config: configs/controller/specCfg-rd53a-16x1.json
[16:55:30:408][  info  ][  ScanHelper   ]: Loading controller ...
[16:55:30:408][  info  ][  ScanHelper   ]: Found controller of type: spec
[16:55:30:408][  info  ][  ScanHelper   ]: ... loading controler config:
[16:55:30:408][  info  ][  ScanHelper   ]: ~~~ {
[16:55:30:408][  info  ][  ScanHelper   ]: ~~~     "cmdPeriod": 6.250000073038109e-9,
[16:55:30:408][  info  ][  ScanHelper   ]: ~~~     "idle": {
[16:55:30:408][  info  ][  ScanHelper   ]: ~~~         "word": 1768515945
[16:55:30:408][  info  ][  ScanHelper   ]: ~~~     },
[16:55:30:408][  info  ][  ScanHelper   ]: ~~~     "pulse": {
[16:55:30:408][  info  ][  ScanHelper   ]: ~~~         "interval": 500,
[16:55:30:408][  info  ][  ScanHelper   ]: ~~~         "word": 1549575846
[16:55:30:408][  info  ][  ScanHelper   ]: ~~~     },
[16:55:30:408][  info  ][  ScanHelper   ]: ~~~     "rxActiveLanes": 1,
[16:55:30:408][  info  ][  ScanHelper   ]: ~~~     "rxPolarity": 0,
[16:55:30:408][  info  ][  ScanHelper   ]: ~~~     "specNum": 0,
[16:55:30:408][  info  ][  ScanHelper   ]: ~~~     "spiConfig": 541200,
[16:55:30:408][  info  ][  ScanHelper   ]: ~~~     "sync": {
[16:55:30:408][  info  ][  ScanHelper   ]: ~~~         "interval": 32,
[16:55:30:408][  info  ][  ScanHelper   ]: ~~~         "word": 2172551550
[16:55:30:408][  info  ][  ScanHelper   ]: ~~~     },
[16:55:30:408][  info  ][  ScanHelper   ]: ~~~     "txPolarity": 0
[16:55:30:408][  info  ][  ScanHelper   ]: ~~~ }
[16:55:30:408][  info  ][    SpecCom    ]: Opening SPEC with id #0
[16:55:30:408][  info  ][    SpecCom    ]: Mapping BARs ...
[16:55:30:408][  info  ][    SpecCom    ]: ... Mapped BAR0 at 0x7fb352414000 with size 1048576
[16:55:30:408][warning ][    SpecCom    ]: ... BAR4 not mapped (Mmap failed)
[16:55:30:408][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[16:55:30:408][  info  ][    SpecCom    ]: Firmware Version: 0x4d9ff6d
[16:55:30:408][  info  ][    SpecCom    ]: Firmware Identifier: 0x4030232
[16:55:30:408][  info  ][    SpecCom    ]: FPGA card: PLDA XpressK7 325
[16:55:30:408][  info  ][    SpecCom    ]: FE Chip Type: RD53A/B
[16:55:30:408][  info  ][    SpecCom    ]: FMC Card Type: Ohio Card (Display Port)
[16:55:30:408][  info  ][    SpecCom    ]: RX Speed: 640Mbps
[16:55:30:408][  info  ][    SpecCom    ]: Channel Configuration: 16x1
[16:55:30:408][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[16:55:30:408][  info  ][    SpecCom    ]: Flushing buffers ...
[16:55:30:408][  info  ][    SpecCom    ]: Init success!
[16:55:30:408][  info  ][  scanConsole  ]: #######################
[16:55:30:408][  info  ][  scanConsole  ]: ##  Loading Configs  ##
[16:55:30:408][  info  ][  scanConsole  ]: #######################
[16:55:30:408][  info  ][  scanConsole  ]: Opening global config: configs/KEKQ17/connectivity.json
[16:55:30:408][  info  ][  ScanHelper   ]: Chip type: RD53A
[16:55:30:408][  info  ][  ScanHelper   ]: Chip count 4
[16:55:30:408][  info  ][  ScanHelper   ]: Loading chip #0
[16:55:30:409][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(4)
[16:55:30:409][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ17/20UPGFC0013396.json
[16:55:30:584][  info  ][  ScanHelper   ]: Loading chip #1
[16:55:30:585][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(5)
[16:55:30:585][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ17/20UPGFC0013428.json
[16:55:30:759][  info  ][  ScanHelper   ]: Loading chip #2
[16:55:30:759][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(6)
[16:55:30:759][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ17/20UPGFC0013381.json
[16:55:30:934][  info  ][  ScanHelper   ]: Loading chip #3
[16:55:30:935][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(7)
[16:55:30:935][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ17/20UPGFC0013349.json
[16:55:31:111][  info  ][  scanConsole  ]: #################
[16:55:31:111][  info  ][  scanConsole  ]: # Configure FEs #
[16:55:31:111][  info  ][  scanConsole  ]: #################
[16:55:31:112][  info  ][  scanConsole  ]: Configuring 20UPGFC0013396
[16:55:31:142][  info  ][  scanConsole  ]: Configuring 20UPGFC0013428
[16:55:31:173][  info  ][  scanConsole  ]: Configuring 20UPGFC0013381
[16:55:31:214][  info  ][  scanConsole  ]: Configuring 20UPGFC0013349
[16:55:31:244][  info  ][  scanConsole  ]: Sent configuration to all FEs in 132 ms!
[16:55:31:245][  info  ][  scanConsole  ]: Checking com 20UPGFC0013396
[16:55:31:245][  info  ][    SpecRx     ]: Active Rx channels: 0x10
[16:55:31:245][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[16:55:31:245][  info  ][    SpecRx     ]: Rx Status 0xf0
[16:55:31:245][  info  ][    SpecRx     ]: Number of lanes: 1
[16:55:31:245][  info  ][    SpecRx     ]: Channel 4 Lane 0 synchronized!
[16:55:31:255][  info  ][  scanConsole  ]: ... success!
[16:55:31:255][  info  ][  scanConsole  ]: Checking com 20UPGFC0013428
[16:55:31:255][  info  ][    SpecRx     ]: Active Rx channels: 0x20
[16:55:31:255][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[16:55:31:255][  info  ][    SpecRx     ]: Rx Status 0xf0
[16:55:31:255][  info  ][    SpecRx     ]: Number of lanes: 1
[16:55:31:255][  info  ][    SpecRx     ]: Channel 5 Lane 0 synchronized!
[16:55:31:266][  info  ][  scanConsole  ]: ... success!
[16:55:31:266][  info  ][  scanConsole  ]: Checking com 20UPGFC0013381
[16:55:31:266][  info  ][    SpecRx     ]: Active Rx channels: 0x40
[16:55:31:266][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[16:55:31:266][  info  ][    SpecRx     ]: Rx Status 0xf0
[16:55:31:266][  info  ][    SpecRx     ]: Number of lanes: 1
[16:55:31:266][  info  ][    SpecRx     ]: Channel 6 Lane 0 synchronized!
[16:55:31:276][  info  ][  scanConsole  ]: ... success!
[16:55:31:276][  info  ][  scanConsole  ]: Checking com 20UPGFC0013349
[16:55:31:276][  info  ][    SpecRx     ]: Active Rx channels: 0x80
[16:55:31:276][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[16:55:31:276][  info  ][    SpecRx     ]: Rx Status 0xf0
[16:55:31:276][  info  ][    SpecRx     ]: Number of lanes: 1
[16:55:31:276][  info  ][    SpecRx     ]: Channel 7 Lane 0 synchronized!
[16:55:31:287][  info  ][  scanConsole  ]: ... success!
[16:55:31:287][  info  ][  scanConsole  ]: Enabling Tx channels
[16:55:31:287][  info  ][  scanConsole  ]: Enabling Tx channel 1
[16:55:31:287][  info  ][  scanConsole  ]: Enabling Tx channel 1
[16:55:31:287][  info  ][  scanConsole  ]: Enabling Tx channel 1
[16:55:31:287][  info  ][  scanConsole  ]: Enabling Tx channel 1
[16:55:31:287][  info  ][  scanConsole  ]: Enabling Rx channels
[16:55:31:287][  info  ][  scanConsole  ]: Enabling Rx channel 4
[16:55:31:287][  info  ][  scanConsole  ]: Enabling Rx channel 5
[16:55:31:287][  info  ][  scanConsole  ]: Enabling Rx channel 6
[16:55:31:287][  info  ][  scanConsole  ]: Enabling Rx channel 7
[16:55:31:287][  info  ][  scanConsole  ]: ##############
[16:55:31:287][  info  ][  scanConsole  ]: # Setup Scan #
[16:55:31:287][  info  ][  scanConsole  ]: ##############
[16:55:31:288][  info  ][  scanConsole  ]: Found Scan config, constructing scan ...
[16:55:31:288][  info  ][  ScanFactory  ]: Loading Scan:
[16:55:31:288][  info  ][  ScanFactory  ]:   Name: AnalogScan
[16:55:31:288][  info  ][  ScanFactory  ]:   Number of Loops: 3
[16:55:31:288][  info  ][  ScanFactory  ]:   Loading Loop #0
[16:55:31:288][  info  ][  ScanFactory  ]:    Type: Rd53aReadRegLoop
[16:55:31:288][  info  ][  ScanFactory  ]:    Loading loop config ... 
[16:55:31:288][  info  ][  ScanFactory  ]: ~~~ {
[16:55:31:288][  info  ][  ScanFactory  ]: ~~~     "CurMux": [],
[16:55:31:288][  info  ][  ScanFactory  ]: ~~~     "EnblRingOsc": 0,
[16:55:31:288][  info  ][  ScanFactory  ]: ~~~     "Registers": [],
[16:55:31:288][  info  ][  ScanFactory  ]: ~~~     "RingOscDur": 9,
[16:55:31:288][  info  ][  ScanFactory  ]: ~~~     "RingOscRep": 10,
[16:55:31:288][  info  ][  ScanFactory  ]: ~~~     "VoltMux": [3, 5, 7, 15]
[16:55:31:288][  info  ][  ScanFactory  ]: ~~~ }
[16:55:31:289][  info  ][  ScanFactory  ]:   Loading Loop #1
[16:55:31:289][  info  ][  ScanFactory  ]:    Type: Rd53aTriggerLoop
[16:55:31:289][  info  ][  ScanFactory  ]:    Loading loop config ... 
[16:55:31:289][  info  ][  ScanFactory  ]: ~~~ {
[16:55:31:289][  info  ][  ScanFactory  ]: ~~~     "count": 1,
[16:55:31:289][  info  ][  ScanFactory  ]: ~~~     "delay": 48,
[16:55:31:289][  info  ][  ScanFactory  ]: ~~~     "extTrig": false,
[16:55:31:289][  info  ][  ScanFactory  ]: ~~~     "frequency": 30000.0,
[16:55:31:289][  info  ][  ScanFactory  ]: ~~~     "noInject": false,
[16:55:31:289][  info  ][  ScanFactory  ]: ~~~     "sendEcr": false,
[16:55:31:289][  info  ][  ScanFactory  ]: ~~~     "time": 0.0,
[16:55:31:289][  info  ][  ScanFactory  ]: ~~~     "trigMultiplier": 16
[16:55:31:289][  info  ][  ScanFactory  ]: ~~~ }
[16:55:31:289][  info  ][  ScanFactory  ]:   Loading Loop #2
[16:55:31:289][  info  ][  ScanFactory  ]:    Type: StdDataLoop
[16:55:31:289][warning ][  ScanFactory  ]: ~~~ Config empty.
[16:55:31:289][  info  ][ScanBuildHistogrammers]: Loading histogrammer ...
[16:55:31:289][  info  ][ScanBuildHistogrammers]: ... done!
[16:55:31:289][  info  ][ScanBuildAnalyses]: Loading analyses ...
[16:55:31:290][  info  ][  scanConsole  ]: Running pre scan!
[16:55:31:290][  info  ][  ScanFactory  ]: Entering pre scan phase ...
[16:55:31:290][  info  ][     Rd53a     ]: Write named register: GlobalPulseRt -> 16384
[16:55:31:290][  info  ][     Rd53a     ]: Write named register: InjEnDig -> 0
[16:55:31:290][  info  ][     Rd53a     ]: Write named register: InjVcalDiff -> 0
[16:55:31:290][  info  ][     Rd53a     ]: Write named register: InjVcalHigh -> 3500
[16:55:31:290][  info  ][     Rd53a     ]: Write named register: InjVcalMed -> 500
[16:55:31:290][  info  ][     Rd53a     ]: Write named register: LatencyConfig -> 48
[16:55:31:290][  info  ][  scanConsole  ]: Starting histogrammer and analysis threads:
[16:55:31:290][  info  ][  scanConsole  ]:  .. started threads of Fe 4
[16:55:31:290][  info  ][  scanConsole  ]:  .. started threads of Fe 5
[16:55:31:290][  info  ][  scanConsole  ]:  .. started threads of Fe 6
[16:55:31:291][  info  ][  scanConsole  ]:  .. started threads of Fe 7
[16:55:31:291][  info  ][Rd53aDataProcessor]:   -> Processor thread #0 started!
[16:55:31:291][  info  ][Rd53aDataProcessor]:   -> Processor thread #1 started!
[16:55:31:291][  info  ][Rd53aDataProcessor]:   -> Processor thread #2 started!
[16:55:31:291][  info  ][Rd53aDataProcessor]:   -> Processor thread #3 started!
[16:55:31:291][  info  ][  scanConsole  ]: ########
[16:55:31:291][  info  ][  scanConsole  ]: # Scan #
[16:55:31:291][  info  ][  scanConsole  ]: ########
[16:55:31:291][  info  ][  scanConsole  ]: Starting scan!
[16:55:31:291][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0013396 on Rx 4
[16:55:31:295][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0013396] MON MUX_V: 3 Bias 0 1300 -> 0.270769 V Bias 1 1659 -> 0.342782 V, Temperature 0.0720133 -> -30.7521 C
[16:55:31:300][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0013396] MON MUX_V: 5 Bias 0 1272 -> 0.265152 V Bias 1 1633 -> 0.337567 V, Temperature 0.0724145 -> -24.8562 C
[16:55:31:304][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0013396] MON MUX_V: 7 Bias 0 1263 -> 0.263347 V Bias 1 1636 -> 0.338168 V, Temperature 0.0748216 -> -23.0164 C
[16:55:31:308][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0013396] MON MUX_V: 15 Bias 0 1258 -> 0.262344 V Bias 1 1630 -> 0.336965 V, Temperature 0.074621 -> -26.49 C
[16:55:31:343][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0013428 on Rx 5
[16:55:31:347][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0013428] MON MUX_V: 3 Bias 0 1366 -> 0.286581 V Bias 1 1726 -> 0.35951 V, Temperature 0.0729292 -> -22.0319 C
[16:55:31:351][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0013428] MON MUX_V: 5 Bias 0 1322 -> 0.277667 V Bias 1 1694 -> 0.353028 V, Temperature 0.0753601 -> -17.8855 C
[16:55:31:355][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0013428] MON MUX_V: 7 Bias 0 1305 -> 0.274224 V Bias 1 1682 -> 0.350597 V, Temperature 0.0763731 -> -16.2469 C
[16:55:31:359][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0013428] MON MUX_V: 15 Bias 0 1290 -> 0.271185 V Bias 1 1673 -> 0.348773 V, Temperature 0.0775886 -> -12.7596 C
[16:55:31:394][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0013381 on Rx 6
[16:55:31:398][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0013381] MON MUX_V: 3 Bias 0 1366 -> 0.292501 V Bias 1 1741 -> 0.367838 V, Temperature 0.0753368 -> -11.5687 C
[16:55:31:402][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0013381] MON MUX_V: 5 Bias 0 1329 -> 0.285068 V Bias 1 1705 -> 0.360606 V, Temperature 0.0755377 -> -21.5762 C
[16:55:31:407][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0013381] MON MUX_V: 7 Bias 0 1319 -> 0.283059 V Bias 1 1696 -> 0.358797 V, Temperature 0.0757386 -> -19.1796 C
[16:55:31:411][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0013381] MON MUX_V: 15 Bias 0 1312 -> 0.281653 V Bias 1 1689 -> 0.357391 V, Temperature 0.0757386 -> -24.2931 C
[16:55:31:446][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0013349 on Rx 7
[16:55:31:450][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0013349] MON MUX_V: 3 Bias 0 1322 -> 0.272295 V Bias 1 1676 -> 0.343493 V, Temperature 0.0711981 -> -26.7995 C
[16:55:31:454][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0013349] MON MUX_V: 5 Bias 0 1287 -> 0.265256 V Bias 1 1652 -> 0.338666 V, Temperature 0.0734105 -> -21.5186 C
[16:55:31:458][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0013349] MON MUX_V: 7 Bias 0 1261 -> 0.260027 V Bias 1 1635 -> 0.335247 V, Temperature 0.0752206 -> -18.2912 C
[16:55:31:462][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0013349] MON MUX_V: 15 Bias 0 1282 -> 0.26425 V Bias 1 1648 -> 0.337862 V, Temperature 0.0736116 -> -22.0027 C
[16:55:31:499][  info  ][  scanConsole  ]: Scan done!
[16:55:31:499][  info  ][  scanConsole  ]: Waiting for processors to finish ...
[16:55:31:500][  info  ][  scanConsole  ]: Processor done, waiting for histogrammer ...
[16:55:31:900][  info  ][HistogramAlgorithm]: Histogrammer done!
[16:55:31:900][  info  ][HistogramAlgorithm]: Histogrammer done!
[16:55:31:900][  info  ][HistogramAlgorithm]: Histogrammer done!
[16:55:31:900][  info  ][HistogramAlgorithm]: Histogrammer done!
[16:55:31:900][  info  ][  scanConsole  ]: Processor done, waiting for analysis ...
[16:55:32:300][  info  ][AnalysisAlgorithm]: Analysis done!
[16:55:32:300][  info  ][AnalysisAlgorithm]: Analysis done!
[16:55:32:300][  info  ][AnalysisAlgorithm]: Analysis done!
[16:55:32:300][  info  ][AnalysisAlgorithm]: Analysis done!
[16:55:32:301][  info  ][  scanConsole  ]: All done!
[16:55:32:301][  info  ][  scanConsole  ]: ##########
[16:55:32:301][  info  ][  scanConsole  ]: # Timing #
[16:55:32:301][  info  ][  scanConsole  ]: ##########
[16:55:32:301][  info  ][  scanConsole  ]: -> Configuration: 132 ms
[16:55:32:301][  info  ][  scanConsole  ]: -> Scan:          207 ms
[16:55:32:301][  info  ][  scanConsole  ]: -> Processing:    1 ms
[16:55:32:301][  info  ][  scanConsole  ]: -> Analysis:      800 ms
[16:55:32:301][  info  ][  scanConsole  ]: ###########
[16:55:32:301][  info  ][  scanConsole  ]: # Cleanup #
[16:55:32:301][  info  ][  scanConsole  ]: ###########
[16:55:32:309][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0013396 to configs/KEKQ17/20UPGFC0013396.json
[16:55:32:477][  info  ][  scanConsole  ]: -> Storing output results of FE 4
[16:55:32:477][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0013396, this usually means that the chip did not send any data at all.
[16:55:32:478][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0013428 to configs/KEKQ17/20UPGFC0013428.json
[16:55:32:640][  info  ][  scanConsole  ]: -> Storing output results of FE 5
[16:55:32:640][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0013428, this usually means that the chip did not send any data at all.
[16:55:32:642][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0013381 to configs/KEKQ17/20UPGFC0013381.json
[16:55:32:804][  info  ][  scanConsole  ]: -> Storing output results of FE 6
[16:55:32:804][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0013381, this usually means that the chip did not send any data at all.
[16:55:32:805][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0013349 to configs/KEKQ17/20UPGFC0013349.json
[16:55:32:968][  info  ][  scanConsole  ]: -> Storing output results of FE 7
[16:55:32:968][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0013349, this usually means that the chip did not send any data at all.
[16:55:32:969][  info  ][  scanConsole  ]: Finishing run: 1864
20UPGFC0013349.json.after
20UPGFC0013349.json.before
20UPGFC0013381.json.after
20UPGFC0013381.json.before
20UPGFC0013396.json.after
20UPGFC0013396.json.before
20UPGFC0013428.json.after
20UPGFC0013428.json.before
reg_readtemp.json
scanLog.json

