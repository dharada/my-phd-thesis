[17:04:55:630][  info  ][               ]: #####################################
[17:04:55:630][  info  ][               ]: # Welcome to the YARR Scan Console! #
[17:04:55:630][  info  ][               ]: #####################################
[17:04:55:630][  info  ][               ]: -> Parsing command line parameters ...
[17:04:55:630][  info  ][               ]: Configuring logger ...
[17:04:55:632][  info  ][  scanConsole  ]: Scan Type/Config configs/scans/rd53a/reg_readtemp.json
[17:04:55:632][  info  ][  scanConsole  ]: Connectivity:
[17:04:55:632][  info  ][  scanConsole  ]:     configs/KEKQ17/connectivity.json
[17:04:55:632][  info  ][  scanConsole  ]: Target ToT: -1
[17:04:55:632][  info  ][  scanConsole  ]: Target Charge: -1
[17:04:55:632][  info  ][  scanConsole  ]: Output Plots: true
[17:04:55:632][  info  ][  scanConsole  ]: Output Directory: ./data/001867_reg_readtemp/
[17:04:55:637][  info  ][  scanConsole  ]: Timestamp: 2022-08-26_17:04:55
[17:04:55:637][  info  ][  scanConsole  ]: Run Number: 1867
[17:04:55:637][  info  ][  scanConsole  ]: #################
[17:04:55:637][  info  ][  scanConsole  ]: # Init Hardware #
[17:04:55:637][  info  ][  scanConsole  ]: #################
[17:04:55:637][  info  ][  scanConsole  ]: -> Opening controller config: configs/controller/specCfg-rd53a-16x1.json
[17:04:55:637][  info  ][  ScanHelper   ]: Loading controller ...
[17:04:55:637][  info  ][  ScanHelper   ]: Found controller of type: spec
[17:04:55:637][  info  ][  ScanHelper   ]: ... loading controler config:
[17:04:55:637][  info  ][  ScanHelper   ]: ~~~ {
[17:04:55:637][  info  ][  ScanHelper   ]: ~~~     "cmdPeriod": 6.250000073038109e-9,
[17:04:55:637][  info  ][  ScanHelper   ]: ~~~     "idle": {
[17:04:55:637][  info  ][  ScanHelper   ]: ~~~         "word": 1768515945
[17:04:55:637][  info  ][  ScanHelper   ]: ~~~     },
[17:04:55:637][  info  ][  ScanHelper   ]: ~~~     "pulse": {
[17:04:55:637][  info  ][  ScanHelper   ]: ~~~         "interval": 500,
[17:04:55:637][  info  ][  ScanHelper   ]: ~~~         "word": 1549575846
[17:04:55:637][  info  ][  ScanHelper   ]: ~~~     },
[17:04:55:637][  info  ][  ScanHelper   ]: ~~~     "rxActiveLanes": 1,
[17:04:55:637][  info  ][  ScanHelper   ]: ~~~     "rxPolarity": 0,
[17:04:55:637][  info  ][  ScanHelper   ]: ~~~     "specNum": 0,
[17:04:55:637][  info  ][  ScanHelper   ]: ~~~     "spiConfig": 541200,
[17:04:55:637][  info  ][  ScanHelper   ]: ~~~     "sync": {
[17:04:55:637][  info  ][  ScanHelper   ]: ~~~         "interval": 32,
[17:04:55:637][  info  ][  ScanHelper   ]: ~~~         "word": 2172551550
[17:04:55:637][  info  ][  ScanHelper   ]: ~~~     },
[17:04:55:637][  info  ][  ScanHelper   ]: ~~~     "txPolarity": 0
[17:04:55:637][  info  ][  ScanHelper   ]: ~~~ }
[17:04:55:637][  info  ][    SpecCom    ]: Opening SPEC with id #0
[17:04:55:637][  info  ][    SpecCom    ]: Mapping BARs ...
[17:04:55:637][  info  ][    SpecCom    ]: ... Mapped BAR0 at 0x7f77c5549000 with size 1048576
[17:04:55:637][warning ][    SpecCom    ]: ... BAR4 not mapped (Mmap failed)
[17:04:55:638][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[17:04:55:638][  info  ][    SpecCom    ]: Firmware Version: 0x4d9ff6d
[17:04:55:638][  info  ][    SpecCom    ]: Firmware Identifier: 0x4030232
[17:04:55:638][  info  ][    SpecCom    ]: FPGA card: PLDA XpressK7 325
[17:04:55:638][  info  ][    SpecCom    ]: FE Chip Type: RD53A/B
[17:04:55:638][  info  ][    SpecCom    ]: FMC Card Type: Ohio Card (Display Port)
[17:04:55:638][  info  ][    SpecCom    ]: RX Speed: 640Mbps
[17:04:55:638][  info  ][    SpecCom    ]: Channel Configuration: 16x1
[17:04:55:638][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[17:04:55:638][  info  ][    SpecCom    ]: Flushing buffers ...
[17:04:55:638][  info  ][    SpecCom    ]: Init success!
[17:04:55:638][  info  ][  scanConsole  ]: #######################
[17:04:55:638][  info  ][  scanConsole  ]: ##  Loading Configs  ##
[17:04:55:638][  info  ][  scanConsole  ]: #######################
[17:04:55:638][  info  ][  scanConsole  ]: Opening global config: configs/KEKQ17/connectivity.json
[17:04:55:638][  info  ][  ScanHelper   ]: Chip type: RD53A
[17:04:55:638][  info  ][  ScanHelper   ]: Chip count 4
[17:04:55:638][  info  ][  ScanHelper   ]: Loading chip #0
[17:04:55:638][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(4)
[17:04:55:638][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ17/20UPGFC0013396.json
[17:04:55:825][  info  ][  ScanHelper   ]: Loading chip #1
[17:04:55:825][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(5)
[17:04:55:825][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ17/20UPGFC0013428.json
[17:04:56:006][  info  ][  ScanHelper   ]: Loading chip #2
[17:04:56:007][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(6)
[17:04:56:007][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ17/20UPGFC0013381.json
[17:04:56:188][  info  ][  ScanHelper   ]: Loading chip #3
[17:04:56:189][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(7)
[17:04:56:189][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ17/20UPGFC0013349.json
[17:04:56:382][  info  ][  scanConsole  ]: #################
[17:04:56:382][  info  ][  scanConsole  ]: # Configure FEs #
[17:04:56:382][  info  ][  scanConsole  ]: #################
[17:04:56:382][  info  ][  scanConsole  ]: Configuring 20UPGFC0013396
[17:04:56:412][  info  ][  scanConsole  ]: Configuring 20UPGFC0013428
[17:04:56:448][  info  ][  scanConsole  ]: Configuring 20UPGFC0013381
[17:04:56:484][  info  ][  scanConsole  ]: Configuring 20UPGFC0013349
[17:04:56:522][  info  ][  scanConsole  ]: Sent configuration to all FEs in 140 ms!
[17:04:56:523][  info  ][  scanConsole  ]: Checking com 20UPGFC0013396
[17:04:56:523][  info  ][    SpecRx     ]: Active Rx channels: 0x10
[17:04:56:523][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[17:04:56:523][  info  ][    SpecRx     ]: Rx Status 0xf0
[17:04:56:523][  info  ][    SpecRx     ]: Number of lanes: 1
[17:04:56:523][  info  ][    SpecRx     ]: Channel 4 Lane 0 synchronized!
[17:04:56:534][  info  ][  scanConsole  ]: ... success!
[17:04:56:534][  info  ][  scanConsole  ]: Checking com 20UPGFC0013428
[17:04:56:534][  info  ][    SpecRx     ]: Active Rx channels: 0x20
[17:04:56:534][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[17:04:56:534][  info  ][    SpecRx     ]: Rx Status 0xf0
[17:04:56:534][  info  ][    SpecRx     ]: Number of lanes: 1
[17:04:56:534][  info  ][    SpecRx     ]: Channel 5 Lane 0 synchronized!
[17:04:56:544][  info  ][  scanConsole  ]: ... success!
[17:04:56:544][  info  ][  scanConsole  ]: Checking com 20UPGFC0013381
[17:04:56:544][  info  ][    SpecRx     ]: Active Rx channels: 0x40
[17:04:56:544][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[17:04:56:544][  info  ][    SpecRx     ]: Rx Status 0xf0
[17:04:56:544][  info  ][    SpecRx     ]: Number of lanes: 1
[17:04:56:544][  info  ][    SpecRx     ]: Channel 6 Lane 0 synchronized!
[17:04:56:555][  info  ][  scanConsole  ]: ... success!
[17:04:56:555][  info  ][  scanConsole  ]: Checking com 20UPGFC0013349
[17:04:56:555][  info  ][    SpecRx     ]: Active Rx channels: 0x80
[17:04:56:555][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[17:04:56:555][  info  ][    SpecRx     ]: Rx Status 0xf0
[17:04:56:555][  info  ][    SpecRx     ]: Number of lanes: 1
[17:04:56:555][  info  ][    SpecRx     ]: Channel 7 Lane 0 synchronized!
[17:04:56:565][  info  ][  scanConsole  ]: ... success!
[17:04:56:565][  info  ][  scanConsole  ]: Enabling Tx channels
[17:04:56:565][  info  ][  scanConsole  ]: Enabling Tx channel 1
[17:04:56:565][  info  ][  scanConsole  ]: Enabling Tx channel 1
[17:04:56:565][  info  ][  scanConsole  ]: Enabling Tx channel 1
[17:04:56:565][  info  ][  scanConsole  ]: Enabling Tx channel 1
[17:04:56:565][  info  ][  scanConsole  ]: Enabling Rx channels
[17:04:56:565][  info  ][  scanConsole  ]: Enabling Rx channel 4
[17:04:56:565][  info  ][  scanConsole  ]: Enabling Rx channel 5
[17:04:56:565][  info  ][  scanConsole  ]: Enabling Rx channel 6
[17:04:56:565][  info  ][  scanConsole  ]: Enabling Rx channel 7
[17:04:56:565][  info  ][  scanConsole  ]: ##############
[17:04:56:565][  info  ][  scanConsole  ]: # Setup Scan #
[17:04:56:566][  info  ][  scanConsole  ]: ##############
[17:04:56:566][  info  ][  scanConsole  ]: Found Scan config, constructing scan ...
[17:04:56:566][  info  ][  ScanFactory  ]: Loading Scan:
[17:04:56:566][  info  ][  ScanFactory  ]:   Name: AnalogScan
[17:04:56:566][  info  ][  ScanFactory  ]:   Number of Loops: 3
[17:04:56:566][  info  ][  ScanFactory  ]:   Loading Loop #0
[17:04:56:566][  info  ][  ScanFactory  ]:    Type: Rd53aReadRegLoop
[17:04:56:566][  info  ][  ScanFactory  ]:    Loading loop config ... 
[17:04:56:567][  info  ][  ScanFactory  ]: ~~~ {
[17:04:56:567][  info  ][  ScanFactory  ]: ~~~     "CurMux": [],
[17:04:56:567][  info  ][  ScanFactory  ]: ~~~     "EnblRingOsc": 0,
[17:04:56:567][  info  ][  ScanFactory  ]: ~~~     "Registers": [],
[17:04:56:567][  info  ][  ScanFactory  ]: ~~~     "RingOscDur": 9,
[17:04:56:567][  info  ][  ScanFactory  ]: ~~~     "RingOscRep": 10,
[17:04:56:567][  info  ][  ScanFactory  ]: ~~~     "VoltMux": [3, 5, 7, 15]
[17:04:56:567][  info  ][  ScanFactory  ]: ~~~ }
[17:04:56:567][  info  ][  ScanFactory  ]:   Loading Loop #1
[17:04:56:567][  info  ][  ScanFactory  ]:    Type: Rd53aTriggerLoop
[17:04:56:567][  info  ][  ScanFactory  ]:    Loading loop config ... 
[17:04:56:567][  info  ][  ScanFactory  ]: ~~~ {
[17:04:56:567][  info  ][  ScanFactory  ]: ~~~     "count": 1,
[17:04:56:567][  info  ][  ScanFactory  ]: ~~~     "delay": 48,
[17:04:56:567][  info  ][  ScanFactory  ]: ~~~     "extTrig": false,
[17:04:56:567][  info  ][  ScanFactory  ]: ~~~     "frequency": 30000.0,
[17:04:56:567][  info  ][  ScanFactory  ]: ~~~     "noInject": false,
[17:04:56:567][  info  ][  ScanFactory  ]: ~~~     "sendEcr": false,
[17:04:56:567][  info  ][  ScanFactory  ]: ~~~     "time": 0.0,
[17:04:56:567][  info  ][  ScanFactory  ]: ~~~     "trigMultiplier": 16
[17:04:56:567][  info  ][  ScanFactory  ]: ~~~ }
[17:04:56:567][  info  ][  ScanFactory  ]:   Loading Loop #2
[17:04:56:567][  info  ][  ScanFactory  ]:    Type: StdDataLoop
[17:04:56:567][warning ][  ScanFactory  ]: ~~~ Config empty.
[17:04:56:567][  info  ][ScanBuildHistogrammers]: Loading histogrammer ...
[17:04:56:567][  info  ][ScanBuildHistogrammers]: ... done!
[17:04:56:567][  info  ][ScanBuildAnalyses]: Loading analyses ...
[17:04:56:567][  info  ][  scanConsole  ]: Running pre scan!
[17:04:56:567][  info  ][  ScanFactory  ]: Entering pre scan phase ...
[17:04:56:567][  info  ][     Rd53a     ]: Write named register: GlobalPulseRt -> 16384
[17:04:56:568][  info  ][     Rd53a     ]: Write named register: InjEnDig -> 0
[17:04:56:568][  info  ][     Rd53a     ]: Write named register: InjVcalDiff -> 0
[17:04:56:568][  info  ][     Rd53a     ]: Write named register: InjVcalHigh -> 3500
[17:04:56:568][  info  ][     Rd53a     ]: Write named register: InjVcalMed -> 500
[17:04:56:568][  info  ][     Rd53a     ]: Write named register: LatencyConfig -> 48
[17:04:56:568][  info  ][  scanConsole  ]: Starting histogrammer and analysis threads:
[17:04:56:568][  info  ][  scanConsole  ]:  .. started threads of Fe 4
[17:04:56:568][  info  ][  scanConsole  ]:  .. started threads of Fe 5
[17:04:56:568][  info  ][  scanConsole  ]:  .. started threads of Fe 6
[17:04:56:568][  info  ][  scanConsole  ]:  .. started threads of Fe 7
[17:04:56:568][  info  ][Rd53aDataProcessor]:   -> Processor thread #0 started!
[17:04:56:568][  info  ][Rd53aDataProcessor]:   -> Processor thread #1 started!
[17:04:56:568][  info  ][Rd53aDataProcessor]:   -> Processor thread #2 started!
[17:04:56:569][  info  ][Rd53aDataProcessor]:   -> Processor thread #3 started!
[17:04:56:569][  info  ][  scanConsole  ]: ########
[17:04:56:569][  info  ][  scanConsole  ]: # Scan #
[17:04:56:569][  info  ][  scanConsole  ]: ########
[17:04:56:569][  info  ][  scanConsole  ]: Starting scan!
[17:04:56:569][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0013396 on Rx 4
[17:04:56:573][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0013396] MON MUX_V: 3 Bias 0 1302 -> 0.27117 V Bias 1 1661 -> 0.343183 V, Temperature 0.0720133 -> -30.7521 C
[17:04:56:577][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0013396] MON MUX_V: 5 Bias 0 1275 -> 0.265754 V Bias 1 1632 -> 0.337366 V, Temperature 0.0716121 -> -27.6073 C
[17:04:56:581][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0013396] MON MUX_V: 7 Bias 0 1265 -> 0.263748 V Bias 1 1637 -> 0.338369 V, Temperature 0.074621 -> -23.6871 C
[17:04:56:585][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0013396] MON MUX_V: 15 Bias 0 1262 -> 0.263146 V Bias 1 1633 -> 0.337567 V, Temperature 0.0744204 -> -27.1531 C
[17:04:56:620][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0013428 on Rx 5
[17:04:56:624][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0013428] MON MUX_V: 3 Bias 0 1369 -> 0.287189 V Bias 1 1727 -> 0.359713 V, Temperature 0.072524 -> -23.4269 C
[17:04:56:629][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0013428] MON MUX_V: 5 Bias 0 1326 -> 0.278478 V Bias 1 1696 -> 0.353433 V, Temperature 0.074955 -> -19.2579 C
[17:04:56:633][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0013428] MON MUX_V: 7 Bias 0 1309 -> 0.275034 V Bias 1 1685 -> 0.351204 V, Temperature 0.0761705 -> -16.9283 C
[17:04:56:637][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0013428] MON MUX_V: 15 Bias 0 1294 -> 0.271995 V Bias 1 1674 -> 0.348976 V, Temperature 0.0769808 -> -14.7992 C
[17:04:56:672][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0013381 on Rx 6
[17:04:56:676][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0013381] MON MUX_V: 3 Bias 0 1369 -> 0.293104 V Bias 1 1743 -> 0.36824 V, Temperature 0.0751359 -> -12.2663 C
[17:04:56:681][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0013381] MON MUX_V: 5 Bias 0 1331 -> 0.28547 V Bias 1 1707 -> 0.361007 V, Temperature 0.0755377 -> -21.5762 C
[17:04:56:685][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0013381] MON MUX_V: 7 Bias 0 1323 -> 0.283863 V Bias 1 1699 -> 0.3594 V, Temperature 0.0755377 -> -19.8532 C
[17:04:56:690][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0013381] MON MUX_V: 15 Bias 0 1315 -> 0.282255 V Bias 1 1693 -> 0.358195 V, Temperature 0.0759395 -> -23.6329 C
[17:04:56:724][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0013349 on Rx 7
[17:04:56:729][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0013349] MON MUX_V: 3 Bias 0 1324 -> 0.272697 V Bias 1 1679 -> 0.344097 V, Temperature 0.0713993 -> -26.1036 C
[17:04:56:733][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0013349] MON MUX_V: 5 Bias 0 1289 -> 0.265658 V Bias 1 1652 -> 0.338666 V, Temperature 0.0730083 -> -22.8972 C
[17:04:56:737][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0013349] MON MUX_V: 7 Bias 0 1264 -> 0.26063 V Bias 1 1634 -> 0.335046 V, Temperature 0.0744161 -> -21.017 C
[17:04:56:741][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0013349] MON MUX_V: 15 Bias 0 1285 -> 0.264854 V Bias 1 1649 -> 0.338063 V, Temperature 0.0732094 -> -23.3751 C
[17:04:56:778][  info  ][  scanConsole  ]: Scan done!
[17:04:56:778][  info  ][  scanConsole  ]: Waiting for processors to finish ...
[17:04:56:779][  info  ][  scanConsole  ]: Processor done, waiting for histogrammer ...
[17:04:57:178][  info  ][HistogramAlgorithm]: Histogrammer done!
[17:04:57:178][  info  ][HistogramAlgorithm]: Histogrammer done!
[17:04:57:178][  info  ][HistogramAlgorithm]: Histogrammer done!
[17:04:57:178][  info  ][HistogramAlgorithm]: Histogrammer done!
[17:04:57:179][  info  ][  scanConsole  ]: Processor done, waiting for analysis ...
[17:04:57:579][  info  ][AnalysisAlgorithm]: Analysis done!
[17:04:57:579][  info  ][AnalysisAlgorithm]: Analysis done!
[17:04:57:579][  info  ][AnalysisAlgorithm]: Analysis done!
[17:04:57:579][  info  ][AnalysisAlgorithm]: Analysis done!
[17:04:57:579][  info  ][  scanConsole  ]: All done!
[17:04:57:579][  info  ][  scanConsole  ]: ##########
[17:04:57:579][  info  ][  scanConsole  ]: # Timing #
[17:04:57:579][  info  ][  scanConsole  ]: ##########
[17:04:57:579][  info  ][  scanConsole  ]: -> Configuration: 140 ms
[17:04:57:579][  info  ][  scanConsole  ]: -> Scan:          209 ms
[17:04:57:579][  info  ][  scanConsole  ]: -> Processing:    0 ms
[17:04:57:579][  info  ][  scanConsole  ]: -> Analysis:      800 ms
[17:04:57:579][  info  ][  scanConsole  ]: ###########
[17:04:57:579][  info  ][  scanConsole  ]: # Cleanup #
[17:04:57:579][  info  ][  scanConsole  ]: ###########
[17:04:57:582][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0013396 to configs/KEKQ17/20UPGFC0013396.json
[17:04:57:784][  info  ][  scanConsole  ]: -> Storing output results of FE 4
[17:04:57:784][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0013396, this usually means that the chip did not send any data at all.
[17:04:57:785][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0013428 to configs/KEKQ17/20UPGFC0013428.json
[17:04:57:962][  info  ][  scanConsole  ]: -> Storing output results of FE 5
[17:04:57:962][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0013428, this usually means that the chip did not send any data at all.
[17:04:57:964][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0013381 to configs/KEKQ17/20UPGFC0013381.json
[17:04:58:138][  info  ][  scanConsole  ]: -> Storing output results of FE 6
[17:04:58:138][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0013381, this usually means that the chip did not send any data at all.
[17:04:58:139][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0013349 to configs/KEKQ17/20UPGFC0013349.json
[17:04:58:314][  info  ][  scanConsole  ]: -> Storing output results of FE 7
[17:04:58:314][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0013349, this usually means that the chip did not send any data at all.
[17:04:58:315][  info  ][  scanConsole  ]: Finishing run: 1867
20UPGFC0013349.json.after
20UPGFC0013349.json.before
20UPGFC0013381.json.after
20UPGFC0013381.json.before
20UPGFC0013396.json.after
20UPGFC0013396.json.before
20UPGFC0013428.json.after
20UPGFC0013428.json.before
reg_readtemp.json
scanLog.json

