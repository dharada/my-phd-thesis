[15:14:10:261][  info  ][               ]: #####################################
[15:14:10:261][  info  ][               ]: # Welcome to the YARR Scan Console! #
[15:14:10:261][  info  ][               ]: #####################################
[15:14:10:261][  info  ][               ]: -> Parsing command line parameters ...
[15:14:10:261][  info  ][               ]: Configuring logger ...
[15:14:10:264][  info  ][  scanConsole  ]: Scan Type/Config configs/scans/rd53a/reg_readtemp.json
[15:14:10:264][  info  ][  scanConsole  ]: Connectivity:
[15:14:10:264][  info  ][  scanConsole  ]:     configs/KEKQ18/connectivity.json
[15:14:10:264][  info  ][  scanConsole  ]: Target ToT: -1
[15:14:10:264][  info  ][  scanConsole  ]: Target Charge: -1
[15:14:10:264][  info  ][  scanConsole  ]: Output Plots: true
[15:14:10:264][  info  ][  scanConsole  ]: Output Directory: ./data/001847_reg_readtemp/
[15:14:10:268][  info  ][  scanConsole  ]: Timestamp: 2022-08-26_15:14:10
[15:14:10:268][  info  ][  scanConsole  ]: Run Number: 1847
[15:14:10:268][  info  ][  scanConsole  ]: #################
[15:14:10:268][  info  ][  scanConsole  ]: # Init Hardware #
[15:14:10:268][  info  ][  scanConsole  ]: #################
[15:14:10:268][  info  ][  scanConsole  ]: -> Opening controller config: configs/controller/specCfg-rd53a-16x1.json
[15:14:10:268][  info  ][  ScanHelper   ]: Loading controller ...
[15:14:10:268][  info  ][  ScanHelper   ]: Found controller of type: spec
[15:14:10:268][  info  ][  ScanHelper   ]: ... loading controler config:
[15:14:10:268][  info  ][  ScanHelper   ]: ~~~ {
[15:14:10:268][  info  ][  ScanHelper   ]: ~~~     "cmdPeriod": 6.250000073038109e-9,
[15:14:10:268][  info  ][  ScanHelper   ]: ~~~     "idle": {
[15:14:10:268][  info  ][  ScanHelper   ]: ~~~         "word": 1768515945
[15:14:10:268][  info  ][  ScanHelper   ]: ~~~     },
[15:14:10:268][  info  ][  ScanHelper   ]: ~~~     "pulse": {
[15:14:10:268][  info  ][  ScanHelper   ]: ~~~         "interval": 500,
[15:14:10:268][  info  ][  ScanHelper   ]: ~~~         "word": 1549575846
[15:14:10:268][  info  ][  ScanHelper   ]: ~~~     },
[15:14:10:268][  info  ][  ScanHelper   ]: ~~~     "rxActiveLanes": 1,
[15:14:10:268][  info  ][  ScanHelper   ]: ~~~     "rxPolarity": 0,
[15:14:10:268][  info  ][  ScanHelper   ]: ~~~     "specNum": 0,
[15:14:10:268][  info  ][  ScanHelper   ]: ~~~     "spiConfig": 541200,
[15:14:10:268][  info  ][  ScanHelper   ]: ~~~     "sync": {
[15:14:10:268][  info  ][  ScanHelper   ]: ~~~         "interval": 32,
[15:14:10:268][  info  ][  ScanHelper   ]: ~~~         "word": 2172551550
[15:14:10:268][  info  ][  ScanHelper   ]: ~~~     },
[15:14:10:268][  info  ][  ScanHelper   ]: ~~~     "txPolarity": 0
[15:14:10:268][  info  ][  ScanHelper   ]: ~~~ }
[15:14:10:268][  info  ][    SpecCom    ]: Opening SPEC with id #0
[15:14:10:269][  info  ][    SpecCom    ]: Mapping BARs ...
[15:14:10:269][  info  ][    SpecCom    ]: ... Mapped BAR0 at 0x7fe4e68b4000 with size 1048576
[15:14:10:269][warning ][    SpecCom    ]: ... BAR4 not mapped (Mmap failed)
[15:14:10:269][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[15:14:10:269][  info  ][    SpecCom    ]: Firmware Version: 0x4d9ff6d
[15:14:10:269][  info  ][    SpecCom    ]: Firmware Identifier: 0x4030232
[15:14:10:269][  info  ][    SpecCom    ]: FPGA card: PLDA XpressK7 325
[15:14:10:269][  info  ][    SpecCom    ]: FE Chip Type: RD53A/B
[15:14:10:269][  info  ][    SpecCom    ]: FMC Card Type: Ohio Card (Display Port)
[15:14:10:269][  info  ][    SpecCom    ]: RX Speed: 640Mbps
[15:14:10:269][  info  ][    SpecCom    ]: Channel Configuration: 16x1
[15:14:10:269][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[15:14:10:269][  info  ][    SpecCom    ]: Flushing buffers ...
[15:14:10:269][  info  ][    SpecCom    ]: Init success!
[15:14:10:269][  info  ][  scanConsole  ]: #######################
[15:14:10:269][  info  ][  scanConsole  ]: ##  Loading Configs  ##
[15:14:10:269][  info  ][  scanConsole  ]: #######################
[15:14:10:269][  info  ][  scanConsole  ]: Opening global config: configs/KEKQ18/connectivity.json
[15:14:10:269][  info  ][  ScanHelper   ]: Chip type: RD53A
[15:14:10:269][  info  ][  ScanHelper   ]: Chip count 4
[15:14:10:269][  info  ][  ScanHelper   ]: Loading chip #0
[15:14:10:270][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(4)
[15:14:10:270][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ18/20UPGFC0009364.json
[15:14:10:456][  info  ][  ScanHelper   ]: Loading chip #1
[15:14:10:456][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(5)
[15:14:10:456][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ18/20UPGFC0009253.json
[15:14:10:630][  info  ][  ScanHelper   ]: Loading chip #2
[15:14:10:630][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(6)
[15:14:10:630][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ18/20UPGFC0009285.json
[15:14:10:803][  info  ][  ScanHelper   ]: Loading chip #3
[15:14:10:804][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(7)
[15:14:10:804][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ18/20UPGFC0009269.json
[15:14:10:981][  info  ][  scanConsole  ]: #################
[15:14:10:981][  info  ][  scanConsole  ]: # Configure FEs #
[15:14:10:981][  info  ][  scanConsole  ]: #################
[15:14:10:981][  info  ][  scanConsole  ]: Configuring 20UPGFC0009364
[15:14:11:014][  info  ][  scanConsole  ]: Configuring 20UPGFC0009253
[15:14:11:044][  info  ][  scanConsole  ]: Configuring 20UPGFC0009285
[15:14:11:078][  info  ][  scanConsole  ]: Configuring 20UPGFC0009269
[15:14:11:125][  info  ][  scanConsole  ]: Sent configuration to all FEs in 143 ms!
[15:14:11:126][  info  ][  scanConsole  ]: Checking com 20UPGFC0009364
[15:14:11:126][  info  ][    SpecRx     ]: Active Rx channels: 0x10
[15:14:11:126][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[15:14:11:126][  info  ][    SpecRx     ]: Rx Status 0xf0
[15:14:11:126][  info  ][    SpecRx     ]: Number of lanes: 1
[15:14:11:126][  info  ][    SpecRx     ]: Channel 4 Lane 0 synchronized!
[15:14:11:137][  info  ][  scanConsole  ]: ... success!
[15:14:11:137][  info  ][  scanConsole  ]: Checking com 20UPGFC0009253
[15:14:11:137][  info  ][    SpecRx     ]: Active Rx channels: 0x20
[15:14:11:137][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[15:14:11:137][  info  ][    SpecRx     ]: Rx Status 0xf0
[15:14:11:137][  info  ][    SpecRx     ]: Number of lanes: 1
[15:14:11:137][  info  ][    SpecRx     ]: Channel 5 Lane 0 synchronized!
[15:14:11:148][  info  ][  scanConsole  ]: ... success!
[15:14:11:148][  info  ][  scanConsole  ]: Checking com 20UPGFC0009285
[15:14:11:148][  info  ][    SpecRx     ]: Active Rx channels: 0x40
[15:14:11:148][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[15:14:11:148][  info  ][    SpecRx     ]: Rx Status 0xf0
[15:14:11:148][  info  ][    SpecRx     ]: Number of lanes: 1
[15:14:11:148][  info  ][    SpecRx     ]: Channel 6 Lane 0 synchronized!
[15:14:11:158][  info  ][  scanConsole  ]: ... success!
[15:14:11:158][  info  ][  scanConsole  ]: Checking com 20UPGFC0009269
[15:14:11:158][  info  ][    SpecRx     ]: Active Rx channels: 0x80
[15:14:11:158][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[15:14:11:158][  info  ][    SpecRx     ]: Rx Status 0xf0
[15:14:11:158][  info  ][    SpecRx     ]: Number of lanes: 1
[15:14:11:158][  info  ][    SpecRx     ]: Channel 7 Lane 0 synchronized!
[15:14:11:169][  info  ][  scanConsole  ]: ... success!
[15:14:11:169][  info  ][  scanConsole  ]: Enabling Tx channels
[15:14:11:169][  info  ][  scanConsole  ]: Enabling Tx channel 1
[15:14:11:169][  info  ][  scanConsole  ]: Enabling Tx channel 1
[15:14:11:169][  info  ][  scanConsole  ]: Enabling Tx channel 1
[15:14:11:169][  info  ][  scanConsole  ]: Enabling Tx channel 1
[15:14:11:169][  info  ][  scanConsole  ]: Enabling Rx channels
[15:14:11:169][  info  ][  scanConsole  ]: Enabling Rx channel 4
[15:14:11:169][  info  ][  scanConsole  ]: Enabling Rx channel 5
[15:14:11:169][  info  ][  scanConsole  ]: Enabling Rx channel 6
[15:14:11:169][  info  ][  scanConsole  ]: Enabling Rx channel 7
[15:14:11:169][  info  ][  scanConsole  ]: ##############
[15:14:11:169][  info  ][  scanConsole  ]: # Setup Scan #
[15:14:11:169][  info  ][  scanConsole  ]: ##############
[15:14:11:170][  info  ][  scanConsole  ]: Found Scan config, constructing scan ...
[15:14:11:170][  info  ][  ScanFactory  ]: Loading Scan:
[15:14:11:170][  info  ][  ScanFactory  ]:   Name: AnalogScan
[15:14:11:170][  info  ][  ScanFactory  ]:   Number of Loops: 3
[15:14:11:170][  info  ][  ScanFactory  ]:   Loading Loop #0
[15:14:11:170][  info  ][  ScanFactory  ]:    Type: Rd53aReadRegLoop
[15:14:11:170][  info  ][  ScanFactory  ]:    Loading loop config ... 
[15:14:11:170][  info  ][  ScanFactory  ]: ~~~ {
[15:14:11:170][  info  ][  ScanFactory  ]: ~~~     "CurMux": [],
[15:14:11:170][  info  ][  ScanFactory  ]: ~~~     "EnblRingOsc": 0,
[15:14:11:170][  info  ][  ScanFactory  ]: ~~~     "Registers": [],
[15:14:11:170][  info  ][  ScanFactory  ]: ~~~     "RingOscDur": 9,
[15:14:11:170][  info  ][  ScanFactory  ]: ~~~     "RingOscRep": 10,
[15:14:11:170][  info  ][  ScanFactory  ]: ~~~     "VoltMux": [3, 5, 7, 15]
[15:14:11:170][  info  ][  ScanFactory  ]: ~~~ }
[15:14:11:170][  info  ][  ScanFactory  ]:   Loading Loop #1
[15:14:11:170][  info  ][  ScanFactory  ]:    Type: Rd53aTriggerLoop
[15:14:11:170][  info  ][  ScanFactory  ]:    Loading loop config ... 
[15:14:11:171][  info  ][  ScanFactory  ]: ~~~ {
[15:14:11:171][  info  ][  ScanFactory  ]: ~~~     "count": 1,
[15:14:11:171][  info  ][  ScanFactory  ]: ~~~     "delay": 48,
[15:14:11:171][  info  ][  ScanFactory  ]: ~~~     "extTrig": false,
[15:14:11:171][  info  ][  ScanFactory  ]: ~~~     "frequency": 30000.0,
[15:14:11:171][  info  ][  ScanFactory  ]: ~~~     "noInject": false,
[15:14:11:171][  info  ][  ScanFactory  ]: ~~~     "sendEcr": false,
[15:14:11:171][  info  ][  ScanFactory  ]: ~~~     "time": 0.0,
[15:14:11:171][  info  ][  ScanFactory  ]: ~~~     "trigMultiplier": 16
[15:14:11:171][  info  ][  ScanFactory  ]: ~~~ }
[15:14:11:171][  info  ][  ScanFactory  ]:   Loading Loop #2
[15:14:11:171][  info  ][  ScanFactory  ]:    Type: StdDataLoop
[15:14:11:171][warning ][  ScanFactory  ]: ~~~ Config empty.
[15:14:11:171][  info  ][ScanBuildHistogrammers]: Loading histogrammer ...
[15:14:11:171][  info  ][ScanBuildHistogrammers]: ... done!
[15:14:11:171][  info  ][ScanBuildAnalyses]: Loading analyses ...
[15:14:11:172][  info  ][  scanConsole  ]: Running pre scan!
[15:14:11:172][  info  ][  ScanFactory  ]: Entering pre scan phase ...
[15:14:11:172][  info  ][     Rd53a     ]: Write named register: GlobalPulseRt -> 16384
[15:14:11:172][  info  ][     Rd53a     ]: Write named register: InjEnDig -> 0
[15:14:11:172][  info  ][     Rd53a     ]: Write named register: InjVcalDiff -> 0
[15:14:11:172][  info  ][     Rd53a     ]: Write named register: InjVcalHigh -> 3500
[15:14:11:172][  info  ][     Rd53a     ]: Write named register: InjVcalMed -> 500
[15:14:11:172][  info  ][     Rd53a     ]: Write named register: LatencyConfig -> 48
[15:14:11:172][  info  ][  scanConsole  ]: Starting histogrammer and analysis threads:
[15:14:11:172][  info  ][  scanConsole  ]:  .. started threads of Fe 4
[15:14:11:172][  info  ][  scanConsole  ]:  .. started threads of Fe 5
[15:14:11:173][  info  ][  scanConsole  ]:  .. started threads of Fe 6
[15:14:11:173][  info  ][  scanConsole  ]:  .. started threads of Fe 7
[15:14:11:174][  info  ][Rd53aDataProcessor]:   -> Processor thread #0 started!
[15:14:11:174][  info  ][Rd53aDataProcessor]:   -> Processor thread #1 started!
[15:14:11:174][  info  ][Rd53aDataProcessor]:   -> Processor thread #2 started!
[15:14:11:174][  info  ][Rd53aDataProcessor]:   -> Processor thread #3 started!
[15:14:11:174][  info  ][  scanConsole  ]: ########
[15:14:11:174][  info  ][  scanConsole  ]: # Scan #
[15:14:11:174][  info  ][  scanConsole  ]: ########
[15:14:11:174][  info  ][  scanConsole  ]: Starting scan!
[15:14:11:174][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0009364 on Rx 4
[15:14:11:178][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0009364] MON MUX_V: 3 Bias 0 1239 -> 0.272107 V Bias 1 1630 -> 0.349777 V, Temperature 0.0776703 -> -0.311386 C
[15:14:11:182][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0009364] MON MUX_V: 5 Bias 0 1207 -> 0.26575 V Bias 1 1600 -> 0.343818 V, Temperature 0.0780676 -> -9.2442 C
[15:14:11:186][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0009364] MON MUX_V: 7 Bias 0 1194 -> 0.263168 V Bias 1 1601 -> 0.344016 V, Temperature 0.0808486 -> -11.5289 C
[15:14:11:190][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0009364] MON MUX_V: 15 Bias 0 1189 -> 0.262174 V Bias 1 1595 -> 0.342825 V, Temperature 0.08065 -> -9.24466 C
[15:14:11:225][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0009253 on Rx 5
[15:14:11:229][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0009253] MON MUX_V: 3 Bias 0 1277 -> 0.277374 V Bias 1 1660 -> 0.35309 V, Temperature 0.0757164 -> 1.41466 C
[15:14:11:233][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0009253] MON MUX_V: 5 Bias 0 1228 -> 0.267687 V Bias 1 1624 -> 0.345973 V, Temperature 0.0782864 -> -14.117 C
[15:14:11:237][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0009253] MON MUX_V: 7 Bias 0 1211 -> 0.264326 V Bias 1 1613 -> 0.343799 V, Temperature 0.0794726 -> -11.0477 C
[15:14:11:241][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0009253] MON MUX_V: 15 Bias 0 1197 -> 0.261558 V Bias 1 1604 -> 0.342019 V, Temperature 0.0804611 -> -10.8684 C
[15:14:11:276][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0009285 on Rx 6
[15:14:11:280][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0009285] MON MUX_V: 3 Bias 0 1274 -> 0.275541 V Bias 1 1671 -> 0.35433 V, Temperature 0.0787891 -> -2.0177 C
[15:14:11:285][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0009285] MON MUX_V: 5 Bias 0 1233 -> 0.267404 V Bias 1 1633 -> 0.346789 V, Temperature 0.0793844 -> 0.773987 C
[15:14:11:289][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0009285] MON MUX_V: 7 Bias 0 1223 -> 0.26542 V Bias 1 1624 -> 0.345003 V, Temperature 0.0795829 -> -0.379517 C
[15:14:11:293][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0009285] MON MUX_V: 15 Bias 0 1216 -> 0.26403 V Bias 1 1618 -> 0.343812 V, Temperature 0.0797814 -> 0.912689 C
[15:14:11:328][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0009269 on Rx 7
[15:14:11:333][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0009269] MON MUX_V: 3 Bias 0 1277 -> 0.276376 V Bias 1 1668 -> 0.353996 V, Temperature 0.07762 -> 3.71451 C
[15:14:11:337][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0009269] MON MUX_V: 5 Bias 0 1240 -> 0.269031 V Bias 1 1640 -> 0.348438 V, Temperature 0.0794066 -> -6.44775 C
[15:14:11:341][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0009269] MON MUX_V: 7 Bias 0 1210 -> 0.263076 V Bias 1 1620 -> 0.344467 V, Temperature 0.0813918 -> -7.98145 C
[15:14:11:345][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0009269] MON MUX_V: 15 Bias 0 1233 -> 0.267642 V Bias 1 1634 -> 0.347247 V, Temperature 0.0796052 -> -6.12424 C
[15:14:11:382][  info  ][  scanConsole  ]: Scan done!
[15:14:11:382][  info  ][  scanConsole  ]: Waiting for processors to finish ...
[15:14:11:383][  info  ][  scanConsole  ]: Processor done, waiting for histogrammer ...
[15:14:11:782][  info  ][HistogramAlgorithm]: Histogrammer done!
[15:14:11:782][  info  ][HistogramAlgorithm]: Histogrammer done!
[15:14:11:782][  info  ][HistogramAlgorithm]: Histogrammer done!
[15:14:11:782][  info  ][HistogramAlgorithm]: Histogrammer done!
[15:14:11:783][  info  ][  scanConsole  ]: Processor done, waiting for analysis ...
[15:14:12:183][  info  ][AnalysisAlgorithm]: Analysis done!
[15:14:12:183][  info  ][AnalysisAlgorithm]: Analysis done!
[15:14:12:183][  info  ][AnalysisAlgorithm]: Analysis done!
[15:14:12:183][  info  ][AnalysisAlgorithm]: Analysis done!
[15:14:12:183][  info  ][  scanConsole  ]: All done!
[15:14:12:183][  info  ][  scanConsole  ]: ##########
[15:14:12:183][  info  ][  scanConsole  ]: # Timing #
[15:14:12:183][  info  ][  scanConsole  ]: ##########
[15:14:12:183][  info  ][  scanConsole  ]: -> Configuration: 143 ms
[15:14:12:183][  info  ][  scanConsole  ]: -> Scan:          207 ms
[15:14:12:183][  info  ][  scanConsole  ]: -> Processing:    0 ms
[15:14:12:184][  info  ][  scanConsole  ]: -> Analysis:      800 ms
[15:14:12:184][  info  ][  scanConsole  ]: ###########
[15:14:12:184][  info  ][  scanConsole  ]: # Cleanup #
[15:14:12:184][  info  ][  scanConsole  ]: ###########
[15:14:12:192][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0009364 to configs/KEKQ18/20UPGFC0009364.json
[15:14:12:363][  info  ][  scanConsole  ]: -> Storing output results of FE 4
[15:14:12:363][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0009364, this usually means that the chip did not send any data at all.
[15:14:12:365][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0009253 to configs/KEKQ18/20UPGFC0009253.json
[15:14:12:531][  info  ][  scanConsole  ]: -> Storing output results of FE 5
[15:14:12:531][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0009253, this usually means that the chip did not send any data at all.
[15:14:12:532][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0009285 to configs/KEKQ18/20UPGFC0009285.json
[15:14:12:698][  info  ][  scanConsole  ]: -> Storing output results of FE 6
[15:14:12:698][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0009285, this usually means that the chip did not send any data at all.
[15:14:12:699][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0009269 to configs/KEKQ18/20UPGFC0009269.json
[15:14:12:865][  info  ][  scanConsole  ]: -> Storing output results of FE 7
[15:14:12:865][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0009269, this usually means that the chip did not send any data at all.
[15:14:12:866][  info  ][  scanConsole  ]: Finishing run: 1847
20UPGFC0009253.json.after
20UPGFC0009253.json.before
20UPGFC0009269.json.after
20UPGFC0009269.json.before
20UPGFC0009285.json.after
20UPGFC0009285.json.before
20UPGFC0009364.json.after
20UPGFC0009364.json.before
reg_readtemp.json
scanLog.json

