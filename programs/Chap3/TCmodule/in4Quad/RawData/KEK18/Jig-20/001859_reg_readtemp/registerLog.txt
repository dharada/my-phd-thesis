[16:22:54:785][  info  ][               ]: #####################################
[16:22:54:785][  info  ][               ]: # Welcome to the YARR Scan Console! #
[16:22:54:785][  info  ][               ]: #####################################
[16:22:54:785][  info  ][               ]: -> Parsing command line parameters ...
[16:22:54:785][  info  ][               ]: Configuring logger ...
[16:22:54:794][  info  ][  scanConsole  ]: Scan Type/Config configs/scans/rd53a/reg_readtemp.json
[16:22:54:794][  info  ][  scanConsole  ]: Connectivity:
[16:22:54:794][  info  ][  scanConsole  ]:     configs/KEKQ18/connectivity.json
[16:22:54:794][  info  ][  scanConsole  ]: Target ToT: -1
[16:22:54:794][  info  ][  scanConsole  ]: Target Charge: -1
[16:22:54:794][  info  ][  scanConsole  ]: Output Plots: true
[16:22:54:794][  info  ][  scanConsole  ]: Output Directory: ./data/001859_reg_readtemp/
[16:22:54:808][  info  ][  scanConsole  ]: Timestamp: 2022-08-26_16:22:54
[16:22:54:808][  info  ][  scanConsole  ]: Run Number: 1859
[16:22:54:808][  info  ][  scanConsole  ]: #################
[16:22:54:808][  info  ][  scanConsole  ]: # Init Hardware #
[16:22:54:808][  info  ][  scanConsole  ]: #################
[16:22:54:808][  info  ][  scanConsole  ]: -> Opening controller config: configs/controller/specCfg-rd53a-16x1.json
[16:22:54:808][  info  ][  ScanHelper   ]: Loading controller ...
[16:22:54:808][  info  ][  ScanHelper   ]: Found controller of type: spec
[16:22:54:808][  info  ][  ScanHelper   ]: ... loading controler config:
[16:22:54:808][  info  ][  ScanHelper   ]: ~~~ {
[16:22:54:808][  info  ][  ScanHelper   ]: ~~~     "cmdPeriod": 6.250000073038109e-9,
[16:22:54:808][  info  ][  ScanHelper   ]: ~~~     "idle": {
[16:22:54:808][  info  ][  ScanHelper   ]: ~~~         "word": 1768515945
[16:22:54:808][  info  ][  ScanHelper   ]: ~~~     },
[16:22:54:808][  info  ][  ScanHelper   ]: ~~~     "pulse": {
[16:22:54:808][  info  ][  ScanHelper   ]: ~~~         "interval": 500,
[16:22:54:808][  info  ][  ScanHelper   ]: ~~~         "word": 1549575846
[16:22:54:808][  info  ][  ScanHelper   ]: ~~~     },
[16:22:54:808][  info  ][  ScanHelper   ]: ~~~     "rxActiveLanes": 1,
[16:22:54:808][  info  ][  ScanHelper   ]: ~~~     "rxPolarity": 0,
[16:22:54:808][  info  ][  ScanHelper   ]: ~~~     "specNum": 0,
[16:22:54:808][  info  ][  ScanHelper   ]: ~~~     "spiConfig": 541200,
[16:22:54:808][  info  ][  ScanHelper   ]: ~~~     "sync": {
[16:22:54:808][  info  ][  ScanHelper   ]: ~~~         "interval": 32,
[16:22:54:808][  info  ][  ScanHelper   ]: ~~~         "word": 2172551550
[16:22:54:808][  info  ][  ScanHelper   ]: ~~~     },
[16:22:54:808][  info  ][  ScanHelper   ]: ~~~     "txPolarity": 0
[16:22:54:808][  info  ][  ScanHelper   ]: ~~~ }
[16:22:54:808][  info  ][    SpecCom    ]: Opening SPEC with id #0
[16:22:54:809][  info  ][    SpecCom    ]: Mapping BARs ...
[16:22:54:809][  info  ][    SpecCom    ]: ... Mapped BAR0 at 0x7f2325472000 with size 1048576
[16:22:54:809][warning ][    SpecCom    ]: ... BAR4 not mapped (Mmap failed)
[16:22:54:809][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[16:22:54:809][  info  ][    SpecCom    ]: Firmware Version: 0x4d9ff6d
[16:22:54:809][  info  ][    SpecCom    ]: Firmware Identifier: 0x4030232
[16:22:54:809][  info  ][    SpecCom    ]: FPGA card: PLDA XpressK7 325
[16:22:54:809][  info  ][    SpecCom    ]: FE Chip Type: RD53A/B
[16:22:54:809][  info  ][    SpecCom    ]: FMC Card Type: Ohio Card (Display Port)
[16:22:54:809][  info  ][    SpecCom    ]: RX Speed: 640Mbps
[16:22:54:810][  info  ][    SpecCom    ]: Channel Configuration: 16x1
[16:22:54:810][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[16:22:54:810][  info  ][    SpecCom    ]: Flushing buffers ...
[16:22:54:810][  info  ][    SpecCom    ]: Init success!
[16:22:54:810][  info  ][  scanConsole  ]: #######################
[16:22:54:810][  info  ][  scanConsole  ]: ##  Loading Configs  ##
[16:22:54:810][  info  ][  scanConsole  ]: #######################
[16:22:54:810][  info  ][  scanConsole  ]: Opening global config: configs/KEKQ18/connectivity.json
[16:22:54:810][  info  ][  ScanHelper   ]: Chip type: RD53A
[16:22:54:810][  info  ][  ScanHelper   ]: Chip count 4
[16:22:54:810][  info  ][  ScanHelper   ]: Loading chip #0
[16:22:54:812][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(4)
[16:22:54:812][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ18/20UPGFC0009364.json
[16:22:55:009][  info  ][  ScanHelper   ]: Loading chip #1
[16:22:55:009][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(5)
[16:22:55:009][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ18/20UPGFC0009253.json
[16:22:55:181][  info  ][  ScanHelper   ]: Loading chip #2
[16:22:55:182][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(6)
[16:22:55:182][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ18/20UPGFC0009285.json
[16:22:55:354][  info  ][  ScanHelper   ]: Loading chip #3
[16:22:55:354][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(7)
[16:22:55:354][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ18/20UPGFC0009269.json
[16:22:55:528][  info  ][  scanConsole  ]: #################
[16:22:55:528][  info  ][  scanConsole  ]: # Configure FEs #
[16:22:55:528][  info  ][  scanConsole  ]: #################
[16:22:55:528][  info  ][  scanConsole  ]: Configuring 20UPGFC0009364
[16:22:55:559][  info  ][  scanConsole  ]: Configuring 20UPGFC0009253
[16:22:55:605][  info  ][  scanConsole  ]: Configuring 20UPGFC0009285
[16:22:55:650][  info  ][  scanConsole  ]: Configuring 20UPGFC0009269
[16:22:55:694][  info  ][  scanConsole  ]: Sent configuration to all FEs in 166 ms!
[16:22:55:695][  info  ][  scanConsole  ]: Checking com 20UPGFC0009364
[16:22:55:695][  info  ][    SpecRx     ]: Active Rx channels: 0x10
[16:22:55:695][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[16:22:55:695][  info  ][    SpecRx     ]: Rx Status 0xf0
[16:22:55:695][  info  ][    SpecRx     ]: Number of lanes: 1
[16:22:55:695][  info  ][    SpecRx     ]: Channel 4 Lane 0 synchronized!
[16:22:55:706][  info  ][  scanConsole  ]: ... success!
[16:22:55:706][  info  ][  scanConsole  ]: Checking com 20UPGFC0009253
[16:22:55:706][  info  ][    SpecRx     ]: Active Rx channels: 0x20
[16:22:55:706][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[16:22:55:706][  info  ][    SpecRx     ]: Rx Status 0xf0
[16:22:55:706][  info  ][    SpecRx     ]: Number of lanes: 1
[16:22:55:706][  info  ][    SpecRx     ]: Channel 5 Lane 0 synchronized!
[16:22:55:717][  info  ][  scanConsole  ]: ... success!
[16:22:55:717][  info  ][  scanConsole  ]: Checking com 20UPGFC0009285
[16:22:55:717][  info  ][    SpecRx     ]: Active Rx channels: 0x40
[16:22:55:717][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[16:22:55:717][  info  ][    SpecRx     ]: Rx Status 0xf0
[16:22:55:717][  info  ][    SpecRx     ]: Number of lanes: 1
[16:22:55:717][  info  ][    SpecRx     ]: Channel 6 Lane 0 synchronized!
[16:22:55:727][  info  ][  scanConsole  ]: ... success!
[16:22:55:727][  info  ][  scanConsole  ]: Checking com 20UPGFC0009269
[16:22:55:727][  info  ][    SpecRx     ]: Active Rx channels: 0x80
[16:22:55:727][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[16:22:55:727][  info  ][    SpecRx     ]: Rx Status 0xf0
[16:22:55:727][  info  ][    SpecRx     ]: Number of lanes: 1
[16:22:55:727][  info  ][    SpecRx     ]: Channel 7 Lane 0 synchronized!
[16:22:55:738][  info  ][  scanConsole  ]: ... success!
[16:22:55:738][  info  ][  scanConsole  ]: Enabling Tx channels
[16:22:55:738][  info  ][  scanConsole  ]: Enabling Tx channel 1
[16:22:55:738][  info  ][  scanConsole  ]: Enabling Tx channel 1
[16:22:55:738][  info  ][  scanConsole  ]: Enabling Tx channel 1
[16:22:55:738][  info  ][  scanConsole  ]: Enabling Tx channel 1
[16:22:55:738][  info  ][  scanConsole  ]: Enabling Rx channels
[16:22:55:738][  info  ][  scanConsole  ]: Enabling Rx channel 4
[16:22:55:738][  info  ][  scanConsole  ]: Enabling Rx channel 5
[16:22:55:738][  info  ][  scanConsole  ]: Enabling Rx channel 6
[16:22:55:738][  info  ][  scanConsole  ]: Enabling Rx channel 7
[16:22:55:738][  info  ][  scanConsole  ]: ##############
[16:22:55:738][  info  ][  scanConsole  ]: # Setup Scan #
[16:22:55:738][  info  ][  scanConsole  ]: ##############
[16:22:55:739][  info  ][  scanConsole  ]: Found Scan config, constructing scan ...
[16:22:55:739][  info  ][  ScanFactory  ]: Loading Scan:
[16:22:55:739][  info  ][  ScanFactory  ]:   Name: AnalogScan
[16:22:55:739][  info  ][  ScanFactory  ]:   Number of Loops: 3
[16:22:55:739][  info  ][  ScanFactory  ]:   Loading Loop #0
[16:22:55:739][  info  ][  ScanFactory  ]:    Type: Rd53aReadRegLoop
[16:22:55:739][  info  ][  ScanFactory  ]:    Loading loop config ... 
[16:22:55:739][  info  ][  ScanFactory  ]: ~~~ {
[16:22:55:739][  info  ][  ScanFactory  ]: ~~~     "CurMux": [],
[16:22:55:739][  info  ][  ScanFactory  ]: ~~~     "EnblRingOsc": 0,
[16:22:55:739][  info  ][  ScanFactory  ]: ~~~     "Registers": [],
[16:22:55:739][  info  ][  ScanFactory  ]: ~~~     "RingOscDur": 9,
[16:22:55:739][  info  ][  ScanFactory  ]: ~~~     "RingOscRep": 10,
[16:22:55:739][  info  ][  ScanFactory  ]: ~~~     "VoltMux": [3, 5, 7, 15]
[16:22:55:739][  info  ][  ScanFactory  ]: ~~~ }
[16:22:55:739][  info  ][  ScanFactory  ]:   Loading Loop #1
[16:22:55:739][  info  ][  ScanFactory  ]:    Type: Rd53aTriggerLoop
[16:22:55:739][  info  ][  ScanFactory  ]:    Loading loop config ... 
[16:22:55:740][  info  ][  ScanFactory  ]: ~~~ {
[16:22:55:740][  info  ][  ScanFactory  ]: ~~~     "count": 1,
[16:22:55:740][  info  ][  ScanFactory  ]: ~~~     "delay": 48,
[16:22:55:740][  info  ][  ScanFactory  ]: ~~~     "extTrig": false,
[16:22:55:740][  info  ][  ScanFactory  ]: ~~~     "frequency": 30000.0,
[16:22:55:740][  info  ][  ScanFactory  ]: ~~~     "noInject": false,
[16:22:55:740][  info  ][  ScanFactory  ]: ~~~     "sendEcr": false,
[16:22:55:740][  info  ][  ScanFactory  ]: ~~~     "time": 0.0,
[16:22:55:740][  info  ][  ScanFactory  ]: ~~~     "trigMultiplier": 16
[16:22:55:740][  info  ][  ScanFactory  ]: ~~~ }
[16:22:55:740][  info  ][  ScanFactory  ]:   Loading Loop #2
[16:22:55:740][  info  ][  ScanFactory  ]:    Type: StdDataLoop
[16:22:55:740][warning ][  ScanFactory  ]: ~~~ Config empty.
[16:22:55:740][  info  ][ScanBuildHistogrammers]: Loading histogrammer ...
[16:22:55:740][  info  ][ScanBuildHistogrammers]: ... done!
[16:22:55:740][  info  ][ScanBuildAnalyses]: Loading analyses ...
[16:22:55:741][  info  ][  scanConsole  ]: Running pre scan!
[16:22:55:741][  info  ][  ScanFactory  ]: Entering pre scan phase ...
[16:22:55:741][  info  ][     Rd53a     ]: Write named register: GlobalPulseRt -> 16384
[16:22:55:741][  info  ][     Rd53a     ]: Write named register: InjEnDig -> 0
[16:22:55:741][  info  ][     Rd53a     ]: Write named register: InjVcalDiff -> 0
[16:22:55:741][  info  ][     Rd53a     ]: Write named register: InjVcalHigh -> 3500
[16:22:55:741][  info  ][     Rd53a     ]: Write named register: InjVcalMed -> 500
[16:22:55:741][  info  ][     Rd53a     ]: Write named register: LatencyConfig -> 48
[16:22:55:741][  info  ][  scanConsole  ]: Starting histogrammer and analysis threads:
[16:22:55:741][  info  ][  scanConsole  ]:  .. started threads of Fe 4
[16:22:55:741][  info  ][  scanConsole  ]:  .. started threads of Fe 5
[16:22:55:741][  info  ][  scanConsole  ]:  .. started threads of Fe 6
[16:22:55:741][  info  ][  scanConsole  ]:  .. started threads of Fe 7
[16:22:55:742][  info  ][Rd53aDataProcessor]:   -> Processor thread #0 started!
[16:22:55:742][  info  ][Rd53aDataProcessor]:   -> Processor thread #1 started!
[16:22:55:742][  info  ][Rd53aDataProcessor]:   -> Processor thread #2 started!
[16:22:55:742][  info  ][Rd53aDataProcessor]:   -> Processor thread #3 started!
[16:22:55:742][  info  ][  scanConsole  ]: ########
[16:22:55:742][  info  ][  scanConsole  ]: # Scan #
[16:22:55:742][  info  ][  scanConsole  ]: ########
[16:22:55:742][  info  ][  scanConsole  ]: Starting scan!
[16:22:55:742][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0009364 on Rx 4
[16:22:55:746][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0009364] MON MUX_V: 3 Bias 0 1302 -> 0.284621 V Bias 1 1679 -> 0.359511 V, Temperature 0.0748893 -> -9.0927 C
[16:22:55:750][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0009364] MON MUX_V: 5 Bias 0 1272 -> 0.278662 V Bias 1 1649 -> 0.353551 V, Temperature 0.0748893 -> -22.232 C
[16:22:55:754][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0009364] MON MUX_V: 7 Bias 0 1262 -> 0.276676 V Bias 1 1652 -> 0.354147 V, Temperature 0.0774717 -> -26.1907 C
[16:22:55:758][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0009364] MON MUX_V: 15 Bias 0 1257 -> 0.275682 V Bias 1 1645 -> 0.352757 V, Temperature 0.0770744 -> -23.8559 C
[16:22:55:793][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0009253 on Rx 5
[16:22:55:797][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0009253] MON MUX_V: 3 Bias 0 1336 -> 0.289038 V Bias 1 1704 -> 0.361789 V, Temperature 0.072751 -> -7.20993 C
[16:22:55:801][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0009253] MON MUX_V: 5 Bias 0 1291 -> 0.280141 V Bias 1 1671 -> 0.355265 V, Temperature 0.0751233 -> -28.8364 C
[16:22:55:805][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0009253] MON MUX_V: 7 Bias 0 1275 -> 0.276978 V Bias 1 1660 -> 0.35309 V, Temperature 0.0761118 -> -24.0805 C
[16:22:55:809][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0009253] MON MUX_V: 15 Bias 0 1259 -> 0.273815 V Bias 1 1650 -> 0.351113 V, Temperature 0.077298 -> -23.8559 C
[16:22:55:844][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0009285 on Rx 6
[16:22:55:848][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0009285] MON MUX_V: 3 Bias 0 1337 -> 0.288044 V Bias 1 1719 -> 0.363856 V, Temperature 0.0758121 -> -12.4453 C
[16:22:55:852][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0009285] MON MUX_V: 5 Bias 0 1297 -> 0.280106 V Bias 1 1681 -> 0.356315 V, Temperature 0.076209 -> -10.069 C
[16:22:55:856][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0009285] MON MUX_V: 7 Bias 0 1287 -> 0.278121 V Bias 1 1672 -> 0.354529 V, Temperature 0.0764075 -> -11.1852 C
[16:22:55:860][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0009285] MON MUX_V: 15 Bias 0 1280 -> 0.276732 V Bias 1 1666 -> 0.353338 V, Temperature 0.0766059 -> -9.76654 C
[16:22:55:895][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0009269 on Rx 7
[16:22:55:900][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0009269] MON MUX_V: 3 Bias 0 1339 -> 0.288684 V Bias 1 1713 -> 0.36293 V, Temperature 0.0742452 -> -6.06012 C
[16:22:55:904][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0009269] MON MUX_V: 5 Bias 0 1302 -> 0.281339 V Bias 1 1686 -> 0.35757 V, Temperature 0.0762304 -> -18.7137 C
[16:22:55:908][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0009269] MON MUX_V: 7 Bias 0 1274 -> 0.275781 V Bias 1 1667 -> 0.353798 V, Temperature 0.0780171 -> -21.0141 C
[16:22:55:912][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0009269] MON MUX_V: 15 Bias 0 1296 -> 0.280148 V Bias 1 1682 -> 0.356776 V, Temperature 0.0766275 -> -17.0183 C
[16:22:55:948][  info  ][  scanConsole  ]: Scan done!
[16:22:55:948][  info  ][  scanConsole  ]: Waiting for processors to finish ...
[16:22:55:949][  info  ][  scanConsole  ]: Processor done, waiting for histogrammer ...
[16:22:56:348][  info  ][HistogramAlgorithm]: Histogrammer done!
[16:22:56:348][  info  ][HistogramAlgorithm]: Histogrammer done!
[16:22:56:348][  info  ][HistogramAlgorithm]: Histogrammer done!
[16:22:56:348][  info  ][HistogramAlgorithm]: Histogrammer done!
[16:22:56:348][  info  ][  scanConsole  ]: Processor done, waiting for analysis ...
[16:22:56:749][  info  ][AnalysisAlgorithm]: Analysis done!
[16:22:56:749][  info  ][AnalysisAlgorithm]: Analysis done!
[16:22:56:749][  info  ][AnalysisAlgorithm]: Analysis done!
[16:22:56:749][  info  ][AnalysisAlgorithm]: Analysis done!
[16:22:56:749][  info  ][  scanConsole  ]: All done!
[16:22:56:749][  info  ][  scanConsole  ]: ##########
[16:22:56:749][  info  ][  scanConsole  ]: # Timing #
[16:22:56:749][  info  ][  scanConsole  ]: ##########
[16:22:56:749][  info  ][  scanConsole  ]: -> Configuration: 166 ms
[16:22:56:749][  info  ][  scanConsole  ]: -> Scan:          205 ms
[16:22:56:749][  info  ][  scanConsole  ]: -> Processing:    0 ms
[16:22:56:749][  info  ][  scanConsole  ]: -> Analysis:      800 ms
[16:22:56:749][  info  ][  scanConsole  ]: ###########
[16:22:56:749][  info  ][  scanConsole  ]: # Cleanup #
[16:22:56:749][  info  ][  scanConsole  ]: ###########
[16:22:56:756][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0009364 to configs/KEKQ18/20UPGFC0009364.json
[16:22:56:925][  info  ][  scanConsole  ]: -> Storing output results of FE 4
[16:22:56:925][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0009364, this usually means that the chip did not send any data at all.
[16:22:56:927][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0009253 to configs/KEKQ18/20UPGFC0009253.json
[16:22:57:092][  info  ][  scanConsole  ]: -> Storing output results of FE 5
[16:22:57:092][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0009253, this usually means that the chip did not send any data at all.
[16:22:57:094][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0009285 to configs/KEKQ18/20UPGFC0009285.json
[16:22:57:261][  info  ][  scanConsole  ]: -> Storing output results of FE 6
[16:22:57:261][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0009285, this usually means that the chip did not send any data at all.
[16:22:57:262][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0009269 to configs/KEKQ18/20UPGFC0009269.json
[16:22:57:430][  info  ][  scanConsole  ]: -> Storing output results of FE 7
[16:22:57:430][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0009269, this usually means that the chip did not send any data at all.
[16:22:57:431][  info  ][  scanConsole  ]: Finishing run: 1859
20UPGFC0009253.json.after
20UPGFC0009253.json.before
20UPGFC0009269.json.after
20UPGFC0009269.json.before
20UPGFC0009285.json.after
20UPGFC0009285.json.before
20UPGFC0009364.json.after
20UPGFC0009364.json.before
reg_readtemp.json
scanLog.json

