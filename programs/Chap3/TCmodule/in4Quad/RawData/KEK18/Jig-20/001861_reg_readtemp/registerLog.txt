[16:28:40:864][  info  ][               ]: #####################################
[16:28:40:864][  info  ][               ]: # Welcome to the YARR Scan Console! #
[16:28:40:864][  info  ][               ]: #####################################
[16:28:40:864][  info  ][               ]: -> Parsing command line parameters ...
[16:28:40:864][  info  ][               ]: Configuring logger ...
[16:28:40:866][  info  ][  scanConsole  ]: Scan Type/Config configs/scans/rd53a/reg_readtemp.json
[16:28:40:866][  info  ][  scanConsole  ]: Connectivity:
[16:28:40:866][  info  ][  scanConsole  ]:     configs/KEKQ18/connectivity.json
[16:28:40:866][  info  ][  scanConsole  ]: Target ToT: -1
[16:28:40:866][  info  ][  scanConsole  ]: Target Charge: -1
[16:28:40:866][  info  ][  scanConsole  ]: Output Plots: true
[16:28:40:866][  info  ][  scanConsole  ]: Output Directory: ./data/001861_reg_readtemp/
[16:28:40:870][  info  ][  scanConsole  ]: Timestamp: 2022-08-26_16:28:40
[16:28:40:870][  info  ][  scanConsole  ]: Run Number: 1861
[16:28:40:870][  info  ][  scanConsole  ]: #################
[16:28:40:870][  info  ][  scanConsole  ]: # Init Hardware #
[16:28:40:870][  info  ][  scanConsole  ]: #################
[16:28:40:870][  info  ][  scanConsole  ]: -> Opening controller config: configs/controller/specCfg-rd53a-16x1.json
[16:28:40:871][  info  ][  ScanHelper   ]: Loading controller ...
[16:28:40:871][  info  ][  ScanHelper   ]: Found controller of type: spec
[16:28:40:871][  info  ][  ScanHelper   ]: ... loading controler config:
[16:28:40:871][  info  ][  ScanHelper   ]: ~~~ {
[16:28:40:871][  info  ][  ScanHelper   ]: ~~~     "cmdPeriod": 6.250000073038109e-9,
[16:28:40:871][  info  ][  ScanHelper   ]: ~~~     "idle": {
[16:28:40:871][  info  ][  ScanHelper   ]: ~~~         "word": 1768515945
[16:28:40:871][  info  ][  ScanHelper   ]: ~~~     },
[16:28:40:871][  info  ][  ScanHelper   ]: ~~~     "pulse": {
[16:28:40:871][  info  ][  ScanHelper   ]: ~~~         "interval": 500,
[16:28:40:871][  info  ][  ScanHelper   ]: ~~~         "word": 1549575846
[16:28:40:871][  info  ][  ScanHelper   ]: ~~~     },
[16:28:40:871][  info  ][  ScanHelper   ]: ~~~     "rxActiveLanes": 1,
[16:28:40:871][  info  ][  ScanHelper   ]: ~~~     "rxPolarity": 0,
[16:28:40:871][  info  ][  ScanHelper   ]: ~~~     "specNum": 0,
[16:28:40:871][  info  ][  ScanHelper   ]: ~~~     "spiConfig": 541200,
[16:28:40:871][  info  ][  ScanHelper   ]: ~~~     "sync": {
[16:28:40:871][  info  ][  ScanHelper   ]: ~~~         "interval": 32,
[16:28:40:871][  info  ][  ScanHelper   ]: ~~~         "word": 2172551550
[16:28:40:871][  info  ][  ScanHelper   ]: ~~~     },
[16:28:40:871][  info  ][  ScanHelper   ]: ~~~     "txPolarity": 0
[16:28:40:871][  info  ][  ScanHelper   ]: ~~~ }
[16:28:40:871][  info  ][    SpecCom    ]: Opening SPEC with id #0
[16:28:40:871][  info  ][    SpecCom    ]: Mapping BARs ...
[16:28:40:871][  info  ][    SpecCom    ]: ... Mapped BAR0 at 0x7f1ab1210000 with size 1048576
[16:28:40:871][warning ][    SpecCom    ]: ... BAR4 not mapped (Mmap failed)
[16:28:40:871][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[16:28:40:871][  info  ][    SpecCom    ]: Firmware Version: 0x4d9ff6d
[16:28:40:871][  info  ][    SpecCom    ]: Firmware Identifier: 0x4030232
[16:28:40:871][  info  ][    SpecCom    ]: FPGA card: PLDA XpressK7 325
[16:28:40:871][  info  ][    SpecCom    ]: FE Chip Type: RD53A/B
[16:28:40:871][  info  ][    SpecCom    ]: FMC Card Type: Ohio Card (Display Port)
[16:28:40:871][  info  ][    SpecCom    ]: RX Speed: 640Mbps
[16:28:40:871][  info  ][    SpecCom    ]: Channel Configuration: 16x1
[16:28:40:871][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[16:28:40:871][  info  ][    SpecCom    ]: Flushing buffers ...
[16:28:40:871][  info  ][    SpecCom    ]: Init success!
[16:28:40:871][  info  ][  scanConsole  ]: #######################
[16:28:40:871][  info  ][  scanConsole  ]: ##  Loading Configs  ##
[16:28:40:871][  info  ][  scanConsole  ]: #######################
[16:28:40:871][  info  ][  scanConsole  ]: Opening global config: configs/KEKQ18/connectivity.json
[16:28:40:871][  info  ][  ScanHelper   ]: Chip type: RD53A
[16:28:40:871][  info  ][  ScanHelper   ]: Chip count 4
[16:28:40:871][  info  ][  ScanHelper   ]: Loading chip #0
[16:28:40:872][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(4)
[16:28:40:872][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ18/20UPGFC0009364.json
[16:28:41:052][  info  ][  ScanHelper   ]: Loading chip #1
[16:28:41:053][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(5)
[16:28:41:053][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ18/20UPGFC0009253.json
[16:28:41:226][  info  ][  ScanHelper   ]: Loading chip #2
[16:28:41:227][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(6)
[16:28:41:227][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ18/20UPGFC0009285.json
[16:28:41:399][  info  ][  ScanHelper   ]: Loading chip #3
[16:28:41:400][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(7)
[16:28:41:400][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ18/20UPGFC0009269.json
[16:28:41:578][  info  ][  scanConsole  ]: #################
[16:28:41:578][  info  ][  scanConsole  ]: # Configure FEs #
[16:28:41:578][  info  ][  scanConsole  ]: #################
[16:28:41:578][  info  ][  scanConsole  ]: Configuring 20UPGFC0009364
[16:28:41:609][  info  ][  scanConsole  ]: Configuring 20UPGFC0009253
[16:28:41:650][  info  ][  scanConsole  ]: Configuring 20UPGFC0009285
[16:28:41:680][  info  ][  scanConsole  ]: Configuring 20UPGFC0009269
[16:28:41:710][  info  ][  scanConsole  ]: Sent configuration to all FEs in 131 ms!
[16:28:41:711][  info  ][  scanConsole  ]: Checking com 20UPGFC0009364
[16:28:41:712][  info  ][    SpecRx     ]: Active Rx channels: 0x10
[16:28:41:712][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[16:28:41:712][  info  ][    SpecRx     ]: Rx Status 0xf0
[16:28:41:712][  info  ][    SpecRx     ]: Number of lanes: 1
[16:28:41:712][  info  ][    SpecRx     ]: Channel 4 Lane 0 synchronized!
[16:28:41:722][  info  ][  scanConsole  ]: ... success!
[16:28:41:722][  info  ][  scanConsole  ]: Checking com 20UPGFC0009253
[16:28:41:722][  info  ][    SpecRx     ]: Active Rx channels: 0x20
[16:28:41:722][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[16:28:41:722][  info  ][    SpecRx     ]: Rx Status 0xf0
[16:28:41:722][  info  ][    SpecRx     ]: Number of lanes: 1
[16:28:41:722][  info  ][    SpecRx     ]: Channel 5 Lane 0 synchronized!
[16:28:41:732][  info  ][  scanConsole  ]: ... success!
[16:28:41:732][  info  ][  scanConsole  ]: Checking com 20UPGFC0009285
[16:28:41:732][  info  ][    SpecRx     ]: Active Rx channels: 0x40
[16:28:41:732][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[16:28:41:732][  info  ][    SpecRx     ]: Rx Status 0xf0
[16:28:41:732][  info  ][    SpecRx     ]: Number of lanes: 1
[16:28:41:732][  info  ][    SpecRx     ]: Channel 6 Lane 0 synchronized!
[16:28:41:743][  info  ][  scanConsole  ]: ... success!
[16:28:41:743][  info  ][  scanConsole  ]: Checking com 20UPGFC0009269
[16:28:41:743][  info  ][    SpecRx     ]: Active Rx channels: 0x80
[16:28:41:743][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[16:28:41:743][  info  ][    SpecRx     ]: Rx Status 0xf0
[16:28:41:743][  info  ][    SpecRx     ]: Number of lanes: 1
[16:28:41:743][  info  ][    SpecRx     ]: Channel 7 Lane 0 synchronized!
[16:28:41:753][  info  ][  scanConsole  ]: ... success!
[16:28:41:753][  info  ][  scanConsole  ]: Enabling Tx channels
[16:28:41:753][  info  ][  scanConsole  ]: Enabling Tx channel 1
[16:28:41:753][  info  ][  scanConsole  ]: Enabling Tx channel 1
[16:28:41:753][  info  ][  scanConsole  ]: Enabling Tx channel 1
[16:28:41:753][  info  ][  scanConsole  ]: Enabling Tx channel 1
[16:28:41:753][  info  ][  scanConsole  ]: Enabling Rx channels
[16:28:41:753][  info  ][  scanConsole  ]: Enabling Rx channel 4
[16:28:41:753][  info  ][  scanConsole  ]: Enabling Rx channel 5
[16:28:41:753][  info  ][  scanConsole  ]: Enabling Rx channel 6
[16:28:41:753][  info  ][  scanConsole  ]: Enabling Rx channel 7
[16:28:41:753][  info  ][  scanConsole  ]: ##############
[16:28:41:753][  info  ][  scanConsole  ]: # Setup Scan #
[16:28:41:753][  info  ][  scanConsole  ]: ##############
[16:28:41:753][  info  ][  scanConsole  ]: Found Scan config, constructing scan ...
[16:28:41:753][  info  ][  ScanFactory  ]: Loading Scan:
[16:28:41:753][  info  ][  ScanFactory  ]:   Name: AnalogScan
[16:28:41:753][  info  ][  ScanFactory  ]:   Number of Loops: 3
[16:28:41:753][  info  ][  ScanFactory  ]:   Loading Loop #0
[16:28:41:753][  info  ][  ScanFactory  ]:    Type: Rd53aReadRegLoop
[16:28:41:753][  info  ][  ScanFactory  ]:    Loading loop config ... 
[16:28:41:754][  info  ][  ScanFactory  ]: ~~~ {
[16:28:41:754][  info  ][  ScanFactory  ]: ~~~     "CurMux": [],
[16:28:41:754][  info  ][  ScanFactory  ]: ~~~     "EnblRingOsc": 0,
[16:28:41:754][  info  ][  ScanFactory  ]: ~~~     "Registers": [],
[16:28:41:754][  info  ][  ScanFactory  ]: ~~~     "RingOscDur": 9,
[16:28:41:754][  info  ][  ScanFactory  ]: ~~~     "RingOscRep": 10,
[16:28:41:754][  info  ][  ScanFactory  ]: ~~~     "VoltMux": [3, 5, 7, 15]
[16:28:41:754][  info  ][  ScanFactory  ]: ~~~ }
[16:28:41:754][  info  ][  ScanFactory  ]:   Loading Loop #1
[16:28:41:754][  info  ][  ScanFactory  ]:    Type: Rd53aTriggerLoop
[16:28:41:754][  info  ][  ScanFactory  ]:    Loading loop config ... 
[16:28:41:754][  info  ][  ScanFactory  ]: ~~~ {
[16:28:41:754][  info  ][  ScanFactory  ]: ~~~     "count": 1,
[16:28:41:754][  info  ][  ScanFactory  ]: ~~~     "delay": 48,
[16:28:41:754][  info  ][  ScanFactory  ]: ~~~     "extTrig": false,
[16:28:41:754][  info  ][  ScanFactory  ]: ~~~     "frequency": 30000.0,
[16:28:41:754][  info  ][  ScanFactory  ]: ~~~     "noInject": false,
[16:28:41:754][  info  ][  ScanFactory  ]: ~~~     "sendEcr": false,
[16:28:41:754][  info  ][  ScanFactory  ]: ~~~     "time": 0.0,
[16:28:41:754][  info  ][  ScanFactory  ]: ~~~     "trigMultiplier": 16
[16:28:41:754][  info  ][  ScanFactory  ]: ~~~ }
[16:28:41:754][  info  ][  ScanFactory  ]:   Loading Loop #2
[16:28:41:754][  info  ][  ScanFactory  ]:    Type: StdDataLoop
[16:28:41:754][warning ][  ScanFactory  ]: ~~~ Config empty.
[16:28:41:754][  info  ][ScanBuildHistogrammers]: Loading histogrammer ...
[16:28:41:754][  info  ][ScanBuildHistogrammers]: ... done!
[16:28:41:754][  info  ][ScanBuildAnalyses]: Loading analyses ...
[16:28:41:754][  info  ][  scanConsole  ]: Running pre scan!
[16:28:41:754][  info  ][  ScanFactory  ]: Entering pre scan phase ...
[16:28:41:754][  info  ][     Rd53a     ]: Write named register: GlobalPulseRt -> 16384
[16:28:41:754][  info  ][     Rd53a     ]: Write named register: InjEnDig -> 0
[16:28:41:754][  info  ][     Rd53a     ]: Write named register: InjVcalDiff -> 0
[16:28:41:754][  info  ][     Rd53a     ]: Write named register: InjVcalHigh -> 3500
[16:28:41:754][  info  ][     Rd53a     ]: Write named register: InjVcalMed -> 500
[16:28:41:754][  info  ][     Rd53a     ]: Write named register: LatencyConfig -> 48
[16:28:41:754][  info  ][  scanConsole  ]: Starting histogrammer and analysis threads:
[16:28:41:754][  info  ][  scanConsole  ]:  .. started threads of Fe 4
[16:28:41:754][  info  ][  scanConsole  ]:  .. started threads of Fe 5
[16:28:41:754][  info  ][  scanConsole  ]:  .. started threads of Fe 6
[16:28:41:754][  info  ][  scanConsole  ]:  .. started threads of Fe 7
[16:28:41:754][  info  ][Rd53aDataProcessor]:   -> Processor thread #0 started!
[16:28:41:754][  info  ][Rd53aDataProcessor]:   -> Processor thread #1 started!
[16:28:41:754][  info  ][Rd53aDataProcessor]:   -> Processor thread #2 started!
[16:28:41:754][  info  ][Rd53aDataProcessor]:   -> Processor thread #3 started!
[16:28:41:754][  info  ][  scanConsole  ]: ########
[16:28:41:754][  info  ][  scanConsole  ]: # Scan #
[16:28:41:754][  info  ][  scanConsole  ]: ########
[16:28:41:754][  info  ][  scanConsole  ]: Starting scan!
[16:28:41:754][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0009364 on Rx 4
[16:28:41:758][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0009364] MON MUX_V: 3 Bias 0 1298 -> 0.283827 V Bias 1 1675 -> 0.358716 V, Temperature 0.0748893 -> -9.0929 C
[16:28:41:762][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0009364] MON MUX_V: 5 Bias 0 1268 -> 0.277867 V Bias 1 1646 -> 0.352955 V, Temperature 0.0750879 -> -21.4202 C
[16:28:41:766][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0009364] MON MUX_V: 7 Bias 0 1258 -> 0.275881 V Bias 1 1649 -> 0.353551 V, Temperature 0.0776703 -> -25.3283 C
[16:28:41:770][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0009364] MON MUX_V: 15 Bias 0 1253 -> 0.274888 V Bias 1 1643 -> 0.35236 V, Temperature 0.0774717 -> -22.2324 C
[16:28:41:805][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0009253 on Rx 5
[16:28:41:809][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0009253] MON MUX_V: 3 Bias 0 1335 -> 0.28884 V Bias 1 1703 -> 0.361591 V, Temperature 0.072751 -> -7.20993 C
[16:28:41:813][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0009253] MON MUX_V: 5 Bias 0 1288 -> 0.279548 V Bias 1 1670 -> 0.355067 V, Temperature 0.0755187 -> -26.9966 C
[16:28:41:817][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0009253] MON MUX_V: 7 Bias 0 1272 -> 0.276385 V Bias 1 1657 -> 0.352497 V, Temperature 0.0761118 -> -24.0804 C
[16:28:41:821][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0009253] MON MUX_V: 15 Bias 0 1258 -> 0.273618 V Bias 1 1649 -> 0.350915 V, Temperature 0.077298 -> -23.856 C
[16:28:41:856][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0009285 on Rx 6
[16:28:41:860][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0009285] MON MUX_V: 3 Bias 0 1332 -> 0.287052 V Bias 1 1715 -> 0.363063 V, Temperature 0.0760106 -> -11.7501 C
[16:28:41:864][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0009285] MON MUX_V: 5 Bias 0 1293 -> 0.279312 V Bias 1 1679 -> 0.355918 V, Temperature 0.0766059 -> -8.71365 C
[16:28:41:868][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0009285] MON MUX_V: 7 Bias 0 1284 -> 0.277526 V Bias 1 1669 -> 0.353933 V, Temperature 0.0764075 -> -11.1853 C
[16:28:41:872][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0009285] MON MUX_V: 15 Bias 0 1278 -> 0.276335 V Bias 1 1664 -> 0.352941 V, Temperature 0.0766059 -> -9.76654 C
[16:28:41:907][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0009269 on Rx 7
[16:28:41:911][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0009269] MON MUX_V: 3 Bias 0 1335 -> 0.28789 V Bias 1 1711 -> 0.362532 V, Temperature 0.0746423 -> -4.91011 C
[16:28:41:916][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0009269] MON MUX_V: 5 Bias 0 1298 -> 0.280545 V Bias 1 1682 -> 0.356776 V, Temperature 0.0762304 -> -18.7137 C
[16:28:41:920][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0009269] MON MUX_V: 7 Bias 0 1270 -> 0.274987 V Bias 1 1664 -> 0.353202 V, Temperature 0.0782156 -> -20.2473 C
[16:28:41:924][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0009269] MON MUX_V: 15 Bias 0 1292 -> 0.279354 V Bias 1 1679 -> 0.35618 V, Temperature 0.0768259 -> -16.2921 C
[16:28:41:961][  info  ][  scanConsole  ]: Scan done!
[16:28:41:961][  info  ][  scanConsole  ]: Waiting for processors to finish ...
[16:28:41:962][  info  ][  scanConsole  ]: Processor done, waiting for histogrammer ...
[16:28:42:361][  info  ][HistogramAlgorithm]: Histogrammer done!
[16:28:42:361][  info  ][HistogramAlgorithm]: Histogrammer done!
[16:28:42:361][  info  ][HistogramAlgorithm]: Histogrammer done!
[16:28:42:361][  info  ][HistogramAlgorithm]: Histogrammer done!
[16:28:42:362][  info  ][  scanConsole  ]: Processor done, waiting for analysis ...
[16:28:42:762][  info  ][AnalysisAlgorithm]: Analysis done!
[16:28:42:762][  info  ][AnalysisAlgorithm]: Analysis done!
[16:28:42:762][  info  ][AnalysisAlgorithm]: Analysis done!
[16:28:42:762][  info  ][AnalysisAlgorithm]: Analysis done!
[16:28:42:762][  info  ][  scanConsole  ]: All done!
[16:28:42:762][  info  ][  scanConsole  ]: ##########
[16:28:42:762][  info  ][  scanConsole  ]: # Timing #
[16:28:42:762][  info  ][  scanConsole  ]: ##########
[16:28:42:762][  info  ][  scanConsole  ]: -> Configuration: 131 ms
[16:28:42:762][  info  ][  scanConsole  ]: -> Scan:          206 ms
[16:28:42:762][  info  ][  scanConsole  ]: -> Processing:    1 ms
[16:28:42:762][  info  ][  scanConsole  ]: -> Analysis:      800 ms
[16:28:42:762][  info  ][  scanConsole  ]: ###########
[16:28:42:763][  info  ][  scanConsole  ]: # Cleanup #
[16:28:42:763][  info  ][  scanConsole  ]: ###########
[16:28:42:771][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0009364 to configs/KEKQ18/20UPGFC0009364.json
[16:28:42:940][  info  ][  scanConsole  ]: -> Storing output results of FE 4
[16:28:42:940][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0009364, this usually means that the chip did not send any data at all.
[16:28:42:942][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0009253 to configs/KEKQ18/20UPGFC0009253.json
[16:28:43:105][  info  ][  scanConsole  ]: -> Storing output results of FE 5
[16:28:43:105][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0009253, this usually means that the chip did not send any data at all.
[16:28:43:106][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0009285 to configs/KEKQ18/20UPGFC0009285.json
[16:28:43:269][  info  ][  scanConsole  ]: -> Storing output results of FE 6
[16:28:43:269][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0009285, this usually means that the chip did not send any data at all.
[16:28:43:270][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0009269 to configs/KEKQ18/20UPGFC0009269.json
[16:28:43:433][  info  ][  scanConsole  ]: -> Storing output results of FE 7
[16:28:43:433][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0009269, this usually means that the chip did not send any data at all.
[16:28:43:434][  info  ][  scanConsole  ]: Finishing run: 1861
20UPGFC0009253.json.after
20UPGFC0009253.json.before
20UPGFC0009269.json.after
20UPGFC0009269.json.before
20UPGFC0009285.json.after
20UPGFC0009285.json.before
20UPGFC0009364.json.after
20UPGFC0009364.json.before
reg_readtemp.json
scanLog.json

