[17:19:46:461][  info  ][               ]: #####################################
[17:19:46:461][  info  ][               ]: # Welcome to the YARR Scan Console! #
[17:19:46:461][  info  ][               ]: #####################################
[17:19:46:461][  info  ][               ]: -> Parsing command line parameters ...
[17:19:46:461][  info  ][               ]: Configuring logger ...
[17:19:46:464][  info  ][  scanConsole  ]: Scan Type/Config configs/scans/rd53a/reg_readtemp.json
[17:19:46:464][  info  ][  scanConsole  ]: Connectivity:
[17:19:46:464][  info  ][  scanConsole  ]:     configs/CERNQ7/example_rd53a_setup.json
[17:19:46:464][  info  ][  scanConsole  ]: Target ToT: -1
[17:19:46:464][  info  ][  scanConsole  ]: Target Charge: -1
[17:19:46:464][  info  ][  scanConsole  ]: Output Plots: true
[17:19:46:464][  info  ][  scanConsole  ]: Output Directory: ./data/001737_reg_readtemp/
[17:19:46:469][  info  ][  scanConsole  ]: Timestamp: 2022-08-23_17:19:46
[17:19:46:469][  info  ][  scanConsole  ]: Run Number: 1737
[17:19:46:469][  info  ][  scanConsole  ]: #################
[17:19:46:469][  info  ][  scanConsole  ]: # Init Hardware #
[17:19:46:469][  info  ][  scanConsole  ]: #################
[17:19:46:469][  info  ][  scanConsole  ]: -> Opening controller config: configs/controller/specCfg-rd53a-16x1.json
[17:19:46:469][  info  ][  ScanHelper   ]: Loading controller ...
[17:19:46:469][  info  ][  ScanHelper   ]: Found controller of type: spec
[17:19:46:469][  info  ][  ScanHelper   ]: ... loading controler config:
[17:19:46:469][  info  ][  ScanHelper   ]: ~~~ {
[17:19:46:469][  info  ][  ScanHelper   ]: ~~~     "cmdPeriod": 6.250000073038109e-9,
[17:19:46:469][  info  ][  ScanHelper   ]: ~~~     "idle": {
[17:19:46:469][  info  ][  ScanHelper   ]: ~~~         "word": 1768515945
[17:19:46:469][  info  ][  ScanHelper   ]: ~~~     },
[17:19:46:469][  info  ][  ScanHelper   ]: ~~~     "pulse": {
[17:19:46:469][  info  ][  ScanHelper   ]: ~~~         "interval": 500,
[17:19:46:469][  info  ][  ScanHelper   ]: ~~~         "word": 1549575846
[17:19:46:469][  info  ][  ScanHelper   ]: ~~~     },
[17:19:46:469][  info  ][  ScanHelper   ]: ~~~     "rxActiveLanes": 1,
[17:19:46:469][  info  ][  ScanHelper   ]: ~~~     "rxPolarity": 0,
[17:19:46:469][  info  ][  ScanHelper   ]: ~~~     "specNum": 0,
[17:19:46:469][  info  ][  ScanHelper   ]: ~~~     "spiConfig": 541200,
[17:19:46:469][  info  ][  ScanHelper   ]: ~~~     "sync": {
[17:19:46:469][  info  ][  ScanHelper   ]: ~~~         "interval": 32,
[17:19:46:469][  info  ][  ScanHelper   ]: ~~~         "word": 2172551550
[17:19:46:469][  info  ][  ScanHelper   ]: ~~~     },
[17:19:46:469][  info  ][  ScanHelper   ]: ~~~     "txPolarity": 0
[17:19:46:469][  info  ][  ScanHelper   ]: ~~~ }
[17:19:46:469][  info  ][    SpecCom    ]: Opening SPEC with id #0
[17:19:46:469][  info  ][    SpecCom    ]: Mapping BARs ...
[17:19:46:469][  info  ][    SpecCom    ]: ... Mapped BAR0 at 0x7f26299c8000 with size 1048576
[17:19:46:469][warning ][    SpecCom    ]: ... BAR4 not mapped (Mmap failed)
[17:19:46:469][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[17:19:46:469][  info  ][    SpecCom    ]: Firmware Version: 0x4d9ff6d
[17:19:46:469][  info  ][    SpecCom    ]: Firmware Identifier: 0x4030232
[17:19:46:469][  info  ][    SpecCom    ]: FPGA card: PLDA XpressK7 325
[17:19:46:469][  info  ][    SpecCom    ]: FE Chip Type: RD53A/B
[17:19:46:469][  info  ][    SpecCom    ]: FMC Card Type: Ohio Card (Display Port)
[17:19:46:469][  info  ][    SpecCom    ]: RX Speed: 640Mbps
[17:19:46:469][  info  ][    SpecCom    ]: Channel Configuration: 16x1
[17:19:46:469][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[17:19:46:469][  info  ][    SpecCom    ]: Flushing buffers ...
[17:19:46:469][  info  ][    SpecCom    ]: Init success!
[17:19:46:470][  info  ][  scanConsole  ]: #######################
[17:19:46:470][  info  ][  scanConsole  ]: ##  Loading Configs  ##
[17:19:46:470][  info  ][  scanConsole  ]: #######################
[17:19:46:470][  info  ][  scanConsole  ]: Opening global config: configs/CERNQ7/example_rd53a_setup.json
[17:19:46:470][  info  ][  ScanHelper   ]: Chip type: RD53A
[17:19:46:470][  info  ][  ScanHelper   ]: Chip count 4
[17:19:46:470][  info  ][  ScanHelper   ]: Loading chip #0
[17:19:46:471][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(4)
[17:19:46:471][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ7/20UPGFC0012613.json
[17:19:46:656][  info  ][  ScanHelper   ]: Loading chip #1
[17:19:46:656][warning ][  ScanHelper   ]:  ... chip not enabled, skipping!
[17:19:46:656][  info  ][  ScanHelper   ]: Loading chip #2
[17:19:46:656][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(6)
[17:19:46:656][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ7/20UPGFC0012661.json
[17:19:46:829][  info  ][  ScanHelper   ]: Loading chip #3
[17:19:46:830][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(7)
[17:19:46:830][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ7/20UPGFC0012629.json
[17:19:47:004][  info  ][  scanConsole  ]: #################
[17:19:47:004][  info  ][  scanConsole  ]: # Configure FEs #
[17:19:47:004][  info  ][  scanConsole  ]: #################
[17:19:47:004][  info  ][  scanConsole  ]: Configuring 20UPGFC0012613
[17:19:47:036][  info  ][  scanConsole  ]: Configuring 20UPGFC0012661
[17:19:47:066][  info  ][  scanConsole  ]: Configuring 20UPGFC0012629
[17:19:47:106][  info  ][  scanConsole  ]: Sent configuration to all FEs in 102 ms!
[17:19:47:107][  info  ][  scanConsole  ]: Checking com 20UPGFC0012613
[17:19:47:107][  info  ][    SpecRx     ]: Active Rx channels: 0x10
[17:19:47:107][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[17:19:47:107][  info  ][    SpecRx     ]: Rx Status 0xd0
[17:19:47:107][  info  ][    SpecRx     ]: Number of lanes: 1
[17:19:47:107][  info  ][    SpecRx     ]: Channel 4 Lane 0 synchronized!
[17:19:47:118][  info  ][  scanConsole  ]: ... success!
[17:19:47:118][  info  ][  scanConsole  ]: Checking com 20UPGFC0012661
[17:19:47:118][  info  ][    SpecRx     ]: Active Rx channels: 0x40
[17:19:47:118][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[17:19:47:118][  info  ][    SpecRx     ]: Rx Status 0xd0
[17:19:47:118][  info  ][    SpecRx     ]: Number of lanes: 1
[17:19:47:118][  info  ][    SpecRx     ]: Channel 6 Lane 0 synchronized!
[17:19:47:128][  info  ][  scanConsole  ]: ... success!
[17:19:47:129][  info  ][  scanConsole  ]: Checking com 20UPGFC0012629
[17:19:47:129][  info  ][    SpecRx     ]: Active Rx channels: 0x80
[17:19:47:129][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[17:19:47:129][  info  ][    SpecRx     ]: Rx Status 0xd0
[17:19:47:129][  info  ][    SpecRx     ]: Number of lanes: 1
[17:19:47:129][  info  ][    SpecRx     ]: Channel 7 Lane 0 synchronized!
[17:19:47:139][  info  ][  scanConsole  ]: ... success!
[17:19:47:139][  info  ][  scanConsole  ]: Enabling Tx channels
[17:19:47:139][  info  ][  scanConsole  ]: Enabling Tx channel 1
[17:19:47:139][  info  ][  scanConsole  ]: Enabling Tx channel 1
[17:19:47:139][  info  ][  scanConsole  ]: Enabling Tx channel 1
[17:19:47:139][  info  ][  scanConsole  ]: Enabling Rx channels
[17:19:47:139][  info  ][  scanConsole  ]: Enabling Rx channel 4
[17:19:47:139][  info  ][  scanConsole  ]: Enabling Rx channel 6
[17:19:47:139][  info  ][  scanConsole  ]: Enabling Rx channel 7
[17:19:47:139][  info  ][  scanConsole  ]: ##############
[17:19:47:139][  info  ][  scanConsole  ]: # Setup Scan #
[17:19:47:139][  info  ][  scanConsole  ]: ##############
[17:19:47:140][  info  ][  scanConsole  ]: Found Scan config, constructing scan ...
[17:19:47:140][  info  ][  ScanFactory  ]: Loading Scan:
[17:19:47:140][  info  ][  ScanFactory  ]:   Name: AnalogScan
[17:19:47:140][  info  ][  ScanFactory  ]:   Number of Loops: 3
[17:19:47:140][  info  ][  ScanFactory  ]:   Loading Loop #0
[17:19:47:140][  info  ][  ScanFactory  ]:    Type: Rd53aReadRegLoop
[17:19:47:140][  info  ][  ScanFactory  ]:    Loading loop config ... 
[17:19:47:140][  info  ][  ScanFactory  ]: ~~~ {
[17:19:47:140][  info  ][  ScanFactory  ]: ~~~     "CurMux": [],
[17:19:47:141][  info  ][  ScanFactory  ]: ~~~     "EnblRingOsc": 0,
[17:19:47:141][  info  ][  ScanFactory  ]: ~~~     "Registers": [],
[17:19:47:141][  info  ][  ScanFactory  ]: ~~~     "RingOscDur": 9,
[17:19:47:141][  info  ][  ScanFactory  ]: ~~~     "RingOscRep": 10,
[17:19:47:141][  info  ][  ScanFactory  ]: ~~~     "VoltMux": [3, 5, 7, 15]
[17:19:47:141][  info  ][  ScanFactory  ]: ~~~ }
[17:19:47:141][  info  ][  ScanFactory  ]:   Loading Loop #1
[17:19:47:141][  info  ][  ScanFactory  ]:    Type: Rd53aTriggerLoop
[17:19:47:141][  info  ][  ScanFactory  ]:    Loading loop config ... 
[17:19:47:141][  info  ][  ScanFactory  ]: ~~~ {
[17:19:47:141][  info  ][  ScanFactory  ]: ~~~     "count": 1,
[17:19:47:141][  info  ][  ScanFactory  ]: ~~~     "delay": 48,
[17:19:47:141][  info  ][  ScanFactory  ]: ~~~     "extTrig": false,
[17:19:47:141][  info  ][  ScanFactory  ]: ~~~     "frequency": 30000.0,
[17:19:47:141][  info  ][  ScanFactory  ]: ~~~     "noInject": false,
[17:19:47:141][  info  ][  ScanFactory  ]: ~~~     "sendEcr": false,
[17:19:47:141][  info  ][  ScanFactory  ]: ~~~     "time": 0.0,
[17:19:47:141][  info  ][  ScanFactory  ]: ~~~     "trigMultiplier": 16
[17:19:47:141][  info  ][  ScanFactory  ]: ~~~ }
[17:19:47:141][  info  ][  ScanFactory  ]:   Loading Loop #2
[17:19:47:141][  info  ][  ScanFactory  ]:    Type: StdDataLoop
[17:19:47:141][warning ][  ScanFactory  ]: ~~~ Config empty.
[17:19:47:141][  info  ][ScanBuildHistogrammers]: Loading histogrammer ...
[17:19:47:141][  info  ][ScanBuildHistogrammers]: ... done!
[17:19:47:142][  info  ][ScanBuildAnalyses]: Loading analyses ...
[17:19:47:142][  info  ][  scanConsole  ]: Running pre scan!
[17:19:47:142][  info  ][  ScanFactory  ]: Entering pre scan phase ...
[17:19:47:142][  info  ][     Rd53a     ]: Write named register: GlobalPulseRt -> 16384
[17:19:47:142][  info  ][     Rd53a     ]: Write named register: InjEnDig -> 0
[17:19:47:142][  info  ][     Rd53a     ]: Write named register: InjVcalDiff -> 0
[17:19:47:142][  info  ][     Rd53a     ]: Write named register: InjVcalHigh -> 3500
[17:19:47:142][  info  ][     Rd53a     ]: Write named register: InjVcalMed -> 500
[17:19:47:142][  info  ][     Rd53a     ]: Write named register: LatencyConfig -> 48
[17:19:47:142][  info  ][  scanConsole  ]: Starting histogrammer and analysis threads:
[17:19:47:142][  info  ][  scanConsole  ]:  .. started threads of Fe 4
[17:19:47:142][  info  ][  scanConsole  ]:  .. started threads of Fe 6
[17:19:47:143][  info  ][  scanConsole  ]:  .. started threads of Fe 7
[17:19:47:143][  info  ][Rd53aDataProcessor]:   -> Processor thread #0 started!
[17:19:47:143][  info  ][Rd53aDataProcessor]:   -> Processor thread #1 started!
[17:19:47:143][  info  ][Rd53aDataProcessor]:   -> Processor thread #2 started!
[17:19:47:143][  info  ][Rd53aDataProcessor]:   -> Processor thread #3 started!
[17:19:47:143][  info  ][  scanConsole  ]: ########
[17:19:47:143][  info  ][  scanConsole  ]: # Scan #
[17:19:47:143][  info  ][  scanConsole  ]: ########
[17:19:47:143][  info  ][  scanConsole  ]: Starting scan!
[17:19:47:143][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0012613 on Rx 4
[17:19:47:148][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0012613] MON MUX_V: 3 Bias 0 1319 -> 0.283276 V Bias 1 1701 -> 0.358767 V, Temperature 0.0754913 -> -18.7957 C
[17:19:47:152][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0012613] MON MUX_V: 5 Bias 0 1268 -> 0.273197 V Bias 1 1646 -> 0.347898 V, Temperature 0.0747008 -> -20.3001 C
[17:19:47:156][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0012613] MON MUX_V: 7 Bias 0 1269 -> 0.273395 V Bias 1 1655 -> 0.349676 V, Temperature 0.0762818 -> -19.5791 C
[17:19:47:161][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0012613] MON MUX_V: 15 Bias 0 1269 -> 0.273395 V Bias 1 1659 -> 0.350467 V, Temperature 0.0770723 -> -21.0522 C
[17:19:47:195][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0012661 on Rx 6
[17:19:47:200][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0012661] MON MUX_V: 3 Bias 0 1340 -> 0.284414 V Bias 1 1729 -> 0.361287 V, Temperature 0.0768726 -> -15.4561 C
[17:19:47:204][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0012661] MON MUX_V: 5 Bias 0 1298 -> 0.276114 V Bias 1 1696 -> 0.354765 V, Temperature 0.0786512 -> -23.5952 C
[17:19:47:208][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0012661] MON MUX_V: 7 Bias 0 1279 -> 0.272359 V Bias 1 1679 -> 0.351406 V, Temperature 0.0790465 -> -25.2428 C
[17:19:47:213][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0012661] MON MUX_V: 15 Bias 0 1271 -> 0.270779 V Bias 1 1673 -> 0.35022 V, Temperature 0.0794417 -> -25.2429 C
[17:19:47:247][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0012629 on Rx 7
[17:19:47:252][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0012629] MON MUX_V: 3 Bias 0 1280 -> 0.27692 V Bias 1 1657 -> 0.351566 V, Temperature 0.074646 -> -14.764 C
[17:19:47:256][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0012629] MON MUX_V: 5 Bias 0 1250 -> 0.27098 V Bias 1 1630 -> 0.34622 V, Temperature 0.07524 -> -19.5479 C
[17:19:47:260][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0012629] MON MUX_V: 7 Bias 0 1238 -> 0.268604 V Bias 1 1619 -> 0.344042 V, Temperature 0.075438 -> -18.8582 C
[17:19:47:264][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0012629] MON MUX_V: 15 Bias 0 1216 -> 0.264248 V Bias 1 1596 -> 0.339488 V, Temperature 0.07524 -> -18.224 C
[17:19:47:301][  info  ][  scanConsole  ]: Scan done!
[17:19:47:301][  info  ][  scanConsole  ]: Waiting for processors to finish ...
[17:19:47:302][  info  ][  scanConsole  ]: Processor done, waiting for histogrammer ...
[17:19:47:701][  info  ][HistogramAlgorithm]: Histogrammer done!
[17:19:47:701][  info  ][HistogramAlgorithm]: Histogrammer done!
[17:19:47:701][  info  ][HistogramAlgorithm]: Histogrammer done!
[17:19:47:702][  info  ][  scanConsole  ]: Processor done, waiting for analysis ...
[17:19:48:102][  info  ][AnalysisAlgorithm]: Analysis done!
[17:19:48:102][  info  ][AnalysisAlgorithm]: Analysis done!
[17:19:48:102][  info  ][AnalysisAlgorithm]: Analysis done!
[17:19:48:102][  info  ][  scanConsole  ]: All done!
[17:19:48:102][  info  ][  scanConsole  ]: ##########
[17:19:48:102][  info  ][  scanConsole  ]: # Timing #
[17:19:48:102][  info  ][  scanConsole  ]: ##########
[17:19:48:102][  info  ][  scanConsole  ]: -> Configuration: 102 ms
[17:19:48:102][  info  ][  scanConsole  ]: -> Scan:          157 ms
[17:19:48:102][  info  ][  scanConsole  ]: -> Processing:    0 ms
[17:19:48:102][  info  ][  scanConsole  ]: -> Analysis:      800 ms
[17:19:48:102][  info  ][  scanConsole  ]: ###########
[17:19:48:102][  info  ][  scanConsole  ]: # Cleanup #
[17:19:48:102][  info  ][  scanConsole  ]: ###########
[17:19:48:111][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0012613 to configs/CERNQ7/20UPGFC0012613.json
[17:19:48:318][  info  ][  scanConsole  ]: -> Storing output results of FE 4
[17:19:48:318][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0012613, this usually means that the chip did not send any data at all.
[17:19:48:320][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0012661 to configs/CERNQ7/20UPGFC0012661.json
[17:19:48:486][  info  ][  scanConsole  ]: -> Storing output results of FE 6
[17:19:48:486][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0012661, this usually means that the chip did not send any data at all.
[17:19:48:488][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0012629 to configs/CERNQ7/20UPGFC0012629.json
[17:19:48:657][  info  ][  scanConsole  ]: -> Storing output results of FE 7
[17:19:48:657][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0012629, this usually means that the chip did not send any data at all.
[17:19:48:658][  info  ][  scanConsole  ]: Finishing run: 1737
20UPGFC0012613.json.after
20UPGFC0012613.json.before
20UPGFC0012629.json.after
20UPGFC0012629.json.before
20UPGFC0012661.json.after
20UPGFC0012661.json.before
reg_readtemp.json
scanLog.json

