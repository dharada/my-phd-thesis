[17:27:09:069][  info  ][               ]: #####################################
[17:27:09:069][  info  ][               ]: # Welcome to the YARR Scan Console! #
[17:27:09:069][  info  ][               ]: #####################################
[17:27:09:069][  info  ][               ]: -> Parsing command line parameters ...
[17:27:09:069][  info  ][               ]: Configuring logger ...
[17:27:09:071][  info  ][  scanConsole  ]: Scan Type/Config configs/scans/rd53a/reg_readtemp.json
[17:27:09:071][  info  ][  scanConsole  ]: Connectivity:
[17:27:09:071][  info  ][  scanConsole  ]:     configs/CERNQ7/example_rd53a_setup.json
[17:27:09:071][  info  ][  scanConsole  ]: Target ToT: -1
[17:27:09:071][  info  ][  scanConsole  ]: Target Charge: -1
[17:27:09:071][  info  ][  scanConsole  ]: Output Plots: true
[17:27:09:071][  info  ][  scanConsole  ]: Output Directory: ./data/001740_reg_readtemp/
[17:27:09:075][  info  ][  scanConsole  ]: Timestamp: 2022-08-23_17:27:09
[17:27:09:075][  info  ][  scanConsole  ]: Run Number: 1740
[17:27:09:075][  info  ][  scanConsole  ]: #################
[17:27:09:075][  info  ][  scanConsole  ]: # Init Hardware #
[17:27:09:075][  info  ][  scanConsole  ]: #################
[17:27:09:075][  info  ][  scanConsole  ]: -> Opening controller config: configs/controller/specCfg-rd53a-16x1.json
[17:27:09:075][  info  ][  ScanHelper   ]: Loading controller ...
[17:27:09:075][  info  ][  ScanHelper   ]: Found controller of type: spec
[17:27:09:075][  info  ][  ScanHelper   ]: ... loading controler config:
[17:27:09:075][  info  ][  ScanHelper   ]: ~~~ {
[17:27:09:075][  info  ][  ScanHelper   ]: ~~~     "cmdPeriod": 6.250000073038109e-9,
[17:27:09:075][  info  ][  ScanHelper   ]: ~~~     "idle": {
[17:27:09:075][  info  ][  ScanHelper   ]: ~~~         "word": 1768515945
[17:27:09:075][  info  ][  ScanHelper   ]: ~~~     },
[17:27:09:075][  info  ][  ScanHelper   ]: ~~~     "pulse": {
[17:27:09:075][  info  ][  ScanHelper   ]: ~~~         "interval": 500,
[17:27:09:075][  info  ][  ScanHelper   ]: ~~~         "word": 1549575846
[17:27:09:075][  info  ][  ScanHelper   ]: ~~~     },
[17:27:09:075][  info  ][  ScanHelper   ]: ~~~     "rxActiveLanes": 1,
[17:27:09:075][  info  ][  ScanHelper   ]: ~~~     "rxPolarity": 0,
[17:27:09:075][  info  ][  ScanHelper   ]: ~~~     "specNum": 0,
[17:27:09:075][  info  ][  ScanHelper   ]: ~~~     "spiConfig": 541200,
[17:27:09:075][  info  ][  ScanHelper   ]: ~~~     "sync": {
[17:27:09:075][  info  ][  ScanHelper   ]: ~~~         "interval": 32,
[17:27:09:075][  info  ][  ScanHelper   ]: ~~~         "word": 2172551550
[17:27:09:075][  info  ][  ScanHelper   ]: ~~~     },
[17:27:09:075][  info  ][  ScanHelper   ]: ~~~     "txPolarity": 0
[17:27:09:075][  info  ][  ScanHelper   ]: ~~~ }
[17:27:09:075][  info  ][    SpecCom    ]: Opening SPEC with id #0
[17:27:09:075][  info  ][    SpecCom    ]: Mapping BARs ...
[17:27:09:075][  info  ][    SpecCom    ]: ... Mapped BAR0 at 0x7f4bc8135000 with size 1048576
[17:27:09:076][warning ][    SpecCom    ]: ... BAR4 not mapped (Mmap failed)
[17:27:09:076][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[17:27:09:076][  info  ][    SpecCom    ]: Firmware Version: 0x4d9ff6d
[17:27:09:076][  info  ][    SpecCom    ]: Firmware Identifier: 0x4030232
[17:27:09:076][  info  ][    SpecCom    ]: FPGA card: PLDA XpressK7 325
[17:27:09:076][  info  ][    SpecCom    ]: FE Chip Type: RD53A/B
[17:27:09:076][  info  ][    SpecCom    ]: FMC Card Type: Ohio Card (Display Port)
[17:27:09:076][  info  ][    SpecCom    ]: RX Speed: 640Mbps
[17:27:09:076][  info  ][    SpecCom    ]: Channel Configuration: 16x1
[17:27:09:076][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[17:27:09:076][  info  ][    SpecCom    ]: Flushing buffers ...
[17:27:09:076][  info  ][    SpecCom    ]: Init success!
[17:27:09:076][  info  ][  scanConsole  ]: #######################
[17:27:09:076][  info  ][  scanConsole  ]: ##  Loading Configs  ##
[17:27:09:076][  info  ][  scanConsole  ]: #######################
[17:27:09:076][  info  ][  scanConsole  ]: Opening global config: configs/CERNQ7/example_rd53a_setup.json
[17:27:09:076][  info  ][  ScanHelper   ]: Chip type: RD53A
[17:27:09:076][  info  ][  ScanHelper   ]: Chip count 4
[17:27:09:076][  info  ][  ScanHelper   ]: Loading chip #0
[17:27:09:076][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(4)
[17:27:09:076][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ7/20UPGFC0012613.json
[17:27:09:269][  info  ][  ScanHelper   ]: Loading chip #1
[17:27:09:269][warning ][  ScanHelper   ]:  ... chip not enabled, skipping!
[17:27:09:269][  info  ][  ScanHelper   ]: Loading chip #2
[17:27:09:269][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(6)
[17:27:09:269][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ7/20UPGFC0012661.json
[17:27:09:459][  info  ][  ScanHelper   ]: Loading chip #3
[17:27:09:460][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(7)
[17:27:09:460][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ7/20UPGFC0012629.json
[17:27:09:634][  info  ][  scanConsole  ]: #################
[17:27:09:634][  info  ][  scanConsole  ]: # Configure FEs #
[17:27:09:634][  info  ][  scanConsole  ]: #################
[17:27:09:634][  info  ][  scanConsole  ]: Configuring 20UPGFC0012613
[17:27:09:665][  info  ][  scanConsole  ]: Configuring 20UPGFC0012661
[17:27:09:695][  info  ][  scanConsole  ]: Configuring 20UPGFC0012629
[17:27:09:725][  info  ][  scanConsole  ]: Sent configuration to all FEs in 90 ms!
[17:27:09:726][  info  ][  scanConsole  ]: Checking com 20UPGFC0012613
[17:27:09:727][  info  ][    SpecRx     ]: Active Rx channels: 0x10
[17:27:09:727][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[17:27:09:727][  info  ][    SpecRx     ]: Rx Status 0xd0
[17:27:09:727][  info  ][    SpecRx     ]: Number of lanes: 1
[17:27:09:727][  info  ][    SpecRx     ]: Channel 4 Lane 0 synchronized!
[17:27:09:737][  info  ][  scanConsole  ]: ... success!
[17:27:09:737][  info  ][  scanConsole  ]: Checking com 20UPGFC0012661
[17:27:09:737][  info  ][    SpecRx     ]: Active Rx channels: 0x40
[17:27:09:737][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[17:27:09:737][  info  ][    SpecRx     ]: Rx Status 0xd0
[17:27:09:737][  info  ][    SpecRx     ]: Number of lanes: 1
[17:27:09:737][  info  ][    SpecRx     ]: Channel 6 Lane 0 synchronized!
[17:27:09:747][  info  ][  scanConsole  ]: ... success!
[17:27:09:747][  info  ][  scanConsole  ]: Checking com 20UPGFC0012629
[17:27:09:747][  info  ][    SpecRx     ]: Active Rx channels: 0x80
[17:27:09:747][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[17:27:09:748][  info  ][    SpecRx     ]: Rx Status 0xd0
[17:27:09:748][  info  ][    SpecRx     ]: Number of lanes: 1
[17:27:09:748][  info  ][    SpecRx     ]: Channel 7 Lane 0 synchronized!
[17:27:09:758][  info  ][  scanConsole  ]: ... success!
[17:27:09:758][  info  ][  scanConsole  ]: Enabling Tx channels
[17:27:09:758][  info  ][  scanConsole  ]: Enabling Tx channel 1
[17:27:09:758][  info  ][  scanConsole  ]: Enabling Tx channel 1
[17:27:09:758][  info  ][  scanConsole  ]: Enabling Tx channel 1
[17:27:09:758][  info  ][  scanConsole  ]: Enabling Rx channels
[17:27:09:758][  info  ][  scanConsole  ]: Enabling Rx channel 4
[17:27:09:758][  info  ][  scanConsole  ]: Enabling Rx channel 6
[17:27:09:758][  info  ][  scanConsole  ]: Enabling Rx channel 7
[17:27:09:758][  info  ][  scanConsole  ]: ##############
[17:27:09:758][  info  ][  scanConsole  ]: # Setup Scan #
[17:27:09:758][  info  ][  scanConsole  ]: ##############
[17:27:09:759][  info  ][  scanConsole  ]: Found Scan config, constructing scan ...
[17:27:09:759][  info  ][  ScanFactory  ]: Loading Scan:
[17:27:09:759][  info  ][  ScanFactory  ]:   Name: AnalogScan
[17:27:09:759][  info  ][  ScanFactory  ]:   Number of Loops: 3
[17:27:09:759][  info  ][  ScanFactory  ]:   Loading Loop #0
[17:27:09:759][  info  ][  ScanFactory  ]:    Type: Rd53aReadRegLoop
[17:27:09:759][  info  ][  ScanFactory  ]:    Loading loop config ... 
[17:27:09:759][  info  ][  ScanFactory  ]: ~~~ {
[17:27:09:759][  info  ][  ScanFactory  ]: ~~~     "CurMux": [],
[17:27:09:759][  info  ][  ScanFactory  ]: ~~~     "EnblRingOsc": 0,
[17:27:09:759][  info  ][  ScanFactory  ]: ~~~     "Registers": [],
[17:27:09:759][  info  ][  ScanFactory  ]: ~~~     "RingOscDur": 9,
[17:27:09:759][  info  ][  ScanFactory  ]: ~~~     "RingOscRep": 10,
[17:27:09:759][  info  ][  ScanFactory  ]: ~~~     "VoltMux": [3, 5, 7, 15]
[17:27:09:759][  info  ][  ScanFactory  ]: ~~~ }
[17:27:09:759][  info  ][  ScanFactory  ]:   Loading Loop #1
[17:27:09:759][  info  ][  ScanFactory  ]:    Type: Rd53aTriggerLoop
[17:27:09:759][  info  ][  ScanFactory  ]:    Loading loop config ... 
[17:27:09:760][  info  ][  ScanFactory  ]: ~~~ {
[17:27:09:760][  info  ][  ScanFactory  ]: ~~~     "count": 1,
[17:27:09:760][  info  ][  ScanFactory  ]: ~~~     "delay": 48,
[17:27:09:760][  info  ][  ScanFactory  ]: ~~~     "extTrig": false,
[17:27:09:760][  info  ][  ScanFactory  ]: ~~~     "frequency": 30000.0,
[17:27:09:760][  info  ][  ScanFactory  ]: ~~~     "noInject": false,
[17:27:09:760][  info  ][  ScanFactory  ]: ~~~     "sendEcr": false,
[17:27:09:760][  info  ][  ScanFactory  ]: ~~~     "time": 0.0,
[17:27:09:760][  info  ][  ScanFactory  ]: ~~~     "trigMultiplier": 16
[17:27:09:760][  info  ][  ScanFactory  ]: ~~~ }
[17:27:09:760][  info  ][  ScanFactory  ]:   Loading Loop #2
[17:27:09:760][  info  ][  ScanFactory  ]:    Type: StdDataLoop
[17:27:09:760][warning ][  ScanFactory  ]: ~~~ Config empty.
[17:27:09:760][  info  ][ScanBuildHistogrammers]: Loading histogrammer ...
[17:27:09:760][  info  ][ScanBuildHistogrammers]: ... done!
[17:27:09:760][  info  ][ScanBuildAnalyses]: Loading analyses ...
[17:27:09:760][  info  ][  scanConsole  ]: Running pre scan!
[17:27:09:761][  info  ][  ScanFactory  ]: Entering pre scan phase ...
[17:27:09:761][  info  ][     Rd53a     ]: Write named register: GlobalPulseRt -> 16384
[17:27:09:761][  info  ][     Rd53a     ]: Write named register: InjEnDig -> 0
[17:27:09:761][  info  ][     Rd53a     ]: Write named register: InjVcalDiff -> 0
[17:27:09:761][  info  ][     Rd53a     ]: Write named register: InjVcalHigh -> 3500
[17:27:09:761][  info  ][     Rd53a     ]: Write named register: InjVcalMed -> 500
[17:27:09:761][  info  ][     Rd53a     ]: Write named register: LatencyConfig -> 48
[17:27:09:761][  info  ][  scanConsole  ]: Starting histogrammer and analysis threads:
[17:27:09:761][  info  ][  scanConsole  ]:  .. started threads of Fe 4
[17:27:09:761][  info  ][  scanConsole  ]:  .. started threads of Fe 6
[17:27:09:761][  info  ][  scanConsole  ]:  .. started threads of Fe 7
[17:27:09:761][  info  ][Rd53aDataProcessor]:   -> Processor thread #0 started!
[17:27:09:762][  info  ][Rd53aDataProcessor]:   -> Processor thread #1 started!
[17:27:09:762][  info  ][Rd53aDataProcessor]:   -> Processor thread #2 started!
[17:27:09:762][  info  ][Rd53aDataProcessor]:   -> Processor thread #3 started!
[17:27:09:762][  info  ][  scanConsole  ]: ########
[17:27:09:762][  info  ][  scanConsole  ]: # Scan #
[17:27:09:762][  info  ][  scanConsole  ]: ########
[17:27:09:762][  info  ][  scanConsole  ]: Starting scan!
[17:27:09:762][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0012613 on Rx 4
[17:27:09:766][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0012613] MON MUX_V: 3 Bias 0 1317 -> 0.28288 V Bias 1 1701 -> 0.358767 V, Temperature 0.0758866 -> -17.2913 C
[17:27:09:770][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0012613] MON MUX_V: 5 Bias 0 1268 -> 0.273197 V Bias 1 1645 -> 0.3477 V, Temperature 0.0745032 -> -21.0208 C
[17:27:09:774][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0012613] MON MUX_V: 7 Bias 0 1271 -> 0.27379 V Bias 1 1655 -> 0.349676 V, Temperature 0.0758865 -> -21.0209 C
[17:27:09:778][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0012613] MON MUX_V: 15 Bias 0 1271 -> 0.27379 V Bias 1 1659 -> 0.350467 V, Temperature 0.076677 -> -22.5566 C
[17:27:09:813][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0012661 on Rx 6
[17:27:09:817][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0012661] MON MUX_V: 3 Bias 0 1340 -> 0.284414 V Bias 1 1728 -> 0.361089 V, Temperature 0.0766751 -> -16.148 C
[17:27:09:821][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0012661] MON MUX_V: 5 Bias 0 1298 -> 0.276114 V Bias 1 1695 -> 0.354568 V, Temperature 0.0784536 -> -24.4192 C
[17:27:09:825][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0012661] MON MUX_V: 7 Bias 0 1279 -> 0.272359 V Bias 1 1680 -> 0.351604 V, Temperature 0.0792441 -> -24.4189 C
[17:27:09:829][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0012661] MON MUX_V: 15 Bias 0 1271 -> 0.270779 V Bias 1 1673 -> 0.35022 V, Temperature 0.0794417 -> -25.2429 C
[17:27:09:864][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0012629 on Rx 7
[17:27:09:868][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0012629] MON MUX_V: 3 Bias 0 1281 -> 0.277118 V Bias 1 1657 -> 0.351566 V, Temperature 0.074448 -> -15.4559 C
[17:27:09:872][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0012629] MON MUX_V: 5 Bias 0 1250 -> 0.27098 V Bias 1 1632 -> 0.346616 V, Temperature 0.075636 -> -18.0435 C
[17:27:09:877][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0012629] MON MUX_V: 7 Bias 0 1239 -> 0.268802 V Bias 1 1621 -> 0.344438 V, Temperature 0.075636 -> -18.1374 C
[17:27:09:880][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0012629] MON MUX_V: 15 Bias 0 1217 -> 0.264446 V Bias 1 1598 -> 0.339884 V, Temperature 0.075438 -> -17.532 C
[17:27:09:917][  info  ][  scanConsole  ]: Scan done!
[17:27:09:917][  info  ][  scanConsole  ]: Waiting for processors to finish ...
[17:27:09:918][  info  ][  scanConsole  ]: Processor done, waiting for histogrammer ...
[17:27:10:317][  info  ][HistogramAlgorithm]: Histogrammer done!
[17:27:10:317][  info  ][HistogramAlgorithm]: Histogrammer done!
[17:27:10:317][  info  ][HistogramAlgorithm]: Histogrammer done!
[17:27:10:317][  info  ][  scanConsole  ]: Processor done, waiting for analysis ...
[17:27:10:718][  info  ][AnalysisAlgorithm]: Analysis done!
[17:27:10:718][  info  ][AnalysisAlgorithm]: Analysis done!
[17:27:10:718][  info  ][AnalysisAlgorithm]: Analysis done!
[17:27:10:718][  info  ][  scanConsole  ]: All done!
[17:27:10:718][  info  ][  scanConsole  ]: ##########
[17:27:10:718][  info  ][  scanConsole  ]: # Timing #
[17:27:10:718][  info  ][  scanConsole  ]: ##########
[17:27:10:718][  info  ][  scanConsole  ]: -> Configuration: 90 ms
[17:27:10:718][  info  ][  scanConsole  ]: -> Scan:          154 ms
[17:27:10:718][  info  ][  scanConsole  ]: -> Processing:    0 ms
[17:27:10:718][  info  ][  scanConsole  ]: -> Analysis:      800 ms
[17:27:10:718][  info  ][  scanConsole  ]: ###########
[17:27:10:718][  info  ][  scanConsole  ]: # Cleanup #
[17:27:10:718][  info  ][  scanConsole  ]: ###########
[17:27:10:722][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0012613 to configs/CERNQ7/20UPGFC0012613.json
[17:27:10:919][  info  ][  scanConsole  ]: -> Storing output results of FE 4
[17:27:10:919][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0012613, this usually means that the chip did not send any data at all.
[17:27:10:921][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0012661 to configs/CERNQ7/20UPGFC0012661.json
[17:27:11:088][  info  ][  scanConsole  ]: -> Storing output results of FE 6
[17:27:11:088][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0012661, this usually means that the chip did not send any data at all.
[17:27:11:090][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0012629 to configs/CERNQ7/20UPGFC0012629.json
[17:27:11:258][  info  ][  scanConsole  ]: -> Storing output results of FE 7
[17:27:11:258][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0012629, this usually means that the chip did not send any data at all.
[17:27:11:259][  info  ][  scanConsole  ]: Finishing run: 1740
20UPGFC0012613.json.after
20UPGFC0012613.json.before
20UPGFC0012629.json.after
20UPGFC0012629.json.before
20UPGFC0012661.json.after
20UPGFC0012661.json.before
reg_readtemp.json
scanLog.json

