[17:28:04:349][  info  ][               ]: #####################################
[17:28:04:349][  info  ][               ]: # Welcome to the YARR Scan Console! #
[17:28:04:349][  info  ][               ]: #####################################
[17:28:04:349][  info  ][               ]: -> Parsing command line parameters ...
[17:28:04:349][  info  ][               ]: Configuring logger ...
[17:28:04:351][  info  ][  scanConsole  ]: Scan Type/Config configs/scans/rd53a/reg_readtemp.json
[17:28:04:351][  info  ][  scanConsole  ]: Connectivity:
[17:28:04:351][  info  ][  scanConsole  ]:     configs/CERNQ7/example_rd53a_setup.json
[17:28:04:351][  info  ][  scanConsole  ]: Target ToT: -1
[17:28:04:351][  info  ][  scanConsole  ]: Target Charge: -1
[17:28:04:351][  info  ][  scanConsole  ]: Output Plots: true
[17:28:04:351][  info  ][  scanConsole  ]: Output Directory: ./data/001741_reg_readtemp/
[17:28:04:357][  info  ][  scanConsole  ]: Timestamp: 2022-08-23_17:28:04
[17:28:04:357][  info  ][  scanConsole  ]: Run Number: 1741
[17:28:04:357][  info  ][  scanConsole  ]: #################
[17:28:04:357][  info  ][  scanConsole  ]: # Init Hardware #
[17:28:04:357][  info  ][  scanConsole  ]: #################
[17:28:04:357][  info  ][  scanConsole  ]: -> Opening controller config: configs/controller/specCfg-rd53a-16x1.json
[17:28:04:357][  info  ][  ScanHelper   ]: Loading controller ...
[17:28:04:357][  info  ][  ScanHelper   ]: Found controller of type: spec
[17:28:04:357][  info  ][  ScanHelper   ]: ... loading controler config:
[17:28:04:357][  info  ][  ScanHelper   ]: ~~~ {
[17:28:04:357][  info  ][  ScanHelper   ]: ~~~     "cmdPeriod": 6.250000073038109e-9,
[17:28:04:357][  info  ][  ScanHelper   ]: ~~~     "idle": {
[17:28:04:357][  info  ][  ScanHelper   ]: ~~~         "word": 1768515945
[17:28:04:357][  info  ][  ScanHelper   ]: ~~~     },
[17:28:04:357][  info  ][  ScanHelper   ]: ~~~     "pulse": {
[17:28:04:357][  info  ][  ScanHelper   ]: ~~~         "interval": 500,
[17:28:04:357][  info  ][  ScanHelper   ]: ~~~         "word": 1549575846
[17:28:04:357][  info  ][  ScanHelper   ]: ~~~     },
[17:28:04:357][  info  ][  ScanHelper   ]: ~~~     "rxActiveLanes": 1,
[17:28:04:357][  info  ][  ScanHelper   ]: ~~~     "rxPolarity": 0,
[17:28:04:357][  info  ][  ScanHelper   ]: ~~~     "specNum": 0,
[17:28:04:357][  info  ][  ScanHelper   ]: ~~~     "spiConfig": 541200,
[17:28:04:357][  info  ][  ScanHelper   ]: ~~~     "sync": {
[17:28:04:357][  info  ][  ScanHelper   ]: ~~~         "interval": 32,
[17:28:04:357][  info  ][  ScanHelper   ]: ~~~         "word": 2172551550
[17:28:04:357][  info  ][  ScanHelper   ]: ~~~     },
[17:28:04:357][  info  ][  ScanHelper   ]: ~~~     "txPolarity": 0
[17:28:04:357][  info  ][  ScanHelper   ]: ~~~ }
[17:28:04:357][  info  ][    SpecCom    ]: Opening SPEC with id #0
[17:28:04:357][  info  ][    SpecCom    ]: Mapping BARs ...
[17:28:04:357][  info  ][    SpecCom    ]: ... Mapped BAR0 at 0x7f3824fd4000 with size 1048576
[17:28:04:357][warning ][    SpecCom    ]: ... BAR4 not mapped (Mmap failed)
[17:28:04:357][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[17:28:04:357][  info  ][    SpecCom    ]: Firmware Version: 0x4d9ff6d
[17:28:04:357][  info  ][    SpecCom    ]: Firmware Identifier: 0x4030232
[17:28:04:357][  info  ][    SpecCom    ]: FPGA card: PLDA XpressK7 325
[17:28:04:357][  info  ][    SpecCom    ]: FE Chip Type: RD53A/B
[17:28:04:357][  info  ][    SpecCom    ]: FMC Card Type: Ohio Card (Display Port)
[17:28:04:357][  info  ][    SpecCom    ]: RX Speed: 640Mbps
[17:28:04:357][  info  ][    SpecCom    ]: Channel Configuration: 16x1
[17:28:04:357][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[17:28:04:357][  info  ][    SpecCom    ]: Flushing buffers ...
[17:28:04:357][  info  ][    SpecCom    ]: Init success!
[17:28:04:357][  info  ][  scanConsole  ]: #######################
[17:28:04:357][  info  ][  scanConsole  ]: ##  Loading Configs  ##
[17:28:04:357][  info  ][  scanConsole  ]: #######################
[17:28:04:357][  info  ][  scanConsole  ]: Opening global config: configs/CERNQ7/example_rd53a_setup.json
[17:28:04:357][  info  ][  ScanHelper   ]: Chip type: RD53A
[17:28:04:357][  info  ][  ScanHelper   ]: Chip count 4
[17:28:04:357][  info  ][  ScanHelper   ]: Loading chip #0
[17:28:04:358][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(4)
[17:28:04:358][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ7/20UPGFC0012613.json
[17:28:04:541][  info  ][  ScanHelper   ]: Loading chip #1
[17:28:04:541][warning ][  ScanHelper   ]:  ... chip not enabled, skipping!
[17:28:04:541][  info  ][  ScanHelper   ]: Loading chip #2
[17:28:04:542][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(6)
[17:28:04:542][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ7/20UPGFC0012661.json
[17:28:04:713][  info  ][  ScanHelper   ]: Loading chip #3
[17:28:04:714][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(7)
[17:28:04:714][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ7/20UPGFC0012629.json
[17:28:04:888][  info  ][  scanConsole  ]: #################
[17:28:04:889][  info  ][  scanConsole  ]: # Configure FEs #
[17:28:04:889][  info  ][  scanConsole  ]: #################
[17:28:04:889][  info  ][  scanConsole  ]: Configuring 20UPGFC0012613
[17:28:04:919][  info  ][  scanConsole  ]: Configuring 20UPGFC0012661
[17:28:04:949][  info  ][  scanConsole  ]: Configuring 20UPGFC0012629
[17:28:04:979][  info  ][  scanConsole  ]: Sent configuration to all FEs in 90 ms!
[17:28:04:980][  info  ][  scanConsole  ]: Checking com 20UPGFC0012613
[17:28:04:980][  info  ][    SpecRx     ]: Active Rx channels: 0x10
[17:28:04:980][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[17:28:04:980][  info  ][    SpecRx     ]: Rx Status 0xd0
[17:28:04:980][  info  ][    SpecRx     ]: Number of lanes: 1
[17:28:04:980][  info  ][    SpecRx     ]: Channel 4 Lane 0 synchronized!
[17:28:04:990][  info  ][  scanConsole  ]: ... success!
[17:28:04:990][  info  ][  scanConsole  ]: Checking com 20UPGFC0012661
[17:28:04:990][  info  ][    SpecRx     ]: Active Rx channels: 0x40
[17:28:04:990][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[17:28:04:990][  info  ][    SpecRx     ]: Rx Status 0xd0
[17:28:04:990][  info  ][    SpecRx     ]: Number of lanes: 1
[17:28:04:990][  info  ][    SpecRx     ]: Channel 6 Lane 0 synchronized!
[17:28:05:001][  info  ][  scanConsole  ]: ... success!
[17:28:05:001][  info  ][  scanConsole  ]: Checking com 20UPGFC0012629
[17:28:05:001][  info  ][    SpecRx     ]: Active Rx channels: 0x80
[17:28:05:001][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[17:28:05:001][  info  ][    SpecRx     ]: Rx Status 0xd0
[17:28:05:001][  info  ][    SpecRx     ]: Number of lanes: 1
[17:28:05:001][  info  ][    SpecRx     ]: Channel 7 Lane 0 synchronized!
[17:28:05:011][  info  ][  scanConsole  ]: ... success!
[17:28:05:011][  info  ][  scanConsole  ]: Enabling Tx channels
[17:28:05:011][  info  ][  scanConsole  ]: Enabling Tx channel 1
[17:28:05:011][  info  ][  scanConsole  ]: Enabling Tx channel 1
[17:28:05:011][  info  ][  scanConsole  ]: Enabling Tx channel 1
[17:28:05:011][  info  ][  scanConsole  ]: Enabling Rx channels
[17:28:05:011][  info  ][  scanConsole  ]: Enabling Rx channel 4
[17:28:05:011][  info  ][  scanConsole  ]: Enabling Rx channel 6
[17:28:05:011][  info  ][  scanConsole  ]: Enabling Rx channel 7
[17:28:05:011][  info  ][  scanConsole  ]: ##############
[17:28:05:011][  info  ][  scanConsole  ]: # Setup Scan #
[17:28:05:011][  info  ][  scanConsole  ]: ##############
[17:28:05:011][  info  ][  scanConsole  ]: Found Scan config, constructing scan ...
[17:28:05:011][  info  ][  ScanFactory  ]: Loading Scan:
[17:28:05:011][  info  ][  ScanFactory  ]:   Name: AnalogScan
[17:28:05:011][  info  ][  ScanFactory  ]:   Number of Loops: 3
[17:28:05:011][  info  ][  ScanFactory  ]:   Loading Loop #0
[17:28:05:011][  info  ][  ScanFactory  ]:    Type: Rd53aReadRegLoop
[17:28:05:011][  info  ][  ScanFactory  ]:    Loading loop config ... 
[17:28:05:011][  info  ][  ScanFactory  ]: ~~~ {
[17:28:05:011][  info  ][  ScanFactory  ]: ~~~     "CurMux": [],
[17:28:05:011][  info  ][  ScanFactory  ]: ~~~     "EnblRingOsc": 0,
[17:28:05:011][  info  ][  ScanFactory  ]: ~~~     "Registers": [],
[17:28:05:011][  info  ][  ScanFactory  ]: ~~~     "RingOscDur": 9,
[17:28:05:011][  info  ][  ScanFactory  ]: ~~~     "RingOscRep": 10,
[17:28:05:011][  info  ][  ScanFactory  ]: ~~~     "VoltMux": [3, 5, 7, 15]
[17:28:05:011][  info  ][  ScanFactory  ]: ~~~ }
[17:28:05:011][  info  ][  ScanFactory  ]:   Loading Loop #1
[17:28:05:011][  info  ][  ScanFactory  ]:    Type: Rd53aTriggerLoop
[17:28:05:011][  info  ][  ScanFactory  ]:    Loading loop config ... 
[17:28:05:011][  info  ][  ScanFactory  ]: ~~~ {
[17:28:05:011][  info  ][  ScanFactory  ]: ~~~     "count": 1,
[17:28:05:011][  info  ][  ScanFactory  ]: ~~~     "delay": 48,
[17:28:05:011][  info  ][  ScanFactory  ]: ~~~     "extTrig": false,
[17:28:05:011][  info  ][  ScanFactory  ]: ~~~     "frequency": 30000.0,
[17:28:05:011][  info  ][  ScanFactory  ]: ~~~     "noInject": false,
[17:28:05:011][  info  ][  ScanFactory  ]: ~~~     "sendEcr": false,
[17:28:05:011][  info  ][  ScanFactory  ]: ~~~     "time": 0.0,
[17:28:05:011][  info  ][  ScanFactory  ]: ~~~     "trigMultiplier": 16
[17:28:05:011][  info  ][  ScanFactory  ]: ~~~ }
[17:28:05:011][  info  ][  ScanFactory  ]:   Loading Loop #2
[17:28:05:012][  info  ][  ScanFactory  ]:    Type: StdDataLoop
[17:28:05:012][warning ][  ScanFactory  ]: ~~~ Config empty.
[17:28:05:012][  info  ][ScanBuildHistogrammers]: Loading histogrammer ...
[17:28:05:012][  info  ][ScanBuildHistogrammers]: ... done!
[17:28:05:012][  info  ][ScanBuildAnalyses]: Loading analyses ...
[17:28:05:012][  info  ][  scanConsole  ]: Running pre scan!
[17:28:05:012][  info  ][  ScanFactory  ]: Entering pre scan phase ...
[17:28:05:012][  info  ][     Rd53a     ]: Write named register: GlobalPulseRt -> 16384
[17:28:05:012][  info  ][     Rd53a     ]: Write named register: InjEnDig -> 0
[17:28:05:012][  info  ][     Rd53a     ]: Write named register: InjVcalDiff -> 0
[17:28:05:012][  info  ][     Rd53a     ]: Write named register: InjVcalHigh -> 3500
[17:28:05:012][  info  ][     Rd53a     ]: Write named register: InjVcalMed -> 500
[17:28:05:012][  info  ][     Rd53a     ]: Write named register: LatencyConfig -> 48
[17:28:05:012][  info  ][  scanConsole  ]: Starting histogrammer and analysis threads:
[17:28:05:012][  info  ][  scanConsole  ]:  .. started threads of Fe 4
[17:28:05:012][  info  ][  scanConsole  ]:  .. started threads of Fe 6
[17:28:05:012][  info  ][  scanConsole  ]:  .. started threads of Fe 7
[17:28:05:012][  info  ][Rd53aDataProcessor]:   -> Processor thread #0 started!
[17:28:05:012][  info  ][Rd53aDataProcessor]:   -> Processor thread #1 started!
[17:28:05:012][  info  ][Rd53aDataProcessor]:   -> Processor thread #2 started!
[17:28:05:012][  info  ][Rd53aDataProcessor]:   -> Processor thread #3 started!
[17:28:05:012][  info  ][  scanConsole  ]: ########
[17:28:05:012][  info  ][  scanConsole  ]: # Scan #
[17:28:05:012][  info  ][  scanConsole  ]: ########
[17:28:05:012][  info  ][  scanConsole  ]: Starting scan!
[17:28:05:012][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0012613 on Rx 4
[17:28:05:016][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0012613] MON MUX_V: 3 Bias 0 1319 -> 0.283276 V Bias 1 1701 -> 0.358767 V, Temperature 0.0754913 -> -18.7957 C
[17:28:05:021][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0012613] MON MUX_V: 5 Bias 0 1269 -> 0.273395 V Bias 1 1646 -> 0.347898 V, Temperature 0.0745032 -> -21.0208 C
[17:28:05:025][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0012613] MON MUX_V: 7 Bias 0 1269 -> 0.273395 V Bias 1 1656 -> 0.349874 V, Temperature 0.0764794 -> -18.8583 C
[17:28:05:029][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0012613] MON MUX_V: 15 Bias 0 1271 -> 0.27379 V Bias 1 1660 -> 0.350665 V, Temperature 0.0768747 -> -21.8043 C
[17:28:05:064][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0012661 on Rx 6
[17:28:05:069][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0012661] MON MUX_V: 3 Bias 0 1341 -> 0.284612 V Bias 1 1729 -> 0.361287 V, Temperature 0.076675 -> -16.1482 C
[17:28:05:073][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0012661] MON MUX_V: 5 Bias 0 1299 -> 0.276312 V Bias 1 1696 -> 0.354765 V, Temperature 0.0784536 -> -24.4189 C
[17:28:05:078][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0012661] MON MUX_V: 7 Bias 0 1278 -> 0.272162 V Bias 1 1681 -> 0.351801 V, Temperature 0.0796393 -> -22.7715 C
[17:28:05:082][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0012661] MON MUX_V: 15 Bias 0 1272 -> 0.270976 V Bias 1 1674 -> 0.350418 V, Temperature 0.0794417 -> -25.2427 C
[17:28:05:117][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0012629 on Rx 7
[17:28:05:121][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0012629] MON MUX_V: 3 Bias 0 1281 -> 0.277118 V Bias 1 1659 -> 0.351962 V, Temperature 0.074844 -> -14.072 C
[17:28:05:125][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0012629] MON MUX_V: 5 Bias 0 1251 -> 0.271178 V Bias 1 1632 -> 0.346616 V, Temperature 0.075438 -> -18.7957 C
[17:28:05:129][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0012629] MON MUX_V: 7 Bias 0 1239 -> 0.268802 V Bias 1 1621 -> 0.344438 V, Temperature 0.075636 -> -18.1374 C
[17:28:05:133][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0012629] MON MUX_V: 15 Bias 0 1217 -> 0.264446 V Bias 1 1597 -> 0.339686 V, Temperature 0.07524 -> -18.224 C
[17:28:05:170][  info  ][  scanConsole  ]: Scan done!
[17:28:05:170][  info  ][  scanConsole  ]: Waiting for processors to finish ...
[17:28:05:171][  info  ][  scanConsole  ]: Processor done, waiting for histogrammer ...
[17:28:05:570][  info  ][HistogramAlgorithm]: Histogrammer done!
[17:28:05:570][  info  ][HistogramAlgorithm]: Histogrammer done!
[17:28:05:570][  info  ][HistogramAlgorithm]: Histogrammer done!
[17:28:05:571][  info  ][  scanConsole  ]: Processor done, waiting for analysis ...
[17:28:05:971][  info  ][AnalysisAlgorithm]: Analysis done!
[17:28:05:971][  info  ][AnalysisAlgorithm]: Analysis done!
[17:28:05:971][  info  ][AnalysisAlgorithm]: Analysis done!
[17:28:05:971][  info  ][  scanConsole  ]: All done!
[17:28:05:971][  info  ][  scanConsole  ]: ##########
[17:28:05:971][  info  ][  scanConsole  ]: # Timing #
[17:28:05:971][  info  ][  scanConsole  ]: ##########
[17:28:05:971][  info  ][  scanConsole  ]: -> Configuration: 90 ms
[17:28:05:971][  info  ][  scanConsole  ]: -> Scan:          157 ms
[17:28:05:971][  info  ][  scanConsole  ]: -> Processing:    0 ms
[17:28:05:971][  info  ][  scanConsole  ]: -> Analysis:      800 ms
[17:28:05:971][  info  ][  scanConsole  ]: ###########
[17:28:05:971][  info  ][  scanConsole  ]: # Cleanup #
[17:28:05:971][  info  ][  scanConsole  ]: ###########
[17:28:05:979][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0012613 to configs/CERNQ7/20UPGFC0012613.json
[17:28:06:186][  info  ][  scanConsole  ]: -> Storing output results of FE 4
[17:28:06:186][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0012613, this usually means that the chip did not send any data at all.
[17:28:06:187][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0012661 to configs/CERNQ7/20UPGFC0012661.json
[17:28:06:353][  info  ][  scanConsole  ]: -> Storing output results of FE 6
[17:28:06:353][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0012661, this usually means that the chip did not send any data at all.
[17:28:06:354][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0012629 to configs/CERNQ7/20UPGFC0012629.json
[17:28:06:520][  info  ][  scanConsole  ]: -> Storing output results of FE 7
[17:28:06:520][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0012629, this usually means that the chip did not send any data at all.
[17:28:06:521][  info  ][  scanConsole  ]: Finishing run: 1741
20UPGFC0012613.json.after
20UPGFC0012613.json.before
20UPGFC0012629.json.after
20UPGFC0012629.json.before
20UPGFC0012661.json.after
20UPGFC0012661.json.before
reg_readtemp.json
scanLog.json

