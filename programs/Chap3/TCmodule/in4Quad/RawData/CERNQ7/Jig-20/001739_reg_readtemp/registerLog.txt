[17:25:16:426][  info  ][               ]: #####################################
[17:25:16:426][  info  ][               ]: # Welcome to the YARR Scan Console! #
[17:25:16:426][  info  ][               ]: #####################################
[17:25:16:426][  info  ][               ]: -> Parsing command line parameters ...
[17:25:16:426][  info  ][               ]: Configuring logger ...
[17:25:16:428][  info  ][  scanConsole  ]: Scan Type/Config configs/scans/rd53a/reg_readtemp.json
[17:25:16:428][  info  ][  scanConsole  ]: Connectivity:
[17:25:16:428][  info  ][  scanConsole  ]:     configs/CERNQ7/example_rd53a_setup.json
[17:25:16:428][  info  ][  scanConsole  ]: Target ToT: -1
[17:25:16:428][  info  ][  scanConsole  ]: Target Charge: -1
[17:25:16:428][  info  ][  scanConsole  ]: Output Plots: true
[17:25:16:428][  info  ][  scanConsole  ]: Output Directory: ./data/001739_reg_readtemp/
[17:25:16:432][  info  ][  scanConsole  ]: Timestamp: 2022-08-23_17:25:16
[17:25:16:432][  info  ][  scanConsole  ]: Run Number: 1739
[17:25:16:432][  info  ][  scanConsole  ]: #################
[17:25:16:432][  info  ][  scanConsole  ]: # Init Hardware #
[17:25:16:432][  info  ][  scanConsole  ]: #################
[17:25:16:432][  info  ][  scanConsole  ]: -> Opening controller config: configs/controller/specCfg-rd53a-16x1.json
[17:25:16:432][  info  ][  ScanHelper   ]: Loading controller ...
[17:25:16:432][  info  ][  ScanHelper   ]: Found controller of type: spec
[17:25:16:432][  info  ][  ScanHelper   ]: ... loading controler config:
[17:25:16:432][  info  ][  ScanHelper   ]: ~~~ {
[17:25:16:432][  info  ][  ScanHelper   ]: ~~~     "cmdPeriod": 6.250000073038109e-9,
[17:25:16:432][  info  ][  ScanHelper   ]: ~~~     "idle": {
[17:25:16:432][  info  ][  ScanHelper   ]: ~~~         "word": 1768515945
[17:25:16:432][  info  ][  ScanHelper   ]: ~~~     },
[17:25:16:432][  info  ][  ScanHelper   ]: ~~~     "pulse": {
[17:25:16:432][  info  ][  ScanHelper   ]: ~~~         "interval": 500,
[17:25:16:432][  info  ][  ScanHelper   ]: ~~~         "word": 1549575846
[17:25:16:432][  info  ][  ScanHelper   ]: ~~~     },
[17:25:16:432][  info  ][  ScanHelper   ]: ~~~     "rxActiveLanes": 1,
[17:25:16:432][  info  ][  ScanHelper   ]: ~~~     "rxPolarity": 0,
[17:25:16:432][  info  ][  ScanHelper   ]: ~~~     "specNum": 0,
[17:25:16:432][  info  ][  ScanHelper   ]: ~~~     "spiConfig": 541200,
[17:25:16:432][  info  ][  ScanHelper   ]: ~~~     "sync": {
[17:25:16:432][  info  ][  ScanHelper   ]: ~~~         "interval": 32,
[17:25:16:432][  info  ][  ScanHelper   ]: ~~~         "word": 2172551550
[17:25:16:432][  info  ][  ScanHelper   ]: ~~~     },
[17:25:16:432][  info  ][  ScanHelper   ]: ~~~     "txPolarity": 0
[17:25:16:432][  info  ][  ScanHelper   ]: ~~~ }
[17:25:16:432][  info  ][    SpecCom    ]: Opening SPEC with id #0
[17:25:16:432][  info  ][    SpecCom    ]: Mapping BARs ...
[17:25:16:432][  info  ][    SpecCom    ]: ... Mapped BAR0 at 0x7f65a2afc000 with size 1048576
[17:25:16:432][warning ][    SpecCom    ]: ... BAR4 not mapped (Mmap failed)
[17:25:16:433][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[17:25:16:433][  info  ][    SpecCom    ]: Firmware Version: 0x4d9ff6d
[17:25:16:433][  info  ][    SpecCom    ]: Firmware Identifier: 0x4030232
[17:25:16:433][  info  ][    SpecCom    ]: FPGA card: PLDA XpressK7 325
[17:25:16:433][  info  ][    SpecCom    ]: FE Chip Type: RD53A/B
[17:25:16:433][  info  ][    SpecCom    ]: FMC Card Type: Ohio Card (Display Port)
[17:25:16:433][  info  ][    SpecCom    ]: RX Speed: 640Mbps
[17:25:16:433][  info  ][    SpecCom    ]: Channel Configuration: 16x1
[17:25:16:433][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[17:25:16:433][  info  ][    SpecCom    ]: Flushing buffers ...
[17:25:16:433][  info  ][    SpecCom    ]: Init success!
[17:25:16:433][  info  ][  scanConsole  ]: #######################
[17:25:16:433][  info  ][  scanConsole  ]: ##  Loading Configs  ##
[17:25:16:433][  info  ][  scanConsole  ]: #######################
[17:25:16:433][  info  ][  scanConsole  ]: Opening global config: configs/CERNQ7/example_rd53a_setup.json
[17:25:16:433][  info  ][  ScanHelper   ]: Chip type: RD53A
[17:25:16:433][  info  ][  ScanHelper   ]: Chip count 4
[17:25:16:433][  info  ][  ScanHelper   ]: Loading chip #0
[17:25:16:433][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(4)
[17:25:16:433][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ7/20UPGFC0012613.json
[17:25:16:610][  info  ][  ScanHelper   ]: Loading chip #1
[17:25:16:610][warning ][  ScanHelper   ]:  ... chip not enabled, skipping!
[17:25:16:610][  info  ][  ScanHelper   ]: Loading chip #2
[17:25:16:610][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(6)
[17:25:16:610][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ7/20UPGFC0012661.json
[17:25:16:782][  info  ][  ScanHelper   ]: Loading chip #3
[17:25:16:783][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(7)
[17:25:16:783][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ7/20UPGFC0012629.json
[17:25:16:957][  info  ][  scanConsole  ]: #################
[17:25:16:957][  info  ][  scanConsole  ]: # Configure FEs #
[17:25:16:957][  info  ][  scanConsole  ]: #################
[17:25:16:957][  info  ][  scanConsole  ]: Configuring 20UPGFC0012613
[17:25:16:987][  info  ][  scanConsole  ]: Configuring 20UPGFC0012661
[17:25:17:018][  info  ][  scanConsole  ]: Configuring 20UPGFC0012629
[17:25:17:059][  info  ][  scanConsole  ]: Sent configuration to all FEs in 102 ms!
[17:25:17:060][  info  ][  scanConsole  ]: Checking com 20UPGFC0012613
[17:25:17:060][  info  ][    SpecRx     ]: Active Rx channels: 0x10
[17:25:17:060][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[17:25:17:060][  info  ][    SpecRx     ]: Rx Status 0xd0
[17:25:17:060][  info  ][    SpecRx     ]: Number of lanes: 1
[17:25:17:060][  info  ][    SpecRx     ]: Channel 4 Lane 0 synchronized!
[17:25:17:071][  info  ][  scanConsole  ]: ... success!
[17:25:17:071][  info  ][  scanConsole  ]: Checking com 20UPGFC0012661
[17:25:17:071][  info  ][    SpecRx     ]: Active Rx channels: 0x40
[17:25:17:071][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[17:25:17:071][  info  ][    SpecRx     ]: Rx Status 0xd0
[17:25:17:071][  info  ][    SpecRx     ]: Number of lanes: 1
[17:25:17:071][  info  ][    SpecRx     ]: Channel 6 Lane 0 synchronized!
[17:25:17:081][  info  ][  scanConsole  ]: ... success!
[17:25:17:081][  info  ][  scanConsole  ]: Checking com 20UPGFC0012629
[17:25:17:081][  info  ][    SpecRx     ]: Active Rx channels: 0x80
[17:25:17:081][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[17:25:17:081][  info  ][    SpecRx     ]: Rx Status 0xd0
[17:25:17:081][  info  ][    SpecRx     ]: Number of lanes: 1
[17:25:17:081][  info  ][    SpecRx     ]: Channel 7 Lane 0 synchronized!
[17:25:17:092][  info  ][  scanConsole  ]: ... success!
[17:25:17:092][  info  ][  scanConsole  ]: Enabling Tx channels
[17:25:17:092][  info  ][  scanConsole  ]: Enabling Tx channel 1
[17:25:17:092][  info  ][  scanConsole  ]: Enabling Tx channel 1
[17:25:17:092][  info  ][  scanConsole  ]: Enabling Tx channel 1
[17:25:17:092][  info  ][  scanConsole  ]: Enabling Rx channels
[17:25:17:092][  info  ][  scanConsole  ]: Enabling Rx channel 4
[17:25:17:092][  info  ][  scanConsole  ]: Enabling Rx channel 6
[17:25:17:092][  info  ][  scanConsole  ]: Enabling Rx channel 7
[17:25:17:092][  info  ][  scanConsole  ]: ##############
[17:25:17:092][  info  ][  scanConsole  ]: # Setup Scan #
[17:25:17:092][  info  ][  scanConsole  ]: ##############
[17:25:17:092][  info  ][  scanConsole  ]: Found Scan config, constructing scan ...
[17:25:17:093][  info  ][  ScanFactory  ]: Loading Scan:
[17:25:17:093][  info  ][  ScanFactory  ]:   Name: AnalogScan
[17:25:17:093][  info  ][  ScanFactory  ]:   Number of Loops: 3
[17:25:17:093][  info  ][  ScanFactory  ]:   Loading Loop #0
[17:25:17:093][  info  ][  ScanFactory  ]:    Type: Rd53aReadRegLoop
[17:25:17:093][  info  ][  ScanFactory  ]:    Loading loop config ... 
[17:25:17:093][  info  ][  ScanFactory  ]: ~~~ {
[17:25:17:093][  info  ][  ScanFactory  ]: ~~~     "CurMux": [],
[17:25:17:093][  info  ][  ScanFactory  ]: ~~~     "EnblRingOsc": 0,
[17:25:17:093][  info  ][  ScanFactory  ]: ~~~     "Registers": [],
[17:25:17:093][  info  ][  ScanFactory  ]: ~~~     "RingOscDur": 9,
[17:25:17:093][  info  ][  ScanFactory  ]: ~~~     "RingOscRep": 10,
[17:25:17:093][  info  ][  ScanFactory  ]: ~~~     "VoltMux": [3, 5, 7, 15]
[17:25:17:093][  info  ][  ScanFactory  ]: ~~~ }
[17:25:17:093][  info  ][  ScanFactory  ]:   Loading Loop #1
[17:25:17:093][  info  ][  ScanFactory  ]:    Type: Rd53aTriggerLoop
[17:25:17:093][  info  ][  ScanFactory  ]:    Loading loop config ... 
[17:25:17:093][  info  ][  ScanFactory  ]: ~~~ {
[17:25:17:093][  info  ][  ScanFactory  ]: ~~~     "count": 1,
[17:25:17:093][  info  ][  ScanFactory  ]: ~~~     "delay": 48,
[17:25:17:093][  info  ][  ScanFactory  ]: ~~~     "extTrig": false,
[17:25:17:093][  info  ][  ScanFactory  ]: ~~~     "frequency": 30000.0,
[17:25:17:093][  info  ][  ScanFactory  ]: ~~~     "noInject": false,
[17:25:17:093][  info  ][  ScanFactory  ]: ~~~     "sendEcr": false,
[17:25:17:093][  info  ][  ScanFactory  ]: ~~~     "time": 0.0,
[17:25:17:093][  info  ][  ScanFactory  ]: ~~~     "trigMultiplier": 16
[17:25:17:094][  info  ][  ScanFactory  ]: ~~~ }
[17:25:17:094][  info  ][  ScanFactory  ]:   Loading Loop #2
[17:25:17:094][  info  ][  ScanFactory  ]:    Type: StdDataLoop
[17:25:17:094][warning ][  ScanFactory  ]: ~~~ Config empty.
[17:25:17:094][  info  ][ScanBuildHistogrammers]: Loading histogrammer ...
[17:25:17:094][  info  ][ScanBuildHistogrammers]: ... done!
[17:25:17:094][  info  ][ScanBuildAnalyses]: Loading analyses ...
[17:25:17:094][  info  ][  scanConsole  ]: Running pre scan!
[17:25:17:094][  info  ][  ScanFactory  ]: Entering pre scan phase ...
[17:25:17:094][  info  ][     Rd53a     ]: Write named register: GlobalPulseRt -> 16384
[17:25:17:094][  info  ][     Rd53a     ]: Write named register: InjEnDig -> 0
[17:25:17:095][  info  ][     Rd53a     ]: Write named register: InjVcalDiff -> 0
[17:25:17:095][  info  ][     Rd53a     ]: Write named register: InjVcalHigh -> 3500
[17:25:17:095][  info  ][     Rd53a     ]: Write named register: InjVcalMed -> 500
[17:25:17:095][  info  ][     Rd53a     ]: Write named register: LatencyConfig -> 48
[17:25:17:095][  info  ][  scanConsole  ]: Starting histogrammer and analysis threads:
[17:25:17:095][  info  ][  scanConsole  ]:  .. started threads of Fe 4
[17:25:17:095][  info  ][  scanConsole  ]:  .. started threads of Fe 6
[17:25:17:095][  info  ][  scanConsole  ]:  .. started threads of Fe 7
[17:25:17:095][  info  ][Rd53aDataProcessor]:   -> Processor thread #0 started!
[17:25:17:095][  info  ][Rd53aDataProcessor]:   -> Processor thread #1 started!
[17:25:17:096][  info  ][Rd53aDataProcessor]:   -> Processor thread #2 started!
[17:25:17:096][  info  ][Rd53aDataProcessor]:   -> Processor thread #3 started!
[17:25:17:096][  info  ][  scanConsole  ]: ########
[17:25:17:096][  info  ][  scanConsole  ]: # Scan #
[17:25:17:096][  info  ][  scanConsole  ]: ########
[17:25:17:096][  info  ][  scanConsole  ]: Starting scan!
[17:25:17:096][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0012613 on Rx 4
[17:25:17:100][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0012613] MON MUX_V: 3 Bias 0 1318 -> 0.283078 V Bias 1 1700 -> 0.358569 V, Temperature 0.0754913 -> -18.7957 C
[17:25:17:104][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0012613] MON MUX_V: 5 Bias 0 1268 -> 0.273197 V Bias 1 1645 -> 0.3477 V, Temperature 0.0745032 -> -21.0208 C
[17:25:17:108][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0012613] MON MUX_V: 7 Bias 0 1268 -> 0.273197 V Bias 1 1656 -> 0.349874 V, Temperature 0.076677 -> -18.1376 C
[17:25:17:112][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0012613] MON MUX_V: 15 Bias 0 1271 -> 0.27379 V Bias 1 1660 -> 0.350665 V, Temperature 0.0768747 -> -21.8043 C
[17:25:17:148][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0012661 on Rx 6
[17:25:17:151][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0012661] MON MUX_V: 3 Bias 0 1341 -> 0.284612 V Bias 1 1730 -> 0.361484 V, Temperature 0.0768726 -> -15.4561 C
[17:25:17:155][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0012661] MON MUX_V: 5 Bias 0 1299 -> 0.276312 V Bias 1 1697 -> 0.354963 V, Temperature 0.0786512 -> -23.5952 C
[17:25:17:159][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0012661] MON MUX_V: 7 Bias 0 1280 -> 0.272557 V Bias 1 1682 -> 0.351999 V, Temperature 0.0794417 -> -23.5953 C
[17:25:17:163][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0012661] MON MUX_V: 15 Bias 0 1272 -> 0.270976 V Bias 1 1674 -> 0.350418 V, Temperature 0.0794417 -> -25.2427 C
[17:25:17:198][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0012629 on Rx 7
[17:25:17:203][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0012629] MON MUX_V: 3 Bias 0 1281 -> 0.277118 V Bias 1 1659 -> 0.351962 V, Temperature 0.074844 -> -14.072 C
[17:25:17:207][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0012629] MON MUX_V: 5 Bias 0 1251 -> 0.271178 V Bias 1 1632 -> 0.346616 V, Temperature 0.075438 -> -18.7957 C
[17:25:17:211][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0012629] MON MUX_V: 7 Bias 0 1239 -> 0.268802 V Bias 1 1621 -> 0.344438 V, Temperature 0.075636 -> -18.1374 C
[17:25:17:215][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0012629] MON MUX_V: 15 Bias 0 1217 -> 0.264446 V Bias 1 1598 -> 0.339884 V, Temperature 0.075438 -> -17.532 C
[17:25:17:251][  info  ][  scanConsole  ]: Scan done!
[17:25:17:251][  info  ][  scanConsole  ]: Waiting for processors to finish ...
[17:25:17:252][  info  ][  scanConsole  ]: Processor done, waiting for histogrammer ...
[17:25:17:652][  info  ][HistogramAlgorithm]: Histogrammer done!
[17:25:17:652][  info  ][HistogramAlgorithm]: Histogrammer done!
[17:25:17:652][  info  ][HistogramAlgorithm]: Histogrammer done!
[17:25:17:652][  info  ][  scanConsole  ]: Processor done, waiting for analysis ...
[17:25:18:052][  info  ][AnalysisAlgorithm]: Analysis done!
[17:25:18:052][  info  ][AnalysisAlgorithm]: Analysis done!
[17:25:18:052][  info  ][AnalysisAlgorithm]: Analysis done!
[17:25:18:052][  info  ][  scanConsole  ]: All done!
[17:25:18:052][  info  ][  scanConsole  ]: ##########
[17:25:18:052][  info  ][  scanConsole  ]: # Timing #
[17:25:18:053][  info  ][  scanConsole  ]: ##########
[17:25:18:053][  info  ][  scanConsole  ]: -> Configuration: 102 ms
[17:25:18:053][  info  ][  scanConsole  ]: -> Scan:          155 ms
[17:25:18:053][  info  ][  scanConsole  ]: -> Processing:    0 ms
[17:25:18:053][  info  ][  scanConsole  ]: -> Analysis:      800 ms
[17:25:18:053][  info  ][  scanConsole  ]: ###########
[17:25:18:053][  info  ][  scanConsole  ]: # Cleanup #
[17:25:18:053][  info  ][  scanConsole  ]: ###########
[17:25:18:061][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0012613 to configs/CERNQ7/20UPGFC0012613.json
[17:25:18:229][  info  ][  scanConsole  ]: -> Storing output results of FE 4
[17:25:18:229][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0012613, this usually means that the chip did not send any data at all.
[17:25:18:230][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0012661 to configs/CERNQ7/20UPGFC0012661.json
[17:25:18:393][  info  ][  scanConsole  ]: -> Storing output results of FE 6
[17:25:18:393][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0012661, this usually means that the chip did not send any data at all.
[17:25:18:394][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0012629 to configs/CERNQ7/20UPGFC0012629.json
[17:25:18:559][  info  ][  scanConsole  ]: -> Storing output results of FE 7
[17:25:18:559][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0012629, this usually means that the chip did not send any data at all.
[17:25:18:560][  info  ][  scanConsole  ]: Finishing run: 1739
20UPGFC0012613.json.after
20UPGFC0012613.json.before
20UPGFC0012629.json.after
20UPGFC0012629.json.before
20UPGFC0012661.json.after
20UPGFC0012661.json.before
reg_readtemp.json
scanLog.json

