[17:55:01:161][  info  ][               ]: #####################################
[17:55:01:161][  info  ][               ]: # Welcome to the YARR Scan Console! #
[17:55:01:161][  info  ][               ]: #####################################
[17:55:01:162][  info  ][               ]: -> Parsing command line parameters ...
[17:55:01:162][  info  ][               ]: Configuring logger ...
[17:55:01:164][  info  ][  scanConsole  ]: Scan Type/Config configs/scans/rd53a/reg_readtemp.json
[17:55:01:164][  info  ][  scanConsole  ]: Connectivity:
[17:55:01:164][  info  ][  scanConsole  ]:     configs/CERNQ7/example_rd53a_setup.json
[17:55:01:164][  info  ][  scanConsole  ]: Target ToT: -1
[17:55:01:164][  info  ][  scanConsole  ]: Target Charge: -1
[17:55:01:164][  info  ][  scanConsole  ]: Output Plots: true
[17:55:01:164][  info  ][  scanConsole  ]: Output Directory: ./data/001747_reg_readtemp/
[17:55:01:168][  info  ][  scanConsole  ]: Timestamp: 2022-08-23_17:55:01
[17:55:01:168][  info  ][  scanConsole  ]: Run Number: 1747
[17:55:01:168][  info  ][  scanConsole  ]: #################
[17:55:01:168][  info  ][  scanConsole  ]: # Init Hardware #
[17:55:01:168][  info  ][  scanConsole  ]: #################
[17:55:01:168][  info  ][  scanConsole  ]: -> Opening controller config: configs/controller/specCfg-rd53a-16x1.json
[17:55:01:168][  info  ][  ScanHelper   ]: Loading controller ...
[17:55:01:168][  info  ][  ScanHelper   ]: Found controller of type: spec
[17:55:01:168][  info  ][  ScanHelper   ]: ... loading controler config:
[17:55:01:168][  info  ][  ScanHelper   ]: ~~~ {
[17:55:01:169][  info  ][  ScanHelper   ]: ~~~     "cmdPeriod": 6.250000073038109e-9,
[17:55:01:169][  info  ][  ScanHelper   ]: ~~~     "idle": {
[17:55:01:169][  info  ][  ScanHelper   ]: ~~~         "word": 1768515945
[17:55:01:169][  info  ][  ScanHelper   ]: ~~~     },
[17:55:01:169][  info  ][  ScanHelper   ]: ~~~     "pulse": {
[17:55:01:169][  info  ][  ScanHelper   ]: ~~~         "interval": 500,
[17:55:01:169][  info  ][  ScanHelper   ]: ~~~         "word": 1549575846
[17:55:01:169][  info  ][  ScanHelper   ]: ~~~     },
[17:55:01:169][  info  ][  ScanHelper   ]: ~~~     "rxActiveLanes": 1,
[17:55:01:169][  info  ][  ScanHelper   ]: ~~~     "rxPolarity": 0,
[17:55:01:169][  info  ][  ScanHelper   ]: ~~~     "specNum": 0,
[17:55:01:169][  info  ][  ScanHelper   ]: ~~~     "spiConfig": 541200,
[17:55:01:169][  info  ][  ScanHelper   ]: ~~~     "sync": {
[17:55:01:169][  info  ][  ScanHelper   ]: ~~~         "interval": 32,
[17:55:01:169][  info  ][  ScanHelper   ]: ~~~         "word": 2172551550
[17:55:01:169][  info  ][  ScanHelper   ]: ~~~     },
[17:55:01:169][  info  ][  ScanHelper   ]: ~~~     "txPolarity": 0
[17:55:01:169][  info  ][  ScanHelper   ]: ~~~ }
[17:55:01:169][  info  ][    SpecCom    ]: Opening SPEC with id #0
[17:55:01:169][  info  ][    SpecCom    ]: Mapping BARs ...
[17:55:01:169][  info  ][    SpecCom    ]: ... Mapped BAR0 at 0x7fc6422b0000 with size 1048576
[17:55:01:169][warning ][    SpecCom    ]: ... BAR4 not mapped (Mmap failed)
[17:55:01:169][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[17:55:01:169][  info  ][    SpecCom    ]: Firmware Version: 0x4d9ff6d
[17:55:01:169][  info  ][    SpecCom    ]: Firmware Identifier: 0x4030232
[17:55:01:169][  info  ][    SpecCom    ]: FPGA card: PLDA XpressK7 325
[17:55:01:169][  info  ][    SpecCom    ]: FE Chip Type: RD53A/B
[17:55:01:169][  info  ][    SpecCom    ]: FMC Card Type: Ohio Card (Display Port)
[17:55:01:169][  info  ][    SpecCom    ]: RX Speed: 640Mbps
[17:55:01:169][  info  ][    SpecCom    ]: Channel Configuration: 16x1
[17:55:01:169][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[17:55:01:169][  info  ][    SpecCom    ]: Flushing buffers ...
[17:55:01:169][  info  ][    SpecCom    ]: Init success!
[17:55:01:169][  info  ][  scanConsole  ]: #######################
[17:55:01:169][  info  ][  scanConsole  ]: ##  Loading Configs  ##
[17:55:01:169][  info  ][  scanConsole  ]: #######################
[17:55:01:169][  info  ][  scanConsole  ]: Opening global config: configs/CERNQ7/example_rd53a_setup.json
[17:55:01:169][  info  ][  ScanHelper   ]: Chip type: RD53A
[17:55:01:169][  info  ][  ScanHelper   ]: Chip count 4
[17:55:01:169][  info  ][  ScanHelper   ]: Loading chip #0
[17:55:01:170][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(4)
[17:55:01:170][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ7/20UPGFC0012613.json
[17:55:01:348][  info  ][  ScanHelper   ]: Loading chip #1
[17:55:01:348][warning ][  ScanHelper   ]:  ... chip not enabled, skipping!
[17:55:01:348][  info  ][  ScanHelper   ]: Loading chip #2
[17:55:01:348][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(6)
[17:55:01:348][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ7/20UPGFC0012661.json
[17:55:01:521][  info  ][  ScanHelper   ]: Loading chip #3
[17:55:01:522][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(7)
[17:55:01:522][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ7/20UPGFC0012629.json
[17:55:01:697][  info  ][  scanConsole  ]: #################
[17:55:01:697][  info  ][  scanConsole  ]: # Configure FEs #
[17:55:01:697][  info  ][  scanConsole  ]: #################
[17:55:01:697][  info  ][  scanConsole  ]: Configuring 20UPGFC0012613
[17:55:01:728][  info  ][  scanConsole  ]: Configuring 20UPGFC0012661
[17:55:01:768][  info  ][  scanConsole  ]: Configuring 20UPGFC0012629
[17:55:01:798][  info  ][  scanConsole  ]: Sent configuration to all FEs in 101 ms!
[17:55:01:799][  info  ][  scanConsole  ]: Checking com 20UPGFC0012613
[17:55:01:799][  info  ][    SpecRx     ]: Active Rx channels: 0x10
[17:55:01:799][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[17:55:01:799][  info  ][    SpecRx     ]: Rx Status 0xd0
[17:55:01:799][  info  ][    SpecRx     ]: Number of lanes: 1
[17:55:01:799][  info  ][    SpecRx     ]: Channel 4 Lane 0 synchronized!
[17:55:01:810][  info  ][  scanConsole  ]: ... success!
[17:55:01:810][  info  ][  scanConsole  ]: Checking com 20UPGFC0012661
[17:55:01:810][  info  ][    SpecRx     ]: Active Rx channels: 0x40
[17:55:01:810][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[17:55:01:810][  info  ][    SpecRx     ]: Rx Status 0xd0
[17:55:01:810][  info  ][    SpecRx     ]: Number of lanes: 1
[17:55:01:810][  info  ][    SpecRx     ]: Channel 6 Lane 0 synchronized!
[17:55:01:820][  info  ][  scanConsole  ]: ... success!
[17:55:01:820][  info  ][  scanConsole  ]: Checking com 20UPGFC0012629
[17:55:01:820][  info  ][    SpecRx     ]: Active Rx channels: 0x80
[17:55:01:820][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[17:55:01:820][  info  ][    SpecRx     ]: Rx Status 0xd0
[17:55:01:820][  info  ][    SpecRx     ]: Number of lanes: 1
[17:55:01:820][  info  ][    SpecRx     ]: Channel 7 Lane 0 synchronized!
[17:55:01:830][  info  ][  scanConsole  ]: ... success!
[17:55:01:830][  info  ][  scanConsole  ]: Enabling Tx channels
[17:55:01:830][  info  ][  scanConsole  ]: Enabling Tx channel 1
[17:55:01:830][  info  ][  scanConsole  ]: Enabling Tx channel 1
[17:55:01:830][  info  ][  scanConsole  ]: Enabling Tx channel 1
[17:55:01:830][  info  ][  scanConsole  ]: Enabling Rx channels
[17:55:01:830][  info  ][  scanConsole  ]: Enabling Rx channel 4
[17:55:01:831][  info  ][  scanConsole  ]: Enabling Rx channel 6
[17:55:01:831][  info  ][  scanConsole  ]: Enabling Rx channel 7
[17:55:01:831][  info  ][  scanConsole  ]: ##############
[17:55:01:831][  info  ][  scanConsole  ]: # Setup Scan #
[17:55:01:831][  info  ][  scanConsole  ]: ##############
[17:55:01:831][  info  ][  scanConsole  ]: Found Scan config, constructing scan ...
[17:55:01:831][  info  ][  ScanFactory  ]: Loading Scan:
[17:55:01:831][  info  ][  ScanFactory  ]:   Name: AnalogScan
[17:55:01:831][  info  ][  ScanFactory  ]:   Number of Loops: 3
[17:55:01:831][  info  ][  ScanFactory  ]:   Loading Loop #0
[17:55:01:831][  info  ][  ScanFactory  ]:    Type: Rd53aReadRegLoop
[17:55:01:831][  info  ][  ScanFactory  ]:    Loading loop config ... 
[17:55:01:831][  info  ][  ScanFactory  ]: ~~~ {
[17:55:01:831][  info  ][  ScanFactory  ]: ~~~     "CurMux": [],
[17:55:01:831][  info  ][  ScanFactory  ]: ~~~     "EnblRingOsc": 0,
[17:55:01:831][  info  ][  ScanFactory  ]: ~~~     "Registers": [],
[17:55:01:831][  info  ][  ScanFactory  ]: ~~~     "RingOscDur": 9,
[17:55:01:832][  info  ][  ScanFactory  ]: ~~~     "RingOscRep": 10,
[17:55:01:832][  info  ][  ScanFactory  ]: ~~~     "VoltMux": [3, 5, 7, 15]
[17:55:01:832][  info  ][  ScanFactory  ]: ~~~ }
[17:55:01:832][  info  ][  ScanFactory  ]:   Loading Loop #1
[17:55:01:832][  info  ][  ScanFactory  ]:    Type: Rd53aTriggerLoop
[17:55:01:832][  info  ][  ScanFactory  ]:    Loading loop config ... 
[17:55:01:832][  info  ][  ScanFactory  ]: ~~~ {
[17:55:01:832][  info  ][  ScanFactory  ]: ~~~     "count": 1,
[17:55:01:832][  info  ][  ScanFactory  ]: ~~~     "delay": 48,
[17:55:01:832][  info  ][  ScanFactory  ]: ~~~     "extTrig": false,
[17:55:01:832][  info  ][  ScanFactory  ]: ~~~     "frequency": 30000.0,
[17:55:01:832][  info  ][  ScanFactory  ]: ~~~     "noInject": false,
[17:55:01:832][  info  ][  ScanFactory  ]: ~~~     "sendEcr": false,
[17:55:01:832][  info  ][  ScanFactory  ]: ~~~     "time": 0.0,
[17:55:01:832][  info  ][  ScanFactory  ]: ~~~     "trigMultiplier": 16
[17:55:01:832][  info  ][  ScanFactory  ]: ~~~ }
[17:55:01:832][  info  ][  ScanFactory  ]:   Loading Loop #2
[17:55:01:832][  info  ][  ScanFactory  ]:    Type: StdDataLoop
[17:55:01:832][warning ][  ScanFactory  ]: ~~~ Config empty.
[17:55:01:832][  info  ][ScanBuildHistogrammers]: Loading histogrammer ...
[17:55:01:832][  info  ][ScanBuildHistogrammers]: ... done!
[17:55:01:832][  info  ][ScanBuildAnalyses]: Loading analyses ...
[17:55:01:832][  info  ][  scanConsole  ]: Running pre scan!
[17:55:01:832][  info  ][  ScanFactory  ]: Entering pre scan phase ...
[17:55:01:832][  info  ][     Rd53a     ]: Write named register: GlobalPulseRt -> 16384
[17:55:01:832][  info  ][     Rd53a     ]: Write named register: InjEnDig -> 0
[17:55:01:832][  info  ][     Rd53a     ]: Write named register: InjVcalDiff -> 0
[17:55:01:833][  info  ][     Rd53a     ]: Write named register: InjVcalHigh -> 3500
[17:55:01:833][  info  ][     Rd53a     ]: Write named register: InjVcalMed -> 500
[17:55:01:833][  info  ][     Rd53a     ]: Write named register: LatencyConfig -> 48
[17:55:01:833][  info  ][  scanConsole  ]: Starting histogrammer and analysis threads:
[17:55:01:833][  info  ][  scanConsole  ]:  .. started threads of Fe 4
[17:55:01:833][  info  ][  scanConsole  ]:  .. started threads of Fe 6
[17:55:01:833][  info  ][  scanConsole  ]:  .. started threads of Fe 7
[17:55:01:833][  info  ][Rd53aDataProcessor]:   -> Processor thread #0 started!
[17:55:01:833][  info  ][Rd53aDataProcessor]:   -> Processor thread #1 started!
[17:55:01:833][  info  ][Rd53aDataProcessor]:   -> Processor thread #2 started!
[17:55:01:833][  info  ][Rd53aDataProcessor]:   -> Processor thread #3 started!
[17:55:01:833][  info  ][  scanConsole  ]: ########
[17:55:01:833][  info  ][  scanConsole  ]: # Scan #
[17:55:01:833][  info  ][  scanConsole  ]: ########
[17:55:01:833][  info  ][  scanConsole  ]: Starting scan!
[17:55:01:833][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0012613 on Rx 4
[17:55:01:837][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0012613] MON MUX_V: 3 Bias 0 1348 -> 0.289007 V Bias 1 1724 -> 0.363312 V, Temperature 0.0743056 -> -23.3088 C
[17:55:01:842][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0012613] MON MUX_V: 5 Bias 0 1299 -> 0.279323 V Bias 1 1669 -> 0.352443 V, Temperature 0.0731198 -> -26.0667 C
[17:55:01:846][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0012613] MON MUX_V: 7 Bias 0 1300 -> 0.279521 V Bias 1 1679 -> 0.354419 V, Temperature 0.0748985 -> -24.6249 C
[17:55:01:850][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0012613] MON MUX_V: 15 Bias 0 1301 -> 0.279719 V Bias 1 1683 -> 0.35521 V, Temperature 0.0754913 -> -27.0696 C
[17:55:01:885][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0012661 on Rx 6
[17:55:01:889][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0012661] MON MUX_V: 3 Bias 0 1372 -> 0.290738 V Bias 1 1752 -> 0.365832 V, Temperature 0.0750941 -> -21.6841 C
[17:55:01:893][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0012661] MON MUX_V: 5 Bias 0 1331 -> 0.282636 V Bias 1 1721 -> 0.359706 V, Temperature 0.0770703 -> -30.1858 C
[17:55:01:898][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0012661] MON MUX_V: 7 Bias 0 1311 -> 0.278683 V Bias 1 1704 -> 0.356346 V, Temperature 0.0776631 -> -31.0096 C
[17:55:01:902][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0012661] MON MUX_V: 15 Bias 0 1303 -> 0.277102 V Bias 1 1696 -> 0.354765 V, Temperature 0.0776632 -> -32.657 C
[17:55:01:937][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0012629 on Rx 7
[17:55:01:941][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0012629] MON MUX_V: 3 Bias 0 1310 -> 0.28286 V Bias 1 1680 -> 0.35612 V, Temperature 0.07326 -> -19.6081 C
[17:55:01:946][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0012629] MON MUX_V: 5 Bias 0 1280 -> 0.27692 V Bias 1 1655 -> 0.35117 V, Temperature 0.07425 -> -23.3087 C
[17:55:01:950][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0012629] MON MUX_V: 7 Bias 0 1269 -> 0.274742 V Bias 1 1644 -> 0.348992 V, Temperature 0.07425 -> -23.1834 C
[17:55:01:954][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0012629] MON MUX_V: 15 Bias 0 1247 -> 0.270386 V Bias 1 1620 -> 0.34424 V, Temperature 0.073854 -> -23.068 C
[17:55:01:990][  info  ][  scanConsole  ]: Scan done!
[17:55:01:990][  info  ][  scanConsole  ]: Waiting for processors to finish ...
[17:55:01:991][  info  ][  scanConsole  ]: Processor done, waiting for histogrammer ...
[17:55:02:391][  info  ][HistogramAlgorithm]: Histogrammer done!
[17:55:02:391][  info  ][HistogramAlgorithm]: Histogrammer done!
[17:55:02:391][  info  ][HistogramAlgorithm]: Histogrammer done!
[17:55:02:391][  info  ][  scanConsole  ]: Processor done, waiting for analysis ...
[17:55:02:791][  info  ][AnalysisAlgorithm]: Analysis done!
[17:55:02:791][  info  ][AnalysisAlgorithm]: Analysis done!
[17:55:02:791][  info  ][AnalysisAlgorithm]: Analysis done!
[17:55:02:792][  info  ][  scanConsole  ]: All done!
[17:55:02:792][  info  ][  scanConsole  ]: ##########
[17:55:02:792][  info  ][  scanConsole  ]: # Timing #
[17:55:02:792][  info  ][  scanConsole  ]: ##########
[17:55:02:792][  info  ][  scanConsole  ]: -> Configuration: 101 ms
[17:55:02:792][  info  ][  scanConsole  ]: -> Scan:          156 ms
[17:55:02:792][  info  ][  scanConsole  ]: -> Processing:    0 ms
[17:55:02:792][  info  ][  scanConsole  ]: -> Analysis:      800 ms
[17:55:02:792][  info  ][  scanConsole  ]: ###########
[17:55:02:792][  info  ][  scanConsole  ]: # Cleanup #
[17:55:02:792][  info  ][  scanConsole  ]: ###########
[17:55:02:795][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0012613 to configs/CERNQ7/20UPGFC0012613.json
[17:55:02:960][  info  ][  scanConsole  ]: -> Storing output results of FE 4
[17:55:02:960][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0012613, this usually means that the chip did not send any data at all.
[17:55:02:961][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0012661 to configs/CERNQ7/20UPGFC0012661.json
[17:55:03:127][  info  ][  scanConsole  ]: -> Storing output results of FE 6
[17:55:03:127][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0012661, this usually means that the chip did not send any data at all.
[17:55:03:128][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0012629 to configs/CERNQ7/20UPGFC0012629.json
[17:55:03:291][  info  ][  scanConsole  ]: -> Storing output results of FE 7
[17:55:03:291][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0012629, this usually means that the chip did not send any data at all.
[17:55:03:292][  info  ][  scanConsole  ]: Finishing run: 1747
20UPGFC0012613.json.after
20UPGFC0012613.json.before
20UPGFC0012629.json.after
20UPGFC0012629.json.before
20UPGFC0012661.json.after
20UPGFC0012661.json.before
reg_readtemp.json
scanLog.json

