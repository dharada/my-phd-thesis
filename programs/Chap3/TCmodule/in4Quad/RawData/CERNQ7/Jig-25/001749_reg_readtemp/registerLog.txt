[17:56:55:580][  info  ][               ]: #####################################
[17:56:55:580][  info  ][               ]: # Welcome to the YARR Scan Console! #
[17:56:55:580][  info  ][               ]: #####################################
[17:56:55:580][  info  ][               ]: -> Parsing command line parameters ...
[17:56:55:580][  info  ][               ]: Configuring logger ...
[17:56:55:582][  info  ][  scanConsole  ]: Scan Type/Config configs/scans/rd53a/reg_readtemp.json
[17:56:55:582][  info  ][  scanConsole  ]: Connectivity:
[17:56:55:582][  info  ][  scanConsole  ]:     configs/CERNQ7/example_rd53a_setup.json
[17:56:55:582][  info  ][  scanConsole  ]: Target ToT: -1
[17:56:55:582][  info  ][  scanConsole  ]: Target Charge: -1
[17:56:55:582][  info  ][  scanConsole  ]: Output Plots: true
[17:56:55:582][  info  ][  scanConsole  ]: Output Directory: ./data/001749_reg_readtemp/
[17:56:55:587][  info  ][  scanConsole  ]: Timestamp: 2022-08-23_17:56:55
[17:56:55:587][  info  ][  scanConsole  ]: Run Number: 1749
[17:56:55:587][  info  ][  scanConsole  ]: #################
[17:56:55:587][  info  ][  scanConsole  ]: # Init Hardware #
[17:56:55:587][  info  ][  scanConsole  ]: #################
[17:56:55:587][  info  ][  scanConsole  ]: -> Opening controller config: configs/controller/specCfg-rd53a-16x1.json
[17:56:55:587][  info  ][  ScanHelper   ]: Loading controller ...
[17:56:55:587][  info  ][  ScanHelper   ]: Found controller of type: spec
[17:56:55:587][  info  ][  ScanHelper   ]: ... loading controler config:
[17:56:55:587][  info  ][  ScanHelper   ]: ~~~ {
[17:56:55:587][  info  ][  ScanHelper   ]: ~~~     "cmdPeriod": 6.250000073038109e-9,
[17:56:55:587][  info  ][  ScanHelper   ]: ~~~     "idle": {
[17:56:55:587][  info  ][  ScanHelper   ]: ~~~         "word": 1768515945
[17:56:55:587][  info  ][  ScanHelper   ]: ~~~     },
[17:56:55:587][  info  ][  ScanHelper   ]: ~~~     "pulse": {
[17:56:55:587][  info  ][  ScanHelper   ]: ~~~         "interval": 500,
[17:56:55:587][  info  ][  ScanHelper   ]: ~~~         "word": 1549575846
[17:56:55:587][  info  ][  ScanHelper   ]: ~~~     },
[17:56:55:587][  info  ][  ScanHelper   ]: ~~~     "rxActiveLanes": 1,
[17:56:55:587][  info  ][  ScanHelper   ]: ~~~     "rxPolarity": 0,
[17:56:55:587][  info  ][  ScanHelper   ]: ~~~     "specNum": 0,
[17:56:55:587][  info  ][  ScanHelper   ]: ~~~     "spiConfig": 541200,
[17:56:55:587][  info  ][  ScanHelper   ]: ~~~     "sync": {
[17:56:55:587][  info  ][  ScanHelper   ]: ~~~         "interval": 32,
[17:56:55:587][  info  ][  ScanHelper   ]: ~~~         "word": 2172551550
[17:56:55:587][  info  ][  ScanHelper   ]: ~~~     },
[17:56:55:587][  info  ][  ScanHelper   ]: ~~~     "txPolarity": 0
[17:56:55:587][  info  ][  ScanHelper   ]: ~~~ }
[17:56:55:587][  info  ][    SpecCom    ]: Opening SPEC with id #0
[17:56:55:587][  info  ][    SpecCom    ]: Mapping BARs ...
[17:56:55:587][  info  ][    SpecCom    ]: ... Mapped BAR0 at 0x7fefa8c55000 with size 1048576
[17:56:55:587][warning ][    SpecCom    ]: ... BAR4 not mapped (Mmap failed)
[17:56:55:587][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[17:56:55:587][  info  ][    SpecCom    ]: Firmware Version: 0x4d9ff6d
[17:56:55:587][  info  ][    SpecCom    ]: Firmware Identifier: 0x4030232
[17:56:55:587][  info  ][    SpecCom    ]: FPGA card: PLDA XpressK7 325
[17:56:55:587][  info  ][    SpecCom    ]: FE Chip Type: RD53A/B
[17:56:55:587][  info  ][    SpecCom    ]: FMC Card Type: Ohio Card (Display Port)
[17:56:55:587][  info  ][    SpecCom    ]: RX Speed: 640Mbps
[17:56:55:587][  info  ][    SpecCom    ]: Channel Configuration: 16x1
[17:56:55:587][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[17:56:55:587][  info  ][    SpecCom    ]: Flushing buffers ...
[17:56:55:587][  info  ][    SpecCom    ]: Init success!
[17:56:55:587][  info  ][  scanConsole  ]: #######################
[17:56:55:587][  info  ][  scanConsole  ]: ##  Loading Configs  ##
[17:56:55:587][  info  ][  scanConsole  ]: #######################
[17:56:55:587][  info  ][  scanConsole  ]: Opening global config: configs/CERNQ7/example_rd53a_setup.json
[17:56:55:587][  info  ][  ScanHelper   ]: Chip type: RD53A
[17:56:55:587][  info  ][  ScanHelper   ]: Chip count 4
[17:56:55:587][  info  ][  ScanHelper   ]: Loading chip #0
[17:56:55:588][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(4)
[17:56:55:588][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ7/20UPGFC0012613.json
[17:56:55:763][  info  ][  ScanHelper   ]: Loading chip #1
[17:56:55:763][warning ][  ScanHelper   ]:  ... chip not enabled, skipping!
[17:56:55:763][  info  ][  ScanHelper   ]: Loading chip #2
[17:56:55:763][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(6)
[17:56:55:763][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ7/20UPGFC0012661.json
[17:56:55:936][  info  ][  ScanHelper   ]: Loading chip #3
[17:56:55:936][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(7)
[17:56:55:936][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ7/20UPGFC0012629.json
[17:56:56:111][  info  ][  scanConsole  ]: #################
[17:56:56:111][  info  ][  scanConsole  ]: # Configure FEs #
[17:56:56:111][  info  ][  scanConsole  ]: #################
[17:56:56:111][  info  ][  scanConsole  ]: Configuring 20UPGFC0012613
[17:56:56:142][  info  ][  scanConsole  ]: Configuring 20UPGFC0012661
[17:56:56:184][  info  ][  scanConsole  ]: Configuring 20UPGFC0012629
[17:56:56:214][  info  ][  scanConsole  ]: Sent configuration to all FEs in 102 ms!
[17:56:56:215][  info  ][  scanConsole  ]: Checking com 20UPGFC0012613
[17:56:56:215][  info  ][    SpecRx     ]: Active Rx channels: 0x10
[17:56:56:215][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[17:56:56:215][  info  ][    SpecRx     ]: Rx Status 0xd0
[17:56:56:215][  info  ][    SpecRx     ]: Number of lanes: 1
[17:56:56:215][  info  ][    SpecRx     ]: Channel 4 Lane 0 synchronized!
[17:56:56:225][  info  ][  scanConsole  ]: ... success!
[17:56:56:225][  info  ][  scanConsole  ]: Checking com 20UPGFC0012661
[17:56:56:225][  info  ][    SpecRx     ]: Active Rx channels: 0x40
[17:56:56:225][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[17:56:56:225][  info  ][    SpecRx     ]: Rx Status 0xd0
[17:56:56:225][  info  ][    SpecRx     ]: Number of lanes: 1
[17:56:56:225][  info  ][    SpecRx     ]: Channel 6 Lane 0 synchronized!
[17:56:56:236][  info  ][  scanConsole  ]: ... success!
[17:56:56:236][  info  ][  scanConsole  ]: Checking com 20UPGFC0012629
[17:56:56:236][  info  ][    SpecRx     ]: Active Rx channels: 0x80
[17:56:56:236][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[17:56:56:236][  info  ][    SpecRx     ]: Rx Status 0xd0
[17:56:56:236][  info  ][    SpecRx     ]: Number of lanes: 1
[17:56:56:236][  info  ][    SpecRx     ]: Channel 7 Lane 0 synchronized!
[17:56:56:246][  info  ][  scanConsole  ]: ... success!
[17:56:56:246][  info  ][  scanConsole  ]: Enabling Tx channels
[17:56:56:246][  info  ][  scanConsole  ]: Enabling Tx channel 1
[17:56:56:246][  info  ][  scanConsole  ]: Enabling Tx channel 1
[17:56:56:246][  info  ][  scanConsole  ]: Enabling Tx channel 1
[17:56:56:246][  info  ][  scanConsole  ]: Enabling Rx channels
[17:56:56:246][  info  ][  scanConsole  ]: Enabling Rx channel 4
[17:56:56:246][  info  ][  scanConsole  ]: Enabling Rx channel 6
[17:56:56:246][  info  ][  scanConsole  ]: Enabling Rx channel 7
[17:56:56:246][  info  ][  scanConsole  ]: ##############
[17:56:56:246][  info  ][  scanConsole  ]: # Setup Scan #
[17:56:56:246][  info  ][  scanConsole  ]: ##############
[17:56:56:246][  info  ][  scanConsole  ]: Found Scan config, constructing scan ...
[17:56:56:247][  info  ][  ScanFactory  ]: Loading Scan:
[17:56:56:247][  info  ][  ScanFactory  ]:   Name: AnalogScan
[17:56:56:247][  info  ][  ScanFactory  ]:   Number of Loops: 3
[17:56:56:247][  info  ][  ScanFactory  ]:   Loading Loop #0
[17:56:56:247][  info  ][  ScanFactory  ]:    Type: Rd53aReadRegLoop
[17:56:56:247][  info  ][  ScanFactory  ]:    Loading loop config ... 
[17:56:56:247][  info  ][  ScanFactory  ]: ~~~ {
[17:56:56:247][  info  ][  ScanFactory  ]: ~~~     "CurMux": [],
[17:56:56:247][  info  ][  ScanFactory  ]: ~~~     "EnblRingOsc": 0,
[17:56:56:247][  info  ][  ScanFactory  ]: ~~~     "Registers": [],
[17:56:56:247][  info  ][  ScanFactory  ]: ~~~     "RingOscDur": 9,
[17:56:56:247][  info  ][  ScanFactory  ]: ~~~     "RingOscRep": 10,
[17:56:56:247][  info  ][  ScanFactory  ]: ~~~     "VoltMux": [3, 5, 7, 15]
[17:56:56:247][  info  ][  ScanFactory  ]: ~~~ }
[17:56:56:247][  info  ][  ScanFactory  ]:   Loading Loop #1
[17:56:56:247][  info  ][  ScanFactory  ]:    Type: Rd53aTriggerLoop
[17:56:56:247][  info  ][  ScanFactory  ]:    Loading loop config ... 
[17:56:56:247][  info  ][  ScanFactory  ]: ~~~ {
[17:56:56:247][  info  ][  ScanFactory  ]: ~~~     "count": 1,
[17:56:56:247][  info  ][  ScanFactory  ]: ~~~     "delay": 48,
[17:56:56:247][  info  ][  ScanFactory  ]: ~~~     "extTrig": false,
[17:56:56:247][  info  ][  ScanFactory  ]: ~~~     "frequency": 30000.0,
[17:56:56:247][  info  ][  ScanFactory  ]: ~~~     "noInject": false,
[17:56:56:247][  info  ][  ScanFactory  ]: ~~~     "sendEcr": false,
[17:56:56:247][  info  ][  ScanFactory  ]: ~~~     "time": 0.0,
[17:56:56:247][  info  ][  ScanFactory  ]: ~~~     "trigMultiplier": 16
[17:56:56:247][  info  ][  ScanFactory  ]: ~~~ }
[17:56:56:247][  info  ][  ScanFactory  ]:   Loading Loop #2
[17:56:56:247][  info  ][  ScanFactory  ]:    Type: StdDataLoop
[17:56:56:247][warning ][  ScanFactory  ]: ~~~ Config empty.
[17:56:56:247][  info  ][ScanBuildHistogrammers]: Loading histogrammer ...
[17:56:56:247][  info  ][ScanBuildHistogrammers]: ... done!
[17:56:56:247][  info  ][ScanBuildAnalyses]: Loading analyses ...
[17:56:56:248][  info  ][  scanConsole  ]: Running pre scan!
[17:56:56:248][  info  ][  ScanFactory  ]: Entering pre scan phase ...
[17:56:56:248][  info  ][     Rd53a     ]: Write named register: GlobalPulseRt -> 16384
[17:56:56:248][  info  ][     Rd53a     ]: Write named register: InjEnDig -> 0
[17:56:56:248][  info  ][     Rd53a     ]: Write named register: InjVcalDiff -> 0
[17:56:56:248][  info  ][     Rd53a     ]: Write named register: InjVcalHigh -> 3500
[17:56:56:248][  info  ][     Rd53a     ]: Write named register: InjVcalMed -> 500
[17:56:56:248][  info  ][     Rd53a     ]: Write named register: LatencyConfig -> 48
[17:56:56:248][  info  ][  scanConsole  ]: Starting histogrammer and analysis threads:
[17:56:56:248][  info  ][  scanConsole  ]:  .. started threads of Fe 4
[17:56:56:248][  info  ][  scanConsole  ]:  .. started threads of Fe 6
[17:56:56:248][  info  ][  scanConsole  ]:  .. started threads of Fe 7
[17:56:56:248][  info  ][Rd53aDataProcessor]:   -> Processor thread #0 started!
[17:56:56:248][  info  ][Rd53aDataProcessor]:   -> Processor thread #1 started!
[17:56:56:248][  info  ][Rd53aDataProcessor]:   -> Processor thread #2 started!
[17:56:56:248][  info  ][Rd53aDataProcessor]:   -> Processor thread #3 started!
[17:56:56:248][  info  ][  scanConsole  ]: ########
[17:56:56:248][  info  ][  scanConsole  ]: # Scan #
[17:56:56:248][  info  ][  scanConsole  ]: ########
[17:56:56:248][  info  ][  scanConsole  ]: Starting scan!
[17:56:56:249][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0012613 on Rx 4
[17:56:56:253][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0012613] MON MUX_V: 3 Bias 0 1350 -> 0.289402 V Bias 1 1723 -> 0.363115 V, Temperature 0.0737127 -> -25.5652 C
[17:56:56:257][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0012613] MON MUX_V: 5 Bias 0 1297 -> 0.278928 V Bias 1 1669 -> 0.352443 V, Temperature 0.0735151 -> -24.625 C
[17:56:56:261][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0012613] MON MUX_V: 7 Bias 0 1300 -> 0.279521 V Bias 1 1679 -> 0.354419 V, Temperature 0.0748985 -> -24.6249 C
[17:56:56:266][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0012613] MON MUX_V: 15 Bias 0 1301 -> 0.279719 V Bias 1 1684 -> 0.355407 V, Temperature 0.0756889 -> -26.3174 C
[17:56:56:300][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0012661 on Rx 6
[17:56:56:305][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0012661] MON MUX_V: 3 Bias 0 1373 -> 0.290935 V Bias 1 1753 -> 0.36603 V, Temperature 0.0750941 -> -21.6841 C
[17:56:56:309][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0012661] MON MUX_V: 5 Bias 0 1331 -> 0.282636 V Bias 1 1721 -> 0.359706 V, Temperature 0.0770703 -> -30.1858 C
[17:56:56:313][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0012661] MON MUX_V: 7 Bias 0 1311 -> 0.278683 V Bias 1 1705 -> 0.356544 V, Temperature 0.0778608 -> -30.1856 C
[17:56:56:317][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0012661] MON MUX_V: 15 Bias 0 1304 -> 0.2773 V Bias 1 1698 -> 0.355161 V, Temperature 0.0778607 -> -31.8335 C
[17:56:56:351][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0012629 on Rx 7
[17:56:56:355][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0012629] MON MUX_V: 3 Bias 0 1311 -> 0.283058 V Bias 1 1680 -> 0.35612 V, Temperature 0.073062 -> -20.3001 C
[17:56:56:360][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0012629] MON MUX_V: 5 Bias 0 1279 -> 0.276722 V Bias 1 1655 -> 0.35117 V, Temperature 0.074448 -> -22.5565 C
[17:56:56:364][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0012629] MON MUX_V: 7 Bias 0 1270 -> 0.27494 V Bias 1 1644 -> 0.348992 V, Temperature 0.074052 -> -23.9042 C
[17:56:56:368][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0012629] MON MUX_V: 15 Bias 0 1246 -> 0.270188 V Bias 1 1619 -> 0.344042 V, Temperature 0.0738541 -> -23.0678 C
[17:56:56:405][  info  ][  scanConsole  ]: Scan done!
[17:56:56:405][  info  ][  scanConsole  ]: Waiting for processors to finish ...
[17:56:56:406][  info  ][  scanConsole  ]: Processor done, waiting for histogrammer ...
[17:56:56:805][  info  ][HistogramAlgorithm]: Histogrammer done!
[17:56:56:805][  info  ][HistogramAlgorithm]: Histogrammer done!
[17:56:56:805][  info  ][HistogramAlgorithm]: Histogrammer done!
[17:56:56:806][  info  ][  scanConsole  ]: Processor done, waiting for analysis ...
[17:56:57:206][  info  ][AnalysisAlgorithm]: Analysis done!
[17:56:57:206][  info  ][AnalysisAlgorithm]: Analysis done!
[17:56:57:206][  info  ][AnalysisAlgorithm]: Analysis done!
[17:56:57:206][  info  ][  scanConsole  ]: All done!
[17:56:57:206][  info  ][  scanConsole  ]: ##########
[17:56:57:206][  info  ][  scanConsole  ]: # Timing #
[17:56:57:206][  info  ][  scanConsole  ]: ##########
[17:56:57:206][  info  ][  scanConsole  ]: -> Configuration: 102 ms
[17:56:57:206][  info  ][  scanConsole  ]: -> Scan:          156 ms
[17:56:57:206][  info  ][  scanConsole  ]: -> Processing:    0 ms
[17:56:57:206][  info  ][  scanConsole  ]: -> Analysis:      800 ms
[17:56:57:206][  info  ][  scanConsole  ]: ###########
[17:56:57:206][  info  ][  scanConsole  ]: # Cleanup #
[17:56:57:206][  info  ][  scanConsole  ]: ###########
[17:56:57:215][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0012613 to configs/CERNQ7/20UPGFC0012613.json
[17:56:57:415][  info  ][  scanConsole  ]: -> Storing output results of FE 4
[17:56:57:415][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0012613, this usually means that the chip did not send any data at all.
[17:56:57:417][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0012661 to configs/CERNQ7/20UPGFC0012661.json
[17:56:57:579][  info  ][  scanConsole  ]: -> Storing output results of FE 6
[17:56:57:580][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0012661, this usually means that the chip did not send any data at all.
[17:56:57:581][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0012629 to configs/CERNQ7/20UPGFC0012629.json
[17:56:57:747][  info  ][  scanConsole  ]: -> Storing output results of FE 7
[17:56:57:747][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0012629, this usually means that the chip did not send any data at all.
[17:56:57:748][  info  ][  scanConsole  ]: Finishing run: 1749
20UPGFC0012613.json.after
20UPGFC0012613.json.before
20UPGFC0012629.json.after
20UPGFC0012629.json.before
20UPGFC0012661.json.after
20UPGFC0012661.json.before
reg_readtemp.json
scanLog.json

