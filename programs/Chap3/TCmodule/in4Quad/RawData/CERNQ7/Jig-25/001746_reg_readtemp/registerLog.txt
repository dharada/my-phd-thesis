[17:54:21:851][  info  ][               ]: #####################################
[17:54:21:851][  info  ][               ]: # Welcome to the YARR Scan Console! #
[17:54:21:851][  info  ][               ]: #####################################
[17:54:21:851][  info  ][               ]: -> Parsing command line parameters ...
[17:54:21:851][  info  ][               ]: Configuring logger ...
[17:54:21:853][  info  ][  scanConsole  ]: Scan Type/Config configs/scans/rd53a/reg_readtemp.json
[17:54:21:853][  info  ][  scanConsole  ]: Connectivity:
[17:54:21:853][  info  ][  scanConsole  ]:     configs/CERNQ7/example_rd53a_setup.json
[17:54:21:853][  info  ][  scanConsole  ]: Target ToT: -1
[17:54:21:853][  info  ][  scanConsole  ]: Target Charge: -1
[17:54:21:853][  info  ][  scanConsole  ]: Output Plots: true
[17:54:21:853][  info  ][  scanConsole  ]: Output Directory: ./data/001746_reg_readtemp/
[17:54:21:857][  info  ][  scanConsole  ]: Timestamp: 2022-08-23_17:54:21
[17:54:21:857][  info  ][  scanConsole  ]: Run Number: 1746
[17:54:21:857][  info  ][  scanConsole  ]: #################
[17:54:21:857][  info  ][  scanConsole  ]: # Init Hardware #
[17:54:21:857][  info  ][  scanConsole  ]: #################
[17:54:21:857][  info  ][  scanConsole  ]: -> Opening controller config: configs/controller/specCfg-rd53a-16x1.json
[17:54:21:858][  info  ][  ScanHelper   ]: Loading controller ...
[17:54:21:858][  info  ][  ScanHelper   ]: Found controller of type: spec
[17:54:21:858][  info  ][  ScanHelper   ]: ... loading controler config:
[17:54:21:858][  info  ][  ScanHelper   ]: ~~~ {
[17:54:21:858][  info  ][  ScanHelper   ]: ~~~     "cmdPeriod": 6.250000073038109e-9,
[17:54:21:858][  info  ][  ScanHelper   ]: ~~~     "idle": {
[17:54:21:858][  info  ][  ScanHelper   ]: ~~~         "word": 1768515945
[17:54:21:858][  info  ][  ScanHelper   ]: ~~~     },
[17:54:21:858][  info  ][  ScanHelper   ]: ~~~     "pulse": {
[17:54:21:858][  info  ][  ScanHelper   ]: ~~~         "interval": 500,
[17:54:21:858][  info  ][  ScanHelper   ]: ~~~         "word": 1549575846
[17:54:21:858][  info  ][  ScanHelper   ]: ~~~     },
[17:54:21:858][  info  ][  ScanHelper   ]: ~~~     "rxActiveLanes": 1,
[17:54:21:858][  info  ][  ScanHelper   ]: ~~~     "rxPolarity": 0,
[17:54:21:858][  info  ][  ScanHelper   ]: ~~~     "specNum": 0,
[17:54:21:858][  info  ][  ScanHelper   ]: ~~~     "spiConfig": 541200,
[17:54:21:858][  info  ][  ScanHelper   ]: ~~~     "sync": {
[17:54:21:858][  info  ][  ScanHelper   ]: ~~~         "interval": 32,
[17:54:21:858][  info  ][  ScanHelper   ]: ~~~         "word": 2172551550
[17:54:21:858][  info  ][  ScanHelper   ]: ~~~     },
[17:54:21:858][  info  ][  ScanHelper   ]: ~~~     "txPolarity": 0
[17:54:21:858][  info  ][  ScanHelper   ]: ~~~ }
[17:54:21:858][  info  ][    SpecCom    ]: Opening SPEC with id #0
[17:54:21:858][  info  ][    SpecCom    ]: Mapping BARs ...
[17:54:21:858][  info  ][    SpecCom    ]: ... Mapped BAR0 at 0x7fcbab052000 with size 1048576
[17:54:21:858][warning ][    SpecCom    ]: ... BAR4 not mapped (Mmap failed)
[17:54:21:858][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[17:54:21:858][  info  ][    SpecCom    ]: Firmware Version: 0x4d9ff6d
[17:54:21:858][  info  ][    SpecCom    ]: Firmware Identifier: 0x4030232
[17:54:21:858][  info  ][    SpecCom    ]: FPGA card: PLDA XpressK7 325
[17:54:21:858][  info  ][    SpecCom    ]: FE Chip Type: RD53A/B
[17:54:21:858][  info  ][    SpecCom    ]: FMC Card Type: Ohio Card (Display Port)
[17:54:21:858][  info  ][    SpecCom    ]: RX Speed: 640Mbps
[17:54:21:858][  info  ][    SpecCom    ]: Channel Configuration: 16x1
[17:54:21:858][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[17:54:21:858][  info  ][    SpecCom    ]: Flushing buffers ...
[17:54:21:858][  info  ][    SpecCom    ]: Init success!
[17:54:21:858][  info  ][  scanConsole  ]: #######################
[17:54:21:858][  info  ][  scanConsole  ]: ##  Loading Configs  ##
[17:54:21:858][  info  ][  scanConsole  ]: #######################
[17:54:21:858][  info  ][  scanConsole  ]: Opening global config: configs/CERNQ7/example_rd53a_setup.json
[17:54:21:858][  info  ][  ScanHelper   ]: Chip type: RD53A
[17:54:21:858][  info  ][  ScanHelper   ]: Chip count 4
[17:54:21:858][  info  ][  ScanHelper   ]: Loading chip #0
[17:54:21:859][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(4)
[17:54:21:859][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ7/20UPGFC0012613.json
[17:54:22:036][  info  ][  ScanHelper   ]: Loading chip #1
[17:54:22:036][warning ][  ScanHelper   ]:  ... chip not enabled, skipping!
[17:54:22:036][  info  ][  ScanHelper   ]: Loading chip #2
[17:54:22:037][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(6)
[17:54:22:037][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ7/20UPGFC0012661.json
[17:54:22:212][  info  ][  ScanHelper   ]: Loading chip #3
[17:54:22:212][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(7)
[17:54:22:212][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ7/20UPGFC0012629.json
[17:54:22:391][  info  ][  scanConsole  ]: #################
[17:54:22:391][  info  ][  scanConsole  ]: # Configure FEs #
[17:54:22:391][  info  ][  scanConsole  ]: #################
[17:54:22:391][  info  ][  scanConsole  ]: Configuring 20UPGFC0012613
[17:54:22:423][  info  ][  scanConsole  ]: Configuring 20UPGFC0012661
[17:54:22:453][  info  ][  scanConsole  ]: Configuring 20UPGFC0012629
[17:54:22:492][  info  ][  scanConsole  ]: Sent configuration to all FEs in 100 ms!
[17:54:22:493][  info  ][  scanConsole  ]: Checking com 20UPGFC0012613
[17:54:22:493][  info  ][    SpecRx     ]: Active Rx channels: 0x10
[17:54:22:493][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[17:54:22:493][  info  ][    SpecRx     ]: Rx Status 0xd0
[17:54:22:493][  info  ][    SpecRx     ]: Number of lanes: 1
[17:54:22:493][  info  ][    SpecRx     ]: Channel 4 Lane 0 synchronized!
[17:54:22:504][  info  ][  scanConsole  ]: ... success!
[17:54:22:504][  info  ][  scanConsole  ]: Checking com 20UPGFC0012661
[17:54:22:504][  info  ][    SpecRx     ]: Active Rx channels: 0x40
[17:54:22:504][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[17:54:22:504][  info  ][    SpecRx     ]: Rx Status 0xd0
[17:54:22:504][  info  ][    SpecRx     ]: Number of lanes: 1
[17:54:22:504][  info  ][    SpecRx     ]: Channel 6 Lane 0 synchronized!
[17:54:22:514][  info  ][  scanConsole  ]: ... success!
[17:54:22:514][  info  ][  scanConsole  ]: Checking com 20UPGFC0012629
[17:54:22:514][  info  ][    SpecRx     ]: Active Rx channels: 0x80
[17:54:22:515][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[17:54:22:515][  info  ][    SpecRx     ]: Rx Status 0xd0
[17:54:22:515][  info  ][    SpecRx     ]: Number of lanes: 1
[17:54:22:515][  info  ][    SpecRx     ]: Channel 7 Lane 0 synchronized!
[17:54:22:525][  info  ][  scanConsole  ]: ... success!
[17:54:22:525][  info  ][  scanConsole  ]: Enabling Tx channels
[17:54:22:525][  info  ][  scanConsole  ]: Enabling Tx channel 1
[17:54:22:525][  info  ][  scanConsole  ]: Enabling Tx channel 1
[17:54:22:525][  info  ][  scanConsole  ]: Enabling Tx channel 1
[17:54:22:525][  info  ][  scanConsole  ]: Enabling Rx channels
[17:54:22:525][  info  ][  scanConsole  ]: Enabling Rx channel 4
[17:54:22:525][  info  ][  scanConsole  ]: Enabling Rx channel 6
[17:54:22:525][  info  ][  scanConsole  ]: Enabling Rx channel 7
[17:54:22:525][  info  ][  scanConsole  ]: ##############
[17:54:22:525][  info  ][  scanConsole  ]: # Setup Scan #
[17:54:22:525][  info  ][  scanConsole  ]: ##############
[17:54:22:525][  info  ][  scanConsole  ]: Found Scan config, constructing scan ...
[17:54:22:526][  info  ][  ScanFactory  ]: Loading Scan:
[17:54:22:526][  info  ][  ScanFactory  ]:   Name: AnalogScan
[17:54:22:526][  info  ][  ScanFactory  ]:   Number of Loops: 3
[17:54:22:526][  info  ][  ScanFactory  ]:   Loading Loop #0
[17:54:22:526][  info  ][  ScanFactory  ]:    Type: Rd53aReadRegLoop
[17:54:22:526][  info  ][  ScanFactory  ]:    Loading loop config ... 
[17:54:22:526][  info  ][  ScanFactory  ]: ~~~ {
[17:54:22:526][  info  ][  ScanFactory  ]: ~~~     "CurMux": [],
[17:54:22:526][  info  ][  ScanFactory  ]: ~~~     "EnblRingOsc": 0,
[17:54:22:526][  info  ][  ScanFactory  ]: ~~~     "Registers": [],
[17:54:22:526][  info  ][  ScanFactory  ]: ~~~     "RingOscDur": 9,
[17:54:22:526][  info  ][  ScanFactory  ]: ~~~     "RingOscRep": 10,
[17:54:22:526][  info  ][  ScanFactory  ]: ~~~     "VoltMux": [3, 5, 7, 15]
[17:54:22:526][  info  ][  ScanFactory  ]: ~~~ }
[17:54:22:526][  info  ][  ScanFactory  ]:   Loading Loop #1
[17:54:22:526][  info  ][  ScanFactory  ]:    Type: Rd53aTriggerLoop
[17:54:22:526][  info  ][  ScanFactory  ]:    Loading loop config ... 
[17:54:22:526][  info  ][  ScanFactory  ]: ~~~ {
[17:54:22:526][  info  ][  ScanFactory  ]: ~~~     "count": 1,
[17:54:22:526][  info  ][  ScanFactory  ]: ~~~     "delay": 48,
[17:54:22:526][  info  ][  ScanFactory  ]: ~~~     "extTrig": false,
[17:54:22:526][  info  ][  ScanFactory  ]: ~~~     "frequency": 30000.0,
[17:54:22:526][  info  ][  ScanFactory  ]: ~~~     "noInject": false,
[17:54:22:526][  info  ][  ScanFactory  ]: ~~~     "sendEcr": false,
[17:54:22:526][  info  ][  ScanFactory  ]: ~~~     "time": 0.0,
[17:54:22:526][  info  ][  ScanFactory  ]: ~~~     "trigMultiplier": 16
[17:54:22:526][  info  ][  ScanFactory  ]: ~~~ }
[17:54:22:526][  info  ][  ScanFactory  ]:   Loading Loop #2
[17:54:22:526][  info  ][  ScanFactory  ]:    Type: StdDataLoop
[17:54:22:526][warning ][  ScanFactory  ]: ~~~ Config empty.
[17:54:22:526][  info  ][ScanBuildHistogrammers]: Loading histogrammer ...
[17:54:22:527][  info  ][ScanBuildHistogrammers]: ... done!
[17:54:22:527][  info  ][ScanBuildAnalyses]: Loading analyses ...
[17:54:22:527][  info  ][  scanConsole  ]: Running pre scan!
[17:54:22:527][  info  ][  ScanFactory  ]: Entering pre scan phase ...
[17:54:22:527][  info  ][     Rd53a     ]: Write named register: GlobalPulseRt -> 16384
[17:54:22:527][  info  ][     Rd53a     ]: Write named register: InjEnDig -> 0
[17:54:22:527][  info  ][     Rd53a     ]: Write named register: InjVcalDiff -> 0
[17:54:22:527][  info  ][     Rd53a     ]: Write named register: InjVcalHigh -> 3500
[17:54:22:527][  info  ][     Rd53a     ]: Write named register: InjVcalMed -> 500
[17:54:22:527][  info  ][     Rd53a     ]: Write named register: LatencyConfig -> 48
[17:54:22:527][  info  ][  scanConsole  ]: Starting histogrammer and analysis threads:
[17:54:22:527][  info  ][  scanConsole  ]:  .. started threads of Fe 4
[17:54:22:528][  info  ][  scanConsole  ]:  .. started threads of Fe 6
[17:54:22:528][  info  ][  scanConsole  ]:  .. started threads of Fe 7
[17:54:22:528][  info  ][Rd53aDataProcessor]:   -> Processor thread #0 started!
[17:54:22:528][  info  ][Rd53aDataProcessor]:   -> Processor thread #1 started!
[17:54:22:528][  info  ][Rd53aDataProcessor]:   -> Processor thread #2 started!
[17:54:22:528][  info  ][Rd53aDataProcessor]:   -> Processor thread #3 started!
[17:54:22:528][  info  ][  scanConsole  ]: ########
[17:54:22:528][  info  ][  scanConsole  ]: # Scan #
[17:54:22:528][  info  ][  scanConsole  ]: ########
[17:54:22:528][  info  ][  scanConsole  ]: Starting scan!
[17:54:22:528][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0012613 on Rx 4
[17:54:22:532][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0012613] MON MUX_V: 3 Bias 0 1349 -> 0.289204 V Bias 1 1724 -> 0.363312 V, Temperature 0.0741079 -> -24.061 C
[17:54:22:537][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0012613] MON MUX_V: 5 Bias 0 1298 -> 0.279126 V Bias 1 1670 -> 0.352641 V, Temperature 0.0735151 -> -24.625 C
[17:54:22:541][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0012613] MON MUX_V: 7 Bias 0 1300 -> 0.279521 V Bias 1 1680 -> 0.354617 V, Temperature 0.0750961 -> -23.9041 C
[17:54:22:545][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0012613] MON MUX_V: 15 Bias 0 1302 -> 0.279916 V Bias 1 1683 -> 0.35521 V, Temperature 0.0752937 -> -27.8218 C
[17:54:22:580][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0012661 on Rx 6
[17:54:22:584][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0012661] MON MUX_V: 3 Bias 0 1373 -> 0.290935 V Bias 1 1753 -> 0.36603 V, Temperature 0.0750941 -> -21.6841 C
[17:54:22:588][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0012661] MON MUX_V: 5 Bias 0 1332 -> 0.282833 V Bias 1 1721 -> 0.359706 V, Temperature 0.0768727 -> -31.0095 C
[17:54:22:593][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0012661] MON MUX_V: 7 Bias 0 1311 -> 0.278683 V Bias 1 1704 -> 0.356346 V, Temperature 0.0776631 -> -31.0096 C
[17:54:22:597][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0012661] MON MUX_V: 15 Bias 0 1304 -> 0.2773 V Bias 1 1698 -> 0.355161 V, Temperature 0.0778607 -> -31.8335 C
[17:54:22:632][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0012629 on Rx 7
[17:54:22:636][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0012629] MON MUX_V: 3 Bias 0 1310 -> 0.28286 V Bias 1 1681 -> 0.356318 V, Temperature 0.073458 -> -18.916 C
[17:54:22:640][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0012629] MON MUX_V: 5 Bias 0 1280 -> 0.27692 V Bias 1 1655 -> 0.35117 V, Temperature 0.07425 -> -23.3087 C
[17:54:22:645][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0012629] MON MUX_V: 7 Bias 0 1270 -> 0.27494 V Bias 1 1644 -> 0.348992 V, Temperature 0.074052 -> -23.9042 C
[17:54:22:649][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0012629] MON MUX_V: 15 Bias 0 1247 -> 0.270386 V Bias 1 1620 -> 0.34424 V, Temperature 0.073854 -> -23.068 C
[17:54:22:685][  info  ][  scanConsole  ]: Scan done!
[17:54:22:685][  info  ][  scanConsole  ]: Waiting for processors to finish ...
[17:54:22:686][  info  ][  scanConsole  ]: Processor done, waiting for histogrammer ...
[17:54:23:086][  info  ][HistogramAlgorithm]: Histogrammer done!
[17:54:23:086][  info  ][HistogramAlgorithm]: Histogrammer done!
[17:54:23:086][  info  ][HistogramAlgorithm]: Histogrammer done!
[17:54:23:086][  info  ][  scanConsole  ]: Processor done, waiting for analysis ...
[17:54:23:486][  info  ][AnalysisAlgorithm]: Analysis done!
[17:54:23:486][  info  ][AnalysisAlgorithm]: Analysis done!
[17:54:23:486][  info  ][AnalysisAlgorithm]: Analysis done!
[17:54:23:486][  info  ][  scanConsole  ]: All done!
[17:54:23:486][  info  ][  scanConsole  ]: ##########
[17:54:23:486][  info  ][  scanConsole  ]: # Timing #
[17:54:23:486][  info  ][  scanConsole  ]: ##########
[17:54:23:486][  info  ][  scanConsole  ]: -> Configuration: 100 ms
[17:54:23:486][  info  ][  scanConsole  ]: -> Scan:          156 ms
[17:54:23:486][  info  ][  scanConsole  ]: -> Processing:    0 ms
[17:54:23:486][  info  ][  scanConsole  ]: -> Analysis:      800 ms
[17:54:23:487][  info  ][  scanConsole  ]: ###########
[17:54:23:487][  info  ][  scanConsole  ]: # Cleanup #
[17:54:23:487][  info  ][  scanConsole  ]: ###########
[17:54:23:495][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0012613 to configs/CERNQ7/20UPGFC0012613.json
[17:54:23:704][  info  ][  scanConsole  ]: -> Storing output results of FE 4
[17:54:23:704][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0012613, this usually means that the chip did not send any data at all.
[17:54:23:705][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0012661 to configs/CERNQ7/20UPGFC0012661.json
[17:54:23:872][  info  ][  scanConsole  ]: -> Storing output results of FE 6
[17:54:23:872][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0012661, this usually means that the chip did not send any data at all.
[17:54:23:874][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0012629 to configs/CERNQ7/20UPGFC0012629.json
[17:54:24:040][  info  ][  scanConsole  ]: -> Storing output results of FE 7
[17:54:24:040][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0012629, this usually means that the chip did not send any data at all.
[17:54:24:041][  info  ][  scanConsole  ]: Finishing run: 1746
20UPGFC0012613.json.after
20UPGFC0012613.json.before
20UPGFC0012629.json.after
20UPGFC0012629.json.before
20UPGFC0012661.json.after
20UPGFC0012661.json.before
reg_readtemp.json
scanLog.json

