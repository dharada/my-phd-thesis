[16:43:07:151][  info  ][               ]: #####################################
[16:43:07:151][  info  ][               ]: # Welcome to the YARR Scan Console! #
[16:43:07:151][  info  ][               ]: #####################################
[16:43:07:151][  info  ][               ]: -> Parsing command line parameters ...
[16:43:07:151][  info  ][               ]: Configuring logger ...
[16:43:07:153][  info  ][  scanConsole  ]: Scan Type/Config configs/scans/rd53a/reg_readtemp.json
[16:43:07:153][  info  ][  scanConsole  ]: Connectivity:
[16:43:07:153][  info  ][  scanConsole  ]:     configs/CERNQ7/example_rd53a_setup.json
[16:43:07:153][  info  ][  scanConsole  ]: Target ToT: -1
[16:43:07:153][  info  ][  scanConsole  ]: Target Charge: -1
[16:43:07:153][  info  ][  scanConsole  ]: Output Plots: true
[16:43:07:153][  info  ][  scanConsole  ]: Output Directory: ./data/001734_reg_readtemp/
[16:43:07:158][  info  ][  scanConsole  ]: Timestamp: 2022-08-23_16:43:07
[16:43:07:158][  info  ][  scanConsole  ]: Run Number: 1734
[16:43:07:158][  info  ][  scanConsole  ]: #################
[16:43:07:158][  info  ][  scanConsole  ]: # Init Hardware #
[16:43:07:158][  info  ][  scanConsole  ]: #################
[16:43:07:158][  info  ][  scanConsole  ]: -> Opening controller config: configs/controller/specCfg-rd53a-16x1.json
[16:43:07:158][  info  ][  ScanHelper   ]: Loading controller ...
[16:43:07:158][  info  ][  ScanHelper   ]: Found controller of type: spec
[16:43:07:158][  info  ][  ScanHelper   ]: ... loading controler config:
[16:43:07:158][  info  ][  ScanHelper   ]: ~~~ {
[16:43:07:158][  info  ][  ScanHelper   ]: ~~~     "cmdPeriod": 6.250000073038109e-9,
[16:43:07:158][  info  ][  ScanHelper   ]: ~~~     "idle": {
[16:43:07:158][  info  ][  ScanHelper   ]: ~~~         "word": 1768515945
[16:43:07:158][  info  ][  ScanHelper   ]: ~~~     },
[16:43:07:158][  info  ][  ScanHelper   ]: ~~~     "pulse": {
[16:43:07:158][  info  ][  ScanHelper   ]: ~~~         "interval": 500,
[16:43:07:158][  info  ][  ScanHelper   ]: ~~~         "word": 1549575846
[16:43:07:158][  info  ][  ScanHelper   ]: ~~~     },
[16:43:07:158][  info  ][  ScanHelper   ]: ~~~     "rxActiveLanes": 1,
[16:43:07:158][  info  ][  ScanHelper   ]: ~~~     "rxPolarity": 0,
[16:43:07:158][  info  ][  ScanHelper   ]: ~~~     "specNum": 0,
[16:43:07:158][  info  ][  ScanHelper   ]: ~~~     "spiConfig": 541200,
[16:43:07:158][  info  ][  ScanHelper   ]: ~~~     "sync": {
[16:43:07:158][  info  ][  ScanHelper   ]: ~~~         "interval": 32,
[16:43:07:158][  info  ][  ScanHelper   ]: ~~~         "word": 2172551550
[16:43:07:158][  info  ][  ScanHelper   ]: ~~~     },
[16:43:07:158][  info  ][  ScanHelper   ]: ~~~     "txPolarity": 0
[16:43:07:158][  info  ][  ScanHelper   ]: ~~~ }
[16:43:07:158][  info  ][    SpecCom    ]: Opening SPEC with id #0
[16:43:07:158][  info  ][    SpecCom    ]: Mapping BARs ...
[16:43:07:158][  info  ][    SpecCom    ]: ... Mapped BAR0 at 0x7efc49e42000 with size 1048576
[16:43:07:158][warning ][    SpecCom    ]: ... BAR4 not mapped (Mmap failed)
[16:43:07:158][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[16:43:07:158][  info  ][    SpecCom    ]: Firmware Version: 0x4d9ff6d
[16:43:07:158][  info  ][    SpecCom    ]: Firmware Identifier: 0x4030232
[16:43:07:158][  info  ][    SpecCom    ]: FPGA card: PLDA XpressK7 325
[16:43:07:158][  info  ][    SpecCom    ]: FE Chip Type: RD53A/B
[16:43:07:158][  info  ][    SpecCom    ]: FMC Card Type: Ohio Card (Display Port)
[16:43:07:158][  info  ][    SpecCom    ]: RX Speed: 640Mbps
[16:43:07:158][  info  ][    SpecCom    ]: Channel Configuration: 16x1
[16:43:07:158][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[16:43:07:158][  info  ][    SpecCom    ]: Flushing buffers ...
[16:43:07:158][  info  ][    SpecCom    ]: Init success!
[16:43:07:158][  info  ][  scanConsole  ]: #######################
[16:43:07:158][  info  ][  scanConsole  ]: ##  Loading Configs  ##
[16:43:07:158][  info  ][  scanConsole  ]: #######################
[16:43:07:158][  info  ][  scanConsole  ]: Opening global config: configs/CERNQ7/example_rd53a_setup.json
[16:43:07:158][  info  ][  ScanHelper   ]: Chip type: RD53A
[16:43:07:158][  info  ][  ScanHelper   ]: Chip count 4
[16:43:07:158][  info  ][  ScanHelper   ]: Loading chip #0
[16:43:07:159][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(4)
[16:43:07:159][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ7/20UPGFC0012613.json
[16:43:07:337][  info  ][  ScanHelper   ]: Loading chip #1
[16:43:07:337][warning ][  ScanHelper   ]:  ... chip not enabled, skipping!
[16:43:07:337][  info  ][  ScanHelper   ]: Loading chip #2
[16:43:07:337][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(6)
[16:43:07:337][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ7/20UPGFC0012661.json
[16:43:07:510][  info  ][  ScanHelper   ]: Loading chip #3
[16:43:07:511][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(7)
[16:43:07:511][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ7/20UPGFC0012629.json
[16:43:07:692][  info  ][  scanConsole  ]: #################
[16:43:07:692][  info  ][  scanConsole  ]: # Configure FEs #
[16:43:07:692][  info  ][  scanConsole  ]: #################
[16:43:07:692][  info  ][  scanConsole  ]: Configuring 20UPGFC0012613
[16:43:07:723][  info  ][  scanConsole  ]: Configuring 20UPGFC0012661
[16:43:07:754][  info  ][  scanConsole  ]: Configuring 20UPGFC0012629
[16:43:07:784][  info  ][  scanConsole  ]: Sent configuration to all FEs in 91 ms!
[16:43:07:785][  info  ][  scanConsole  ]: Checking com 20UPGFC0012613
[16:43:07:785][  info  ][    SpecRx     ]: Active Rx channels: 0x10
[16:43:07:785][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[16:43:07:785][  info  ][    SpecRx     ]: Rx Status 0xd0
[16:43:07:785][  info  ][    SpecRx     ]: Number of lanes: 1
[16:43:07:785][  info  ][    SpecRx     ]: Channel 4 Lane 0 synchronized!
[16:43:07:795][  info  ][  scanConsole  ]: ... success!
[16:43:07:795][  info  ][  scanConsole  ]: Checking com 20UPGFC0012661
[16:43:07:795][  info  ][    SpecRx     ]: Active Rx channels: 0x40
[16:43:07:795][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[16:43:07:795][  info  ][    SpecRx     ]: Rx Status 0xd0
[16:43:07:795][  info  ][    SpecRx     ]: Number of lanes: 1
[16:43:07:795][  info  ][    SpecRx     ]: Channel 6 Lane 0 synchronized!
[16:43:07:806][  info  ][  scanConsole  ]: ... success!
[16:43:07:806][  info  ][  scanConsole  ]: Checking com 20UPGFC0012629
[16:43:07:806][  info  ][    SpecRx     ]: Active Rx channels: 0x80
[16:43:07:806][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[16:43:07:806][  info  ][    SpecRx     ]: Rx Status 0xd0
[16:43:07:806][  info  ][    SpecRx     ]: Number of lanes: 1
[16:43:07:806][  info  ][    SpecRx     ]: Channel 7 Lane 0 synchronized!
[16:43:07:816][  info  ][  scanConsole  ]: ... success!
[16:43:07:816][  info  ][  scanConsole  ]: Enabling Tx channels
[16:43:07:816][  info  ][  scanConsole  ]: Enabling Tx channel 1
[16:43:07:816][  info  ][  scanConsole  ]: Enabling Tx channel 1
[16:43:07:816][  info  ][  scanConsole  ]: Enabling Tx channel 1
[16:43:07:816][  info  ][  scanConsole  ]: Enabling Rx channels
[16:43:07:816][  info  ][  scanConsole  ]: Enabling Rx channel 4
[16:43:07:816][  info  ][  scanConsole  ]: Enabling Rx channel 6
[16:43:07:816][  info  ][  scanConsole  ]: Enabling Rx channel 7
[16:43:07:816][  info  ][  scanConsole  ]: ##############
[16:43:07:816][  info  ][  scanConsole  ]: # Setup Scan #
[16:43:07:816][  info  ][  scanConsole  ]: ##############
[16:43:07:816][  info  ][  scanConsole  ]: Found Scan config, constructing scan ...
[16:43:07:817][  info  ][  ScanFactory  ]: Loading Scan:
[16:43:07:817][  info  ][  ScanFactory  ]:   Name: AnalogScan
[16:43:07:817][  info  ][  ScanFactory  ]:   Number of Loops: 3
[16:43:07:817][  info  ][  ScanFactory  ]:   Loading Loop #0
[16:43:07:817][  info  ][  ScanFactory  ]:    Type: Rd53aReadRegLoop
[16:43:07:817][  info  ][  ScanFactory  ]:    Loading loop config ... 
[16:43:07:817][  info  ][  ScanFactory  ]: ~~~ {
[16:43:07:817][  info  ][  ScanFactory  ]: ~~~     "CurMux": [],
[16:43:07:817][  info  ][  ScanFactory  ]: ~~~     "EnblRingOsc": 0,
[16:43:07:817][  info  ][  ScanFactory  ]: ~~~     "Registers": [],
[16:43:07:817][  info  ][  ScanFactory  ]: ~~~     "RingOscDur": 9,
[16:43:07:817][  info  ][  ScanFactory  ]: ~~~     "RingOscRep": 10,
[16:43:07:817][  info  ][  ScanFactory  ]: ~~~     "VoltMux": [3, 5, 7, 15]
[16:43:07:817][  info  ][  ScanFactory  ]: ~~~ }
[16:43:07:817][  info  ][  ScanFactory  ]:   Loading Loop #1
[16:43:07:817][  info  ][  ScanFactory  ]:    Type: Rd53aTriggerLoop
[16:43:07:817][  info  ][  ScanFactory  ]:    Loading loop config ... 
[16:43:07:817][  info  ][  ScanFactory  ]: ~~~ {
[16:43:07:817][  info  ][  ScanFactory  ]: ~~~     "count": 1,
[16:43:07:817][  info  ][  ScanFactory  ]: ~~~     "delay": 48,
[16:43:07:817][  info  ][  ScanFactory  ]: ~~~     "extTrig": false,
[16:43:07:817][  info  ][  ScanFactory  ]: ~~~     "frequency": 30000.0,
[16:43:07:817][  info  ][  ScanFactory  ]: ~~~     "noInject": false,
[16:43:07:817][  info  ][  ScanFactory  ]: ~~~     "sendEcr": false,
[16:43:07:817][  info  ][  ScanFactory  ]: ~~~     "time": 0.0,
[16:43:07:817][  info  ][  ScanFactory  ]: ~~~     "trigMultiplier": 16
[16:43:07:817][  info  ][  ScanFactory  ]: ~~~ }
[16:43:07:817][  info  ][  ScanFactory  ]:   Loading Loop #2
[16:43:07:817][  info  ][  ScanFactory  ]:    Type: StdDataLoop
[16:43:07:817][warning ][  ScanFactory  ]: ~~~ Config empty.
[16:43:07:817][  info  ][ScanBuildHistogrammers]: Loading histogrammer ...
[16:43:07:817][  info  ][ScanBuildHistogrammers]: ... done!
[16:43:07:817][  info  ][ScanBuildAnalyses]: Loading analyses ...
[16:43:07:818][  info  ][  scanConsole  ]: Running pre scan!
[16:43:07:818][  info  ][  ScanFactory  ]: Entering pre scan phase ...
[16:43:07:818][  info  ][     Rd53a     ]: Write named register: GlobalPulseRt -> 16384
[16:43:07:818][  info  ][     Rd53a     ]: Write named register: InjEnDig -> 0
[16:43:07:818][  info  ][     Rd53a     ]: Write named register: InjVcalDiff -> 0
[16:43:07:818][  info  ][     Rd53a     ]: Write named register: InjVcalHigh -> 3500
[16:43:07:818][  info  ][     Rd53a     ]: Write named register: InjVcalMed -> 500
[16:43:07:818][  info  ][     Rd53a     ]: Write named register: LatencyConfig -> 48
[16:43:07:818][  info  ][  scanConsole  ]: Starting histogrammer and analysis threads:
[16:43:07:818][  info  ][  scanConsole  ]:  .. started threads of Fe 4
[16:43:07:818][  info  ][  scanConsole  ]:  .. started threads of Fe 6
[16:43:07:818][  info  ][  scanConsole  ]:  .. started threads of Fe 7
[16:43:07:819][  info  ][Rd53aDataProcessor]:   -> Processor thread #0 started!
[16:43:07:819][  info  ][Rd53aDataProcessor]:   -> Processor thread #1 started!
[16:43:07:819][  info  ][Rd53aDataProcessor]:   -> Processor thread #2 started!
[16:43:07:819][  info  ][Rd53aDataProcessor]:   -> Processor thread #3 started!
[16:43:07:819][  info  ][  scanConsole  ]: ########
[16:43:07:819][  info  ][  scanConsole  ]: # Scan #
[16:43:07:819][  info  ][  scanConsole  ]: ########
[16:43:07:819][  info  ][  scanConsole  ]: Starting scan!
[16:43:07:819][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0012613 on Rx 4
[16:43:07:823][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0012613] MON MUX_V: 3 Bias 0 1288 -> 0.277149 V Bias 1 1677 -> 0.354024 V, Temperature 0.0768747 -> -13.5304 C
[16:43:07:827][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0012613] MON MUX_V: 5 Bias 0 1238 -> 0.267268 V Bias 1 1623 -> 0.343353 V, Temperature 0.0760842 -> -15.2542 C
[16:43:07:831][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0012613] MON MUX_V: 7 Bias 0 1238 -> 0.267268 V Bias 1 1632 -> 0.345131 V, Temperature 0.0778628 -> -13.8125 C
[16:43:07:835][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0012613] MON MUX_V: 15 Bias 0 1239 -> 0.267466 V Bias 1 1636 -> 0.345922 V, Temperature 0.0784556 -> -15.787 C
[16:43:07:870][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0012661 on Rx 6
[16:43:07:875][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0012661] MON MUX_V: 3 Bias 0 1308 -> 0.27809 V Bias 1 1704 -> 0.356346 V, Temperature 0.078256 -> -10.6121 C
[16:43:07:879][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0012661] MON MUX_V: 5 Bias 0 1266 -> 0.269791 V Bias 1 1670 -> 0.349627 V, Temperature 0.0798369 -> -18.6524 C
[16:43:07:882][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0012661] MON MUX_V: 7 Bias 0 1247 -> 0.266036 V Bias 1 1654 -> 0.346466 V, Temperature 0.0804298 -> -19.4761 C
[16:43:07:886][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0012661] MON MUX_V: 15 Bias 0 1238 -> 0.264257 V Bias 1 1648 -> 0.34528 V, Temperature 0.0810226 -> -18.6523 C
[16:43:07:921][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0012629 on Rx 7
[16:43:07:925][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0012629] MON MUX_V: 3 Bias 0 1251 -> 0.271178 V Bias 1 1634 -> 0.347012 V, Temperature 0.075834 -> -10.612 C
[16:43:07:930][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0012629] MON MUX_V: 5 Bias 0 1219 -> 0.264842 V Bias 1 1608 -> 0.341864 V, Temperature 0.077022 -> -12.7782 C
[16:43:07:934][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0012629] MON MUX_V: 7 Bias 0 1208 -> 0.262664 V Bias 1 1598 -> 0.339884 V, Temperature 0.07722 -> -12.3707 C
[16:43:07:938][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0012629] MON MUX_V: 15 Bias 0 1186 -> 0.258308 V Bias 1 1574 -> 0.335132 V, Temperature 0.076824 -> -12.688 C
[16:43:07:974][  info  ][  scanConsole  ]: Scan done!
[16:43:07:974][  info  ][  scanConsole  ]: Waiting for processors to finish ...
[16:43:07:975][  info  ][  scanConsole  ]: Processor done, waiting for histogrammer ...
[16:43:08:374][  info  ][HistogramAlgorithm]: Histogrammer done!
[16:43:08:374][  info  ][HistogramAlgorithm]: Histogrammer done!
[16:43:08:374][  info  ][HistogramAlgorithm]: Histogrammer done!
[16:43:08:375][  info  ][  scanConsole  ]: Processor done, waiting for analysis ...
[16:43:08:775][  info  ][AnalysisAlgorithm]: Analysis done!
[16:43:08:775][  info  ][AnalysisAlgorithm]: Analysis done!
[16:43:08:775][  info  ][AnalysisAlgorithm]: Analysis done!
[16:43:08:775][  info  ][  scanConsole  ]: All done!
[16:43:08:775][  info  ][  scanConsole  ]: ##########
[16:43:08:775][  info  ][  scanConsole  ]: # Timing #
[16:43:08:775][  info  ][  scanConsole  ]: ##########
[16:43:08:775][  info  ][  scanConsole  ]: -> Configuration: 91 ms
[16:43:08:775][  info  ][  scanConsole  ]: -> Scan:          155 ms
[16:43:08:775][  info  ][  scanConsole  ]: -> Processing:    0 ms
[16:43:08:775][  info  ][  scanConsole  ]: -> Analysis:      800 ms
[16:43:08:775][  info  ][  scanConsole  ]: ###########
[16:43:08:775][  info  ][  scanConsole  ]: # Cleanup #
[16:43:08:775][  info  ][  scanConsole  ]: ###########
[16:43:08:784][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0012613 to configs/CERNQ7/20UPGFC0012613.json
[16:43:08:951][  info  ][  scanConsole  ]: -> Storing output results of FE 4
[16:43:08:951][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0012613, this usually means that the chip did not send any data at all.
[16:43:08:952][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0012661 to configs/CERNQ7/20UPGFC0012661.json
[16:43:09:126][  info  ][  scanConsole  ]: -> Storing output results of FE 6
[16:43:09:126][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0012661, this usually means that the chip did not send any data at all.
[16:43:09:127][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0012629 to configs/CERNQ7/20UPGFC0012629.json
[16:43:09:301][  info  ][  scanConsole  ]: -> Storing output results of FE 7
[16:43:09:301][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0012629, this usually means that the chip did not send any data at all.
[16:43:09:303][  info  ][  scanConsole  ]: Finishing run: 1734
20UPGFC0012613.json.after
20UPGFC0012613.json.before
20UPGFC0012629.json.after
20UPGFC0012629.json.before
20UPGFC0012661.json.after
20UPGFC0012661.json.before
reg_readtemp.json
scanLog.json

