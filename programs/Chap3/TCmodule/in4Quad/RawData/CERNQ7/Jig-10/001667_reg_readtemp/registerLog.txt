[14:47:16:046][  info  ][               ]: #####################################
[14:47:16:046][  info  ][               ]: # Welcome to the YARR Scan Console! #
[14:47:16:046][  info  ][               ]: #####################################
[14:47:16:046][  info  ][               ]: -> Parsing command line parameters ...
[14:47:16:046][  info  ][               ]: Configuring logger ...
[14:47:16:048][  info  ][  scanConsole  ]: Scan Type/Config configs/scans/rd53a/reg_readtemp.json
[14:47:16:048][  info  ][  scanConsole  ]: Connectivity:
[14:47:16:048][  info  ][  scanConsole  ]:     configs/CERNQ7/example_rd53a_setup.json
[14:47:16:048][  info  ][  scanConsole  ]: Target ToT: -1
[14:47:16:048][  info  ][  scanConsole  ]: Target Charge: -1
[14:47:16:048][  info  ][  scanConsole  ]: Output Plots: true
[14:47:16:048][  info  ][  scanConsole  ]: Output Directory: ./data/001667_reg_readtemp/
[14:47:16:053][  info  ][  scanConsole  ]: Timestamp: 2022-08-23_14:47:16
[14:47:16:053][  info  ][  scanConsole  ]: Run Number: 1667
[14:47:16:053][  info  ][  scanConsole  ]: #################
[14:47:16:053][  info  ][  scanConsole  ]: # Init Hardware #
[14:47:16:053][  info  ][  scanConsole  ]: #################
[14:47:16:053][  info  ][  scanConsole  ]: -> Opening controller config: configs/controller/specCfg-rd53a-16x1.json
[14:47:16:053][  info  ][  ScanHelper   ]: Loading controller ...
[14:47:16:053][  info  ][  ScanHelper   ]: Found controller of type: spec
[14:47:16:053][  info  ][  ScanHelper   ]: ... loading controler config:
[14:47:16:053][  info  ][  ScanHelper   ]: ~~~ {
[14:47:16:053][  info  ][  ScanHelper   ]: ~~~     "cmdPeriod": 6.250000073038109e-9,
[14:47:16:053][  info  ][  ScanHelper   ]: ~~~     "idle": {
[14:47:16:053][  info  ][  ScanHelper   ]: ~~~         "word": 1768515945
[14:47:16:053][  info  ][  ScanHelper   ]: ~~~     },
[14:47:16:053][  info  ][  ScanHelper   ]: ~~~     "pulse": {
[14:47:16:053][  info  ][  ScanHelper   ]: ~~~         "interval": 500,
[14:47:16:053][  info  ][  ScanHelper   ]: ~~~         "word": 1549575846
[14:47:16:053][  info  ][  ScanHelper   ]: ~~~     },
[14:47:16:053][  info  ][  ScanHelper   ]: ~~~     "rxActiveLanes": 1,
[14:47:16:053][  info  ][  ScanHelper   ]: ~~~     "rxPolarity": 0,
[14:47:16:053][  info  ][  ScanHelper   ]: ~~~     "specNum": 0,
[14:47:16:053][  info  ][  ScanHelper   ]: ~~~     "spiConfig": 541200,
[14:47:16:053][  info  ][  ScanHelper   ]: ~~~     "sync": {
[14:47:16:053][  info  ][  ScanHelper   ]: ~~~         "interval": 32,
[14:47:16:053][  info  ][  ScanHelper   ]: ~~~         "word": 2172551550
[14:47:16:053][  info  ][  ScanHelper   ]: ~~~     },
[14:47:16:053][  info  ][  ScanHelper   ]: ~~~     "txPolarity": 0
[14:47:16:053][  info  ][  ScanHelper   ]: ~~~ }
[14:47:16:053][  info  ][    SpecCom    ]: Opening SPEC with id #0
[14:47:16:054][  info  ][    SpecCom    ]: Mapping BARs ...
[14:47:16:054][  info  ][    SpecCom    ]: ... Mapped BAR0 at 0x7f2d9e8a9000 with size 1048576
[14:47:16:054][warning ][    SpecCom    ]: ... BAR4 not mapped (Mmap failed)
[14:47:16:054][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[14:47:16:054][  info  ][    SpecCom    ]: Firmware Version: 0x4d9ff6d
[14:47:16:054][  info  ][    SpecCom    ]: Firmware Identifier: 0x4030232
[14:47:16:054][  info  ][    SpecCom    ]: FPGA card: PLDA XpressK7 325
[14:47:16:054][  info  ][    SpecCom    ]: FE Chip Type: RD53A/B
[14:47:16:054][  info  ][    SpecCom    ]: FMC Card Type: Ohio Card (Display Port)
[14:47:16:054][  info  ][    SpecCom    ]: RX Speed: 640Mbps
[14:47:16:054][  info  ][    SpecCom    ]: Channel Configuration: 16x1
[14:47:16:054][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[14:47:16:054][  info  ][    SpecCom    ]: Flushing buffers ...
[14:47:16:054][  info  ][    SpecCom    ]: Init success!
[14:47:16:054][  info  ][  scanConsole  ]: #######################
[14:47:16:054][  info  ][  scanConsole  ]: ##  Loading Configs  ##
[14:47:16:054][  info  ][  scanConsole  ]: #######################
[14:47:16:054][  info  ][  scanConsole  ]: Opening global config: configs/CERNQ7/example_rd53a_setup.json
[14:47:16:054][  info  ][  ScanHelper   ]: Chip type: RD53A
[14:47:16:054][  info  ][  ScanHelper   ]: Chip count 4
[14:47:16:054][  info  ][  ScanHelper   ]: Loading chip #0
[14:47:16:055][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(4)
[14:47:16:055][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ7/20UPGFC0012613.json
[14:47:16:229][  info  ][  ScanHelper   ]: Loading chip #1
[14:47:16:229][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(5)
[14:47:16:229][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ7/20UPGFC0012645.json
[14:47:16:398][  info  ][  ScanHelper   ]: Loading chip #2
[14:47:16:399][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(6)
[14:47:16:399][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ7/20UPGFC0012661.json
[14:47:16:572][  info  ][  ScanHelper   ]: Loading chip #3
[14:47:16:572][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(7)
[14:47:16:572][  info  ][  ScanHelper   ]: Loading config file: configs/CERNQ7/20UPGFC0012629.json
[14:47:16:749][  info  ][  scanConsole  ]: #################
[14:47:16:749][  info  ][  scanConsole  ]: # Configure FEs #
[14:47:16:749][  info  ][  scanConsole  ]: #################
[14:47:16:749][  info  ][  scanConsole  ]: Configuring 20UPGFC0012613
[14:47:16:779][  info  ][  scanConsole  ]: Configuring 20UPGFC0012645
[14:47:16:810][  info  ][  scanConsole  ]: Configuring 20UPGFC0012661
[14:47:16:840][  info  ][  scanConsole  ]: Configuring 20UPGFC0012629
[14:47:16:887][  info  ][  scanConsole  ]: Sent configuration to all FEs in 138 ms!
[14:47:16:888][  info  ][  scanConsole  ]: Checking com 20UPGFC0012613
[14:47:16:888][  info  ][    SpecRx     ]: Active Rx channels: 0x10
[14:47:16:888][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[14:47:16:888][  info  ][    SpecRx     ]: Rx Status 0xf0
[14:47:16:888][  info  ][    SpecRx     ]: Number of lanes: 1
[14:47:16:888][  info  ][    SpecRx     ]: Channel 4 Lane 0 synchronized!
[14:47:16:899][  info  ][  scanConsole  ]: ... success!
[14:47:16:899][  info  ][  scanConsole  ]: Checking com 20UPGFC0012645
[14:47:16:899][  info  ][    SpecRx     ]: Active Rx channels: 0x20
[14:47:16:899][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[14:47:16:899][  info  ][    SpecRx     ]: Rx Status 0xf0
[14:47:16:899][  info  ][    SpecRx     ]: Number of lanes: 1
[14:47:16:899][  info  ][    SpecRx     ]: Channel 5 Lane 0 synchronized!
[14:47:16:909][  info  ][  scanConsole  ]: ... success!
[14:47:16:909][  info  ][  scanConsole  ]: Checking com 20UPGFC0012661
[14:47:16:909][  info  ][    SpecRx     ]: Active Rx channels: 0x40
[14:47:16:909][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[14:47:16:909][  info  ][    SpecRx     ]: Rx Status 0xf0
[14:47:16:909][  info  ][    SpecRx     ]: Number of lanes: 1
[14:47:16:909][  info  ][    SpecRx     ]: Channel 6 Lane 0 synchronized!
[14:47:16:920][  info  ][  scanConsole  ]: ... success!
[14:47:16:920][  info  ][  scanConsole  ]: Checking com 20UPGFC0012629
[14:47:16:920][  info  ][    SpecRx     ]: Active Rx channels: 0x80
[14:47:16:920][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[14:47:16:920][  info  ][    SpecRx     ]: Rx Status 0xf0
[14:47:16:920][  info  ][    SpecRx     ]: Number of lanes: 1
[14:47:16:920][  info  ][    SpecRx     ]: Channel 7 Lane 0 synchronized!
[14:47:16:930][  info  ][  scanConsole  ]: ... success!
[14:47:16:930][  info  ][  scanConsole  ]: Enabling Tx channels
[14:47:16:931][  info  ][  scanConsole  ]: Enabling Tx channel 1
[14:47:16:931][  info  ][  scanConsole  ]: Enabling Tx channel 1
[14:47:16:931][  info  ][  scanConsole  ]: Enabling Tx channel 1
[14:47:16:931][  info  ][  scanConsole  ]: Enabling Tx channel 1
[14:47:16:931][  info  ][  scanConsole  ]: Enabling Rx channels
[14:47:16:931][  info  ][  scanConsole  ]: Enabling Rx channel 4
[14:47:16:931][  info  ][  scanConsole  ]: Enabling Rx channel 5
[14:47:16:931][  info  ][  scanConsole  ]: Enabling Rx channel 6
[14:47:16:931][  info  ][  scanConsole  ]: Enabling Rx channel 7
[14:47:16:931][  info  ][  scanConsole  ]: ##############
[14:47:16:931][  info  ][  scanConsole  ]: # Setup Scan #
[14:47:16:931][  info  ][  scanConsole  ]: ##############
[14:47:16:932][  info  ][  scanConsole  ]: Found Scan config, constructing scan ...
[14:47:16:932][  info  ][  ScanFactory  ]: Loading Scan:
[14:47:16:932][  info  ][  ScanFactory  ]:   Name: AnalogScan
[14:47:16:932][  info  ][  ScanFactory  ]:   Number of Loops: 3
[14:47:16:932][  info  ][  ScanFactory  ]:   Loading Loop #0
[14:47:16:932][  info  ][  ScanFactory  ]:    Type: Rd53aReadRegLoop
[14:47:16:932][  info  ][  ScanFactory  ]:    Loading loop config ... 
[14:47:16:932][  info  ][  ScanFactory  ]: ~~~ {
[14:47:16:932][  info  ][  ScanFactory  ]: ~~~     "CurMux": [],
[14:47:16:933][  info  ][  ScanFactory  ]: ~~~     "EnblRingOsc": 0,
[14:47:16:933][  info  ][  ScanFactory  ]: ~~~     "Registers": [],
[14:47:16:933][  info  ][  ScanFactory  ]: ~~~     "RingOscDur": 9,
[14:47:16:933][  info  ][  ScanFactory  ]: ~~~     "RingOscRep": 10,
[14:47:16:933][  info  ][  ScanFactory  ]: ~~~     "VoltMux": [3, 5, 7, 15]
[14:47:16:933][  info  ][  ScanFactory  ]: ~~~ }
[14:47:16:933][  info  ][  ScanFactory  ]:   Loading Loop #1
[14:47:16:933][  info  ][  ScanFactory  ]:    Type: Rd53aTriggerLoop
[14:47:16:933][  info  ][  ScanFactory  ]:    Loading loop config ... 
[14:47:16:933][  info  ][  ScanFactory  ]: ~~~ {
[14:47:16:933][  info  ][  ScanFactory  ]: ~~~     "count": 1,
[14:47:16:933][  info  ][  ScanFactory  ]: ~~~     "delay": 48,
[14:47:16:933][  info  ][  ScanFactory  ]: ~~~     "extTrig": false,
[14:47:16:933][  info  ][  ScanFactory  ]: ~~~     "frequency": 30000.0,
[14:47:16:933][  info  ][  ScanFactory  ]: ~~~     "noInject": false,
[14:47:16:933][  info  ][  ScanFactory  ]: ~~~     "sendEcr": false,
[14:47:16:933][  info  ][  ScanFactory  ]: ~~~     "time": 0.0,
[14:47:16:933][  info  ][  ScanFactory  ]: ~~~     "trigMultiplier": 16
[14:47:16:933][  info  ][  ScanFactory  ]: ~~~ }
[14:47:16:933][  info  ][  ScanFactory  ]:   Loading Loop #2
[14:47:16:933][  info  ][  ScanFactory  ]:    Type: StdDataLoop
[14:47:16:933][warning ][  ScanFactory  ]: ~~~ Config empty.
[14:47:16:933][  info  ][ScanBuildHistogrammers]: Loading histogrammer ...
[14:47:16:933][  info  ][ScanBuildHistogrammers]: ... done!
[14:47:16:933][  info  ][ScanBuildAnalyses]: Loading analyses ...
[14:47:16:934][  info  ][  scanConsole  ]: Running pre scan!
[14:47:16:934][  info  ][  ScanFactory  ]: Entering pre scan phase ...
[14:47:16:934][  info  ][     Rd53a     ]: Write named register: GlobalPulseRt -> 16384
[14:47:16:934][  info  ][     Rd53a     ]: Write named register: InjEnDig -> 0
[14:47:16:934][  info  ][     Rd53a     ]: Write named register: InjVcalDiff -> 0
[14:47:16:934][  info  ][     Rd53a     ]: Write named register: InjVcalHigh -> 3500
[14:47:16:934][  info  ][     Rd53a     ]: Write named register: InjVcalMed -> 500
[14:47:16:934][  info  ][     Rd53a     ]: Write named register: LatencyConfig -> 48
[14:47:16:934][  info  ][  scanConsole  ]: Starting histogrammer and analysis threads:
[14:47:16:934][  info  ][  scanConsole  ]:  .. started threads of Fe 4
[14:47:16:934][  info  ][  scanConsole  ]:  .. started threads of Fe 5
[14:47:16:934][  info  ][  scanConsole  ]:  .. started threads of Fe 6
[14:47:16:935][  info  ][  scanConsole  ]:  .. started threads of Fe 7
[14:47:16:935][  info  ][Rd53aDataProcessor]:   -> Processor thread #0 started!
[14:47:16:935][  info  ][Rd53aDataProcessor]:   -> Processor thread #1 started!
[14:47:16:935][  info  ][Rd53aDataProcessor]:   -> Processor thread #2 started!
[14:47:16:935][  info  ][Rd53aDataProcessor]:   -> Processor thread #3 started!
[14:47:16:935][  info  ][  scanConsole  ]: ########
[14:47:16:935][  info  ][  scanConsole  ]: # Scan #
[14:47:16:935][  info  ][  scanConsole  ]: ########
[14:47:16:935][  info  ][  scanConsole  ]: Starting scan!
[14:47:16:935][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0012613 on Rx 4
[14:47:16:939][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0012613] MON MUX_V: 3 Bias 0 1249 -> 0.269442 V Bias 1 1648 -> 0.348293 V, Temperature 0.0788509 -> -6.00867 C
[14:47:16:944][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0012613] MON MUX_V: 5 Bias 0 1199 -> 0.259561 V Bias 1 1594 -> 0.337622 V, Temperature 0.0780604 -> -8.04596 C
[14:47:16:948][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0012613] MON MUX_V: 7 Bias 0 1201 -> 0.259956 V Bias 1 1602 -> 0.339203 V, Temperature 0.0792461 -> -8.76663 C
[14:47:16:952][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0012613] MON MUX_V: 15 Bias 0 1202 -> 0.260154 V Bias 1 1606 -> 0.339993 V, Temperature 0.079839 -> -10.5219 C
[14:47:16:987][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0012645 on Rx 5
[14:47:16:991][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0012645] MON MUX_V: 3 Bias 0 1252 -> 0.273181 V Bias 1 1645 -> 0.351382 V, Temperature 0.0782016 -> -1.21036 C
[14:47:16:995][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0012645] MON MUX_V: 5 Bias 0 1217 -> 0.266216 V Bias 1 1216 -> 0.266017 V, Temperature -0.00019896 -> -281.876 C
[14:47:16:999][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0012645] MON MUX_V: 7 Bias 0 1191 -> 0.261043 V Bias 1 1595 -> 0.341433 V, Temperature 0.0803904 -> -3.61784 C
[14:47:17:003][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0012645] MON MUX_V: 15 Bias 0 1191 -> 0.261043 V Bias 1 1596 -> 0.341632 V, Temperature 0.0805894 -> -4.23567 C
[14:47:17:038][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0012661 on Rx 6
[14:47:17:042][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0012661] MON MUX_V: 3 Bias 0 1271 -> 0.270779 V Bias 1 1676 -> 0.350813 V, Temperature 0.0800346 -> -4.38388 C
[14:47:17:047][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0012661] MON MUX_V: 5 Bias 0 1229 -> 0.262479 V Bias 1 1642 -> 0.344094 V, Temperature 0.0816154 -> -11.2381 C
[14:47:17:051][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0012661] MON MUX_V: 7 Bias 0 1208 -> 0.258329 V Bias 1 1625 -> 0.340735 V, Temperature 0.0824059 -> -11.238 C
[14:47:17:055][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0012661] MON MUX_V: 15 Bias 0 1200 -> 0.256748 V Bias 1 1619 -> 0.339549 V, Temperature 0.0828011 -> -11.2381 C
[14:47:17:090][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0012629 on Rx 7
[14:47:17:095][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0012629] MON MUX_V: 3 Bias 0 1216 -> 0.264248 V Bias 1 1608 -> 0.341864 V, Temperature 0.077616 -> -4.384 C
[14:47:17:099][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0012629] MON MUX_V: 5 Bias 0 1184 -> 0.257912 V Bias 1 1580 -> 0.33632 V, Temperature 0.078408 -> -7.51291 C
[14:47:17:103][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0012629] MON MUX_V: 7 Bias 0 1172 -> 0.255536 V Bias 1 1570 -> 0.33434 V, Temperature 0.078804 -> -6.6041 C
[14:47:17:108][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0012629] MON MUX_V: 15 Bias 0 1150 -> 0.25118 V Bias 1 1547 -> 0.329786 V, Temperature 0.078606 -> -6.45996 C
[14:47:17:144][  info  ][  scanConsole  ]: Scan done!
[14:47:17:144][  info  ][  scanConsole  ]: Waiting for processors to finish ...
[14:47:17:145][  info  ][  scanConsole  ]: Processor done, waiting for histogrammer ...
[14:47:17:545][  info  ][HistogramAlgorithm]: Histogrammer done!
[14:47:17:545][  info  ][HistogramAlgorithm]: Histogrammer done!
[14:47:17:545][  info  ][HistogramAlgorithm]: Histogrammer done!
[14:47:17:545][  info  ][HistogramAlgorithm]: Histogrammer done!
[14:47:17:545][  info  ][  scanConsole  ]: Processor done, waiting for analysis ...
[14:47:17:945][  info  ][AnalysisAlgorithm]: Analysis done!
[14:47:17:945][  info  ][AnalysisAlgorithm]: Analysis done!
[14:47:17:945][  info  ][AnalysisAlgorithm]: Analysis done!
[14:47:17:945][  info  ][AnalysisAlgorithm]: Analysis done!
[14:47:17:946][  info  ][  scanConsole  ]: All done!
[14:47:17:946][  info  ][  scanConsole  ]: ##########
[14:47:17:946][  info  ][  scanConsole  ]: # Timing #
[14:47:17:946][  info  ][  scanConsole  ]: ##########
[14:47:17:946][  info  ][  scanConsole  ]: -> Configuration: 138 ms
[14:47:17:946][  info  ][  scanConsole  ]: -> Scan:          208 ms
[14:47:17:946][  info  ][  scanConsole  ]: -> Processing:    0 ms
[14:47:17:946][  info  ][  scanConsole  ]: -> Analysis:      800 ms
[14:47:17:946][  info  ][  scanConsole  ]: ###########
[14:47:17:946][  info  ][  scanConsole  ]: # Cleanup #
[14:47:17:946][  info  ][  scanConsole  ]: ###########
[14:47:17:954][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0012613 to configs/CERNQ7/20UPGFC0012613.json
[14:47:18:122][  info  ][  scanConsole  ]: -> Storing output results of FE 4
[14:47:18:122][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0012613, this usually means that the chip did not send any data at all.
[14:47:18:123][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0012645 to configs/CERNQ7/20UPGFC0012645.json
[14:47:18:286][  info  ][  scanConsole  ]: -> Storing output results of FE 5
[14:47:18:286][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0012645, this usually means that the chip did not send any data at all.
[14:47:18:287][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0012661 to configs/CERNQ7/20UPGFC0012661.json
[14:47:18:449][  info  ][  scanConsole  ]: -> Storing output results of FE 6
[14:47:18:449][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0012661, this usually means that the chip did not send any data at all.
[14:47:18:450][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0012629 to configs/CERNQ7/20UPGFC0012629.json
[14:47:18:622][  info  ][  scanConsole  ]: -> Storing output results of FE 7
[14:47:18:622][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0012629, this usually means that the chip did not send any data at all.
[14:47:18:624][  info  ][  scanConsole  ]: Finishing run: 1667
20UPGFC0012613.json.after
20UPGFC0012613.json.before
20UPGFC0012629.json.after
20UPGFC0012629.json.before
20UPGFC0012645.json.after
20UPGFC0012645.json.before
20UPGFC0012661.json.after
20UPGFC0012661.json.before
reg_readtemp.json
scanLog.json

