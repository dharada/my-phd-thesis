[15:01:12:079][  info  ][               ]: #####################################
[15:01:12:079][  info  ][               ]: # Welcome to the YARR Scan Console! #
[15:01:12:079][  info  ][               ]: #####################################
[15:01:12:079][  info  ][               ]: -> Parsing command line parameters ...
[15:01:12:079][  info  ][               ]: Configuring logger ...
[15:01:12:081][  info  ][  scanConsole  ]: Scan Type/Config configs/scans/rd53a/reg_readtemp.json
[15:01:12:081][  info  ][  scanConsole  ]: Connectivity:
[15:01:12:081][  info  ][  scanConsole  ]:     configs/KEKQ14/connectivity.json
[15:01:12:081][  info  ][  scanConsole  ]: Target ToT: -1
[15:01:12:081][  info  ][  scanConsole  ]: Target Charge: -1
[15:01:12:081][  info  ][  scanConsole  ]: Output Plots: true
[15:01:12:081][  info  ][  scanConsole  ]: Output Directory: ./data/001768_reg_readtemp/
[15:01:12:085][  info  ][  scanConsole  ]: Timestamp: 2022-08-24_15:01:12
[15:01:12:085][  info  ][  scanConsole  ]: Run Number: 1768
[15:01:12:085][  info  ][  scanConsole  ]: #################
[15:01:12:085][  info  ][  scanConsole  ]: # Init Hardware #
[15:01:12:085][  info  ][  scanConsole  ]: #################
[15:01:12:085][  info  ][  scanConsole  ]: -> Opening controller config: configs/controller/specCfg-rd53a-16x1.json
[15:01:12:085][  info  ][  ScanHelper   ]: Loading controller ...
[15:01:12:085][  info  ][  ScanHelper   ]: Found controller of type: spec
[15:01:12:085][  info  ][  ScanHelper   ]: ... loading controler config:
[15:01:12:085][  info  ][  ScanHelper   ]: ~~~ {
[15:01:12:085][  info  ][  ScanHelper   ]: ~~~     "cmdPeriod": 6.250000073038109e-9,
[15:01:12:085][  info  ][  ScanHelper   ]: ~~~     "idle": {
[15:01:12:085][  info  ][  ScanHelper   ]: ~~~         "word": 1768515945
[15:01:12:085][  info  ][  ScanHelper   ]: ~~~     },
[15:01:12:085][  info  ][  ScanHelper   ]: ~~~     "pulse": {
[15:01:12:085][  info  ][  ScanHelper   ]: ~~~         "interval": 500,
[15:01:12:085][  info  ][  ScanHelper   ]: ~~~         "word": 1549575846
[15:01:12:085][  info  ][  ScanHelper   ]: ~~~     },
[15:01:12:085][  info  ][  ScanHelper   ]: ~~~     "rxActiveLanes": 1,
[15:01:12:085][  info  ][  ScanHelper   ]: ~~~     "rxPolarity": 0,
[15:01:12:085][  info  ][  ScanHelper   ]: ~~~     "specNum": 0,
[15:01:12:085][  info  ][  ScanHelper   ]: ~~~     "spiConfig": 541200,
[15:01:12:085][  info  ][  ScanHelper   ]: ~~~     "sync": {
[15:01:12:085][  info  ][  ScanHelper   ]: ~~~         "interval": 32,
[15:01:12:085][  info  ][  ScanHelper   ]: ~~~         "word": 2172551550
[15:01:12:085][  info  ][  ScanHelper   ]: ~~~     },
[15:01:12:085][  info  ][  ScanHelper   ]: ~~~     "txPolarity": 0
[15:01:12:085][  info  ][  ScanHelper   ]: ~~~ }
[15:01:12:085][  info  ][    SpecCom    ]: Opening SPEC with id #0
[15:01:12:086][  info  ][    SpecCom    ]: Mapping BARs ...
[15:01:12:086][  info  ][    SpecCom    ]: ... Mapped BAR0 at 0x7fd870bda000 with size 1048576
[15:01:12:086][warning ][    SpecCom    ]: ... BAR4 not mapped (Mmap failed)
[15:01:12:086][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[15:01:12:086][  info  ][    SpecCom    ]: Firmware Version: 0x4d9ff6d
[15:01:12:086][  info  ][    SpecCom    ]: Firmware Identifier: 0x4030232
[15:01:12:086][  info  ][    SpecCom    ]: FPGA card: PLDA XpressK7 325
[15:01:12:086][  info  ][    SpecCom    ]: FE Chip Type: RD53A/B
[15:01:12:086][  info  ][    SpecCom    ]: FMC Card Type: Ohio Card (Display Port)
[15:01:12:086][  info  ][    SpecCom    ]: RX Speed: 640Mbps
[15:01:12:086][  info  ][    SpecCom    ]: Channel Configuration: 16x1
[15:01:12:086][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[15:01:12:086][  info  ][    SpecCom    ]: Flushing buffers ...
[15:01:12:086][  info  ][    SpecCom    ]: Init success!
[15:01:12:086][  info  ][  scanConsole  ]: #######################
[15:01:12:086][  info  ][  scanConsole  ]: ##  Loading Configs  ##
[15:01:12:086][  info  ][  scanConsole  ]: #######################
[15:01:12:086][  info  ][  scanConsole  ]: Opening global config: configs/KEKQ14/connectivity.json
[15:01:12:086][  info  ][  ScanHelper   ]: Chip type: RD53A
[15:01:12:086][  info  ][  ScanHelper   ]: Chip count 4
[15:01:12:086][  info  ][  ScanHelper   ]: Loading chip #0
[15:01:12:086][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(4)
[15:01:12:087][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ14/20UPGFC0009300.json
[15:01:12:263][  info  ][  ScanHelper   ]: Loading chip #1
[15:01:12:263][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(5)
[15:01:12:263][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ14/20UPGFC0009316.json
[15:01:12:438][  info  ][  ScanHelper   ]: Loading chip #2
[15:01:12:438][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(6)
[15:01:12:438][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ14/20UPGFC0009332.json
[15:01:12:613][  info  ][  ScanHelper   ]: Loading chip #3
[15:01:12:613][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(7)
[15:01:12:613][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ14/20UPGFC0009348.json
[15:01:12:793][  info  ][  scanConsole  ]: #################
[15:01:12:794][  info  ][  scanConsole  ]: # Configure FEs #
[15:01:12:794][  info  ][  scanConsole  ]: #################
[15:01:12:794][  info  ][  scanConsole  ]: Configuring 20UPGFC0009300
[15:01:12:825][  info  ][  scanConsole  ]: Configuring 20UPGFC0009316
[15:01:12:855][  info  ][  scanConsole  ]: Configuring 20UPGFC0009332
[15:01:12:886][  info  ][  scanConsole  ]: Configuring 20UPGFC0009348
[15:01:12:919][  info  ][  scanConsole  ]: Sent configuration to all FEs in 125 ms!
[15:01:12:920][  info  ][  scanConsole  ]: Checking com 20UPGFC0009300
[15:01:12:920][  info  ][    SpecRx     ]: Active Rx channels: 0x10
[15:01:12:920][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[15:01:12:920][  info  ][    SpecRx     ]: Rx Status 0xf0
[15:01:12:920][  info  ][    SpecRx     ]: Number of lanes: 1
[15:01:12:920][  info  ][    SpecRx     ]: Channel 4 Lane 0 synchronized!
[15:01:12:930][  info  ][  scanConsole  ]: ... success!
[15:01:12:930][  info  ][  scanConsole  ]: Checking com 20UPGFC0009316
[15:01:12:930][  info  ][    SpecRx     ]: Active Rx channels: 0x20
[15:01:12:930][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[15:01:12:930][  info  ][    SpecRx     ]: Rx Status 0xf0
[15:01:12:930][  info  ][    SpecRx     ]: Number of lanes: 1
[15:01:12:930][  info  ][    SpecRx     ]: Channel 5 Lane 0 synchronized!
[15:01:12:941][  info  ][  scanConsole  ]: ... success!
[15:01:12:941][  info  ][  scanConsole  ]: Checking com 20UPGFC0009332
[15:01:12:941][  info  ][    SpecRx     ]: Active Rx channels: 0x40
[15:01:12:941][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[15:01:12:941][  info  ][    SpecRx     ]: Rx Status 0xf0
[15:01:12:941][  info  ][    SpecRx     ]: Number of lanes: 1
[15:01:12:941][  info  ][    SpecRx     ]: Channel 6 Lane 0 synchronized!
[15:01:12:951][  info  ][  scanConsole  ]: ... success!
[15:01:12:951][  info  ][  scanConsole  ]: Checking com 20UPGFC0009348
[15:01:12:951][  info  ][    SpecRx     ]: Active Rx channels: 0x80
[15:01:12:951][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[15:01:12:951][  info  ][    SpecRx     ]: Rx Status 0xf0
[15:01:12:951][  info  ][    SpecRx     ]: Number of lanes: 1
[15:01:12:951][  info  ][    SpecRx     ]: Channel 7 Lane 0 synchronized!
[15:01:12:961][  info  ][  scanConsole  ]: ... success!
[15:01:12:961][  info  ][  scanConsole  ]: Enabling Tx channels
[15:01:12:961][  info  ][  scanConsole  ]: Enabling Tx channel 1
[15:01:12:961][  info  ][  scanConsole  ]: Enabling Tx channel 1
[15:01:12:961][  info  ][  scanConsole  ]: Enabling Tx channel 1
[15:01:12:961][  info  ][  scanConsole  ]: Enabling Tx channel 1
[15:01:12:961][  info  ][  scanConsole  ]: Enabling Rx channels
[15:01:12:961][  info  ][  scanConsole  ]: Enabling Rx channel 4
[15:01:12:961][  info  ][  scanConsole  ]: Enabling Rx channel 5
[15:01:12:961][  info  ][  scanConsole  ]: Enabling Rx channel 6
[15:01:12:961][  info  ][  scanConsole  ]: Enabling Rx channel 7
[15:01:12:961][  info  ][  scanConsole  ]: ##############
[15:01:12:961][  info  ][  scanConsole  ]: # Setup Scan #
[15:01:12:961][  info  ][  scanConsole  ]: ##############
[15:01:12:962][  info  ][  scanConsole  ]: Found Scan config, constructing scan ...
[15:01:12:962][  info  ][  ScanFactory  ]: Loading Scan:
[15:01:12:962][  info  ][  ScanFactory  ]:   Name: AnalogScan
[15:01:12:962][  info  ][  ScanFactory  ]:   Number of Loops: 3
[15:01:12:962][  info  ][  ScanFactory  ]:   Loading Loop #0
[15:01:12:962][  info  ][  ScanFactory  ]:    Type: Rd53aReadRegLoop
[15:01:12:962][  info  ][  ScanFactory  ]:    Loading loop config ... 
[15:01:12:962][  info  ][  ScanFactory  ]: ~~~ {
[15:01:12:962][  info  ][  ScanFactory  ]: ~~~     "CurMux": [],
[15:01:12:962][  info  ][  ScanFactory  ]: ~~~     "EnblRingOsc": 0,
[15:01:12:962][  info  ][  ScanFactory  ]: ~~~     "Registers": [],
[15:01:12:962][  info  ][  ScanFactory  ]: ~~~     "RingOscDur": 9,
[15:01:12:962][  info  ][  ScanFactory  ]: ~~~     "RingOscRep": 10,
[15:01:12:962][  info  ][  ScanFactory  ]: ~~~     "VoltMux": [3, 5, 7, 15]
[15:01:12:962][  info  ][  ScanFactory  ]: ~~~ }
[15:01:12:962][  info  ][  ScanFactory  ]:   Loading Loop #1
[15:01:12:962][  info  ][  ScanFactory  ]:    Type: Rd53aTriggerLoop
[15:01:12:962][  info  ][  ScanFactory  ]:    Loading loop config ... 
[15:01:12:962][  info  ][  ScanFactory  ]: ~~~ {
[15:01:12:962][  info  ][  ScanFactory  ]: ~~~     "count": 1,
[15:01:12:962][  info  ][  ScanFactory  ]: ~~~     "delay": 48,
[15:01:12:962][  info  ][  ScanFactory  ]: ~~~     "extTrig": false,
[15:01:12:962][  info  ][  ScanFactory  ]: ~~~     "frequency": 30000.0,
[15:01:12:962][  info  ][  ScanFactory  ]: ~~~     "noInject": false,
[15:01:12:962][  info  ][  ScanFactory  ]: ~~~     "sendEcr": false,
[15:01:12:962][  info  ][  ScanFactory  ]: ~~~     "time": 0.0,
[15:01:12:962][  info  ][  ScanFactory  ]: ~~~     "trigMultiplier": 16
[15:01:12:962][  info  ][  ScanFactory  ]: ~~~ }
[15:01:12:962][  info  ][  ScanFactory  ]:   Loading Loop #2
[15:01:12:962][  info  ][  ScanFactory  ]:    Type: StdDataLoop
[15:01:12:962][warning ][  ScanFactory  ]: ~~~ Config empty.
[15:01:12:962][  info  ][ScanBuildHistogrammers]: Loading histogrammer ...
[15:01:12:962][  info  ][ScanBuildHistogrammers]: ... done!
[15:01:12:962][  info  ][ScanBuildAnalyses]: Loading analyses ...
[15:01:12:962][  info  ][  scanConsole  ]: Running pre scan!
[15:01:12:962][  info  ][  ScanFactory  ]: Entering pre scan phase ...
[15:01:12:962][  info  ][     Rd53a     ]: Write named register: GlobalPulseRt -> 16384
[15:01:12:962][  info  ][     Rd53a     ]: Write named register: InjEnDig -> 0
[15:01:12:962][  info  ][     Rd53a     ]: Write named register: InjVcalDiff -> 0
[15:01:12:962][  info  ][     Rd53a     ]: Write named register: InjVcalHigh -> 3500
[15:01:12:962][  info  ][     Rd53a     ]: Write named register: InjVcalMed -> 500
[15:01:12:962][  info  ][     Rd53a     ]: Write named register: LatencyConfig -> 48
[15:01:12:962][  info  ][  scanConsole  ]: Starting histogrammer and analysis threads:
[15:01:12:962][  info  ][  scanConsole  ]:  .. started threads of Fe 4
[15:01:12:962][  info  ][  scanConsole  ]:  .. started threads of Fe 5
[15:01:12:962][  info  ][  scanConsole  ]:  .. started threads of Fe 6
[15:01:12:962][  info  ][  scanConsole  ]:  .. started threads of Fe 7
[15:01:12:963][  info  ][Rd53aDataProcessor]:   -> Processor thread #0 started!
[15:01:12:963][  info  ][Rd53aDataProcessor]:   -> Processor thread #1 started!
[15:01:12:963][  info  ][Rd53aDataProcessor]:   -> Processor thread #2 started!
[15:01:12:963][  info  ][Rd53aDataProcessor]:   -> Processor thread #3 started!
[15:01:12:963][  info  ][  scanConsole  ]: ########
[15:01:12:963][  info  ][  scanConsole  ]: # Scan #
[15:01:12:963][  info  ][  scanConsole  ]: ########
[15:01:12:963][  info  ][  scanConsole  ]: Starting scan!
[15:01:12:963][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0009300 on Rx 4
[15:01:12:967][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0009300] MON MUX_V: 3 Bias 0 1299 -> 0.281178 V Bias 1 1691 -> 0.358514 V, Temperature 0.0773362 -> -7.29224 C
[15:01:12:970][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0009300] MON MUX_V: 5 Bias 0 1246 -> 0.270722 V Bias 1 1644 -> 0.349241 V, Temperature 0.0785199 -> -1.88019 C
[15:01:12:974][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0009300] MON MUX_V: 7 Bias 0 1235 -> 0.268551 V Bias 1 1637 -> 0.34786 V, Temperature 0.079309 -> -9.92566 C
[15:01:12:978][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0009300] MON MUX_V: 15 Bias 0 1221 -> 0.265789 V Bias 1 1627 -> 0.345888 V, Temperature 0.0800982 -> -11.1754 C
[15:01:13:013][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0009316 on Rx 5
[15:01:13:017][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0009316] MON MUX_V: 3 Bias 0 1290 -> 0.277692 V Bias 1 1679 -> 0.355181 V, Temperature 0.0774888 -> -6.77444 C
[15:01:13:021][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0009316] MON MUX_V: 5 Bias 0 1263 -> 0.272314 V Bias 1 1650 -> 0.349404 V, Temperature 0.0770904 -> -9.22336 C
[15:01:13:025][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0009316] MON MUX_V: 7 Bias 0 1236 -> 0.266935 V Bias 1 1640 -> 0.347412 V, Temperature 0.0804768 -> -5.26437 C
[15:01:13:029][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0009316] MON MUX_V: 15 Bias 0 1228 -> 0.265342 V Bias 1 1623 -> 0.344026 V, Temperature 0.078684 -> -7.37433 C
[15:01:13:064][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0009332 on Rx 6
[15:01:13:068][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0009332] MON MUX_V: 3 Bias 0 1267 -> 0.278528 V Bias 1 1655 -> 0.356206 V, Temperature 0.0776776 -> -9.4516 C
[15:01:13:072][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0009332] MON MUX_V: 5 Bias 0 1217 -> 0.268518 V Bias 1 1611 -> 0.347397 V, Temperature 0.0788788 -> -9.53461 C
[15:01:13:077][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0009332] MON MUX_V: 7 Bias 0 1219 -> 0.268919 V Bias 1 1615 -> 0.348198 V, Temperature 0.0792792 -> -8.19647 C
[15:01:13:081][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0009332] MON MUX_V: 15 Bias 0 1196 -> 0.264314 V Bias 1 1599 -> 0.344995 V, Temperature 0.0806806 -> -9.48254 C
[15:01:13:115][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0009348 on Rx 7
[15:01:13:120][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0009348] MON MUX_V: 3 Bias 0 1266 -> 0.274009 V Bias 1 1652 -> 0.351441 V, Temperature 0.0774316 -> -7.29947 C
[15:01:13:124][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0009348] MON MUX_V: 5 Bias 0 1222 -> 0.265183 V Bias 1 1605 -> 0.342013 V, Temperature 0.0768298 -> -7.116 C
[15:01:13:128][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0009348] MON MUX_V: 7 Bias 0 1196 -> 0.259967 V Bias 1 1593 -> 0.339606 V, Temperature 0.0796382 -> -5.41281 C
[15:01:13:132][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0009348] MON MUX_V: 15 Bias 0 1203 -> 0.261371 V Bias 1 1604 -> 0.341812 V, Temperature 0.0804406 -> -3.55972 C
[15:01:13:168][  info  ][  scanConsole  ]: Scan done!
[15:01:13:168][  info  ][  scanConsole  ]: Waiting for processors to finish ...
[15:01:13:169][  info  ][  scanConsole  ]: Processor done, waiting for histogrammer ...
[15:01:13:569][  info  ][HistogramAlgorithm]: Histogrammer done!
[15:01:13:569][  info  ][HistogramAlgorithm]: Histogrammer done!
[15:01:13:569][  info  ][HistogramAlgorithm]: Histogrammer done!
[15:01:13:569][  info  ][HistogramAlgorithm]: Histogrammer done!
[15:01:13:569][  info  ][  scanConsole  ]: Processor done, waiting for analysis ...
[15:01:13:969][  info  ][AnalysisAlgorithm]: Analysis done!
[15:01:13:969][  info  ][AnalysisAlgorithm]: Analysis done!
[15:01:13:969][  info  ][AnalysisAlgorithm]: Analysis done!
[15:01:13:969][  info  ][AnalysisAlgorithm]: Analysis done!
[15:01:13:970][  info  ][  scanConsole  ]: All done!
[15:01:13:970][  info  ][  scanConsole  ]: ##########
[15:01:13:970][  info  ][  scanConsole  ]: # Timing #
[15:01:13:970][  info  ][  scanConsole  ]: ##########
[15:01:13:970][  info  ][  scanConsole  ]: -> Configuration: 125 ms
[15:01:13:970][  info  ][  scanConsole  ]: -> Scan:          205 ms
[15:01:13:970][  info  ][  scanConsole  ]: -> Processing:    0 ms
[15:01:13:970][  info  ][  scanConsole  ]: -> Analysis:      800 ms
[15:01:13:970][  info  ][  scanConsole  ]: ###########
[15:01:13:970][  info  ][  scanConsole  ]: # Cleanup #
[15:01:13:970][  info  ][  scanConsole  ]: ###########
[15:01:13:978][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0009300 to configs/KEKQ14/20UPGFC0009300.json
[15:01:14:155][  info  ][  scanConsole  ]: -> Storing output results of FE 4
[15:01:14:155][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0009300, this usually means that the chip did not send any data at all.
[15:01:14:156][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0009316 to configs/KEKQ14/20UPGFC0009316.json
[15:01:14:336][  info  ][  scanConsole  ]: -> Storing output results of FE 5
[15:01:14:336][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0009316, this usually means that the chip did not send any data at all.
[15:01:14:338][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0009332 to configs/KEKQ14/20UPGFC0009332.json
[15:01:14:515][  info  ][  scanConsole  ]: -> Storing output results of FE 6
[15:01:14:515][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0009332, this usually means that the chip did not send any data at all.
[15:01:14:517][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0009348 to configs/KEKQ14/20UPGFC0009348.json
[15:01:14:683][  info  ][  scanConsole  ]: -> Storing output results of FE 7
[15:01:14:683][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0009348, this usually means that the chip did not send any data at all.
[15:01:14:685][  info  ][  scanConsole  ]: Finishing run: 1768
20UPGFC0009300.json.after
20UPGFC0009300.json.before
20UPGFC0009316.json.after
20UPGFC0009316.json.before
20UPGFC0009332.json.after
20UPGFC0009332.json.before
20UPGFC0009348.json.after
20UPGFC0009348.json.before
reg_readtemp.json
scanLog.json

