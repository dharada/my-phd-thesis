[15:10:14:004][  info  ][               ]: #####################################
[15:10:14:004][  info  ][               ]: # Welcome to the YARR Scan Console! #
[15:10:14:004][  info  ][               ]: #####################################
[15:10:14:004][  info  ][               ]: -> Parsing command line parameters ...
[15:10:14:004][  info  ][               ]: Configuring logger ...
[15:10:14:006][  info  ][  scanConsole  ]: Scan Type/Config configs/scans/rd53a/reg_readtemp.json
[15:10:14:006][  info  ][  scanConsole  ]: Connectivity:
[15:10:14:006][  info  ][  scanConsole  ]:     configs/KEKQ14/connectivity.json
[15:10:14:006][  info  ][  scanConsole  ]: Target ToT: -1
[15:10:14:006][  info  ][  scanConsole  ]: Target Charge: -1
[15:10:14:006][  info  ][  scanConsole  ]: Output Plots: true
[15:10:14:006][  info  ][  scanConsole  ]: Output Directory: ./data/001771_reg_readtemp/
[15:10:14:011][  info  ][  scanConsole  ]: Timestamp: 2022-08-24_15:10:14
[15:10:14:011][  info  ][  scanConsole  ]: Run Number: 1771
[15:10:14:011][  info  ][  scanConsole  ]: #################
[15:10:14:011][  info  ][  scanConsole  ]: # Init Hardware #
[15:10:14:011][  info  ][  scanConsole  ]: #################
[15:10:14:011][  info  ][  scanConsole  ]: -> Opening controller config: configs/controller/specCfg-rd53a-16x1.json
[15:10:14:011][  info  ][  ScanHelper   ]: Loading controller ...
[15:10:14:011][  info  ][  ScanHelper   ]: Found controller of type: spec
[15:10:14:011][  info  ][  ScanHelper   ]: ... loading controler config:
[15:10:14:011][  info  ][  ScanHelper   ]: ~~~ {
[15:10:14:011][  info  ][  ScanHelper   ]: ~~~     "cmdPeriod": 6.250000073038109e-9,
[15:10:14:011][  info  ][  ScanHelper   ]: ~~~     "idle": {
[15:10:14:011][  info  ][  ScanHelper   ]: ~~~         "word": 1768515945
[15:10:14:011][  info  ][  ScanHelper   ]: ~~~     },
[15:10:14:011][  info  ][  ScanHelper   ]: ~~~     "pulse": {
[15:10:14:011][  info  ][  ScanHelper   ]: ~~~         "interval": 500,
[15:10:14:011][  info  ][  ScanHelper   ]: ~~~         "word": 1549575846
[15:10:14:011][  info  ][  ScanHelper   ]: ~~~     },
[15:10:14:011][  info  ][  ScanHelper   ]: ~~~     "rxActiveLanes": 1,
[15:10:14:011][  info  ][  ScanHelper   ]: ~~~     "rxPolarity": 0,
[15:10:14:011][  info  ][  ScanHelper   ]: ~~~     "specNum": 0,
[15:10:14:011][  info  ][  ScanHelper   ]: ~~~     "spiConfig": 541200,
[15:10:14:011][  info  ][  ScanHelper   ]: ~~~     "sync": {
[15:10:14:011][  info  ][  ScanHelper   ]: ~~~         "interval": 32,
[15:10:14:011][  info  ][  ScanHelper   ]: ~~~         "word": 2172551550
[15:10:14:011][  info  ][  ScanHelper   ]: ~~~     },
[15:10:14:011][  info  ][  ScanHelper   ]: ~~~     "txPolarity": 0
[15:10:14:011][  info  ][  ScanHelper   ]: ~~~ }
[15:10:14:011][  info  ][    SpecCom    ]: Opening SPEC with id #0
[15:10:14:011][  info  ][    SpecCom    ]: Mapping BARs ...
[15:10:14:011][  info  ][    SpecCom    ]: ... Mapped BAR0 at 0x7f75be2e0000 with size 1048576
[15:10:14:011][warning ][    SpecCom    ]: ... BAR4 not mapped (Mmap failed)
[15:10:14:011][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[15:10:14:011][  info  ][    SpecCom    ]: Firmware Version: 0x4d9ff6d
[15:10:14:011][  info  ][    SpecCom    ]: Firmware Identifier: 0x4030232
[15:10:14:011][  info  ][    SpecCom    ]: FPGA card: PLDA XpressK7 325
[15:10:14:011][  info  ][    SpecCom    ]: FE Chip Type: RD53A/B
[15:10:14:011][  info  ][    SpecCom    ]: FMC Card Type: Ohio Card (Display Port)
[15:10:14:011][  info  ][    SpecCom    ]: RX Speed: 640Mbps
[15:10:14:011][  info  ][    SpecCom    ]: Channel Configuration: 16x1
[15:10:14:011][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[15:10:14:011][  info  ][    SpecCom    ]: Flushing buffers ...
[15:10:14:011][  info  ][    SpecCom    ]: Init success!
[15:10:14:011][  info  ][  scanConsole  ]: #######################
[15:10:14:011][  info  ][  scanConsole  ]: ##  Loading Configs  ##
[15:10:14:011][  info  ][  scanConsole  ]: #######################
[15:10:14:011][  info  ][  scanConsole  ]: Opening global config: configs/KEKQ14/connectivity.json
[15:10:14:011][  info  ][  ScanHelper   ]: Chip type: RD53A
[15:10:14:011][  info  ][  ScanHelper   ]: Chip count 4
[15:10:14:011][  info  ][  ScanHelper   ]: Loading chip #0
[15:10:14:012][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(4)
[15:10:14:012][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ14/20UPGFC0009300.json
[15:10:14:184][  info  ][  ScanHelper   ]: Loading chip #1
[15:10:14:184][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(5)
[15:10:14:184][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ14/20UPGFC0009316.json
[15:10:14:358][  info  ][  ScanHelper   ]: Loading chip #2
[15:10:14:359][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(6)
[15:10:14:359][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ14/20UPGFC0009332.json
[15:10:14:534][  info  ][  ScanHelper   ]: Loading chip #3
[15:10:14:535][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(7)
[15:10:14:535][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ14/20UPGFC0009348.json
[15:10:14:715][  info  ][  scanConsole  ]: #################
[15:10:14:715][  info  ][  scanConsole  ]: # Configure FEs #
[15:10:14:715][  info  ][  scanConsole  ]: #################
[15:10:14:715][  info  ][  scanConsole  ]: Configuring 20UPGFC0009300
[15:10:14:746][  info  ][  scanConsole  ]: Configuring 20UPGFC0009316
[15:10:14:776][  info  ][  scanConsole  ]: Configuring 20UPGFC0009332
[15:10:14:806][  info  ][  scanConsole  ]: Configuring 20UPGFC0009348
[15:10:14:836][  info  ][  scanConsole  ]: Sent configuration to all FEs in 120 ms!
[15:10:14:837][  info  ][  scanConsole  ]: Checking com 20UPGFC0009300
[15:10:14:837][  info  ][    SpecRx     ]: Active Rx channels: 0x10
[15:10:14:837][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[15:10:14:837][  info  ][    SpecRx     ]: Rx Status 0xf0
[15:10:14:837][  info  ][    SpecRx     ]: Number of lanes: 1
[15:10:14:837][  info  ][    SpecRx     ]: Channel 4 Lane 0 synchronized!
[15:10:14:848][  info  ][  scanConsole  ]: ... success!
[15:10:14:848][  info  ][  scanConsole  ]: Checking com 20UPGFC0009316
[15:10:14:848][  info  ][    SpecRx     ]: Active Rx channels: 0x20
[15:10:14:848][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[15:10:14:848][  info  ][    SpecRx     ]: Rx Status 0xf0
[15:10:14:848][  info  ][    SpecRx     ]: Number of lanes: 1
[15:10:14:848][  info  ][    SpecRx     ]: Channel 5 Lane 0 synchronized!
[15:10:14:858][  info  ][  scanConsole  ]: ... success!
[15:10:14:858][  info  ][  scanConsole  ]: Checking com 20UPGFC0009332
[15:10:14:858][  info  ][    SpecRx     ]: Active Rx channels: 0x40
[15:10:14:858][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[15:10:14:858][  info  ][    SpecRx     ]: Rx Status 0xf0
[15:10:14:858][  info  ][    SpecRx     ]: Number of lanes: 1
[15:10:14:858][  info  ][    SpecRx     ]: Channel 6 Lane 0 synchronized!
[15:10:14:868][  info  ][  scanConsole  ]: ... success!
[15:10:14:868][  info  ][  scanConsole  ]: Checking com 20UPGFC0009348
[15:10:14:868][  info  ][    SpecRx     ]: Active Rx channels: 0x80
[15:10:14:868][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[15:10:14:868][  info  ][    SpecRx     ]: Rx Status 0xf0
[15:10:14:869][  info  ][    SpecRx     ]: Number of lanes: 1
[15:10:14:869][  info  ][    SpecRx     ]: Channel 7 Lane 0 synchronized!
[15:10:14:879][  info  ][  scanConsole  ]: ... success!
[15:10:14:879][  info  ][  scanConsole  ]: Enabling Tx channels
[15:10:14:879][  info  ][  scanConsole  ]: Enabling Tx channel 1
[15:10:14:879][  info  ][  scanConsole  ]: Enabling Tx channel 1
[15:10:14:879][  info  ][  scanConsole  ]: Enabling Tx channel 1
[15:10:14:879][  info  ][  scanConsole  ]: Enabling Tx channel 1
[15:10:14:879][  info  ][  scanConsole  ]: Enabling Rx channels
[15:10:14:879][  info  ][  scanConsole  ]: Enabling Rx channel 4
[15:10:14:879][  info  ][  scanConsole  ]: Enabling Rx channel 5
[15:10:14:879][  info  ][  scanConsole  ]: Enabling Rx channel 6
[15:10:14:879][  info  ][  scanConsole  ]: Enabling Rx channel 7
[15:10:14:879][  info  ][  scanConsole  ]: ##############
[15:10:14:879][  info  ][  scanConsole  ]: # Setup Scan #
[15:10:14:879][  info  ][  scanConsole  ]: ##############
[15:10:14:880][  info  ][  scanConsole  ]: Found Scan config, constructing scan ...
[15:10:14:880][  info  ][  ScanFactory  ]: Loading Scan:
[15:10:14:880][  info  ][  ScanFactory  ]:   Name: AnalogScan
[15:10:14:880][  info  ][  ScanFactory  ]:   Number of Loops: 3
[15:10:14:880][  info  ][  ScanFactory  ]:   Loading Loop #0
[15:10:14:880][  info  ][  ScanFactory  ]:    Type: Rd53aReadRegLoop
[15:10:14:880][  info  ][  ScanFactory  ]:    Loading loop config ... 
[15:10:14:880][  info  ][  ScanFactory  ]: ~~~ {
[15:10:14:880][  info  ][  ScanFactory  ]: ~~~     "CurMux": [],
[15:10:14:881][  info  ][  ScanFactory  ]: ~~~     "EnblRingOsc": 0,
[15:10:14:881][  info  ][  ScanFactory  ]: ~~~     "Registers": [],
[15:10:14:881][  info  ][  ScanFactory  ]: ~~~     "RingOscDur": 9,
[15:10:14:881][  info  ][  ScanFactory  ]: ~~~     "RingOscRep": 10,
[15:10:14:881][  info  ][  ScanFactory  ]: ~~~     "VoltMux": [3, 5, 7, 15]
[15:10:14:881][  info  ][  ScanFactory  ]: ~~~ }
[15:10:14:881][  info  ][  ScanFactory  ]:   Loading Loop #1
[15:10:14:881][  info  ][  ScanFactory  ]:    Type: Rd53aTriggerLoop
[15:10:14:881][  info  ][  ScanFactory  ]:    Loading loop config ... 
[15:10:14:881][  info  ][  ScanFactory  ]: ~~~ {
[15:10:14:881][  info  ][  ScanFactory  ]: ~~~     "count": 1,
[15:10:14:881][  info  ][  ScanFactory  ]: ~~~     "delay": 48,
[15:10:14:881][  info  ][  ScanFactory  ]: ~~~     "extTrig": false,
[15:10:14:881][  info  ][  ScanFactory  ]: ~~~     "frequency": 30000.0,
[15:10:14:881][  info  ][  ScanFactory  ]: ~~~     "noInject": false,
[15:10:14:881][  info  ][  ScanFactory  ]: ~~~     "sendEcr": false,
[15:10:14:881][  info  ][  ScanFactory  ]: ~~~     "time": 0.0,
[15:10:14:881][  info  ][  ScanFactory  ]: ~~~     "trigMultiplier": 16
[15:10:14:881][  info  ][  ScanFactory  ]: ~~~ }
[15:10:14:881][  info  ][  ScanFactory  ]:   Loading Loop #2
[15:10:14:881][  info  ][  ScanFactory  ]:    Type: StdDataLoop
[15:10:14:881][warning ][  ScanFactory  ]: ~~~ Config empty.
[15:10:14:881][  info  ][ScanBuildHistogrammers]: Loading histogrammer ...
[15:10:14:881][  info  ][ScanBuildHistogrammers]: ... done!
[15:10:14:881][  info  ][ScanBuildAnalyses]: Loading analyses ...
[15:10:14:882][  info  ][  scanConsole  ]: Running pre scan!
[15:10:14:882][  info  ][  ScanFactory  ]: Entering pre scan phase ...
[15:10:14:882][  info  ][     Rd53a     ]: Write named register: GlobalPulseRt -> 16384
[15:10:14:882][  info  ][     Rd53a     ]: Write named register: InjEnDig -> 0
[15:10:14:882][  info  ][     Rd53a     ]: Write named register: InjVcalDiff -> 0
[15:10:14:882][  info  ][     Rd53a     ]: Write named register: InjVcalHigh -> 3500
[15:10:14:882][  info  ][     Rd53a     ]: Write named register: InjVcalMed -> 500
[15:10:14:882][  info  ][     Rd53a     ]: Write named register: LatencyConfig -> 48
[15:10:14:882][  info  ][  scanConsole  ]: Starting histogrammer and analysis threads:
[15:10:14:882][  info  ][  scanConsole  ]:  .. started threads of Fe 4
[15:10:14:882][  info  ][  scanConsole  ]:  .. started threads of Fe 5
[15:10:14:883][  info  ][  scanConsole  ]:  .. started threads of Fe 6
[15:10:14:883][  info  ][  scanConsole  ]:  .. started threads of Fe 7
[15:10:14:883][  info  ][Rd53aDataProcessor]:   -> Processor thread #0 started!
[15:10:14:883][  info  ][Rd53aDataProcessor]:   -> Processor thread #1 started!
[15:10:14:883][  info  ][Rd53aDataProcessor]:   -> Processor thread #2 started!
[15:10:14:883][  info  ][Rd53aDataProcessor]:   -> Processor thread #3 started!
[15:10:14:883][  info  ][  scanConsole  ]: ########
[15:10:14:883][  info  ][  scanConsole  ]: # Scan #
[15:10:14:883][  info  ][  scanConsole  ]: ########
[15:10:14:884][  info  ][  scanConsole  ]: Starting scan!
[15:10:14:884][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0009300 on Rx 4
[15:10:14:888][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0009300] MON MUX_V: 3 Bias 0 1299 -> 0.281178 V Bias 1 1691 -> 0.358514 V, Temperature 0.0773362 -> -7.29224 C
[15:10:14:892][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0009300] MON MUX_V: 5 Bias 0 1246 -> 0.270722 V Bias 1 1644 -> 0.349241 V, Temperature 0.0785199 -> -1.88019 C
[15:10:14:896][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0009300] MON MUX_V: 7 Bias 0 1237 -> 0.268946 V Bias 1 1638 -> 0.348058 V, Temperature 0.0791118 -> -10.5804 C
[15:10:14:900][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0009300] MON MUX_V: 15 Bias 0 1221 -> 0.265789 V Bias 1 1627 -> 0.345888 V, Temperature 0.0800982 -> -11.1754 C
[15:10:14:936][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0009316 on Rx 5
[15:10:14:940][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0009316] MON MUX_V: 3 Bias 0 1290 -> 0.277692 V Bias 1 1677 -> 0.354782 V, Temperature 0.0770904 -> -8.14407 C
[15:10:14:944][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0009316] MON MUX_V: 5 Bias 0 1262 -> 0.272114 V Bias 1 1649 -> 0.349205 V, Temperature 0.0770904 -> -9.22324 C
[15:10:14:948][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0009316] MON MUX_V: 7 Bias 0 1237 -> 0.267134 V Bias 1 1639 -> 0.347213 V, Temperature 0.0800784 -> -6.59064 C
[15:10:14:952][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0009316] MON MUX_V: 15 Bias 0 1228 -> 0.265342 V Bias 1 1624 -> 0.344225 V, Temperature 0.0788832 -> -6.70157 C
[15:10:14:987][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0009332 on Rx 6
[15:10:14:991][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0009332] MON MUX_V: 3 Bias 0 1267 -> 0.278528 V Bias 1 1656 -> 0.356406 V, Temperature 0.0778778 -> -8.77203 C
[15:10:14:996][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0009332] MON MUX_V: 5 Bias 0 1216 -> 0.268318 V Bias 1 1612 -> 0.347597 V, Temperature 0.0792792 -> -8.19647 C
[15:10:15:000][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0009332] MON MUX_V: 7 Bias 0 1220 -> 0.269119 V Bias 1 1615 -> 0.348198 V, Temperature 0.079079 -> -8.8656 C
[15:10:15:004][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0009332] MON MUX_V: 15 Bias 0 1195 -> 0.264114 V Bias 1 1600 -> 0.345195 V, Temperature 0.081081 -> -8.17413 C
[15:10:15:039][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0009348 on Rx 7
[15:10:15:043][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0009348] MON MUX_V: 3 Bias 0 1267 -> 0.27421 V Bias 1 1652 -> 0.351441 V, Temperature 0.077231 -> -7.9881 C
[15:10:15:047][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0009348] MON MUX_V: 5 Bias 0 1223 -> 0.265384 V Bias 1 1607 -> 0.342414 V, Temperature 0.0770304 -> -6.42148 C
[15:10:15:052][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0009348] MON MUX_V: 7 Bias 0 1196 -> 0.259967 V Bias 1 1595 -> 0.340007 V, Temperature 0.0800394 -> -4.064 C
[15:10:15:056][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0009348] MON MUX_V: 15 Bias 0 1203 -> 0.261371 V Bias 1 1605 -> 0.342013 V, Temperature 0.0806412 -> -2.88742 C
[15:10:15:092][  info  ][  scanConsole  ]: Scan done!
[15:10:15:092][  info  ][  scanConsole  ]: Waiting for processors to finish ...
[15:10:15:093][  info  ][  scanConsole  ]: Processor done, waiting for histogrammer ...
[15:10:15:493][  info  ][HistogramAlgorithm]: Histogrammer done!
[15:10:15:493][  info  ][HistogramAlgorithm]: Histogrammer done!
[15:10:15:493][  info  ][HistogramAlgorithm]: Histogrammer done!
[15:10:15:493][  info  ][HistogramAlgorithm]: Histogrammer done!
[15:10:15:493][  info  ][  scanConsole  ]: Processor done, waiting for analysis ...
[15:10:15:893][  info  ][AnalysisAlgorithm]: Analysis done!
[15:10:15:893][  info  ][AnalysisAlgorithm]: Analysis done!
[15:10:15:893][  info  ][AnalysisAlgorithm]: Analysis done!
[15:10:15:893][  info  ][AnalysisAlgorithm]: Analysis done!
[15:10:15:894][  info  ][  scanConsole  ]: All done!
[15:10:15:894][  info  ][  scanConsole  ]: ##########
[15:10:15:894][  info  ][  scanConsole  ]: # Timing #
[15:10:15:894][  info  ][  scanConsole  ]: ##########
[15:10:15:894][  info  ][  scanConsole  ]: -> Configuration: 120 ms
[15:10:15:894][  info  ][  scanConsole  ]: -> Scan:          208 ms
[15:10:15:894][  info  ][  scanConsole  ]: -> Processing:    0 ms
[15:10:15:894][  info  ][  scanConsole  ]: -> Analysis:      800 ms
[15:10:15:894][  info  ][  scanConsole  ]: ###########
[15:10:15:894][  info  ][  scanConsole  ]: # Cleanup #
[15:10:15:894][  info  ][  scanConsole  ]: ###########
[15:10:15:897][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0009300 to configs/KEKQ14/20UPGFC0009300.json
[15:10:16:113][  info  ][  scanConsole  ]: -> Storing output results of FE 4
[15:10:16:113][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0009300, this usually means that the chip did not send any data at all.
[15:10:16:114][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0009316 to configs/KEKQ14/20UPGFC0009316.json
[15:10:16:302][  info  ][  scanConsole  ]: -> Storing output results of FE 5
[15:10:16:302][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0009316, this usually means that the chip did not send any data at all.
[15:10:16:304][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0009332 to configs/KEKQ14/20UPGFC0009332.json
[15:10:16:478][  info  ][  scanConsole  ]: -> Storing output results of FE 6
[15:10:16:478][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0009332, this usually means that the chip did not send any data at all.
[15:10:16:479][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0009348 to configs/KEKQ14/20UPGFC0009348.json
[15:10:16:645][  info  ][  scanConsole  ]: -> Storing output results of FE 7
[15:10:16:645][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0009348, this usually means that the chip did not send any data at all.
[15:10:16:646][  info  ][  scanConsole  ]: Finishing run: 1771
20UPGFC0009300.json.after
20UPGFC0009300.json.before
20UPGFC0009316.json.after
20UPGFC0009316.json.before
20UPGFC0009332.json.after
20UPGFC0009332.json.before
20UPGFC0009348.json.after
20UPGFC0009348.json.before
reg_readtemp.json
scanLog.json

