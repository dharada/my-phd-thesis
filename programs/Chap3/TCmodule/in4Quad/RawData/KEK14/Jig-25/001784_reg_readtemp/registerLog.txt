[16:32:30:294][  info  ][               ]: #####################################
[16:32:30:294][  info  ][               ]: # Welcome to the YARR Scan Console! #
[16:32:30:294][  info  ][               ]: #####################################
[16:32:30:294][  info  ][               ]: -> Parsing command line parameters ...
[16:32:30:294][  info  ][               ]: Configuring logger ...
[16:32:30:304][  info  ][  scanConsole  ]: Scan Type/Config configs/scans/rd53a/reg_readtemp.json
[16:32:30:304][  info  ][  scanConsole  ]: Connectivity:
[16:32:30:304][  info  ][  scanConsole  ]:     configs/KEKQ14/connectivity.json
[16:32:30:304][  info  ][  scanConsole  ]: Target ToT: -1
[16:32:30:304][  info  ][  scanConsole  ]: Target Charge: -1
[16:32:30:304][  info  ][  scanConsole  ]: Output Plots: true
[16:32:30:304][  info  ][  scanConsole  ]: Output Directory: ./data/001784_reg_readtemp/
[16:32:30:322][  info  ][  scanConsole  ]: Timestamp: 2022-08-24_16:32:30
[16:32:30:322][  info  ][  scanConsole  ]: Run Number: 1784
[16:32:30:322][  info  ][  scanConsole  ]: #################
[16:32:30:322][  info  ][  scanConsole  ]: # Init Hardware #
[16:32:30:322][  info  ][  scanConsole  ]: #################
[16:32:30:323][  info  ][  scanConsole  ]: -> Opening controller config: configs/controller/specCfg-rd53a-16x1.json
[16:32:30:323][  info  ][  ScanHelper   ]: Loading controller ...
[16:32:30:323][  info  ][  ScanHelper   ]: Found controller of type: spec
[16:32:30:323][  info  ][  ScanHelper   ]: ... loading controler config:
[16:32:30:323][  info  ][  ScanHelper   ]: ~~~ {
[16:32:30:323][  info  ][  ScanHelper   ]: ~~~     "cmdPeriod": 6.250000073038109e-9,
[16:32:30:323][  info  ][  ScanHelper   ]: ~~~     "idle": {
[16:32:30:323][  info  ][  ScanHelper   ]: ~~~         "word": 1768515945
[16:32:30:323][  info  ][  ScanHelper   ]: ~~~     },
[16:32:30:323][  info  ][  ScanHelper   ]: ~~~     "pulse": {
[16:32:30:323][  info  ][  ScanHelper   ]: ~~~         "interval": 500,
[16:32:30:323][  info  ][  ScanHelper   ]: ~~~         "word": 1549575846
[16:32:30:323][  info  ][  ScanHelper   ]: ~~~     },
[16:32:30:323][  info  ][  ScanHelper   ]: ~~~     "rxActiveLanes": 1,
[16:32:30:323][  info  ][  ScanHelper   ]: ~~~     "rxPolarity": 0,
[16:32:30:323][  info  ][  ScanHelper   ]: ~~~     "specNum": 0,
[16:32:30:323][  info  ][  ScanHelper   ]: ~~~     "spiConfig": 541200,
[16:32:30:323][  info  ][  ScanHelper   ]: ~~~     "sync": {
[16:32:30:323][  info  ][  ScanHelper   ]: ~~~         "interval": 32,
[16:32:30:323][  info  ][  ScanHelper   ]: ~~~         "word": 2172551550
[16:32:30:323][  info  ][  ScanHelper   ]: ~~~     },
[16:32:30:323][  info  ][  ScanHelper   ]: ~~~     "txPolarity": 0
[16:32:30:324][  info  ][  ScanHelper   ]: ~~~ }
[16:32:30:324][  info  ][    SpecCom    ]: Opening SPEC with id #0
[16:32:30:324][  info  ][    SpecCom    ]: Mapping BARs ...
[16:32:30:325][  info  ][    SpecCom    ]: ... Mapped BAR0 at 0x7fa0aaa8e000 with size 1048576
[16:32:30:325][warning ][    SpecCom    ]: ... BAR4 not mapped (Mmap failed)
[16:32:30:325][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[16:32:30:325][  info  ][    SpecCom    ]: Firmware Version: 0x4d9ff6d
[16:32:30:325][  info  ][    SpecCom    ]: Firmware Identifier: 0x4030232
[16:32:30:325][  info  ][    SpecCom    ]: FPGA card: PLDA XpressK7 325
[16:32:30:325][  info  ][    SpecCom    ]: FE Chip Type: RD53A/B
[16:32:30:325][  info  ][    SpecCom    ]: FMC Card Type: Ohio Card (Display Port)
[16:32:30:325][  info  ][    SpecCom    ]: RX Speed: 640Mbps
[16:32:30:325][  info  ][    SpecCom    ]: Channel Configuration: 16x1
[16:32:30:325][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[16:32:30:325][  info  ][    SpecCom    ]: Flushing buffers ...
[16:32:30:325][  info  ][    SpecCom    ]: Init success!
[16:32:30:325][  info  ][  scanConsole  ]: #######################
[16:32:30:325][  info  ][  scanConsole  ]: ##  Loading Configs  ##
[16:32:30:325][  info  ][  scanConsole  ]: #######################
[16:32:30:325][  info  ][  scanConsole  ]: Opening global config: configs/KEKQ14/connectivity.json
[16:32:30:326][  info  ][  ScanHelper   ]: Chip type: RD53A
[16:32:30:326][  info  ][  ScanHelper   ]: Chip count 4
[16:32:30:326][  info  ][  ScanHelper   ]: Loading chip #0
[16:32:30:329][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(4)
[16:32:30:329][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ14/20UPGFC0009300.json
[16:32:30:535][  info  ][  ScanHelper   ]: Loading chip #1
[16:32:30:535][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(5)
[16:32:30:535][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ14/20UPGFC0009316.json
[16:32:30:711][  info  ][  ScanHelper   ]: Loading chip #2
[16:32:30:711][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(6)
[16:32:30:711][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ14/20UPGFC0009332.json
[16:32:30:887][  info  ][  ScanHelper   ]: Loading chip #3
[16:32:30:887][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(7)
[16:32:30:887][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ14/20UPGFC0009348.json
[16:32:31:065][  info  ][  scanConsole  ]: #################
[16:32:31:065][  info  ][  scanConsole  ]: # Configure FEs #
[16:32:31:065][  info  ][  scanConsole  ]: #################
[16:32:31:065][  info  ][  scanConsole  ]: Configuring 20UPGFC0009300
[16:32:31:095][  info  ][  scanConsole  ]: Configuring 20UPGFC0009316
[16:32:31:136][  info  ][  scanConsole  ]: Configuring 20UPGFC0009332
[16:32:31:168][  info  ][  scanConsole  ]: Configuring 20UPGFC0009348
[16:32:31:199][  info  ][  scanConsole  ]: Sent configuration to all FEs in 134 ms!
[16:32:31:200][  info  ][  scanConsole  ]: Checking com 20UPGFC0009300
[16:32:31:200][  info  ][    SpecRx     ]: Active Rx channels: 0x10
[16:32:31:200][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[16:32:31:200][  info  ][    SpecRx     ]: Rx Status 0xf0
[16:32:31:200][  info  ][    SpecRx     ]: Number of lanes: 1
[16:32:31:200][  info  ][    SpecRx     ]: Channel 4 Lane 0 synchronized!
[16:32:31:210][  info  ][  scanConsole  ]: ... success!
[16:32:31:211][  info  ][  scanConsole  ]: Checking com 20UPGFC0009316
[16:32:31:211][  info  ][    SpecRx     ]: Active Rx channels: 0x20
[16:32:31:211][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[16:32:31:211][  info  ][    SpecRx     ]: Rx Status 0xf0
[16:32:31:211][  info  ][    SpecRx     ]: Number of lanes: 1
[16:32:31:211][  info  ][    SpecRx     ]: Channel 5 Lane 0 synchronized!
[16:32:31:221][  info  ][  scanConsole  ]: ... success!
[16:32:31:221][  info  ][  scanConsole  ]: Checking com 20UPGFC0009332
[16:32:31:221][  info  ][    SpecRx     ]: Active Rx channels: 0x40
[16:32:31:221][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[16:32:31:221][  info  ][    SpecRx     ]: Rx Status 0xf0
[16:32:31:221][  info  ][    SpecRx     ]: Number of lanes: 1
[16:32:31:221][  info  ][    SpecRx     ]: Channel 6 Lane 0 synchronized!
[16:32:31:231][  info  ][  scanConsole  ]: ... success!
[16:32:31:231][  info  ][  scanConsole  ]: Checking com 20UPGFC0009348
[16:32:31:231][  info  ][    SpecRx     ]: Active Rx channels: 0x80
[16:32:31:231][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[16:32:31:231][  info  ][    SpecRx     ]: Rx Status 0xf0
[16:32:31:231][  info  ][    SpecRx     ]: Number of lanes: 1
[16:32:31:231][  info  ][    SpecRx     ]: Channel 7 Lane 0 synchronized!
[16:32:31:241][  info  ][  scanConsole  ]: ... success!
[16:32:31:241][  info  ][  scanConsole  ]: Enabling Tx channels
[16:32:31:241][  info  ][  scanConsole  ]: Enabling Tx channel 1
[16:32:31:241][  info  ][  scanConsole  ]: Enabling Tx channel 1
[16:32:31:241][  info  ][  scanConsole  ]: Enabling Tx channel 1
[16:32:31:241][  info  ][  scanConsole  ]: Enabling Tx channel 1
[16:32:31:241][  info  ][  scanConsole  ]: Enabling Rx channels
[16:32:31:241][  info  ][  scanConsole  ]: Enabling Rx channel 4
[16:32:31:241][  info  ][  scanConsole  ]: Enabling Rx channel 5
[16:32:31:241][  info  ][  scanConsole  ]: Enabling Rx channel 6
[16:32:31:241][  info  ][  scanConsole  ]: Enabling Rx channel 7
[16:32:31:241][  info  ][  scanConsole  ]: ##############
[16:32:31:241][  info  ][  scanConsole  ]: # Setup Scan #
[16:32:31:241][  info  ][  scanConsole  ]: ##############
[16:32:31:242][  info  ][  scanConsole  ]: Found Scan config, constructing scan ...
[16:32:31:242][  info  ][  ScanFactory  ]: Loading Scan:
[16:32:31:242][  info  ][  ScanFactory  ]:   Name: AnalogScan
[16:32:31:242][  info  ][  ScanFactory  ]:   Number of Loops: 3
[16:32:31:242][  info  ][  ScanFactory  ]:   Loading Loop #0
[16:32:31:242][  info  ][  ScanFactory  ]:    Type: Rd53aReadRegLoop
[16:32:31:242][  info  ][  ScanFactory  ]:    Loading loop config ... 
[16:32:31:242][  info  ][  ScanFactory  ]: ~~~ {
[16:32:31:242][  info  ][  ScanFactory  ]: ~~~     "CurMux": [],
[16:32:31:242][  info  ][  ScanFactory  ]: ~~~     "EnblRingOsc": 0,
[16:32:31:242][  info  ][  ScanFactory  ]: ~~~     "Registers": [],
[16:32:31:242][  info  ][  ScanFactory  ]: ~~~     "RingOscDur": 9,
[16:32:31:242][  info  ][  ScanFactory  ]: ~~~     "RingOscRep": 10,
[16:32:31:242][  info  ][  ScanFactory  ]: ~~~     "VoltMux": [3, 5, 7, 15]
[16:32:31:242][  info  ][  ScanFactory  ]: ~~~ }
[16:32:31:242][  info  ][  ScanFactory  ]:   Loading Loop #1
[16:32:31:242][  info  ][  ScanFactory  ]:    Type: Rd53aTriggerLoop
[16:32:31:242][  info  ][  ScanFactory  ]:    Loading loop config ... 
[16:32:31:242][  info  ][  ScanFactory  ]: ~~~ {
[16:32:31:242][  info  ][  ScanFactory  ]: ~~~     "count": 1,
[16:32:31:242][  info  ][  ScanFactory  ]: ~~~     "delay": 48,
[16:32:31:242][  info  ][  ScanFactory  ]: ~~~     "extTrig": false,
[16:32:31:242][  info  ][  ScanFactory  ]: ~~~     "frequency": 30000.0,
[16:32:31:242][  info  ][  ScanFactory  ]: ~~~     "noInject": false,
[16:32:31:242][  info  ][  ScanFactory  ]: ~~~     "sendEcr": false,
[16:32:31:242][  info  ][  ScanFactory  ]: ~~~     "time": 0.0,
[16:32:31:242][  info  ][  ScanFactory  ]: ~~~     "trigMultiplier": 16
[16:32:31:242][  info  ][  ScanFactory  ]: ~~~ }
[16:32:31:242][  info  ][  ScanFactory  ]:   Loading Loop #2
[16:32:31:242][  info  ][  ScanFactory  ]:    Type: StdDataLoop
[16:32:31:242][warning ][  ScanFactory  ]: ~~~ Config empty.
[16:32:31:242][  info  ][ScanBuildHistogrammers]: Loading histogrammer ...
[16:32:31:242][  info  ][ScanBuildHistogrammers]: ... done!
[16:32:31:242][  info  ][ScanBuildAnalyses]: Loading analyses ...
[16:32:31:242][  info  ][  scanConsole  ]: Running pre scan!
[16:32:31:242][  info  ][  ScanFactory  ]: Entering pre scan phase ...
[16:32:31:242][  info  ][     Rd53a     ]: Write named register: GlobalPulseRt -> 16384
[16:32:31:242][  info  ][     Rd53a     ]: Write named register: InjEnDig -> 0
[16:32:31:242][  info  ][     Rd53a     ]: Write named register: InjVcalDiff -> 0
[16:32:31:242][  info  ][     Rd53a     ]: Write named register: InjVcalHigh -> 3500
[16:32:31:242][  info  ][     Rd53a     ]: Write named register: InjVcalMed -> 500
[16:32:31:242][  info  ][     Rd53a     ]: Write named register: LatencyConfig -> 48
[16:32:31:242][  info  ][  scanConsole  ]: Starting histogrammer and analysis threads:
[16:32:31:242][  info  ][  scanConsole  ]:  .. started threads of Fe 4
[16:32:31:242][  info  ][  scanConsole  ]:  .. started threads of Fe 5
[16:32:31:242][  info  ][  scanConsole  ]:  .. started threads of Fe 6
[16:32:31:242][  info  ][  scanConsole  ]:  .. started threads of Fe 7
[16:32:31:243][  info  ][Rd53aDataProcessor]:   -> Processor thread #0 started!
[16:32:31:243][  info  ][Rd53aDataProcessor]:   -> Processor thread #1 started!
[16:32:31:243][  info  ][Rd53aDataProcessor]:   -> Processor thread #2 started!
[16:32:31:243][  info  ][Rd53aDataProcessor]:   -> Processor thread #3 started!
[16:32:31:243][  info  ][  scanConsole  ]: ########
[16:32:31:243][  info  ][  scanConsole  ]: # Scan #
[16:32:31:243][  info  ][  scanConsole  ]: ########
[16:32:31:243][  info  ][  scanConsole  ]: Starting scan!
[16:32:31:243][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0009300 on Rx 4
[16:32:31:247][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0009300] MON MUX_V: 3 Bias 0 1382 -> 0.297552 V Bias 1 1753 -> 0.370746 V, Temperature 0.0731931 -> -21.5347 C
[16:32:31:251][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0009300] MON MUX_V: 5 Bias 0 1332 -> 0.287688 V Bias 1 1708 -> 0.361868 V, Temperature 0.0741796 -> -16.8751 C
[16:32:31:255][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0009300] MON MUX_V: 7 Bias 0 1322 -> 0.285715 V Bias 1 1702 -> 0.360684 V, Temperature 0.0749687 -> -24.3311 C
[16:32:31:258][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0009300] MON MUX_V: 15 Bias 0 1307 -> 0.282756 V Bias 1 1690 -> 0.358317 V, Temperature 0.0755606 -> -26.0162 C
[16:32:31:292][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0009316 on Rx 5
[16:32:31:296][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0009316] MON MUX_V: 3 Bias 0 1372 -> 0.294026 V Bias 1 1738 -> 0.366934 V, Temperature 0.0729072 -> -22.5243 C
[16:32:31:300][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0009316] MON MUX_V: 5 Bias 0 1347 -> 0.289046 V Bias 1 1712 -> 0.361754 V, Temperature 0.072708 -> -24.2269 C
[16:32:31:304][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0009316] MON MUX_V: 7 Bias 0 1322 -> 0.284066 V Bias 1 1701 -> 0.359563 V, Temperature 0.0754968 -> -21.8416 C
[16:32:31:308][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0009316] MON MUX_V: 15 Bias 0 1312 -> 0.282074 V Bias 1 1686 -> 0.356575 V, Temperature 0.0745008 -> -21.5042 C
[16:32:31:342][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0009332 on Rx 6
[16:32:31:345][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0009332] MON MUX_V: 3 Bias 0 1346 -> 0.294344 V Bias 1 1711 -> 0.367417 V, Temperature 0.073073 -> -25.0832 C
[16:32:31:349][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0009332] MON MUX_V: 5 Bias 0 1297 -> 0.284534 V Bias 1 1670 -> 0.359209 V, Temperature 0.0746746 -> -23.5852 C
[16:32:31:353][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0009332] MON MUX_V: 7 Bias 0 1303 -> 0.285736 V Bias 1 1676 -> 0.36041 V, Temperature 0.0746746 -> -23.5852 C
[16:32:31:357][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0009332] MON MUX_V: 15 Bias 0 1278 -> 0.28073 V Bias 1 1661 -> 0.357407 V, Temperature 0.0766766 -> -22.5678 C
[16:32:31:391][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0009348 on Rx 7
[16:32:31:395][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0009348] MON MUX_V: 3 Bias 0 1349 -> 0.290659 V Bias 1 1713 -> 0.363678 V, Temperature 0.0730184 -> -22.4515 C
[16:32:31:399][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0009348] MON MUX_V: 5 Bias 0 1307 -> 0.282234 V Bias 1 1668 -> 0.354651 V, Temperature 0.0724166 -> -22.3974 C
[16:32:31:403][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0009348] MON MUX_V: 7 Bias 0 1281 -> 0.277018 V Bias 1 1657 -> 0.352444 V, Temperature 0.0754256 -> -19.5752 C
[16:32:31:407][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0009348] MON MUX_V: 15 Bias 0 1289 -> 0.278623 V Bias 1 1668 -> 0.354651 V, Temperature 0.0760274 -> -18.3502 C
[16:32:31:442][  info  ][  scanConsole  ]: Scan done!
[16:32:31:442][  info  ][  scanConsole  ]: Waiting for processors to finish ...
[16:32:31:442][  info  ][  scanConsole  ]: Processor done, waiting for histogrammer ...
[16:32:31:842][  info  ][HistogramAlgorithm]: Histogrammer done!
[16:32:31:842][  info  ][HistogramAlgorithm]: Histogrammer done!
[16:32:31:842][  info  ][HistogramAlgorithm]: Histogrammer done!
[16:32:31:842][  info  ][HistogramAlgorithm]: Histogrammer done!
[16:32:31:842][  info  ][  scanConsole  ]: Processor done, waiting for analysis ...
[16:32:32:243][  info  ][AnalysisAlgorithm]: Analysis done!
[16:32:32:243][  info  ][AnalysisAlgorithm]: Analysis done!
[16:32:32:243][  info  ][AnalysisAlgorithm]: Analysis done!
[16:32:32:243][  info  ][AnalysisAlgorithm]: Analysis done!
[16:32:32:243][  info  ][  scanConsole  ]: All done!
[16:32:32:243][  info  ][  scanConsole  ]: ##########
[16:32:32:243][  info  ][  scanConsole  ]: # Timing #
[16:32:32:243][  info  ][  scanConsole  ]: ##########
[16:32:32:243][  info  ][  scanConsole  ]: -> Configuration: 134 ms
[16:32:32:243][  info  ][  scanConsole  ]: -> Scan:          198 ms
[16:32:32:243][  info  ][  scanConsole  ]: -> Processing:    0 ms
[16:32:32:243][  info  ][  scanConsole  ]: -> Analysis:      800 ms
[16:32:32:243][  info  ][  scanConsole  ]: ###########
[16:32:32:243][  info  ][  scanConsole  ]: # Cleanup #
[16:32:32:243][  info  ][  scanConsole  ]: ###########
[16:32:32:251][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0009300 to configs/KEKQ14/20UPGFC0009300.json
[16:32:32:418][  info  ][  scanConsole  ]: -> Storing output results of FE 4
[16:32:32:418][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0009300, this usually means that the chip did not send any data at all.
[16:32:32:419][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0009316 to configs/KEKQ14/20UPGFC0009316.json
[16:32:32:582][  info  ][  scanConsole  ]: -> Storing output results of FE 5
[16:32:32:582][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0009316, this usually means that the chip did not send any data at all.
[16:32:32:583][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0009332 to configs/KEKQ14/20UPGFC0009332.json
[16:32:32:747][  info  ][  scanConsole  ]: -> Storing output results of FE 6
[16:32:32:747][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0009332, this usually means that the chip did not send any data at all.
[16:32:32:749][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0009348 to configs/KEKQ14/20UPGFC0009348.json
[16:32:32:911][  info  ][  scanConsole  ]: -> Storing output results of FE 7
[16:32:32:911][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0009348, this usually means that the chip did not send any data at all.
[16:32:32:912][  info  ][  scanConsole  ]: Finishing run: 1784
20UPGFC0009300.json.after
20UPGFC0009300.json.before
20UPGFC0009316.json.after
20UPGFC0009316.json.before
20UPGFC0009332.json.after
20UPGFC0009332.json.before
20UPGFC0009348.json.after
20UPGFC0009348.json.before
reg_readtemp.json
scanLog.json

