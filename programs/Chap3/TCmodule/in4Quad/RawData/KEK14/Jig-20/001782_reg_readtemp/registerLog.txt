[16:12:24:404][  info  ][               ]: #####################################
[16:12:24:404][  info  ][               ]: # Welcome to the YARR Scan Console! #
[16:12:24:404][  info  ][               ]: #####################################
[16:12:24:404][  info  ][               ]: -> Parsing command line parameters ...
[16:12:24:404][  info  ][               ]: Configuring logger ...
[16:12:24:407][  info  ][  scanConsole  ]: Scan Type/Config configs/scans/rd53a/reg_readtemp.json
[16:12:24:407][  info  ][  scanConsole  ]: Connectivity:
[16:12:24:407][  info  ][  scanConsole  ]:     configs/KEKQ14/connectivity.json
[16:12:24:407][  info  ][  scanConsole  ]: Target ToT: -1
[16:12:24:407][  info  ][  scanConsole  ]: Target Charge: -1
[16:12:24:407][  info  ][  scanConsole  ]: Output Plots: true
[16:12:24:407][  info  ][  scanConsole  ]: Output Directory: ./data/001782_reg_readtemp/
[16:12:24:412][  info  ][  scanConsole  ]: Timestamp: 2022-08-24_16:12:24
[16:12:24:412][  info  ][  scanConsole  ]: Run Number: 1782
[16:12:24:412][  info  ][  scanConsole  ]: #################
[16:12:24:412][  info  ][  scanConsole  ]: # Init Hardware #
[16:12:24:412][  info  ][  scanConsole  ]: #################
[16:12:24:412][  info  ][  scanConsole  ]: -> Opening controller config: configs/controller/specCfg-rd53a-16x1.json
[16:12:24:413][  info  ][  ScanHelper   ]: Loading controller ...
[16:12:24:413][  info  ][  ScanHelper   ]: Found controller of type: spec
[16:12:24:413][  info  ][  ScanHelper   ]: ... loading controler config:
[16:12:24:413][  info  ][  ScanHelper   ]: ~~~ {
[16:12:24:413][  info  ][  ScanHelper   ]: ~~~     "cmdPeriod": 6.250000073038109e-9,
[16:12:24:413][  info  ][  ScanHelper   ]: ~~~     "idle": {
[16:12:24:413][  info  ][  ScanHelper   ]: ~~~         "word": 1768515945
[16:12:24:413][  info  ][  ScanHelper   ]: ~~~     },
[16:12:24:413][  info  ][  ScanHelper   ]: ~~~     "pulse": {
[16:12:24:413][  info  ][  ScanHelper   ]: ~~~         "interval": 500,
[16:12:24:413][  info  ][  ScanHelper   ]: ~~~         "word": 1549575846
[16:12:24:413][  info  ][  ScanHelper   ]: ~~~     },
[16:12:24:413][  info  ][  ScanHelper   ]: ~~~     "rxActiveLanes": 1,
[16:12:24:413][  info  ][  ScanHelper   ]: ~~~     "rxPolarity": 0,
[16:12:24:413][  info  ][  ScanHelper   ]: ~~~     "specNum": 0,
[16:12:24:413][  info  ][  ScanHelper   ]: ~~~     "spiConfig": 541200,
[16:12:24:413][  info  ][  ScanHelper   ]: ~~~     "sync": {
[16:12:24:413][  info  ][  ScanHelper   ]: ~~~         "interval": 32,
[16:12:24:413][  info  ][  ScanHelper   ]: ~~~         "word": 2172551550
[16:12:24:413][  info  ][  ScanHelper   ]: ~~~     },
[16:12:24:413][  info  ][  ScanHelper   ]: ~~~     "txPolarity": 0
[16:12:24:413][  info  ][  ScanHelper   ]: ~~~ }
[16:12:24:413][  info  ][    SpecCom    ]: Opening SPEC with id #0
[16:12:24:413][  info  ][    SpecCom    ]: Mapping BARs ...
[16:12:24:413][  info  ][    SpecCom    ]: ... Mapped BAR0 at 0x7fb35a617000 with size 1048576
[16:12:24:413][warning ][    SpecCom    ]: ... BAR4 not mapped (Mmap failed)
[16:12:24:413][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[16:12:24:413][  info  ][    SpecCom    ]: Firmware Version: 0x4d9ff6d
[16:12:24:413][  info  ][    SpecCom    ]: Firmware Identifier: 0x4030232
[16:12:24:413][  info  ][    SpecCom    ]: FPGA card: PLDA XpressK7 325
[16:12:24:413][  info  ][    SpecCom    ]: FE Chip Type: RD53A/B
[16:12:24:413][  info  ][    SpecCom    ]: FMC Card Type: Ohio Card (Display Port)
[16:12:24:413][  info  ][    SpecCom    ]: RX Speed: 640Mbps
[16:12:24:413][  info  ][    SpecCom    ]: Channel Configuration: 16x1
[16:12:24:413][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[16:12:24:413][  info  ][    SpecCom    ]: Flushing buffers ...
[16:12:24:413][  info  ][    SpecCom    ]: Init success!
[16:12:24:413][  info  ][  scanConsole  ]: #######################
[16:12:24:413][  info  ][  scanConsole  ]: ##  Loading Configs  ##
[16:12:24:413][  info  ][  scanConsole  ]: #######################
[16:12:24:413][  info  ][  scanConsole  ]: Opening global config: configs/KEKQ14/connectivity.json
[16:12:24:413][  info  ][  ScanHelper   ]: Chip type: RD53A
[16:12:24:413][  info  ][  ScanHelper   ]: Chip count 4
[16:12:24:413][  info  ][  ScanHelper   ]: Loading chip #0
[16:12:24:414][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(4)
[16:12:24:414][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ14/20UPGFC0009300.json
[16:12:24:588][  info  ][  ScanHelper   ]: Loading chip #1
[16:12:24:589][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(5)
[16:12:24:589][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ14/20UPGFC0009316.json
[16:12:24:766][  info  ][  ScanHelper   ]: Loading chip #2
[16:12:24:766][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(6)
[16:12:24:766][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ14/20UPGFC0009332.json
[16:12:24:943][  info  ][  ScanHelper   ]: Loading chip #3
[16:12:24:943][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(7)
[16:12:24:944][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ14/20UPGFC0009348.json
[16:12:25:123][  info  ][  scanConsole  ]: #################
[16:12:25:123][  info  ][  scanConsole  ]: # Configure FEs #
[16:12:25:123][  info  ][  scanConsole  ]: #################
[16:12:25:123][  info  ][  scanConsole  ]: Configuring 20UPGFC0009300
[16:12:25:154][  info  ][  scanConsole  ]: Configuring 20UPGFC0009316
[16:12:25:184][  info  ][  scanConsole  ]: Configuring 20UPGFC0009332
[16:12:25:214][  info  ][  scanConsole  ]: Configuring 20UPGFC0009348
[16:12:25:244][  info  ][  scanConsole  ]: Sent configuration to all FEs in 121 ms!
[16:12:25:245][  info  ][  scanConsole  ]: Checking com 20UPGFC0009300
[16:12:25:245][  info  ][    SpecRx     ]: Active Rx channels: 0x10
[16:12:25:245][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[16:12:25:245][  info  ][    SpecRx     ]: Rx Status 0xf0
[16:12:25:245][  info  ][    SpecRx     ]: Number of lanes: 1
[16:12:25:245][  info  ][    SpecRx     ]: Channel 4 Lane 0 synchronized!
[16:12:25:256][  info  ][  scanConsole  ]: ... success!
[16:12:25:256][  info  ][  scanConsole  ]: Checking com 20UPGFC0009316
[16:12:25:256][  info  ][    SpecRx     ]: Active Rx channels: 0x20
[16:12:25:256][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[16:12:25:256][  info  ][    SpecRx     ]: Rx Status 0xf0
[16:12:25:256][  info  ][    SpecRx     ]: Number of lanes: 1
[16:12:25:256][  info  ][    SpecRx     ]: Channel 5 Lane 0 synchronized!
[16:12:25:266][  info  ][  scanConsole  ]: ... success!
[16:12:25:266][  info  ][  scanConsole  ]: Checking com 20UPGFC0009332
[16:12:25:266][  info  ][    SpecRx     ]: Active Rx channels: 0x40
[16:12:25:266][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[16:12:25:266][  info  ][    SpecRx     ]: Rx Status 0xf0
[16:12:25:266][  info  ][    SpecRx     ]: Number of lanes: 1
[16:12:25:266][  info  ][    SpecRx     ]: Channel 6 Lane 0 synchronized!
[16:12:25:276][  info  ][  scanConsole  ]: ... success!
[16:12:25:277][  info  ][  scanConsole  ]: Checking com 20UPGFC0009348
[16:12:25:277][  info  ][    SpecRx     ]: Active Rx channels: 0x80
[16:12:25:277][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[16:12:25:277][  info  ][    SpecRx     ]: Rx Status 0xf0
[16:12:25:277][  info  ][    SpecRx     ]: Number of lanes: 1
[16:12:25:277][  info  ][    SpecRx     ]: Channel 7 Lane 0 synchronized!
[16:12:25:287][  info  ][  scanConsole  ]: ... success!
[16:12:25:287][  info  ][  scanConsole  ]: Enabling Tx channels
[16:12:25:287][  info  ][  scanConsole  ]: Enabling Tx channel 1
[16:12:25:287][  info  ][  scanConsole  ]: Enabling Tx channel 1
[16:12:25:287][  info  ][  scanConsole  ]: Enabling Tx channel 1
[16:12:25:287][  info  ][  scanConsole  ]: Enabling Tx channel 1
[16:12:25:287][  info  ][  scanConsole  ]: Enabling Rx channels
[16:12:25:287][  info  ][  scanConsole  ]: Enabling Rx channel 4
[16:12:25:287][  info  ][  scanConsole  ]: Enabling Rx channel 5
[16:12:25:287][  info  ][  scanConsole  ]: Enabling Rx channel 6
[16:12:25:287][  info  ][  scanConsole  ]: Enabling Rx channel 7
[16:12:25:287][  info  ][  scanConsole  ]: ##############
[16:12:25:287][  info  ][  scanConsole  ]: # Setup Scan #
[16:12:25:287][  info  ][  scanConsole  ]: ##############
[16:12:25:287][  info  ][  scanConsole  ]: Found Scan config, constructing scan ...
[16:12:25:287][  info  ][  ScanFactory  ]: Loading Scan:
[16:12:25:287][  info  ][  ScanFactory  ]:   Name: AnalogScan
[16:12:25:287][  info  ][  ScanFactory  ]:   Number of Loops: 3
[16:12:25:287][  info  ][  ScanFactory  ]:   Loading Loop #0
[16:12:25:287][  info  ][  ScanFactory  ]:    Type: Rd53aReadRegLoop
[16:12:25:287][  info  ][  ScanFactory  ]:    Loading loop config ... 
[16:12:25:287][  info  ][  ScanFactory  ]: ~~~ {
[16:12:25:287][  info  ][  ScanFactory  ]: ~~~     "CurMux": [],
[16:12:25:287][  info  ][  ScanFactory  ]: ~~~     "EnblRingOsc": 0,
[16:12:25:287][  info  ][  ScanFactory  ]: ~~~     "Registers": [],
[16:12:25:287][  info  ][  ScanFactory  ]: ~~~     "RingOscDur": 9,
[16:12:25:287][  info  ][  ScanFactory  ]: ~~~     "RingOscRep": 10,
[16:12:25:287][  info  ][  ScanFactory  ]: ~~~     "VoltMux": [3, 5, 7, 15]
[16:12:25:287][  info  ][  ScanFactory  ]: ~~~ }
[16:12:25:287][  info  ][  ScanFactory  ]:   Loading Loop #1
[16:12:25:287][  info  ][  ScanFactory  ]:    Type: Rd53aTriggerLoop
[16:12:25:287][  info  ][  ScanFactory  ]:    Loading loop config ... 
[16:12:25:287][  info  ][  ScanFactory  ]: ~~~ {
[16:12:25:287][  info  ][  ScanFactory  ]: ~~~     "count": 1,
[16:12:25:287][  info  ][  ScanFactory  ]: ~~~     "delay": 48,
[16:12:25:287][  info  ][  ScanFactory  ]: ~~~     "extTrig": false,
[16:12:25:287][  info  ][  ScanFactory  ]: ~~~     "frequency": 30000.0,
[16:12:25:287][  info  ][  ScanFactory  ]: ~~~     "noInject": false,
[16:12:25:287][  info  ][  ScanFactory  ]: ~~~     "sendEcr": false,
[16:12:25:287][  info  ][  ScanFactory  ]: ~~~     "time": 0.0,
[16:12:25:287][  info  ][  ScanFactory  ]: ~~~     "trigMultiplier": 16
[16:12:25:287][  info  ][  ScanFactory  ]: ~~~ }
[16:12:25:287][  info  ][  ScanFactory  ]:   Loading Loop #2
[16:12:25:288][  info  ][  ScanFactory  ]:    Type: StdDataLoop
[16:12:25:288][warning ][  ScanFactory  ]: ~~~ Config empty.
[16:12:25:288][  info  ][ScanBuildHistogrammers]: Loading histogrammer ...
[16:12:25:288][  info  ][ScanBuildHistogrammers]: ... done!
[16:12:25:288][  info  ][ScanBuildAnalyses]: Loading analyses ...
[16:12:25:288][  info  ][  scanConsole  ]: Running pre scan!
[16:12:25:288][  info  ][  ScanFactory  ]: Entering pre scan phase ...
[16:12:25:288][  info  ][     Rd53a     ]: Write named register: GlobalPulseRt -> 16384
[16:12:25:288][  info  ][     Rd53a     ]: Write named register: InjEnDig -> 0
[16:12:25:288][  info  ][     Rd53a     ]: Write named register: InjVcalDiff -> 0
[16:12:25:288][  info  ][     Rd53a     ]: Write named register: InjVcalHigh -> 3500
[16:12:25:288][  info  ][     Rd53a     ]: Write named register: InjVcalMed -> 500
[16:12:25:288][  info  ][     Rd53a     ]: Write named register: LatencyConfig -> 48
[16:12:25:288][  info  ][  scanConsole  ]: Starting histogrammer and analysis threads:
[16:12:25:288][  info  ][  scanConsole  ]:  .. started threads of Fe 4
[16:12:25:288][  info  ][  scanConsole  ]:  .. started threads of Fe 5
[16:12:25:288][  info  ][  scanConsole  ]:  .. started threads of Fe 6
[16:12:25:288][  info  ][  scanConsole  ]:  .. started threads of Fe 7
[16:12:25:288][  info  ][Rd53aDataProcessor]:   -> Processor thread #0 started!
[16:12:25:288][  info  ][Rd53aDataProcessor]:   -> Processor thread #1 started!
[16:12:25:288][  info  ][Rd53aDataProcessor]:   -> Processor thread #2 started!
[16:12:25:288][  info  ][Rd53aDataProcessor]:   -> Processor thread #3 started!
[16:12:25:288][  info  ][  scanConsole  ]: ########
[16:12:25:288][  info  ][  scanConsole  ]: # Scan #
[16:12:25:288][  info  ][  scanConsole  ]: ########
[16:12:25:288][  info  ][  scanConsole  ]: Starting scan!
[16:12:25:288][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0009300 on Rx 4
[16:12:25:292][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0009300] MON MUX_V: 3 Bias 0 1356 -> 0.292423 V Bias 1 1734 -> 0.366997 V, Temperature 0.0745741 -> -16.7872 C
[16:12:25:297][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0009300] MON MUX_V: 5 Bias 0 1306 -> 0.282559 V Bias 1 1688 -> 0.357922 V, Temperature 0.0753633 -> -12.7857 C
[16:12:25:301][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0009300] MON MUX_V: 7 Bias 0 1296 -> 0.280586 V Bias 1 1683 -> 0.356936 V, Temperature 0.0763497 -> -19.7475 C
[16:12:25:305][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0009300] MON MUX_V: 15 Bias 0 1281 -> 0.277627 V Bias 1 1670 -> 0.354371 V, Temperature 0.0767443 -> -22.1447 C
[16:12:25:341][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0009316 on Rx 5
[16:12:25:344][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0009316] MON MUX_V: 3 Bias 0 1346 -> 0.288847 V Bias 1 1719 -> 0.363149 V, Temperature 0.0743016 -> -17.7308 C
[16:12:25:349][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0009316] MON MUX_V: 5 Bias 0 1320 -> 0.283668 V Bias 1 1693 -> 0.35797 V, Temperature 0.0743016 -> -18.771 C
[16:12:25:353][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0009316] MON MUX_V: 7 Bias 0 1295 -> 0.278688 V Bias 1 1683 -> 0.355978 V, Temperature 0.0772896 -> -15.8738 C
[16:12:25:357][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0009316] MON MUX_V: 15 Bias 0 1286 -> 0.276895 V Bias 1 1667 -> 0.35279 V, Temperature 0.0758952 -> -16.7942 C
[16:12:25:392][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0009332 on Rx 6
[16:12:25:396][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0009332] MON MUX_V: 3 Bias 0 1321 -> 0.289339 V Bias 1 1694 -> 0.364014 V, Temperature 0.0746746 -> -19.6461 C
[16:12:25:400][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0009332] MON MUX_V: 5 Bias 0 1272 -> 0.279529 V Bias 1 1653 -> 0.355806 V, Temperature 0.0762762 -> -18.2326 C
[16:12:25:404][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0009332] MON MUX_V: 7 Bias 0 1277 -> 0.28053 V Bias 1 1659 -> 0.357007 V, Temperature 0.0764764 -> -17.5635 C
[16:12:25:408][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0009332] MON MUX_V: 15 Bias 0 1252 -> 0.275525 V Bias 1 1641 -> 0.353403 V, Temperature 0.0778778 -> -18.6422 C
[16:12:25:443][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0009348 on Rx 7
[16:12:25:447][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0009348] MON MUX_V: 3 Bias 0 1324 -> 0.285644 V Bias 1 1695 -> 0.360067 V, Temperature 0.0744226 -> -17.6304 C
[16:12:25:451][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0009348] MON MUX_V: 5 Bias 0 1279 -> 0.276617 V Bias 1 1650 -> 0.35104 V, Temperature 0.0744226 -> -15.4513 C
[16:12:25:455][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0009348] MON MUX_V: 7 Bias 0 1255 -> 0.271803 V Bias 1 1638 -> 0.348633 V, Temperature 0.0768298 -> -14.8543 C
[16:12:25:459][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0009348] MON MUX_V: 15 Bias 0 1263 -> 0.273407 V Bias 1 1650 -> 0.35104 V, Temperature 0.0776322 -> -12.9718 C
[16:12:25:495][  info  ][  scanConsole  ]: Scan done!
[16:12:25:495][  info  ][  scanConsole  ]: Waiting for processors to finish ...
[16:12:25:496][  info  ][  scanConsole  ]: Processor done, waiting for histogrammer ...
[16:12:25:896][  info  ][HistogramAlgorithm]: Histogrammer done!
[16:12:25:896][  info  ][HistogramAlgorithm]: Histogrammer done!
[16:12:25:896][  info  ][HistogramAlgorithm]: Histogrammer done!
[16:12:25:896][  info  ][HistogramAlgorithm]: Histogrammer done!
[16:12:25:896][  info  ][  scanConsole  ]: Processor done, waiting for analysis ...
[16:12:26:296][  info  ][AnalysisAlgorithm]: Analysis done!
[16:12:26:296][  info  ][AnalysisAlgorithm]: Analysis done!
[16:12:26:296][  info  ][AnalysisAlgorithm]: Analysis done!
[16:12:26:296][  info  ][AnalysisAlgorithm]: Analysis done!
[16:12:26:296][  info  ][  scanConsole  ]: All done!
[16:12:26:296][  info  ][  scanConsole  ]: ##########
[16:12:26:297][  info  ][  scanConsole  ]: # Timing #
[16:12:26:297][  info  ][  scanConsole  ]: ##########
[16:12:26:297][  info  ][  scanConsole  ]: -> Configuration: 121 ms
[16:12:26:297][  info  ][  scanConsole  ]: -> Scan:          206 ms
[16:12:26:297][  info  ][  scanConsole  ]: -> Processing:    0 ms
[16:12:26:297][  info  ][  scanConsole  ]: -> Analysis:      800 ms
[16:12:26:297][  info  ][  scanConsole  ]: ###########
[16:12:26:297][  info  ][  scanConsole  ]: # Cleanup #
[16:12:26:297][  info  ][  scanConsole  ]: ###########
[16:12:26:299][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0009300 to configs/KEKQ14/20UPGFC0009300.json
[16:12:26:488][  info  ][  scanConsole  ]: -> Storing output results of FE 4
[16:12:26:488][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0009300, this usually means that the chip did not send any data at all.
[16:12:26:489][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0009316 to configs/KEKQ14/20UPGFC0009316.json
[16:12:26:657][  info  ][  scanConsole  ]: -> Storing output results of FE 5
[16:12:26:657][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0009316, this usually means that the chip did not send any data at all.
[16:12:26:658][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0009332 to configs/KEKQ14/20UPGFC0009332.json
[16:12:26:822][  info  ][  scanConsole  ]: -> Storing output results of FE 6
[16:12:26:822][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0009332, this usually means that the chip did not send any data at all.
[16:12:26:823][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0009348 to configs/KEKQ14/20UPGFC0009348.json
[16:12:26:985][  info  ][  scanConsole  ]: -> Storing output results of FE 7
[16:12:26:985][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0009348, this usually means that the chip did not send any data at all.
[16:12:26:987][  info  ][  scanConsole  ]: Finishing run: 1782
20UPGFC0009300.json.after
20UPGFC0009300.json.before
20UPGFC0009316.json.after
20UPGFC0009316.json.before
20UPGFC0009332.json.after
20UPGFC0009332.json.before
20UPGFC0009348.json.after
20UPGFC0009348.json.before
reg_readtemp.json
scanLog.json

