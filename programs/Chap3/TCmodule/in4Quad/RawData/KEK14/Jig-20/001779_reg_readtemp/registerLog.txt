[16:02:37:368][  info  ][               ]: #####################################
[16:02:37:368][  info  ][               ]: # Welcome to the YARR Scan Console! #
[16:02:37:368][  info  ][               ]: #####################################
[16:02:37:368][  info  ][               ]: -> Parsing command line parameters ...
[16:02:37:368][  info  ][               ]: Configuring logger ...
[16:02:37:370][  info  ][  scanConsole  ]: Scan Type/Config configs/scans/rd53a/reg_readtemp.json
[16:02:37:370][  info  ][  scanConsole  ]: Connectivity:
[16:02:37:370][  info  ][  scanConsole  ]:     configs/KEKQ14/connectivity.json
[16:02:37:370][  info  ][  scanConsole  ]: Target ToT: -1
[16:02:37:370][  info  ][  scanConsole  ]: Target Charge: -1
[16:02:37:370][  info  ][  scanConsole  ]: Output Plots: true
[16:02:37:370][  info  ][  scanConsole  ]: Output Directory: ./data/001779_reg_readtemp/
[16:02:37:375][  info  ][  scanConsole  ]: Timestamp: 2022-08-24_16:02:37
[16:02:37:375][  info  ][  scanConsole  ]: Run Number: 1779
[16:02:37:375][  info  ][  scanConsole  ]: #################
[16:02:37:375][  info  ][  scanConsole  ]: # Init Hardware #
[16:02:37:375][  info  ][  scanConsole  ]: #################
[16:02:37:375][  info  ][  scanConsole  ]: -> Opening controller config: configs/controller/specCfg-rd53a-16x1.json
[16:02:37:375][  info  ][  ScanHelper   ]: Loading controller ...
[16:02:37:375][  info  ][  ScanHelper   ]: Found controller of type: spec
[16:02:37:375][  info  ][  ScanHelper   ]: ... loading controler config:
[16:02:37:375][  info  ][  ScanHelper   ]: ~~~ {
[16:02:37:375][  info  ][  ScanHelper   ]: ~~~     "cmdPeriod": 6.250000073038109e-9,
[16:02:37:375][  info  ][  ScanHelper   ]: ~~~     "idle": {
[16:02:37:375][  info  ][  ScanHelper   ]: ~~~         "word": 1768515945
[16:02:37:375][  info  ][  ScanHelper   ]: ~~~     },
[16:02:37:375][  info  ][  ScanHelper   ]: ~~~     "pulse": {
[16:02:37:375][  info  ][  ScanHelper   ]: ~~~         "interval": 500,
[16:02:37:375][  info  ][  ScanHelper   ]: ~~~         "word": 1549575846
[16:02:37:375][  info  ][  ScanHelper   ]: ~~~     },
[16:02:37:375][  info  ][  ScanHelper   ]: ~~~     "rxActiveLanes": 1,
[16:02:37:375][  info  ][  ScanHelper   ]: ~~~     "rxPolarity": 0,
[16:02:37:375][  info  ][  ScanHelper   ]: ~~~     "specNum": 0,
[16:02:37:375][  info  ][  ScanHelper   ]: ~~~     "spiConfig": 541200,
[16:02:37:375][  info  ][  ScanHelper   ]: ~~~     "sync": {
[16:02:37:375][  info  ][  ScanHelper   ]: ~~~         "interval": 32,
[16:02:37:375][  info  ][  ScanHelper   ]: ~~~         "word": 2172551550
[16:02:37:375][  info  ][  ScanHelper   ]: ~~~     },
[16:02:37:375][  info  ][  ScanHelper   ]: ~~~     "txPolarity": 0
[16:02:37:375][  info  ][  ScanHelper   ]: ~~~ }
[16:02:37:375][  info  ][    SpecCom    ]: Opening SPEC with id #0
[16:02:37:375][  info  ][    SpecCom    ]: Mapping BARs ...
[16:02:37:375][  info  ][    SpecCom    ]: ... Mapped BAR0 at 0x7fb093ab8000 with size 1048576
[16:02:37:375][warning ][    SpecCom    ]: ... BAR4 not mapped (Mmap failed)
[16:02:37:375][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[16:02:37:375][  info  ][    SpecCom    ]: Firmware Version: 0x4d9ff6d
[16:02:37:375][  info  ][    SpecCom    ]: Firmware Identifier: 0x4030232
[16:02:37:375][  info  ][    SpecCom    ]: FPGA card: PLDA XpressK7 325
[16:02:37:375][  info  ][    SpecCom    ]: FE Chip Type: RD53A/B
[16:02:37:375][  info  ][    SpecCom    ]: FMC Card Type: Ohio Card (Display Port)
[16:02:37:375][  info  ][    SpecCom    ]: RX Speed: 640Mbps
[16:02:37:375][  info  ][    SpecCom    ]: Channel Configuration: 16x1
[16:02:37:375][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[16:02:37:375][  info  ][    SpecCom    ]: Flushing buffers ...
[16:02:37:375][  info  ][    SpecCom    ]: Init success!
[16:02:37:375][  info  ][  scanConsole  ]: #######################
[16:02:37:375][  info  ][  scanConsole  ]: ##  Loading Configs  ##
[16:02:37:375][  info  ][  scanConsole  ]: #######################
[16:02:37:375][  info  ][  scanConsole  ]: Opening global config: configs/KEKQ14/connectivity.json
[16:02:37:375][  info  ][  ScanHelper   ]: Chip type: RD53A
[16:02:37:375][  info  ][  ScanHelper   ]: Chip count 4
[16:02:37:375][  info  ][  ScanHelper   ]: Loading chip #0
[16:02:37:376][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(4)
[16:02:37:376][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ14/20UPGFC0009300.json
[16:02:37:556][  info  ][  ScanHelper   ]: Loading chip #1
[16:02:37:556][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(5)
[16:02:37:556][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ14/20UPGFC0009316.json
[16:02:37:729][  info  ][  ScanHelper   ]: Loading chip #2
[16:02:37:729][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(6)
[16:02:37:729][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ14/20UPGFC0009332.json
[16:02:37:904][  info  ][  ScanHelper   ]: Loading chip #3
[16:02:37:904][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(7)
[16:02:37:904][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ14/20UPGFC0009348.json
[16:02:38:085][  info  ][  scanConsole  ]: #################
[16:02:38:085][  info  ][  scanConsole  ]: # Configure FEs #
[16:02:38:085][  info  ][  scanConsole  ]: #################
[16:02:38:085][  info  ][  scanConsole  ]: Configuring 20UPGFC0009300
[16:02:38:115][  info  ][  scanConsole  ]: Configuring 20UPGFC0009316
[16:02:38:158][  info  ][  scanConsole  ]: Configuring 20UPGFC0009332
[16:02:38:193][  info  ][  scanConsole  ]: Configuring 20UPGFC0009348
[16:02:38:228][  info  ][  scanConsole  ]: Sent configuration to all FEs in 143 ms!
[16:02:38:229][  info  ][  scanConsole  ]: Checking com 20UPGFC0009300
[16:02:38:229][  info  ][    SpecRx     ]: Active Rx channels: 0x10
[16:02:38:229][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[16:02:38:229][  info  ][    SpecRx     ]: Rx Status 0xf0
[16:02:38:229][  info  ][    SpecRx     ]: Number of lanes: 1
[16:02:38:229][  info  ][    SpecRx     ]: Channel 4 Lane 0 synchronized!
[16:02:38:240][  info  ][  scanConsole  ]: ... success!
[16:02:38:240][  info  ][  scanConsole  ]: Checking com 20UPGFC0009316
[16:02:38:240][  info  ][    SpecRx     ]: Active Rx channels: 0x20
[16:02:38:240][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[16:02:38:240][  info  ][    SpecRx     ]: Rx Status 0xf0
[16:02:38:240][  info  ][    SpecRx     ]: Number of lanes: 1
[16:02:38:240][  info  ][    SpecRx     ]: Channel 5 Lane 0 synchronized!
[16:02:38:250][  info  ][  scanConsole  ]: ... success!
[16:02:38:250][  info  ][  scanConsole  ]: Checking com 20UPGFC0009332
[16:02:38:250][  info  ][    SpecRx     ]: Active Rx channels: 0x40
[16:02:38:250][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[16:02:38:250][  info  ][    SpecRx     ]: Rx Status 0xf0
[16:02:38:250][  info  ][    SpecRx     ]: Number of lanes: 1
[16:02:38:250][  info  ][    SpecRx     ]: Channel 6 Lane 0 synchronized!
[16:02:38:261][  info  ][  scanConsole  ]: ... success!
[16:02:38:261][  info  ][  scanConsole  ]: Checking com 20UPGFC0009348
[16:02:38:261][  info  ][    SpecRx     ]: Active Rx channels: 0x80
[16:02:38:261][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[16:02:38:261][  info  ][    SpecRx     ]: Rx Status 0xf0
[16:02:38:261][  info  ][    SpecRx     ]: Number of lanes: 1
[16:02:38:261][  info  ][    SpecRx     ]: Channel 7 Lane 0 synchronized!
[16:02:38:272][  info  ][  scanConsole  ]: ... success!
[16:02:38:272][  info  ][  scanConsole  ]: Enabling Tx channels
[16:02:38:272][  info  ][  scanConsole  ]: Enabling Tx channel 1
[16:02:38:272][  info  ][  scanConsole  ]: Enabling Tx channel 1
[16:02:38:272][  info  ][  scanConsole  ]: Enabling Tx channel 1
[16:02:38:272][  info  ][  scanConsole  ]: Enabling Tx channel 1
[16:02:38:272][  info  ][  scanConsole  ]: Enabling Rx channels
[16:02:38:272][  info  ][  scanConsole  ]: Enabling Rx channel 4
[16:02:38:272][  info  ][  scanConsole  ]: Enabling Rx channel 5
[16:02:38:272][  info  ][  scanConsole  ]: Enabling Rx channel 6
[16:02:38:272][  info  ][  scanConsole  ]: Enabling Rx channel 7
[16:02:38:272][  info  ][  scanConsole  ]: ##############
[16:02:38:272][  info  ][  scanConsole  ]: # Setup Scan #
[16:02:38:272][  info  ][  scanConsole  ]: ##############
[16:02:38:272][  info  ][  scanConsole  ]: Found Scan config, constructing scan ...
[16:02:38:273][  info  ][  ScanFactory  ]: Loading Scan:
[16:02:38:273][  info  ][  ScanFactory  ]:   Name: AnalogScan
[16:02:38:273][  info  ][  ScanFactory  ]:   Number of Loops: 3
[16:02:38:273][  info  ][  ScanFactory  ]:   Loading Loop #0
[16:02:38:273][  info  ][  ScanFactory  ]:    Type: Rd53aReadRegLoop
[16:02:38:273][  info  ][  ScanFactory  ]:    Loading loop config ... 
[16:02:38:273][  info  ][  ScanFactory  ]: ~~~ {
[16:02:38:273][  info  ][  ScanFactory  ]: ~~~     "CurMux": [],
[16:02:38:273][  info  ][  ScanFactory  ]: ~~~     "EnblRingOsc": 0,
[16:02:38:273][  info  ][  ScanFactory  ]: ~~~     "Registers": [],
[16:02:38:273][  info  ][  ScanFactory  ]: ~~~     "RingOscDur": 9,
[16:02:38:273][  info  ][  ScanFactory  ]: ~~~     "RingOscRep": 10,
[16:02:38:273][  info  ][  ScanFactory  ]: ~~~     "VoltMux": [3, 5, 7, 15]
[16:02:38:273][  info  ][  ScanFactory  ]: ~~~ }
[16:02:38:273][  info  ][  ScanFactory  ]:   Loading Loop #1
[16:02:38:273][  info  ][  ScanFactory  ]:    Type: Rd53aTriggerLoop
[16:02:38:273][  info  ][  ScanFactory  ]:    Loading loop config ... 
[16:02:38:273][  info  ][  ScanFactory  ]: ~~~ {
[16:02:38:273][  info  ][  ScanFactory  ]: ~~~     "count": 1,
[16:02:38:273][  info  ][  ScanFactory  ]: ~~~     "delay": 48,
[16:02:38:274][  info  ][  ScanFactory  ]: ~~~     "extTrig": false,
[16:02:38:274][  info  ][  ScanFactory  ]: ~~~     "frequency": 30000.0,
[16:02:38:274][  info  ][  ScanFactory  ]: ~~~     "noInject": false,
[16:02:38:274][  info  ][  ScanFactory  ]: ~~~     "sendEcr": false,
[16:02:38:274][  info  ][  ScanFactory  ]: ~~~     "time": 0.0,
[16:02:38:274][  info  ][  ScanFactory  ]: ~~~     "trigMultiplier": 16
[16:02:38:274][  info  ][  ScanFactory  ]: ~~~ }
[16:02:38:274][  info  ][  ScanFactory  ]:   Loading Loop #2
[16:02:38:274][  info  ][  ScanFactory  ]:    Type: StdDataLoop
[16:02:38:274][warning ][  ScanFactory  ]: ~~~ Config empty.
[16:02:38:274][  info  ][ScanBuildHistogrammers]: Loading histogrammer ...
[16:02:38:274][  info  ][ScanBuildHistogrammers]: ... done!
[16:02:38:274][  info  ][ScanBuildAnalyses]: Loading analyses ...
[16:02:38:274][  info  ][  scanConsole  ]: Running pre scan!
[16:02:38:274][  info  ][  ScanFactory  ]: Entering pre scan phase ...
[16:02:38:275][  info  ][     Rd53a     ]: Write named register: GlobalPulseRt -> 16384
[16:02:38:275][  info  ][     Rd53a     ]: Write named register: InjEnDig -> 0
[16:02:38:275][  info  ][     Rd53a     ]: Write named register: InjVcalDiff -> 0
[16:02:38:275][  info  ][     Rd53a     ]: Write named register: InjVcalHigh -> 3500
[16:02:38:275][  info  ][     Rd53a     ]: Write named register: InjVcalMed -> 500
[16:02:38:275][  info  ][     Rd53a     ]: Write named register: LatencyConfig -> 48
[16:02:38:275][  info  ][  scanConsole  ]: Starting histogrammer and analysis threads:
[16:02:38:275][  info  ][  scanConsole  ]:  .. started threads of Fe 4
[16:02:38:275][  info  ][  scanConsole  ]:  .. started threads of Fe 5
[16:02:38:275][  info  ][  scanConsole  ]:  .. started threads of Fe 6
[16:02:38:275][  info  ][  scanConsole  ]:  .. started threads of Fe 7
[16:02:38:276][  info  ][Rd53aDataProcessor]:   -> Processor thread #0 started!
[16:02:38:276][  info  ][Rd53aDataProcessor]:   -> Processor thread #1 started!
[16:02:38:276][  info  ][Rd53aDataProcessor]:   -> Processor thread #2 started!
[16:02:38:276][  info  ][Rd53aDataProcessor]:   -> Processor thread #3 started!
[16:02:38:276][  info  ][  scanConsole  ]: ########
[16:02:38:276][  info  ][  scanConsole  ]: # Scan #
[16:02:38:276][  info  ][  scanConsole  ]: ########
[16:02:38:276][  info  ][  scanConsole  ]: Starting scan!
[16:02:38:276][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0009300 on Rx 4
[16:02:38:280][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0009300] MON MUX_V: 3 Bias 0 1354 -> 0.292028 V Bias 1 1732 -> 0.366603 V, Temperature 0.0745741 -> -16.7872 C
[16:02:38:284][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0009300] MON MUX_V: 5 Bias 0 1303 -> 0.281967 V Bias 1 1687 -> 0.357725 V, Temperature 0.0757579 -> -11.4225 C
[16:02:38:288][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0009300] MON MUX_V: 7 Bias 0 1294 -> 0.280191 V Bias 1 1681 -> 0.356541 V, Temperature 0.0763497 -> -19.7475 C
[16:02:38:293][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0009300] MON MUX_V: 15 Bias 0 1278 -> 0.277035 V Bias 1 1668 -> 0.353976 V, Temperature 0.0769416 -> -21.4995 C
[16:02:38:327][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0009316 on Rx 5
[16:02:38:332][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0009316] MON MUX_V: 3 Bias 0 1346 -> 0.288847 V Bias 1 1718 -> 0.36295 V, Temperature 0.0741024 -> -18.4156 C
[16:02:38:336][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0009316] MON MUX_V: 5 Bias 0 1319 -> 0.283469 V Bias 1 1692 -> 0.35777 V, Temperature 0.0743016 -> -18.771 C
[16:02:38:340][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0009316] MON MUX_V: 7 Bias 0 1294 -> 0.278489 V Bias 1 1682 -> 0.355778 V, Temperature 0.0772896 -> -15.8737 C
[16:02:38:344][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0009316] MON MUX_V: 15 Bias 0 1286 -> 0.276895 V Bias 1 1665 -> 0.352392 V, Temperature 0.0754968 -> -18.14 C
[16:02:38:379][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0009332 on Rx 6
[16:02:38:383][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0009332] MON MUX_V: 3 Bias 0 1320 -> 0.289139 V Bias 1 1694 -> 0.364014 V, Temperature 0.0748748 -> -18.9665 C
[16:02:38:387][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0009332] MON MUX_V: 5 Bias 0 1271 -> 0.279329 V Bias 1 1652 -> 0.355605 V, Temperature 0.0762762 -> -18.2326 C
[16:02:38:392][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0009332] MON MUX_V: 7 Bias 0 1277 -> 0.28053 V Bias 1 1657 -> 0.356606 V, Temperature 0.076076 -> -18.9016 C
[16:02:38:396][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0009332] MON MUX_V: 15 Bias 0 1250 -> 0.275125 V Bias 1 1641 -> 0.353403 V, Temperature 0.0782782 -> -17.3337 C
[16:02:38:431][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0009348 on Rx 7
[16:02:38:435][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0009348] MON MUX_V: 3 Bias 0 1323 -> 0.285444 V Bias 1 1694 -> 0.359866 V, Temperature 0.0744226 -> -17.6304 C
[16:02:38:439][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0009348] MON MUX_V: 5 Bias 0 1279 -> 0.276617 V Bias 1 1648 -> 0.350639 V, Temperature 0.0740214 -> -16.8405 C
[16:02:38:444][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0009348] MON MUX_V: 7 Bias 0 1254 -> 0.271602 V Bias 1 1637 -> 0.348432 V, Temperature 0.0768298 -> -14.8544 C
[16:02:38:448][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0009348] MON MUX_V: 15 Bias 0 1261 -> 0.273006 V Bias 1 1647 -> 0.350438 V, Temperature 0.0774316 -> -13.6442 C
[16:02:38:485][  info  ][  scanConsole  ]: Scan done!
[16:02:38:485][  info  ][  scanConsole  ]: Waiting for processors to finish ...
[16:02:38:486][  info  ][  scanConsole  ]: Processor done, waiting for histogrammer ...
[16:02:38:885][  info  ][HistogramAlgorithm]: Histogrammer done!
[16:02:38:885][  info  ][HistogramAlgorithm]: Histogrammer done!
[16:02:38:885][  info  ][HistogramAlgorithm]: Histogrammer done!
[16:02:38:885][  info  ][HistogramAlgorithm]: Histogrammer done!
[16:02:38:885][  info  ][  scanConsole  ]: Processor done, waiting for analysis ...
[16:02:39:286][  info  ][AnalysisAlgorithm]: Analysis done!
[16:02:39:286][  info  ][AnalysisAlgorithm]: Analysis done!
[16:02:39:286][  info  ][AnalysisAlgorithm]: Analysis done!
[16:02:39:286][  info  ][AnalysisAlgorithm]: Analysis done!
[16:02:39:286][  info  ][  scanConsole  ]: All done!
[16:02:39:286][  info  ][  scanConsole  ]: ##########
[16:02:39:286][  info  ][  scanConsole  ]: # Timing #
[16:02:39:286][  info  ][  scanConsole  ]: ##########
[16:02:39:286][  info  ][  scanConsole  ]: -> Configuration: 143 ms
[16:02:39:286][  info  ][  scanConsole  ]: -> Scan:          209 ms
[16:02:39:286][  info  ][  scanConsole  ]: -> Processing:    0 ms
[16:02:39:286][  info  ][  scanConsole  ]: -> Analysis:      800 ms
[16:02:39:286][  info  ][  scanConsole  ]: ###########
[16:02:39:286][  info  ][  scanConsole  ]: # Cleanup #
[16:02:39:286][  info  ][  scanConsole  ]: ###########
[16:02:39:295][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0009300 to configs/KEKQ14/20UPGFC0009300.json
[16:02:39:463][  info  ][  scanConsole  ]: -> Storing output results of FE 4
[16:02:39:463][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0009300, this usually means that the chip did not send any data at all.
[16:02:39:464][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0009316 to configs/KEKQ14/20UPGFC0009316.json
[16:02:39:628][  info  ][  scanConsole  ]: -> Storing output results of FE 5
[16:02:39:628][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0009316, this usually means that the chip did not send any data at all.
[16:02:39:630][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0009332 to configs/KEKQ14/20UPGFC0009332.json
[16:02:39:794][  info  ][  scanConsole  ]: -> Storing output results of FE 6
[16:02:39:794][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0009332, this usually means that the chip did not send any data at all.
[16:02:39:796][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0009348 to configs/KEKQ14/20UPGFC0009348.json
[16:02:39:960][  info  ][  scanConsole  ]: -> Storing output results of FE 7
[16:02:39:960][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0009348, this usually means that the chip did not send any data at all.
[16:02:39:961][  info  ][  scanConsole  ]: Finishing run: 1779
20UPGFC0009300.json.after
20UPGFC0009300.json.before
20UPGFC0009316.json.after
20UPGFC0009316.json.before
20UPGFC0009332.json.after
20UPGFC0009332.json.before
20UPGFC0009348.json.after
20UPGFC0009348.json.before
reg_readtemp.json
scanLog.json

