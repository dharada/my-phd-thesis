[13:37:26:870][  info  ][               ]: #####################################
[13:37:26:870][  info  ][               ]: # Welcome to the YARR Scan Console! #
[13:37:26:870][  info  ][               ]: #####################################
[13:37:26:870][  info  ][               ]: -> Parsing command line parameters ...
[13:37:26:870][  info  ][               ]: Configuring logger ...
[13:37:26:872][  info  ][  scanConsole  ]: Scan Type/Config configs/scans/rd53a/reg_readtemp.json
[13:37:26:872][  info  ][  scanConsole  ]: Connectivity:
[13:37:26:872][  info  ][  scanConsole  ]:     configs/KEKQ14/connectivity.json
[13:37:26:872][  info  ][  scanConsole  ]: Target ToT: -1
[13:37:26:872][  info  ][  scanConsole  ]: Target Charge: -1
[13:37:26:872][  info  ][  scanConsole  ]: Output Plots: true
[13:37:26:872][  info  ][  scanConsole  ]: Output Directory: ./data/001753_reg_readtemp/
[13:37:26:877][  info  ][  scanConsole  ]: Timestamp: 2022-08-24_13:37:26
[13:37:26:877][  info  ][  scanConsole  ]: Run Number: 1753
[13:37:26:877][  info  ][  scanConsole  ]: #################
[13:37:26:877][  info  ][  scanConsole  ]: # Init Hardware #
[13:37:26:877][  info  ][  scanConsole  ]: #################
[13:37:26:877][  info  ][  scanConsole  ]: -> Opening controller config: configs/controller/specCfg-rd53a-16x1.json
[13:37:26:877][  info  ][  ScanHelper   ]: Loading controller ...
[13:37:26:877][  info  ][  ScanHelper   ]: Found controller of type: spec
[13:37:26:877][  info  ][  ScanHelper   ]: ... loading controler config:
[13:37:26:877][  info  ][  ScanHelper   ]: ~~~ {
[13:37:26:877][  info  ][  ScanHelper   ]: ~~~     "cmdPeriod": 6.250000073038109e-9,
[13:37:26:877][  info  ][  ScanHelper   ]: ~~~     "idle": {
[13:37:26:877][  info  ][  ScanHelper   ]: ~~~         "word": 1768515945
[13:37:26:877][  info  ][  ScanHelper   ]: ~~~     },
[13:37:26:877][  info  ][  ScanHelper   ]: ~~~     "pulse": {
[13:37:26:877][  info  ][  ScanHelper   ]: ~~~         "interval": 500,
[13:37:26:877][  info  ][  ScanHelper   ]: ~~~         "word": 1549575846
[13:37:26:877][  info  ][  ScanHelper   ]: ~~~     },
[13:37:26:877][  info  ][  ScanHelper   ]: ~~~     "rxActiveLanes": 1,
[13:37:26:877][  info  ][  ScanHelper   ]: ~~~     "rxPolarity": 0,
[13:37:26:877][  info  ][  ScanHelper   ]: ~~~     "specNum": 0,
[13:37:26:878][  info  ][  ScanHelper   ]: ~~~     "spiConfig": 541200,
[13:37:26:878][  info  ][  ScanHelper   ]: ~~~     "sync": {
[13:37:26:878][  info  ][  ScanHelper   ]: ~~~         "interval": 32,
[13:37:26:878][  info  ][  ScanHelper   ]: ~~~         "word": 2172551550
[13:37:26:878][  info  ][  ScanHelper   ]: ~~~     },
[13:37:26:878][  info  ][  ScanHelper   ]: ~~~     "txPolarity": 0
[13:37:26:878][  info  ][  ScanHelper   ]: ~~~ }
[13:37:26:878][  info  ][    SpecCom    ]: Opening SPEC with id #0
[13:37:26:878][  info  ][    SpecCom    ]: Mapping BARs ...
[13:37:26:878][  info  ][    SpecCom    ]: ... Mapped BAR0 at 0x7f9a89d39000 with size 1048576
[13:37:26:878][warning ][    SpecCom    ]: ... BAR4 not mapped (Mmap failed)
[13:37:26:878][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[13:37:26:878][  info  ][    SpecCom    ]: Firmware Version: 0x4d9ff6d
[13:37:26:878][  info  ][    SpecCom    ]: Firmware Identifier: 0x4030232
[13:37:26:878][  info  ][    SpecCom    ]: FPGA card: PLDA XpressK7 325
[13:37:26:878][  info  ][    SpecCom    ]: FE Chip Type: RD53A/B
[13:37:26:878][  info  ][    SpecCom    ]: FMC Card Type: Ohio Card (Display Port)
[13:37:26:878][  info  ][    SpecCom    ]: RX Speed: 640Mbps
[13:37:26:878][  info  ][    SpecCom    ]: Channel Configuration: 16x1
[13:37:26:878][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[13:37:26:878][  info  ][    SpecCom    ]: Flushing buffers ...
[13:37:26:878][  info  ][    SpecCom    ]: Init success!
[13:37:26:878][  info  ][  scanConsole  ]: #######################
[13:37:26:878][  info  ][  scanConsole  ]: ##  Loading Configs  ##
[13:37:26:878][  info  ][  scanConsole  ]: #######################
[13:37:26:878][  info  ][  scanConsole  ]: Opening global config: configs/KEKQ14/connectivity.json
[13:37:26:878][  info  ][  ScanHelper   ]: Chip type: RD53A
[13:37:26:878][  info  ][  ScanHelper   ]: Chip count 4
[13:37:26:878][  info  ][  ScanHelper   ]: Loading chip #0
[13:37:26:879][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(4)
[13:37:26:879][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ14/20UPGFC0009300.json
[13:37:27:053][  info  ][  ScanHelper   ]: Loading chip #1
[13:37:27:054][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(5)
[13:37:27:054][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ14/20UPGFC0009316.json
[13:37:27:228][  info  ][  ScanHelper   ]: Loading chip #2
[13:37:27:229][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(6)
[13:37:27:229][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ14/20UPGFC0009332.json
[13:37:27:405][  info  ][  ScanHelper   ]: Loading chip #3
[13:37:27:406][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(7)
[13:37:27:406][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ14/20UPGFC0009348.json
[13:37:27:593][  info  ][  scanConsole  ]: #################
[13:37:27:593][  info  ][  scanConsole  ]: # Configure FEs #
[13:37:27:593][  info  ][  scanConsole  ]: #################
[13:37:27:593][  info  ][  scanConsole  ]: Configuring 20UPGFC0009300
[13:37:27:624][  info  ][  scanConsole  ]: Configuring 20UPGFC0009316
[13:37:27:654][  info  ][  scanConsole  ]: Configuring 20UPGFC0009332
[13:37:27:686][  info  ][  scanConsole  ]: Configuring 20UPGFC0009348
[13:37:27:722][  info  ][  scanConsole  ]: Sent configuration to all FEs in 128 ms!
[13:37:27:723][  info  ][  scanConsole  ]: Checking com 20UPGFC0009300
[13:37:27:723][  info  ][    SpecRx     ]: Active Rx channels: 0x10
[13:37:27:723][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[13:37:27:723][  info  ][    SpecRx     ]: Rx Status 0xf0
[13:37:27:723][  info  ][    SpecRx     ]: Number of lanes: 1
[13:37:27:723][  info  ][    SpecRx     ]: Channel 4 Lane 0 synchronized!
[13:37:27:733][  info  ][  scanConsole  ]: ... success!
[13:37:27:733][  info  ][  scanConsole  ]: Checking com 20UPGFC0009316
[13:37:27:733][  info  ][    SpecRx     ]: Active Rx channels: 0x20
[13:37:27:733][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[13:37:27:733][  info  ][    SpecRx     ]: Rx Status 0xf0
[13:37:27:733][  info  ][    SpecRx     ]: Number of lanes: 1
[13:37:27:733][  info  ][    SpecRx     ]: Channel 5 Lane 0 synchronized!
[13:37:27:744][  info  ][  scanConsole  ]: ... success!
[13:37:27:744][  info  ][  scanConsole  ]: Checking com 20UPGFC0009332
[13:37:27:744][  info  ][    SpecRx     ]: Active Rx channels: 0x40
[13:37:27:744][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[13:37:27:744][  info  ][    SpecRx     ]: Rx Status 0xf0
[13:37:27:744][  info  ][    SpecRx     ]: Number of lanes: 1
[13:37:27:744][  info  ][    SpecRx     ]: Channel 6 Lane 0 synchronized!
[13:37:27:754][  info  ][  scanConsole  ]: ... success!
[13:37:27:755][  info  ][  scanConsole  ]: Checking com 20UPGFC0009348
[13:37:27:755][  info  ][    SpecRx     ]: Active Rx channels: 0x80
[13:37:27:755][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[13:37:27:755][  info  ][    SpecRx     ]: Rx Status 0xf0
[13:37:27:755][  info  ][    SpecRx     ]: Number of lanes: 1
[13:37:27:755][  info  ][    SpecRx     ]: Channel 7 Lane 0 synchronized!
[13:37:27:765][  info  ][  scanConsole  ]: ... success!
[13:37:27:765][  info  ][  scanConsole  ]: Enabling Tx channels
[13:37:27:765][  info  ][  scanConsole  ]: Enabling Tx channel 1
[13:37:27:765][  info  ][  scanConsole  ]: Enabling Tx channel 1
[13:37:27:765][  info  ][  scanConsole  ]: Enabling Tx channel 1
[13:37:27:765][  info  ][  scanConsole  ]: Enabling Tx channel 1
[13:37:27:765][  info  ][  scanConsole  ]: Enabling Rx channels
[13:37:27:765][  info  ][  scanConsole  ]: Enabling Rx channel 4
[13:37:27:765][  info  ][  scanConsole  ]: Enabling Rx channel 5
[13:37:27:766][  info  ][  scanConsole  ]: Enabling Rx channel 6
[13:37:27:766][  info  ][  scanConsole  ]: Enabling Rx channel 7
[13:37:27:766][  info  ][  scanConsole  ]: ##############
[13:37:27:766][  info  ][  scanConsole  ]: # Setup Scan #
[13:37:27:766][  info  ][  scanConsole  ]: ##############
[13:37:27:766][  info  ][  scanConsole  ]: Found Scan config, constructing scan ...
[13:37:27:766][  info  ][  ScanFactory  ]: Loading Scan:
[13:37:27:766][  info  ][  ScanFactory  ]:   Name: AnalogScan
[13:37:27:767][  info  ][  ScanFactory  ]:   Number of Loops: 3
[13:37:27:767][  info  ][  ScanFactory  ]:   Loading Loop #0
[13:37:27:767][  info  ][  ScanFactory  ]:    Type: Rd53aReadRegLoop
[13:37:27:767][  info  ][  ScanFactory  ]:    Loading loop config ... 
[13:37:27:767][  info  ][  ScanFactory  ]: ~~~ {
[13:37:27:767][  info  ][  ScanFactory  ]: ~~~     "CurMux": [],
[13:37:27:767][  info  ][  ScanFactory  ]: ~~~     "EnblRingOsc": 0,
[13:37:27:767][  info  ][  ScanFactory  ]: ~~~     "Registers": [],
[13:37:27:767][  info  ][  ScanFactory  ]: ~~~     "RingOscDur": 9,
[13:37:27:767][  info  ][  ScanFactory  ]: ~~~     "RingOscRep": 10,
[13:37:27:767][  info  ][  ScanFactory  ]: ~~~     "VoltMux": [3, 5, 7, 15]
[13:37:27:767][  info  ][  ScanFactory  ]: ~~~ }
[13:37:27:767][  info  ][  ScanFactory  ]:   Loading Loop #1
[13:37:27:767][  info  ][  ScanFactory  ]:    Type: Rd53aTriggerLoop
[13:37:27:767][  info  ][  ScanFactory  ]:    Loading loop config ... 
[13:37:27:767][  info  ][  ScanFactory  ]: ~~~ {
[13:37:27:767][  info  ][  ScanFactory  ]: ~~~     "count": 1,
[13:37:27:767][  info  ][  ScanFactory  ]: ~~~     "delay": 48,
[13:37:27:767][  info  ][  ScanFactory  ]: ~~~     "extTrig": false,
[13:37:27:767][  info  ][  ScanFactory  ]: ~~~     "frequency": 30000.0,
[13:37:27:767][  info  ][  ScanFactory  ]: ~~~     "noInject": false,
[13:37:27:767][  info  ][  ScanFactory  ]: ~~~     "sendEcr": false,
[13:37:27:767][  info  ][  ScanFactory  ]: ~~~     "time": 0.0,
[13:37:27:767][  info  ][  ScanFactory  ]: ~~~     "trigMultiplier": 16
[13:37:27:767][  info  ][  ScanFactory  ]: ~~~ }
[13:37:27:767][  info  ][  ScanFactory  ]:   Loading Loop #2
[13:37:27:767][  info  ][  ScanFactory  ]:    Type: StdDataLoop
[13:37:27:767][warning ][  ScanFactory  ]: ~~~ Config empty.
[13:37:27:767][  info  ][ScanBuildHistogrammers]: Loading histogrammer ...
[13:37:27:768][  info  ][ScanBuildHistogrammers]: ... done!
[13:37:27:768][  info  ][ScanBuildAnalyses]: Loading analyses ...
[13:37:27:768][  info  ][  scanConsole  ]: Running pre scan!
[13:37:27:768][  info  ][  ScanFactory  ]: Entering pre scan phase ...
[13:37:27:768][  info  ][     Rd53a     ]: Write named register: GlobalPulseRt -> 16384
[13:37:27:768][  info  ][     Rd53a     ]: Write named register: InjEnDig -> 0
[13:37:27:768][  info  ][     Rd53a     ]: Write named register: InjVcalDiff -> 0
[13:37:27:768][  info  ][     Rd53a     ]: Write named register: InjVcalHigh -> 3500
[13:37:27:768][  info  ][     Rd53a     ]: Write named register: InjVcalMed -> 500
[13:37:27:768][  info  ][     Rd53a     ]: Write named register: LatencyConfig -> 48
[13:37:27:768][  info  ][  scanConsole  ]: Starting histogrammer and analysis threads:
[13:37:27:769][  info  ][  scanConsole  ]:  .. started threads of Fe 4
[13:37:27:769][  info  ][  scanConsole  ]:  .. started threads of Fe 5
[13:37:27:769][  info  ][  scanConsole  ]:  .. started threads of Fe 6
[13:37:27:769][  info  ][  scanConsole  ]:  .. started threads of Fe 7
[13:37:27:769][  info  ][Rd53aDataProcessor]:   -> Processor thread #0 started!
[13:37:27:769][  info  ][Rd53aDataProcessor]:   -> Processor thread #1 started!
[13:37:27:769][  info  ][Rd53aDataProcessor]:   -> Processor thread #2 started!
[13:37:27:769][  info  ][Rd53aDataProcessor]:   -> Processor thread #3 started!
[13:37:27:769][  info  ][  scanConsole  ]: ########
[13:37:27:769][  info  ][  scanConsole  ]: # Scan #
[13:37:27:769][  info  ][  scanConsole  ]: ########
[13:37:27:769][  info  ][  scanConsole  ]: Starting scan!
[13:37:27:769][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0009300 on Rx 4
[13:37:27:774][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0009300] MON MUX_V: 3 Bias 0 1054 -> 0.232843 V Bias 1 1507 -> 0.322213 V, Temperature 0.0893706 -> 34.0786 C
[13:37:27:778][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0009300] MON MUX_V: 5 Bias 0 995 -> 0.221203 V Bias 1 1455 -> 0.311954 V, Temperature 0.0907516 -> 40.3778 C
[13:37:27:782][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0009300] MON MUX_V: 7 Bias 0 981 -> 0.218441 V Bias 1 1447 -> 0.310376 V, Temperature 0.0919353 -> 31.9806 C
[13:37:27:786][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0009300] MON MUX_V: 15 Bias 0 968 -> 0.215876 V Bias 1 1435 -> 0.308009 V, Temperature 0.0921326 -> 28.1854 C
[13:37:27:821][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0009316 on Rx 5
[13:37:27:825][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0009316] MON MUX_V: 3 Bias 0 1049 -> 0.229685 V Bias 1 1497 -> 0.318926 V, Temperature 0.0892416 -> 33.6269 C
[13:37:27:829][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0009316] MON MUX_V: 5 Bias 0 1014 -> 0.222713 V Bias 1 1465 -> 0.312552 V, Temperature 0.0898392 -> 34.4235 C
[13:37:27:834][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0009316] MON MUX_V: 7 Bias 0 989 -> 0.217733 V Bias 1 1454 -> 0.310361 V, Temperature 0.092628 -> 35.1837 C
[13:37:27:838][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0009316] MON MUX_V: 15 Bias 0 981 -> 0.216139 V Bias 1 1439 -> 0.307373 V, Temperature 0.0912336 -> 35.0152 C
[13:37:27:873][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0009332 on Rx 6
[13:37:27:877][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0009332] MON MUX_V: 3 Bias 0 1034 -> 0.231882 V Bias 1 1485 -> 0.322172 V, Temperature 0.0902902 -> 33.3653 C
[13:37:27:881][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0009332] MON MUX_V: 5 Bias 0 974 -> 0.21987 V Bias 1 1435 -> 0.312162 V, Temperature 0.0922922 -> 35.2933 C
[13:37:27:885][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0009332] MON MUX_V: 7 Bias 0 973 -> 0.219669 V Bias 1 1435 -> 0.312162 V, Temperature 0.0924924 -> 35.9625 C
[13:37:27:889][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0009332] MON MUX_V: 15 Bias 0 952 -> 0.215465 V Bias 1 1421 -> 0.309359 V, Temperature 0.0938938 -> 33.6988 C
[13:37:27:924][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0009348 on Rx 7
[13:37:27:929][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0009348] MON MUX_V: 3 Bias 0 1027 -> 0.226066 V Bias 1 1472 -> 0.315333 V, Temperature 0.089267 -> 33.3358 C
[13:37:27:933][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0009348] MON MUX_V: 5 Bias 0 979 -> 0.216437 V Bias 1 1425 -> 0.305905 V, Temperature 0.0894676 -> 36.6441 C
[13:37:27:937][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0009348] MON MUX_V: 7 Bias 0 950 -> 0.21062 V Bias 1 1411 -> 0.303096 V, Temperature 0.0924766 -> 37.7491 C
[13:37:27:942][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0009348] MON MUX_V: 15 Bias 0 956 -> 0.211823 V Bias 1 1417 -> 0.3043 V, Temperature 0.0924766 -> 36.7779 C
[13:37:27:977][  info  ][  scanConsole  ]: Scan done!
[13:37:27:977][  info  ][  scanConsole  ]: Waiting for processors to finish ...
[13:37:27:978][  info  ][  scanConsole  ]: Processor done, waiting for histogrammer ...
[13:37:28:378][  info  ][HistogramAlgorithm]: Histogrammer done!
[13:37:28:378][  info  ][HistogramAlgorithm]: Histogrammer done!
[13:37:28:378][  info  ][HistogramAlgorithm]: Histogrammer done!
[13:37:28:378][  info  ][HistogramAlgorithm]: Histogrammer done!
[13:37:28:378][  info  ][  scanConsole  ]: Processor done, waiting for analysis ...
[13:37:28:779][  info  ][AnalysisAlgorithm]: Analysis done!
[13:37:28:779][  info  ][AnalysisAlgorithm]: Analysis done!
[13:37:28:779][  info  ][AnalysisAlgorithm]: Analysis done!
[13:37:28:779][  info  ][AnalysisAlgorithm]: Analysis done!
[13:37:28:779][  info  ][  scanConsole  ]: All done!
[13:37:28:779][  info  ][  scanConsole  ]: ##########
[13:37:28:779][  info  ][  scanConsole  ]: # Timing #
[13:37:28:779][  info  ][  scanConsole  ]: ##########
[13:37:28:779][  info  ][  scanConsole  ]: -> Configuration: 128 ms
[13:37:28:779][  info  ][  scanConsole  ]: -> Scan:          208 ms
[13:37:28:779][  info  ][  scanConsole  ]: -> Processing:    0 ms
[13:37:28:779][  info  ][  scanConsole  ]: -> Analysis:      800 ms
[13:37:28:779][  info  ][  scanConsole  ]: ###########
[13:37:28:779][  info  ][  scanConsole  ]: # Cleanup #
[13:37:28:779][  info  ][  scanConsole  ]: ###########
[13:37:28:782][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0009300 to configs/KEKQ14/20UPGFC0009300.json
[13:37:28:972][  info  ][  scanConsole  ]: -> Storing output results of FE 4
[13:37:28:972][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0009300, this usually means that the chip did not send any data at all.
[13:37:28:974][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0009316 to configs/KEKQ14/20UPGFC0009316.json
[13:37:29:148][  info  ][  scanConsole  ]: -> Storing output results of FE 5
[13:37:29:148][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0009316, this usually means that the chip did not send any data at all.
[13:37:29:150][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0009332 to configs/KEKQ14/20UPGFC0009332.json
[13:37:29:313][  info  ][  scanConsole  ]: -> Storing output results of FE 6
[13:37:29:313][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0009332, this usually means that the chip did not send any data at all.
[13:37:29:314][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0009348 to configs/KEKQ14/20UPGFC0009348.json
[13:37:29:476][  info  ][  scanConsole  ]: -> Storing output results of FE 7
[13:37:29:477][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0009348, this usually means that the chip did not send any data at all.
[13:37:29:478][  info  ][  scanConsole  ]: Finishing run: 1753
20UPGFC0009300.json.after
20UPGFC0009300.json.before
20UPGFC0009316.json.after
20UPGFC0009316.json.before
20UPGFC0009332.json.after
20UPGFC0009332.json.before
20UPGFC0009348.json.after
20UPGFC0009348.json.before
reg_readtemp.json
scanLog.json

