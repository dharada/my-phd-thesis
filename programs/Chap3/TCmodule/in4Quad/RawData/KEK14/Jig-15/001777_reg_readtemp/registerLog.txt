[15:39:52:820][  info  ][               ]: #####################################
[15:39:52:820][  info  ][               ]: # Welcome to the YARR Scan Console! #
[15:39:52:820][  info  ][               ]: #####################################
[15:39:52:820][  info  ][               ]: -> Parsing command line parameters ...
[15:39:52:820][  info  ][               ]: Configuring logger ...
[15:39:52:822][  info  ][  scanConsole  ]: Scan Type/Config configs/scans/rd53a/reg_readtemp.json
[15:39:52:822][  info  ][  scanConsole  ]: Connectivity:
[15:39:52:822][  info  ][  scanConsole  ]:     configs/KEKQ14/connectivity.json
[15:39:52:822][  info  ][  scanConsole  ]: Target ToT: -1
[15:39:52:822][  info  ][  scanConsole  ]: Target Charge: -1
[15:39:52:822][  info  ][  scanConsole  ]: Output Plots: true
[15:39:52:822][  info  ][  scanConsole  ]: Output Directory: ./data/001777_reg_readtemp/
[15:39:52:828][  info  ][  scanConsole  ]: Timestamp: 2022-08-24_15:39:52
[15:39:52:828][  info  ][  scanConsole  ]: Run Number: 1777
[15:39:52:828][  info  ][  scanConsole  ]: #################
[15:39:52:828][  info  ][  scanConsole  ]: # Init Hardware #
[15:39:52:828][  info  ][  scanConsole  ]: #################
[15:39:52:828][  info  ][  scanConsole  ]: -> Opening controller config: configs/controller/specCfg-rd53a-16x1.json
[15:39:52:828][  info  ][  ScanHelper   ]: Loading controller ...
[15:39:52:828][  info  ][  ScanHelper   ]: Found controller of type: spec
[15:39:52:828][  info  ][  ScanHelper   ]: ... loading controler config:
[15:39:52:828][  info  ][  ScanHelper   ]: ~~~ {
[15:39:52:828][  info  ][  ScanHelper   ]: ~~~     "cmdPeriod": 6.250000073038109e-9,
[15:39:52:828][  info  ][  ScanHelper   ]: ~~~     "idle": {
[15:39:52:828][  info  ][  ScanHelper   ]: ~~~         "word": 1768515945
[15:39:52:828][  info  ][  ScanHelper   ]: ~~~     },
[15:39:52:828][  info  ][  ScanHelper   ]: ~~~     "pulse": {
[15:39:52:828][  info  ][  ScanHelper   ]: ~~~         "interval": 500,
[15:39:52:828][  info  ][  ScanHelper   ]: ~~~         "word": 1549575846
[15:39:52:828][  info  ][  ScanHelper   ]: ~~~     },
[15:39:52:828][  info  ][  ScanHelper   ]: ~~~     "rxActiveLanes": 1,
[15:39:52:828][  info  ][  ScanHelper   ]: ~~~     "rxPolarity": 0,
[15:39:52:828][  info  ][  ScanHelper   ]: ~~~     "specNum": 0,
[15:39:52:828][  info  ][  ScanHelper   ]: ~~~     "spiConfig": 541200,
[15:39:52:828][  info  ][  ScanHelper   ]: ~~~     "sync": {
[15:39:52:828][  info  ][  ScanHelper   ]: ~~~         "interval": 32,
[15:39:52:828][  info  ][  ScanHelper   ]: ~~~         "word": 2172551550
[15:39:52:828][  info  ][  ScanHelper   ]: ~~~     },
[15:39:52:828][  info  ][  ScanHelper   ]: ~~~     "txPolarity": 0
[15:39:52:828][  info  ][  ScanHelper   ]: ~~~ }
[15:39:52:828][  info  ][    SpecCom    ]: Opening SPEC with id #0
[15:39:52:829][  info  ][    SpecCom    ]: Mapping BARs ...
[15:39:52:829][  info  ][    SpecCom    ]: ... Mapped BAR0 at 0x7f0e5d162000 with size 1048576
[15:39:52:829][warning ][    SpecCom    ]: ... BAR4 not mapped (Mmap failed)
[15:39:52:829][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[15:39:52:829][  info  ][    SpecCom    ]: Firmware Version: 0x4d9ff6d
[15:39:52:829][  info  ][    SpecCom    ]: Firmware Identifier: 0x4030232
[15:39:52:829][  info  ][    SpecCom    ]: FPGA card: PLDA XpressK7 325
[15:39:52:829][  info  ][    SpecCom    ]: FE Chip Type: RD53A/B
[15:39:52:829][  info  ][    SpecCom    ]: FMC Card Type: Ohio Card (Display Port)
[15:39:52:829][  info  ][    SpecCom    ]: RX Speed: 640Mbps
[15:39:52:829][  info  ][    SpecCom    ]: Channel Configuration: 16x1
[15:39:52:829][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[15:39:52:829][  info  ][    SpecCom    ]: Flushing buffers ...
[15:39:52:829][  info  ][    SpecCom    ]: Init success!
[15:39:52:829][  info  ][  scanConsole  ]: #######################
[15:39:52:829][  info  ][  scanConsole  ]: ##  Loading Configs  ##
[15:39:52:829][  info  ][  scanConsole  ]: #######################
[15:39:52:829][  info  ][  scanConsole  ]: Opening global config: configs/KEKQ14/connectivity.json
[15:39:52:829][  info  ][  ScanHelper   ]: Chip type: RD53A
[15:39:52:829][  info  ][  ScanHelper   ]: Chip count 4
[15:39:52:829][  info  ][  ScanHelper   ]: Loading chip #0
[15:39:52:830][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(4)
[15:39:52:830][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ14/20UPGFC0009300.json
[15:39:53:005][  info  ][  ScanHelper   ]: Loading chip #1
[15:39:53:006][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(5)
[15:39:53:006][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ14/20UPGFC0009316.json
[15:39:53:178][  info  ][  ScanHelper   ]: Loading chip #2
[15:39:53:179][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(6)
[15:39:53:179][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ14/20UPGFC0009332.json
[15:39:53:353][  info  ][  ScanHelper   ]: Loading chip #3
[15:39:53:354][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(7)
[15:39:53:354][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ14/20UPGFC0009348.json
[15:39:53:531][  info  ][  scanConsole  ]: #################
[15:39:53:531][  info  ][  scanConsole  ]: # Configure FEs #
[15:39:53:531][  info  ][  scanConsole  ]: #################
[15:39:53:531][  info  ][  scanConsole  ]: Configuring 20UPGFC0009300
[15:39:53:562][  info  ][  scanConsole  ]: Configuring 20UPGFC0009316
[15:39:53:592][  info  ][  scanConsole  ]: Configuring 20UPGFC0009332
[15:39:53:622][  info  ][  scanConsole  ]: Configuring 20UPGFC0009348
[15:39:53:652][  info  ][  scanConsole  ]: Sent configuration to all FEs in 120 ms!
[15:39:53:653][  info  ][  scanConsole  ]: Checking com 20UPGFC0009300
[15:39:53:653][  info  ][    SpecRx     ]: Active Rx channels: 0x10
[15:39:53:653][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[15:39:53:653][  info  ][    SpecRx     ]: Rx Status 0xf0
[15:39:53:653][  info  ][    SpecRx     ]: Number of lanes: 1
[15:39:53:653][  info  ][    SpecRx     ]: Channel 4 Lane 0 synchronized!
[15:39:53:663][  info  ][  scanConsole  ]: ... success!
[15:39:53:663][  info  ][  scanConsole  ]: Checking com 20UPGFC0009316
[15:39:53:663][  info  ][    SpecRx     ]: Active Rx channels: 0x20
[15:39:53:663][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[15:39:53:663][  info  ][    SpecRx     ]: Rx Status 0xf0
[15:39:53:663][  info  ][    SpecRx     ]: Number of lanes: 1
[15:39:53:663][  info  ][    SpecRx     ]: Channel 5 Lane 0 synchronized!
[15:39:53:674][  info  ][  scanConsole  ]: ... success!
[15:39:53:674][  info  ][  scanConsole  ]: Checking com 20UPGFC0009332
[15:39:53:674][  info  ][    SpecRx     ]: Active Rx channels: 0x40
[15:39:53:674][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[15:39:53:674][  info  ][    SpecRx     ]: Rx Status 0xf0
[15:39:53:674][  info  ][    SpecRx     ]: Number of lanes: 1
[15:39:53:674][  info  ][    SpecRx     ]: Channel 6 Lane 0 synchronized!
[15:39:53:685][  info  ][  scanConsole  ]: ... success!
[15:39:53:685][  info  ][  scanConsole  ]: Checking com 20UPGFC0009348
[15:39:53:685][  info  ][    SpecRx     ]: Active Rx channels: 0x80
[15:39:53:685][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[15:39:53:685][  info  ][    SpecRx     ]: Rx Status 0xf0
[15:39:53:685][  info  ][    SpecRx     ]: Number of lanes: 1
[15:39:53:685][  info  ][    SpecRx     ]: Channel 7 Lane 0 synchronized!
[15:39:53:695][  info  ][  scanConsole  ]: ... success!
[15:39:53:695][  info  ][  scanConsole  ]: Enabling Tx channels
[15:39:53:695][  info  ][  scanConsole  ]: Enabling Tx channel 1
[15:39:53:695][  info  ][  scanConsole  ]: Enabling Tx channel 1
[15:39:53:695][  info  ][  scanConsole  ]: Enabling Tx channel 1
[15:39:53:695][  info  ][  scanConsole  ]: Enabling Tx channel 1
[15:39:53:695][  info  ][  scanConsole  ]: Enabling Rx channels
[15:39:53:695][  info  ][  scanConsole  ]: Enabling Rx channel 4
[15:39:53:695][  info  ][  scanConsole  ]: Enabling Rx channel 5
[15:39:53:695][  info  ][  scanConsole  ]: Enabling Rx channel 6
[15:39:53:695][  info  ][  scanConsole  ]: Enabling Rx channel 7
[15:39:53:695][  info  ][  scanConsole  ]: ##############
[15:39:53:695][  info  ][  scanConsole  ]: # Setup Scan #
[15:39:53:695][  info  ][  scanConsole  ]: ##############
[15:39:53:695][  info  ][  scanConsole  ]: Found Scan config, constructing scan ...
[15:39:53:695][  info  ][  ScanFactory  ]: Loading Scan:
[15:39:53:695][  info  ][  ScanFactory  ]:   Name: AnalogScan
[15:39:53:695][  info  ][  ScanFactory  ]:   Number of Loops: 3
[15:39:53:695][  info  ][  ScanFactory  ]:   Loading Loop #0
[15:39:53:695][  info  ][  ScanFactory  ]:    Type: Rd53aReadRegLoop
[15:39:53:695][  info  ][  ScanFactory  ]:    Loading loop config ... 
[15:39:53:695][  info  ][  ScanFactory  ]: ~~~ {
[15:39:53:695][  info  ][  ScanFactory  ]: ~~~     "CurMux": [],
[15:39:53:695][  info  ][  ScanFactory  ]: ~~~     "EnblRingOsc": 0,
[15:39:53:695][  info  ][  ScanFactory  ]: ~~~     "Registers": [],
[15:39:53:695][  info  ][  ScanFactory  ]: ~~~     "RingOscDur": 9,
[15:39:53:695][  info  ][  ScanFactory  ]: ~~~     "RingOscRep": 10,
[15:39:53:695][  info  ][  ScanFactory  ]: ~~~     "VoltMux": [3, 5, 7, 15]
[15:39:53:695][  info  ][  ScanFactory  ]: ~~~ }
[15:39:53:695][  info  ][  ScanFactory  ]:   Loading Loop #1
[15:39:53:695][  info  ][  ScanFactory  ]:    Type: Rd53aTriggerLoop
[15:39:53:695][  info  ][  ScanFactory  ]:    Loading loop config ... 
[15:39:53:695][  info  ][  ScanFactory  ]: ~~~ {
[15:39:53:695][  info  ][  ScanFactory  ]: ~~~     "count": 1,
[15:39:53:695][  info  ][  ScanFactory  ]: ~~~     "delay": 48,
[15:39:53:695][  info  ][  ScanFactory  ]: ~~~     "extTrig": false,
[15:39:53:695][  info  ][  ScanFactory  ]: ~~~     "frequency": 30000.0,
[15:39:53:695][  info  ][  ScanFactory  ]: ~~~     "noInject": false,
[15:39:53:695][  info  ][  ScanFactory  ]: ~~~     "sendEcr": false,
[15:39:53:695][  info  ][  ScanFactory  ]: ~~~     "time": 0.0,
[15:39:53:695][  info  ][  ScanFactory  ]: ~~~     "trigMultiplier": 16
[15:39:53:695][  info  ][  ScanFactory  ]: ~~~ }
[15:39:53:695][  info  ][  ScanFactory  ]:   Loading Loop #2
[15:39:53:696][  info  ][  ScanFactory  ]:    Type: StdDataLoop
[15:39:53:696][warning ][  ScanFactory  ]: ~~~ Config empty.
[15:39:53:696][  info  ][ScanBuildHistogrammers]: Loading histogrammer ...
[15:39:53:696][  info  ][ScanBuildHistogrammers]: ... done!
[15:39:53:696][  info  ][ScanBuildAnalyses]: Loading analyses ...
[15:39:53:696][  info  ][  scanConsole  ]: Running pre scan!
[15:39:53:696][  info  ][  ScanFactory  ]: Entering pre scan phase ...
[15:39:53:696][  info  ][     Rd53a     ]: Write named register: GlobalPulseRt -> 16384
[15:39:53:696][  info  ][     Rd53a     ]: Write named register: InjEnDig -> 0
[15:39:53:696][  info  ][     Rd53a     ]: Write named register: InjVcalDiff -> 0
[15:39:53:696][  info  ][     Rd53a     ]: Write named register: InjVcalHigh -> 3500
[15:39:53:696][  info  ][     Rd53a     ]: Write named register: InjVcalMed -> 500
[15:39:53:696][  info  ][     Rd53a     ]: Write named register: LatencyConfig -> 48
[15:39:53:696][  info  ][  scanConsole  ]: Starting histogrammer and analysis threads:
[15:39:53:696][  info  ][  scanConsole  ]:  .. started threads of Fe 4
[15:39:53:696][  info  ][  scanConsole  ]:  .. started threads of Fe 5
[15:39:53:696][  info  ][  scanConsole  ]:  .. started threads of Fe 6
[15:39:53:696][  info  ][  scanConsole  ]:  .. started threads of Fe 7
[15:39:53:696][  info  ][Rd53aDataProcessor]:   -> Processor thread #0 started!
[15:39:53:696][  info  ][Rd53aDataProcessor]:   -> Processor thread #1 started!
[15:39:53:696][  info  ][Rd53aDataProcessor]:   -> Processor thread #2 started!
[15:39:53:696][  info  ][Rd53aDataProcessor]:   -> Processor thread #3 started!
[15:39:53:696][  info  ][  scanConsole  ]: ########
[15:39:53:696][  info  ][  scanConsole  ]: # Scan #
[15:39:53:696][  info  ][  scanConsole  ]: ########
[15:39:53:696][  info  ][  scanConsole  ]: Starting scan!
[15:39:53:696][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0009300 on Rx 4
[15:39:53:700][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0009300] MON MUX_V: 3 Bias 0 1326 -> 0.286504 V Bias 1 1711 -> 0.36246 V, Temperature 0.0759552 -> -12.0396 C
[15:39:53:704][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0009300] MON MUX_V: 5 Bias 0 1274 -> 0.276246 V Bias 1 1664 -> 0.353187 V, Temperature 0.0769416 -> -7.33289 C
[15:39:53:708][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0009300] MON MUX_V: 7 Bias 0 1265 -> 0.27447 V Bias 1 1659 -> 0.352201 V, Temperature 0.0777307 -> -15.164 C
[15:39:53:712][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0009300] MON MUX_V: 15 Bias 0 1248 -> 0.271116 V Bias 1 1646 -> 0.349636 V, Temperature 0.0785199 -> -16.3374 C
[15:39:53:747][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0009316 on Rx 5
[15:39:53:751][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0009316] MON MUX_V: 3 Bias 0 1316 -> 0.282871 V Bias 1 1697 -> 0.358766 V, Temperature 0.0758952 -> -12.2527 C
[15:39:53:755][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0009316] MON MUX_V: 5 Bias 0 1289 -> 0.277493 V Bias 1 1670 -> 0.353388 V, Temperature 0.0758952 -> -13.3151 C
[15:39:53:759][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0009316] MON MUX_V: 7 Bias 0 1264 -> 0.272513 V Bias 1 1660 -> 0.351396 V, Temperature 0.0788832 -> -10.5692 C
[15:39:53:763][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0009316] MON MUX_V: 15 Bias 0 1256 -> 0.270919 V Bias 1 1644 -> 0.348209 V, Temperature 0.0772896 -> -12.0844 C
[15:39:53:798][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0009332 on Rx 6
[15:39:53:802][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0009332] MON MUX_V: 3 Bias 0 1294 -> 0.283934 V Bias 1 1674 -> 0.36001 V, Temperature 0.076076 -> -14.8888 C
[15:39:53:806][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0009332] MON MUX_V: 5 Bias 0 1242 -> 0.273523 V Bias 1 1632 -> 0.351601 V, Temperature 0.078078 -> -12.2109 C
[15:39:53:810][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0009332] MON MUX_V: 7 Bias 0 1248 -> 0.274725 V Bias 1 1635 -> 0.352202 V, Temperature 0.0774774 -> -14.2182 C
[15:39:53:814][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0009332] MON MUX_V: 15 Bias 0 1221 -> 0.269319 V Bias 1 1619 -> 0.348999 V, Temperature 0.0796796 -> -12.7538 C
[15:39:53:849][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0009348 on Rx 7
[15:39:53:853][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0009348] MON MUX_V: 3 Bias 0 1294 -> 0.279626 V Bias 1 1671 -> 0.355252 V, Temperature 0.0756262 -> -13.4979 C
[15:39:53:858][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0009348] MON MUX_V: 5 Bias 0 1250 -> 0.2708 V Bias 1 1627 -> 0.346426 V, Temperature 0.0756262 -> -11.2838 C
[15:39:53:862][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0009348] MON MUX_V: 7 Bias 0 1224 -> 0.265584 V Bias 1 1615 -> 0.344019 V, Temperature 0.0784346 -> -9.4592 C
[15:39:53:866][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0009348] MON MUX_V: 15 Bias 0 1231 -> 0.266988 V Bias 1 1625 -> 0.346025 V, Temperature 0.0790364 -> -8.26578 C
[15:39:53:902][  info  ][  scanConsole  ]: Scan done!
[15:39:53:902][  info  ][  scanConsole  ]: Waiting for processors to finish ...
[15:39:53:903][  info  ][  scanConsole  ]: Processor done, waiting for histogrammer ...
[15:39:54:303][  info  ][HistogramAlgorithm]: Histogrammer done!
[15:39:54:303][  info  ][HistogramAlgorithm]: Histogrammer done!
[15:39:54:303][  info  ][HistogramAlgorithm]: Histogrammer done!
[15:39:54:303][  info  ][HistogramAlgorithm]: Histogrammer done!
[15:39:54:303][  info  ][  scanConsole  ]: Processor done, waiting for analysis ...
[15:39:54:704][  info  ][AnalysisAlgorithm]: Analysis done!
[15:39:54:704][  info  ][AnalysisAlgorithm]: Analysis done!
[15:39:54:704][  info  ][AnalysisAlgorithm]: Analysis done!
[15:39:54:704][  info  ][AnalysisAlgorithm]: Analysis done!
[15:39:54:704][  info  ][  scanConsole  ]: All done!
[15:39:54:704][  info  ][  scanConsole  ]: ##########
[15:39:54:704][  info  ][  scanConsole  ]: # Timing #
[15:39:54:704][  info  ][  scanConsole  ]: ##########
[15:39:54:704][  info  ][  scanConsole  ]: -> Configuration: 120 ms
[15:39:54:704][  info  ][  scanConsole  ]: -> Scan:          206 ms
[15:39:54:704][  info  ][  scanConsole  ]: -> Processing:    0 ms
[15:39:54:704][  info  ][  scanConsole  ]: -> Analysis:      800 ms
[15:39:54:704][  info  ][  scanConsole  ]: ###########
[15:39:54:704][  info  ][  scanConsole  ]: # Cleanup #
[15:39:54:704][  info  ][  scanConsole  ]: ###########
[15:39:54:712][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0009300 to configs/KEKQ14/20UPGFC0009300.json
[15:39:54:882][  info  ][  scanConsole  ]: -> Storing output results of FE 4
[15:39:54:882][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0009300, this usually means that the chip did not send any data at all.
[15:39:54:883][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0009316 to configs/KEKQ14/20UPGFC0009316.json
[15:39:55:047][  info  ][  scanConsole  ]: -> Storing output results of FE 5
[15:39:55:047][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0009316, this usually means that the chip did not send any data at all.
[15:39:55:048][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0009332 to configs/KEKQ14/20UPGFC0009332.json
[15:39:55:212][  info  ][  scanConsole  ]: -> Storing output results of FE 6
[15:39:55:212][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0009332, this usually means that the chip did not send any data at all.
[15:39:55:213][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0009348 to configs/KEKQ14/20UPGFC0009348.json
[15:39:55:378][  info  ][  scanConsole  ]: -> Storing output results of FE 7
[15:39:55:378][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0009348, this usually means that the chip did not send any data at all.
[15:39:55:379][  info  ][  scanConsole  ]: Finishing run: 1777
20UPGFC0009300.json.after
20UPGFC0009300.json.before
20UPGFC0009316.json.after
20UPGFC0009316.json.before
20UPGFC0009332.json.after
20UPGFC0009332.json.before
20UPGFC0009348.json.after
20UPGFC0009348.json.before
reg_readtemp.json
scanLog.json

