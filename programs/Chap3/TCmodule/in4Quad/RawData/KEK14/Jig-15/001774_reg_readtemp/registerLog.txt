[15:35:07:258][  info  ][               ]: #####################################
[15:35:07:259][  info  ][               ]: # Welcome to the YARR Scan Console! #
[15:35:07:259][  info  ][               ]: #####################################
[15:35:07:259][  info  ][               ]: -> Parsing command line parameters ...
[15:35:07:259][  info  ][               ]: Configuring logger ...
[15:35:07:261][  info  ][  scanConsole  ]: Scan Type/Config configs/scans/rd53a/reg_readtemp.json
[15:35:07:261][  info  ][  scanConsole  ]: Connectivity:
[15:35:07:261][  info  ][  scanConsole  ]:     configs/KEKQ14/connectivity.json
[15:35:07:261][  info  ][  scanConsole  ]: Target ToT: -1
[15:35:07:261][  info  ][  scanConsole  ]: Target Charge: -1
[15:35:07:261][  info  ][  scanConsole  ]: Output Plots: true
[15:35:07:261][  info  ][  scanConsole  ]: Output Directory: ./data/001774_reg_readtemp/
[15:35:07:265][  info  ][  scanConsole  ]: Timestamp: 2022-08-24_15:35:07
[15:35:07:265][  info  ][  scanConsole  ]: Run Number: 1774
[15:35:07:265][  info  ][  scanConsole  ]: #################
[15:35:07:265][  info  ][  scanConsole  ]: # Init Hardware #
[15:35:07:265][  info  ][  scanConsole  ]: #################
[15:35:07:265][  info  ][  scanConsole  ]: -> Opening controller config: configs/controller/specCfg-rd53a-16x1.json
[15:35:07:265][  info  ][  ScanHelper   ]: Loading controller ...
[15:35:07:265][  info  ][  ScanHelper   ]: Found controller of type: spec
[15:35:07:265][  info  ][  ScanHelper   ]: ... loading controler config:
[15:35:07:265][  info  ][  ScanHelper   ]: ~~~ {
[15:35:07:265][  info  ][  ScanHelper   ]: ~~~     "cmdPeriod": 6.250000073038109e-9,
[15:35:07:265][  info  ][  ScanHelper   ]: ~~~     "idle": {
[15:35:07:265][  info  ][  ScanHelper   ]: ~~~         "word": 1768515945
[15:35:07:265][  info  ][  ScanHelper   ]: ~~~     },
[15:35:07:265][  info  ][  ScanHelper   ]: ~~~     "pulse": {
[15:35:07:265][  info  ][  ScanHelper   ]: ~~~         "interval": 500,
[15:35:07:265][  info  ][  ScanHelper   ]: ~~~         "word": 1549575846
[15:35:07:265][  info  ][  ScanHelper   ]: ~~~     },
[15:35:07:265][  info  ][  ScanHelper   ]: ~~~     "rxActiveLanes": 1,
[15:35:07:265][  info  ][  ScanHelper   ]: ~~~     "rxPolarity": 0,
[15:35:07:265][  info  ][  ScanHelper   ]: ~~~     "specNum": 0,
[15:35:07:265][  info  ][  ScanHelper   ]: ~~~     "spiConfig": 541200,
[15:35:07:265][  info  ][  ScanHelper   ]: ~~~     "sync": {
[15:35:07:265][  info  ][  ScanHelper   ]: ~~~         "interval": 32,
[15:35:07:265][  info  ][  ScanHelper   ]: ~~~         "word": 2172551550
[15:35:07:266][  info  ][  ScanHelper   ]: ~~~     },
[15:35:07:266][  info  ][  ScanHelper   ]: ~~~     "txPolarity": 0
[15:35:07:266][  info  ][  ScanHelper   ]: ~~~ }
[15:35:07:266][  info  ][    SpecCom    ]: Opening SPEC with id #0
[15:35:07:266][  info  ][    SpecCom    ]: Mapping BARs ...
[15:35:07:266][  info  ][    SpecCom    ]: ... Mapped BAR0 at 0x7f7940998000 with size 1048576
[15:35:07:266][warning ][    SpecCom    ]: ... BAR4 not mapped (Mmap failed)
[15:35:07:266][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[15:35:07:266][  info  ][    SpecCom    ]: Firmware Version: 0x4d9ff6d
[15:35:07:266][  info  ][    SpecCom    ]: Firmware Identifier: 0x4030232
[15:35:07:266][  info  ][    SpecCom    ]: FPGA card: PLDA XpressK7 325
[15:35:07:266][  info  ][    SpecCom    ]: FE Chip Type: RD53A/B
[15:35:07:266][  info  ][    SpecCom    ]: FMC Card Type: Ohio Card (Display Port)
[15:35:07:266][  info  ][    SpecCom    ]: RX Speed: 640Mbps
[15:35:07:266][  info  ][    SpecCom    ]: Channel Configuration: 16x1
[15:35:07:266][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[15:35:07:266][  info  ][    SpecCom    ]: Flushing buffers ...
[15:35:07:266][  info  ][    SpecCom    ]: Init success!
[15:35:07:266][  info  ][  scanConsole  ]: #######################
[15:35:07:266][  info  ][  scanConsole  ]: ##  Loading Configs  ##
[15:35:07:266][  info  ][  scanConsole  ]: #######################
[15:35:07:266][  info  ][  scanConsole  ]: Opening global config: configs/KEKQ14/connectivity.json
[15:35:07:266][  info  ][  ScanHelper   ]: Chip type: RD53A
[15:35:07:266][  info  ][  ScanHelper   ]: Chip count 4
[15:35:07:266][  info  ][  ScanHelper   ]: Loading chip #0
[15:35:07:267][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(4)
[15:35:07:267][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ14/20UPGFC0009300.json
[15:35:07:440][  info  ][  ScanHelper   ]: Loading chip #1
[15:35:07:441][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(5)
[15:35:07:441][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ14/20UPGFC0009316.json
[15:35:07:615][  info  ][  ScanHelper   ]: Loading chip #2
[15:35:07:615][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(6)
[15:35:07:616][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ14/20UPGFC0009332.json
[15:35:07:788][  info  ][  ScanHelper   ]: Loading chip #3
[15:35:07:789][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(7)
[15:35:07:789][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ14/20UPGFC0009348.json
[15:35:07:968][  info  ][  scanConsole  ]: #################
[15:35:07:968][  info  ][  scanConsole  ]: # Configure FEs #
[15:35:07:968][  info  ][  scanConsole  ]: #################
[15:35:07:968][  info  ][  scanConsole  ]: Configuring 20UPGFC0009300
[15:35:07:998][  info  ][  scanConsole  ]: Configuring 20UPGFC0009316
[15:35:08:028][  info  ][  scanConsole  ]: Configuring 20UPGFC0009332
[15:35:08:078][  info  ][  scanConsole  ]: Configuring 20UPGFC0009348
[15:35:08:110][  info  ][  scanConsole  ]: Sent configuration to all FEs in 142 ms!
[15:35:08:111][  info  ][  scanConsole  ]: Checking com 20UPGFC0009300
[15:35:08:111][  info  ][    SpecRx     ]: Active Rx channels: 0x10
[15:35:08:111][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[15:35:08:111][  info  ][    SpecRx     ]: Rx Status 0xf0
[15:35:08:111][  info  ][    SpecRx     ]: Number of lanes: 1
[15:35:08:111][  info  ][    SpecRx     ]: Channel 4 Lane 0 synchronized!
[15:35:08:121][  info  ][  scanConsole  ]: ... success!
[15:35:08:122][  info  ][  scanConsole  ]: Checking com 20UPGFC0009316
[15:35:08:122][  info  ][    SpecRx     ]: Active Rx channels: 0x20
[15:35:08:122][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[15:35:08:122][  info  ][    SpecRx     ]: Rx Status 0xf0
[15:35:08:122][  info  ][    SpecRx     ]: Number of lanes: 1
[15:35:08:122][  info  ][    SpecRx     ]: Channel 5 Lane 0 synchronized!
[15:35:08:132][  info  ][  scanConsole  ]: ... success!
[15:35:08:132][  info  ][  scanConsole  ]: Checking com 20UPGFC0009332
[15:35:08:132][  info  ][    SpecRx     ]: Active Rx channels: 0x40
[15:35:08:132][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[15:35:08:132][  info  ][    SpecRx     ]: Rx Status 0xf0
[15:35:08:132][  info  ][    SpecRx     ]: Number of lanes: 1
[15:35:08:132][  info  ][    SpecRx     ]: Channel 6 Lane 0 synchronized!
[15:35:08:142][  info  ][  scanConsole  ]: ... success!
[15:35:08:142][  info  ][  scanConsole  ]: Checking com 20UPGFC0009348
[15:35:08:142][  info  ][    SpecRx     ]: Active Rx channels: 0x80
[15:35:08:142][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[15:35:08:142][  info  ][    SpecRx     ]: Rx Status 0xf0
[15:35:08:142][  info  ][    SpecRx     ]: Number of lanes: 1
[15:35:08:142][  info  ][    SpecRx     ]: Channel 7 Lane 0 synchronized!
[15:35:08:153][  info  ][  scanConsole  ]: ... success!
[15:35:08:153][  info  ][  scanConsole  ]: Enabling Tx channels
[15:35:08:153][  info  ][  scanConsole  ]: Enabling Tx channel 1
[15:35:08:153][  info  ][  scanConsole  ]: Enabling Tx channel 1
[15:35:08:153][  info  ][  scanConsole  ]: Enabling Tx channel 1
[15:35:08:153][  info  ][  scanConsole  ]: Enabling Tx channel 1
[15:35:08:153][  info  ][  scanConsole  ]: Enabling Rx channels
[15:35:08:153][  info  ][  scanConsole  ]: Enabling Rx channel 4
[15:35:08:153][  info  ][  scanConsole  ]: Enabling Rx channel 5
[15:35:08:153][  info  ][  scanConsole  ]: Enabling Rx channel 6
[15:35:08:153][  info  ][  scanConsole  ]: Enabling Rx channel 7
[15:35:08:153][  info  ][  scanConsole  ]: ##############
[15:35:08:153][  info  ][  scanConsole  ]: # Setup Scan #
[15:35:08:153][  info  ][  scanConsole  ]: ##############
[15:35:08:153][  info  ][  scanConsole  ]: Found Scan config, constructing scan ...
[15:35:08:154][  info  ][  ScanFactory  ]: Loading Scan:
[15:35:08:154][  info  ][  ScanFactory  ]:   Name: AnalogScan
[15:35:08:154][  info  ][  ScanFactory  ]:   Number of Loops: 3
[15:35:08:154][  info  ][  ScanFactory  ]:   Loading Loop #0
[15:35:08:154][  info  ][  ScanFactory  ]:    Type: Rd53aReadRegLoop
[15:35:08:154][  info  ][  ScanFactory  ]:    Loading loop config ... 
[15:35:08:154][  info  ][  ScanFactory  ]: ~~~ {
[15:35:08:154][  info  ][  ScanFactory  ]: ~~~     "CurMux": [],
[15:35:08:154][  info  ][  ScanFactory  ]: ~~~     "EnblRingOsc": 0,
[15:35:08:154][  info  ][  ScanFactory  ]: ~~~     "Registers": [],
[15:35:08:154][  info  ][  ScanFactory  ]: ~~~     "RingOscDur": 9,
[15:35:08:154][  info  ][  ScanFactory  ]: ~~~     "RingOscRep": 10,
[15:35:08:154][  info  ][  ScanFactory  ]: ~~~     "VoltMux": [3, 5, 7, 15]
[15:35:08:154][  info  ][  ScanFactory  ]: ~~~ }
[15:35:08:154][  info  ][  ScanFactory  ]:   Loading Loop #1
[15:35:08:154][  info  ][  ScanFactory  ]:    Type: Rd53aTriggerLoop
[15:35:08:154][  info  ][  ScanFactory  ]:    Loading loop config ... 
[15:35:08:154][  info  ][  ScanFactory  ]: ~~~ {
[15:35:08:154][  info  ][  ScanFactory  ]: ~~~     "count": 1,
[15:35:08:154][  info  ][  ScanFactory  ]: ~~~     "delay": 48,
[15:35:08:154][  info  ][  ScanFactory  ]: ~~~     "extTrig": false,
[15:35:08:154][  info  ][  ScanFactory  ]: ~~~     "frequency": 30000.0,
[15:35:08:154][  info  ][  ScanFactory  ]: ~~~     "noInject": false,
[15:35:08:154][  info  ][  ScanFactory  ]: ~~~     "sendEcr": false,
[15:35:08:154][  info  ][  ScanFactory  ]: ~~~     "time": 0.0,
[15:35:08:154][  info  ][  ScanFactory  ]: ~~~     "trigMultiplier": 16
[15:35:08:154][  info  ][  ScanFactory  ]: ~~~ }
[15:35:08:154][  info  ][  ScanFactory  ]:   Loading Loop #2
[15:35:08:154][  info  ][  ScanFactory  ]:    Type: StdDataLoop
[15:35:08:155][warning ][  ScanFactory  ]: ~~~ Config empty.
[15:35:08:155][  info  ][ScanBuildHistogrammers]: Loading histogrammer ...
[15:35:08:155][  info  ][ScanBuildHistogrammers]: ... done!
[15:35:08:155][  info  ][ScanBuildAnalyses]: Loading analyses ...
[15:35:08:155][  info  ][  scanConsole  ]: Running pre scan!
[15:35:08:155][  info  ][  ScanFactory  ]: Entering pre scan phase ...
[15:35:08:155][  info  ][     Rd53a     ]: Write named register: GlobalPulseRt -> 16384
[15:35:08:155][  info  ][     Rd53a     ]: Write named register: InjEnDig -> 0
[15:35:08:155][  info  ][     Rd53a     ]: Write named register: InjVcalDiff -> 0
[15:35:08:155][  info  ][     Rd53a     ]: Write named register: InjVcalHigh -> 3500
[15:35:08:155][  info  ][     Rd53a     ]: Write named register: InjVcalMed -> 500
[15:35:08:155][  info  ][     Rd53a     ]: Write named register: LatencyConfig -> 48
[15:35:08:156][  info  ][  scanConsole  ]: Starting histogrammer and analysis threads:
[15:35:08:156][  info  ][  scanConsole  ]:  .. started threads of Fe 4
[15:35:08:156][  info  ][  scanConsole  ]:  .. started threads of Fe 5
[15:35:08:156][  info  ][  scanConsole  ]:  .. started threads of Fe 6
[15:35:08:156][  info  ][  scanConsole  ]:  .. started threads of Fe 7
[15:35:08:156][  info  ][Rd53aDataProcessor]:   -> Processor thread #0 started!
[15:35:08:156][  info  ][Rd53aDataProcessor]:   -> Processor thread #1 started!
[15:35:08:156][  info  ][Rd53aDataProcessor]:   -> Processor thread #2 started!
[15:35:08:156][  info  ][Rd53aDataProcessor]:   -> Processor thread #3 started!
[15:35:08:156][  info  ][  scanConsole  ]: ########
[15:35:08:156][  info  ][  scanConsole  ]: # Scan #
[15:35:08:156][  info  ][  scanConsole  ]: ########
[15:35:08:157][  info  ][  scanConsole  ]: Starting scan!
[15:35:08:157][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0009300 on Rx 4
[15:35:08:161][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0009300] MON MUX_V: 3 Bias 0 1325 -> 0.286307 V Bias 1 1711 -> 0.36246 V, Temperature 0.0761524 -> -11.3615 C
[15:35:08:165][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0009300] MON MUX_V: 5 Bias 0 1275 -> 0.276443 V Bias 1 1665 -> 0.353384 V, Temperature 0.0769416 -> -7.33289 C
[15:35:08:169][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0009300] MON MUX_V: 7 Bias 0 1264 -> 0.274273 V Bias 1 1658 -> 0.352003 V, Temperature 0.0777307 -> -15.164 C
[15:35:08:173][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0009300] MON MUX_V: 15 Bias 0 1248 -> 0.271116 V Bias 1 1647 -> 0.349833 V, Temperature 0.0787172 -> -15.6921 C
[15:35:08:208][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0009316 on Rx 5
[15:35:08:212][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0009316] MON MUX_V: 3 Bias 0 1316 -> 0.282871 V Bias 1 1698 -> 0.358966 V, Temperature 0.0760944 -> -11.5679 C
[15:35:08:217][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0009316] MON MUX_V: 5 Bias 0 1289 -> 0.277493 V Bias 1 1670 -> 0.353388 V, Temperature 0.0758952 -> -13.3151 C
[15:35:08:221][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0009316] MON MUX_V: 7 Bias 0 1265 -> 0.272712 V Bias 1 1660 -> 0.351396 V, Temperature 0.078684 -> -11.2322 C
[15:35:08:225][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0009316] MON MUX_V: 15 Bias 0 1258 -> 0.271318 V Bias 1 1645 -> 0.348408 V, Temperature 0.0770904 -> -12.7572 C
[15:35:08:260][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0009332 on Rx 6
[15:35:08:265][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0009332] MON MUX_V: 3 Bias 0 1292 -> 0.283533 V Bias 1 1674 -> 0.36001 V, Temperature 0.0764764 -> -13.5295 C
[15:35:08:269][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0009332] MON MUX_V: 5 Bias 0 1242 -> 0.273523 V Bias 1 1630 -> 0.351201 V, Temperature 0.0776776 -> -13.549 C
[15:35:08:273][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0009332] MON MUX_V: 7 Bias 0 1247 -> 0.274524 V Bias 1 1635 -> 0.352202 V, Temperature 0.0776776 -> -13.5492 C
[15:35:08:278][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0009332] MON MUX_V: 15 Bias 0 1221 -> 0.269319 V Bias 1 1618 -> 0.348799 V, Temperature 0.0794794 -> -13.4081 C
[15:35:08:312][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0009348 on Rx 7
[15:35:08:317][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0009348] MON MUX_V: 3 Bias 0 1293 -> 0.279426 V Bias 1 1670 -> 0.355052 V, Temperature 0.0756262 -> -13.498 C
[15:35:08:321][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0009348] MON MUX_V: 5 Bias 0 1250 -> 0.2708 V Bias 1 1626 -> 0.346225 V, Temperature 0.0754256 -> -11.9783 C
[15:35:08:325][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0009348] MON MUX_V: 7 Bias 0 1223 -> 0.265384 V Bias 1 1615 -> 0.344019 V, Temperature 0.0786352 -> -8.78479 C
[15:35:08:329][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0009348] MON MUX_V: 15 Bias 0 1232 -> 0.267189 V Bias 1 1624 -> 0.345824 V, Temperature 0.0786352 -> -9.61047 C
[15:35:08:365][  info  ][  scanConsole  ]: Scan done!
[15:35:08:365][  info  ][  scanConsole  ]: Waiting for processors to finish ...
[15:35:08:366][  info  ][  scanConsole  ]: Processor done, waiting for histogrammer ...
[15:35:08:766][  info  ][HistogramAlgorithm]: Histogrammer done!
[15:35:08:766][  info  ][HistogramAlgorithm]: Histogrammer done!
[15:35:08:766][  info  ][HistogramAlgorithm]: Histogrammer done!
[15:35:08:766][  info  ][HistogramAlgorithm]: Histogrammer done!
[15:35:08:766][  info  ][  scanConsole  ]: Processor done, waiting for analysis ...
[15:35:09:166][  info  ][AnalysisAlgorithm]: Analysis done!
[15:35:09:166][  info  ][AnalysisAlgorithm]: Analysis done!
[15:35:09:166][  info  ][AnalysisAlgorithm]: Analysis done!
[15:35:09:166][  info  ][AnalysisAlgorithm]: Analysis done!
[15:35:09:167][  info  ][  scanConsole  ]: All done!
[15:35:09:167][  info  ][  scanConsole  ]: ##########
[15:35:09:167][  info  ][  scanConsole  ]: # Timing #
[15:35:09:167][  info  ][  scanConsole  ]: ##########
[15:35:09:167][  info  ][  scanConsole  ]: -> Configuration: 142 ms
[15:35:09:167][  info  ][  scanConsole  ]: -> Scan:          208 ms
[15:35:09:167][  info  ][  scanConsole  ]: -> Processing:    0 ms
[15:35:09:167][  info  ][  scanConsole  ]: -> Analysis:      800 ms
[15:35:09:167][  info  ][  scanConsole  ]: ###########
[15:35:09:167][  info  ][  scanConsole  ]: # Cleanup #
[15:35:09:167][  info  ][  scanConsole  ]: ###########
[15:35:09:867][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0009300 to configs/KEKQ14/20UPGFC0009300.json
[15:35:10:036][  info  ][  scanConsole  ]: -> Storing output results of FE 4
[15:35:10:037][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0009300, this usually means that the chip did not send any data at all.
[15:35:10:038][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0009316 to configs/KEKQ14/20UPGFC0009316.json
[15:35:10:199][  info  ][  scanConsole  ]: -> Storing output results of FE 5
[15:35:10:199][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0009316, this usually means that the chip did not send any data at all.
[15:35:10:201][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0009332 to configs/KEKQ14/20UPGFC0009332.json
[15:35:10:364][  info  ][  scanConsole  ]: -> Storing output results of FE 6
[15:35:10:364][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0009332, this usually means that the chip did not send any data at all.
[15:35:10:365][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0009348 to configs/KEKQ14/20UPGFC0009348.json
[15:35:10:529][  info  ][  scanConsole  ]: -> Storing output results of FE 7
[15:35:10:529][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0009348, this usually means that the chip did not send any data at all.
[15:35:10:531][  info  ][  scanConsole  ]: Finishing run: 1774
20UPGFC0009300.json.after
20UPGFC0009300.json.before
20UPGFC0009316.json.after
20UPGFC0009316.json.before
20UPGFC0009332.json.after
20UPGFC0009332.json.before
20UPGFC0009348.json.after
20UPGFC0009348.json.before
reg_readtemp.json
scanLog.json

