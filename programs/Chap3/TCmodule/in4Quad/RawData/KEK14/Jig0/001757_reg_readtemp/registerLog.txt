[14:03:14:610][  info  ][               ]: #####################################
[14:03:14:610][  info  ][               ]: # Welcome to the YARR Scan Console! #
[14:03:14:610][  info  ][               ]: #####################################
[14:03:14:610][  info  ][               ]: -> Parsing command line parameters ...
[14:03:14:610][  info  ][               ]: Configuring logger ...
[14:03:14:612][  info  ][  scanConsole  ]: Scan Type/Config configs/scans/rd53a/reg_readtemp.json
[14:03:14:612][  info  ][  scanConsole  ]: Connectivity:
[14:03:14:612][  info  ][  scanConsole  ]:     configs/KEKQ14/connectivity.json
[14:03:14:612][  info  ][  scanConsole  ]: Target ToT: -1
[14:03:14:612][  info  ][  scanConsole  ]: Target Charge: -1
[14:03:14:612][  info  ][  scanConsole  ]: Output Plots: true
[14:03:14:612][  info  ][  scanConsole  ]: Output Directory: ./data/001757_reg_readtemp/
[14:03:14:616][  info  ][  scanConsole  ]: Timestamp: 2022-08-24_14:03:14
[14:03:14:616][  info  ][  scanConsole  ]: Run Number: 1757
[14:03:14:616][  info  ][  scanConsole  ]: #################
[14:03:14:616][  info  ][  scanConsole  ]: # Init Hardware #
[14:03:14:616][  info  ][  scanConsole  ]: #################
[14:03:14:616][  info  ][  scanConsole  ]: -> Opening controller config: configs/controller/specCfg-rd53a-16x1.json
[14:03:14:617][  info  ][  ScanHelper   ]: Loading controller ...
[14:03:14:617][  info  ][  ScanHelper   ]: Found controller of type: spec
[14:03:14:617][  info  ][  ScanHelper   ]: ... loading controler config:
[14:03:14:617][  info  ][  ScanHelper   ]: ~~~ {
[14:03:14:617][  info  ][  ScanHelper   ]: ~~~     "cmdPeriod": 6.250000073038109e-9,
[14:03:14:617][  info  ][  ScanHelper   ]: ~~~     "idle": {
[14:03:14:617][  info  ][  ScanHelper   ]: ~~~         "word": 1768515945
[14:03:14:617][  info  ][  ScanHelper   ]: ~~~     },
[14:03:14:617][  info  ][  ScanHelper   ]: ~~~     "pulse": {
[14:03:14:617][  info  ][  ScanHelper   ]: ~~~         "interval": 500,
[14:03:14:617][  info  ][  ScanHelper   ]: ~~~         "word": 1549575846
[14:03:14:617][  info  ][  ScanHelper   ]: ~~~     },
[14:03:14:617][  info  ][  ScanHelper   ]: ~~~     "rxActiveLanes": 1,
[14:03:14:617][  info  ][  ScanHelper   ]: ~~~     "rxPolarity": 0,
[14:03:14:617][  info  ][  ScanHelper   ]: ~~~     "specNum": 0,
[14:03:14:617][  info  ][  ScanHelper   ]: ~~~     "spiConfig": 541200,
[14:03:14:617][  info  ][  ScanHelper   ]: ~~~     "sync": {
[14:03:14:617][  info  ][  ScanHelper   ]: ~~~         "interval": 32,
[14:03:14:617][  info  ][  ScanHelper   ]: ~~~         "word": 2172551550
[14:03:14:617][  info  ][  ScanHelper   ]: ~~~     },
[14:03:14:617][  info  ][  ScanHelper   ]: ~~~     "txPolarity": 0
[14:03:14:617][  info  ][  ScanHelper   ]: ~~~ }
[14:03:14:617][  info  ][    SpecCom    ]: Opening SPEC with id #0
[14:03:14:617][  info  ][    SpecCom    ]: Mapping BARs ...
[14:03:14:617][  info  ][    SpecCom    ]: ... Mapped BAR0 at 0x7f97bb677000 with size 1048576
[14:03:14:617][warning ][    SpecCom    ]: ... BAR4 not mapped (Mmap failed)
[14:03:14:617][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[14:03:14:617][  info  ][    SpecCom    ]: Firmware Version: 0x4d9ff6d
[14:03:14:617][  info  ][    SpecCom    ]: Firmware Identifier: 0x4030232
[14:03:14:617][  info  ][    SpecCom    ]: FPGA card: PLDA XpressK7 325
[14:03:14:617][  info  ][    SpecCom    ]: FE Chip Type: RD53A/B
[14:03:14:617][  info  ][    SpecCom    ]: FMC Card Type: Ohio Card (Display Port)
[14:03:14:617][  info  ][    SpecCom    ]: RX Speed: 640Mbps
[14:03:14:617][  info  ][    SpecCom    ]: Channel Configuration: 16x1
[14:03:14:617][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[14:03:14:617][  info  ][    SpecCom    ]: Flushing buffers ...
[14:03:14:617][  info  ][    SpecCom    ]: Init success!
[14:03:14:617][  info  ][  scanConsole  ]: #######################
[14:03:14:617][  info  ][  scanConsole  ]: ##  Loading Configs  ##
[14:03:14:617][  info  ][  scanConsole  ]: #######################
[14:03:14:617][  info  ][  scanConsole  ]: Opening global config: configs/KEKQ14/connectivity.json
[14:03:14:618][  info  ][  ScanHelper   ]: Chip type: RD53A
[14:03:14:618][  info  ][  ScanHelper   ]: Chip count 4
[14:03:14:618][  info  ][  ScanHelper   ]: Loading chip #0
[14:03:14:618][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(4)
[14:03:14:619][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ14/20UPGFC0009300.json
[14:03:14:793][  info  ][  ScanHelper   ]: Loading chip #1
[14:03:14:793][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(5)
[14:03:14:793][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ14/20UPGFC0009316.json
[14:03:14:967][  info  ][  ScanHelper   ]: Loading chip #2
[14:03:14:968][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(6)
[14:03:14:968][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ14/20UPGFC0009332.json
[14:03:15:143][  info  ][  ScanHelper   ]: Loading chip #3
[14:03:15:144][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(7)
[14:03:15:144][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ14/20UPGFC0009348.json
[14:03:15:323][  info  ][  scanConsole  ]: #################
[14:03:15:323][  info  ][  scanConsole  ]: # Configure FEs #
[14:03:15:323][  info  ][  scanConsole  ]: #################
[14:03:15:323][  info  ][  scanConsole  ]: Configuring 20UPGFC0009300
[14:03:15:353][  info  ][  scanConsole  ]: Configuring 20UPGFC0009316
[14:03:15:383][  info  ][  scanConsole  ]: Configuring 20UPGFC0009332
[14:03:15:415][  info  ][  scanConsole  ]: Configuring 20UPGFC0009348
[14:03:15:464][  info  ][  scanConsole  ]: Sent configuration to all FEs in 141 ms!
[14:03:15:465][  info  ][  scanConsole  ]: Checking com 20UPGFC0009300
[14:03:15:465][  info  ][    SpecRx     ]: Active Rx channels: 0x10
[14:03:15:465][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[14:03:15:465][  info  ][    SpecRx     ]: Rx Status 0xf0
[14:03:15:465][  info  ][    SpecRx     ]: Number of lanes: 1
[14:03:15:465][  info  ][    SpecRx     ]: Channel 4 Lane 0 synchronized!
[14:03:15:475][  info  ][  scanConsole  ]: ... success!
[14:03:15:476][  info  ][  scanConsole  ]: Checking com 20UPGFC0009316
[14:03:15:476][  info  ][    SpecRx     ]: Active Rx channels: 0x20
[14:03:15:476][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[14:03:15:476][  info  ][    SpecRx     ]: Rx Status 0xf0
[14:03:15:476][  info  ][    SpecRx     ]: Number of lanes: 1
[14:03:15:476][  info  ][    SpecRx     ]: Channel 5 Lane 0 synchronized!
[14:03:15:486][  info  ][  scanConsole  ]: ... success!
[14:03:15:486][  info  ][  scanConsole  ]: Checking com 20UPGFC0009332
[14:03:15:486][  info  ][    SpecRx     ]: Active Rx channels: 0x40
[14:03:15:486][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[14:03:15:486][  info  ][    SpecRx     ]: Rx Status 0xf0
[14:03:15:486][  info  ][    SpecRx     ]: Number of lanes: 1
[14:03:15:486][  info  ][    SpecRx     ]: Channel 6 Lane 0 synchronized!
[14:03:15:496][  info  ][  scanConsole  ]: ... success!
[14:03:15:496][  info  ][  scanConsole  ]: Checking com 20UPGFC0009348
[14:03:15:497][  info  ][    SpecRx     ]: Active Rx channels: 0x80
[14:03:15:497][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[14:03:15:497][  info  ][    SpecRx     ]: Rx Status 0xf0
[14:03:15:497][  info  ][    SpecRx     ]: Number of lanes: 1
[14:03:15:497][  info  ][    SpecRx     ]: Channel 7 Lane 0 synchronized!
[14:03:15:507][  info  ][  scanConsole  ]: ... success!
[14:03:15:507][  info  ][  scanConsole  ]: Enabling Tx channels
[14:03:15:507][  info  ][  scanConsole  ]: Enabling Tx channel 1
[14:03:15:507][  info  ][  scanConsole  ]: Enabling Tx channel 1
[14:03:15:507][  info  ][  scanConsole  ]: Enabling Tx channel 1
[14:03:15:507][  info  ][  scanConsole  ]: Enabling Tx channel 1
[14:03:15:507][  info  ][  scanConsole  ]: Enabling Rx channels
[14:03:15:507][  info  ][  scanConsole  ]: Enabling Rx channel 4
[14:03:15:507][  info  ][  scanConsole  ]: Enabling Rx channel 5
[14:03:15:507][  info  ][  scanConsole  ]: Enabling Rx channel 6
[14:03:15:507][  info  ][  scanConsole  ]: Enabling Rx channel 7
[14:03:15:507][  info  ][  scanConsole  ]: ##############
[14:03:15:507][  info  ][  scanConsole  ]: # Setup Scan #
[14:03:15:507][  info  ][  scanConsole  ]: ##############
[14:03:15:507][  info  ][  scanConsole  ]: Found Scan config, constructing scan ...
[14:03:15:507][  info  ][  ScanFactory  ]: Loading Scan:
[14:03:15:507][  info  ][  ScanFactory  ]:   Name: AnalogScan
[14:03:15:507][  info  ][  ScanFactory  ]:   Number of Loops: 3
[14:03:15:507][  info  ][  ScanFactory  ]:   Loading Loop #0
[14:03:15:507][  info  ][  ScanFactory  ]:    Type: Rd53aReadRegLoop
[14:03:15:507][  info  ][  ScanFactory  ]:    Loading loop config ... 
[14:03:15:507][  info  ][  ScanFactory  ]: ~~~ {
[14:03:15:507][  info  ][  ScanFactory  ]: ~~~     "CurMux": [],
[14:03:15:507][  info  ][  ScanFactory  ]: ~~~     "EnblRingOsc": 0,
[14:03:15:507][  info  ][  ScanFactory  ]: ~~~     "Registers": [],
[14:03:15:507][  info  ][  ScanFactory  ]: ~~~     "RingOscDur": 9,
[14:03:15:507][  info  ][  ScanFactory  ]: ~~~     "RingOscRep": 10,
[14:03:15:507][  info  ][  ScanFactory  ]: ~~~     "VoltMux": [3, 5, 7, 15]
[14:03:15:507][  info  ][  ScanFactory  ]: ~~~ }
[14:03:15:507][  info  ][  ScanFactory  ]:   Loading Loop #1
[14:03:15:507][  info  ][  ScanFactory  ]:    Type: Rd53aTriggerLoop
[14:03:15:507][  info  ][  ScanFactory  ]:    Loading loop config ... 
[14:03:15:507][  info  ][  ScanFactory  ]: ~~~ {
[14:03:15:507][  info  ][  ScanFactory  ]: ~~~     "count": 1,
[14:03:15:507][  info  ][  ScanFactory  ]: ~~~     "delay": 48,
[14:03:15:507][  info  ][  ScanFactory  ]: ~~~     "extTrig": false,
[14:03:15:507][  info  ][  ScanFactory  ]: ~~~     "frequency": 30000.0,
[14:03:15:507][  info  ][  ScanFactory  ]: ~~~     "noInject": false,
[14:03:15:507][  info  ][  ScanFactory  ]: ~~~     "sendEcr": false,
[14:03:15:507][  info  ][  ScanFactory  ]: ~~~     "time": 0.0,
[14:03:15:507][  info  ][  ScanFactory  ]: ~~~     "trigMultiplier": 16
[14:03:15:507][  info  ][  ScanFactory  ]: ~~~ }
[14:03:15:507][  info  ][  ScanFactory  ]:   Loading Loop #2
[14:03:15:507][  info  ][  ScanFactory  ]:    Type: StdDataLoop
[14:03:15:507][warning ][  ScanFactory  ]: ~~~ Config empty.
[14:03:15:507][  info  ][ScanBuildHistogrammers]: Loading histogrammer ...
[14:03:15:507][  info  ][ScanBuildHistogrammers]: ... done!
[14:03:15:507][  info  ][ScanBuildAnalyses]: Loading analyses ...
[14:03:15:507][  info  ][  scanConsole  ]: Running pre scan!
[14:03:15:507][  info  ][  ScanFactory  ]: Entering pre scan phase ...
[14:03:15:507][  info  ][     Rd53a     ]: Write named register: GlobalPulseRt -> 16384
[14:03:15:507][  info  ][     Rd53a     ]: Write named register: InjEnDig -> 0
[14:03:15:507][  info  ][     Rd53a     ]: Write named register: InjVcalDiff -> 0
[14:03:15:507][  info  ][     Rd53a     ]: Write named register: InjVcalHigh -> 3500
[14:03:15:507][  info  ][     Rd53a     ]: Write named register: InjVcalMed -> 500
[14:03:15:507][  info  ][     Rd53a     ]: Write named register: LatencyConfig -> 48
[14:03:15:508][  info  ][  scanConsole  ]: Starting histogrammer and analysis threads:
[14:03:15:508][  info  ][  scanConsole  ]:  .. started threads of Fe 4
[14:03:15:508][  info  ][  scanConsole  ]:  .. started threads of Fe 5
[14:03:15:508][  info  ][  scanConsole  ]:  .. started threads of Fe 6
[14:03:15:508][  info  ][  scanConsole  ]:  .. started threads of Fe 7
[14:03:15:508][  info  ][Rd53aDataProcessor]:   -> Processor thread #0 started!
[14:03:15:508][  info  ][Rd53aDataProcessor]:   -> Processor thread #1 started!
[14:03:15:508][  info  ][Rd53aDataProcessor]:   -> Processor thread #2 started!
[14:03:15:508][  info  ][Rd53aDataProcessor]:   -> Processor thread #3 started!
[14:03:15:508][  info  ][  scanConsole  ]: ########
[14:03:15:508][  info  ][  scanConsole  ]: # Scan #
[14:03:15:508][  info  ][  scanConsole  ]: ########
[14:03:15:508][  info  ][  scanConsole  ]: Starting scan!
[14:03:15:508][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0009300 on Rx 4
[14:03:15:512][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0009300] MON MUX_V: 3 Bias 0 1240 -> 0.269538 V Bias 1 1647 -> 0.349833 V, Temperature 0.0802955 -> 2.88095 C
[14:03:15:516][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0009300] MON MUX_V: 5 Bias 0 1186 -> 0.258884 V Bias 1 1598 -> 0.340166 V, Temperature 0.0812819 -> 7.6619 C
[14:03:15:519][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0009300] MON MUX_V: 7 Bias 0 1175 -> 0.256714 V Bias 1 1592 -> 0.338983 V, Temperature 0.0822683 -> -0.103882 C
[14:03:15:523][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0009300] MON MUX_V: 15 Bias 0 1161 -> 0.253952 V Bias 1 1579 -> 0.336418 V, Temperature 0.0824656 -> -3.43219 C
[14:03:15:558][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0009316 on Rx 5
[14:03:15:566][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0009316] MON MUX_V: 3 Bias 0 1233 -> 0.266338 V Bias 1 1635 -> 0.346416 V, Temperature 0.0800784 -> 2.12759 C
[14:03:15:570][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0009316] MON MUX_V: 5 Bias 0 1203 -> 0.260362 V Bias 1 1606 -> 0.340639 V, Temperature 0.0802776 -> 1.68848 C
[14:03:15:575][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0009316] MON MUX_V: 7 Bias 0 1178 -> 0.255382 V Bias 1 1594 -> 0.338249 V, Temperature 0.0828672 -> 2.69257 C
[14:03:15:579][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0009316] MON MUX_V: 15 Bias 0 1170 -> 0.253788 V Bias 1 1580 -> 0.33546 V, Temperature 0.081672 -> 2.71826 C
[14:03:15:613][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0009332 on Rx 6
[14:03:15:617][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0009332] MON MUX_V: 3 Bias 0 1213 -> 0.267718 V Bias 1 1615 -> 0.348198 V, Temperature 0.0804804 -> 0.0632629 C
[14:03:15:621][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0009332] MON MUX_V: 5 Bias 0 1160 -> 0.257107 V Bias 1 1571 -> 0.339389 V, Temperature 0.0822822 -> 1.83963 C
[14:03:15:625][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0009332] MON MUX_V: 7 Bias 0 1162 -> 0.257507 V Bias 1 1573 -> 0.33979 V, Temperature 0.0822822 -> 1.83963 C
[14:03:15:629][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0009332] MON MUX_V: 15 Bias 0 1138 -> 0.252703 V Bias 1 1558 -> 0.336787 V, Temperature 0.084084 -> 1.63998 C
[14:03:15:663][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0009348 on Rx 7
[14:03:15:667][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0009348] MON MUX_V: 3 Bias 0 1210 -> 0.262776 V Bias 1 1609 -> 0.342815 V, Temperature 0.0800394 -> 1.65424 C
[14:03:15:671][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0009348] MON MUX_V: 5 Bias 0 1164 -> 0.253548 V Bias 1 1564 -> 0.333788 V, Temperature 0.08024 -> 4.69238 C
[14:03:15:675][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0009348] MON MUX_V: 7 Bias 0 1137 -> 0.248132 V Bias 1 1550 -> 0.33098 V, Temperature 0.0828478 -> 5.37769 C
[14:03:15:679][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0009348] MON MUX_V: 15 Bias 0 1144 -> 0.249536 V Bias 1 1559 -> 0.332785 V, Temperature 0.083249 -> 5.85239 C
[14:03:15:714][  info  ][  scanConsole  ]: Scan done!
[14:03:15:714][  info  ][  scanConsole  ]: Waiting for processors to finish ...
[14:03:15:715][  info  ][  scanConsole  ]: Processor done, waiting for histogrammer ...
[14:03:16:114][  info  ][HistogramAlgorithm]: Histogrammer done!
[14:03:16:114][  info  ][HistogramAlgorithm]: Histogrammer done!
[14:03:16:114][  info  ][HistogramAlgorithm]: Histogrammer done!
[14:03:16:114][  info  ][HistogramAlgorithm]: Histogrammer done!
[14:03:16:115][  info  ][  scanConsole  ]: Processor done, waiting for analysis ...
[14:03:16:515][  info  ][AnalysisAlgorithm]: Analysis done!
[14:03:16:515][  info  ][AnalysisAlgorithm]: Analysis done!
[14:03:16:515][  info  ][AnalysisAlgorithm]: Analysis done!
[14:03:16:515][  info  ][AnalysisAlgorithm]: Analysis done!
[14:03:16:515][  info  ][  scanConsole  ]: All done!
[14:03:16:515][  info  ][  scanConsole  ]: ##########
[14:03:16:515][  info  ][  scanConsole  ]: # Timing #
[14:03:16:516][  info  ][  scanConsole  ]: ##########
[14:03:16:516][  info  ][  scanConsole  ]: -> Configuration: 141 ms
[14:03:16:516][  info  ][  scanConsole  ]: -> Scan:          206 ms
[14:03:16:516][  info  ][  scanConsole  ]: -> Processing:    0 ms
[14:03:16:516][  info  ][  scanConsole  ]: -> Analysis:      800 ms
[14:03:16:516][  info  ][  scanConsole  ]: ###########
[14:03:16:516][  info  ][  scanConsole  ]: # Cleanup #
[14:03:16:516][  info  ][  scanConsole  ]: ###########
[14:03:16:523][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0009300 to configs/KEKQ14/20UPGFC0009300.json
[14:03:16:726][  info  ][  scanConsole  ]: -> Storing output results of FE 4
[14:03:16:726][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0009300, this usually means that the chip did not send any data at all.
[14:03:16:727][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0009316 to configs/KEKQ14/20UPGFC0009316.json
[14:03:16:894][  info  ][  scanConsole  ]: -> Storing output results of FE 5
[14:03:16:894][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0009316, this usually means that the chip did not send any data at all.
[14:03:16:895][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0009332 to configs/KEKQ14/20UPGFC0009332.json
[14:03:17:058][  info  ][  scanConsole  ]: -> Storing output results of FE 6
[14:03:17:058][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0009332, this usually means that the chip did not send any data at all.
[14:03:17:059][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0009348 to configs/KEKQ14/20UPGFC0009348.json
[14:03:17:225][  info  ][  scanConsole  ]: -> Storing output results of FE 7
[14:03:17:225][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0009348, this usually means that the chip did not send any data at all.
[14:03:17:226][  info  ][  scanConsole  ]: Finishing run: 1757
20UPGFC0009300.json.after
20UPGFC0009300.json.before
20UPGFC0009316.json.after
20UPGFC0009316.json.before
20UPGFC0009332.json.after
20UPGFC0009332.json.before
20UPGFC0009348.json.after
20UPGFC0009348.json.before
reg_readtemp.json
scanLog.json

