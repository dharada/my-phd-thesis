[13:58:36:840][  info  ][               ]: #####################################
[13:58:36:840][  info  ][               ]: # Welcome to the YARR Scan Console! #
[13:58:36:840][  info  ][               ]: #####################################
[13:58:36:840][  info  ][               ]: -> Parsing command line parameters ...
[13:58:36:840][  info  ][               ]: Configuring logger ...
[13:58:36:842][  info  ][  scanConsole  ]: Scan Type/Config configs/scans/rd53a/reg_readtemp.json
[13:58:36:842][  info  ][  scanConsole  ]: Connectivity:
[13:58:36:842][  info  ][  scanConsole  ]:     configs/KEKQ14/connectivity.json
[13:58:36:842][  info  ][  scanConsole  ]: Target ToT: -1
[13:58:36:842][  info  ][  scanConsole  ]: Target Charge: -1
[13:58:36:842][  info  ][  scanConsole  ]: Output Plots: true
[13:58:36:842][  info  ][  scanConsole  ]: Output Directory: ./data/001754_reg_readtemp/
[13:58:36:847][  info  ][  scanConsole  ]: Timestamp: 2022-08-24_13:58:36
[13:58:36:847][  info  ][  scanConsole  ]: Run Number: 1754
[13:58:36:847][  info  ][  scanConsole  ]: #################
[13:58:36:847][  info  ][  scanConsole  ]: # Init Hardware #
[13:58:36:847][  info  ][  scanConsole  ]: #################
[13:58:36:847][  info  ][  scanConsole  ]: -> Opening controller config: configs/controller/specCfg-rd53a-16x1.json
[13:58:36:847][  info  ][  ScanHelper   ]: Loading controller ...
[13:58:36:847][  info  ][  ScanHelper   ]: Found controller of type: spec
[13:58:36:847][  info  ][  ScanHelper   ]: ... loading controler config:
[13:58:36:847][  info  ][  ScanHelper   ]: ~~~ {
[13:58:36:847][  info  ][  ScanHelper   ]: ~~~     "cmdPeriod": 6.250000073038109e-9,
[13:58:36:847][  info  ][  ScanHelper   ]: ~~~     "idle": {
[13:58:36:847][  info  ][  ScanHelper   ]: ~~~         "word": 1768515945
[13:58:36:847][  info  ][  ScanHelper   ]: ~~~     },
[13:58:36:847][  info  ][  ScanHelper   ]: ~~~     "pulse": {
[13:58:36:847][  info  ][  ScanHelper   ]: ~~~         "interval": 500,
[13:58:36:847][  info  ][  ScanHelper   ]: ~~~         "word": 1549575846
[13:58:36:847][  info  ][  ScanHelper   ]: ~~~     },
[13:58:36:847][  info  ][  ScanHelper   ]: ~~~     "rxActiveLanes": 1,
[13:58:36:847][  info  ][  ScanHelper   ]: ~~~     "rxPolarity": 0,
[13:58:36:847][  info  ][  ScanHelper   ]: ~~~     "specNum": 0,
[13:58:36:847][  info  ][  ScanHelper   ]: ~~~     "spiConfig": 541200,
[13:58:36:847][  info  ][  ScanHelper   ]: ~~~     "sync": {
[13:58:36:847][  info  ][  ScanHelper   ]: ~~~         "interval": 32,
[13:58:36:847][  info  ][  ScanHelper   ]: ~~~         "word": 2172551550
[13:58:36:847][  info  ][  ScanHelper   ]: ~~~     },
[13:58:36:847][  info  ][  ScanHelper   ]: ~~~     "txPolarity": 0
[13:58:36:847][  info  ][  ScanHelper   ]: ~~~ }
[13:58:36:847][  info  ][    SpecCom    ]: Opening SPEC with id #0
[13:58:36:848][  info  ][    SpecCom    ]: Mapping BARs ...
[13:58:36:848][  info  ][    SpecCom    ]: ... Mapped BAR0 at 0x7f8fd37ce000 with size 1048576
[13:58:36:848][warning ][    SpecCom    ]: ... BAR4 not mapped (Mmap failed)
[13:58:36:848][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[13:58:36:848][  info  ][    SpecCom    ]: Firmware Version: 0x4d9ff6d
[13:58:36:848][  info  ][    SpecCom    ]: Firmware Identifier: 0x4030232
[13:58:36:848][  info  ][    SpecCom    ]: FPGA card: PLDA XpressK7 325
[13:58:36:848][  info  ][    SpecCom    ]: FE Chip Type: RD53A/B
[13:58:36:848][  info  ][    SpecCom    ]: FMC Card Type: Ohio Card (Display Port)
[13:58:36:848][  info  ][    SpecCom    ]: RX Speed: 640Mbps
[13:58:36:848][  info  ][    SpecCom    ]: Channel Configuration: 16x1
[13:58:36:848][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[13:58:36:848][  info  ][    SpecCom    ]: Flushing buffers ...
[13:58:36:848][  info  ][    SpecCom    ]: Init success!
[13:58:36:848][  info  ][  scanConsole  ]: #######################
[13:58:36:848][  info  ][  scanConsole  ]: ##  Loading Configs  ##
[13:58:36:848][  info  ][  scanConsole  ]: #######################
[13:58:36:848][  info  ][  scanConsole  ]: Opening global config: configs/KEKQ14/connectivity.json
[13:58:36:848][  info  ][  ScanHelper   ]: Chip type: RD53A
[13:58:36:848][  info  ][  ScanHelper   ]: Chip count 4
[13:58:36:848][  info  ][  ScanHelper   ]: Loading chip #0
[13:58:36:849][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(4)
[13:58:36:849][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ14/20UPGFC0009300.json
[13:58:37:023][  info  ][  ScanHelper   ]: Loading chip #1
[13:58:37:023][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(5)
[13:58:37:024][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ14/20UPGFC0009316.json
[13:58:37:198][  info  ][  ScanHelper   ]: Loading chip #2
[13:58:37:199][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(6)
[13:58:37:199][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ14/20UPGFC0009332.json
[13:58:37:373][  info  ][  ScanHelper   ]: Loading chip #3
[13:58:37:373][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(7)
[13:58:37:373][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ14/20UPGFC0009348.json
[13:58:37:555][  info  ][  scanConsole  ]: #################
[13:58:37:555][  info  ][  scanConsole  ]: # Configure FEs #
[13:58:37:555][  info  ][  scanConsole  ]: #################
[13:58:37:555][  info  ][  scanConsole  ]: Configuring 20UPGFC0009300
[13:58:37:587][  info  ][  scanConsole  ]: Configuring 20UPGFC0009316
[13:58:37:623][  info  ][  scanConsole  ]: Configuring 20UPGFC0009332
[13:58:37:673][  info  ][  scanConsole  ]: Configuring 20UPGFC0009348
[13:58:37:708][  info  ][  scanConsole  ]: Sent configuration to all FEs in 153 ms!
[13:58:37:710][  info  ][  scanConsole  ]: Checking com 20UPGFC0009300
[13:58:37:710][  info  ][    SpecRx     ]: Active Rx channels: 0x10
[13:58:37:710][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[13:58:37:710][  info  ][    SpecRx     ]: Rx Status 0xf0
[13:58:37:710][  info  ][    SpecRx     ]: Number of lanes: 1
[13:58:37:710][  info  ][    SpecRx     ]: Channel 4 Lane 0 synchronized!
[13:58:37:720][  info  ][  scanConsole  ]: ... success!
[13:58:37:720][  info  ][  scanConsole  ]: Checking com 20UPGFC0009316
[13:58:37:720][  info  ][    SpecRx     ]: Active Rx channels: 0x20
[13:58:37:720][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[13:58:37:720][  info  ][    SpecRx     ]: Rx Status 0xf0
[13:58:37:720][  info  ][    SpecRx     ]: Number of lanes: 1
[13:58:37:720][  info  ][    SpecRx     ]: Channel 5 Lane 0 synchronized!
[13:58:37:731][  info  ][  scanConsole  ]: ... success!
[13:58:37:731][  info  ][  scanConsole  ]: Checking com 20UPGFC0009332
[13:58:37:731][  info  ][    SpecRx     ]: Active Rx channels: 0x40
[13:58:37:731][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[13:58:37:731][  info  ][    SpecRx     ]: Rx Status 0xf0
[13:58:37:731][  info  ][    SpecRx     ]: Number of lanes: 1
[13:58:37:731][  info  ][    SpecRx     ]: Channel 6 Lane 0 synchronized!
[13:58:37:741][  info  ][  scanConsole  ]: ... success!
[13:58:37:741][  info  ][  scanConsole  ]: Checking com 20UPGFC0009348
[13:58:37:741][  info  ][    SpecRx     ]: Active Rx channels: 0x80
[13:58:37:741][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[13:58:37:741][  info  ][    SpecRx     ]: Rx Status 0xf0
[13:58:37:741][  info  ][    SpecRx     ]: Number of lanes: 1
[13:58:37:741][  info  ][    SpecRx     ]: Channel 7 Lane 0 synchronized!
[13:58:37:752][  info  ][  scanConsole  ]: ... success!
[13:58:37:752][  info  ][  scanConsole  ]: Enabling Tx channels
[13:58:37:752][  info  ][  scanConsole  ]: Enabling Tx channel 1
[13:58:37:752][  info  ][  scanConsole  ]: Enabling Tx channel 1
[13:58:37:752][  info  ][  scanConsole  ]: Enabling Tx channel 1
[13:58:37:752][  info  ][  scanConsole  ]: Enabling Tx channel 1
[13:58:37:752][  info  ][  scanConsole  ]: Enabling Rx channels
[13:58:37:752][  info  ][  scanConsole  ]: Enabling Rx channel 4
[13:58:37:752][  info  ][  scanConsole  ]: Enabling Rx channel 5
[13:58:37:752][  info  ][  scanConsole  ]: Enabling Rx channel 6
[13:58:37:752][  info  ][  scanConsole  ]: Enabling Rx channel 7
[13:58:37:752][  info  ][  scanConsole  ]: ##############
[13:58:37:752][  info  ][  scanConsole  ]: # Setup Scan #
[13:58:37:752][  info  ][  scanConsole  ]: ##############
[13:58:37:753][  info  ][  scanConsole  ]: Found Scan config, constructing scan ...
[13:58:37:753][  info  ][  ScanFactory  ]: Loading Scan:
[13:58:37:753][  info  ][  ScanFactory  ]:   Name: AnalogScan
[13:58:37:753][  info  ][  ScanFactory  ]:   Number of Loops: 3
[13:58:37:753][  info  ][  ScanFactory  ]:   Loading Loop #0
[13:58:37:753][  info  ][  ScanFactory  ]:    Type: Rd53aReadRegLoop
[13:58:37:753][  info  ][  ScanFactory  ]:    Loading loop config ... 
[13:58:37:753][  info  ][  ScanFactory  ]: ~~~ {
[13:58:37:753][  info  ][  ScanFactory  ]: ~~~     "CurMux": [],
[13:58:37:753][  info  ][  ScanFactory  ]: ~~~     "EnblRingOsc": 0,
[13:58:37:753][  info  ][  ScanFactory  ]: ~~~     "Registers": [],
[13:58:37:753][  info  ][  ScanFactory  ]: ~~~     "RingOscDur": 9,
[13:58:37:753][  info  ][  ScanFactory  ]: ~~~     "RingOscRep": 10,
[13:58:37:753][  info  ][  ScanFactory  ]: ~~~     "VoltMux": [3, 5, 7, 15]
[13:58:37:753][  info  ][  ScanFactory  ]: ~~~ }
[13:58:37:753][  info  ][  ScanFactory  ]:   Loading Loop #1
[13:58:37:754][  info  ][  ScanFactory  ]:    Type: Rd53aTriggerLoop
[13:58:37:754][  info  ][  ScanFactory  ]:    Loading loop config ... 
[13:58:37:754][  info  ][  ScanFactory  ]: ~~~ {
[13:58:37:754][  info  ][  ScanFactory  ]: ~~~     "count": 1,
[13:58:37:754][  info  ][  ScanFactory  ]: ~~~     "delay": 48,
[13:58:37:754][  info  ][  ScanFactory  ]: ~~~     "extTrig": false,
[13:58:37:754][  info  ][  ScanFactory  ]: ~~~     "frequency": 30000.0,
[13:58:37:754][  info  ][  ScanFactory  ]: ~~~     "noInject": false,
[13:58:37:754][  info  ][  ScanFactory  ]: ~~~     "sendEcr": false,
[13:58:37:754][  info  ][  ScanFactory  ]: ~~~     "time": 0.0,
[13:58:37:754][  info  ][  ScanFactory  ]: ~~~     "trigMultiplier": 16
[13:58:37:754][  info  ][  ScanFactory  ]: ~~~ }
[13:58:37:754][  info  ][  ScanFactory  ]:   Loading Loop #2
[13:58:37:754][  info  ][  ScanFactory  ]:    Type: StdDataLoop
[13:58:37:754][warning ][  ScanFactory  ]: ~~~ Config empty.
[13:58:37:754][  info  ][ScanBuildHistogrammers]: Loading histogrammer ...
[13:58:37:754][  info  ][ScanBuildHistogrammers]: ... done!
[13:58:37:754][  info  ][ScanBuildAnalyses]: Loading analyses ...
[13:58:37:755][  info  ][  scanConsole  ]: Running pre scan!
[13:58:37:755][  info  ][  ScanFactory  ]: Entering pre scan phase ...
[13:58:37:755][  info  ][     Rd53a     ]: Write named register: GlobalPulseRt -> 16384
[13:58:37:755][  info  ][     Rd53a     ]: Write named register: InjEnDig -> 0
[13:58:37:755][  info  ][     Rd53a     ]: Write named register: InjVcalDiff -> 0
[13:58:37:755][  info  ][     Rd53a     ]: Write named register: InjVcalHigh -> 3500
[13:58:37:755][  info  ][     Rd53a     ]: Write named register: InjVcalMed -> 500
[13:58:37:755][  info  ][     Rd53a     ]: Write named register: LatencyConfig -> 48
[13:58:37:755][  info  ][  scanConsole  ]: Starting histogrammer and analysis threads:
[13:58:37:755][  info  ][  scanConsole  ]:  .. started threads of Fe 4
[13:58:37:755][  info  ][  scanConsole  ]:  .. started threads of Fe 5
[13:58:37:755][  info  ][  scanConsole  ]:  .. started threads of Fe 6
[13:58:37:755][  info  ][  scanConsole  ]:  .. started threads of Fe 7
[13:58:37:756][  info  ][Rd53aDataProcessor]:   -> Processor thread #0 started!
[13:58:37:756][  info  ][Rd53aDataProcessor]:   -> Processor thread #1 started!
[13:58:37:756][  info  ][Rd53aDataProcessor]:   -> Processor thread #2 started!
[13:58:37:756][  info  ][Rd53aDataProcessor]:   -> Processor thread #3 started!
[13:58:37:756][  info  ][  scanConsole  ]: ########
[13:58:37:756][  info  ][  scanConsole  ]: # Scan #
[13:58:37:756][  info  ][  scanConsole  ]: ########
[13:58:37:756][  info  ][  scanConsole  ]: Starting scan!
[13:58:37:756][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0009300 on Rx 4
[13:58:37:761][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0009300] MON MUX_V: 3 Bias 0 1241 -> 0.269735 V Bias 1 1647 -> 0.349833 V, Temperature 0.0800982 -> 2.20273 C
[13:58:37:765][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0009300] MON MUX_V: 5 Bias 0 1188 -> 0.259279 V Bias 1 1599 -> 0.340364 V, Temperature 0.0810846 -> 6.98041 C
[13:58:37:769][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0009300] MON MUX_V: 7 Bias 0 1176 -> 0.256911 V Bias 1 1594 -> 0.339377 V, Temperature 0.0824656 -> 0.550934 C
[13:58:37:773][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0009300] MON MUX_V: 15 Bias 0 1160 -> 0.253755 V Bias 1 1580 -> 0.336615 V, Temperature 0.0828602 -> -2.14163 C
[13:58:37:808][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0009316 on Rx 5
[13:58:37:812][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0009316] MON MUX_V: 3 Bias 0 1232 -> 0.266138 V Bias 1 1634 -> 0.346217 V, Temperature 0.0800784 -> 2.1275 C
[13:58:37:816][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0009316] MON MUX_V: 5 Bias 0 1202 -> 0.260162 V Bias 1 1604 -> 0.340241 V, Temperature 0.0800784 -> 1.00641 C
[13:58:37:821][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0009316] MON MUX_V: 7 Bias 0 1177 -> 0.255182 V Bias 1 1594 -> 0.338249 V, Temperature 0.0830664 -> 3.35562 C
[13:58:37:825][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0009316] MON MUX_V: 15 Bias 0 1169 -> 0.253589 V Bias 1 1580 -> 0.33546 V, Temperature 0.0818712 -> 3.3912 C
[13:58:37:860][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0009332 on Rx 6
[13:58:37:864][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0009332] MON MUX_V: 3 Bias 0 1213 -> 0.267718 V Bias 1 1615 -> 0.348198 V, Temperature 0.0804804 -> 0.0632629 C
[13:58:37:868][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0009332] MON MUX_V: 5 Bias 0 1159 -> 0.256907 V Bias 1 1570 -> 0.339189 V, Temperature 0.0822822 -> 1.83954 C
[13:58:37:872][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0009332] MON MUX_V: 7 Bias 0 1162 -> 0.257507 V Bias 1 1574 -> 0.33999 V, Temperature 0.0824824 -> 2.50876 C
[13:58:37:877][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0009332] MON MUX_V: 15 Bias 0 1138 -> 0.252703 V Bias 1 1558 -> 0.336787 V, Temperature 0.084084 -> 1.63998 C
[13:58:37:912][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0009348 on Rx 7
[13:58:37:916][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0009348] MON MUX_V: 3 Bias 0 1209 -> 0.262575 V Bias 1 1609 -> 0.342815 V, Temperature 0.08024 -> 2.34296 C
[13:58:37:920][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0009348] MON MUX_V: 5 Bias 0 1165 -> 0.253749 V Bias 1 1564 -> 0.333788 V, Temperature 0.0800394 -> 3.99768 C
[13:58:37:924][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0009348] MON MUX_V: 7 Bias 0 1138 -> 0.248332 V Bias 1 1550 -> 0.33098 V, Temperature 0.0826472 -> 4.70334 C
[13:58:37:929][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0009348] MON MUX_V: 15 Bias 0 1145 -> 0.249737 V Bias 1 1559 -> 0.332785 V, Temperature 0.0830484 -> 5.18008 C
[13:58:37:965][  info  ][  scanConsole  ]: Scan done!
[13:58:37:965][  info  ][  scanConsole  ]: Waiting for processors to finish ...
[13:58:37:966][  info  ][  scanConsole  ]: Processor done, waiting for histogrammer ...
[13:58:38:366][  info  ][HistogramAlgorithm]: Histogrammer done!
[13:58:38:366][  info  ][HistogramAlgorithm]: Histogrammer done!
[13:58:38:366][  info  ][HistogramAlgorithm]: Histogrammer done!
[13:58:38:366][  info  ][HistogramAlgorithm]: Histogrammer done!
[13:58:38:366][  info  ][  scanConsole  ]: Processor done, waiting for analysis ...
[13:58:38:766][  info  ][AnalysisAlgorithm]: Analysis done!
[13:58:38:766][  info  ][AnalysisAlgorithm]: Analysis done!
[13:58:38:766][  info  ][AnalysisAlgorithm]: Analysis done!
[13:58:38:766][  info  ][AnalysisAlgorithm]: Analysis done!
[13:58:38:767][  info  ][  scanConsole  ]: All done!
[13:58:38:767][  info  ][  scanConsole  ]: ##########
[13:58:38:767][  info  ][  scanConsole  ]: # Timing #
[13:58:38:767][  info  ][  scanConsole  ]: ##########
[13:58:38:767][  info  ][  scanConsole  ]: -> Configuration: 153 ms
[13:58:38:767][  info  ][  scanConsole  ]: -> Scan:          208 ms
[13:58:38:767][  info  ][  scanConsole  ]: -> Processing:    0 ms
[13:58:38:767][  info  ][  scanConsole  ]: -> Analysis:      800 ms
[13:58:38:767][  info  ][  scanConsole  ]: ###########
[13:58:38:767][  info  ][  scanConsole  ]: # Cleanup #
[13:58:38:767][  info  ][  scanConsole  ]: ###########
[13:58:38:776][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0009300 to configs/KEKQ14/20UPGFC0009300.json
[13:58:38:949][  info  ][  scanConsole  ]: -> Storing output results of FE 4
[13:58:38:949][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0009300, this usually means that the chip did not send any data at all.
[13:58:38:951][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0009316 to configs/KEKQ14/20UPGFC0009316.json
[13:58:39:112][  info  ][  scanConsole  ]: -> Storing output results of FE 5
[13:58:39:112][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0009316, this usually means that the chip did not send any data at all.
[13:58:39:114][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0009332 to configs/KEKQ14/20UPGFC0009332.json
[13:58:39:276][  info  ][  scanConsole  ]: -> Storing output results of FE 6
[13:58:39:276][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0009332, this usually means that the chip did not send any data at all.
[13:58:39:277][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0009348 to configs/KEKQ14/20UPGFC0009348.json
[13:58:39:440][  info  ][  scanConsole  ]: -> Storing output results of FE 7
[13:58:39:440][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0009348, this usually means that the chip did not send any data at all.
[13:58:39:441][  info  ][  scanConsole  ]: Finishing run: 1754
20UPGFC0009300.json.after
20UPGFC0009300.json.before
20UPGFC0009316.json.after
20UPGFC0009316.json.before
20UPGFC0009332.json.after
20UPGFC0009332.json.before
20UPGFC0009348.json.after
20UPGFC0009348.json.before
reg_readtemp.json
scanLog.json

