[13:59:16:226][  info  ][               ]: #####################################
[13:59:16:227][  info  ][               ]: # Welcome to the YARR Scan Console! #
[13:59:16:227][  info  ][               ]: #####################################
[13:59:16:227][  info  ][               ]: -> Parsing command line parameters ...
[13:59:16:227][  info  ][               ]: Configuring logger ...
[13:59:16:230][  info  ][  scanConsole  ]: Scan Type/Config configs/scans/rd53a/reg_readtemp.json
[13:59:16:230][  info  ][  scanConsole  ]: Connectivity:
[13:59:16:230][  info  ][  scanConsole  ]:     configs/KEKQ14/connectivity.json
[13:59:16:230][  info  ][  scanConsole  ]: Target ToT: -1
[13:59:16:230][  info  ][  scanConsole  ]: Target Charge: -1
[13:59:16:230][  info  ][  scanConsole  ]: Output Plots: true
[13:59:16:230][  info  ][  scanConsole  ]: Output Directory: ./data/001755_reg_readtemp/
[13:59:16:238][  info  ][  scanConsole  ]: Timestamp: 2022-08-24_13:59:16
[13:59:16:238][  info  ][  scanConsole  ]: Run Number: 1755
[13:59:16:238][  info  ][  scanConsole  ]: #################
[13:59:16:238][  info  ][  scanConsole  ]: # Init Hardware #
[13:59:16:238][  info  ][  scanConsole  ]: #################
[13:59:16:238][  info  ][  scanConsole  ]: -> Opening controller config: configs/controller/specCfg-rd53a-16x1.json
[13:59:16:238][  info  ][  ScanHelper   ]: Loading controller ...
[13:59:16:238][  info  ][  ScanHelper   ]: Found controller of type: spec
[13:59:16:238][  info  ][  ScanHelper   ]: ... loading controler config:
[13:59:16:238][  info  ][  ScanHelper   ]: ~~~ {
[13:59:16:238][  info  ][  ScanHelper   ]: ~~~     "cmdPeriod": 6.250000073038109e-9,
[13:59:16:238][  info  ][  ScanHelper   ]: ~~~     "idle": {
[13:59:16:238][  info  ][  ScanHelper   ]: ~~~         "word": 1768515945
[13:59:16:238][  info  ][  ScanHelper   ]: ~~~     },
[13:59:16:238][  info  ][  ScanHelper   ]: ~~~     "pulse": {
[13:59:16:238][  info  ][  ScanHelper   ]: ~~~         "interval": 500,
[13:59:16:238][  info  ][  ScanHelper   ]: ~~~         "word": 1549575846
[13:59:16:238][  info  ][  ScanHelper   ]: ~~~     },
[13:59:16:238][  info  ][  ScanHelper   ]: ~~~     "rxActiveLanes": 1,
[13:59:16:238][  info  ][  ScanHelper   ]: ~~~     "rxPolarity": 0,
[13:59:16:238][  info  ][  ScanHelper   ]: ~~~     "specNum": 0,
[13:59:16:238][  info  ][  ScanHelper   ]: ~~~     "spiConfig": 541200,
[13:59:16:238][  info  ][  ScanHelper   ]: ~~~     "sync": {
[13:59:16:238][  info  ][  ScanHelper   ]: ~~~         "interval": 32,
[13:59:16:238][  info  ][  ScanHelper   ]: ~~~         "word": 2172551550
[13:59:16:238][  info  ][  ScanHelper   ]: ~~~     },
[13:59:16:238][  info  ][  ScanHelper   ]: ~~~     "txPolarity": 0
[13:59:16:238][  info  ][  ScanHelper   ]: ~~~ }
[13:59:16:238][  info  ][    SpecCom    ]: Opening SPEC with id #0
[13:59:16:238][  info  ][    SpecCom    ]: Mapping BARs ...
[13:59:16:238][  info  ][    SpecCom    ]: ... Mapped BAR0 at 0x7f829f065000 with size 1048576
[13:59:16:238][warning ][    SpecCom    ]: ... BAR4 not mapped (Mmap failed)
[13:59:16:238][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[13:59:16:238][  info  ][    SpecCom    ]: Firmware Version: 0x4d9ff6d
[13:59:16:238][  info  ][    SpecCom    ]: Firmware Identifier: 0x4030232
[13:59:16:238][  info  ][    SpecCom    ]: FPGA card: PLDA XpressK7 325
[13:59:16:238][  info  ][    SpecCom    ]: FE Chip Type: RD53A/B
[13:59:16:238][  info  ][    SpecCom    ]: FMC Card Type: Ohio Card (Display Port)
[13:59:16:238][  info  ][    SpecCom    ]: RX Speed: 640Mbps
[13:59:16:239][  info  ][    SpecCom    ]: Channel Configuration: 16x1
[13:59:16:239][  info  ][    SpecCom    ]: ~~~~~~~~~~~~~~~~~~~~~~~~~~~
[13:59:16:239][  info  ][    SpecCom    ]: Flushing buffers ...
[13:59:16:239][  info  ][    SpecCom    ]: Init success!
[13:59:16:239][  info  ][  scanConsole  ]: #######################
[13:59:16:239][  info  ][  scanConsole  ]: ##  Loading Configs  ##
[13:59:16:239][  info  ][  scanConsole  ]: #######################
[13:59:16:239][  info  ][  scanConsole  ]: Opening global config: configs/KEKQ14/connectivity.json
[13:59:16:239][  info  ][  ScanHelper   ]: Chip type: RD53A
[13:59:16:239][  info  ][  ScanHelper   ]: Chip count 4
[13:59:16:239][  info  ][  ScanHelper   ]: Loading chip #0
[13:59:16:240][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(4)
[13:59:16:240][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ14/20UPGFC0009300.json
[13:59:16:440][  info  ][  ScanHelper   ]: Loading chip #1
[13:59:16:440][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(5)
[13:59:16:440][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ14/20UPGFC0009316.json
[13:59:16:614][  info  ][  ScanHelper   ]: Loading chip #2
[13:59:16:615][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(6)
[13:59:16:615][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ14/20UPGFC0009332.json
[13:59:16:789][  info  ][  ScanHelper   ]: Loading chip #3
[13:59:16:789][  info  ][  Bookkeeper   ]: Added FE: Tx(1), Rx(7)
[13:59:16:789][  info  ][  ScanHelper   ]: Loading config file: configs/KEKQ14/20UPGFC0009348.json
[13:59:16:968][  info  ][  scanConsole  ]: #################
[13:59:16:968][  info  ][  scanConsole  ]: # Configure FEs #
[13:59:16:968][  info  ][  scanConsole  ]: #################
[13:59:16:968][  info  ][  scanConsole  ]: Configuring 20UPGFC0009300
[13:59:16:998][  info  ][  scanConsole  ]: Configuring 20UPGFC0009316
[13:59:17:028][  info  ][  scanConsole  ]: Configuring 20UPGFC0009332
[13:59:17:059][  info  ][  scanConsole  ]: Configuring 20UPGFC0009348
[13:59:17:089][  info  ][  scanConsole  ]: Sent configuration to all FEs in 121 ms!
[13:59:17:090][  info  ][  scanConsole  ]: Checking com 20UPGFC0009300
[13:59:17:090][  info  ][    SpecRx     ]: Active Rx channels: 0x10
[13:59:17:090][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[13:59:17:090][  info  ][    SpecRx     ]: Rx Status 0xf0
[13:59:17:090][  info  ][    SpecRx     ]: Number of lanes: 1
[13:59:17:090][  info  ][    SpecRx     ]: Channel 4 Lane 0 synchronized!
[13:59:17:100][  info  ][  scanConsole  ]: ... success!
[13:59:17:100][  info  ][  scanConsole  ]: Checking com 20UPGFC0009316
[13:59:17:100][  info  ][    SpecRx     ]: Active Rx channels: 0x20
[13:59:17:100][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[13:59:17:100][  info  ][    SpecRx     ]: Rx Status 0xf0
[13:59:17:100][  info  ][    SpecRx     ]: Number of lanes: 1
[13:59:17:100][  info  ][    SpecRx     ]: Channel 5 Lane 0 synchronized!
[13:59:17:111][  info  ][  scanConsole  ]: ... success!
[13:59:17:111][  info  ][  scanConsole  ]: Checking com 20UPGFC0009332
[13:59:17:111][  info  ][    SpecRx     ]: Active Rx channels: 0x40
[13:59:17:111][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[13:59:17:111][  info  ][    SpecRx     ]: Rx Status 0xf0
[13:59:17:111][  info  ][    SpecRx     ]: Number of lanes: 1
[13:59:17:111][  info  ][    SpecRx     ]: Channel 6 Lane 0 synchronized!
[13:59:17:121][  info  ][  scanConsole  ]: ... success!
[13:59:17:121][  info  ][  scanConsole  ]: Checking com 20UPGFC0009348
[13:59:17:121][  info  ][    SpecRx     ]: Active Rx channels: 0x80
[13:59:17:121][  info  ][    SpecRx     ]: Active Rx lanes: 0x1
[13:59:17:121][  info  ][    SpecRx     ]: Rx Status 0xf0
[13:59:17:121][  info  ][    SpecRx     ]: Number of lanes: 1
[13:59:17:121][  info  ][    SpecRx     ]: Channel 7 Lane 0 synchronized!
[13:59:17:132][  info  ][  scanConsole  ]: ... success!
[13:59:17:132][  info  ][  scanConsole  ]: Enabling Tx channels
[13:59:17:132][  info  ][  scanConsole  ]: Enabling Tx channel 1
[13:59:17:132][  info  ][  scanConsole  ]: Enabling Tx channel 1
[13:59:17:132][  info  ][  scanConsole  ]: Enabling Tx channel 1
[13:59:17:132][  info  ][  scanConsole  ]: Enabling Tx channel 1
[13:59:17:132][  info  ][  scanConsole  ]: Enabling Rx channels
[13:59:17:132][  info  ][  scanConsole  ]: Enabling Rx channel 4
[13:59:17:132][  info  ][  scanConsole  ]: Enabling Rx channel 5
[13:59:17:132][  info  ][  scanConsole  ]: Enabling Rx channel 6
[13:59:17:132][  info  ][  scanConsole  ]: Enabling Rx channel 7
[13:59:17:132][  info  ][  scanConsole  ]: ##############
[13:59:17:132][  info  ][  scanConsole  ]: # Setup Scan #
[13:59:17:132][  info  ][  scanConsole  ]: ##############
[13:59:17:133][  info  ][  scanConsole  ]: Found Scan config, constructing scan ...
[13:59:17:133][  info  ][  ScanFactory  ]: Loading Scan:
[13:59:17:133][  info  ][  ScanFactory  ]:   Name: AnalogScan
[13:59:17:133][  info  ][  ScanFactory  ]:   Number of Loops: 3
[13:59:17:133][  info  ][  ScanFactory  ]:   Loading Loop #0
[13:59:17:133][  info  ][  ScanFactory  ]:    Type: Rd53aReadRegLoop
[13:59:17:133][  info  ][  ScanFactory  ]:    Loading loop config ... 
[13:59:17:133][  info  ][  ScanFactory  ]: ~~~ {
[13:59:17:133][  info  ][  ScanFactory  ]: ~~~     "CurMux": [],
[13:59:17:133][  info  ][  ScanFactory  ]: ~~~     "EnblRingOsc": 0,
[13:59:17:133][  info  ][  ScanFactory  ]: ~~~     "Registers": [],
[13:59:17:133][  info  ][  ScanFactory  ]: ~~~     "RingOscDur": 9,
[13:59:17:133][  info  ][  ScanFactory  ]: ~~~     "RingOscRep": 10,
[13:59:17:133][  info  ][  ScanFactory  ]: ~~~     "VoltMux": [3, 5, 7, 15]
[13:59:17:134][  info  ][  ScanFactory  ]: ~~~ }
[13:59:17:134][  info  ][  ScanFactory  ]:   Loading Loop #1
[13:59:17:134][  info  ][  ScanFactory  ]:    Type: Rd53aTriggerLoop
[13:59:17:134][  info  ][  ScanFactory  ]:    Loading loop config ... 
[13:59:17:134][  info  ][  ScanFactory  ]: ~~~ {
[13:59:17:134][  info  ][  ScanFactory  ]: ~~~     "count": 1,
[13:59:17:134][  info  ][  ScanFactory  ]: ~~~     "delay": 48,
[13:59:17:134][  info  ][  ScanFactory  ]: ~~~     "extTrig": false,
[13:59:17:134][  info  ][  ScanFactory  ]: ~~~     "frequency": 30000.0,
[13:59:17:134][  info  ][  ScanFactory  ]: ~~~     "noInject": false,
[13:59:17:134][  info  ][  ScanFactory  ]: ~~~     "sendEcr": false,
[13:59:17:134][  info  ][  ScanFactory  ]: ~~~     "time": 0.0,
[13:59:17:134][  info  ][  ScanFactory  ]: ~~~     "trigMultiplier": 16
[13:59:17:134][  info  ][  ScanFactory  ]: ~~~ }
[13:59:17:134][  info  ][  ScanFactory  ]:   Loading Loop #2
[13:59:17:134][  info  ][  ScanFactory  ]:    Type: StdDataLoop
[13:59:17:134][warning ][  ScanFactory  ]: ~~~ Config empty.
[13:59:17:134][  info  ][ScanBuildHistogrammers]: Loading histogrammer ...
[13:59:17:134][  info  ][ScanBuildHistogrammers]: ... done!
[13:59:17:134][  info  ][ScanBuildAnalyses]: Loading analyses ...
[13:59:17:135][  info  ][  scanConsole  ]: Running pre scan!
[13:59:17:135][  info  ][  ScanFactory  ]: Entering pre scan phase ...
[13:59:17:135][  info  ][     Rd53a     ]: Write named register: GlobalPulseRt -> 16384
[13:59:17:135][  info  ][     Rd53a     ]: Write named register: InjEnDig -> 0
[13:59:17:135][  info  ][     Rd53a     ]: Write named register: InjVcalDiff -> 0
[13:59:17:135][  info  ][     Rd53a     ]: Write named register: InjVcalHigh -> 3500
[13:59:17:135][  info  ][     Rd53a     ]: Write named register: InjVcalMed -> 500
[13:59:17:135][  info  ][     Rd53a     ]: Write named register: LatencyConfig -> 48
[13:59:17:135][  info  ][  scanConsole  ]: Starting histogrammer and analysis threads:
[13:59:17:135][  info  ][  scanConsole  ]:  .. started threads of Fe 4
[13:59:17:135][  info  ][  scanConsole  ]:  .. started threads of Fe 5
[13:59:17:136][  info  ][  scanConsole  ]:  .. started threads of Fe 6
[13:59:17:136][  info  ][  scanConsole  ]:  .. started threads of Fe 7
[13:59:17:136][  info  ][Rd53aDataProcessor]:   -> Processor thread #0 started!
[13:59:17:136][  info  ][Rd53aDataProcessor]:   -> Processor thread #1 started!
[13:59:17:136][  info  ][Rd53aDataProcessor]:   -> Processor thread #2 started!
[13:59:17:136][  info  ][Rd53aDataProcessor]:   -> Processor thread #3 started!
[13:59:17:136][  info  ][  scanConsole  ]: ########
[13:59:17:136][  info  ][  scanConsole  ]: # Scan #
[13:59:17:136][  info  ][  scanConsole  ]: ########
[13:59:17:136][  info  ][  scanConsole  ]: Starting scan!
[13:59:17:136][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0009300 on Rx 4
[13:59:17:140][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0009300] MON MUX_V: 3 Bias 0 1240 -> 0.269538 V Bias 1 1647 -> 0.349833 V, Temperature 0.0802955 -> 2.88095 C
[13:59:17:145][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0009300] MON MUX_V: 5 Bias 0 1186 -> 0.258884 V Bias 1 1598 -> 0.340166 V, Temperature 0.0812819 -> 7.6619 C
[13:59:17:149][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0009300] MON MUX_V: 7 Bias 0 1176 -> 0.256911 V Bias 1 1593 -> 0.33918 V, Temperature 0.0822684 -> -0.10379 C
[13:59:17:153][  info  ][Rd53aReadRegLoop]: [4][20UPGFC0009300] MON MUX_V: 15 Bias 0 1161 -> 0.253952 V Bias 1 1580 -> 0.336615 V, Temperature 0.0826629 -> -2.7869 C
[13:59:17:188][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0009316 on Rx 5
[13:59:17:192][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0009316] MON MUX_V: 3 Bias 0 1233 -> 0.266338 V Bias 1 1635 -> 0.346416 V, Temperature 0.0800784 -> 2.12759 C
[13:59:17:196][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0009316] MON MUX_V: 5 Bias 0 1203 -> 0.260362 V Bias 1 1605 -> 0.34044 V, Temperature 0.0800784 -> 1.00641 C
[13:59:17:200][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0009316] MON MUX_V: 7 Bias 0 1177 -> 0.255182 V Bias 1 1595 -> 0.338448 V, Temperature 0.0832656 -> 4.01862 C
[13:59:17:204][  info  ][Rd53aReadRegLoop]: [5][20UPGFC0009316] MON MUX_V: 15 Bias 0 1168 -> 0.25339 V Bias 1 1580 -> 0.33546 V, Temperature 0.0820704 -> 4.06406 C
[13:59:17:239][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0009332 on Rx 6
[13:59:17:243][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0009332] MON MUX_V: 3 Bias 0 1212 -> 0.267517 V Bias 1 1616 -> 0.348398 V, Temperature 0.0808808 -> 1.42261 C
[13:59:17:248][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0009332] MON MUX_V: 5 Bias 0 1159 -> 0.256907 V Bias 1 1570 -> 0.339189 V, Temperature 0.0822822 -> 1.83954 C
[13:59:17:252][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0009332] MON MUX_V: 7 Bias 0 1161 -> 0.257307 V Bias 1 1574 -> 0.33999 V, Temperature 0.0826826 -> 3.17776 C
[13:59:17:256][  info  ][Rd53aReadRegLoop]: [6][20UPGFC0009332] MON MUX_V: 15 Bias 0 1137 -> 0.252502 V Bias 1 1558 -> 0.336787 V, Temperature 0.0842842 -> 2.29419 C
[13:59:17:291][  info  ][Rd53aReadRegLoop]: Measuring for FE 20UPGFC0009348 on Rx 7
[13:59:17:295][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0009348] MON MUX_V: 3 Bias 0 1210 -> 0.262776 V Bias 1 1610 -> 0.343016 V, Temperature 0.08024 -> 2.34296 C
[13:59:17:300][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0009348] MON MUX_V: 5 Bias 0 1165 -> 0.253749 V Bias 1 1564 -> 0.333788 V, Temperature 0.0800394 -> 3.99768 C
[13:59:17:304][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0009348] MON MUX_V: 7 Bias 0 1138 -> 0.248332 V Bias 1 1552 -> 0.331381 V, Temperature 0.0830484 -> 6.05225 C
[13:59:17:308][  info  ][Rd53aReadRegLoop]: [7][20UPGFC0009348] MON MUX_V: 15 Bias 0 1145 -> 0.249737 V Bias 1 1561 -> 0.333186 V, Temperature 0.0834496 -> 6.52466 C
[13:59:17:345][  info  ][  scanConsole  ]: Scan done!
[13:59:17:345][  info  ][  scanConsole  ]: Waiting for processors to finish ...
[13:59:17:345][  info  ][  scanConsole  ]: Processor done, waiting for histogrammer ...
[13:59:17:745][  info  ][HistogramAlgorithm]: Histogrammer done!
[13:59:17:745][  info  ][HistogramAlgorithm]: Histogrammer done!
[13:59:17:745][  info  ][HistogramAlgorithm]: Histogrammer done!
[13:59:17:745][  info  ][HistogramAlgorithm]: Histogrammer done!
[13:59:17:745][  info  ][  scanConsole  ]: Processor done, waiting for analysis ...
[13:59:18:146][  info  ][AnalysisAlgorithm]: Analysis done!
[13:59:18:146][  info  ][AnalysisAlgorithm]: Analysis done!
[13:59:18:146][  info  ][AnalysisAlgorithm]: Analysis done!
[13:59:18:146][  info  ][AnalysisAlgorithm]: Analysis done!
[13:59:18:146][  info  ][  scanConsole  ]: All done!
[13:59:18:146][  info  ][  scanConsole  ]: ##########
[13:59:18:146][  info  ][  scanConsole  ]: # Timing #
[13:59:18:146][  info  ][  scanConsole  ]: ##########
[13:59:18:146][  info  ][  scanConsole  ]: -> Configuration: 121 ms
[13:59:18:146][  info  ][  scanConsole  ]: -> Scan:          208 ms
[13:59:18:146][  info  ][  scanConsole  ]: -> Processing:    0 ms
[13:59:18:146][  info  ][  scanConsole  ]: -> Analysis:      800 ms
[13:59:18:146][  info  ][  scanConsole  ]: ###########
[13:59:18:146][  info  ][  scanConsole  ]: # Cleanup #
[13:59:18:146][  info  ][  scanConsole  ]: ###########
[13:59:18:154][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0009300 to configs/KEKQ14/20UPGFC0009300.json
[13:59:18:353][  info  ][  scanConsole  ]: -> Storing output results of FE 4
[13:59:18:353][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0009300, this usually means that the chip did not send any data at all.
[13:59:18:354][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0009316 to configs/KEKQ14/20UPGFC0009316.json
[13:59:18:516][  info  ][  scanConsole  ]: -> Storing output results of FE 5
[13:59:18:516][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0009316, this usually means that the chip did not send any data at all.
[13:59:18:517][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0009332 to configs/KEKQ14/20UPGFC0009332.json
[13:59:18:681][  info  ][  scanConsole  ]: -> Storing output results of FE 6
[13:59:18:681][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0009332, this usually means that the chip did not send any data at all.
[13:59:18:682][  info  ][  scanConsole  ]: Saving config of FE 20UPGFC0009348 to configs/KEKQ14/20UPGFC0009348.json
[13:59:18:848][  info  ][  scanConsole  ]: -> Storing output results of FE 7
[13:59:18:848][warning ][  scanConsole  ]: There were no results for chip 20UPGFC0009348, this usually means that the chip did not send any data at all.
[13:59:18:849][  info  ][  scanConsole  ]: Finishing run: 1755
20UPGFC0009300.json.after
20UPGFC0009300.json.before
20UPGFC0009316.json.after
20UPGFC0009316.json.before
20UPGFC0009332.json.after
20UPGFC0009332.json.before
20UPGFC0009348.json.after
20UPGFC0009348.json.before
reg_readtemp.json
scanLog.json

