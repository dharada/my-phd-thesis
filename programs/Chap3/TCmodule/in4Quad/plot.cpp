#include <iostream>
#include <fstream>
#include <nlohmann/json.hpp>
#include <tuple>
#include <math.h>

#include <TROOT.h>
#include <TGraph.h>
#include <TH1F.h>
#include <THStack.h>
#include <TStyle.h>
#include <TLegend.h>
#include <TCanvas.h>
#include <TGaxis.h>
#include <TAxis.h>

using namespace std;
using json = nlohmann::json;

class moduleInfo{
public:
  string moduleName;
  double Jig_sep[6][2];
  double moduleNTC[6];
  double Jig[6];
  double power[6];
  double NMOS[6][4][4];
  double deltaT_ntc[6];
  double deltaT_nmos[6][4][4];
  TGraph *grNTC;
  TGraph *grNMOS[4][4];
  void calDelta(){
    for(int ii=0;ii<6;ii++){
      deltaT_ntc[ii]=(moduleNTC[ii]-Jig[ii])/(4.6*power[ii]);
      for(int jj=0;jj<4;jj++){
	for(int kk=0;kk<4;kk++){
	  deltaT_nmos[ii][jj][kk]=(NMOS[ii][jj][kk]-Jig[ii])/(4.6*power[ii]);
	}
      }
    }
  }
  void createGr(int color, int style){
    grNTC=new TGraph(6, Jig, deltaT_ntc);
    grNTC->SetTitle("#DeltaT between jig and module NTC, after 100 thermal cycle");
    grNTC->GetXaxis()->SetTitle("Jig temperature [#circC]");
    grNTC->GetYaxis()->SetTitle("#DeltaT/Power [#circC/W]");
    //    grNTC->GetYaxis()->SetTitle("#DeltaT [#circC]");
    grNTC->GetXaxis()->SetTitleSize(0.05);
    grNTC->GetXaxis()->SetLabelSize(0.05);
    grNTC->GetYaxis()->SetTitleSize(0.05);
    grNTC->GetYaxis()->SetLabelSize(0.05);
    grNTC->SetLineColor(color);
    grNTC->SetMarkerColor(color);
    grNTC->SetMarkerStyle(style);

    for(int ii=0;ii<4;ii++){
      for(int jj=0;jj<4;jj++){
	grNMOS[ii][jj]=new TGraph(6, Jig, deltaT_nmos[ii][jj]);
	grNMOS[ii][jj]->SetTitle("");
	grNMOS[ii][jj]->GetXaxis()->SetTitle("Jig temperature [#circC]");
	grNMOS[ii][jj]->GetYaxis()->SetTitle("#DeltaT/Power [#circC/W]");
	grNMOS[ii][jj]->GetXaxis()->SetTitleSize(0.05);
	grNMOS[ii][jj]->GetXaxis()->SetLabelSize(0.05);
	grNMOS[ii][jj]->GetYaxis()->SetTitleSize(0.05);
	grNMOS[ii][jj]->GetYaxis()->SetLabelSize(0.05);
	grNMOS[ii][jj]->SetLineColor(color);
	grNMOS[ii][jj]->SetMarkerColor(color);
	grNMOS[ii][jj]->SetMarkerStyle(style);
      }
    }
  }
      
};


int main(){
  int selectVoltage=1;
  TCanvas *c1 = new TCanvas("c1","",600,400);
  c1->SetLeftMargin(0.12);
  ifstream i("RawData/data.json");
  json j;
  i >> j;
  moduleInfo module[j["module"].size()];

  for(int ii=0;ii<j["module"].size();ii++){
    cout<<j["module"][ii]["ModuleName"]<<endl;
    module[ii].moduleName = j["module"][ii]["ModuleName"];
    
    for(int jj=0;jj<6;jj++){
      
      module[ii].Jig_sep[jj][0]=j["module"][ii]["JigTemperatures"][jj]["Jig"][selectVoltage][0];
      module[ii].Jig_sep[jj][1]=j["module"][ii]["JigTemperatures"][jj]["Jig"][selectVoltage][1];
      module[ii].Jig[jj]=(module[ii].Jig_sep[jj][0]+module[ii].Jig_sep[jj][1])/2;

      module[ii].power[jj]=j["module"][ii]["JigTemperatures"][jj]["LV"][selectVoltage];
	
      module[ii].moduleNTC[jj]=j["module"][ii]["JigTemperatures"][jj]["ModuleNTC"][selectVoltage];
      
      //      cout<<"hoge"<<endl;
      for(int kk=0;kk<4;kk++){
	for(int ll=0;ll<4;ll++){
	  if(j["module"][ii]["NMOSreverse"][jj]==1){
	    module[ii].NMOS[jj][kk][ll]=j["module"][ii]["JigTemperatures"][jj]["NMOS"][kk][selectVoltage][ll];
	  }else{	
	    module[ii].NMOS[jj][kk][ll]=j["module"][ii]["JigTemperatures"][jj]["NMOS"][ll][selectVoltage][kk];
	  }
	}
      }
      
      cout<<j["module"][ii]["ModuleName"]<<" "<<module[ii].Jig[jj]<<" "<<module[ii].Jig_sep[jj][0]<<" "<<module[ii].Jig_sep[jj][1]<<endl;
    }
    cout<<"start analysis"<<endl;
    module[ii].calDelta();
    module[ii].createGr(ii+1, 20+ii);
  }
  cout<<"plotting"<<endl;
  module[0].grNTC->GetYaxis()->SetRangeUser(0.5,2.8);
  //  module[0].grNTC->GetYaxis()->SetRangeUser(5,20);
  module[0].grNTC->Draw("apl");
  for(int ii=0;ii<j["module"].size();ii++){
    module[ii].grNTC->Draw("plsame");
  }
  TLegend* leg = new TLegend(0.65,0.6,0.85,0.85);
  for(int ii=0;ii<j["module"].size();ii++){
    leg->AddEntry(module[ii].grNTC,module[ii].moduleName.c_str(),"pl");
  }
  leg->Draw();

  
  c1->SaveAs("deltaT_ntc.pdf");
  
}
