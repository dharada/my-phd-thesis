#include <fstream>
#include <nlohmann/json.hpp>
#include <iostream>
#include <TCanvas.h>
#include <TGraph.h>
#include <TGraphErrors.h>
#include <TF1.h>
#include <TLegend.h>
#include <TAxis.h>
#include <TLine.h>
using namespace std;

#include "SaveTGraph.cpp"
#include "GraphByModule.cpp"

// for convenience                                                                                                              
using json = nlohmann::json;

int main(){
  cout<<"0: normalised NTC, 1: just NTC, 2: normalised current, 3: just current, 4:TempSens(all), 5:TempSensDiff(20C), 6:TempSensDiff(10C), 7:TempSensDiff(-10C), 8:ThermalCamera(all), 9:ThermalCamera(Diff), 10:By module, by temp"<<endl;
  int id; //0: normalised NTC, 1: just NTC, 2: normalised current, 3: just current, 4:TempSens(all), 5:TempSensDiff(20C), 6:TempSensDiff(10C), 7:TempSensDiff(-10C), 8:ThermalCamera(all)
  cin>>id;
  double highest=0;
  double lowest=100;
  TCanvas *c1=new TCanvas("","",600,450);
  ifstream i("module.json");

  const int numOfModule=9+7+12+4+5;
  //number of module, CERN:9, KEK:7, Paris:12, Siegen:4, Goe:5
  
  double data20[numOfModule], data10[numOfModule], dataMin[numOfModule];
  double dataSens20[16][numOfModule], dataSens10[16][numOfModule], dataSensMin[16][numOfModule]; 
  json j;
  i >> j;
  bool isFirstEvent=true;

  double xAxis[numOfModule];
  double yAxis[7];

  double plotX[6][7];
  double plotTempSens[3][16];
  double plotCamera[3][16];
  //set the data
  vector <string> vec_ModuleName;
  vector <double> vec_JigTemp[7];
  vector <double> vec_ModuleNTC[7];
  vector <double> vec_Current[7];
  vector <double> vec_eCurrent[7];
  vector <double> vec_Camera[7];
  vector <double> vec_Sensor[7];
  vector <double> vec_Power[7];
  vector <double> vec_TempSens[7][16];
  vector <double> vec_CameraDist[7][16];

  string sendId[16]={"Sens1.1", "Sens1.2", "Sens1.3", "Sens1.4", "Sens2.1", "Sens2.2", "Sens2.3", "Sens2.4", "Sens3.1", "Sens3.2", "Sens3.3", "Sens3.4", "Sens4.1", "Sens4.2", "Sens4.3", "Sens4.4"};
  
  for(int ii=0;ii<numOfModule;ii++){
    cout<<j["Module"][ii]["ModuleName"]<<endl;
    vec_ModuleName.push_back(j["Module"][ii]["ModuleName"]);

    for(int jj=0;jj<3;jj++){
      for(int kk=0;kk<16;kk++){
	plotTempSens[jj][kk]=j["Module"][ii]["TempSens"][jj][kk];
	plotCamera[jj][kk]=j["Module"][ii]["CameraDist"][jj][kk];
      }
    }
    for(int jj=0;jj<7;jj++){

      yAxis[jj]=j["Module"][ii]["ModuleNTC"][jj];
      plotX[0][jj]=j["Module"][ii]["ModuleNTC"][jj];
      plotX[1][jj]=j["Module"][ii]["Current"][jj];
      plotX[2][jj]=j["Module"][ii]["eCurrent"][jj];
      plotX[3][jj]=j["Module"][ii]["Camera"][jj];
      plotX[4][jj]=j["Module"][ii]["Sensor"][jj];
      plotX[5][jj]=j["Module"][ii]["JigTemperature"][jj];
      
      vec_JigTemp[jj].push_back(j["Module"][ii]["JigTemperature"][jj]);
      vec_ModuleNTC[jj].push_back(j["Module"][ii]["ModuleNTC"][jj]);
      vec_Current[jj].push_back(j["Module"][ii]["Current"][jj]);
      vec_eCurrent[jj].push_back(j["Module"][ii]["eCurrent"][jj]);
      vec_Camera[jj].push_back(j["Module"][ii]["Camera"][jj]);
      vec_Sensor[jj].push_back(j["Module"][ii]["Sensor"][jj]);
      vec_Power[jj].push_back(j["Module"][ii]["LV"][jj]);

      for(int kk=0;kk<16;kk++){
	if(jj==0){
	  vec_TempSens[1][kk].push_back(j["Module"][ii]["TempSens"][jj][kk]);
	  vec_CameraDist[1][kk].push_back(j["Module"][ii]["CameraDist"][jj][kk]);
	};
	if(jj==1){
	  vec_TempSens[3][kk].push_back(j["Module"][ii]["TempSens"][jj][kk]);
	  vec_CameraDist[3][kk].push_back(j["Module"][ii]["CameraDist"][jj][kk]);
	};
	if(jj==2){
	  vec_TempSens[5][kk].push_back(j["Module"][ii]["TempSens"][jj][kk]);
	  vec_CameraDist[5][kk].push_back(j["Module"][ii]["CameraDist"][jj][kk]);
	};
	if(jj==3){
	  vec_TempSens[0][kk].push_back(-99);
	  vec_CameraDist[0][kk].push_back(-99);
	};
	if(jj==4){
	  vec_TempSens[2][kk].push_back(-99);
	  vec_CameraDist[2][kk].push_back(-99);
	};
	if(jj==5){
	  vec_TempSens[4][kk].push_back(-99);
	  vec_CameraDist[4][kk].push_back(-99);
	};
	if(jj==6){
	  vec_TempSens[6][kk].push_back(-99);
	  vec_CameraDist[6][kk].push_back(-99);
	};
      }
    }
    makePlot(j["Module"][ii]["ModuleName"], plotX[0], plotX[1], plotX[2], plotX[3], plotX[4]);
    modulePlot(j["Module"][ii]["ModuleName"], plotX[0], plotX[1], plotX[5], plotTempSens, plotCamera);
	
  }
  TCanvas *c2=new TCanvas("","",600,400);
  c2->SetBottomMargin(0.15);
  TGraph *gr[9];
  TGraph *gr16[3][16];

  for(int ii=0;ii<numOfModule;ii++){
    xAxis[ii]=ii+0.5;
    if(id==0||id==1){
      /*
      data20[ii]=vec_ModuleNTC[2][ii];
      data10[ii]=vec_ModuleNTC[4][ii];
      dataMin[ii]=vec_ModuleNTC[6][ii];
      */
      data20[ii]=vec_ModuleNTC[1][ii]-vec_JigTemp[1][ii];
      data10[ii]=vec_ModuleNTC[3][ii]-vec_JigTemp[3][ii];
      dataMin[ii]=vec_ModuleNTC[5][ii]-vec_JigTemp[5][ii];
      
    }else if(id==2||id==3){
      /*
      data20[ii]=vec_Current[1][ii];
      data10[ii]=vec_Current[3][ii];
      dataMin[ii]=vec_Current[5][ii];
      */
      data20[ii]=vec_Current[1][ii]-vec_JigTemp[1][ii];
      data10[ii]=vec_Current[3][ii]-vec_JigTemp[3][ii];
      dataMin[ii]=vec_Current[5][ii]-vec_JigTemp[5][ii];
    }else if(id==4){
      for(int kk=0;kk<16;kk++){
	dataSens20[kk][ii]=vec_TempSens[1][kk][ii];
	dataSens10[kk][ii]=vec_TempSens[3][kk][ii];
	dataSensMin[kk][ii]=vec_TempSens[5][kk][ii];
      }
    }else if(id==5||id==6||id==7){
      for(int kk=0;kk<16;kk++){
        dataSens20[kk][ii]=vec_TempSens[1][kk][ii]-vec_JigTemp[1][ii];
        dataSens10[kk][ii]=vec_TempSens[3][kk][ii]-vec_JigTemp[3][ii];
        dataSensMin[kk][ii]=vec_TempSens[5][kk][ii]-vec_JigTemp[5][ii];
      }  
    }else if(id==8){
      for(int kk=0;kk<16;kk++){
	dataSens20[kk][ii]=vec_CameraDist[1][kk][ii];
	dataSens10[kk][ii]=vec_CameraDist[3][kk][ii];
	dataSensMin[kk][ii]=vec_CameraDist[5][kk][ii];
      }
    }else if(id==9){
      for(int kk=0;kk<16;kk++){
	dataSens20[kk][ii]=vec_CameraDist[1][kk][ii]-vec_JigTemp[1][ii];
	dataSens10[kk][ii]=vec_CameraDist[3][kk][ii]-vec_JigTemp[3][ii];
	dataSensMin[kk][ii]=vec_CameraDist[5][kk][ii]-vec_JigTemp[5][ii];
      }
    }

    if(id==0||id==2){
      data20[ii]=data20[ii]/(4.6*vec_Power[1][ii]);
      data10[ii]=data10[ii]/(4.6*vec_Power[3][ii]);
      dataMin[ii]=dataMin[ii]/(4.6*vec_Power[5][ii]);
    }
    //    if(id==7||id==9){
    if(0){
      for(int kk=0;kk<16;kk++){
	dataSens20[kk][ii]=dataSens20[kk][ii]/(4.6*vec_Power[1][ii]);
	dataSens10[kk][ii]=dataSens10[kk][ii]/(4.6*vec_Power[3][ii]);
	dataSensMin[kk][ii]=dataSensMin[kk][ii]/(4.6*vec_Power[5][ii]);
      }
    }

    if(id==0||id==1||id==2||id==3){    
      if(data20[ii]>=highest&&data20[ii]>=-10){highest=data20[ii];}
      if(data10[ii]>=highest&&data10[ii]>=-10){highest=data10[ii];}
      if(dataMin[ii]>=highest&&dataMin[ii]>=-10){highest=dataMin[ii];}

      if(data20[ii]<=lowest&&data20[ii]>=-20){lowest=data20[ii];}
      if(data10[ii]<=lowest&&data10[ii]>=-20){lowest=data10[ii];}
      if(dataMin[ii]<=lowest&&dataMin[ii]>=-20){lowest=dataMin[ii];}
    }else if(id==4||id==5||id==6||id==7||id==8||id==9){
      for(int kk=0;kk<16;kk++){
	if(dataSens20[kk][ii]>=highest&&dataSens20[kk][ii]>=-20){highest=dataSens20[kk][ii];}
	if(dataSens10[kk][ii]>=highest&&dataSens10[kk][ii]>=-20){highest=dataSens10[kk][ii];}
	if(dataSensMin[kk][ii]>=highest&&dataSensMin[kk][ii]>=-20){highest=dataSensMin[kk][ii];}
	
	if(dataSens20[kk][ii]<=lowest&&dataSens20[kk][ii]>=-20){lowest=dataSens20[kk][ii];}
	if(dataSens10[kk][ii]<=lowest&&dataSens10[kk][ii]>=-20){lowest=dataSens10[kk][ii];}
	if(dataSensMin[kk][ii]<=lowest&&dataSensMin[kk][ii]>=-20){lowest=dataSensMin[kk][ii];}
      }
    }


  }
  if(id==0||id==1||id==2||id==3){
    gr[0]=new TGraph(numOfModule, xAxis, data20);
    gr[1]=new TGraph(numOfModule, xAxis, data10);
    gr[2]=new TGraph(numOfModule, xAxis, dataMin);
  }else if(id==4||id==5||id==6||id==7||id==8||id==9){
    for(int kk=0;kk<16;kk++){
      gr16[0][kk]=new TGraph(numOfModule, xAxis, dataSens20[kk]);
      gr16[1][kk]=new TGraph(numOfModule, xAxis, dataSens10[kk]);
      gr16[2][kk]=new TGraph(numOfModule, xAxis, dataSensMin[kk]);
    }
  }

  if(id==0||id==1||id==2||id==3){
    for(int ii=0;ii<numOfModule;ii++){
      gr[0]->GetXaxis()->SetBinLabel(2.5*ii+2.0, vec_ModuleName[ii].c_str());      
    }
    gr[0]->SetMarkerStyle(21);
    gr[1]->SetMarkerStyle(20);
    gr[2]->SetMarkerStyle(22);
    gr[0]->SetMarkerColor(2);
    gr[1]->SetMarkerColor(4);
    gr[2]->SetMarkerColor(1);
    if(id==0||id==1){gr[0]->SetTitle("Module NTC of each Jig temperature");}
    else if(id==2||id==3){gr[0]->SetTitle("Temperature from current of each Jig temperature");}
    if(id==0){gr[0]->GetYaxis()->SetTitle("Module NTC - Jig Temp/Power [C/W]");}
    if(id==1){gr[0]->GetYaxis()->SetTitle("Module NTC - Jig Temp [C]");}
    //    if(id==1){gr[0]->GetYaxis()->SetTitle("Module NTC - Jig Temp [C]");}
    
    if(id==2){gr[0]->GetYaxis()->SetTitle("Temp. from Current - Jig Temp/Power [C/W]");}
    if(id==3){gr[0]->GetYaxis()->SetTitle("Temp. from Current - Jig Temp [C]");}
    //    if(id==3){gr[0]->GetYaxis()->SetTitle("Temp. from Current - Jig Temp [C]");}
    gr[0]->GetXaxis()->SetLabelSize(0.05);
    //    if(id==0||id==2){gr[0]->GetYaxis()->SetRangeUser(lowest-0.2,highest+0.2);}
    //    if(id==1||id==3){gr[0]->GetYaxis()->SetRangeUser(lowest-0.5,highest+0.5);}

    if(id==0||id==2){gr[0]->GetYaxis()->SetRangeUser(4,10);}
    if(id==1||id==3){gr[0]->GetYaxis()->SetRangeUser(4,10);}
    gr[0]->Draw("ap");
    gr[1]->Draw("psame");
    //    gr[2]->Draw("psame");

    TLegend* leg = new TLegend(0.6,0.65,0.85,0.85);
    leg->AddEntry(gr[0],"Jig 20C","p");
    leg->AddEntry(gr[1],"Jig 10C","p");
    //    leg->AddEntry(gr[2],"Jig -10C","p");
    leg->Draw();
    
  }else if(id==4||id==5||id==6||id==7||id==8||id==9){
    cout<<"tempsensplot"<<endl;
    for(int kk=0;kk<16;kk++){
      gr16[0][kk]->SetMarkerStyle(21);
      gr16[1][kk]->SetMarkerStyle(20);
      gr16[2][kk]->SetMarkerStyle(22);
      gr16[0][kk]->SetMarkerColor(2);
      gr16[1][kk]->SetMarkerColor(4);
      gr16[2][kk]->SetMarkerColor(1);
      

      if(kk==0){
	cout<<highest<<" "<<lowest<<endl;
	if(id==5||id==9){gr16[0][kk]->GetYaxis()->SetRangeUser(lowest-0.5,highest+0.5);}
	if(id==4||id==8||id==7){gr16[0][kk]->GetYaxis()->SetRangeUser(lowest-0.5,highest+0.5);}
	for(int ii=0;ii<numOfModule;ii++){
	  gr16[0][kk]->GetXaxis()->SetBinLabel(2.5*ii+2.0, vec_ModuleName[ii].c_str());
	}
	if(id==4){
	  gr16[0][kk]->SetTitle("Temperature Sensor in Chip");
	  gr16[0][kk]->GetYaxis()->SetTitle("Temperature Sensors [C]");
	}else if(id==5||id==6||id==7){
	  gr16[0][kk]->SetTitle("Temperature Sensor in Chip");
	  gr16[0][kk]->GetYaxis()->SetTitle("Temperature Sensors - Jig temperature [C]");
	}else if(id==8){
	  gr16[0][kk]->SetTitle("Temperature from Camera");
          gr16[0][kk]->GetYaxis()->SetTitle("Temperature [C]");
	}else if(id==9){
	  gr16[0][kk]->SetTitle("Temperature from Camera");
          gr16[0][kk]->GetYaxis()->SetTitle("Camera - Jig temperature [C]");
	}
	cout<<"before draw"<<endl;
	gr16[0][kk]->Draw("ap");
	gr16[1][kk]->Draw("psame");
	gr16[2][kk]->Draw("psame");
      }else{
	gr16[0][kk]->Draw("psame");
	gr16[1][kk]->Draw("psame");
	gr16[2][kk]->Draw("psame");
      }
      	cout<<"after draw"<<endl;
    }
    TLegend* leg = new TLegend(0.7,0.75,0.95,0.95);
    leg->AddEntry(gr16[0][0],"Jig 20C","p");
    leg->AddEntry(gr16[1][0],"Jig 10C","p");
    leg->AddEntry(gr16[2][0],"Jig -10C","p");
    leg->Draw();

  }
    
  c2->SaveAs("jsontest.pdf");
  //  for(int ii=0;ii<9;ii++){
  //    gr[ii]=new TGraph(7,
}
