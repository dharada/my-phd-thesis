#include <fstream>
#include <nlohmann/json.hpp>
#include <iostream>
#include <TCanvas.h>
#include <TGraph.h>
#include <TGraphErrors.h>
#include <TF1.h>
#include <TLegend.h>
#include <TAxis.h>
#include <TLine.h>
#include <TMultiGraph.h>
using namespace std;

#include "SaveTGraph.cpp"
#include "GraphByModule.cpp"
#include "function_2.cpp"
// for convenience                                                                                                              
using json = nlohmann::json;

int main(){
  bool includeCameAndSens=false;
  cout<<"0: Absolute temperature, 1: Absolute temperature/Power, 2: Diff temperature, 3: Diff temperature/Power"<<endl;
  //    cout<<"0: normalised NTC, 1: just NTC, 2: normalised current, 3: just current, 4:TempSens(all), 5:TempSensDiff(20C), 6:TempSensDiff(10C), 7:TempSensDiff(-10C), 8:ThermalCamera(all), 9:ThermalCamera(Diff), 10:By module, by temp"<<endl;
  int id; //0: normalised NTC, 1: just NTC, 2: normalised current, 3: just current, 4:TempSens(all), 5:TempSensDiff(20C), 6:TempSensDiff(10C), 7:TempSensDiff(-10C), 8:ThermalCamera(all)
  string plotType[4]={"absolute", "abspower", "diff", "diffpower"};
  cin>>id;
  double highest[3]={0,0,0};
  double lowest[3]={100,100,100};
  TCanvas *c1=new TCanvas("","",600,450);
  ifstream i("TCmodule.json");

  json j;
  i >> j;
  const int numOfModule=j["Module"].size();
  cout<<"numOfModule"<<numOfModule<<endl;
  //  const int numOfModule=37;
  //number of module, CERN:9, KEK:7, Paris:12, Siegen:4, Goe:5
  moduleData moduleData[numOfModule];  
  double data20[numOfModule], data10[numOfModule], dataMin[numOfModule];
  double dataSens20[16][numOfModule], dataSens10[16][numOfModule], dataSensMin[16][numOfModule];

  double data_N[3][3][numOfModule];
  double data_C[3][3][numOfModule];
  double data_EC[3][3][numOfModule];
  double data_E0[3][3][numOfModule];
  double dataSens_T[3][16][numOfModule];
  double dataSens_C[3][16][numOfModule];


  bool isFirstEvent=true;

  double xAxis[numOfModule];
  double yAxis[7];

  double plotX[6][7];
  double plotTempSens[3][16];
  double plotCamera[3][16];
  //set the data

  string sendId[16]={"Sens1.1", "Sens1.2", "Sens1.3", "Sens1.4", "Sens2.1", "Sens2.2", "Sens2.3", "Sens2.4", "Sens3.1", "Sens3.2", "Sens3.3", "Sens3.4", "Sens4.1", "Sens4.2", "Sens4.3", "Sens4.4"};
  
  for(int ii=0;ii<numOfModule;ii++){
    moduleData[ii].setModuleName(j["Module"][ii]["ModuleName"]);
    moduleData[ii].setTCcycles(j["Module"][ii]);
    moduleData[ii].setModuleNTC(j["Module"][ii]);
    moduleData[ii].setJigTemp(j["Module"][ii]);
    moduleData[ii].setCurrTemp(j["Module"][ii]);
    moduleData[ii].setLV(j["Module"][ii]);
    moduleData[ii].setECurr(j["Module"][ii]);
    moduleData[ii].setE0(j["Module"][ii]);
    moduleData[ii].makePlots();
    moduleData[ii].setMaxMin();
  }

  TGraphErrors *gr[3][3][2];
  
  for(int ii=0;ii<numOfModule;ii++){
    xAxis[ii]=ii+0.5;
    for(int TC=0;TC<moduleData[ii].retCycle().size();TC++){
      
      if(id==0||id==1){
	
	data_N[TC][0][ii]=moduleData[ii].retNTC()[TC][1];
	data_N[TC][1][ii]=moduleData[ii].retNTC()[TC][3];
	data_N[TC][2][ii]=moduleData[ii].retNTC()[TC][5];
      
	data_C[TC][0][ii]=moduleData[ii].retcurr()[TC][1];
	data_C[TC][1][ii]=moduleData[ii].retcurr()[TC][3];
	data_C[TC][2][ii]=moduleData[ii].retcurr()[TC][5];

	data_EC[TC][0][ii]=moduleData[ii].retECurr()[TC][1];
	data_EC[TC][1][ii]=moduleData[ii].retECurr()[TC][3];
	data_EC[TC][2][ii]=moduleData[ii].retECurr()[TC][5];

	data_E0[TC][0][ii]=moduleData[ii].retE0()[TC][1];
	data_E0[TC][1][ii]=moduleData[ii].retE0()[TC][3];
	data_E0[TC][2][ii]=moduleData[ii].retE0()[TC][5];
      
    }else if(id==2||id==3){

	data_N[TC][0][ii]=moduleData[ii].retNTC()[TC][1]-moduleData[ii].retJig()[TC][1];
	data_N[TC][1][ii]=moduleData[ii].retNTC()[TC][3]-moduleData[ii].retJig()[TC][3];
	data_N[TC][2][ii]=moduleData[ii].retNTC()[TC][5]-moduleData[ii].retJig()[TC][5];
      
	data_C[TC][0][ii]=moduleData[ii].retcurr()[TC][1]-moduleData[ii].retJig()[TC][1];
	data_C[TC][1][ii]=moduleData[ii].retcurr()[TC][3]-moduleData[ii].retJig()[TC][3];
	data_C[TC][2][ii]=moduleData[ii].retcurr()[TC][5]-moduleData[ii].retJig()[TC][5];

	data_EC[TC][0][ii]=moduleData[ii].retECurr()[TC][1];
	data_EC[TC][1][ii]=moduleData[ii].retECurr()[TC][3];
	data_EC[TC][2][ii]=moduleData[ii].retECurr()[TC][5];

	data_E0[TC][0][ii]=moduleData[ii].retE0()[TC][1];
	data_E0[TC][1][ii]=moduleData[ii].retE0()[TC][3];
	data_E0[TC][2][ii]=moduleData[ii].retE0()[TC][5];
    }

    if(id==1||id==3){
      data_N[TC][0][ii]=data_N[TC][0][ii]/(4.6*moduleData[ii].retLV()[TC][1]);
      data_N[TC][1][ii]=data_N[TC][1][ii]/(4.6*moduleData[ii].retLV()[TC][3]);
      data_N[TC][2][ii]=data_N[TC][2][ii]/(4.6*moduleData[ii].retLV()[TC][5]);

      data_C[TC][0][ii]=data_C[TC][0][ii]/(4.6*moduleData[ii].retLV()[TC][1]);
      data_C[TC][1][ii]=data_C[TC][1][ii]/(4.6*moduleData[ii].retLV()[TC][3]);
      data_C[TC][2][ii]=data_C[TC][2][ii]/(4.6*moduleData[ii].retLV()[TC][5]);

      data_EC[TC][0][ii]=moduleData[ii].retECurr()[TC][1]/(4.6*moduleData[ii].retLV()[TC][1]);;
      data_EC[TC][1][ii]=moduleData[ii].retECurr()[TC][3]/(4.6*moduleData[ii].retLV()[TC][3]);;
      data_EC[TC][2][ii]=moduleData[ii].retECurr()[TC][5]/(4.6*moduleData[ii].retLV()[TC][5]);;

      data_E0[TC][0][ii]=moduleData[ii].retE0()[TC][1];
      data_E0[TC][1][ii]=moduleData[ii].retE0()[TC][3];
      data_E0[TC][2][ii]=moduleData[ii].retE0()[TC][5];
    }
    
    for(int jj=0;jj<3;jj++){
      //highest[jj]=retMax(data_N[jj][ii], data_C[jj][ii], dataSens_T[jj], dataSens_C[jj], highest[jj], ii, includeCameAndSens);
      //      lowest[jj]=retMin(data_N[jj][ii], data_C[jj][ii], dataSens_T[jj], dataSens_C[jj], lowest[jj], ii, includeCameAndSens);
    }
    }
  }  

  retMeanSigma(data_N[0][0],data_N[1][0],data_N[2][0],"NTC  20C");
  retMeanSigma(data_N[0][1],data_N[1][1],data_N[2][1],"NTC  10C");
  retMeanSigma(data_N[0][2],data_N[1][2],data_N[2][2],"NTC -20C");
  cout<<endl;
  retMeanSigma(data_C[0][0],data_C[1][0],data_C[2][0],"Current  20C");
  retMeanSigma(data_C[0][1],data_C[1][1],data_C[2][1],"Current  10C");
  retMeanSigma(data_C[0][2],data_C[1][2],data_C[2][2],"Current -20C");
  cout<<endl;
  /*
  retMeanSigma2(dataSens_T[0],"Sensor  20C");
  retMeanSigma2(dataSens_T[1],"Sensor  10C");
  retMeanSigma2(dataSens_T[2],"Sensor -20C");
  cout<<endl;
  retMeanSigma2(dataSens_T[0],"Camera  20C");
  retMeanSigma2(dataSens_T[1],"Camera  10C");
  retMeanSigma2(dataSens_T[2],"Camera -20C");
  */
  
  // Add the fiunction to return mean and sigma

  cout<<highest[0]<<" "<<highest[1]<<" "<<highest[2]<<endl;
  cout<<lowest[0]<<" "<<lowest[1]<<" "<<lowest[2]<<endl;

  /*--------------------------------------------------------*/
  //Define graphs//
  cout<<"Check define graphs"<<endl;


  TCanvas *c2=new TCanvas("","",600,450);
  TMultiGraph *mg[3];
  TLegend* leg[3][2];
  string saveName;
  for(int kk=0;kk<3;kk++){
    mg[kk] = new TMultiGraph();
    leg[kk][0] = new TLegend(0.68,0.65,0.78,0.85);
    leg[kk][1] = new TLegend(0.78,0.65,0.88,0.85);
  }
  
  for(int tempId=0;tempId<3;tempId++){
    for(int TC=0;TC<3;TC++){
      gr[TC][tempId][0]=new TGraphErrors(numOfModule, xAxis, data_N[TC][tempId], data_E0[TC][tempId], data_E0[TC][tempId]);
      gr[TC][tempId][0]->SetMarkerStyle(20);
      gr[TC][tempId][0]->SetMarkerColor(1+TC);
          
      gr[TC][tempId][1]=new TGraphErrors(numOfModule, xAxis, data_C[TC][tempId], data_E0[TC][tempId], data_EC[TC][tempId]);
      gr[TC][tempId][1]->SetMarkerStyle(21);
      gr[TC][tempId][1]->SetMarkerColor(1+TC);

      for(int kk=0;kk<16;kk++){
	
      }
      mg[tempId]->Add(gr[TC][tempId][0]);
      mg[tempId]->Add(gr[TC][tempId][1]);
    }
  }

  for(int tempId =0;tempId<3;tempId++){
    /*
    if(tempId==0){mg[tempId]->SetTitle("Jig 20C");
    }else if(tempId==1){mg[tempId]->SetTitle("Jig 10C");
    }else if(tempId==2){mg[tempId]->SetTitle("Jig -10C");}
    */
    if(tempId==0){
      saveName=plotType[id]+"_20C.pdf";
      mg[tempId]->SetTitle("Module temperature (Cooling Jig = 20C)");
    }else if(tempId==1){
      saveName=plotType[id]+"_10C.pdf";
      mg[tempId]->SetTitle("Module temperature (Cooling Jig = 10C)");
    }else if(tempId==2){
      saveName=plotType[id]+"_-10C.pdf";
    }
    //    mg[tempId]->SetTitle(saveName.c_str());    

    mg[tempId]->GetXaxis()->SetLabelSize(0.05);
    for(int ii=0;ii<numOfModule;ii++){
      mg[tempId]->GetXaxis()->SetBinLabel(15*ii+5.0, moduleData[ii].retModuleName().c_str());
    }

    if(id==0){mg[tempId]->GetYaxis()->SetTitle("Temperature [C]");
    }else if(id==1){mg[tempId]->GetYaxis()->SetTitle("Temperature / Power [C/W]");
    }else if(id==2){mg[tempId]->GetYaxis()->SetTitle("Temperature from Jig[C]");
    }else if(id==3){mg[tempId]->GetYaxis()->SetTitle("Temperature from Jig / Power [C/W]");}

    //    if(id==0||id==2){mg[tempId]->GetYaxis()->SetRangeUser(lowest[tempId]-0.5,highest[tempId]+0.5);
    //    }else if(id==1||id==3){mg[tempId]->GetYaxis()->SetRangeUser(lowest[tempId]-0.2,highest[tempId]+0.2);}

    mg[tempId]->Draw("ap");
    string legName[3]={"Before TC", "50 cycles", "100 cycles"};;
    //    leg[tempId][0]->SetNColumns(2);
    leg[tempId][0]->SetBorderSize(0);

    //    leg[tempId][1]->SetNColumns(2);
    leg[tempId][1]->SetBorderSize(0);
    
    for(int TC=0;TC<3;TC++){
      leg[tempId][0]->SetHeader("Module NTC");
      leg[tempId][1]->SetHeader("Leakage Current");
      leg[tempId][0]->AddEntry(gr[TC][tempId][0],legName[TC].c_str(),"p");
      leg[tempId][1]->AddEntry(gr[TC][tempId][1],legName[TC].c_str(),"p");
    }
      if(includeCameAndSens){
      //      leg[tempId]->AddEntry(gr16[tempId][0][0],"Temperature sensors","p");
      //      leg[tempId]->AddEntry(gr16[tempId][1][0],"Thermal Camera","p");
    }
    leg[tempId][0]->Draw();
    leg[tempId][1]->Draw();


    c2->SaveAs(saveName.c_str());    
  }
  
  // edited till here
    

  //  for(int ii=0;ii<9;ii++){
  //    gr[ii]=new TGraph(7,
}
