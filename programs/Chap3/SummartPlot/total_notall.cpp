{
  double NTC20[10]= {25.5, 25.7, 26.5, 25.9, 25.9, 26.5, 26.1, 26.3, 26.7, 26.3};
  double NTC10[10]= {16.0, 15.9, 16.0, 15.8, 16.2, 16.6, 16.6, 16.9, 17.1, 16.3};
  double NTCmin[10]={-5.3, -5.1, -4.9, -5.0, -5.3, -3.7, -3.1, -3.3, -4.2, -3.9};
  string module[10]={"CERN7", "CERN10",  "KEK14", "KEK18", "Paris3", "Paris10", "Goe7", "Goe4", "Siegen3", "Siegen4"};

  double xAxis[10]={0.5, 1.5, 2.5, 3.5, 4.5, 5.5, 6.5, 7.5, 8.5, 9.5};
  THStack *hs = new THStack("hs","test stacked histograms");

  TGraph *gr[3];
  gr[0]=new TGraph(10,xAxis,NTC20);
  gr[1]=new TGraph(10,xAxis,NTC10);
  gr[2]=new TGraph(10,xAxis,NTCmin);

  for(int ii=0;ii<10;ii++){
    gr[0]->GetXaxis()->SetBinLabel(9.7*ii+5.5, module[ii].c_str());
    cout<<ii+1<<endl;
  }
  gr[0]->GetYaxis()->SetRangeUser(-10,30);

  gr[0]->SetMarkerStyle(20);
  gr[1]->SetMarkerStyle(21);
  gr[2]->SetMarkerStyle(22);

  gr[0]->SetMarkerColor(2);
  gr[1]->SetMarkerColor(4);
  gr[2]->SetMarkerColor(1);
  gr[0]->SetTitle("Module NTC of each Jig temperature");
  gr[0]->GetYaxis()->SetTitle("Module NTC [C]");
  gr[0]->GetXaxis()->SetLabelSize(0.06);  
  gr[0]->Draw("ap");
  gr[1]->Draw("psame");
  gr[2]->Draw("psame");

  TLegend* leg = new TLegend(0.6,0.65,0.85,0.85);
  leg->AddEntry(gr[0],"Jig 20C","p");
  leg->AddEntry(gr[1],"Jig 10C","p");
  leg->AddEntry(gr[2],"Jig -10C","p");

  leg->Draw();

  /*
  TH1F *hist[3];
  hist[0]=new TH1F("","",5,0,5);
  hist[1]=new TH1F("","",5,0,5);
  hist[2]=new TH1F("","",5,0,5);


  for(int ii=0;ii<5;ii++){
    hist[0]->Fill(xAxis[ii], NTC20[ii]);
    hist[1]->Fill(xAxis[ii], NTC10[ii]);
    hist[2]->Fill(xAxis[ii], NTCmin[ii]);
	    
  }
  hist[0]->SetFillColor(2);
  hist[1]->SetFillColor(3);
  hist[2]->SetFillColor(4);

  hs->Add(hist[0]);
  hs->Add(hist[1]);
  hs->Add(hist[2]);
  hist[0]->Draw("");
  */
  //  hs->Draw();
}
