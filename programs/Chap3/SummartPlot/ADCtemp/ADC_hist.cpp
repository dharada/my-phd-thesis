#include <fstream>
#include <nlohmann/json.hpp>
#include <iostream>
#include <vector>
#include <TCanvas.h>
#include <TGraph.h>
#include <TGraphErrors.h>
#include <TH1D.h>
#include <THStack.h>
#include <TF1.h>
#include <TLegend.h>
#include <TAxis.h>
#include <TLine.h>
using namespace std;

using json = nlohmann::json;

int main(){
  TH1D *hist_slope[4];
  TH1D *hist_offset[4];
  for(int ii=0;ii<4;ii++){
    hist_slope[ii]=new TH1D("","",30, 0.19, 0.22);
    hist_offset[ii]=new TH1D("","",35, 0, 35);
  }
  THStack *hs[2];
  hs[0]= new THStack("",";slope [mV/DAC]");
  hs[1]= new THStack("","; offset [mV]");
  TCanvas *c1=new TCanvas("","",600,400);

  ifstream i("mod_Nf.json");

  vector <double> v_slope[4];
  vector <double> v_offset[4];

  vector <double> v_xAxis[4];
  vector <double> v_yAxis[4];

  vector <double> v_xTotal;
  vector <double> v_yTotal;

  json j;
  i >> j;

  for(int ii=0;ii<j["Module"].size();ii++){
    cout<<j["Module"][ii]["ModuleName"]<<endl;
    for(int kk=0;kk<4;kk++){
      if(j["Module"][ii]["ADC"][kk][1]>0.10){
      double slope = j["Module"][ii]["ADC"][kk][0];
      double offset = j["Module"][ii]["ADC"][kk][1];
      v_slope[kk].push_back(slope);
      v_offset[kk].push_back(offset);
      

	v_xAxis[kk].push_back(slope);
	v_yAxis[kk].push_back(offset);
	v_xTotal.push_back(slope);
	v_yTotal.push_back(offset);
      }
      
    }
  }
  double Nf[4][v_slope[0].size()];
  double offset[4][v_offset[0].size()];
  /*
  for(int ii=0;ii<4;ii++){
    for(int jj=0;jj<v_Nf[ii].size();jj++){
      Nf[ii][jj]=v_Nf[ii][jj];
    }
    for(int jj=0;jj<v_offset[ii].size();jj++){
      offset[ii][jj]=v_offset[ii][jj];
    }
  }
  */
  for(int ii=0;ii<4;ii++){
    for(int jj=0;jj<v_slope[ii].size();jj++){
      hist_slope[ii]->Fill(v_slope[ii][jj]);
    }
    for(int jj=0;jj<v_offset[ii].size();jj++){
      hist_offset[ii]->Fill(v_offset[ii][jj]);
    }
    hist_slope[ii]->SetFillColor(ii+1);
    hist_offset[ii]->SetFillColor(ii+1);
  }
  for(int ii=0;ii<4;ii++){
    hs[0]->Add(hist_slope[ii],"hist");
    hs[1]->Add(hist_offset[ii],"hist");
  }
 
  hs[0]->Draw();
    TLegend* leg1 = new TLegend(0.6,0.6,0.85,0.85);
  for(int ii=0;ii<4;ii++){
    string title = "FE ";
    title = title+to_string(ii+1);
    leg1->AddEntry(hist_offset[ii],title.c_str(),"f");
  }
  leg1->Draw();
  c1->SaveAs("ADC_slope.pdf");
  TCanvas *c2 = new TCanvas("","",600, 400);
  hs[1]->Draw();

  TLegend* leg2 = new TLegend(0.15,0.6,0.3,0.85);
  for(int ii=0;ii<4;ii++){
    string title = "FE ";
    title = title+to_string(ii+1);
    leg2->AddEntry(hist_offset[ii],title.c_str(),"f");
  }
  leg2->Draw();
  c2->SaveAs("ADC_offset.pdf");

  double xAxis[4][999];
  double yAxis[4][999];
  for(int jj=0;jj<4;jj++){
    for(int ii=0;ii<v_xAxis[jj].size();ii++){
      xAxis[jj][ii]=v_xAxis[jj][ii];
      yAxis[jj][ii]=v_yAxis[jj][ii];
    }
  }
  TGraph *gr[4];
  for(int ii=0;ii<4;ii++){
    gr[ii]= new TGraph(v_xAxis[ii].size(), xAxis[ii], yAxis[ii]);
    gr[ii]->SetMarkerStyle(8);
    gr[ii]->SetMarkerColor(ii+1);
    gr[ii]->SetMarkerSize(0.5);
  }
  TCanvas *c3 = new TCanvas("","",600,400);
  //  gr[0]->GetXaxis()->SetRangeUser(1800,7000);
  //  gr[0]->GetYaxis()->SetRangeUser(-600, -120);
  gr[0]->SetTitle();
  gr[0]->GetXaxis()->SetTitle("Slope [T/V]");
  gr[0]->GetYaxis()->SetTitle("Offset [#circC]");
    
  
  gr[0]->Draw("ap");
  gr[1]->Draw("psame");
  gr[2]->Draw("psame");
  gr[3]->Draw("psame");

  TLegend* leg3 = new TLegend(0.6,0.6,0.8,0.85);
  for(int ii=0;ii<4;ii++){
    string title = "FE ";
    title = title+to_string(ii+1);
    leg3->AddEntry(gr[ii],title.c_str(),"p");
  }
  leg3->Draw();
  c3->SaveAs("ADC_relation.pdf");

  double xTotal[v_xTotal.size()];
  double yTotal[v_yTotal.size()];
  for(int ii=0;ii<v_xTotal.size();ii++){
    xTotal[ii]=v_xTotal[ii];
    yTotal[ii]=v_yTotal[ii];
  }
  TGraph *grTotal=new TGraph(v_xTotal.size(), xTotal, yTotal);
  TF1 *f1 = new TF1("","pol1");
  TCanvas *c4 = new TCanvas("","",600,400);
  grTotal->Draw("ap");
  grTotal->Fit(f1);
  c4->SaveAs("ADC_Fit.pdf");
}
