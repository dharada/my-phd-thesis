
#include <TROOT.h>                                                                                                                           
#include <TStyle.h>                                                                                                                          
#include <TGraph.h>                                                                                                                          
#include <TCanvas.h>                                                                                                                         
#include <TH1D.h>                                                                                                                            
#include <TF1.h>                                                                                                                             
#include <TLine.h>                                                                                                                           
#include <TLegend.h>
#include <THStack.h>
#include <TLatex.h>
using namespace std;

class Data{
};

//addData(vector<double> vecNTC20, vector<double> vecNTC10, vector<double> vecNTCmin, vector<string> vecname, double NTC20, double NTC10, double NTCmin, string name){

int main(){
  TCanvas *c1=new TCanvas("c1","",600,400);
  /*
  double NTC20[10]= {25.5, 25.7, 26.5, 25.9, 25.9, 26.5, 26.1, 26.3, 26.7, 26.3};
  double NTC10[10]= {16.0, 15.9, 16.0, 15.8, 16.2, 16.6, 16.6, 16.9, 17.1, 16.3};
  double NTCmin[10]={-5.3, -5.1, -4.9, -5.0, -5.3, -3.7, -3.1, -3.3, -4.2, -3.9};
  string module[10]={"CERN7", "CERN10",  "KEK14", "KEK18", "Paris3", "Paris10", "Goe7", "Goe4", "Siegen3", "Siegen4"};
  `*/
  TLine *line[2];
  vector <double> vecNTC20;
  vector <double> vecNTC10;
  vector <double> vecNTCmin;
  cout<<"check"<<endl;
  vector <string> vecName;
  //All FE
  vecName.push_back("CERN3"); vecNTC20.push_back(27.3); vecNTC10.push_back(17.8); vecNTCmin.push_back(-2.7);
  vecName.push_back("CERN5"); vecNTC20.push_back(28.9); vecNTC10.push_back(19.1); vecNTCmin.push_back(-1.1);
  vecName.push_back("CERN6"); vecNTC20.push_back(27.5); vecNTC10.push_back(17.7); vecNTCmin.push_back(-3.2);
  vecName.push_back("CERN7"); vecNTC20.push_back(25.5); vecNTC10.push_back(16.0); vecNTCmin.push_back(-5.3);
  vecName.push_back("CERN11"); vecNTC20.push_back(25.5); vecNTC10.push_back(15.9); vecNTCmin.push_back(-5.4);

  vecName.push_back("Goe4"); vecNTC20.push_back(26.3); vecNTC10.push_back(16.9); vecNTCmin.push_back(-3.3);
  vecName.push_back("Goe7"); vecNTC20.push_back(26.1); vecNTC10.push_back(16.6); vecNTCmin.push_back(-3.1);
  vecName.push_back("Goe10"); vecNTC20.push_back(27.1); vecNTC10.push_back(17.2); vecNTCmin.push_back(-2.6);

  
  vecName.push_back("KEK11"); vecNTC20.push_back(27.3); vecNTC10.push_back(16.9); vecNTCmin.push_back(-3.3);
  vecName.push_back("KEK14"); vecNTC20.push_back(26.5); vecNTC10.push_back(16.0); vecNTCmin.push_back(-4.9);
  vecName.push_back("KEK17"); vecNTC20.push_back(27.9); vecNTC10.push_back(17.8); vecNTCmin.push_back(-3.0);
  vecName.push_back("KEK18"); vecNTC20.push_back(27.9); vecNTC10.push_back(17.7); vecNTCmin.push_back(-1.9);
  vecName.push_back("KEK19"); vecNTC20.push_back(27.5); vecNTC10.push_back(16.9); vecNTCmin.push_back(-4.0);
  vecName.push_back("KEK20"); vecNTC20.push_back(25.7); vecNTC10.push_back(15.9); vecNTCmin.push_back(-4.5);
  vecName.push_back("KEK21"); vecNTC20.push_back(25.7); vecNTC10.push_back(15.9); vecNTCmin.push_back(-5.6);

  
  vecName.push_back("Paris3"); vecNTC20.push_back(25.9); vecNTC10.push_back(16.2); vecNTCmin.push_back(-5.3);
  vecName.push_back("Paris6"); vecNTC20.push_back(26.9); vecNTC10.push_back(17.2); vecNTCmin.push_back(-2.7);
  vecName.push_back("Paris8"); vecNTC20.push_back(26.3); vecNTC10.push_back(16.3); vecNTCmin.push_back(-3.9);
  vecName.push_back("Paris12"); vecNTC20.push_back(27.3); vecNTC10.push_back(17.7); vecNTCmin.push_back(-3.8);
  vecName.push_back("Paris16"); vecNTC20.push_back(28.7); vecNTC10.push_back(19.2); vecNTCmin.push_back(-1.7);
  vecName.push_back("Paris17"); vecNTC20.push_back(26.5); vecNTC10.push_back(16.5); vecNTCmin.push_back(-4.7);
  
  vecName.push_back("Siegen1"); vecNTC20.push_back(24.4); vecNTC10.push_back(16.8); vecNTCmin.push_back(-3.4);
  vecName.push_back("Siegen2"); vecNTC20.push_back(28.3); vecNTC10.push_back(18.8); vecNTCmin.push_back(-2.6);
  vecName.push_back("Siegen3"); vecNTC20.push_back(26.7); vecNTC10.push_back(17.1); vecNTCmin.push_back(-4.2);
  vecName.push_back("Siegen4"); vecNTC20.push_back(26.3); vecNTC10.push_back(16.3); vecNTCmin.push_back(-3.9);


  line[0]=new TLine(vecName.size()+0.5,-15,vecName.size()+0.5,30);
  vecName.push_back(""); vecNTC20.push_back(-99); vecNTC10.push_back(-99); vecNTCmin.push_back(-99);
  //3FE
  vecName.push_back("CERN10"); vecNTC20.push_back(25.7); vecNTC10.push_back(15.9); vecNTCmin.push_back(-5.1);
  vecName.push_back("Goe5"); vecNTC20.push_back(26.9); vecNTC10.push_back(17.1); vecNTCmin.push_back(-4.6);
  vecName.push_back("Paris7"); vecNTC20.push_back(26.3); vecNTC10.push_back(16.6); vecNTCmin.push_back(-4.8);
  vecName.push_back("Paris9"); vecNTC20.push_back(30.0); vecNTC10.push_back(20.0); vecNTCmin.push_back(-1.3);
  vecName.push_back("Paris10"); vecNTC20.push_back(26.5); vecNTC10.push_back(16.6); vecNTCmin.push_back(-3.7);
  vecName.push_back("Paris11"); vecNTC20.push_back(26.7); vecNTC10.push_back(16.8); vecNTCmin.push_back(-4.6);
  vecName.push_back("Paris13"); vecNTC20.push_back(26.3); vecNTC10.push_back(16.8); vecNTCmin.push_back(-4.7);
  vecName.push_back("Paris18"); vecNTC20.push_back(26.3); vecNTC10.push_back(16.6); vecNTCmin.push_back(-5.1);


  line[1]=new TLine(vecName.size()+0.5,-15,vecName.size()+0.5,30);
    vecName.push_back(""); vecNTC20.push_back(-99); vecNTC10.push_back(-99); vecNTCmin.push_back(-99);
  //2FE
  vecName.push_back("CERN8"); vecNTC20.push_back(27.9); vecNTC10.push_back(18.5); vecNTCmin.push_back(-2.0);  
  vecName.push_back(""); vecNTC20.push_back(-99); vecNTC10.push_back(-99); vecNTCmin.push_back(-99);

  int numModule = vecName.size();
  cout<<"check"<<endl;  
  double NTC20[38], NTC10[38], NTCmin[38];
  string module[38];
  double xAxis[38];

  for(int ii=0;ii<numModule;ii++){
    NTC20[ii]=vecNTC20[ii];
    NTC10[ii]=vecNTC10[ii];
    NTCmin[ii]=vecNTCmin[ii];
    module[ii]=vecName[ii];
    xAxis[ii]=ii+0.5;
  }
  //  cout<<"Check"<<endl;
  //  double xAxis[10]={0.5, 1.5, 2.5, 3.5, 4.5, 5.5, 6.5, 7.5, 8.5, 9.5};
  THStack *hs = new THStack("hs","test stacked histograms");

  TGraph *gr[3];
  gr[0]=new TGraph(numModule,xAxis,NTC20);
  gr[1]=new TGraph(numModule,xAxis,NTC10);
  gr[2]=new TGraph(numModule,xAxis,NTCmin);

  for(int ii=0;ii<numModule;ii++){
    gr[0]->GetXaxis()->SetBinLabel(2.5*ii+2.0, module[ii].c_str());

  }
  gr[0]->GetYaxis()->SetRangeUser(-15,30);

  gr[0]->SetMarkerStyle(20);
  gr[1]->SetMarkerStyle(21);
  gr[2]->SetMarkerStyle(22);

  gr[0]->SetMarkerColor(2);
  gr[1]->SetMarkerColor(4);
  gr[2]->SetMarkerColor(1);
  gr[0]->SetTitle("Module NTC of each Jig temperature");
  gr[0]->GetYaxis()->SetTitle("Module NTC [C]");
  gr[0]->GetXaxis()->SetLabelSize(0.05);  
  gr[0]->Draw("ap");
  gr[1]->Draw("psame");
  gr[2]->Draw("psame");

  line[0]->Draw();
  line[1]->Draw();
  TLegend* leg = new TLegend(0.12,0.4,0.3,0.57);
  leg->AddEntry(gr[0],"Jig 20C","p");
  leg->AddEntry(gr[1],"Jig 10C","p");
  leg->AddEntry(gr[2],"Jig -10C","p");

  leg->Draw();

  TLatex latex(2,220,"All FEs working");
  latex.DrawLatex(2, -14, "All FEs working");
  latex.DrawLatex(26, -14, "3 FEs");
  latex.DrawLatex(35, -14, "2 FEs");
  /*
    TLegend* lg[3];
  for(int ii=0;ii<3;ii++){
    lg[ii]= new TLegend(0.2,0.65,0.4,0.85);
    lg[ii]->SetBorderSize(0);
    lg[ii]->SetFillColor(0);
  }

  lg[0]->SetHeader("All FEs working");
  lg[1]->SetHeader("3 FEs");
  lg[2]->SetHeader("2 FEs");

  for(int ii=0;ii<3;ii++){
    lg[ii]->Draw();
  }
  */
  c1->SaveAs("hoge.pdf");
  
  /*
  TH1F *hist[3];
  hist[0]=new TH1F("","",5,0,5);
  hist[1]=new TH1F("","",5,0,5);
  hist[2]=new TH1F("","",5,0,5);


  for(int ii=0;ii<5;ii++){
    hist[0]->Fill(xAxis[ii], NTC20[ii]);
    hist[1]->Fill(xAxis[ii], NTC10[ii]);
    hist[2]->Fill(xAxis[ii], NTCmin[ii]);
	    
  }
  hist[0]->SetFillColor(2);
  hist[1]->SetFillColor(3);
  hist[2]->SetFillColor(4);

  hs->Add(hist[0]);
  hs->Add(hist[1]);
  hs->Add(hist[2]);
  hist[0]->Draw("");
  */
  //  hs->Draw();
}
