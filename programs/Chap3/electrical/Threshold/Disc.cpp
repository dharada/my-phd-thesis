#include <iostream>
#include <fstream>
#include <nlohmann/json.hpp>
#include <tuple>
#include <math.h>
#include <string>
#include <sstream>

#include <TROOT.h>
#include <TGraph.h>
#include <TGraphErrors.h>
#include <TH1D.h>
#include <THStack.h>
#include <TStyle.h>
#include <TLegend.h>
#include <TCanvas.h>
#include <TGaxis.h>
#include <TAxis.h>

using namespace std;
using json = nlohmann::json;
class moduleData{
public:
  string moduleName;
  string fileDir;
  
};
class assembly{
public:
  string site;
  string savename;
  void setName(string a, string b){
    site=a;
    savename=b;
  }
  vector <string> modules;
  TH1D *hist[2][4];
  void defineHist(){
    int number = modules.size();
    for(int kk=0;kk<4;kk++){
    for(int ii=0;ii<2;ii++){
      hist[ii][kk]=new TH1D("","",6*number, 0, number);
      //      hist[ii][kk]=new TH1D("","",8*number, 0, number);
      hist[ii][kk]->SetBarWidth(0.65);
      hist[ii][kk]->SetBarOffset(0.5);
      hist[ii][kk]->SetFillColor(kk+1);
      hist[ii][kk]->SetLineColor(kk+1);
      
      for(int jj=0;jj<number;jj++){
	hist[ii][kk]->GetXaxis()->SetBinLabel(6*jj+1,"");
	hist[ii][kk]->GetXaxis()->SetBinLabel(6*jj+2,"");
	hist[ii][kk]->GetXaxis()->SetBinLabel(6*jj+4,"");
	hist[ii][kk]->GetXaxis()->SetBinLabel(6*jj+5,"");
	hist[ii][kk]->GetXaxis()->SetBinLabel(6*jj+3,modules[jj].c_str());
	hist[ii][kk]->GetXaxis()->SetBinLabel(6*jj+6,"");
	/*
	hist[ii][kk]->GetXaxis()->SetBinLabel(8*jj+1,"");
	hist[ii][kk]->GetXaxis()->SetBinLabel(8*jj+2,"");
	hist[ii][kk]->GetXaxis()->SetBinLabel(8*jj+3,"");
	hist[ii][kk]->GetXaxis()->SetBinLabel(8*jj+5,"");
	hist[ii][kk]->GetXaxis()->SetBinLabel(8*jj+4,modules[jj].c_str());
	hist[ii][kk]->GetXaxis()->SetBinLabel(8*jj+6,"");
	hist[ii][kk]->GetXaxis()->SetBinLabel(8*jj+7,"");
	hist[ii][kk]->GetXaxis()->SetBinLabel(8*jj+8,"");
	*/
      }
    }
    }
  }

  void setData(){
    for(int moduleN=0;moduleN<modules.size();moduleN++){
      bool isThre=true;	
      for(int countThre=0;countThre<1;countThre++){
	string fileDir="../../../../../ThesisData/Chap3/CSV/"+modules[moduleN];
	if(isThre){fileDir=fileDir+"/discbumpscan_evolution.csv";}
	else{fileDir=fileDir+"/NoiseDist_evolution.csv";}
	ifstream ifs(fileDir.c_str());
	cout<<fileDir<<endl;
	string name, chip, fe, stage, id;
	double mean, sigma, disc, zero, range;
	string line;
	string line2;
	bool isFirst=true;
	while(getline(ifs, line)){
	  if(isFirst){isFirst=false;}
	  else{
	    istringstream i_line(line);
	    int count=0;
	    while(getline(i_line, line2, ',')){
	      if(count==0){name=line2;}
	      else if(count==1){chip=line2;}
	      else if(count==2){id=line2;}
	      else if(count==3){fe=line2;}
	      else if(count==4){stage=line2;}
	      else if(count==5){range=stod(line2);}
	      else if(count==6){disc=stod(line2);}
	      else if(count==7){zero=stod(line2);}
	      count++;
	    }
	    if(stage=="Stage4"&&fe=="diff"){
	      int ife=0;
	      if(chip=="FE1"){ife=0;}else if(chip=="FE2"){ife=1;}else if(chip=="FE3"){ife=2;}else if(chip=="FE4"){ife=3;}
	      //	      cout<<name<<" "<<chip<<" "<<id<<" "<<fe<<" "<<stage<<" "<<mean<<" "<<sigma<<" "<<":::"<<moduleN+0.252+0.125*ife<<endl;
	      if(isThre){
		double dfe=ife;
		hist[0][ife]->Fill(moduleN+0.252+dfe/6,mean);
		hist[0][ife]->SetBinError(6*moduleN+3+ife, sigma);

		//		hist[0][ife]->Fill(moduleN+0.252+0.125*ife,mean);
		//		hist[0][ife]->SetBinError(8*moduleN+3+ife, sigma);
	      }else{
		double dfe=ife;
		hist[1][ife]->Fill(moduleN+0.252+dfe/6,mean);
		hist[1][ife]->SetBinError(6*moduleN+3+ife, sigma);

		//		hist[1][ife]->Fill(moduleN+0.252+0.125*ife,mean);
		//		hist[1][ife]->SetBinError(8*moduleN+3+ife, sigma);
	      }
	    }
	  }
	}
	//ModuleID,Chip,ChipID,FE,Stage,Threshold[e],Stdev[e]
	//	while(ifs>>
	isThre=false;
      }
    }
  }

  void createPlot(){
    TLegend* leg[2];
    leg[0]= new TLegend(0.6,0.2,0.85,0.45);
    leg[1]= new TLegend(0.6,0.2,0.85,0.45);
    for(int ii=0;ii<2;ii++){
      leg[ii]->AddEntry(hist[ii][0],"FE1","f");
      leg[ii]->AddEntry(hist[ii][1],"FE2","f");
      leg[ii]->AddEntry(hist[ii][2],"FE3","f");
      leg[ii]->AddEntry(hist[ii][3],"FE4","f");
    }
    
    TCanvas *c1 = new TCanvas("","",600,400);
    gPad->SetLeftMargin(0.15);
    gPad->SetBottomMargin(0.17);
    hist[0][0]->GetYaxis()->SetRangeUser(1000,1600);
    hist[0][0]->Draw("hist");
    string titleThre=site+", Threshold";
    hist[0][0]->SetTitle(titleThre.c_str());
    hist[0][0]->GetYaxis()->SetTitle("Threshold Mean [e]");
    hist[0][0]->GetXaxis()->SetLabelSize(0.06);
    hist[0][0]->GetYaxis()->SetLabelSize(0.05);
    hist[0][0]->GetYaxis()->SetTitleSize(0.05);
    
    gStyle->SetOptStat(0);
    for(int ii=1;ii<4;ii++){hist[0][ii]->Draw("histsame");}
    leg[0]->Draw();
    string save=savename+"_thre.pdf";
    c1->SaveAs(save.c_str());

    
    TCanvas *c2 = new TCanvas("","",600,400);
    gPad->SetLeftMargin(0.15);
    gPad->SetBottomMargin(0.17);
    hist[1][0]->Draw("hist");
    string titleNoise=site+", Noise";
    hist[1][0]->SetTitle(titleNoise.c_str());
    hist[1][0]->GetYaxis()->SetTitle("ENC noise [e]");
    hist[1][0]->GetXaxis()->SetLabelSize(0.06);
    hist[1][0]->GetYaxis()->SetLabelSize(0.05);
    hist[1][0]->GetYaxis()->SetTitleSize(0.05);
    gStyle->SetOptStat(0);
    for(int ii=1;ii<4;ii++){hist[1][ii]->Draw("histsame");}
    leg[1]->Draw();
    save=savename+"_noise.pdf";
    c2->SaveAs(save.c_str());
  }
};

int main(){
  int stop=6;
  vector <string> module_list[6];
  string input1[5]={"CERNQ4", "CERNQ8", "CERNQ9", "CERNQ10", "CERNQ11"};//CERN
  string input2[5]={"KEKQ19", "KEKQ20", "KEKQ22", "KEKQ24", "KEKQ25"};//KEK
  string input3[9]={"Paris6", "Paris7", "Paris8", "Paris9", "Paris10", "Paris11",  "Paris12", "Paris13", "Paris16"};//Paris
  string input4[4]={"Goe4", "Goe5", "Goe7", "Goe10"};//Goe
  string input5[2]={"Liv5", "Liv8"};//Liv
  string input6[4]={"SiegenQ1", "SiegenQ2", "SiegenQ3", "SiegenQ4"};//Siegen

  assembly place[6];
  place[0].setName("CERN","cern");
  place[1].setName("KEK","kek");
  place[2].setName("Paris","paris");
  place[3].setName("Goettingen","goe");
  place[4].setName("Liverpool","liv");
  place[5].setName("Siegen","siegen");
  for(int ii=0;ii<9;ii++){
    if(ii<5){module_list[0].push_back(input1[ii]);}
    if(ii<5){module_list[1].push_back(input2[ii]);}
    if(ii<9){module_list[2].push_back(input3[ii]);}
    if(ii<4){module_list[3].push_back(input4[ii]);}
    if(ii<2){module_list[4].push_back(input5[ii]);}
    if(ii<4){module_list[5].push_back(input6[ii]);}

    if(ii<5){place[0].modules.push_back(input1[ii]);}
    if(ii<5){place[1].modules.push_back(input2[ii]);}
    if(ii<9){place[2].modules.push_back(input3[ii]);}
    if(ii<4){place[3].modules.push_back(input4[ii]);}
    if(ii<2){place[4].modules.push_back(input5[ii]);}
    if(ii<4){place[5].modules.push_back(input6[ii]);}
  }
  for(int ii=0;ii<6;ii++){
    place[ii].defineHist();
    place[ii].setData();
    place[ii].createPlot();
  }
  
}
  
