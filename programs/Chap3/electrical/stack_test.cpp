#include <iostream>
#include <fstream>
#include <nlohmann/json.hpp>
#include <tuple>
#include <math.h>

#include <TROOT.h>
#include <TGraph.h>
#include <TGraphErrors.h>
#include <TH1D.h>
#include <THStack.h>
#include <TStyle.h>
#include <TLegend.h>
#include <TCanvas.h>
#include <TGaxis.h>
#include <TAxis.h>

using namespace std;

int main(){
  TH1D *hist=new TH1D("","", 1, 0, 1);
  hist->Fill(0.5, 1./768);
  hist->SetFillColor(1);
  TCanvas *c1 = new TCanvas("","",600,400);
  hist->Draw();
  c1->SaveAs("hist.pdf");
  THStack *hs = new THStack("hs","");
  hs->Add(hist);
  hs->Add(hist);
  TCanvas *c2 = new TCanvas("","",600,400);
  hs->Draw();
  c2->SaveAs("stack.pdf");
}
