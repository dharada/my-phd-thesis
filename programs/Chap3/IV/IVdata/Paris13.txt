{
    "unit": {
        "Time": "s",
        "Voltage": "V",
        "Current": "A"
    },
    "V_step": -5,
    "Step_duration": 2,
    "N_measurements_per_step": 10,
    "A_Compliance": 1e-05,
    "Sensor_IV": [
        {
            "Time": 26.793045043945312,
            "Voltage": 0.0,
            "Current_mean": 2.5016737300000002e-08,
            "Current_sigma": 1.2765051041599858e-08
        },
        {
            "Time": 32.788103342056274,
            "Voltage": -5.0,
            "Current_mean": -3.4448435999999996e-07,
            "Current_sigma": 1.632617772163466e-08
        },
        {
            "Time": 38.783241510391235,
            "Voltage": -10.0,
            "Current_mean": -3.7933947e-07,
            "Current_sigma": 1.0315213641418193e-08
        },
        {
            "Time": 44.778076171875,
            "Voltage": -15.0,
            "Current_mean": -3.9230561e-07,
            "Current_sigma": 8.148270617922545e-09
        },
        {
            "Time": 50.773038148880005,
            "Voltage": -20.0,
            "Current_mean": -3.9445547e-07,
            "Current_sigma": 6.773939895813959e-09
        },
        {
            "Time": 56.768009185791016,
            "Voltage": -25.0,
            "Current_mean": -3.9664674e-07,
            "Current_sigma": 6.557104361560824e-09
        },
        {
            "Time": 62.763039350509644,
            "Voltage": -30.0,
            "Current_mean": -3.9740944e-07,
            "Current_sigma": 6.500758483315618e-09
        },
        {
            "Time": 68.75816750526428,
            "Voltage": -35.0,
            "Current_mean": -3.9759062e-07,
            "Current_sigma": 5.324022160509846e-09
        },
        {
            "Time": 74.75315117835999,
            "Voltage": -40.0,
            "Current_mean": -3.9725667e-07,
            "Current_sigma": 6.4716250274641255e-09
        },
        {
            "Time": 80.74813508987427,
            "Voltage": -45.0,
            "Current_mean": -3.9842057e-07,
            "Current_sigma": 5.5783139230505784e-09
        },
        {
            "Time": 86.74330592155457,
            "Voltage": -50.0,
            "Current_mean": -4.0031975000000007e-07,
            "Current_sigma": 5.286878644767621e-09
        },
        {
            "Time": 92.73830938339233,
            "Voltage": -55.0,
            "Current_mean": -4.0228994e-07,
            "Current_sigma": 5.1756279509253785e-09
        },
        {
            "Time": 98.73334407806396,
            "Voltage": -60.0,
            "Current_mean": -4.0966730000000004e-07,
            "Current_sigma": 5.851228271739195e-09
        },
        {
            "Time": 104.72829985618591,
            "Voltage": -65.0,
            "Current_mean": -4.1841448e-07,
            "Current_sigma": 5.407909259002031e-09
        },
        {
            "Time": 110.7232084274292,
            "Voltage": -70.0,
            "Current_mean": -4.3045089e-07,
            "Current_sigma": 5.270680972597375e-09
        },
        {
            "Time": 116.71821117401123,
            "Voltage": -75.0,
            "Current_mean": -4.4656674e-07,
            "Current_sigma": 5.537630901604045e-09
        },
        {
            "Time": 122.71326422691345,
            "Voltage": -80.0,
            "Current_mean": -4.6453233e-07,
            "Current_sigma": 5.352941083002875e-09
        },
        {
            "Time": 128.70812249183655,
            "Voltage": -85.0,
            "Current_mean": -4.8800214e-07,
            "Current_sigma": 5.335312224265799e-09
        },
        {
            "Time": 134.70326972007751,
            "Voltage": -90.0,
            "Current_mean": -5.1425586e-07,
            "Current_sigma": 4.870280057080898e-09
        },
        {
            "Time": 140.69832944869995,
            "Voltage": -95.0,
            "Current_mean": -5.4177987e-07,
            "Current_sigma": 4.89620900085975e-09
        },
        {
            "Time": 146.69326305389404,
            "Voltage": -100.0,
            "Current_mean": -5.7404192e-07,
            "Current_sigma": 4.351877201576337e-09
        },
        {
            "Time": 152.68844747543335,
            "Voltage": -105.0,
            "Current_mean": -6.068841e-07,
            "Current_sigma": 4.996311842949761e-09
        },
        {
            "Time": 158.68319058418274,
            "Voltage": -110.0,
            "Current_mean": -6.422905700000001e-07,
            "Current_sigma": 5.03564136353056e-09
        },
        {
            "Time": 164.67839694023132,
            "Voltage": -115.0,
            "Current_mean": -6.7979234e-07,
            "Current_sigma": 4.968388366100223e-09
        },
        {
            "Time": 170.67317152023315,
            "Voltage": -120.0,
            "Current_mean": -7.1873572e-07,
            "Current_sigma": 4.750008840160183e-09
        },
        {
            "Time": 176.66820979118347,
            "Voltage": -125.0,
            "Current_mean": -7.5928941e-07,
            "Current_sigma": 3.913598956574354e-09
        },
        {
            "Time": 182.66317582130432,
            "Voltage": -130.0,
            "Current_mean": -7.9838751e-07,
            "Current_sigma": 4.564980517472121e-09
        },
        {
            "Time": 188.65820217132568,
            "Voltage": -135.0,
            "Current_mean": -8.372941699999999e-07,
            "Current_sigma": 4.741630162096153e-09
        },
        {
            "Time": 194.65318894386292,
            "Voltage": -140.0,
            "Current_mean": -8.788782299999999e-07,
            "Current_sigma": 4.837939157130835e-09
        },
        {
            "Time": 200.6481909751892,
            "Voltage": -145.0,
            "Current_mean": -9.1770952e-07,
            "Current_sigma": 4.85736749542383e-09
        },
        {
            "Time": 206.6433892250061,
            "Voltage": -150.0,
            "Current_mean": -9.5861751e-07,
            "Current_sigma": 4.040967725050525e-09
        },
        {
            "Time": 212.63843250274658,
            "Voltage": -155.0,
            "Current_mean": -1.0007725e-06,
            "Current_sigma": 4.547435460124746e-09
        },
        {
            "Time": 218.63330936431885,
            "Voltage": -160.0,
            "Current_mean": -1.045012e-06,
            "Current_sigma": 4.066780249779889e-09
        },
        {
            "Time": 224.62800407409668,
            "Voltage": -165.0,
            "Current_mean": -1.0906008000000001e-06,
            "Current_sigma": 4.876223903800956e-09
        },
        {
            "Time": 230.6228199005127,
            "Voltage": -170.0,
            "Current_mean": -1.1358565e-06,
            "Current_sigma": 3.247194766255922e-09
        },
        {
            "Time": 236.6177680492401,
            "Voltage": -175.0,
            "Current_mean": -1.1788972e-06,
            "Current_sigma": 4.886746070750955e-09
        },
        {
            "Time": 242.61280131340027,
            "Voltage": -180.0,
            "Current_mean": -1.2270333e-06,
            "Current_sigma": 5.28285544095234e-09
        },
        {
            "Time": 248.6078062057495,
            "Voltage": -185.0,
            "Current_mean": -1.2694177000000001e-06,
            "Current_sigma": 4.380756762250096e-09
        },
        {
            "Time": 254.60298705101013,
            "Voltage": -190.0,
            "Current_mean": -1.3152234000000003e-06,
            "Current_sigma": 3.886327474621763e-09
        },
        {
            "Time": 260.59803009033203,
            "Voltage": -195.0,
            "Current_mean": -1.3603128e-06,
            "Current_sigma": 3.001159969078656e-09
        },
        {
            "Time": 266.59302949905396,
            "Voltage": -200.0,
            "Current_mean": -1.4095393e-06,
            "Current_sigma": 5.077888794568063e-09
        }
    ]
}