{
    "unit": {
        "Time": "s",
        "Voltage": "V",
        "Current": "A"
    },
    "V_step": -5,
    "Step_duration": 2,
    "N_measurements_per_step": 10,
    "A_Compliance": 1e-05,
    "Sensor_IV": [
        {
            "Time": 25.480407238006592,
            "Voltage": 0.0,
            "Current_mean": 3.3516506e-08,
            "Current_sigma": 1.5198295618291676e-08
        },
        {
            "Time": 31.475305318832397,
            "Voltage": -5.0,
            "Current_mean": -2.1683404999999996e-07,
            "Current_sigma": 2.0678788531645177e-08
        },
        {
            "Time": 37.469966650009155,
            "Voltage": -10.0,
            "Current_mean": -2.3723262999999997e-07,
            "Current_sigma": 1.2261014953832325e-08
        },
        {
            "Time": 43.46472406387329,
            "Voltage": -15.0,
            "Current_mean": -2.5206647999999994e-07,
            "Current_sigma": 6.194473213244211e-09
        },
        {
            "Time": 49.45938038825989,
            "Voltage": -20.0,
            "Current_mean": -2.5248595000000003e-07,
            "Current_sigma": 6.463548571992017e-09
        },
        {
            "Time": 55.45405101776123,
            "Voltage": -25.0,
            "Current_mean": -2.5519867000000005e-07,
            "Current_sigma": 5.9567977216034356e-09
        },
        {
            "Time": 61.44874668121338,
            "Voltage": -30.0,
            "Current_mean": -2.5718179e-07,
            "Current_sigma": 6.40933944325778e-09
        },
        {
            "Time": 67.44350719451904,
            "Voltage": -35.0,
            "Current_mean": -2.5639415e-07,
            "Current_sigma": 5.72974511514255e-09
        },
        {
            "Time": 73.43854904174805,
            "Voltage": -40.0,
            "Current_mean": -2.5769533000000004e-07,
            "Current_sigma": 6.2954210709133705e-09
        },
        {
            "Time": 79.43293523788452,
            "Voltage": -45.0,
            "Current_mean": -2.5648440000000006e-07,
            "Current_sigma": 5.378129271410273e-09
        },
        {
            "Time": 85.4278633594513,
            "Voltage": -50.0,
            "Current_mean": -2.5784709e-07,
            "Current_sigma": 5.8786167712226325e-09
        },
        {
            "Time": 91.42240524291992,
            "Voltage": -55.0,
            "Current_mean": -2.5649951e-07,
            "Current_sigma": 5.103767585117873e-09
        },
        {
            "Time": 97.41719436645508,
            "Voltage": -60.0,
            "Current_mean": -2.6327721000000006e-07,
            "Current_sigma": 4.035623475858478e-09
        },
        {
            "Time": 103.41193151473999,
            "Voltage": -65.0,
            "Current_mean": -2.5350442000000003e-07,
            "Current_sigma": 8.647667313212251e-10
        },
        {
            "Time": 109.4068603515625,
            "Voltage": -70.0,
            "Current_mean": -2.6242159e-07,
            "Current_sigma": 1.5291606445694318e-09
        },
        {
            "Time": 115.40134024620056,
            "Voltage": -75.0,
            "Current_mean": -2.6781571999999996e-07,
            "Current_sigma": 9.417382680978784e-10
        },
        {
            "Time": 121.39614081382751,
            "Voltage": -80.0,
            "Current_mean": -2.8136733000000004e-07,
            "Current_sigma": 9.568631417815273e-10
        },
        {
            "Time": 127.39080142974854,
            "Voltage": -85.0,
            "Current_mean": -3.1273597999999996e-07,
            "Current_sigma": 9.587396036463706e-10
        },
        {
            "Time": 133.385436296463,
            "Voltage": -90.0,
            "Current_mean": -3.2307638000000006e-07,
            "Current_sigma": 3.181511210352711e-09
        },
        {
            "Time": 139.3802671432495,
            "Voltage": -95.0,
            "Current_mean": -3.5286250000000007e-07,
            "Current_sigma": 2.937297402715628e-09
        },
        {
            "Time": 145.37483048439026,
            "Voltage": -100.0,
            "Current_mean": -3.7011365e-07,
            "Current_sigma": 1.4020682274768244e-09
        },
        {
            "Time": 151.36981177330017,
            "Voltage": -105.0,
            "Current_mean": -4.0706158e-07,
            "Current_sigma": 2.702146055193904e-09
        },
        {
            "Time": 157.36415123939514,
            "Voltage": -110.0,
            "Current_mean": -4.4088091000000003e-07,
            "Current_sigma": 7.568342479697971e-10
        },
        {
            "Time": 163.35911417007446,
            "Voltage": -115.0,
            "Current_mean": -4.6939258000000004e-07,
            "Current_sigma": 3.3014224415545533e-09
        },
        {
            "Time": 169.35363578796387,
            "Voltage": -120.0,
            "Current_mean": -5.0209553e-07,
            "Current_sigma": 2.9925512697529704e-09
        },
        {
            "Time": 175.34845185279846,
            "Voltage": -125.0,
            "Current_mean": -5.438672100000001e-07,
            "Current_sigma": 2.5515122799822092e-09
        },
        {
            "Time": 181.34317231178284,
            "Voltage": -130.0,
            "Current_mean": -5.7713869e-07,
            "Current_sigma": 3.784061778420132e-09
        },
        {
            "Time": 187.33796954154968,
            "Voltage": -135.0,
            "Current_mean": -6.201515599999999e-07,
            "Current_sigma": 3.622455910621961e-09
        },
        {
            "Time": 193.3328561782837,
            "Voltage": -140.0,
            "Current_mean": -6.6332381e-07,
            "Current_sigma": 4.7845644241560905e-09
        },
        {
            "Time": 199.32747888565063,
            "Voltage": -145.0,
            "Current_mean": -1.04521396e-06,
            "Current_sigma": 3.660031001855584e-08
        },
        {
            "Time": 205.32241106033325,
            "Voltage": -150.0,
            "Current_mean": -8.3963917e-06,
            "Current_sigma": 1.2013286552891384e-06
        }
    ]
}