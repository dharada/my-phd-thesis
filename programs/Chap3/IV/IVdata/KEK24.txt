{
    "unit": {
        "Time": "s",
        "Voltage": "V",
        "Current": "A"
    },
    "V_step": -5,
    "Step_duration": 2,
    "N_measurements_per_step": 10,
    "A_Compliance": 1e-05,
    "Sensor_IV": [
        {
            "Time": 33.25012135505676,
            "Voltage": 0.0,
            "Current_mean": 5.1871602e-08,
            "Current_sigma": 1.1727260981787521e-08
        },
        {
            "Time": 39.24448752403259,
            "Voltage": -5.0,
            "Current_mean": -2.6740838e-07,
            "Current_sigma": 1.760356310687128e-08
        },
        {
            "Time": 45.23943495750427,
            "Voltage": -10.0,
            "Current_mean": -2.8006878e-07,
            "Current_sigma": 1.1306571528434255e-08
        },
        {
            "Time": 51.23381042480469,
            "Voltage": -15.0,
            "Current_mean": -3.008951e-07,
            "Current_sigma": 6.668828529659463e-09
        },
        {
            "Time": 57.22876954078674,
            "Voltage": -20.0,
            "Current_mean": -3.0491430000000003e-07,
            "Current_sigma": 6.47305276882554e-09
        },
        {
            "Time": 63.22328591346741,
            "Voltage": -25.0,
            "Current_mean": -3.0640843e-07,
            "Current_sigma": 6.47267297784308e-09
        },
        {
            "Time": 69.21816444396973,
            "Voltage": -30.0,
            "Current_mean": -3.09088e-07,
            "Current_sigma": 6.180061576877695e-09
        },
        {
            "Time": 75.21258759498596,
            "Voltage": -35.0,
            "Current_mean": -3.0991417e-07,
            "Current_sigma": 5.7807746515583925e-09
        },
        {
            "Time": 81.20736980438232,
            "Voltage": -40.0,
            "Current_mean": -3.0922985e-07,
            "Current_sigma": 6.652450779863012e-09
        },
        {
            "Time": 87.20187091827393,
            "Voltage": -45.0,
            "Current_mean": -3.1160169e-07,
            "Current_sigma": 5.217150812359174e-09
        },
        {
            "Time": 93.19648456573486,
            "Voltage": -50.0,
            "Current_mean": -3.1019182999999997e-07,
            "Current_sigma": 5.345910423127198e-09
        },
        {
            "Time": 99.19079732894897,
            "Voltage": -55.0,
            "Current_mean": -3.1345280000000004e-07,
            "Current_sigma": 2.770425409932555e-09
        },
        {
            "Time": 105.18538069725037,
            "Voltage": -60.0,
            "Current_mean": -3.1466884999999996e-07,
            "Current_sigma": 1.336039004108791e-09
        },
        {
            "Time": 111.17986226081848,
            "Voltage": -65.0,
            "Current_mean": -3.170746e-07,
            "Current_sigma": 2.9263050575768707e-09
        },
        {
            "Time": 117.17435479164124,
            "Voltage": -70.0,
            "Current_mean": -3.1672941e-07,
            "Current_sigma": 3.433695855328482e-09
        },
        {
            "Time": 123.16916394233704,
            "Voltage": -75.0,
            "Current_mean": -3.1149890000000004e-07,
            "Current_sigma": 4.7210104399376355e-09
        },
        {
            "Time": 129.16363763809204,
            "Voltage": -80.0,
            "Current_mean": -3.1329837e-07,
            "Current_sigma": 2.7621371454183827e-09
        },
        {
            "Time": 135.15839958190918,
            "Voltage": -85.0,
            "Current_mean": -3.1218191000000003e-07,
            "Current_sigma": 2.7650040207746526e-09
        },
        {
            "Time": 141.1527931690216,
            "Voltage": -90.0,
            "Current_mean": -3.1474159e-07,
            "Current_sigma": 2.369436005234156e-09
        },
        {
            "Time": 147.14751052856445,
            "Voltage": -95.0,
            "Current_mean": -3.2017888e-07,
            "Current_sigma": 8.953965409805814e-10
        },
        {
            "Time": 153.1420774459839,
            "Voltage": -100.0,
            "Current_mean": -3.1476828e-07,
            "Current_sigma": 3.69909327506079e-09
        },
        {
            "Time": 159.136714220047,
            "Voltage": -105.0,
            "Current_mean": -3.2448862e-07,
            "Current_sigma": 3.725532182064734e-09
        },
        {
            "Time": 165.13125848770142,
            "Voltage": -110.0,
            "Current_mean": -3.240403e-07,
            "Current_sigma": 4.3778347650408185e-09
        },
        {
            "Time": 171.12584972381592,
            "Voltage": -115.0,
            "Current_mean": -3.2952196e-07,
            "Current_sigma": 1.1841421495749535e-09
        },
        {
            "Time": 177.12056183815002,
            "Voltage": -120.0,
            "Current_mean": -3.3357177000000006e-07,
            "Current_sigma": 4.6393639102898625e-09
        },
        {
            "Time": 183.11511039733887,
            "Voltage": -125.0,
            "Current_mean": -3.3458056e-07,
            "Current_sigma": 3.51041108481614e-09
        },
        {
            "Time": 189.109769821167,
            "Voltage": -130.0,
            "Current_mean": -3.4002381e-07,
            "Current_sigma": 3.157563541862623e-09
        },
        {
            "Time": 195.1041202545166,
            "Voltage": -135.0,
            "Current_mean": -3.4765152e-07,
            "Current_sigma": 3.3875087813318013e-09
        },
        {
            "Time": 201.09889316558838,
            "Voltage": -140.0,
            "Current_mean": -3.5487239000000004e-07,
            "Current_sigma": 2.3128119013227224e-09
        },
        {
            "Time": 207.09337210655212,
            "Voltage": -145.0,
            "Current_mean": -3.6155271e-07,
            "Current_sigma": 2.524784937554085e-09
        },
        {
            "Time": 213.08819317817688,
            "Voltage": -150.0,
            "Current_mean": -3.6493352000000004e-07,
            "Current_sigma": 4.387417718613082e-09
        },
        {
            "Time": 219.0824909210205,
            "Voltage": -155.0,
            "Current_mean": -3.7024393000000003e-07,
            "Current_sigma": 1.7205214860907707e-09
        },
        {
            "Time": 225.07735776901245,
            "Voltage": -160.0,
            "Current_mean": -3.7192782e-07,
            "Current_sigma": 2.7632536632745087e-09
        },
        {
            "Time": 231.07207226753235,
            "Voltage": -165.0,
            "Current_mean": -3.7152650000000005e-07,
            "Current_sigma": 9.046594795833426e-10
        },
        {
            "Time": 237.06690430641174,
            "Voltage": -170.0,
            "Current_mean": -3.7726253e-07,
            "Current_sigma": 2.2092407759454384e-09
        },
        {
            "Time": 243.0613465309143,
            "Voltage": -175.0,
            "Current_mean": -3.8452633000000004e-07,
            "Current_sigma": 1.4161587121858906e-09
        },
        {
            "Time": 249.0558660030365,
            "Voltage": -180.0,
            "Current_mean": -3.8419242e-07,
            "Current_sigma": 3.43870829492704e-09
        },
        {
            "Time": 255.05062699317932,
            "Voltage": -185.0,
            "Current_mean": -3.8714532e-07,
            "Current_sigma": 1.8561179104787473e-09
        },
        {
            "Time": 261.04534101486206,
            "Voltage": -190.0,
            "Current_mean": -3.8963746e-07,
            "Current_sigma": 1.4777416324919584e-09
        },
        {
            "Time": 267.0402910709381,
            "Voltage": -195.0,
            "Current_mean": -3.9403607e-07,
            "Current_sigma": 1.1739785398805272e-09
        },
        {
            "Time": 273.0348150730133,
            "Voltage": -200.0,
            "Current_mean": -3.9726053e-07,
            "Current_sigma": 3.237024006722841e-09
        }
    ]
}