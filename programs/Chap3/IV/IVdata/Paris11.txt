{
    "unit": {
        "Time": "s",
        "Voltage": "V",
        "Current": "A"
    },
    "V_step": -5,
    "Step_duration": 2,
    "N_measurements_per_step": 10,
    "A_Compliance": 1e-05,
    "Sensor_IV": [
        {
            "Time": 41.4493408203125,
            "Voltage": 0.0,
            "Current_mean": 5.327270699999999e-08,
            "Current_sigma": 1.094314715305067e-08
        },
        {
            "Time": 47.444106578826904,
            "Voltage": -5.0,
            "Current_mean": -3.7315892e-07,
            "Current_sigma": 1.8442090134027647e-08
        },
        {
            "Time": 53.43839192390442,
            "Voltage": -10.0,
            "Current_mean": -4.1537353000000004e-07,
            "Current_sigma": 1.0150341381357577e-08
        },
        {
            "Time": 59.43304133415222,
            "Voltage": -15.0,
            "Current_mean": -4.4583386000000004e-07,
            "Current_sigma": 7.653287245125467e-09
        },
        {
            "Time": 65.42751550674438,
            "Voltage": -20.0,
            "Current_mean": -4.5225285000000006e-07,
            "Current_sigma": 6.160900697828202e-09
        },
        {
            "Time": 71.42218112945557,
            "Voltage": -25.0,
            "Current_mean": -4.5661953e-07,
            "Current_sigma": 5.966298514833123e-09
        },
        {
            "Time": 77.41662263870239,
            "Voltage": -30.0,
            "Current_mean": -4.5615259e-07,
            "Current_sigma": 4.9105341909918465e-09
        },
        {
            "Time": 83.4110631942749,
            "Voltage": -35.0,
            "Current_mean": -4.5765745e-07,
            "Current_sigma": 5.373616589085978e-09
        },
        {
            "Time": 89.40580296516418,
            "Voltage": -40.0,
            "Current_mean": -4.576812500000001e-07,
            "Current_sigma": 5.1825421291582395e-09
        },
        {
            "Time": 95.40012454986572,
            "Voltage": -45.0,
            "Current_mean": -4.5837062e-07,
            "Current_sigma": 5.680778077481997e-09
        },
        {
            "Time": 101.39490175247192,
            "Voltage": -50.0,
            "Current_mean": -4.5975194000000007e-07,
            "Current_sigma": 5.51623811708668e-09
        },
        {
            "Time": 107.38936996459961,
            "Voltage": -55.0,
            "Current_mean": -4.5804119999999995e-07,
            "Current_sigma": 3.616109645738084e-09
        },
        {
            "Time": 113.38397550582886,
            "Voltage": -60.0,
            "Current_mean": -4.5803460999999997e-07,
            "Current_sigma": 3.34911338250887e-09
        },
        {
            "Time": 119.3783826828003,
            "Voltage": -65.0,
            "Current_mean": -4.6191244e-07,
            "Current_sigma": 5.261087337651793e-09
        },
        {
            "Time": 125.3731210231781,
            "Voltage": -70.0,
            "Current_mean": -4.6897827999999996e-07,
            "Current_sigma": 3.0865086488782156e-09
        },
        {
            "Time": 131.36748886108398,
            "Voltage": -75.0,
            "Current_mean": -4.7044449e-07,
            "Current_sigma": 3.50998591349025e-09
        },
        {
            "Time": 137.36223673820496,
            "Voltage": -80.0,
            "Current_mean": -4.7371529999999993e-07,
            "Current_sigma": 4.533371695107297e-09
        },
        {
            "Time": 143.356538772583,
            "Voltage": -85.0,
            "Current_mean": -4.7924215e-07,
            "Current_sigma": 4.519481002117392e-09
        },
        {
            "Time": 149.35097908973694,
            "Voltage": -90.0,
            "Current_mean": -4.8274512e-07,
            "Current_sigma": 4.434655185197613e-09
        },
        {
            "Time": 155.3455033302307,
            "Voltage": -95.0,
            "Current_mean": -4.9194445e-07,
            "Current_sigma": 4.626316864256073e-09
        },
        {
            "Time": 161.3399634361267,
            "Voltage": -100.0,
            "Current_mean": -5.0608886e-07,
            "Current_sigma": 4.7691459267252475e-09
        },
        {
            "Time": 167.33455276489258,
            "Voltage": -105.0,
            "Current_mean": -5.258283599999999e-07,
            "Current_sigma": 3.5483701098391792e-09
        },
        {
            "Time": 173.32898998260498,
            "Voltage": -110.0,
            "Current_mean": -5.4876296e-07,
            "Current_sigma": 4.819109230386902e-09
        },
        {
            "Time": 179.32365822792053,
            "Voltage": -115.0,
            "Current_mean": -5.757052600000001e-07,
            "Current_sigma": 4.0757879523351075e-09
        },
        {
            "Time": 185.318106174469,
            "Voltage": -120.0,
            "Current_mean": -6.0274049e-07,
            "Current_sigma": 4.188750209656814e-09
        },
        {
            "Time": 191.3129267692566,
            "Voltage": -125.0,
            "Current_mean": -6.3702435e-07,
            "Current_sigma": 3.9309193762401325e-09
        },
        {
            "Time": 197.30710554122925,
            "Voltage": -130.0,
            "Current_mean": -6.6882676e-07,
            "Current_sigma": 4.175365096180229e-09
        },
        {
            "Time": 203.30173110961914,
            "Voltage": -135.0,
            "Current_mean": -7.000879e-07,
            "Current_sigma": 4.1472627707923155e-09
        },
        {
            "Time": 209.29617309570312,
            "Voltage": -140.0,
            "Current_mean": -7.3323238e-07,
            "Current_sigma": 4.106395187460644e-09
        },
        {
            "Time": 215.2906482219696,
            "Voltage": -145.0,
            "Current_mean": -7.650934100000001e-07,
            "Current_sigma": 4.937433857268358e-09
        },
        {
            "Time": 221.2851095199585,
            "Voltage": -150.0,
            "Current_mean": -8.040398599999999e-07,
            "Current_sigma": 3.828555559267748e-09
        },
        {
            "Time": 227.27955508232117,
            "Voltage": -155.0,
            "Current_mean": -8.3617659e-07,
            "Current_sigma": 4.099697883125062e-09
        },
        {
            "Time": 233.274245262146,
            "Voltage": -160.0,
            "Current_mean": -8.710893400000001e-07,
            "Current_sigma": 3.934457836907138e-09
        },
        {
            "Time": 239.26865887641907,
            "Voltage": -165.0,
            "Current_mean": -9.082429200000001e-07,
            "Current_sigma": 3.4710681729980673e-09
        },
        {
            "Time": 245.26309990882874,
            "Voltage": -170.0,
            "Current_mean": -9.4410533e-07,
            "Current_sigma": 4.397348222065211e-09
        },
        {
            "Time": 251.2572889328003,
            "Voltage": -175.0,
            "Current_mean": -9.816815599999999e-07,
            "Current_sigma": 3.509063164207782e-09
        },
        {
            "Time": 257.2516679763794,
            "Voltage": -180.0,
            "Current_mean": -1.0239267e-06,
            "Current_sigma": 5.026026682181425e-09
        },
        {
            "Time": 263.24599409103394,
            "Voltage": -185.0,
            "Current_mean": -1.0691307e-06,
            "Current_sigma": 2.874489417270531e-09
        },
        {
            "Time": 269.24060559272766,
            "Voltage": -190.0,
            "Current_mean": -1.1171224000000002e-06,
            "Current_sigma": 4.175937839575655e-09
        },
        {
            "Time": 275.2349569797516,
            "Voltage": -195.0,
            "Current_mean": -1.2830592e-06,
            "Current_sigma": 7.461935530919571e-08
        },
        {
            "Time": 281.2453467845917,
            "Voltage": -200.0,
            "Current_mean": -7.818075600000002e-06,
            "Current_sigma": 1.6727585002330252e-06
        }
    ]
}