{
    "unit": {
        "Time": "s",
        "Voltage": "V",
        "Current": "A"
    },
    "V_step": -5,
    "Step_duration": 2,
    "N_measurements_per_step": 10,
    "A_Compliance": 1e-05,
    "Sensor_IV": [
        {
            "Time": 30.660325527191162,
            "Voltage": 0.0,
            "Current_mean": 6.34826057e-09,
            "Current_sigma": 1.1570532968122687e-08
        },
        {
            "Time": 36.65500354766846,
            "Voltage": -5.0,
            "Current_mean": -4.7165571999999996e-07,
            "Current_sigma": 1.65532108995687e-08
        },
        {
            "Time": 42.64967203140259,
            "Voltage": -10.0,
            "Current_mean": -5.1846681e-07,
            "Current_sigma": 1.1585715543154842e-08
        },
        {
            "Time": 48.64425086975098,
            "Voltage": -15.0,
            "Current_mean": -5.4705937e-07,
            "Current_sigma": 6.706428848955303e-09
        },
        {
            "Time": 54.63945484161377,
            "Voltage": -20.0,
            "Current_mean": -5.5090577e-07,
            "Current_sigma": 6.223251325320237e-09
        },
        {
            "Time": 60.63401985168457,
            "Voltage": -25.0,
            "Current_mean": -5.521116700000001e-07,
            "Current_sigma": 6.566891249145224e-09
        },
        {
            "Time": 66.62906098365784,
            "Voltage": -30.0,
            "Current_mean": -5.5480885e-07,
            "Current_sigma": 6.5532788680552885e-09
        },
        {
            "Time": 72.62350368499756,
            "Voltage": -35.0,
            "Current_mean": -5.5644137e-07,
            "Current_sigma": 6.42248732860564e-09
        },
        {
            "Time": 78.6182861328125,
            "Voltage": -40.0,
            "Current_mean": -5.567888700000001e-07,
            "Current_sigma": 5.126062524599176e-09
        },
        {
            "Time": 84.613112449646,
            "Voltage": -45.0,
            "Current_mean": -5.5647647e-07,
            "Current_sigma": 5.491501999462454e-09
        },
        {
            "Time": 90.60771751403809,
            "Voltage": -50.0,
            "Current_mean": -5.5753835e-07,
            "Current_sigma": 5.229725815996476e-09
        },
        {
            "Time": 96.60247159004211,
            "Voltage": -55.0,
            "Current_mean": -5.565391e-07,
            "Current_sigma": 3.857460663182461e-09
        },
        {
            "Time": 102.59695434570312,
            "Voltage": -60.0,
            "Current_mean": -5.592003099999999e-07,
            "Current_sigma": 4.854370433835877e-09
        },
        {
            "Time": 108.59187150001526,
            "Voltage": -65.0,
            "Current_mean": -5.595336300000001e-07,
            "Current_sigma": 5.355378383466462e-09
        },
        {
            "Time": 114.58639597892761,
            "Voltage": -70.0,
            "Current_mean": -5.6455676e-07,
            "Current_sigma": 5.153557703024182e-09
        },
        {
            "Time": 120.58140134811401,
            "Voltage": -75.0,
            "Current_mean": -5.7430236e-07,
            "Current_sigma": 3.5111253734949302e-09
        },
        {
            "Time": 126.57595872879028,
            "Voltage": -80.0,
            "Current_mean": -6.154672999999999e-07,
            "Current_sigma": 5.5336111993886964e-09
        },
        {
            "Time": 132.57129549980164,
            "Voltage": -85.0,
            "Current_mean": -6.6989342e-07,
            "Current_sigma": 4.583673541123963e-09
        },
        {
            "Time": 138.56585502624512,
            "Voltage": -90.0,
            "Current_mean": -7.451196e-07,
            "Current_sigma": 4.957214076273077e-09
        },
        {
            "Time": 144.57679343223572,
            "Voltage": -95.0,
            "Current_mean": -8.3688727e-07,
            "Current_sigma": 4.9323361008856775e-09
        },
        {
            "Time": 150.5714430809021,
            "Voltage": -100.0,
            "Current_mean": -9.1982692e-07,
            "Current_sigma": 3.6612419548016704e-09
        },
        {
            "Time": 156.56617331504822,
            "Voltage": -105.0,
            "Current_mean": -1.00198226e-06,
            "Current_sigma": 5.246011372881309e-09
        },
        {
            "Time": 162.56118488311768,
            "Voltage": -110.0,
            "Current_mean": -1.0833818e-06,
            "Current_sigma": 4.773588310694553e-09
        },
        {
            "Time": 168.55602931976318,
            "Voltage": -115.0,
            "Current_mean": -1.1612925000000001e-06,
            "Current_sigma": 4.805559618816536e-09
        },
        {
            "Time": 174.55107045173645,
            "Voltage": -120.0,
            "Current_mean": -1.2358656e-06,
            "Current_sigma": 6.010939530555928e-09
        },
        {
            "Time": 180.54607462882996,
            "Voltage": -125.0,
            "Current_mean": -1.3132330000000003e-06,
            "Current_sigma": 5.073907586860441e-09
        },
        {
            "Time": 186.54155445098877,
            "Voltage": -130.0,
            "Current_mean": -1.3850156999999999e-06,
            "Current_sigma": 4.903491797688673e-09
        },
        {
            "Time": 192.5357632637024,
            "Voltage": -135.0,
            "Current_mean": -1.4568985e-06,
            "Current_sigma": 4.336030379275516e-09
        },
        {
            "Time": 198.53083658218384,
            "Voltage": -140.0,
            "Current_mean": -1.5306067e-06,
            "Current_sigma": 4.5609930508607465e-09
        },
        {
            "Time": 204.52557945251465,
            "Voltage": -145.0,
            "Current_mean": -1.6077691999999997e-06,
            "Current_sigma": 4.057740080389574e-09
        },
        {
            "Time": 210.52056431770325,
            "Voltage": -150.0,
            "Current_mean": -1.6787705000000003e-06,
            "Current_sigma": 3.5856064270915213e-09
        },
        {
            "Time": 216.515362739563,
            "Voltage": -155.0,
            "Current_mean": -1.7512829999999998e-06,
            "Current_sigma": 3.7446706397225214e-09
        },
        {
            "Time": 222.51060938835144,
            "Voltage": -160.0,
            "Current_mean": -1.8257337e-06,
            "Current_sigma": 4.878048996268915e-09
        },
        {
            "Time": 228.50521063804626,
            "Voltage": -165.0,
            "Current_mean": -1.8984544999999998e-06,
            "Current_sigma": 4.2691972840336226e-09
        },
        {
            "Time": 234.5000491142273,
            "Voltage": -170.0,
            "Current_mean": -1.9739936e-06,
            "Current_sigma": 3.853942999059528e-09
        },
        {
            "Time": 240.49498558044434,
            "Voltage": -175.0,
            "Current_mean": -2.0468486e-06,
            "Current_sigma": 4.398435521864596e-09
        },
        {
            "Time": 246.489839553833,
            "Voltage": -180.0,
            "Current_mean": -2.1215595e-06,
            "Current_sigma": 3.976826379162135e-09
        },
        {
            "Time": 252.48475670814514,
            "Voltage": -185.0,
            "Current_mean": -2.1979534000000003e-06,
            "Current_sigma": 3.5396770813169093e-09
        },
        {
            "Time": 258.4796667098999,
            "Voltage": -190.0,
            "Current_mean": -2.2724166999999996e-06,
            "Current_sigma": 3.06846277637512e-09
        },
        {
            "Time": 264.47448086738586,
            "Voltage": -195.0,
            "Current_mean": -2.3477649e-06,
            "Current_sigma": 3.80411930543723e-09
        },
        {
            "Time": 270.4690444469452,
            "Voltage": -200.0,
            "Current_mean": -2.4244001e-06,
            "Current_sigma": 3.698973194009416e-09
        }
    ]
}