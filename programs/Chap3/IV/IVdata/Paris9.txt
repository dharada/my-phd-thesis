{
    "unit": {
        "Time": "s",
        "Voltage": "V",
        "Current": "A"
    },
    "V_step": -5,
    "Step_duration": 2,
    "N_measurements_per_step": 10,
    "A_Compliance": 1e-05,
    "Sensor_IV": [
        {
            "Time": 33.275444984436035,
            "Voltage": 0.0,
            "Current_mean": 5.7958572e-08,
            "Current_sigma": 1.3656856633979723e-08
        },
        {
            "Time": 39.270912647247314,
            "Voltage": -5.0,
            "Current_mean": -4.4580906999999995e-07,
            "Current_sigma": 1.6954752242250555e-08
        },
        {
            "Time": 45.266395568847656,
            "Voltage": -10.0,
            "Current_mean": -5.118406299999999e-07,
            "Current_sigma": 1.0154960254678504e-08
        },
        {
            "Time": 51.26189088821411,
            "Voltage": -15.0,
            "Current_mean": -5.4696204e-07,
            "Current_sigma": 6.344288317408032e-09
        },
        {
            "Time": 57.25761532783508,
            "Voltage": -20.0,
            "Current_mean": -5.4775643e-07,
            "Current_sigma": 5.977781931460865e-09
        },
        {
            "Time": 63.253252029418945,
            "Voltage": -25.0,
            "Current_mean": -5.5201832e-07,
            "Current_sigma": 6.064318621708461e-09
        },
        {
            "Time": 69.24901723861694,
            "Voltage": -30.0,
            "Current_mean": -5.518300499999999e-07,
            "Current_sigma": 5.36402673581145e-09
        },
        {
            "Time": 75.24458503723145,
            "Voltage": -35.0,
            "Current_mean": -5.564705899999999e-07,
            "Current_sigma": 5.957401352511003e-09
        },
        {
            "Time": 81.24008107185364,
            "Voltage": -40.0,
            "Current_mean": -5.556468600000001e-07,
            "Current_sigma": 4.974334550108174e-09
        },
        {
            "Time": 87.23559498786926,
            "Voltage": -45.0,
            "Current_mean": -5.5685885e-07,
            "Current_sigma": 4.652540748720004e-09
        },
        {
            "Time": 93.23101615905762,
            "Voltage": -50.0,
            "Current_mean": -5.5574697e-07,
            "Current_sigma": 5.376989746326492e-09
        },
        {
            "Time": 99.22637367248535,
            "Voltage": -55.0,
            "Current_mean": -5.5806717e-07,
            "Current_sigma": 5.809981528722782e-09
        },
        {
            "Time": 105.22192215919495,
            "Voltage": -60.0,
            "Current_mean": -5.5880092e-07,
            "Current_sigma": 5.4311676517669765e-09
        },
        {
            "Time": 111.2174072265625,
            "Voltage": -65.0,
            "Current_mean": -5.5922975e-07,
            "Current_sigma": 5.082810509403237e-09
        },
        {
            "Time": 117.21294927597046,
            "Voltage": -70.0,
            "Current_mean": -5.694398300000001e-07,
            "Current_sigma": 4.651751399000164e-09
        },
        {
            "Time": 123.20848250389099,
            "Voltage": -75.0,
            "Current_mean": -5.8220248e-07,
            "Current_sigma": 4.9755859658536664e-09
        },
        {
            "Time": 129.20408248901367,
            "Voltage": -80.0,
            "Current_mean": -5.9315875e-07,
            "Current_sigma": 4.682876134866262e-09
        },
        {
            "Time": 135.19966077804565,
            "Voltage": -85.0,
            "Current_mean": -6.083515800000001e-07,
            "Current_sigma": 4.798188408097368e-09
        },
        {
            "Time": 141.1952006816864,
            "Voltage": -90.0,
            "Current_mean": -6.245512599999999e-07,
            "Current_sigma": 5.0372241125842305e-09
        },
        {
            "Time": 147.1907148361206,
            "Voltage": -95.0,
            "Current_mean": -6.396831500000001e-07,
            "Current_sigma": 4.1955503932737955e-09
        },
        {
            "Time": 153.1863977909088,
            "Voltage": -100.0,
            "Current_mean": -6.6076718e-07,
            "Current_sigma": 4.822724332739736e-09
        },
        {
            "Time": 159.18192839622498,
            "Voltage": -105.0,
            "Current_mean": -6.7969975e-07,
            "Current_sigma": 4.480818155035995e-09
        },
        {
            "Time": 165.1773874759674,
            "Voltage": -110.0,
            "Current_mean": -6.998266800000001e-07,
            "Current_sigma": 4.5426627741447045e-09
        },
        {
            "Time": 171.17301559448242,
            "Voltage": -115.0,
            "Current_mean": -7.201757400000001e-07,
            "Current_sigma": 4.758819594647401e-09
        },
        {
            "Time": 177.1685824394226,
            "Voltage": -120.0,
            "Current_mean": -7.4074525e-07,
            "Current_sigma": 4.324432129251191e-09
        },
        {
            "Time": 183.16411876678467,
            "Voltage": -125.0,
            "Current_mean": -7.6306298e-07,
            "Current_sigma": 4.406542801743781e-09
        },
        {
            "Time": 189.15968990325928,
            "Voltage": -130.0,
            "Current_mean": -7.9050732e-07,
            "Current_sigma": 4.549452469429698e-09
        },
        {
            "Time": 195.15512943267822,
            "Voltage": -135.0,
            "Current_mean": -8.1471038e-07,
            "Current_sigma": 4.334839701488393e-09
        },
        {
            "Time": 201.1506040096283,
            "Voltage": -140.0,
            "Current_mean": -8.3912836e-07,
            "Current_sigma": 4.319977851841376e-09
        },
        {
            "Time": 207.1458625793457,
            "Voltage": -145.0,
            "Current_mean": -8.6711923e-07,
            "Current_sigma": 4.8937727319625486e-09
        },
        {
            "Time": 213.1414008140564,
            "Voltage": -150.0,
            "Current_mean": -8.9492251e-07,
            "Current_sigma": 4.4269540430074435e-09
        },
        {
            "Time": 219.1369640827179,
            "Voltage": -155.0,
            "Current_mean": -9.223758299999999e-07,
            "Current_sigma": 3.905348589831632e-09
        },
        {
            "Time": 225.1323585510254,
            "Voltage": -160.0,
            "Current_mean": -9.526525199999999e-07,
            "Current_sigma": 3.9697274888334465e-09
        },
        {
            "Time": 231.12785816192627,
            "Voltage": -165.0,
            "Current_mean": -9.8146728e-07,
            "Current_sigma": 3.9854812005076856e-09
        },
        {
            "Time": 237.12321710586548,
            "Voltage": -170.0,
            "Current_mean": -1.0135819e-06,
            "Current_sigma": 4.113666915295856e-09
        },
        {
            "Time": 243.11880731582642,
            "Voltage": -175.0,
            "Current_mean": -1.0450750000000001e-06,
            "Current_sigma": 3.035030411709251e-09
        },
        {
            "Time": 249.11426830291748,
            "Voltage": -180.0,
            "Current_mean": -1.0728352000000001e-06,
            "Current_sigma": 3.953316830207294e-09
        },
        {
            "Time": 255.1098198890686,
            "Voltage": -185.0,
            "Current_mean": -1.1075099999999998e-06,
            "Current_sigma": 3.829784615353708e-09
        },
        {
            "Time": 261.1052827835083,
            "Voltage": -190.0,
            "Current_mean": -1.1449167999999998e-06,
            "Current_sigma": 3.2082671896212197e-09
        },
        {
            "Time": 267.1008150577545,
            "Voltage": -195.0,
            "Current_mean": -1.174605e-06,
            "Current_sigma": 4.103103727667642e-09
        },
        {
            "Time": 273.0965015888214,
            "Voltage": -200.0,
            "Current_mean": -1.2098110999999999e-06,
            "Current_sigma": 3.3601139995541918e-09
        }
    ]
}