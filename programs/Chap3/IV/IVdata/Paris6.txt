{
    "unit": {
        "Time": "s",
        "Voltage": "V",
        "Current": "A"
    },
    "V_step": -5,
    "Step_duration": 2,
    "N_measurements_per_step": 10,
    "A_Compliance": 1e-05,
    "Sensor_IV": [
        {
            "Time": 37.134933948516846,
            "Voltage": 0.0,
            "Current_mean": 1.44188292e-08,
            "Current_sigma": 1.5030154072822234e-08
        },
        {
            "Time": 43.12989377975464,
            "Voltage": -5.0,
            "Current_mean": -2.5081083e-07,
            "Current_sigma": 1.7312595263220932e-08
        },
        {
            "Time": 49.12465453147888,
            "Voltage": -10.0,
            "Current_mean": -2.7772601999999995e-07,
            "Current_sigma": 1.3078438005190076e-08
        },
        {
            "Time": 55.119532108306885,
            "Voltage": -15.0,
            "Current_mean": -2.9559902e-07,
            "Current_sigma": 8.795107751562795e-09
        },
        {
            "Time": 61.11422252655029,
            "Voltage": -20.0,
            "Current_mean": -3.04867e-07,
            "Current_sigma": 7.016708922279733e-09
        },
        {
            "Time": 67.10912346839905,
            "Voltage": -25.0,
            "Current_mean": -3.1018172e-07,
            "Current_sigma": 7.090069234330511e-09
        },
        {
            "Time": 73.10415410995483,
            "Voltage": -30.0,
            "Current_mean": -3.1109974e-07,
            "Current_sigma": 7.145602458323579e-09
        },
        {
            "Time": 79.09868478775024,
            "Voltage": -35.0,
            "Current_mean": -3.0962632e-07,
            "Current_sigma": 6.545073461589253e-09
        },
        {
            "Time": 85.09343767166138,
            "Voltage": -40.0,
            "Current_mean": -3.1039198000000005e-07,
            "Current_sigma": 6.190602713758977e-09
        },
        {
            "Time": 91.08822798728943,
            "Voltage": -45.0,
            "Current_mean": -3.1156573e-07,
            "Current_sigma": 5.920275658117617e-09
        },
        {
            "Time": 97.08310961723328,
            "Voltage": -50.0,
            "Current_mean": -3.1146496e-07,
            "Current_sigma": 5.305563710144283e-09
        },
        {
            "Time": 103.07792639732361,
            "Voltage": -55.0,
            "Current_mean": -3.1206016e-07,
            "Current_sigma": 5.368305519286319e-09
        },
        {
            "Time": 109.07293677330017,
            "Voltage": -60.0,
            "Current_mean": -3.1525584e-07,
            "Current_sigma": 5.1063821643508025e-09
        },
        {
            "Time": 115.06764388084412,
            "Voltage": -65.0,
            "Current_mean": -3.1580936e-07,
            "Current_sigma": 5.094187586887641e-09
        },
        {
            "Time": 121.06260824203491,
            "Voltage": -70.0,
            "Current_mean": -3.1202067e-07,
            "Current_sigma": 4.381432205352486e-09
        },
        {
            "Time": 127.05731010437012,
            "Voltage": -75.0,
            "Current_mean": -3.1605049e-07,
            "Current_sigma": 5.275342553133391e-09
        },
        {
            "Time": 133.05234479904175,
            "Voltage": -80.0,
            "Current_mean": -3.1430801e-07,
            "Current_sigma": 5.496821665007877e-09
        },
        {
            "Time": 139.0468554496765,
            "Voltage": -85.0,
            "Current_mean": -3.1541164e-07,
            "Current_sigma": 5.255744796734333e-09
        },
        {
            "Time": 145.04154324531555,
            "Voltage": -90.0,
            "Current_mean": -3.1788091000000006e-07,
            "Current_sigma": 4.865922739717512e-09
        },
        {
            "Time": 151.03645086288452,
            "Voltage": -95.0,
            "Current_mean": -3.1901798000000004e-07,
            "Current_sigma": 4.420351020179284e-09
        },
        {
            "Time": 157.03107380867004,
            "Voltage": -100.0,
            "Current_mean": -3.1547480000000005e-07,
            "Current_sigma": 3.1326208244854665e-09
        },
        {
            "Time": 163.0260283946991,
            "Voltage": -105.0,
            "Current_mean": -3.2181316e-07,
            "Current_sigma": 4.583673336135555e-09
        },
        {
            "Time": 169.02083539962769,
            "Voltage": -110.0,
            "Current_mean": -3.2086552e-07,
            "Current_sigma": 4.612674242519198e-09
        },
        {
            "Time": 175.01582431793213,
            "Voltage": -115.0,
            "Current_mean": -3.2252491999999996e-07,
            "Current_sigma": 5.077436691047956e-09
        },
        {
            "Time": 181.01055645942688,
            "Voltage": -120.0,
            "Current_mean": -3.2354289e-07,
            "Current_sigma": 4.784393964850729e-09
        },
        {
            "Time": 187.00576877593994,
            "Voltage": -125.0,
            "Current_mean": -3.2428089e-07,
            "Current_sigma": 4.757868417568937e-09
        },
        {
            "Time": 193.0006103515625,
            "Voltage": -130.0,
            "Current_mean": -3.2588145999999995e-07,
            "Current_sigma": 4.371495719361966e-09
        },
        {
            "Time": 198.9954493045807,
            "Voltage": -135.0,
            "Current_mean": -3.2778055e-07,
            "Current_sigma": 4.336917204708885e-09
        },
        {
            "Time": 204.98999524116516,
            "Voltage": -140.0,
            "Current_mean": -3.2759111e-07,
            "Current_sigma": 4.149137402750119e-09
        },
        {
            "Time": 210.98471689224243,
            "Voltage": -145.0,
            "Current_mean": -3.3113575e-07,
            "Current_sigma": 4.001376732888323e-09
        },
        {
            "Time": 216.97942543029785,
            "Voltage": -150.0,
            "Current_mean": -3.3091407e-07,
            "Current_sigma": 4.823163583178582e-09
        },
        {
            "Time": 222.97418642044067,
            "Voltage": -155.0,
            "Current_mean": -3.3268731999999996e-07,
            "Current_sigma": 4.03134588935259e-09
        },
        {
            "Time": 228.96908116340637,
            "Voltage": -160.0,
            "Current_mean": -3.3506256e-07,
            "Current_sigma": 3.783176605235343e-09
        },
        {
            "Time": 234.96391606330872,
            "Voltage": -165.0,
            "Current_mean": -3.3691274e-07,
            "Current_sigma": 3.8603654060723355e-09
        },
        {
            "Time": 240.95882511138916,
            "Voltage": -170.0,
            "Current_mean": -3.3895997e-07,
            "Current_sigma": 3.5375570081766917e-09
        },
        {
            "Time": 246.9534387588501,
            "Voltage": -175.0,
            "Current_mean": -3.4011438999999993e-07,
            "Current_sigma": 3.5735359434179464e-09
        },
        {
            "Time": 252.94864463806152,
            "Voltage": -180.0,
            "Current_mean": -3.4246589e-07,
            "Current_sigma": 3.5509857010272415e-09
        },
        {
            "Time": 258.9434106349945,
            "Voltage": -185.0,
            "Current_mean": -3.4349400999999995e-07,
            "Current_sigma": 4.122887323575561e-09
        },
        {
            "Time": 264.93839383125305,
            "Voltage": -190.0,
            "Current_mean": -3.4330532e-07,
            "Current_sigma": 3.423379359288132e-09
        },
        {
            "Time": 270.93296456336975,
            "Voltage": -195.0,
            "Current_mean": -3.4789012e-07,
            "Current_sigma": 3.2944596275565358e-09
        },
        {
            "Time": 276.92760181427,
            "Voltage": -200.0,
            "Current_mean": -3.482368699999999e-07,
            "Current_sigma": 3.173992852559685e-09
        }
    ]
}