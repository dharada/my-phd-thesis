{
    "unit": {
        "Time": "s",
        "Voltage": "V",
        "Current": "A"
    },
    "V_step": -5,
    "Step_duration": 2,
    "N_measurements_per_step": 10,
    "A_Compliance": 1e-05,
    "Sensor_IV": [
        {
            "Time": 20.308440923690796,
            "Voltage": 0.0,
            "Current_mean": 3.5793598e-08,
            "Current_sigma": 1.2442669139691692e-08
        },
        {
            "Time": 26.305173873901367,
            "Voltage": -5.0,
            "Current_mean": -3.6605311000000003e-07,
            "Current_sigma": 2.0474566047437978e-08
        },
        {
            "Time": 32.30199384689331,
            "Voltage": -10.0,
            "Current_mean": -4.2628253e-07,
            "Current_sigma": 8.213089586148443e-09
        },
        {
            "Time": 38.29879093170166,
            "Voltage": -15.0,
            "Current_mean": -4.306828e-07,
            "Current_sigma": 7.221788938344847e-09
        },
        {
            "Time": 44.29553484916687,
            "Voltage": -20.0,
            "Current_mean": -4.3609445000000003e-07,
            "Current_sigma": 7.412415255940544e-09
        },
        {
            "Time": 50.29225277900696,
            "Voltage": -25.0,
            "Current_mean": -4.3671609999999994e-07,
            "Current_sigma": 8.45197998802647e-09
        },
        {
            "Time": 56.289024353027344,
            "Voltage": -30.0,
            "Current_mean": -4.3970753000000005e-07,
            "Current_sigma": 6.583197006933641e-09
        },
        {
            "Time": 62.28570795059204,
            "Voltage": -35.0,
            "Current_mean": -4.3842008e-07,
            "Current_sigma": 6.9065448029821754e-09
        },
        {
            "Time": 68.28265690803528,
            "Voltage": -40.0,
            "Current_mean": -4.390189200000001e-07,
            "Current_sigma": 6.745426049375976e-09
        },
        {
            "Time": 74.27960538864136,
            "Voltage": -45.0,
            "Current_mean": -4.4128633e-07,
            "Current_sigma": 6.062431928203396e-09
        },
        {
            "Time": 80.27640223503113,
            "Voltage": -50.0,
            "Current_mean": -4.4209392000000004e-07,
            "Current_sigma": 4.338275196619042e-09
        },
        {
            "Time": 86.27317070960999,
            "Voltage": -55.0,
            "Current_mean": -4.4101059e-07,
            "Current_sigma": 5.952523736777535e-09
        },
        {
            "Time": 92.26995182037354,
            "Voltage": -60.0,
            "Current_mean": -4.4198430999999996e-07,
            "Current_sigma": 5.975594914391368e-09
        },
        {
            "Time": 98.26676559448242,
            "Voltage": -65.0,
            "Current_mean": -4.4285972e-07,
            "Current_sigma": 4.516828401832415e-09
        },
        {
            "Time": 104.26345658302307,
            "Voltage": -70.0,
            "Current_mean": -4.4565327000000003e-07,
            "Current_sigma": 5.440912200550561e-09
        },
        {
            "Time": 110.26027655601501,
            "Voltage": -75.0,
            "Current_mean": -4.5392267000000003e-07,
            "Current_sigma": 5.491589267607321e-09
        },
        {
            "Time": 116.25706028938293,
            "Voltage": -80.0,
            "Current_mean": -4.6144089999999996e-07,
            "Current_sigma": 5.402710249124964e-09
        },
        {
            "Time": 122.25383853912354,
            "Voltage": -85.0,
            "Current_mean": -4.7413210000000007e-07,
            "Current_sigma": 5.436092128910264e-09
        },
        {
            "Time": 128.25049757957458,
            "Voltage": -90.0,
            "Current_mean": -4.915122000000001e-07,
            "Current_sigma": 5.055637806251565e-09
        },
        {
            "Time": 134.24728798866272,
            "Voltage": -95.0,
            "Current_mean": -5.1448401e-07,
            "Current_sigma": 4.9205450068970894e-09
        },
        {
            "Time": 140.2439522743225,
            "Voltage": -100.0,
            "Current_mean": -5.489499699999999e-07,
            "Current_sigma": 5.359120472064411e-09
        },
        {
            "Time": 146.24074745178223,
            "Voltage": -105.0,
            "Current_mean": -5.8812279e-07,
            "Current_sigma": 4.37677512295298e-09
        },
        {
            "Time": 152.23755383491516,
            "Voltage": -110.0,
            "Current_mean": -6.2685423e-07,
            "Current_sigma": 5.267633599833998e-09
        },
        {
            "Time": 158.234233379364,
            "Voltage": -115.0,
            "Current_mean": -6.6934687e-07,
            "Current_sigma": 5.047421532238031e-09
        },
        {
            "Time": 164.23089814186096,
            "Voltage": -120.0,
            "Current_mean": -7.1539883e-07,
            "Current_sigma": 5.00076140463628e-09
        },
        {
            "Time": 170.22769904136658,
            "Voltage": -125.0,
            "Current_mean": -7.617870199999999e-07,
            "Current_sigma": 4.784455343881892e-09
        },
        {
            "Time": 176.2244963645935,
            "Voltage": -130.0,
            "Current_mean": -8.1117265e-07,
            "Current_sigma": 4.2650393433707e-09
        },
        {
            "Time": 182.22120141983032,
            "Voltage": -135.0,
            "Current_mean": -8.593974499999999e-07,
            "Current_sigma": 3.5266349440365892e-09
        },
        {
            "Time": 188.21791315078735,
            "Voltage": -140.0,
            "Current_mean": -9.1547736e-07,
            "Current_sigma": 4.333868041415184e-09
        },
        {
            "Time": 194.21468877792358,
            "Voltage": -145.0,
            "Current_mean": -9.733641599999999e-07,
            "Current_sigma": 4.726602403672238e-09
        },
        {
            "Time": 200.21158480644226,
            "Voltage": -150.0,
            "Current_mean": -1.0354063999999998e-06,
            "Current_sigma": 4.923338403969417e-09
        },
        {
            "Time": 206.20842933654785,
            "Voltage": -155.0,
            "Current_mean": -1.0973486000000002e-06,
            "Current_sigma": 4.592585441774602e-09
        },
        {
            "Time": 212.20505380630493,
            "Voltage": -160.0,
            "Current_mean": -1.1661664e-06,
            "Current_sigma": 3.143680238192155e-09
        },
        {
            "Time": 218.20187783241272,
            "Voltage": -165.0,
            "Current_mean": -1.2318436e-06,
            "Current_sigma": 4.205918216037945e-09
        },
        {
            "Time": 224.1986484527588,
            "Voltage": -170.0,
            "Current_mean": -1.303364e-06,
            "Current_sigma": 3.879319218625821e-09
        },
        {
            "Time": 230.19550466537476,
            "Voltage": -175.0,
            "Current_mean": -1.3739758000000002e-06,
            "Current_sigma": 4.714150396412887e-09
        },
        {
            "Time": 236.192280292511,
            "Voltage": -180.0,
            "Current_mean": -1.4474517999999998e-06,
            "Current_sigma": 4.182734077131838e-09
        },
        {
            "Time": 242.1890218257904,
            "Voltage": -185.0,
            "Current_mean": -1.5192527000000002e-06,
            "Current_sigma": 3.799734992075094e-09
        },
        {
            "Time": 248.18571877479553,
            "Voltage": -190.0,
            "Current_mean": -1.5923745999999998e-06,
            "Current_sigma": 3.44344755150998e-09
        },
        {
            "Time": 254.18241024017334,
            "Voltage": -195.0,
            "Current_mean": -1.6672262000000003e-06,
            "Current_sigma": 4.475829214793619e-09
        },
        {
            "Time": 260.1790635585785,
            "Voltage": -200.0,
            "Current_mean": -1.7437049999999999e-06,
            "Current_sigma": 3.841757514471718e-09
        }
    ]
}