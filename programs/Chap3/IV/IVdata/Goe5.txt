{
    "unit": {
        "Time": "s",
        "Voltage": "V",
        "Current": "A"
    },
    "V_step": -5,
    "Step_duration": 2,
    "N_measurements_per_step": 10,
    "A_Compliance": 1e-05,
    "Sensor_IV": [
        {
            "Time": 33.25161385536194,
            "Voltage": 0.0,
            "Current_mean": 9.609478100000002e-09,
            "Current_sigma": 1.4610461442736395e-08
        },
        {
            "Time": 39.24650025367737,
            "Voltage": -5.0,
            "Current_mean": -5.848606400000001e-07,
            "Current_sigma": 1.5525197876755067e-08
        },
        {
            "Time": 45.2414493560791,
            "Voltage": -10.0,
            "Current_mean": -6.902907100000001e-07,
            "Current_sigma": 1.0989095350523623e-08
        },
        {
            "Time": 51.236119508743286,
            "Voltage": -15.0,
            "Current_mean": -7.3284206e-07,
            "Current_sigma": 7.422470200708123e-09
        },
        {
            "Time": 57.231178522109985,
            "Voltage": -20.0,
            "Current_mean": -7.4493798e-07,
            "Current_sigma": 5.742675279310167e-09
        },
        {
            "Time": 63.22624945640564,
            "Voltage": -25.0,
            "Current_mean": -7.4558188e-07,
            "Current_sigma": 5.810502381859928e-09
        },
        {
            "Time": 69.22128891944885,
            "Voltage": -30.0,
            "Current_mean": -7.470357600000001e-07,
            "Current_sigma": 5.378405552986875e-09
        },
        {
            "Time": 75.21618628501892,
            "Voltage": -35.0,
            "Current_mean": -7.520354700000001e-07,
            "Current_sigma": 7.257889309165582e-09
        },
        {
            "Time": 81.21104550361633,
            "Voltage": -40.0,
            "Current_mean": -7.5914809e-07,
            "Current_sigma": 6.7811992857384784e-09
        },
        {
            "Time": 87.20588850975037,
            "Voltage": -45.0,
            "Current_mean": -7.662687599999999e-07,
            "Current_sigma": 5.907413903088221e-09
        },
        {
            "Time": 93.2009060382843,
            "Voltage": -50.0,
            "Current_mean": -7.6740226e-07,
            "Current_sigma": 5.012430764050504e-09
        },
        {
            "Time": 99.19595336914062,
            "Voltage": -55.0,
            "Current_mean": -7.7163831e-07,
            "Current_sigma": 5.564901996342769e-09
        },
        {
            "Time": 105.2069571018219,
            "Voltage": -60.0,
            "Current_mean": -7.7659661e-07,
            "Current_sigma": 5.406495963089227e-09
        },
        {
            "Time": 111.20187497138977,
            "Voltage": -65.0,
            "Current_mean": -7.767263899999999e-07,
            "Current_sigma": 5.576243207832671e-09
        },
        {
            "Time": 117.19678997993469,
            "Voltage": -70.0,
            "Current_mean": -7.7777946e-07,
            "Current_sigma": 5.260265893127448e-09
        },
        {
            "Time": 123.19183039665222,
            "Voltage": -75.0,
            "Current_mean": -7.833531999999999e-07,
            "Current_sigma": 4.914679425150741e-09
        },
        {
            "Time": 129.1868281364441,
            "Voltage": -80.0,
            "Current_mean": -7.819241799999999e-07,
            "Current_sigma": 4.996122293699374e-09
        },
        {
            "Time": 135.18179321289062,
            "Voltage": -85.0,
            "Current_mean": -7.865157899999999e-07,
            "Current_sigma": 5.094126207790695e-09
        },
        {
            "Time": 141.17674255371094,
            "Voltage": -90.0,
            "Current_mean": -7.925791899999999e-07,
            "Current_sigma": 5.425991147882567e-09
        },
        {
            "Time": 147.17166328430176,
            "Voltage": -95.0,
            "Current_mean": -8.0439822e-07,
            "Current_sigma": 5.2882606122996516e-09
        },
        {
            "Time": 153.1665723323822,
            "Voltage": -100.0,
            "Current_mean": -8.082617500000001e-07,
            "Current_sigma": 5.065578767771753e-09
        },
        {
            "Time": 159.16149592399597,
            "Voltage": -105.0,
            "Current_mean": -8.179888999999999e-07,
            "Current_sigma": 4.819903422061499e-09
        },
        {
            "Time": 165.15627455711365,
            "Voltage": -110.0,
            "Current_mean": -8.2266754e-07,
            "Current_sigma": 4.277598008275194e-09
        },
        {
            "Time": 171.15115356445312,
            "Voltage": -115.0,
            "Current_mean": -8.312528000000001e-07,
            "Current_sigma": 4.453140364057715e-09
        },
        {
            "Time": 177.14611983299255,
            "Voltage": -120.0,
            "Current_mean": -8.3994189e-07,
            "Current_sigma": 3.85583235280008e-09
        },
        {
            "Time": 183.1410412788391,
            "Voltage": -125.0,
            "Current_mean": -8.4459996e-07,
            "Current_sigma": 4.506046865091408e-09
        },
        {
            "Time": 189.13582181930542,
            "Voltage": -130.0,
            "Current_mean": -8.5292969e-07,
            "Current_sigma": 4.6311517054508065e-09
        },
        {
            "Time": 195.13076543807983,
            "Voltage": -135.0,
            "Current_mean": -8.6104557e-07,
            "Current_sigma": 4.543711892946112e-09
        },
        {
            "Time": 201.12576246261597,
            "Voltage": -140.0,
            "Current_mean": -8.7064895e-07,
            "Current_sigma": 4.111415768868428e-09
        },
        {
            "Time": 207.12076902389526,
            "Voltage": -145.0,
            "Current_mean": -8.803935799999999e-07,
            "Current_sigma": 4.628411868189797e-09
        },
        {
            "Time": 213.1157476902008,
            "Voltage": -150.0,
            "Current_mean": -8.905768599999999e-07,
            "Current_sigma": 4.307232011675258e-09
        },
        {
            "Time": 219.11065673828125,
            "Voltage": -155.0,
            "Current_mean": -9.0146208e-07,
            "Current_sigma": 4.119753331887731e-09
        },
        {
            "Time": 225.10546588897705,
            "Voltage": -160.0,
            "Current_mean": -9.135405399999999e-07,
            "Current_sigma": 4.363402373652908e-09
        },
        {
            "Time": 231.1005699634552,
            "Voltage": -165.0,
            "Current_mean": -9.251906e-07,
            "Current_sigma": 3.987695072344417e-09
        },
        {
            "Time": 237.09570264816284,
            "Voltage": -170.0,
            "Current_mean": -9.393351599999999e-07,
            "Current_sigma": 4.36378606102546e-09
        },
        {
            "Time": 243.09065747261047,
            "Voltage": -175.0,
            "Current_mean": -9.5516338e-07,
            "Current_sigma": 4.134024480043631e-09
        },
        {
            "Time": 249.0856695175171,
            "Voltage": -180.0,
            "Current_mean": -9.7095378e-07,
            "Current_sigma": 3.723314873818772e-09
        },
        {
            "Time": 255.08076930046082,
            "Voltage": -185.0,
            "Current_mean": -9.897075200000002e-07,
            "Current_sigma": 3.0451682806702296e-09
        },
        {
            "Time": 261.07577085494995,
            "Voltage": -190.0,
            "Current_mean": -1.0088158000000001e-06,
            "Current_sigma": 3.2072396792257436e-09
        },
        {
            "Time": 267.0706124305725,
            "Voltage": -195.0,
            "Current_mean": -1.0288029e-06,
            "Current_sigma": 4.078597147304459e-09
        },
        {
            "Time": 273.0656797885895,
            "Voltage": -200.0,
            "Current_mean": -1.0515413999999998e-06,
            "Current_sigma": 3.5321262774708273e-09
        }
    ]
}