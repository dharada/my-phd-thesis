#include <iostream>
#include <fstream>
#include <nlohmann/json.hpp>
#include <tuple>
#include <math.h>

#include <TROOT.h>
#include <TGraph.h>
#include <TGraphErrors.h>
#include <TH1F.h>
#include <THStack.h>
#include <TStyle.h>
#include <TLegend.h>
#include <TCanvas.h>
#include <TGaxis.h>
#include <TAxis.h>

using namespace std;
using json = nlohmann::json;

class thisModule{
public:
  string moduleName;
  double tempSingle;
  double tempIHR;
  double ratio(){
    double Eg0=1.17;
    double alpha=4.73*0.0001;
    double beta=636;
    double kB=8.617 * pow(10,-5);
    double T1=tempSingle+273.2;
    double T2=tempIHR+273.2;
    cout<<moduleName<<": "<<(T2/T1)*(T2/T1)*exp((-(1.17-(4.73*0.0001*T2*T2)/(T2+636))/(2*kB))*(1./T2)-(1.17-(4.73*0.0001*T1*T1)/(T1+636))/(2*kB)*(-1./T1))<<endl;
    return (T2/T1)*(T2/T1)*exp((-(1.17-(4.73*0.0001*T2*T2)/(T2+636))/(2*kB))*(1./T2)-(1.17-(4.73*0.0001*T1*T1)/(T1+636))/(2*kB)*(-1./T1));
  }
};


int main(){
  TCanvas *c1 = new TCanvas("c1", "", 1200, 800);

  TGraphErrors *gr[2];
  thisModule module[2];
  ifstream ifs("Paris18_iv_cellloading.json");
  json jsonFile[2];
  ifs >> jsonFile[0];

  ifstream ifs2("Goe4_iv_cellloading.json");
  ifs2 >> jsonFile[1];

  module[0].moduleName="Paris18";
  module[0].tempSingle=23.5;
  module[0].tempIHR=20;

  module[0].moduleName="Goe4";
  module[0].tempSingle=23.0;
  module[0].tempIHR=20;
  double volt[2][31];
  double curr[2][31];
  double eVolt[2][31];
  double eCurr[2][31];
  
  for(int jj=0;jj<31;jj++){
    double voltage = jsonFile[0]["Sensor_IV"][jj]["Voltage"];
    double current = jsonFile[0]["Sensor_IV"][jj]["Current_mean"];
    double eCurrent = jsonFile[0]["Sensor_IV"][jj]["Current_sigma"];
    voltage = abs(voltage);
    current = abs(current);
    current = current*module[0].ratio();
    cout<<voltage<<" "<<current<<endl;
    volt[0][jj]=voltage;
    curr[0][jj]=current;
    eVolt[0][jj]=0;
    eCurr[0][jj]=eCurrent;
  }


  for(int jj=0;jj<31;jj++){
    double voltage = jsonFile[1]["Sensor_IV"][jj]["Voltage"];
    double current = jsonFile[1]["Sensor_IV"][jj]["Current_mean"];
    double eCurrent = jsonFile[1]["Sensor_IV"][jj]["Current_sigma"];
    voltage = abs(voltage);
    current = abs(current);
    current = current*module[1].ratio();
    cout<<voltage<<" "<<current<<endl;
    volt[1][jj]=voltage;
    curr[1][jj]=current;
    eVolt[1][jj]=0;
    eCurr[1][jj]=eCurrent;
  }

  gr[0]=new TGraphErrors(31, volt[0], curr[0], eVolt[0], eCurr[0]);
  gr[1]=new TGraphErrors(31, volt[1], curr[1], eVolt[1], eCurr[1]);
  gPad->SetGrid(1, 1); gPad->Update();
  
  gr[0]->SetMarkerStyle(8);
  gr[1]->SetMarkerStyle(21);

  for(int ii=0;ii<2;ii++){
    gr[ii]->SetLineWidth(3);
    gr[ii]->SetTitle("");
    gr[ii]->GetXaxis()->SetTitle("Bias voltage [V]");
    gr[ii]->GetYaxis()->SetTitle("Leakage current [A]");
    gr[ii]->SetMarkerSize(2);
  }

  gr[1]->SetMarkerColor(2);
  gr[1]->SetLineColor(2);
  gr[0]->Draw("apl");
  gr[1]->Draw("plsame");

  TLegend* leg_ch1 = new TLegend(0.15,0.6,0.35,0.75);
  leg_ch1->AddEntry(gr[0],"Paris18","pl");
  leg_ch1->AddEntry(gr[1],"Goettingen4","pl");
  leg_ch1->Draw();

  c1->SaveAs("Paris18_IV.pdf");
}
