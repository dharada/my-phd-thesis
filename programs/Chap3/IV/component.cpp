#include <iostream>
#include <fstream>
#include <nlohmann/json.hpp>
#include <tuple>
#include <math.h>

#include <TROOT.h>
#include <TGraph.h>
#include <TH1F.h>
#include <THStack.h>
#include <TStyle.h>
#include <TLegend.h>
#include <TCanvas.h>
#include <TGaxis.h>
#include <TAxis.h>

using namespace std;
using json = nlohmann::json;

class thisModule{
public:
  string moduleName;
  double tempSingle;
  double tempIHR;
  double ratio(){
    double Eg0=1.17;
    double alpha=4.73*0.0001;
    double beta=636;
    double kB=8.617 * pow(10,-5);
    double T1=tempSingle+273.2;
    double T2=tempIHR+273.2;
    cout<<moduleName<<": "<<(T2/T1)*(T2/T1)*exp((-(1.17-(4.73*0.0001*T2*T2)/(T2+636))/(2*kB))*(1./T2)-(1.17-(4.73*0.0001*T1*T1)/(T1+636))/(2*kB)*(-1./T1))<<endl;
    return (T2/T1)*(T2/T1)*exp((-(1.17-(4.73*0.0001*T2*T2)/(T2+636))/(2*kB))*(1./T2)-(1.17-(4.73*0.0001*T1*T1)/(T1+636))/(2*kB)*(-1./T1));
  }
};

int main(){

  TGraph *each_ch1[6];
  TGraph *each_ch2[5];
  double ch1_volt[6][23];
  double ch2_volt[6][23];
  double ch1_curr[6][23];
  double ch2_curr[6][23];
  
  TCanvas *c1 = new TCanvas("c1", "", 1200, 800);
  
  double data_ch1[11][2]={
    {10,2.15E-6},
    {20,2.6E-6},
    {30,2.91E-6},
    {40,3.21E-6},
    {50,3.49E-6},
    {60,3.95E-6},
    {70,4.33E-6},
    {80,4.8E-6},
    {90,5.28E-6},
    {100,5.85E-6},
    {110,6.42E-6}
  };
  
  double data_ch2[11][2]={
    {10,1.28E-6},
    {20,1.83E-6},
    {30,1.95E-6},
    {40,2.03E-6},
    {50,2.08E-6},
    {60,2.11E-6},
    {70,2.15E-6},
    {80,2.2E-6},
    {90,2.28E-6},
    {100,2.4E-6},
    {110,2.51E-6}
  };

  thisModule ch1[6];
  thisModule ch2[5];
  
  double xaxis_ch1[11];
  double yaxis_ch1[11];

  double xaxis_ch2[11];
  double yaxis_ch2[11];
  THStack *hs_ch1 = new THStack("", "");
  THStack *hs_ch2 = new THStack("", "");
  TH1F* hist_ch1[6];
  TH1F* hist_ch2[5];
  for(int ii=0;ii<6;ii++){
    hist_ch1[ii]= new TH1F("","",24, 107.5, -2.5);
    //    hist_ch1[ii]->Sumw2();
  }
  for(int ii=0;ii<5;ii++){
    hist_ch2[ii]= new TH1F("","",24, 107.5, -2.5);
    //    hist_ch2[ii]->Sumw2();
  }
  string modules_ch1[1]={"Paris18"};
  double tempSingle_ch1[1]={23.5};
  double tempIHR_ch1[6]={20};
  for(int ii=0;ii<1;ii++){
    ch1[ii].moduleName=modules_ch1[ii];
    ch1[ii].tempSingle=tempSingle_ch1[ii];
    ch1[ii].tempIHR=tempIHR_ch1[ii];
  }
  string modules_ch2[5]={"KEKQ24", "Liv8", "Paris6", "Paris16", "Goe7"};
  double tempSingle_ch2[5]={20.0, 20.4, 20.2, 20.2, 20.2};
  double tempIHR_ch2[5]={20, 20, 20, 20, 20};
  for(int ii=0;ii<5;ii++){
    ch2[ii].moduleName=modules_ch2[ii];
    ch2[ii].tempSingle=tempSingle_ch2[ii];
    ch2[ii].tempIHR=tempIHR_ch2[ii];
  }
  for(int ii=0;ii<1;ii++){
    string fileName = "Paris18_iv_cellloading.json";
    //    cout<<fileName<<endl;
    ifstream ifs(fileName);
    json jsonFile;
    ifs >> jsonFile;

    for(int jj=0;jj<30;jj++){
      double voltage = jsonFile["Sensor_IV"][jj]["Voltage"];
      double current = jsonFile["Sensor_IV"][jj]["Current_mean"];
      voltage = abs(voltage);
      current = abs(current);
      current = current*ch1[ii].ratio();
      cout<<voltage<<" "<<current<<endl;
      hist_ch1[ii]->Fill(voltage, current);
      ch1_volt[ii][jj]=voltage;
      ch1_curr[ii][jj]=current;
    }
    each_ch1[ii] = new TGraph(23, ch1_volt[ii], ch1_curr[ii]);
    hist_ch1[ii]->SetFillColorAlpha(ii+1,0.5);
    hs_ch1->Add(hist_ch1[ii],"hist");
  }

  /*
    for(int ii=0;ii<5;ii++){
    string fileName = "../IVbefore/"+ch2[ii].moduleName+"_iv_LVoff.json";
    //    cout<<fileName<<endl;
    ifstream ifs(fileName);
    json jsonFile;
    ifs >> jsonFile;

    for(int jj=0;jj<23;jj++){
      double voltage = jsonFile["Sensor_IV"][jj]["Voltage"];
      double current = jsonFile["Sensor_IV"][jj]["Current_mean"];
      voltage = abs(voltage);
      current = abs(current);
      current = current*ch2[ii].ratio();
      cout<<voltage<<" "<<current<<endl;
      hist_ch2[ii]->Fill(voltage, current);
      ch2_volt[ii][jj]=voltage;
      ch2_curr[ii][jj]=current;
    }
    each_ch2[ii] = new TGraph(23, ch2_volt[ii], ch2_curr[ii]);
    hist_ch2[ii]->SetFillColorAlpha(ii+1,0.5);
    hs_ch2->Add(hist_ch2[ii],"hist");
  }
  */

    //Create TGraph
  for(int ii=0;ii<11;ii++){
    xaxis_ch1[ii]=data_ch1[ii][0];
    yaxis_ch1[ii]=data_ch1[ii][1];

    //    xaxis_ch2[ii]=data_ch2[ii][0];
    //    yaxis_ch2[ii]=data_ch2[ii][1];
  }

  TGraph *gr_ch1 = new TGraph(11,xaxis_ch1,yaxis_ch1);
  TGraph *gr_ch2 = new TGraph(11,xaxis_ch2,yaxis_ch2);

    
  gr_ch1->GetYaxis()->SetRangeUser(0,10e-6);
  gr_ch1->SetMarkerStyle(8);
  gr_ch1->SetMarkerSize(1.5);
  gr_ch1->SetLineWidth(2);
  gr_ch1->SetTitle("IV scan ch1");
  gr_ch1->GetXaxis()->SetTitle("Voltage");
  gr_ch1->GetYaxis()->SetTitle("Current");
  gr_ch1->Draw();
  /*
  gr_ch2->GetYaxis()->SetRangeUser(0,5e-6);
  gr_ch2->SetMarkerStyle(8);
  gr_ch2->SetMarkerSize(1.5);
  gr_ch2->SetLineWidth(2);
  gr_ch2->SetTitle("IV scan ch2");
  gr_ch2->GetXaxis()->SetTitle("Voltage");
  gr_ch2->GetYaxis()->SetTitle("Current");
  //  gr_ch2->Draw();
  */
  TLegend* leg_ch1 = new TLegend(0.15,0.6,0.4,0.85);
  TLegend* leg_ch1_1 = new TLegend(0.15,0.6,0.4,0.85);
  leg_ch1->AddEntry(gr_ch1,"IV scan on IHR","pl");
  for(int ii=0;ii<6;ii++){
    leg_ch1->AddEntry(hist_ch1[ii],modules_ch1[ii].c_str(),"f");
    leg_ch1_1->AddEntry(each_ch1[ii],modules_ch1[ii].c_str(),"pl");
  }
  leg_ch1->Draw();
  /*
  TLegend* leg_ch2 = new TLegend(0.15,0.6,0.4,0.85);
  TLegend* leg_ch2_2 = new TLegend(0.15,0.6,0.4,0.85);
  leg_ch2->AddEntry(gr_ch2,"IV scan on IHR","pl");
  for(int ii=0;ii<5;ii++){
    leg_ch2->AddEntry(hist_ch2[ii],modules_ch2[ii].c_str(),"f");
    leg_ch2_2->AddEntry(each_ch2[ii],modules_ch2[ii].c_str(),"pl");
  }
  //  leg_ch2->Draw();
  */
  
  hs_ch1->Draw("same");
  gr_ch1->Draw("same");
  c1->SaveAs("test.pdf");
  /*
  TCanvas *c2 = new TCanvas("c2", "", 1200, 800);
  gr_ch2->Draw();
  leg_ch2->Draw();
  hs_ch2->Draw("same");
  gr_ch2->Draw("same");
  gr_ch2->Draw("plsame");
  c2->SaveAs("test_2.pdf");

  
  TCanvas *c3 = new TCanvas("c3", "", 1200, 800);
  each_ch1[0]->SetTitle("Channel 1 (normalised at 20C)");
  each_ch1[0]->GetXaxis()->SetTitle("Bias voltage (V)");
  each_ch1[0]->GetYaxis()->SetTitle("Leakage Current (A)");
  for(int ii=0;ii<6;ii++){
    each_ch1[ii]->SetLineColor(ii+1);
    each_ch1[ii]->SetMarkerSize(2);
    each_ch1[ii]->SetMarkerStyle(8);
    each_ch1[ii]->SetMarkerColor(ii+1);
    if(ii==0){
      each_ch1[ii]->GetYaxis()->SetRangeUser(0, 1e-6);
      each_ch1[ii]->Draw("apl");}
    else{each_ch1[ii]->Draw("plsame");}
  }
  leg_ch1_1->Draw();
  c3->SaveAs("test_3.pdf");
  

  TCanvas *c4 = new TCanvas("c4", "", 1200, 800);
  each_ch2[0]->SetTitle("Channel 2 (normalised at 20C)");
  each_ch2[0]->GetXaxis()->SetTitle("Bias voltage (V)");
  each_ch2[0]->GetYaxis()->SetTitle("Leakage Current (A)");
  for(int ii=0;ii<5;ii++){
    each_ch2[ii]->SetLineColor(ii+1);
    each_ch2[ii]->SetMarkerSize(2);
    each_ch2[ii]->SetMarkerStyle(8);
    each_ch2[ii]->SetMarkerColor(ii+1);
    if(ii==0){
      each_ch2[ii]->GetYaxis()->SetRangeUser(0, 2e-6);
      each_ch2[ii]->Draw("apl");}
    else{each_ch2[ii]->Draw("plsame");}
  }
  leg_ch2_2->Draw();
  c4->SaveAs("test_4.pdf");
  */

}
  
