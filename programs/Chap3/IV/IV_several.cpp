#include <iostream>
#include <fstream>
#include <nlohmann/json.hpp>
#include <tuple>
#include <math.h>

#include <TROOT.h>
#include <TGraph.h>
#include <TGraphErrors.h>
#include <TH1F.h>
#include <THStack.h>
#include <TStyle.h>
#include <TLegend.h>
#include <TCanvas.h>
#include <TGaxis.h>
#include <TAxis.h>

using namespace std;
using json = nlohmann::json;

class thisModule{
public:
  string moduleName;
  double tempSingle;
  double tempIHR;
  double ratio(){
    double Eg0=1.17;
    double alpha=4.73*0.0001;
    double beta=636;
    double kB=8.617 * pow(10,-5);
    double T1=tempSingle+273.2;
    double T2=tempIHR+273.2;
    cout<<moduleName<<": "<<(T2/T1)*(T2/T1)*exp((-(1.17-(4.73*0.0001*T2*T2)/(T2+636))/(2*kB))*(1./T2)-(1.17-(4.73*0.0001*T1*T1)/(T1+636))/(2*kB)*(-1./T1))<<endl;
    return (T2/T1)*(T2/T1)*exp((-(1.17-(4.73*0.0001*T2*T2)/(T2+636))/(2*kB))*(1./T2)-(1.17-(4.73*0.0001*T1*T1)/(T1+636))/(2*kB)*(-1./T1));
  }
};


int main(){
  TCanvas *c1 = new TCanvas("c1", "", 1200, 800);
  cout<<"first"<<endl;
  TGraphErrors *gr[3];
  thisModule module[3];
  ifstream ifs("data/Paris18_iv_cellloading.json");
  json jsonFile[3];
  ifs >> jsonFile[0];
  cout<<"second"<<endl;
  ifstream ifs2("data/KEKQ19_iv_cellloading.json");
  ifs2 >> jsonFile[1];
  cout<<"third"<<endl;
  ifstream ifs3("data/Goe4_iv_cellloading.json");
  ifs3 >> jsonFile[2];

  module[0].moduleName="Paris18";
  module[0].tempSingle=23.5;
  module[0].tempIHR=20;

  module[1].moduleName="KEKQ19";
  module[1].tempSingle=23.8;
  module[1].tempIHR=20;

  module[2].moduleName="Paris10";
  module[2].tempSingle=23.5;
  module[2].tempIHR=20;
  
  double volt[3][31];
  double curr[3][31];
  double eVolt[3][31];
  double eCurr[3][31];
  
  for(int jj=0;jj<31;jj++){
    double voltage = jsonFile[0]["Sensor_IV"][jj]["Voltage"];
    double current = jsonFile[0]["Sensor_IV"][jj]["Current_mean"];
    double eCurrent = jsonFile[0]["Sensor_IV"][jj]["Current_sigma"];
    voltage = abs(voltage);
    current = abs(current);
    current = current*module[0].ratio();
    cout<<voltage<<" "<<current<<endl;
    volt[0][jj]=voltage;
    curr[0][jj]=current;
    eVolt[0][jj]=0;
    eCurr[0][jj]=eCurrent;
  }


  for(int jj=0;jj<31;jj++){
    double voltage = jsonFile[1]["Sensor_IV"][jj]["Voltage"];
    double current = jsonFile[1]["Sensor_IV"][jj]["Current_mean"];
    double eCurrent = jsonFile[1]["Sensor_IV"][jj]["Current_sigma"];
    voltage = abs(voltage);
    current = abs(current);
    current = current*module[1].ratio();
    cout<<voltage<<" "<<current<<endl;
    volt[1][jj]=voltage;
    curr[1][jj]=current;
    eVolt[1][jj]=0;
    eCurr[1][jj]=eCurrent;
  }
  
  for(int jj=0;jj<31;jj++){
    double voltage = jsonFile[2]["Sensor_IV"][jj]["Voltage"];
    double current = jsonFile[2]["Sensor_IV"][jj]["Current_mean"];
    double eCurrent = jsonFile[2]["Sensor_IV"][jj]["Current_sigma"];
    voltage = abs(voltage);
    current = abs(current);
    current = current*module[2].ratio();
    cout<<voltage<<" "<<current<<endl;
    volt[2][jj]=voltage;
    curr[2][jj]=current;
    eVolt[2][jj]=0;
    eCurr[2][jj]=eCurrent;
  }

  gr[0]=new TGraphErrors(31, volt[0], curr[0], eVolt[0], eCurr[0]);
  gr[1]=new TGraphErrors(31, volt[1], curr[1], eVolt[1], eCurr[1]);
  gr[2]=new TGraphErrors(31, volt[2], curr[2], eVolt[2], eCurr[2]);
  gPad->SetGrid(1, 1); gPad->Update();
  
  gr[0]->SetMarkerStyle(20);
  gr[1]->SetMarkerStyle(21);
  gr[2]->SetMarkerStyle(22);

  for(int ii=0;ii<3;ii++){
    gr[ii]->SetLineWidth(3);
    gr[ii]->SetTitle("");
    gr[ii]->GetXaxis()->SetTitle("Bias voltage [V]");
    gr[ii]->GetYaxis()->SetTitle("Leakage current [A]");
    gr[ii]->SetMarkerSize(2);
  }

  gr[1]->SetMarkerColor(2);
  gr[1]->SetLineColor(2);

  gr[2]->SetMarkerColor(3);
  gr[2]->SetLineColor(3);
  gr[0]->Draw("apl");
  gr[1]->Draw("plsame");
  gr[2]->Draw("plsame");

  TLegend* leg_ch1 = new TLegend(0.15,0.6,0.45,0.85);
  leg_ch1->AddEntry(gr[0],"Paris18","pl");
  leg_ch1->AddEntry(gr[1],"KEKQ19","pl");
  leg_ch1->AddEntry(gr[2],"Goe4","pl");
  leg_ch1->Draw();

  c1->SaveAs("several_IV.pdf");
}
