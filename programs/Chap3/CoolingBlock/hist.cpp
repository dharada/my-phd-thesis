#include <fstream>
#include <nlohmann/json.hpp>
#include <iostream>
#include <TGraphErrors.h>
#include <TGraph.h>
#include <TCanvas.h>
#include <TROOT.h>
#include <TLegend.h>
#include <TH1D.h>
#include <TH1.h>
#include <THStack.h>
#include <TLatex.h>
#include <TStyle.h>
using namespace std;
// for convenience                                                                                         \
                                                                                                            
using json = nlohmann::json;

class ThisPosition{
public:
  TH1D *hist=new TH1D("q","q",20,12,16);
  int position;
  vector <double> vec_Power;
  vector <double> vec_Jig;
  vector <double> vec_Temp;
  vector <double> vec_TFM;

  void push_backs(json data){
    for(int cycle=0;cycle<11;cycle++){
      json newData=data["ThermalTest"][cycle]["Power"];
      for(int power=0;power<5;power++){
	if(newData[power]["Power"]>0.46&&newData[power]["Power"]<0.54){
	  vec_Power.push_back(newData[power]["Power"]);
	  vec_Jig.push_back(newData[power]["JigTemp"]);
	  vec_Temp.push_back(newData[power]["Heaters"][position]);
	  //	  cout<<vec_Power[vec_Power.size()-1]<<" "<<vec_Jig[vec_Jig.size()-1]<<" "<<vec_Temp[vec_Temp.size()-1]<<endl;
	}
      }
    }
  }

  void calcTFMs(){
    for(int jj=0;jj<vec_Power.size();jj++){
        double bunbo;
        if(vec_Power[jj]==0){
          bunbo=0.01;
        }else{
          bunbo=vec_Power[jj];
        }
	//        vec_TFM.push_back((vec_Temp[jj]-vec_Jig[jj])/bunbo);
	vec_TFM.push_back((vec_Temp[jj]-vec_Jig[jj]));
    }
    for(int ii=0;ii<vec_TFM.size();ii++){
      cout<<vec_Temp[ii]-vec_Jig[ii]<<", ";
    }
    cout<<endl;
  }

  void fillHist(int color){
    for(int ii=0;ii<vec_TFM.size();ii++){
      hist->Fill(vec_TFM[ii]);
      hist->SetFillColor(color);
    }
  }

  TH1D retHist(){
    return *hist;
  }
};

int main(){
  TCanvas *c1=new TCanvas("","",600,400);

  ifstream i("data.json");
  json j;
  i >> j;
  THStack *hs;
  hs=new THStack("",";#DeltaT (Module - Jig) [#circC]; Event");
  //    hs=new THStack("",";TFM [#circC cm^{2}/W]");
  ThisPosition position[3];
  for(int ii=0;ii<3;ii++){
    if(ii==0){position[ii].position=0;}
    else if(ii==1){position[ii].position=2;}
    else if(ii==2){position[ii].position=3;}
    position[ii].push_backs(j);
    position[ii].calcTFMs();
    position[ii].fillHist(ii+2);
    //    TH1D hist=position[ii].retHist();

  }
  cout<<"check"<<endl;
  double data[3][9]={
    //   {15.4609, 15.4681, 14.0117, 13.6613, 12.6954, 14.1683, 14.7595, 13.497, 14.7129},
    //    {15.5611, 15.4482, 13.8552, 13.499, 12.6754, 14.1485, 14.479, 13.6172, 14.8119},
    //    {15.1403, 15.6673, 13.5812, 13.357, 12.5351, 13.9901, 14.3387, 13.3367, 14.4158}};
    {7.715, 7.765, 7.16, 6.735, 6.335, 7.155, 7.365, 6.735, 7.43},
    {7.765, 7.755, 7.08, 6.655, 6.325, 7.145, 7.225, 6.795, 7.48}, 
    {7.555, 7.865, 6.94, 6.585, 6.255, 7.065, 7.155, 6.655, 7.28}};
  TH1D*hist[3];
  for(int ii=0;ii<3;ii++){
    hist[ii]=new TH1D("","",20,6,8);
    //    hist[ii]=new TH1D("","",20,12,16);
    for(int jj=0;jj<9;jj++){
      hist[ii]->Fill(data[ii][jj]);
    }
    hist[ii]->SetFillColorAlpha(ii+2,0.5);
    hs->Add(hist[ii],"hist");
  }
  hs->Draw();
  hs->GetHistogram()->GetXaxis()->SetLabelSize(0.05);
  hs->GetHistogram()->GetXaxis()->SetTitleSize(0.04);
  hs->GetHistogram()->GetYaxis()->SetLabelSize(0.05);
  hs->GetHistogram()->GetYaxis()->SetTitleSize(0.04);
    hs->Draw();

    TLegend* leg = new TLegend(0.15,0.55,0.35,0.73);
    leg->AddEntry(hist[0],"Thermistor 1","f");
    leg->AddEntry(hist[1],"Thermistor 4","f");
    leg->AddEntry(hist[2],"Thermistor 5","f");
    leg->Draw();

    TLatex latex(6.2,3.9,"Power density 0.5 W/cm^{2}");
    //    TLatex latex(13.6,3.8,"Power density 0.5 W/cm^{2}");
    latex.Draw();
    TLatex latex2(6.2,3.5,"T_{Jig}=5#circC");
    //    TLatex latex2(13.6,3.4,"T_{Jig}=5#circC");
    latex2.Draw();
  c1->SaveAs("hist.pdf");
}
