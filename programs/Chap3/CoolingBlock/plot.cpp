#include <fstream>
#include <nlohmann/json.hpp>
#include <iostream>
#include <TGraphErrors.h>
#include <TGraph.h>
#include <TCanvas.h>
#include <TROOT.h>
#include <TLegend.h>
#include <THStack.h>
#include <TLatex.h>
#include <TStyle.h>
using namespace std;
// for convenience                                                                                                              
using json = nlohmann::json;

class ThisCycle{
public:
  TGraph *gr[4];
  vector <double> vec_Power;
  vector <double> vec_Jig;
  vector <double> vec_Temp[4];
  vector <double> vec_TFM[4];

  void push_backs(json data){
    vec_Power.push_back(data["Power"]);
    vec_Jig.push_back(data["JigTemp"]);
    for(int ii=0;ii<4;ii++){
      vec_Temp[ii].push_back(data["Heaters"][ii]);
    }
  }
  
  void setTFMs(){
    for(int ii=0;ii<4;ii++){
      for(int jj=0;jj<vec_Power.size();jj++){
	double bunbo;
	if(vec_Power[jj]==0){
	  bunbo=0.01;
	}else{
	  bunbo=vec_Power[jj];
	}
	vec_TFM[ii].push_back((vec_Temp[ii][jj]-vec_Jig[jj])/bunbo);
      }
    }
  }
  void createGraph(){
    const int dataSize=vec_Power.size();
    double xAxis[4][dataSize];
    double yAxis[4][dataSize];
    for(int ii=0;ii<4;ii++){
      for(int jj=0;jj<dataSize;jj++){
	xAxis[ii][jj]=vec_Power[jj];
	yAxis[ii][jj]=vec_TFM[ii][jj];
      }

      gr[ii]=new TGraph(dataSize, xAxis[ii], yAxis[ii]);
      gr[ii]->GetYaxis()->SetRangeUser(10,20);
      
    } 
  }
  
};

class thisPosition{
public:
  int posId;
  TGraphErrors *gr;
  double temp[9][6];
  double jig[9][6];
  double power[9][6];
  double TFM[9][6];
  double xAxis[5]={0.2, 0.5, 0.8, 1.0, 1.2};
  double eXaxis[5]={0,0,0,0,0};
  double yAxis[5];
  double eYaxis[5];

  void setPosId(int id){
    posId = id;
  }
  void setInfo(json data){
    for(int ii=2;ii<11;ii++){
      for(int jj=1;jj<6;jj++){
	temp[ii-2][jj-1]=data[ii]["Power"][jj]["Heaters"][posId];
	power[ii-2][jj-1]=data[ii]["Power"][jj]["Power"];
	jig[ii-2][jj-1]=data[ii]["Power"][jj]["JigTemp"];
	//	cout<<data[ii]["Power"][jj]["Power"]<<" "<<data[ii]["Power"][jj]["Heaters"][posId]<<endl;
      }
    }
  }

  void setTFM(){
    for(int ii=0;ii<9;ii++){
      for(int jj=0;jj<5;jj++){
	double bunbo=0;
	if(power[ii][jj]==0){
	  bunbo=0.01;
	}else{
	  bunbo=power[ii][jj];
	}
	TFM[ii][jj]=(temp[ii][jj]-jig[ii][jj])/bunbo;
      }
    }
  }

  void calMeanSigma(){
    for(int ii=0;ii<5;ii++){
      yAxis[ii]=0;
      eYaxis[ii]=0;
      for(int jj=0;jj<9;jj++){
	yAxis[ii]=yAxis[ii]+TFM[jj][ii]/9;
      }
      for(int jj=0;jj<9;jj++){
	eYaxis[ii]=eYaxis[ii]+(yAxis[ii]-TFM[jj][ii])*(yAxis[ii]-TFM[jj][ii]);
      }
      eYaxis[ii]=sqrt(eYaxis[ii]/9);
      cout<<"Power: "<<xAxis[ii]<<", Mean:"<<yAxis[ii]<<", Sigma:"<<eYaxis[ii]<<endl;
    }
  }

  void setGraph(){
    gr = new TGraphErrors(5, xAxis, yAxis, eXaxis, eYaxis);
  }
  
};

int main(){
  TCanvas *c1=new TCanvas("c1","",600,400);
  ifstream i("data.json");
  json j;
  i >> j;
  ThisCycle cycle[11];
  int cycleNumber=0;
  int powerNumber=0;
  while(j["ThermalTest"][cycleNumber]["Cycle"]>0){
    cout<<j["ThermalTest"][cycleNumber]["Cycle"]<<endl;

    while(j["ThermalTest"][cycleNumber]["Power"][powerNumber]["Power"]>-1){
      //      cycle[cycleNumber].push_backs(j, cycleNumber);
      cycle[cycleNumber].push_backs(j["ThermalTest"][cycleNumber]["Power"][powerNumber]);
      powerNumber++;
    }
    cycle[cycleNumber].setTFMs();
    cycle[cycleNumber].createGraph();
    
    powerNumber=0;
    cycleNumber++;
  }
  cycleNumber=0;
  //  cycle[cycleNumber].gr[0]->Draw();
  
  while(j["ThermalTest"][cycleNumber]["Cycle"]>0){
    if(cycleNumber==0){cycle[cycleNumber].gr[0]->Draw();}
    else{cycle[cycleNumber].gr[0]->Draw("same");}
    cycleNumber++;
  }
  
  c1->SaveAs("plot.pdf");


  TCanvas *c2=new TCanvas("c2","",600,400);
  gPad->SetBottomMargin(0.12);
  thisPosition pos[4];
  for(int ii=0;ii<4;ii++){
    pos[ii].setPosId(ii);
    pos[ii].setInfo(j["ThermalTest"]);
    pos[ii].setTFM();
    pos[ii].calMeanSigma();
    pos[ii].setGraph();
  }
  for(int ii=0;ii<4;ii++){
    pos[ii].gr->SetTitle("The thermal figure merit of Quad heater module");
    pos[ii].gr->GetXaxis()->SetTitle("Power density [W/cm^{2}]");
    pos[ii].gr->GetYaxis()->SetTitle("TFM [#circC cm^{2}/W]");
    //    pos[ii].gr->SetTitleSize(2);
    //    gStyle->SetTitleW(0.2);
    //    gStyle->SetTitleH(0.9);    
    pos[ii].gr->GetXaxis()->SetTitleSize(0.05);
    pos[ii].gr->GetYaxis()->SetTitleSize(0.05);
    pos[ii].gr->GetXaxis()->SetLabelSize(0.05);
    pos[ii].gr->GetYaxis()->SetLabelSize(0.05);

    pos[ii].gr->SetMarkerColor(ii+1);
    pos[ii].gr->SetMarkerSize(1.5);
    pos[ii].gr->SetLineWidth(2);
    pos[ii].gr->SetLineColor(ii+1);
    pos[ii].gr->SetMarkerStyle(ii+20);
    pos[ii].gr->GetYaxis()->SetRangeUser(12,18);
    if(ii==0){
      pos[ii].gr->Draw("apl");
    }else{
      pos[ii].gr->Draw("plsame");
    }
  }
  TLegend* leg = new TLegend(0.65,0.65,0.85,0.85);
  leg->AddEntry(pos[0].gr,"Positon 1","p");
  leg->AddEntry(pos[1].gr,"Positon 3","p");
  leg->AddEntry(pos[2].gr,"Positon 4","p");
  leg->AddEntry(pos[3].gr,"Positon 5","p");

  leg->Draw();
  gPad->SetGrid(1, 1); gPad->Update();

  c2->SaveAs("plot_2.pdf");
}
