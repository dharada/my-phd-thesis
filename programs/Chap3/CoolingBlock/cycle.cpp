#include <fstream>
#include <nlohmann/json.hpp>
#include <iostream>
#include <TGraphErrors.h>
#include <TGraph.h>
#include <TCanvas.h>
#include <TROOT.h>
#include <TLegend.h>
#include <TH1D.h>
#include <TH1.h>
#include <THStack.h>
#include <TLatex.h>
#include <TStyle.h>
using namespace std;

int main(){
  TCanvas *c1=new TCanvas("","",600,400);
  c1->SetLeftMargin(0.14);
  c1->SetBottomMargin(0.14);
  double data[3][9]={
    {7.715, 7.765, 7.16, 6.735, 6.335, 7.155, 7.365, 6.735, 7.43},
    {7.765, 7.755, 7.08, 6.655, 6.325, 7.145, 7.225, 6.795, 7.48},
    {7.555, 7.865, 6.94, 6.585, 6.255, 7.065, 7.155, 6.655, 7.28}};
  double xAxis[9]={1,2,3,4,5,6,7,8,9};

  TGraph* gr[3];
  for(int ii=0;ii<3;ii++){
    gr[ii]=new TGraph(9,xAxis, data[ii]);
    gr[ii]->SetLineColor(ii+2);
    gr[ii]->SetMarkerColor(ii+2);
    gr[ii]->SetMarkerStyle(ii+20);
    gr[ii]->SetMarkerSize(1.5);
  }
  gr[0]->SetTitle("Repeatability measurement");
  gr[0]->GetXaxis()->SetTitle("Cycle");
  gr[0]->GetYaxis()->SetTitle("#Delta T (Module-Jig) [#circC]");

  gr[0]->GetXaxis()->SetTitleSize(0.06);
  gr[0]->GetXaxis()->SetLabelSize(0.05);
  gr[0]->GetYaxis()->SetTitleSize(0.06);
  gr[0]->GetYaxis()->SetLabelSize(0.05);
  gr[0]->Draw("ap");
  gr[1]->Draw("psame");
  gr[2]->Draw("psame");

  TLegend* leg = new TLegend(0.15,0.2,0.35,0.4);
  leg->AddEntry(gr[0],"Thermistor 1","p");
  leg->AddEntry(gr[1],"Thermistor 4","p");
  leg->AddEntry(gr[2],"Thermistor 5","p");
  leg->Draw();
  TLatex latex(4.5,7.5,"Power density 0.5 W/cm^{2}");
  //    TLatex latex(13.6,3.8,"Power density 0.5 W/cm^{2}");                                                                         
  latex.Draw();
  TLatex latex2(4.5,7.7,"T_{Jig}=5#circC");
  //    TLatex latex2(13.6,3.4,"T_{Jig}=5#circC");                                                                                   
  latex2.Draw();
  c1->SaveAs("cycle.pdf");
}
