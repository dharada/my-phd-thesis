#include <fstream>
#include <nlohmann/json.hpp>
#include <iostream>
#include <TGraphErrors.h>
#include <TGraph.h>
#include <TCanvas.h>
#include <TROOT.h>
#include <TLegend.h>
#include <THStack.h>
#include <TLatex.h>
using namespace std;
// for convenience                                                                                                                                                          
using json = nlohmann::json;

class thisTemp{
public:
  TGraphErrors *gr;
  double power[7];
  double jig[7];
  double heater[4][7];
  double TFM[4][7];
  double xAxis[7]={0.2, 0.4, 0.6, 0.8, 1.0, 1.2, 1.5};
  double eXaxis[7]={0, 0, 0, 0, 0, 0, 0};
  double yAxis[7];
  double eYaxis[7];
  
  void setInfo(json j){
    for(int ii=1;ii<8;ii++){
      power[ii-1]=j["Power"][ii]["Power"];
      jig[ii-1]=j["Power"][ii]["JigTemp"];
      //      cout<<j["Power"][ii]["Power"]<<" "<<j["Power"][ii]["JigTemp"]<<endl;
      for(int jj=0;jj<4;jj++){
	heater[jj][ii-1]=j["Power"][ii]["Heaters"][jj];
      }
    }
  }
  void setTFM(){
    double bunbo;
    for(int ii=0;ii<7;ii++){
      for(int jj=0;jj<4;jj++){
	if(power[ii]==0){bunbo=0.01;}
	else{bunbo=power[ii];}
	TFM[jj][ii]=(heater[jj][ii]-jig[ii])/bunbo;
	cout<<TFM[jj][ii]<<" "<<heater[jj][ii]<<" "<<jig[ii]<<endl;
      }
    }
  }
  void calcMean(){
    double mean=0;
    double sigma=0;
    for(int ii=0;ii<7;ii++){
      yAxis[ii]=(TFM[0][ii]+TFM[2][ii]+TFM[3][ii])/3;
    }
    for(int ii=0;ii<7;ii++){
      eYaxis[ii]=pow(yAxis[ii]-TFM[0][ii],2)+pow(yAxis[ii]-TFM[2][ii],2)+pow(yAxis[ii]-TFM[3][ii],2);
      eYaxis[ii]=sqrt(eYaxis[ii]/3);
    }
  }
  void setGraph(){
    for(int ii=0;ii<7;ii++){
      //cout<<xAxis[ii]<<" "<<yAxis[ii]<<" "<<eXaxis[ii]<<" "<<eYaxis[ii]<<endl;
    }
    gr = new TGraphErrors(7, xAxis, yAxis, eXaxis, eYaxis);
    gr->GetYaxis()->SetRangeUser(12,20.5);
  }
};

int main(){
  TCanvas *c1=new TCanvas("c1","",600,400);
  gPad->SetBottomMargin(0.12);
  ifstream i("data_2.json");
  json j;
  i >> j;

  thisTemp jigTemp[3];

  for(int ii=0;ii<3;ii++){
    jigTemp[ii].setInfo(j["ThermalTest"][ii]);
    jigTemp[ii].setTFM();
    jigTemp[ii].calcMean();
    jigTemp[ii].setGraph();
    jigTemp[ii].gr->SetMarkerStyle(ii+20);
    jigTemp[ii].gr->SetMarkerSize(1);
    jigTemp[ii].gr->SetMarkerColor(ii+1);
    jigTemp[ii].gr->SetLineColor(ii+1);
    jigTemp[ii].gr->SetLineWidth(2);    
  }
  //jigTemp[1].gr->Draw();
  
  for(int ii=0;ii<3;ii++){
    if(ii==0){
      jigTemp[ii].gr->SetTitle("The thermal figure merit in each cooling jig temperature");
      jigTemp[ii].gr->GetXaxis()->SetTitle("Power density [W/cm^{2}]");
      jigTemp[ii].gr->GetYaxis()->SetTitle("TFM [#circC cm^{2}/W]");
      jigTemp[ii].gr->GetXaxis()->SetTitleSize(0.05);
      jigTemp[ii].gr->GetYaxis()->SetTitleSize(0.05);
      jigTemp[ii].gr->GetXaxis()->SetLabelSize(0.05);
      jigTemp[ii].gr->GetYaxis()->SetLabelSize(0.05);
	    
      
      jigTemp[ii].gr->Draw();}
    else{jigTemp[ii].gr->Draw("plsame");}
  }
  gPad->SetGrid(1, 1); gPad->Update();
 
  TLegend* leg = new TLegend(0.65,0.65,0.85,0.85);
  leg->AddEntry(jigTemp[0].gr,"JigTemp 0","p");
  leg->AddEntry(jigTemp[1].gr,"JigTemp 5","p");
  leg->AddEntry(jigTemp[2].gr,"JigTemp 10","p");
  leg->Draw();
  c1->SaveAs("plot.pdf");
}
