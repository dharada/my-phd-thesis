#include <iostream>
#include <fstream>
#include <nlohmann/json.hpp>
#include <tuple>
#include <math.h>
#include <vector>

#include <TROOT.h>
#include <TGraph.h>
#include <TH1F.h>
#include <THStack.h>
#include <TStyle.h>
#include <TLegend.h>
#include <TCanvas.h>
#include <TGaxis.h>
#include <TAxis.h>

using namespace std;
double timeStamp(double hour, double min, double sec){
  double start=13*3600+20*60+9;
  return 3600*hour+60*min+sec-start-60;
}

int main(){
    TCanvas *c1 = new TCanvas("","",600,400);
  vector <double> v_time;
  vector <double> v_temp;
  vector <double> v_hum;
  vector <double> v_dew;

  ifstream ifs("data.txt");
  //  ifstream ifs("~/work/unige/my-phd-thesis/programs/Chap3/CoolingBlock/humidity/data.txt");
  double hoge;
  double s_Ho, s_M, s_S, s_T, s_Hu, s_D;
  if(!ifs){
    cout<<"File do not exist"<<endl;
  }
  if(!ifs.is_open()){
    cout<<"File do not open"<<endl;
  }

  while(ifs>>s_Ho>>s_M>>s_S>>s_T>>s_Hu>>s_D){
    //    cout<<hoge<<endl;
    if(v_time.size()!=0){
      if(timeStamp(s_Ho, s_M, s_S)!=v_time[v_time.size()-1]){
	v_time.push_back(timeStamp(s_Ho, s_M, s_S));
	v_temp.push_back(s_T);
	v_hum.push_back(s_Hu);
	v_dew.push_back(s_D);
      }
    }else{
       v_time.push_back(timeStamp(s_Ho, s_M, s_S));
       v_temp.push_back(s_T);
       v_hum.push_back(s_Hu);
       v_dew.push_back(s_D);
    }
    //    cout<<s_Ho<<" "<<s_M<<" "<<s_S<<" "<<v_time[v_time.size()-1]<<endl;

  }
  double x_Axis[v_time.size()];
  double y_temp[v_time.size()];
  double y_hum[v_time.size()];
  double y_dew[v_time.size()];
  for(int ii=0;ii<v_time.size();ii++){
    x_Axis[ii]=v_time[ii];
    y_temp[ii]=v_temp[ii];
    y_hum[ii]=v_hum[ii]*2-40;
    y_dew[ii]=v_dew[ii];
    cout<<y_temp[ii]<<" "<<y_hum[ii]<<" "<<y_dew[ii]<<endl;
  }
  TGraph *gr[3];

  gr[0]=new TGraph(v_time.size(), x_Axis, y_temp);
  gr[1]=new TGraph(v_time.size(), x_Axis, y_hum);
  gr[2]=new TGraph(v_time.size(), x_Axis, y_dew);

  gr[0]->SetLineColor(1);
  gr[1]->SetLineColor(2);
  gr[2]->SetLineColor(3);
      

  gr[0]->SetLineWidth(2);
  gr[1]->SetLineWidth(2);
  gr[2]->SetLineWidth(2);
  gr[0]->GetXaxis()->SetRangeUser(-50,800);
  gr[0]->GetYaxis()->SetRangeUser(-40,40);
  gr[0]->SetTitle("Environmental monitoring");
  gr[0]->GetXaxis()->SetTitle("Time [s]");
  //  gr[0]->GetYaxis()->SetTitle("[#circC]");
  gr[0]->GetYaxis()->SetTitle("[#circC]");
  gr[0]->GetXaxis()->SetLabelOffset(0.007);
  gr[0]->GetXaxis()->SetTitleSize(0.05);
  gr[0]->GetXaxis()->SetLabelSize(0.04);
  gr[0]->GetYaxis()->SetTitleOffset(0.7);
  gr[0]->GetYaxis()->SetTitleSize(0.06);
  gr[0]->GetYaxis()->SetLabelSize(0.04);

  gr[0]->Draw("al");
  gr[1]->Draw("lsame");
  gr[2]->Draw("lsame");


  c1->Modified();
  c1->Update();
  TGaxis *right_axis = new TGaxis(gPad->GetUxmax(),gPad->GetUymin(), gPad->GetUxmax(), gPad->GetUymax(),0, 40, 505,"+L");
  //  TGaxis *right_axis = new TGaxis(gPad->GetUxmax(),gPad->GetUymin(), gPad->GetUxmax(), gPad->GetUymax(),0,rightmax,505,"+L");
  right_axis->SetLineColor(kRed);
  right_axis->SetTextColor(kRed);
  right_axis->SetTitle("Humidity [%]");
  right_axis->SetTitleSize(0.05);
  //  right_axis->CenterTitle();
  right_axis->SetLabelOffset(0.007);
  //  right_axis->SetTextFont(82);                                                                                              
  //  right_axis->SetLabelFont(82);                                                                                             
  right_axis->SetLabelSize(0.04);
  right_axis->SetLabelColor(kRed);
  right_axis->SetTitleOffset(0.5);
 right_axis->Draw("same");

  
  TLegend* leg = new TLegend(0.6,0.65,0.85,0.85);
  leg->AddEntry(gr[0],"Temperature","l");
  leg->AddEntry(gr[1],"Humidity","l");
  leg->AddEntry(gr[2],"Dew point","l");
  
  leg->Draw();

  c1->SaveAs("environmental.pdf");
}
