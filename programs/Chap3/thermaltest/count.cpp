#include <fstream>
#include <nlohmann/json.hpp>
#include <iostream>
#include <TCanvas.h>
#include <TGraph.h>
#include <TGraphErrors.h>
#include <TF1.h>
#include <TLegend.h>
#include <TAxis.h>
#include <TLine.h>
#include <TMultiGraph.h>
using namespace std;

#include "SaveTGraph.cpp"
#include "GraphByModule.cpp"
#include "function.cpp"
using json = nlohmann::json;

void test(double array[][]){
  for(int ii=0;ii<4;ii++){
    cout<<array[0][ii]<<endl;
  }

}

int main(){
  json j;
  ifstream i("module.json");
  i >> j;

  cout<<j["Module"].size()<<endl;

  double array[3][4]={{1,2,3,4},
		      {5,6,7,8},
		      {9,10,11,12}};
  test(array);
  //  cout<<sizeof(j["Module"])<<endl;
}
