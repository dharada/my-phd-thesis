
#include <TROOT.h>
#include <TStyle.h>
#include <TGraph.h>
#include <TGaxis.h>
#include <TGraphErrors.h>
#include <TCanvas.h>
#include <TH1D.h>
#include <TF1.h>
#include <TLine.h>
#include <TLegend.h>
#include <TLatex.h>
#include <THStack.h>
#include <fstream>
#include <nlohmann/json.hpp>
using json = nlohmann::json;
using namespace std;

class Data{
};

//addData(vector<double> vecNTC20, vector<double> vecNTC10, vector<double> vecNTCmin, vector<string> vecname, double NTC20, double NTC10, double NTCmin, string name){

double calMeanSigma(double *data){
  double average = 0;
  for(int ii=0;ii<34;ii++){
    if(data[ii]>-20){
      average = average + data[ii];
    }
  }
  //  average= average +99*3;
  average = average/34;
  cout<<"Average: "<<average<<endl;
  double sigma = 0;
  for(int ii=0;ii<34;ii++){
    if(data[ii]>-20){
      sigma = sigma +(average-data[ii])*(average-data[ii]);
    }
  }
  sigma = sigma/33;
  sigma = sqrt(sigma);
  cout<<"Sigma: "<<sigma<<endl;
}
  
  

int main(){
  ifstream i("module.json");
  json j;
  i >> j;
  TCanvas *c1=new TCanvas("c1","",600,400);
  c1->SetBottomMargin(0.15);


  //    c1->SetBottomMargin(0.14);
  /*
  double NTC20[10]= {25.5, 25.7, 26.5, 25.9, 25.9, 26.5, 26.1, 26.3, 26.7, 26.3};
  double NTC10[10]= {16.0, 15.9, 16.0, 15.8, 16.2, 16.6, 16.6, 16.9, 17.1, 16.3};
  double NTCmin[10]={-5.3, -5.1, -4.9, -5.0, -5.3, -3.7, -3.1, -3.3, -4.2, -3.9};
  string module[10]={"CERN7", "CERN10",  "KEK14", "KEK18", "Paris3", "Paris10", "Goe7", "Goe4", "Siegen3", "Siegen4"};
  `*/
  TLine *line[2];
  vector <double> vecTemp20;
  vector <double> vecTemp10;
  vector <double> vecTempmin;
  vector <double> vecNTC20;
  vector <double> vecNTC10;
  vector <double> vecNTCmin;
  vector <double> vecOnlyTemp20;
  vector <double> vecOnlyTemp10;
  vector <double> vecOnlyTempmin;
  
  vector <double> vecErrOnlyTemp20;
  vector <double> vecErrOnlyTemp10;
  vector <double> vecErrOnlyTempmin;
  vector <double> eTemp20;
  vector <double> eTemp10;
  vector <double> eTempmin;
      
  cout<<"check"<<endl;
  vector <string> vecName;

  bool isFirst = true;
  int counting=0;
  string modules[37]={"CERN3", "CERN5", "CERN6", "CERN7", "CERN11", "Goe4", "Goe7", "Goe10", "KEK11", "KEK14", "KEK17", "KEK18", "KEK19", "KEK20", "KEK21", "Paris3", "Paris6", "Paris8", "Paris12", "Paris16", "Paris17", "Siegen1", "Siegen2", "Siegen3", "Siegen4", "space", "CERN10", "Goe5", "Paris7", "Paris9", "Paris10", "Paris11", "Paris13", "Paris18", "space", "CERN8", "space"};
  for(int jj=0;jj<37;jj++){
  for(int ii=0;ii<40;ii++){
    if(j["Module"][ii]["ModuleName"]==modules[jj]){
      cout<<modules[jj]<<endl;
      vecName.push_back(modules[jj]);
      vecTemp20.push_back(j["Module"][ii]["Sensor"][1]);
      vecTemp10.push_back(j["Module"][ii]["Sensor"][3]);
      vecTempmin.push_back(j["Module"][ii]["Sensor"][5]);

      vecNTC20.push_back(j["Module"][ii]["ModuleNTC"][1]);
      vecNTC10.push_back(j["Module"][ii]["ModuleNTC"][3]);
      vecNTCmin.push_back(j["Module"][ii]["ModuleNTC"][5]);
 
      vecOnlyTemp20.push_back(j["Module"][ii]["Sensor"][1]);
      vecOnlyTemp10.push_back(j["Module"][ii]["Sensor"][3]);
      vecOnlyTempmin.push_back(j["Module"][ii]["Sensor"][5]);
      /*
      vecErrOnlyTemp20.push_back(j["Module"][ii]["eCurrent"][1]);
      vecErrOnlyTemp10.push_back(j["Module"][ii]["eCurrent"][3]);
      vecErrOnlyTempmin.push_back(j["Module"][ii]["eCurrent"][5]);
      
      eTemp20.push_back(j["Module"][ii]["eCurrent"][1]);
      eTemp10.push_back(j["Module"][ii]["eCurrent"][3]);
      eTempmin.push_back(j["Module"][ii]["eCurrent"][5]);
      */
      vecErrOnlyTemp20.push_back(0);
      vecErrOnlyTemp10.push_back(0);
      vecErrOnlyTempmin.push_back(0);
      
      eTemp20.push_back(0);
      eTemp10.push_back(0);
      eTempmin.push_back(0);
    }
  }

  if(modules[jj]=="space"){
    if(counting==0){
      line[0]=new TLine(vecName.size()+0.5,-15,vecName.size()+0.5,50);
      counting++;
    }else if(counting==1){
      line[1]=new TLine(vecName.size()+0.5,-15,vecName.size()+0.5,50);
      counting++;
    }else if(counting==2){
      //      line[1]=new TLine(vecName.size()+0.5,-15,vecName.size()+0.5,30);
      counting++;
    }
    
    vecName.push_back(""); vecTemp20.push_back(-99); vecTemp10.push_back(-99); vecTempmin.push_back(-99);
    eTemp20.push_back(0);  eTemp10.push_back(0); eTempmin.push_back(0);

  }
    
  
  }
  //All FE
  /*
  vecName.push_back("CERN3"); vecNTC20.push_back(); vecNTC10.push_back(17.8); vecNTCmin.push_back(-2.7); 
  vecName.push_back("CERN5"); vecNTC20.push_back(); vecNTC10.push_back(19.1); vecNTCmin.push_back(-1.1);
  vecName.push_back("CERN6"); vecNTC20.push_back(); vecNTC10.push_back(17.7); vecNTCmin.push_back(-3.2);
  vecName.push_back("CERN7"); vecNTC20.push_back(); vecNTC10.push_back(16.0); vecNTCmin.push_back(-5.3);
  vecName.push_back("CERN11"); vecNTC20.push_back(); vecNTC10.push_back(15.9); vecNTCmin.push_back(-5.4);

  vecName.push_back("Goe4"); vecNTC20.push_back(); vecNTC10.push_back(16.9); vecNTCmin.push_back(-3.3);
  vecName.push_back("Goe7"); vecNTC20.push_back(); vecNTC10.push_back(16.6); vecNTCmin.push_back(-3.1);
  vecName.push_back("Goe10"); vecNTC20.push_back(); vecNTC10.push_back(17.2); vecNTCmin.push_back(-2.6);

  
  vecName.push_back("KEK11"); vecNTC20.push_back(); vecNTC10.push_back(16.9); vecNTCmin.push_back(-3.3);
  vecName.push_back("KEK14"); vecNTC20.push_back(); vecNTC10.push_back(16.0); vecNTCmin.push_back(-4.9);
  vecName.push_back("KEK17"); vecNTC20.push_back(); vecNTC10.push_back(17.8); vecNTCmin.push_back(-3.0);
  vecName.push_back("KEK18"); vecNTC20.push_back(); vecNTC10.push_back(17.7); vecNTCmin.push_back(-1.9);
  vecName.push_back("KEK19"); vecNTC20.push_back(); vecNTC10.push_back(16.9); vecNTCmin.push_back(-4.0);
  vecName.push_back("KEK20"); vecNTC20.push_back(); vecNTC10.push_back(15.9); vecNTCmin.push_back(-4.5);
  vecName.push_back("KEK21"); vecNTC20.push_back(); vecNTC10.push_back(15.9); vecNTCmin.push_back(-5.6);

  
  vecName.push_back("Paris3"); vecNTC20.push_back(); vecNTC10.push_back(16.2); vecNTCmin.push_back(-5.3);
  vecName.push_back("Paris6"); vecNTC20.push_back(); vecNTC10.push_back(17.2); vecNTCmin.push_back(-2.7);
  vecName.push_back("Paris8"); vecNTC20.push_back(); vecNTC10.push_back(16.3); vecNTCmin.push_back(-3.9);
  vecName.push_back("Paris12"); vecNTC20.push_back(); vecNTC10.push_back(17.7); vecNTCmin.push_back(-3.8);
  vecName.push_back("Paris16"); vecNTC20.push_back(28.7); vecNTC10.push_back(19.2); vecNTCmin.push_back(-1.7);
  vecName.push_back("Paris17"); vecNTC20.push_back(26.5); vecNTC10.push_back(16.5); vecNTCmin.push_back(-4.7);
  
  vecName.push_back("Siegen1"); vecNTC20.push_back(24.4); vecNTC10.push_back(16.8); vecNTCmin.push_back(-3.4);
  vecName.push_back("Siegen2"); vecNTC20.push_back(28.3); vecNTC10.push_back(18.8); vecNTCmin.push_back(-2.6);
  vecName.push_back("Siegen3"); vecNTC20.push_back(26.7); vecNTC10.push_back(17.1); vecNTCmin.push_back(-4.2);
  vecName.push_back("Siegen4"); vecNTC20.push_back(26.3); vecNTC10.push_back(16.3); vecNTCmin.push_back(-3.9);


  line[0]=new TLine(vecName.size()+0.5,-15,vecName.size()+0.5,30);
  vecName.push_back(""); vecNTC20.push_back(-99); vecNTC10.push_back(-99); vecNTCmin.push_back(-99);
  //3FE
  vecName.push_back("CERN10"); vecNTC20.push_back(25.7); vecNTC10.push_back(15.9); vecNTCmin.push_back(-5.1);
  vecName.push_back("Goe5"); vecNTC20.push_back(26.9); vecNTC10.push_back(17.1); vecNTCmin.push_back(-4.6);
  vecName.push_back("Paris7"); vecNTC20.push_back(26.3); vecNTC10.push_back(16.6); vecNTCmin.push_back(-4.8);
  vecName.push_back("Paris9"); vecNTC20.push_back(30.0); vecNTC10.push_back(20.0); vecNTCmin.push_back(-1.3);
  vecName.push_back("Paris10"); vecNTC20.push_back(26.5); vecNTC10.push_back(16.6); vecNTCmin.push_back(-3.7);
  vecName.push_back("Paris11"); vecNTC20.push_back(26.7); vecNTC10.push_back(16.8); vecNTCmin.push_back(-4.6);
  vecName.push_back("Paris13"); vecNTC20.push_back(26.3); vecNTC10.push_back(16.8); vecNTCmin.push_back(-4.7);
  vecName.push_back("Paris18"); vecNTC20.push_back(26.3); vecNTC10.push_back(16.6); vecNTCmin.push_back(-5.1);
  

  line[1]=new TLine(vecName.size()+0.5,-15,vecName.size()+0.5,30);
    vecName.push_back(""); vecNTC20.push_back(-99); vecNTC10.push_back(-99); vecNTCmin.push_back(-99);
  //2FE
  vecName.push_back("CERN8"); vecNTC20.push_back(27.9); vecNTC10.push_back(18.5); vecNTCmin.push_back(-2.0);  
  vecName.push_back(""); vecNTC20.push_back(-99); vecNTC10.push_back(-99); vecNTCmin.push_back(-99);
  */


  
  int numModule = vecName.size();
  cout<<"check"<<endl;  
  double Temp20[38], Temp10[38], Tempmin[38];
  double NTC20[38], NTC10[38], NTCmin[38];
  double onlyTemp20[38], onlyTemp10[38], onlyTempmin[38];
  double onlyErrTemp20[38], onlyErrTemp10[38], onlyErrTempmin[38];
  double errTemp20[38], errTemp10[38], errTempmin[38];
  string module[38];
  double xAxis[38];
  double exAxis[38];
  cout<<numModule<<endl;  
  for(int ii=0;ii<numModule;ii++){
    Temp20[ii]=vecTemp20[ii];
    Temp10[ii]=vecTemp10[ii];
    Tempmin[ii]=vecTempmin[ii];

    errTemp20[ii]=eTemp20[ii];
    errTemp10[ii]=eTemp10[ii];
    errTempmin[ii]=eTempmin[ii];
	    
    module[ii]=vecName[ii];
    xAxis[ii]=ii+0.5;
    exAxis[ii]=0;
  }

  for(int ii=0;ii<34;ii++){
    NTC20[ii]=vecNTC20[ii];
    NTC10[ii]=vecNTC10[ii];
    NTCmin[ii]=vecNTCmin[ii];
    
    onlyTemp20[ii]=vecOnlyTemp20[ii];
    onlyTemp10[ii]=vecOnlyTemp10[ii];
    onlyTempmin[ii]=vecOnlyTempmin[ii];

    onlyErrTemp20[ii]=vecErrOnlyTemp20[ii];
    onlyErrTemp10[ii]=vecErrOnlyTemp10[ii];
    onlyErrTempmin[ii]=vecErrOnlyTempmin[ii];
  }
  cout<<"Jig20:"<<endl;
  calMeanSigma(onlyTemp20);
  cout<<"Jig10:"<<endl;
  calMeanSigma(onlyTemp10);
  cout<<"Jig-10:"<<endl;
  calMeanSigma(onlyTempmin);
  //  cout<<"Check"<<endl;
  //  double xAxis[10]={0.5, 1.5, 2.5, 3.5, 4.5, 5.5, 6.5, 7.5, 8.5, 9.5};
  
  THStack *hs = new THStack("hs","test stacked histograms");

  TGraph *gr[3];
  gr[0]=new TGraphErrors(numModule,xAxis,Temp20,exAxis,errTemp20);
  gr[1]=new TGraphErrors(numModule,xAxis,Temp10,exAxis,errTemp10);
  gr[2]=new TGraphErrors(numModule,xAxis,Tempmin,exAxis,errTempmin);


  for(int ii=0;ii<numModule;ii++){
    gr[0]->GetXaxis()->SetBinLabel(2.5*ii+2.0, module[ii].c_str());
  }
  
  gr[0]->GetYaxis()->SetRangeUser(-15,50);
  gr[0]->SetMarkerStyle(20);
  gr[1]->SetMarkerStyle(21);
  gr[2]->SetMarkerStyle(22);

  gr[0]->SetMarkerColor(2);
  gr[1]->SetMarkerColor(4);
  gr[2]->SetMarkerColor(1);
  gr[0]->SetTitle("");
  gr[0]->GetYaxis()->SetTitle("ADC temperature sensor [#circC]");
  gr[0]->GetXaxis()->SetLabelSize(0.05);  
  gr[0]->Draw("ap");
  gr[1]->Draw("psame");
  gr[2]->Draw("psame");
  TLine *tick;
  for(int ii=0;ii<numModule;ii++){
    tick=new TLine(ii+0.5,-15,ii+0.5,-13.5);
    tick->Draw();
  }

  line[0]->Draw();
  line[1]->Draw();
  //  TLegend* leg = new TLegend(0.12,0.4,0.3,0.57);
  TLegend* leg = new TLegend(0.12,0.7,0.32,0.88);
  leg->AddEntry(gr[0],"Jig 20C","p");
  leg->AddEntry(gr[1],"Jig 10C","p");
  leg->AddEntry(gr[2],"Jig -10C","p");

  leg->Draw();

   TLatex latex(2,220,"All FEs working");
  latex.DrawLatex(12, 44, "All FEs working");
  //  latex.DrawLatex(2, 44, "All FEs working");
  latex.DrawLatex(26, 44, "3 FEs");
  latex.DrawLatex(35, 44, "2 FEs");

  //  c1->SaveAs("leak_temp.pdf");
  c1->SaveAs("adc_temp.pdf");


  TF1 *f1 = new TF1("","x",-10, 40);
  TGraphErrors *eGr[3];
  eGr[0]=new TGraphErrors(34, onlyTemp20, NTC20, onlyErrTemp20, exAxis);
  eGr[1]=new TGraphErrors(34, onlyTemp10, NTC10, onlyErrTemp10, exAxis);
  eGr[2]=new TGraphErrors(34, onlyTempmin, NTCmin, onlyErrTempmin, exAxis);

  eGr[0]->SetTitle("Module temperature (Cooling Jig = 20#circC)");
  eGr[1]->SetTitle("Module temperature (Cooling Jig = 10#circC)");
  eGr[2]->SetTitle("Module temperature (Cooling Jig = -10#circC)");
  for(int ii=0;ii<3;ii++){
    eGr[ii]->GetXaxis()->SetTitle("NMOS sensor temperature [#circC]");
    eGr[ii]->GetYaxis()->SetTitle("NTC on module flex temperature [#circC]");
  }
  //  eGr[0]->GetXaxis()->SetRangeUser(23,40);
  eGr[0]->GetXaxis()->SetLimits(23,33);      
  eGr[0]->GetYaxis()->SetRangeUser(23,33);
  eGr[0]->GetXaxis()->SetTitleSize(0.05);
  eGr[0]->GetXaxis()->SetLabelSize(0.05);
  eGr[0]->GetYaxis()->SetTitleSize(0.05);
  eGr[0]->GetYaxis()->SetLabelSize(0.05);

  eGr[0]->SetMarkerStyle(20);
  eGr[0]->SetMarkerColor(1);

  //  eGr[1]->GetXaxis()->SetRangeUser(14,30);      
  eGr[1]->GetXaxis()->SetLimits(14,23);
  eGr[1]->GetYaxis()->SetRangeUser(14,23);
  eGr[1]->GetXaxis()->SetTitleSize(0.05);
  eGr[1]->GetXaxis()->SetLabelSize(0.05);
  eGr[1]->GetYaxis()->SetTitleSize(0.05);
  eGr[1]->GetYaxis()->SetLabelSize(0.05);

  eGr[1]->SetMarkerStyle(21);
  eGr[1]->SetMarkerColor(2);

  eGr[2]->GetXaxis()->SetRangeUser(-8,4);
  eGr[2]->GetYaxis()->SetRangeUser(-8,4);
  eGr[2]->SetMarkerStyle(22);
  eGr[2]->SetMarkerColor(3);
gPad->SetGrid(1, 1); gPad->Update();
  
  eGr[0]->Draw("ap");
  f1->Draw("same");
  c1->SaveAs("20C.pdf");

  TCanvas *c2=new TCanvas("c1","",600,400);
  eGr[1]->Draw("ap");
   f1->Draw("same");
   gPad->SetGrid(1, 1); gPad->Update();
  c2->SaveAs("10C.pdf");

  TCanvas *c3=new TCanvas("c1","",600,400);  
  eGr[2]->Draw("ap");
  f1->Draw("same");
  c3->SaveAs("min10C.pdf");
  
  /*
    TLegend* lg[3];
  for(int ii=0;ii<3;ii++){
    lg[ii]= new TLegend(0.2,0.65,0.4,0.85);
    lg[ii]->SetBorderSize(0);
    lg[ii]->SetFillColor(0);
  }

  lg[0]->SetHeader("All FEs working");
  lg[1]->SetHeader("3 FEs");
  lg[2]->SetHeader("2 FEs");
  
  for(int ii=0;ii<3;ii++){
    lg[ii]->Draw();
  }
  */
  //  c1->SaveAs("hoge.pdf");
  
  
  TH1F *hist[3];
  hist[0]=new TH1F("","",5,0,5);
  hist[1]=new TH1F("","",5,0,5);
  hist[2]=new TH1F("","",5,0,5);


  for(int ii=0;ii<5;ii++){
    hist[0]->Fill(xAxis[ii], Temp20[ii]);
    hist[1]->Fill(xAxis[ii], Temp10[ii]);
    hist[2]->Fill(xAxis[ii], Tempmin[ii]);
	    
  }
  hist[0]->SetFillColor(2);
  hist[1]->SetFillColor(3);
  hist[2]->SetFillColor(4);

  hs->Add(hist[0]);
  hs->Add(hist[1]);
  hs->Add(hist[2]);
  hist[0]->Draw("");
  
  //  hs->Draw();
}
