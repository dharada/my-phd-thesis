#include <fstream>
#include <nlohmann/json.hpp>
#include <iostream>
#include <TCanvas.h>
#include <TGraph.h>
#include <TGraphErrors.h>
#include <TF1.h>
#include <TLegend.h>
#include <TAxis.h>
using namespace std;

#include "SaveTGraph.cpp"

// for convenience                                                                                                              
using json = nlohmann::json;

int main(){
  TCanvas *c1=new TCanvas("","",600,400);
  ifstream i("module.json");

      const int numOfModule=9+7+12+4+5;
  //number of module, CERN:9, KEK:7, Paris:12, Siegen:4, Goe:5

  //    const int numOfModule=3;
  //number of module, unige:17, CPPM:15, KEK:3, unknown:1:
    
  double data20[numOfModule], data10[numOfModule], dataMin[numOfModule];
  double curr20[numOfModule], curr10[numOfModule], currMin[numOfModule];
  double ecurr20[numOfModule], ecurr10[numOfModule], ecurrMin[numOfModule];

  
  json j;
  i >> j;
  bool isFirstEvent=true;

  double xAxis[numOfModule];
  double yAxis[7];

  double plotX[5][7];
  //set the data
  vector <string> vec_ModuleName;
  vector <double> vec_JigTemp[7];
  vector <double> vec_ModuleNTC[7];
  vector <double> vec_Current[7];
  vector <double> vec_eCurrent[7];
  vector <double> vec_Camera[7];
  vector <double> vec_Sensor[7];

  for(int ii=0;ii<37;ii++){

    //  if(j["Module"][ii]["LoadingSite"]=="KEK"){
    if(1){
      cout<<j["Module"][ii]["ModuleName"]<<endl;
    vec_ModuleName.push_back(j["Module"][ii]["ModuleName"]);

    for(int jj=0;jj<7;jj++){

      yAxis[jj]=j["Module"][ii]["ModuleNTC"][jj];
      plotX[0][jj]=j["Module"][ii]["ModuleNTC"][jj];
      plotX[1][jj]=j["Module"][ii]["Current"][jj];
      plotX[2][jj]=j["Module"][ii]["eCurrent"][jj];
      plotX[3][jj]=j["Module"][ii]["Camera"][jj];
      plotX[4][jj]=j["Module"][ii]["Sensor"][jj];
      
      
      vec_JigTemp[jj].push_back(j["Module"][ii]["JigTemperature"][jj]);
      vec_ModuleNTC[jj].push_back(j["Module"][ii]["ModuleNTC"][jj]);
      vec_Current[jj].push_back(j["Module"][ii]["Current"][jj]);
      vec_eCurrent[jj].push_back(j["Module"][ii]["eCurrent"][jj]);
      vec_Camera[jj].push_back(j["Module"][ii]["Camera"][jj]);
      vec_Sensor[jj].push_back(j["Module"][ii]["Sensor"][jj]);
    }
    }
    makePlot(j["Module"][ii]["ModuleName"], plotX[0], plotX[1], plotX[2], plotX[3], plotX[4]);
	
  }
  TCanvas *c2=new TCanvas("","",600,400);
  TGraph *gr[9];

  TGraphErrors *grCurr[3];
  /*  gr[0]->Draw();
  for(int ii=1;ii<7;ii++){
    gr[ii]->Draw("same");
  }
  */
  cout<<"check1"<<endl;
  for(int ii=0;ii<numOfModule;ii++){
      cout<<"check2"<<endl;
    xAxis[ii]=ii+0.5;
    data20[ii]=vec_ModuleNTC[1][ii]-vec_JigTemp[1][ii];
    data10[ii]=vec_ModuleNTC[3][ii]-vec_JigTemp[3][ii];
    dataMin[ii]=vec_ModuleNTC[5][ii]-vec_JigTemp[5][ii];

    curr20[ii]=vec_Current[1][ii]-vec_JigTemp[1][ii];
    curr10[ii]=vec_Current[3][ii]-vec_JigTemp[3][ii];
    currMin[ii]=vec_Current[5][ii]-vec_JigTemp[5][ii];

    ecurr20[ii]=vec_eCurrent[1][ii];
    ecurr10[ii]=vec_eCurrent[3][ii];
    ecurrMin[ii]=vec_eCurrent[5][ii];

    
  }
  gr[0]=new TGraph(numOfModule, xAxis, data20);
  gr[1]=new TGraph(numOfModule, xAxis, data10);
  gr[2]=new TGraph(numOfModule, xAxis, dataMin);
  cout<<"check3"<<endl;

  for(int ii=0;ii<numOfModule;ii++){
    gr[0]->GetXaxis()->SetBinLabel(2.5*ii+2.0, vec_ModuleName[ii].c_str());

  }
  gr[0]->SetMarkerStyle(21);
  gr[1]->SetMarkerStyle(20);
  gr[2]->SetMarkerStyle(22);
  gr[0]->SetMarkerColor(2);
  gr[1]->SetMarkerColor(4);
  gr[2]->SetMarkerColor(1);
  gr[0]->SetTitle("Module NTC of each Jig temperature");
  gr[0]->GetYaxis()->SetTitle("Module NTC - Jig Temp [C]");
  gr[0]->GetXaxis()->SetLabelSize(0.05);

  gr[0]->Draw("ap");
  gr[1]->Draw("psame");
  gr[2]->Draw("psame");

  double error[numOfModule];
  grCurr[0]=new TGraphErrors(numOfModule, xAxis, curr20, error, error);
  grCurr[1]=new TGraphErrors(numOfModule, xAxis, curr10, error, error);
  grCurr[2]=new TGraphErrors(numOfModule, xAxis, currMin, error, error);

  grCurr[0]->SetMarkerStyle(25);
  grCurr[1]->SetMarkerStyle(24);
  grCurr[2]->SetMarkerStyle(26);
  grCurr[0]->SetMarkerColor(2);
  grCurr[1]->SetMarkerColor(4);
  grCurr[2]->SetMarkerColor(1);

  grCurr[0]->Draw("psame");
  grCurr[1]->Draw("psame");
  grCurr[2]->Draw("psame");
  TLegend* leg = new TLegend(0.6,0.65,0.85,0.85);
  leg->AddEntry(gr[0],"Jig 20C","p");
  leg->AddEntry(gr[1],"Jig 10C","p");
  leg->AddEntry(gr[2],"Jig -10C","p");

  leg->Draw();



  c2->SaveAs("jsontest.pdf");
  //  for(int ii=0;ii<9;ii++){
  //    gr[ii]=new TGraph(7,
}
