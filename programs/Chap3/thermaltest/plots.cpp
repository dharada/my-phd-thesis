//CERNQ11

{
  double Jigon[3]={-10.7, 10.7, 20.6};
  double Jigoff[4]={-10.5, 10.8, 20.5, 23.9};
  
  double NTCon[3]={-5.4, 15.9, 25.5};
  double NTCoff[4]={-10.0, 11.1, 20.5, 24.0};

  double Error3[3]={0,0,0};
  double Error4[4]={0,0,0,0};

  double Curron[3]={-5.5, 16.0, 25.9};
  double Curroff[4]={-10.4, 10.7, 20.5, 24.0};
  
  double CErron[3]={0.7, 0.2, 0.2};
  double CErroff[4]={1.2, 0.3, 0.2, 0.0};

  double Sens31[3]={-10.2, 12.2, 24.1};

  //  double Cameraon31[3]={-2.1, 17.6, 26.5};
  //  double Cameraoff31[4]={-8.2, 11.4, 21.2, 23.8};

  double Cameraon31[3]={-2.5, 17.4, 26.0};
  double Cameraoff31[4]={-6.9, 11.7, 20.5, 23.5};

  TGraphErrors *graph[6];
  TGraphErrors *graphOff[2];
  graph[0]= new TGraphErrors(4,NTCoff,Curroff,Error4,CErroff);
  graph[1]= new TGraphErrors(4,NTCoff,Cameraoff31,Error4,Error4);

  graph[2]= new TGraphErrors(3,NTCon,Curron,Error3,CErron);
  graph[3]= new TGraphErrors(3,NTCon,Cameraon31,Error3,Error3);
  graph[4]= new TGraphErrors(3,NTCon,Sens31,Error3,Error3);

  //Define Marker Style//
  graph[0]->SetMarkerStyle(20);
  graph[2]->SetMarkerStyle(20);

  graph[1]->SetMarkerStyle(21);
  graph[3]->SetMarkerStyle(21);
  
  graph[4]->SetMarkerStyle(22);

  //Define Marker Color//
  graph[0]->SetMarkerColor(1);
  graph[1]->SetMarkerColor(1);

  graph[2]->SetMarkerColor(2);
  graph[3]->SetMarkerColor(2);

  graph[4]->SetMarkerColor(2);

  graph[0]->SetTitle("CERNQ11");
  graph[0]->GetXaxis()->SetTitle("NTC temperature [C]");
  graph[0]->GetYaxis()->SetTitle("Temperature [C]");

  graph[0]->Draw("ap");
  graph[1]->Draw("psame");
  graph[2]->Draw("psame");
  graph[3]->Draw("psame");
  graph[4]->Draw("psame");

  TF1 *f = new TF1("","x",-15,40);
  f->Draw("same");
  f->SetLineColor(1);
  TLegend* leg = new TLegend(0.6,0.65,0.85,0.85);
  leg->SetHeader("Measurements method (Minimum value)","C"); // option "C" allows to center the header

  leg->AddEntry(graph[0],"Leakage Current, Power Off (-10.4C)","p");
  leg->AddEntry(graph[2],"Leakage Current, Power On (-5.4C)","p");
  
  leg->AddEntry(graph[1],"Thermal Camera, Power Off (-2.5C)","p");
  leg->AddEntry(graph[3],"Thermal Camera, Power On (-6.9C)","p");

  leg->AddEntry(graph[4],"Sensor in Chip, Power On (-10.2C)","p");

  leg->AddEntry(f,"Yaxis = Xaxis","l");
  

  leg->Draw("same");
}

  
    
