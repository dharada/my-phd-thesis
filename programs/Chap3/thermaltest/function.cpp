class moduleData{
private:
  string moduleName;
  vector <double> TCcycle;
  vector <vector <double> >moduleNTC;
  vector <vector <double> >jigTemperature;
  vector <vector <double> >currTemp;


public:
  void setModuleName(string setName){
    moduleName = setName;
  }
  void setTCcycles(nlohmann::json j){
    for(int ii=0;ii<<j["ThermalTest"].size();ii++){
      TCcycle.push_back(j["ThermalTest"][ii]["Cycle"]);
    }
  }
  void setModuleNTC(nlohmann::json j){
    for(int TC=0;TC<TCcycle.size();TC++){
      vector <double> willBePushed;
      for(int ii=0;ii<7;ii++){
	willBePushed.push_back(j["ThermalTest"][ii]["ModuleNTC"][ii]);
      }
      moduleNTC.push_back(willBePushed);
      while(willBePushed.size()>0){
	willBePushed.erase(willBePushed.begin());
      }
    }
  }

  void setJigTemp(nlohmann::json j){
    for(int TC=0;TC<TCcycle.size();TC++){
      vector <double> willBePushed;
      for(int ii=0;ii<7;ii++){
	willBePushed.push_back(j["ThermalTest"][ii]["jigTemperature"][ii]);
      }
      jigTemperature.push_back(willBePushed);
      while(willBePushed.size()>0){
	willBePushed.erase(willBePushed.begin());
      }
    }
  }

  void setCurrTemp(nlohmann::json j){
    for(int TC=0;TC<TCcycle.size();TC++){
      vector <double> willBePushed;
      for(int ii=0;ii<7;ii++){
	willBePushed.push_back(j["ThermalTest"][ii]["Current"][ii]);
      }
      currTemp.push_back(willBePushed);
      while(willBePushed.size()>0){
	willBePushed.erase(willBePushed.begin());
      }
    }
  }


};

  

int numOfModule=9+7+12+4+5;


double retMax(double NTC, double Curr, double Temp[16][37], double Camera[16][37], double maximum, int thisModule, bool include){
  int min=-10;
  if(NTC>maximum&&NTC>min){maximum=NTC;}
  if(Curr>maximum&&Curr>min){maximum=Curr;}
  if(include){
    for(int ii=0;ii<16;ii++){
      if(Temp[ii][thisModule]>maximum&&Temp[ii][thisModule]>min){maximum=Temp[ii][thisModule];}
      if(Camera[ii][thisModule]>maximum&&Camera[ii][thisModule]>min){maximum=Camera[ii][thisModule];}
    }
  }
  return maximum;
}


double retMin(double NTC, double Curr, double Temp[16][37], double Camera[16][37], double minimum, int thisModule, bool include){
  int min=-10;
  if(NTC<minimum&&NTC>min){minimum=NTC;}
  if(Curr<minimum&&Curr>min){minimum=Curr;}
  if(include){
    for(int ii=0;ii<16;ii++){
      if(Temp[ii][thisModule]<minimum&&Temp[ii][thisModule]>min){minimum=Temp[ii][thisModule];}
      if(Camera[ii][thisModule]<minimum&&Camera[ii][thisModule]>min){minimum=Camera[ii][thisModule];}
    }
  }
  return minimum;
}

tuple<double, double> retMeanSigma(double data[37],string tempType){
  double mean=0;
  double sigma=0;
  int count=0;
  for(int ii=0;ii<37;ii++){
    if(data[ii]>-0){
      count++;
    }
  }
  for(int ii=0;ii<37;ii++){
    if(data[ii]>-0){
      mean+=data[ii]/count;
    }
  }
  for(int ii=0;ii<37;ii++){
    if(data[ii]>-0){
      sigma+=pow(mean-data[ii],2)/(count-1);
    }
  }
  sigma=sqrt(sigma);
  cout<<tempType<<endl;
  cout<<"Mean: "<<mean<<", Sigma: "<<sigma<<endl;
  return forward_as_tuple(mean, sigma);
}

tuple<double, double> retMeanSigma2(double data[16][37],string tempType){
  double mean=0;
  double sigma=0;
  int count=0;
  for(int ii=0;ii<37;ii++){
    for(int jj=0;jj<16;jj++){
      if(data[jj][ii]>-30){
	count++;
      }
    }
  }
  for(int ii=0;ii<37;ii++){
    for(int jj=0;jj<16;jj++){
      if(data[jj][ii]>-30){
	mean+=data[jj][ii]/count;
      }
    }
  }
  for(int ii=0;ii<37;ii++){
    for(int jj=0;jj<16;jj++){
      if(data[jj][ii]>-30){
	sigma+=pow(mean-data[jj][ii],2)/(count-1);
      }
    }
  }
  sigma=sqrt(sigma);
  cout<<tempType<<endl;
  cout<<"Mean: "<<mean<<", Sigma: "<<sigma<<endl;
  return forward_as_tuple(mean, sigma);
}
