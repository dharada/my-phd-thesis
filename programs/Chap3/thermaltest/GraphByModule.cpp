int modulePlot(string moduleName, double moduleNTC[7], double Current[7], double jigTemp[7], double tempsens[3][16], double cameratemp[3][16]){

  string sensId[16]={"Sens1.1", "Sens1.2", "Sens1.3", "Sens1.4", "Sens2.1", "Sens2.2", "Sens2.3", "Sens2.4", "Sens3.1", "Sens3.2", "Sens3.3", "Sens3.4", "Sens4.1", "Sens4.2", "Sens4.3", "Sens4.4"};
  double xAxis[16]={1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16};
  TCanvas *c1 = new TCanvas("","",600,400);
  TGraph *gr[2][3];
  string saveFile;
  string plotTitle;
  for(int roop=0;roop<2;roop++){

    if(roop==1){
      for(int jj=0;jj<16;jj++){
	tempsens[0][jj]=tempsens[0][jj]-jigTemp[1];
	tempsens[1][jj]=tempsens[1][jj]-jigTemp[3];
	tempsens[2][jj]=tempsens[2][jj]-jigTemp[5];

	cameratemp[0][jj]=cameratemp[0][jj]-jigTemp[1];
	cameratemp[1][jj]=cameratemp[1][jj]-jigTemp[3];
	cameratemp[2][jj]=cameratemp[2][jj]-jigTemp[5];
      }
    }
    double highest=-99, lowest=100;
  for(int ii=0;ii<3;ii++){
    for(int jj=0;jj<16;jj++){
      if(tempsens[ii][jj]>=highest && tempsens[ii][jj]>=-50){highest=tempsens[ii][jj];}
      if(cameratemp[ii][jj]>=highest && cameratemp[ii][jj]>=-50){highest=cameratemp[ii][jj];}

      if(tempsens[ii][jj]<=lowest && tempsens[ii][jj]>=-50){lowest=tempsens[ii][jj];}
      if(cameratemp[ii][jj]<=lowest && cameratemp[ii][jj]>=-50){lowest=cameratemp[ii][jj];}
    }
  }      
  for(int ii=0;ii<3;ii++){
    gr[0][ii]=new TGraph(16,xAxis,tempsens[ii]);
    gr[1][ii] =new TGraph(16,xAxis,cameratemp[ii]);
    
  }
  gr[0][0]->SetMarkerStyle(21);
  gr[0][1]->SetMarkerStyle(20);
  gr[0][2]->SetMarkerStyle(22);
  gr[1][0]->SetMarkerStyle(25);
  gr[1][1]->SetMarkerStyle(24);
  gr[1][2]->SetMarkerStyle(26);

  gr[0][0]->SetMarkerColor(2);
  gr[0][1]->SetMarkerColor(4);
  gr[0][2]->SetMarkerColor(1);
  gr[1][0]->SetMarkerColor(2);
  gr[1][1]->SetMarkerColor(4);
  gr[1][2]->SetMarkerColor(1);

  for(int ii=0;ii<16;ii++){
    gr[0][0]->GetXaxis()->SetBinLabel(5.8*ii+5.0, sensId[ii].c_str());
  }
  gr[0][0]->GetYaxis()->SetRangeUser(lowest-(highest-lowest)/10, highest+(highest-lowest)/4);
  if(roop==0){plotTitle = moduleName+" TempSens and Camera, temperature distribution";
  }else if(roop==1){plotTitle = moduleName+" TempSens and Camera, temperature difference from Jig";}
  gr[0][0]->SetTitle(plotTitle.c_str());
  if(roop==0){gr[0][0]->GetYaxis()->SetTitle("Temperature [C]");
  }else if(roop==1){gr[0][0]->GetYaxis()->SetTitle("Temperature differences from Jig[C]");}
  gr[0][0]->Draw("ap");
  gr[0][1]->Draw("psame");
  gr[0][2]->Draw("psame");
  gr[1][0]->Draw("psame");
  gr[1][1]->Draw("psame");
  gr[1][2]->Draw("psame");

  TLine *l[3];
  if(roop==0){
    l[0]=new TLine(0, moduleNTC[1], 17.5, moduleNTC[1]);
    l[1]=new TLine(0, moduleNTC[3], 17.5, moduleNTC[3]);
    l[2]=new TLine(0, moduleNTC[5], 17.5, moduleNTC[5]);
  }else if(roop==1){
    l[0]=new TLine(0, moduleNTC[1]-jigTemp[1], 17.5, moduleNTC[1]-jigTemp[1]);
    l[1]=new TLine(0, moduleNTC[3]-jigTemp[3], 17.5, moduleNTC[3]-jigTemp[3]);
    l[2]=new TLine(0, moduleNTC[5]-jigTemp[5], 17.5, moduleNTC[5]-jigTemp[5]);
  }

  l[0]->SetLineColor(2);
  l[1]->SetLineColor(4);
  l[2]->SetLineColor(1);
  for(int ii=0;ii<3;ii++){
    l[ii]->Draw("same");
  }
  TLegend* leg1 = new TLegend(0.12,0.78,0.35,0.9);
  leg1->AddEntry(l[0],"ModuleNTC (at Jig  20C)","l");
  leg1->AddEntry(l[1],"ModuleNTC (at Jig  10C)","l");
  leg1->AddEntry(l[2],"ModuleNTC (at Jig -10C)","l");
  leg1->Draw();

  TLegend* leg2 = new TLegend(0.4,0.78,0.6,0.9);
  leg2->AddEntry(gr[0][0],"Sensor (at Jig  20C)","p");
  leg2->AddEntry(gr[0][1],"Sensor (at Jig  10C)","p");
  leg2->AddEntry(gr[0][2],"Sensor (at Jig -10C)","p");
  leg2->Draw();

  TLegend* leg3 = new TLegend(0.65,0.78,0.88,0.9);
  leg3->AddEntry(gr[1][0],"Camera (at Jig 20C)","p");
  leg3->AddEntry(gr[1][1],"Camera (at Jig 10C)","p");
  leg3->AddEntry(gr[1][2],"Camera (at Jig -10C)","p");
  leg3->Draw();

  if(roop==0){saveFile = "ModulePlots/SensandCamera/"+moduleName+"_original.pdf";}
  else if(roop==1){saveFile = "ModulePlots/SensandCamera/"+moduleName+"_diff.pdf";}
  
  c1->SaveAs(saveFile.c_str());
  c1->Delete();
}
}
