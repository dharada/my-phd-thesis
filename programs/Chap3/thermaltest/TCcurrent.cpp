#include <fstream>
#include <nlohmann/json.hpp>
#include <iostream>
#include <TCanvas.h>
#include <TStyle.h>
#include <TGraph.h>
#include <TGraphErrors.h>
#include <TF1.h>
#include <TLegend.h>
#include <TAxis.h>
#include <TLine.h>
using namespace std;
using json = nlohmann::json;

class moduleInfo{
public:
  string moduleName;
  double temperature[3];
  double leak[3];
  double eLeak[3];
  double ratio[3];
  void correction(){
    double Eg=1.17;
    double alpha=4.73*0.0001;
    double beta=636;
    double kB=8.617 * pow(10,-5);

    for(int ii=0;ii<3;ii++){
      double T1=temperature[ii]+273.2;
      double T2=20+273.2;
      ratio[ii]=pow((T2/T1),2)*exp((-(Eg-(alpha*T2*T2)/(T2+beta))/(2*kB))*(1./T2)-(Eg-(alpha*T1*T1)/(T1+beta))/(2*kB)*(-1./T1));
    }
  }  

};

int main(){
  TCanvas *c1 = new TCanvas("","",600,450);
  c1->SetBottomMargin(0.25);
  c1->SetLeftMargin(0.15);
  c1->SetRightMargin(0.05);
  
  gStyle->SetTitleW(0.8);
  ifstream i("TCmodule.json");
  json j;
  i >> j;

  moduleInfo module[7];
  string bias[7]={"90", "90", "90", "50", "90", "90", "90"};
  for(int ii=0;ii<7;ii++){
    module[ii].moduleName=j["Module"][ii]["ModuleName"];
    for(int jj=0;jj<3;jj++){
      module[ii].temperature[jj]=j["Module"][ii]["ThermalTest"][jj]["ModuleNTC"][0];
      module[ii].leak[jj]=j["Module"][ii]["ThermalTest"][jj]["Leak"][0];
      module[ii].eLeak[jj]=j["Module"][ii]["ThermalTest"][jj]["eLeak"][0];
    }
    module[ii].correction();
  }
  double xAxis[7];
  double exAxis[7];
  double yAxis[3][7];
  double eyAxis[3][7];
  
  for(int ii=0;ii<7;ii++){
    xAxis[ii]=ii+1;
    exAxis[ii]=0;
    yAxis[0][ii]=module[ii].leak[0]*module[ii].ratio[0];
    yAxis[1][ii]=module[ii].leak[1]*module[ii].ratio[1];
    yAxis[2][ii]=module[ii].leak[2]*module[ii].ratio[2];

    eyAxis[0][ii]=module[ii].eLeak[0]*module[ii].ratio[0];
    eyAxis[1][ii]=module[ii].eLeak[1]*module[ii].ratio[1];
    eyAxis[2][ii]=module[ii].eLeak[2]*module[ii].ratio[2];
  }

  TGraphErrors *gr[3];
  for(int ii=0;ii<3;ii++){
    gr[ii]=new TGraphErrors(7,xAxis,yAxis[ii],exAxis,eyAxis[ii]);
    gr[ii]->SetLineColor(ii+1);
    gr[ii]->SetMarkerColor(ii+1);
    gr[ii]->SetMarkerStyle(ii+20);
    gr[ii]->SetMarkerSize(1.5);
  }
  for(int ii=0;ii<7;ii++){
    //    string labelTitle="#splitline{"+module[ii].moduleName+"}{"+bias[ii]+"V}";
    string labelTitle=module[ii].moduleName;
    double dd=ii;
    gr[0]->GetXaxis()->SetBinLabel(14.*dd+8, labelTitle.c_str());
  }
  gr[0]->SetTitle("Leakage current of thermal cycled module, the currents are normalized at 20#circC");
  //  gr[0]->SetTitle("#splitline{Leakage current of thermal cycled module}{the currents are normalized at 20#circC}");
  gr[0]->GetYaxis()->SetTitle("Leakage current [nA]");
  gr[0]->GetYaxis()->SetRangeUser(200,800);
  gr[0]->GetXaxis()->SetRangeUser(0.5,8.5);
  gr[0]->GetXaxis()->SetTitleSize(0.05);
  gr[0]->GetXaxis()->SetLabelSize(0.06);
  gr[0]->GetYaxis()->SetTitleSize(0.05);
  gr[0]->GetYaxis()->SetLabelSize(0.05);
  gr[0]->Draw("ap");
  gr[1]->Draw("psame");
  gr[2]->Draw("psame");
  TLine *tick;
  for(int ii=1;ii<8;ii++){
    tick=new TLine(ii,200,ii,230);
    tick->Draw();
  }
  TLegend* leg = new TLegend(0.2,0.63,0.39,0.8);
  leg->AddEntry(gr[0],"Before TC","p");
  leg->AddEntry(gr[1],"After 50 cycles","p");
  leg->AddEntry(gr[2],"After 100 cycles","p");
  leg->Draw();
  c1->SaveAs("TCleak.pdf");
}
