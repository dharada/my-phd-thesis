#include <iostream>
#include <math.h>
#include <fstream>
#include <TStyle.h>
#include <TGraph.h>
#include <TCanvas.h>
#include <TH1D.h>
#include <TF1.h>
#include <TLegend.h>
using namespace std;

int timeStamp(int hour, int min, int sec){
  return hour*60*60+min*60+sec;
}

int main(){
   TCanvas *c1 = new TCanvas("","",600,400);
  TH1D *hist[7];
  for(int ii=0;ii<7;ii++){
    hist[ii]=new TH1D("","",881, -13, 31);
  }
  TF1 *f[7];
  for(int ii=0;ii<7;ii++){

  }

  string leakFiles[7]={"leak_room.txt", "leak_20_on.txt", "leak_20_off.txt", "leak_10_on.txt", "leak_10_off.txt", "leak_-10_on.txt", "leak_-10_off.txt"};

  int time[7][2]={{timeStamp(10,26,26),timeStamp(10,33,23)},
		  {timeStamp(11,35,58),timeStamp(11,41,37)},
		  {timeStamp(11,21,34),timeStamp(11,26,42)},
		  {timeStamp(12,52,26),timeStamp(12,58,01)},
		  {timeStamp(12,37,14),timeStamp(12,43,19)},
		  {timeStamp(13,37,05),timeStamp(13,42,22)},
		  {timeStamp(13,11,48),timeStamp(13,17,13)}};
  string hoge;
  int hour, min, sec;
  double temp;
  vector <double>v_temp[7];
  for(int ii=0;ii<7;ii++){
    ifstream tempFile("moduleNTC/ModuleNTC.txt");
    while(tempFile>>hoge>>hour>>min>>sec>>temp){
      //      cout<<hour<<" "<<min<<" "<<sec<<endl;
      if(timeStamp(hour,min,sec)>time[ii][0]&&timeStamp(hour,min,sec)<time[ii][1]){
	if(temp>=-100){
	  v_temp[ii].push_back(temp);
	  cout<<leakFiles[ii]<<endl;
	}
      }
    }
  }
  for(int ii=0;ii<7;ii++){
    f[ii]=new TF1("","gaus(0)",v_temp[ii][0]-0.5,v_temp[ii][0]+0.5);
    //    f[ii]->SetParameters(0.5, v_temp[ii][1], 0.1);
    for(int jj=0;jj<v_temp[ii].size();jj++){
      hist[ii]->Fill(v_temp[ii][jj], 1./v_temp[ii].size());
    }
    hist[ii]->SetFillColor(ii+1);
    hist[ii]->SetLineColor(ii+1);
    hist[ii]->Fit(f[ii]);
    f[ii]->SetLineWidth(2);
    f[ii]->SetLineColor(ii+1);
  }
  gStyle->SetOptStat(0);
  hist[0]->GetXaxis()->SetTitle("Module NTC temperature [#circC]");
  hist[0]->GetYaxis()->SetTitle("Normalized event");
  hist[0]->Draw("hist");
  f[0]->Draw("same");
  for(int ii=1;ii<7;ii++){
    hist[ii]->Draw("samehist");
    f[ii]->Draw("same");
  }

   TLegend* leg = new TLegend(0.15,0.6,0.4,0.85);
  leg->AddEntry(f[0],"roomtemp, LV off, 22.9C","l");
  leg->AddEntry(f[1],"Jig 20C, LV on, 27.9C","l");
  leg->AddEntry(f[2],"Jig 20C, LV off, 21.2C","l");
  leg->AddEntry(f[3],"Jig 10C, LV on, 17.8C","l");
  leg->AddEntry(f[4],"Jig 10C, LV off, 11.1C","l");
  leg->AddEntry(f[5],"Jig -10C, LV on, -3.0C","l");
  leg->AddEntry(f[6],"Jig -10C, LV off, -8.9C","l");

  leg->Draw();

  c1->SaveAs("temp_hist.pdf");
  
}
    

