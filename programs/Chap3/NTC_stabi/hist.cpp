
#include <iostream>
#include <math.h>
#include <fstream>
#include <TStyle.h>
#include <TGraph.h>
#include <TCanvas.h>
#include <TH1D.h>
#include <TF1.h>
#include <TLegend.h>
//double returnCurrent[4]{
using namespace std;
int hist(){


  TH1D * hist[7];
  hist[0]=new TH1D("","",641,-110,1590);
  hist[1]=new TH1D("","",641,-110,1590);
  hist[2]=new TH1D("","",321,-110,1590);
  hist[3]=new TH1D("","",321,-110,1590);
  hist[4]=new TH1D("","",641,-110,1590);
  hist[5]=new TH1D("","",641,-110,1590);
  hist[6]=new TH1D("","",641,-110,1590);

  //  char* off_Path_c="leakageCurrent/plus5/off/HV30.txt";
  //  char* off_Path_c="plus25/off/leakageCurrent.txt";
  //  char* on_Path_c="minus5C/leakageCurrent.txt";
  //  char* off_Path_c="minus15C/leakageCurrent.txt";
  ifstream offFile("leak_20_off.txt");
  ifstream onFile("leak_20_on.txt");

  ifstream offFile2("leak_10_off.txt");
  ifstream onFile2("leak_10_on.txt");

  ifstream offFile3("leak_-10_off.txt");
  ifstream onFile3("leak_-10_on.txt");

  ifstream offFile4("leak_room.txt");
  TF1 *f[7];
  f[0] = new TF1("","[0]*exp(-((x-[1])**2)/(2*([2]**2)))",5,10);
  f[1] = new TF1("","[0]*exp(-((x-[1])**2)/(2*([2]**2)))",5,10);
  f[2] = new TF1("","[0]*exp(-((x-[1])**2)/(2*([2]**2)))",5,10);
  f[3] = new TF1("","[0]*exp(-((x-[1])**2)/(2*([2]**2)))",5,10);
  f[4] = new TF1("","[0]*exp(-((x-[1])**2)/(2*([2]**2)))",5,10);
  f[5] = new TF1("","[0]*exp(-((x-[1])**2)/(2*([2]**2)))",5,10);
  f[6] = new TF1("","[0]*exp(-((x-[1])**2)/(2*([2]**2)))",5,10);
  
  string hoge;
  double current;
  int count=0;
  vector<double> leakCurr[7];
  while(offFile>>hoge>>hoge>>hoge>>hoge>>hoge>>hoge>>hoge>>hoge>>current>>hoge>>hoge>>hoge>>hoge>>hoge>>hoge>>hoge>>hoge){
    count++;
    if(count>=00){leakCurr[0].push_back(-1*current*(pow(10,9)));}
  }
  count=0;
  while(onFile>>hoge>>hoge>>hoge>>hoge>>hoge>>hoge>>hoge>>hoge>>current>>hoge>>hoge>>hoge>>hoge>>hoge>>hoge>>hoge>>hoge){
    count++;
    if(count>=00){leakCurr[1].push_back(-1*current*(pow(10,9)));}
  }
  count=0;
  while(offFile2>>hoge>>hoge>>hoge>>hoge>>hoge>>hoge>>hoge>>hoge>>current>>hoge>>hoge>>hoge>>hoge>>hoge>>hoge>>hoge>>hoge){
    count++;
    if(count>=00){leakCurr[2].push_back(-1*current*(pow(10,9)));}
  }
  count=0;
  while(onFile2>>hoge>>hoge>>hoge>>hoge>>hoge>>hoge>>hoge>>hoge>>current>>hoge>>hoge>>hoge>>hoge>>hoge>>hoge>>hoge>>hoge){
    count++;
    if(count>=00){leakCurr[3].push_back(-1*current*(pow(10,9)));}
  }
  while(offFile3>>hoge>>hoge>>hoge>>hoge>>hoge>>hoge>>hoge>>hoge>>current>>hoge>>hoge>>hoge>>hoge>>hoge>>hoge>>hoge>>hoge){
    count++;
    if(count>=00){leakCurr[4].push_back(-1*current*(pow(10,9)));}
  }
  count=0;
  while(onFile3>>hoge>>hoge>>hoge>>hoge>>hoge>>hoge>>hoge>>hoge>>current>>hoge>>hoge>>hoge>>hoge>>hoge>>hoge>>hoge>>hoge){
    count++;
    if(count>=00){leakCurr[5].push_back(-1*current*(pow(10,9)));}
  }
  while(offFile4>>hoge>>hoge>>hoge>>hoge>>hoge>>hoge>>hoge>>hoge>>current>>hoge>>hoge>>hoge>>hoge>>hoge>>hoge>>hoge>>hoge){
    count++;
    if(count>=00){leakCurr[6].push_back(-1*current*(pow(10,9)));}
  }
  
  double xAxis[leakCurr[0].size()];
  double yAxis[leakCurr[0].size()];
  
  for(int ii=0;ii<leakCurr[0].size();ii++){
    hist[0]->Fill(leakCurr[0][ii],1./leakCurr[0].size());
    yAxis[ii]=leakCurr[0][ii];
    double ii2=ii;
    xAxis[ii]=ii2/60;
  }
  for(int ii=0;ii<leakCurr[1].size();ii++){
    hist[1]->Fill(leakCurr[1][ii],1./leakCurr[1].size());
  }
  for(int ii=0;ii<leakCurr[2].size();ii++){
    hist[2]->Fill(leakCurr[2][ii],1./leakCurr[2].size());
  }
  for(int ii=0;ii<leakCurr[3].size();ii++){
    hist[3]->Fill(leakCurr[3][ii],1./leakCurr[3].size());
  }
  for(int ii=0;ii<leakCurr[4].size();ii++){
    hist[4]->Fill(leakCurr[4][ii],1./leakCurr[4].size());
  }
  for(int ii=0;ii<leakCurr[5].size();ii++){
    hist[5]->Fill(leakCurr[5][ii],1./leakCurr[5].size());
  }
  for(int ii=0;ii<leakCurr[6].size();ii++){
    hist[6]->Fill(leakCurr[6][ii],1./leakCurr[6].size());
  }
  
  //f[0]->SetParameters(0.02,130,0.1);
  f[0]->SetParameters(0.1,leakCurr[0][leakCurr[0].size()-2],5);
  f[1]->SetParameters(0.1,leakCurr[1][leakCurr[1].size()-2],5);
  f[2]->SetParameters(0.1,leakCurr[2][leakCurr[2].size()-2],5);
  f[3]->SetParameters(0.1,leakCurr[3][leakCurr[3].size()-2],5);
  f[4]->SetParameters(0.1,leakCurr[4][leakCurr[4].size()-2],5);
  f[5]->SetParameters(0.1,leakCurr[5][leakCurr[5].size()-2],5);
  f[6]->SetParameters(0.1,leakCurr[6][leakCurr[6].size()-2],5);
  TGraph *gr= new TGraph(leakCurr[0].size(),xAxis,yAxis);
  hist[0]->GetXaxis()->SetTitle("Current [nA]");
  hist[0]->GetYaxis()->SetTitle("Event normalised");
  hist[0]->SetTitle("I_leak distribution HV=-90");

  for(int ii=0;ii<7;ii++){
    hist[ii]->SetLineColor(ii+1);
    f[ii]->SetLineColor(ii+1);
  }
  /*
  hist[0]->SetLineColor(1);
  hist[1]->SetLineColor(2);
  hist[2]->SetLineColor(3);
  hist[3]->SetLineColor(4);
  hist[4]->SetLineColor(5);
  hist[5]->SetLineColor(6);
  hist[6]->SetLineColor(7);

  f[0]->SetLineColor(1);
  f[1]->SetLineColor(2);
  f[2]->SetLineColor(3);
  f[3]->SetLineColor(4);
  f[4]->SetLineColor(5);
  f[5]->SetLineColor(6);
  f[6]->SetLineColor(7);
  */
  hist[0]->Draw("");
  hist[0]->Fit(f[0]);
  hist[0]->SetLineWidth(1);
  //  f[0]->Draw("same");
  /*  
  for(int ii=1;ii<7;ii++){
    hist[ii]->Draw("same");
    hist[ii]->Fit(f[0]);
    hist[ii]->SetLineWidth(1);
    f[ii]->Draw("same");
  }
  */
  /*
  hist[0]->Draw();
  hist[0]->Fit(f[0]);
  hist[0]->SetLineWidth(1);
  f[0]->Draw("same");
  
  hist[1]->SetLineWidth(1);
  hist[1]->Draw("same");
  hist[1]->Fit(f[1]);
  f[1]->Draw("same");
  
  hist[2]->SetLineWidth(1);
  hist[2]->Draw("same");
  hist[2]->Fit(f[2]);
  f[2]->Draw("same");

  hist[3]->SetLineWidth(1);
  hist[3]->Draw("same");
  hist[3]->Fit(f[3]);
  f[3]->Draw("same");

  hist[4]->SetLineWidth(1);
  hist[4]->Draw("same");
  hist[4]->Fit(f[4]);
  f[4]->Draw("same");

  hist[5]->SetLineWidth(1);
  hist[5]->Draw("same");
  hist[5]->Fit(f[5]);
  f[5]->Draw("same");

  hist[6]->SetLineWidth(1);
  hist[6]->Draw("same");
  hist[6]->Fit(f[6]);
  f[6]->Draw("same");
  */
  /*
  TLegend* leg = new TLegend(0.6,0.65,0.85,0.85);
  leg->AddEntry(hist[0],"Jig 10C, LV off","l");
  leg->AddEntry(hist[1],"Jig 10C, LV on","l");
  leg->AddEntry(hist[2],"Jig 20C, LV off","l");
  leg->AddEntry(hist[3],"Jig 20C, LV on","l");
  leg->Draw();
  */
  //  gr->GetXaxis()->SetTitle("Time[min]");
  //  gr->GetYaxis()->SetTitle("Leakage current[nA]");
  //  gr->Draw();
  //  return(0,0,0,0);
  //  return(0);
}
