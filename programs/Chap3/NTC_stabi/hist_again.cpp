#include <iostream>
#include <math.h>
#include <fstream>
#include <TStyle.h>
#include <TGraph.h>
#include <TCanvas.h>
#include <TH1D.h>
#include <TF1.h>
#include <TLegend.h>
//double returnCurrent[4]{                                                                                                                                                  
using namespace std;

double retT(double T1, double I1, double I2){
  double kb = 8.617 * pow(10,-5);
  double Eg = 1.21;
  double T2 = 250; 
  bool isOver=false;
  double left=I1/I2;
  double right=-100;
  while(isOver==false){
    /*    right=pow(T1/T2,2)*exp(-Eg/(2*kb)*(1./T1-1./T2));
    if(left<right){T2=T2+0.05;}
    else{isOver=true;}
    */
    if((I2/I1)<=(T2/T1)*(T2/T1)*exp((-(1.17-(4.73*0.0001*T2*T2)/(T2+636))/(2*kb))*(1./T2)-(1.17-(4.73*0.0001*T1*T1)/(T1+636))/(2*kb)*(-1./T1))){
    //    if((I2/I1)<=(T2/T1)*(T2/T1)*exp((-(1.17-(4.73*pow(10,-4)*pow(T2,2))/(T2+636))/(2*kB))*(1./T2)+(1.17-(4.73*pow(10,-4)*\
pow(T1,2))/(T1+636))/(2*kB))*(1./T1)){                                                                                          
      isOver=true;
    }else{
      T2+=0.05;
    }

  }
  T2=T2-273.2;
  cout<<I1<<" "<<I2<<" "<<T2<<endl;
  return T2;

}

int main(){
  TCanvas *c1 = new TCanvas("","",600,400);
  TH1D *hist[7];
  for(int ii=0;ii<7;ii++){
    hist[ii]=new TH1D("","",221, -0.05, 0.5);
  }

  TH1D *hist2[7];
  for(int ii=0;ii<7;ii++){
    hist2[ii]=new TH1D("","",410, -10.5, 30.5);
  }

  string fileName[7]={"leak_room.txt", "leak_20_on.txt", "leak_20_off.txt", "leak_10_on.txt", "leak_10_off.txt", "leak_-10_on.txt", "leak_-10_off.txt"};

  TF1 *f[7];
  for(int ii=0;ii<7;ii++){

    //    f[ii]=new TF1("","[0]*exp(-((x-[1])**2)/(2*([2]**2)))",0,1);
  }

  double current;
  string hoge;
  int count=0;
  vector <double> leakCurr[7];
  for(int ii=0;ii<7;ii++){
    ifstream ifs(fileName[ii].c_str());
    count=0;
    while(ifs>>hoge>>hoge>>hoge>>hoge>>hoge>>hoge>>hoge>>hoge>>current>>hoge>>hoge>>hoge>>hoge>>hoge>>hoge>>hoge>>hoge){
      count++;
      //      cout<<current<<endl;
      if(count>=0){
      leakCurr[ii].push_back(-1*current*pow(10,6));
      }
    }
  }

  double temp[7]={22.9, 27.9, 21.2, 17.8, 11.1, -3.0, -8.9};
  for(int ii=0;ii<7;ii++){
    temp[ii]=temp[ii]+273.2;
  }
  for(int ii=0;ii<7;ii++){
    for(int jj=0;jj<leakCurr[ii].size();jj++){
      double calT=retT(22.9+273.2, 0.291208, leakCurr[ii][jj]);
      hist[ii]->Fill(leakCurr[ii][jj],1./leakCurr[ii].size());
      hist2[ii]->Fill(calT, 1./leakCurr[ii].size());
    }
  }
  for(int ii=0;ii<7;ii++){
    f[ii]=new TF1("","gaus(0)",leakCurr[ii][leakCurr[ii].size()-2]-0.05,leakCurr[ii][leakCurr[ii].size()-2]+0.05);
    cout<<leakCurr[ii][leakCurr[ii].size()-1]<<endl;
    //    f[ii]->SetParameters(0.05, leakCurr[ii][leakCurr[ii].size()-2]+0.02,0.1);
    f[ii]->SetParameters(0.3, 0,0.1);
    hist[ii]->SetLineColor(ii+1);
    f[ii]->SetLineColor(ii+1);
   
  }
  f[0]->SetParameters(1, leakCurr[0][leakCurr[0].size()-2],0.001);
  f[5]->SetParameters(0., leakCurr[5][leakCurr[5].size()-2]+0.1,1);
  f[6]->SetParameters(1, leakCurr[6][leakCurr[6].size()-2],0.001);
  hist[0]->GetXaxis()->SetTitle("Leakage current [#muA]");
  hist[0]->GetYaxis()->SetTitle("Normalized event");
  gStyle->SetOptStat(0);
  hist[0]->Draw("hist");
  f[0]->Draw("same");
  for(int ii=1;ii<7;ii++){
    hist[ii]->Draw("histsame");
     hist[ii]->Fit(f[ii]);
    f[ii]->SetLineWidth(2);
    f[ii]->Draw("same");
  }
  TLegend* leg = new TLegend(0.65,0.55,0.9,0.85);
  leg->AddEntry(hist[0],"roomtemp, LV off, 22.9C","f");
  leg->AddEntry(hist[1],"Jig 20C, LV on, 27.9C","f");
  leg->AddEntry(hist[2],"Jig 20C, LV off, 21.2C","f");
  leg->AddEntry(hist[3],"Jig 10C, LV on, 17.8C","f");
  leg->AddEntry(hist[4],"Jig 10C, LV off, 11.1C","f");
  leg->AddEntry(hist[5],"Jig -10C, LV on, -3.0C","f");
  leg->AddEntry(hist[6],"Jig -10C, LV off, -8.9C","f");

  leg->Draw();

  c1->SaveAs("leak.pdf");

  TCanvas *c2 = new TCanvas("","",600,400);
  TF1 *f2[7];
  for(int ii=0;ii<7;ii++){
    f2[ii]=new TF1("","gaus(0)",-15,35);
    f2[ii]->SetParameters(0.3, 0,0.1);
    hist2[ii]->SetLineColor(ii+1);
    f2[ii]->SetLineColor(ii+1);
  }

  for(int ii=0;ii<7;ii++){
    hist2[ii]->Fit(f2[ii]);
    f2[ii]->SetLineWidth(2);
  }
  hist2[0]->GetXaxis()->SetTitle("Temperature  [#circC]");
  hist2[0]->GetYaxis()->SetTitle("Normalized event");
  hist2[0]->Draw("hist");
  f2[0]->Draw("same");
  for(int ii=1;ii<7;ii++){
    hist2[ii]->Draw("histsame");
    f2[ii]->Draw("same");
  }
  c2->SaveAs("calT.pdf");
}
