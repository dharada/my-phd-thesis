#include <iostream>
#include <math.h>
#include <fstream>
#include <TStyle.h>
#include <TGraph.h>
#include <TCanvas.h>
#include <TH1D.h>
#include <TF1.h>
#include <TLegend.h>
using namespace std;

int timeStamp(int hour, int min, int sec){
  return hour*60*60+min*60+sec;
}

int main(){
  bool isOn =false;
  TH1D *hist[7];
  for(int ii=0;ii<7;ii++){
    hist[ii]=new TH1D("","",441, -13, 31);
  }
  TF1 *f[7];
  for(int ii=0;ii<7;ii++){

  }

  string leakFiles[7]={"leak_room.txt", "leak_20_on.txt", "leak_20_off.txt", "leak_10_on.txt", "leak_10_off.txt", "leak_-10_on.txt", "leak_-10_off.txt"};

  int time[7][2]={{timeStamp(10,26,26),timeStamp(10,33,23)},
		  {timeStamp(11,35,58),timeStamp(11,41,37)},
		  {timeStamp(11,21,34),timeStamp(11,26,42)},
		  {timeStamp(12,52,26),timeStamp(12,58,01)},
		  {timeStamp(12,37,14),timeStamp(12,43,19)},
		  {timeStamp(13,37,05),timeStamp(13,42,22)},
		  {timeStamp(13,11,48),timeStamp(13,17,13)}};
  string hoge;
  int hour, min, sec;
  double temp;
  vector <double>v_temp[7];
  for(int ii=0;ii<7;ii++){
    ifstream tempFile("moduleNTC/ModuleNTC.txt");
    while(tempFile>>hoge>>hour>>min>>sec>>temp){
      //      cout<<hour<<" "<<min<<" "<<sec<<endl;
      if(timeStamp(hour,min,sec)>time[ii][0]&&timeStamp(hour,min,sec)<time[ii][1]){
	if(temp>=-100){
	  v_temp[ii].push_back(temp);
	  cout<<leakFiles[ii]<<endl;
	}
      }
    }
  }
  for(int ii=0;ii<7;ii++){
    f[ii]=new TF1("","gaus(0)",v_temp[ii][0]-0.5,v_temp[ii][0]+0.5);
    //    f[ii]->SetParameters(0.5, v_temp[ii][1], 0.1);
    for(int jj=0;jj<v_temp[ii].size();jj++){
      hist[ii]->Fill(v_temp[ii][jj], 1./v_temp[ii].size());
    }
    hist[ii]->SetFillColor(ii+1);
    hist[ii]->SetLineColor(ii+1);
    hist[ii]->Fit(f[ii]);
    f[ii]->SetLineWidth(2);
    f[ii]->SetLineColor(ii+1);
  }
  hist[0]->SetFillColor(1);
  hist[1]->SetFillColor(2);
  hist[2]->SetFillColor(2);
  hist[3]->SetFillColor(3);
  hist[4]->SetFillColor(3);
  hist[5]->SetFillColor(4);
  hist[6]->SetFillColor(4);

  hist[0]->SetLineColor(1);
  hist[1]->SetLineColor(2);
  hist[2]->SetLineColor(2);
  hist[3]->SetLineColor(3);
  hist[4]->SetLineColor(3);
  hist[5]->SetLineColor(4);
  hist[6]->SetLineColor(4);

  f[0]->SetLineColor(1);
  f[1]->SetLineColor(2);
  f[2]->SetLineColor(2);
  f[3]->SetLineColor(3);
  f[4]->SetLineColor(3);
  f[5]->SetLineColor(4);
  f[6]->SetLineColor(4);
  TCanvas *c1 = new TCanvas("","",600,400);
     TLegend* leg = new TLegend(0.15,0.65,0.4,0.85);
  gStyle->SetOptStat(0);
  gPad->SetBottomMargin(0.12);
  gPad->SetLeftMargin(0.12);
  hist[0]->SetTitle("Module NTC temperature (power off)");
  hist[0]->GetXaxis()->SetTitle("Module NTC temperature [#circC]");
  hist[0]->GetYaxis()->SetTitle("Normalized event");
  hist[0]->GetXaxis()->SetTitleSize(0.05);
  hist[0]->GetYaxis()->SetTitleSize(0.05);
  hist[0]->GetXaxis()->SetLabelSize(0.05);
  hist[0]->GetYaxis()->SetLabelSize(0.05);
  hist[0]->GetYaxis()->SetRangeUser(0,1.3);
  if(isOn==0){
    hist[0]->Draw("hist");
    f[0]->Draw("same");
    hist[2]->Draw("samehist");
    f[2]->Draw("same");
    hist[4]->Draw("samehist");
    f[4]->Draw("same");
    hist[6]->Draw("samehist");
    f[6]->Draw("same");
    leg->AddEntry(f[0],"roomtemp, LV off","l");
    leg->AddEntry(f[2],"Jig 20C, LV off","l");
    leg->AddEntry(f[4],"Jig 10C, LV off","l");
    leg->AddEntry(f[6],"Jig -10C, LV off","l");
    //    leg->AddEntry(f[0],"roomtemp, LV off, 22.9C","l");
    //    leg->AddEntry(f[2],"Jig 20C, LV off, 21.2C","l");
    //    leg->AddEntry(f[4],"Jig 10C, LV off, 11.1C","l");
    //    leg->AddEntry(f[6],"Jig -10C, LV off, -8.9C","l");

  }
  hist[1]->SetTitle("Module NTC temperature (power on)");
  hist[1]->GetXaxis()->SetTitle("Module NTC temperature [#circC]");
  hist[1]->GetYaxis()->SetTitle("Normalized event");
  hist[1]->GetXaxis()->SetTitleSize(0.05);
  hist[1]->GetYaxis()->SetTitleSize(0.05);
  hist[1]->GetXaxis()->SetLabelSize(0.05);
  hist[1]->GetYaxis()->SetLabelSize(0.05);
  hist[1]->GetYaxis()->SetRangeUser(0,1);
  if(isOn==1){
    hist[1]->Draw("hist");
    f[1]->Draw("same");
    hist[3]->Draw("samehist");
    f[3]->Draw("same");
    hist[5]->Draw("samehist");
    f[5]->Draw("same");
    leg->AddEntry(f[1],"Jig 20C, LV on","l");
    leg->AddEntry(f[3],"Jig 10C, LV on","l");
    leg->AddEntry(f[5],"Jig -10C, LV on","l");
    //    leg->AddEntry(f[1],"Jig 20C, LV on, 27.9C","l");
    //    leg->AddEntry(f[3],"Jig 10C, LV on, 17.8C","l");
    //    leg->AddEntry(f[5],"Jig -10C, LV on, -3.0C","l");
  }

  /*
  for(int ii=1;ii<7;ii++){
    hist[ii]->Draw("samehist");
    f[ii]->Draw("same");
  }
  */

  leg->Draw();

  c1->SaveAs("temp_hist.pdf");
  
}
    

