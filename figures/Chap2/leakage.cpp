#include <iostream>
#include <fstream>
#include <nlohmann/json.hpp>
#include <tuple>
#include <math.h>

#include <TROOT.h>
#include <TGraph.h>
#include <TStyle.h>
#include <TLegend.h>
#include <TCanvas.h>
#include <TGaxis.h>
#include <TAxis.h>
#include <TF1.h>
int main(){
  TCanvas *c1 = new TCanvas("c1", "", 1200, 800);

  int t1;
  int t2=273+20;
  double kB=8.617*pow(10,-5);
  double alpha=7.021*pow(10,-4);
  double beta=1108;
  
  double alpha2=4.73*pow(10,-4);
  double beta2=636;
  
  //    TF1 *f1 = new TF1("","(x/300)*(x/300)*exp((-(1.17-(4.73*0.0001*x*x)/(x+636))))"
		      /*
		      exp(-((1.1557-7.021*0.0001*x*x/(x+1108))/(2*8.617*0.00001))*(1/x-1/300))",270,330);
    */
  TF1 *f1 = new TF1("","((x+[3])/[4])*((x+[3])/[4])*exp(-(1.1557-[0]*(x+[3])*(x+[3])/((x+[3])+[1]))/(2*[2])*(1/(x+[3])-1./[4]))",250,320);
  //  TF1 *f1 = new TF1("","(x/300)*(x/300)*exp(1./(2*[2])*(1./x-1./300)
  f1->SetParameter(0,alpha);
  f1->SetParameter(1,beta);
  f1->SetParameter(2,kB);
  f1->SetParameter(3,0);
  f1->SetParameter(4,t2);
  /*
  TF1 *f2 = new TF1("","(x/300)*(x/300)*exp(-(1.1557-[0]*x*x/(x+[1]))/(2*[2])*(1/x-1./300))",0,100);
  f2->SetParameter(0,alpha2);
  f2->SetParameter(1,beta2);
  f2->SetParameter(2,kB);
  //    TF1 *f1 = new TF1("","exp(x)",0,10);
  f2->SetLineColor(3);
  */
  f1->GetYaxis()->SetMoreLogLabels();
  f1->SetTitle();
  c1->SetLogy();
  f1->GetXaxis()->SetTitle("Temperature [K]");
  f1->GetYaxis()->SetTitle("Leakage current");
  f1->GetYaxis()->SetTitleOffset(1);
  f1->GetXaxis()->SetTitleSize(0.05);
  f1->GetXaxis()->SetLabelSize(0.04);
  f1->GetYaxis()->SetTitleSize(0.05);
  f1->GetYaxis()->SetLabelSize(0.04);
  f1->Draw();
  gPad->SetGrid(1, 1); gPad->Update();
  //  f2->Draw("same");
  c1->SaveAs("leakCurr.pdf");
}
