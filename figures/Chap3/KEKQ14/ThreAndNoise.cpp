#include <iostream>
#include <fstream>
#include <nlohmann/json.hpp>
#include <tuple>
#include <math.h>

#include <TROOT.h>
#include <TGraph.h>
#include <TGraphErrors.h>
#include <TH1F.h>
#include <THStack.h>
#include <TStyle.h>
#include <TLegend.h>
#include <TCanvas.h>
#include <TGaxis.h>
#include <TAxis.h>

int main(){
  double threMean[4][5]={{1427.1,1423.0,1423.9,1425.0,1425.7},{1452.0,1451.2,1434.7,1443.5,1448.3},{1432.5,1433.7,1434.6,1433.4,1427.1},{1429.1,1416.2,1410.5,1416.5,1412.6}};
double threSig[4][5]={{40.0,43.8,55.6,40.5,42.2},{35.1,33.3,54.4,43.1,42.9},{38.6,36.0,52.2,37.9,41.7},{42.8,38.1,54.0,41.3,46.3}};
  double noiseMean[4][5]={{45.4, 45.5, 43.7, 45.0, 45.5},{44.4, 44.7, 42.6, 44.6, 44.7},{41.2,41.2,40.0,41.4,41.0},{43.0,43.1,41.3,43.0,43.1}};
  double noiseSig[4][5]={{10.0, 10.0, 10.2, 9.0, 10.1},{10.0, 9.9, 10.3, 10.2, 9.7},{9.2,10.5,9.3,9.2,9.3},{10.0,10.2,10.5,10.2,10.1}};

  double xAxis[5]={0.5,1.5,2.5,3.5,4.5};
  double exAxis[5]={0,0,0,0,0};
  TGraphErrors *grThr[2][4];
  TGraphErrors *grNoi[2][4];

  //  string label[3]={"Initial test", "Reception at CERN", "After cell loading"};

  for(int ii=0;ii<4;ii++){
    grThr[0][ii]=new TGraphErrors(5,xAxis, threMean[ii], exAxis, threSig[ii]);
    grThr[1][ii]=new TGraphErrors(5,xAxis, threMean[ii], exAxis, threSig[ii]);
    grThr[0][ii]->SetMarkerStyle(20+ii);
    grThr[1][ii]->SetMarkerStyle(20+ii);
    grThr[0][ii]->SetMarkerSize(3);
    grThr[1][ii]->SetMarkerSize(3);
    grThr[0][ii]->SetMarkerColor(ii+1);
    grThr[1][ii]->SetMarkerColor(ii+1);
    grThr[0][ii]->SetLineColor(ii+1);
    grThr[1][ii]->SetLineColor(ii+1);

    grNoi[0][ii]=new TGraphErrors(5,xAxis, noiseMean[ii], exAxis, noiseSig[ii]);
    grNoi[1][ii]=new TGraphErrors(5,xAxis, noiseMean[ii], exAxis, noiseSig[ii]);
    grNoi[0][ii]->SetMarkerStyle(20+ii);
    grNoi[1][ii]->SetMarkerStyle(20+ii);
    grNoi[0][ii]->SetMarkerSize(3);
    grNoi[1][ii]->SetMarkerSize(3);
    grNoi[0][ii]->SetMarkerColor(ii+1);
    grNoi[1][ii]->SetMarkerColor(ii+1);
    grNoi[0][ii]->SetLineColor(ii+1);
    grNoi[1][ii]->SetLineColor(ii+1);
  }
  TCanvas *c1 = new TCanvas("c1", "",1200,800);
  grThr[0][0]->SetTitle("Development of Threshold mean");
  grThr[0][0]->GetXaxis()->SetTitle("");
  grThr[0][0]->GetYaxis()->SetTitle("Threshold [E]");
  grThr[0][0]->GetYaxis()->SetRangeUser(1300,1700);
  grThr[0][0]->GetXaxis()->SetRangeUser(0.,5.);
  grThr[0][0]->GetXaxis()->SetLabelSize(0.05);
  grThr[0][0]->GetXaxis()->SetBinLabel(8.0, "#splitline{Before}{thermal cycle}");
  grThr[0][0]->GetXaxis()->SetBinLabel(29.0, "#splitline{After}{25 cycles}");
  grThr[0][0]->GetXaxis()->SetBinLabel(50.0, "#splitline{After}{50 cycles}");
  grThr[0][0]->GetXaxis()->SetBinLabel(71.0, "#splitline{After}{75 cycles}");
  grThr[0][0]->GetXaxis()->SetBinLabel(92.0, "#splitline{After}{100 cycles}");
  grThr[0][0]->Draw("apl");
  for(int ii=0;ii<4;ii++){
    grThr[0][ii]->Draw("samepl");
    //    grThr[1][ii]->Draw("samepl");
  }
  TLegend* leg1 = new TLegend(0.15,0.6,0.35,0.85);
  //  leg1->AddEntry(grThr[0][0],"Dead pixel","p");
  //  leg1->AddEntry(grThr[1][0],"Bad pixel","p");
  leg1->AddEntry(grThr[0][0],"FE1","pl");
  leg1->AddEntry(grThr[0][1],"FE2","pl");
  leg1->AddEntry(grThr[0][2],"FE3","pl");
  leg1->AddEntry(grThr[0][3],"FE4","pl");
  leg1->Draw();
  c1->SetBottomMargin(0.25);
  c1->SaveAs("ThresholdDevelopment.pdf");

  TCanvas *c2 =	new TCanvas("c2", "",1200,800);
  grNoi[0][0]->SetTitle("Development of Noise mean");
  grNoi[0][0]->GetXaxis()->SetTitle("");
  grNoi[0][0]->GetYaxis()->SetTitle("Noise [e]");
  grNoi[0][0]->GetYaxis()->SetRangeUser(20,90);
  grNoi[0][0]->GetXaxis()->SetRangeUser(0.,5.);
  grNoi[0][0]->GetXaxis()->SetLabelSize(0.05);
  grNoi[0][0]->GetXaxis()->SetBinLabel(8.0, "#splitline{Before}{thermal cycle}");
  grNoi[0][0]->GetXaxis()->SetBinLabel(29.0, "#splitline{After}{25 cycles}");
  grNoi[0][0]->GetXaxis()->SetBinLabel(50.0, "#splitline{After}{50 cycles}");
  grNoi[0][0]->GetXaxis()->SetBinLabel(71.0, "#splitline{After}{75 cycles}");
  grNoi[0][0]->GetXaxis()->SetBinLabel(92.0, "#splitline{After}{100 cycles}");
  grNoi[0][0]->Draw("apl");
  grNoi[0][0]->Draw("apl");
  for(int ii=0;ii<4;ii++){
    grNoi[0][ii]->Draw("samepl");
    //    grNoi[1][ii]->Draw("samepl");
  }
  TLegend* leg2 = new TLegend(0.15,0.6,0.35,0.85);
  //  leg2->AddEntry(grNoi[0][0],"Dead pixel","p");
  //  leg2->AddEntry(grNoi[1][0],"Bad pixel","p");
  leg2->AddEntry(grNoi[0][0],"FE1","pl");
  leg2->AddEntry(grNoi[0][1],"FE2","pl");
  leg2->AddEntry(grNoi[0][2],"FE3","pl");
  leg2->AddEntry(grNoi[0][3],"FE4","pl");
  leg2->Draw();
  c2->SetBottomMargin(0.25);
  c2->SaveAs("NoiseDevelopment.pdf");
}
  
    
    
