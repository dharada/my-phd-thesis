#include <iostream>
#include <fstream>
#include <nlohmann/json.hpp>
#include <tuple>
#include <math.h>

#include <TROOT.h>
#include <TGraph.h>
#include <TGraphErrors.h>
#include <TH1F.h>
#include <THStack.h>
#include <TStyle.h>
#include <TLegend.h>
#include <TCanvas.h>
#include <TGaxis.h>
#include <TAxis.h>
using namespace std;
int main(){
  double digiDead[4][5]={{0,0,0,0,0},{0,0,0,0,0},{0,0,0,0,0},{0,0,0,0,0}};
  double digiBad[4][5]={{0,0,0,0,0},{0,0,0,0,0},{0,0,0,0,0},{0,0,0,0,0}};
  double anaDead[4][5]={{0,0,0,0,0},{0,0,0,0,0},{0,0,0,0,0},{0,0,0,0,0}};
  double anaBad[4][5]={{1,1,1,1,1},{0,0,0,0,0},{0,0,0,0,0},{0,0,0,0,0}};

  double discGood[4][5]={{23878,23961,23673,23970,23893},{23639,23669,23687,23622,23591},{23131,23219,22876,23175,23449},{24345,24515,24377,24497,24493}};

  double discBumpRaw[5][4][3]= //circuit, fe, stage
    {
      {{83, 83, 377}, {97, 81, 546}, {39, 55, 616}, {18, 42, 338}}, //0
      {{73, 71, 317}, {80, 63, 529}, {39, 42, 546}, {22, 34, 293}}, //25
      {{106, 141, 511}, {120, 272, 602}, {55, 186, 773}, {19, 96, 352}}, //50
      {{82, 78, 329}, {75, 66, 544}, {36, 38, 575}, {21, 32, 307}}, //75
      {{90, 87, 356}, {83, 71, 595}, {43, 44, 488}, {20, 39, 306}} //100

    }; 

  double discBump[4][5];
  for(int ii=0;ii<4;ii++){
    for(int jj=0;jj<5;jj++){
      discBump[ii][jj]=(discBumpRaw[jj][ii][0]+discBumpRaw[jj][ii][1]+discBumpRaw[jj][ii][2])/76800*100;

    }
  }
  double xAxis[5]={0.5,1.5,2.5,3.5,4.5};

  TGraph *grDig[2][4];
  TGraph *grAna[2][4];
  TGraph *grDisc[4];

  //  string label[3]={"Initial test", "Reception at CERN", "After cell loading"};

  for(int ii=0;ii<4;ii++){
    grDig[0][ii]=new TGraph(5,xAxis, digiDead[ii]);
    grDig[1][ii]=new TGraph(5,xAxis, digiBad[ii]);
    grDig[0][ii]->SetMarkerStyle(22);
    grDig[1][ii]->SetMarkerStyle(23);
    grDig[0][ii]->SetMarkerSize(3);
    grDig[1][ii]->SetMarkerSize(3);
    grDig[0][ii]->SetMarkerColor(ii+1);
    grDig[1][ii]->SetMarkerColor(ii+1);
    grDig[0][ii]->SetLineColor(ii+1);
    grDig[1][ii]->SetLineColor(ii+1);

    grAna[0][ii]=new TGraph(5,xAxis, anaDead[ii]);
    grAna[1][ii]=new TGraph(5,xAxis, anaBad[ii]);
    grAna[0][ii]->SetMarkerStyle(22);
    grAna[1][ii]->SetMarkerStyle(23);
    grAna[0][ii]->SetMarkerSize(3);
    grAna[1][ii]->SetMarkerSize(3);
    grAna[0][ii]->SetMarkerColor(ii+1);
    grAna[1][ii]->SetMarkerColor(ii+1);
    grAna[0][ii]->SetLineColor(ii+1);
    grAna[1][ii]->SetLineColor(ii+1);

    grDisc[ii] = new TGraph(5,xAxis,discBump[ii]);
    //    grDisc[ii] = new TGraph(5,xAxis,discGood[ii]);
    grDisc[ii]->SetMarkerStyle(20+ii);
    grDisc[ii]->SetMarkerSize(3);
    grDisc[ii]->SetMarkerColor(ii+1);
    grDisc[ii]->SetLineColor(ii+1);
  }
  string label[5]={"#splitline{Before}{thermal cycle}","#splitline{After}{25 cycles}","#splitline{After}{50 cycles}","#splitline{After}{75 cycles}","#splitline{After}{100 cycles}"};
  TCanvas *c1 = new TCanvas("c1", "",1200,800);
  grDig[0][0]->SetTitle("Development of Digital scan");
  grDig[0][0]->GetXaxis()->SetTitle("");
  grDig[0][0]->GetYaxis()->SetTitle("# of Pixels");
  grDig[0][0]->GetYaxis()->SetRangeUser(-1,5);
  grDig[0][0]->GetXaxis()->SetRangeUser(0.,5.);
  grDig[0][0]->GetXaxis()->SetLabelSize(0.05);
  for(int ii=0;ii<5;ii++){
    grDig[0][0]->GetXaxis()->SetBinLabel(8.0+21*ii,label[ii].c_str());
  }
  /*
  grDig[0][0]->GetXaxis()->SetBinLabel(8.0, "#splitline{Before}{thermal cycle}");
  grDig[0][0]->GetXaxis()->SetBinLabel(29.0, "#splitline{After}{25 cycles}");
  grDig[0][0]->GetXaxis()->SetBinLabel(50.0, "#splitline{After}{50 cycles}");
  grDig[0][0]->GetXaxis()->SetBinLabel(71.0, "#splitline{After}{75 cycles}");
  grDig[0][0]->GetXaxis()->SetBinLabel(92.0, "#splitline{After}{100 cycles}");
  */
  grDig[0][0]->Draw("apl");
  for(int ii=0;ii<4;ii++){
    grDig[0][ii]->Draw("samepl");
    grDig[1][ii]->Draw("samepl");
  }
  TLegend* leg1 = new TLegend(0.15,0.6,0.35,0.85);
  leg1->AddEntry(grDig[0][0],"Dead pixel","p");
  leg1->AddEntry(grDig[1][0],"Bad pixel","p");
  leg1->AddEntry(grDig[0][0],"FE1","pl");
  leg1->AddEntry(grDig[0][1],"FE2","pl");
  leg1->AddEntry(grDig[0][2],"FE3","pl");
  leg1->AddEntry(grDig[0][3],"FE4","pl");
  leg1->Draw();
  c1->SetBottomMargin(0.25);
  c1->SaveAs("DigitalDevelopment.pdf");

  TCanvas *c2 =	new TCanvas("c2", "",1200,800);
  grAna[0][0]->SetTitle("Development of Analog scan");
  grAna[0][0]->GetXaxis()->SetTitle("");
  grAna[0][0]->GetYaxis()->SetTitle("# of Pixels");
  grAna[0][0]->GetYaxis()->SetRangeUser(-1,5);
  grAna[0][0]->GetXaxis()->SetRangeUser(0.,5.);
  grAna[0][0]->GetXaxis()->SetLabelSize(0.05);
  grAna[0][0]->GetXaxis()->SetBinLabel(8.0, "#splitline{Before}{thermal cycle}");
  grAna[0][0]->GetXaxis()->SetBinLabel(29.0, "#splitline{After}{25 cycles}");
  grAna[0][0]->GetXaxis()->SetBinLabel(50.0, "#splitline{After}{50 cycles}");
  grAna[0][0]->GetXaxis()->SetBinLabel(71.0, "#splitline{After}{75 cycles}");
  grAna[0][0]->GetXaxis()->SetBinLabel(92.0, "#splitline{After}{100 cycles}");
  grAna[0][0]->Draw("apl");
  grAna[0][0]->Draw("apl");
  for(int ii=0;ii<4;ii++){
    grAna[0][ii]->Draw("samepl");
    grAna[1][ii]->Draw("samepl");
  }
  TLegend* leg2 = new TLegend(0.15,0.6,0.35,0.85);
  leg2->AddEntry(grAna[0][0],"Dead pixel","p");
  leg2->AddEntry(grAna[1][0],"Bad pixel","p");
  leg2->AddEntry(grAna[0][0],"FE1","pl");
  leg2->AddEntry(grAna[0][1],"FE2","pl");
  leg2->AddEntry(grAna[0][2],"FE3","pl");
  leg2->AddEntry(grAna[0][3],"FE4","pl");
  leg2->Draw();
  c2->SetBottomMargin(0.25);
  c2->SaveAs("AnalogDevelopment.pdf");

 TCanvas *c3 = new TCanvas("c3", "",1200,800);
  grDisc[0]->SetTitle("Development of Bump connection");
  grDisc[0]->GetXaxis()->SetTitle("");
  grDisc[0]->GetYaxis()->SetTitle("# of Pixels [%]");
  grDisc[0]->GetYaxis()->SetRangeUser(0,5);
  grDisc[0]->GetXaxis()->SetRangeUser(0.,5.);
  grDisc[0]->GetXaxis()->SetLabelSize(0.05);
  grDisc[0]->GetXaxis()->SetBinLabel(8.0, "#splitline{Before}{thermal cycle}");
  grDisc[0]->GetXaxis()->SetBinLabel(29.0, "#splitline{After}{25 cycles}");
  grDisc[0]->GetXaxis()->SetBinLabel(50.0, "#splitline{After}{50 cycles}");
  grDisc[0]->GetXaxis()->SetBinLabel(71.0, "#splitline{After}{75 cycles}");
  grDisc[0]->GetXaxis()->SetBinLabel(92.0, "#splitline{After}{100 cycles}");
  grDisc[0]->Draw("apl");
  grDisc[0]->Draw("apl");
  for(int ii=0;ii<4;ii++){
    grDisc[ii]->Draw("samepl");
  }
TLegend* leg3 = new TLegend(0.15,0.6,0.32,0.85);
  leg3->AddEntry(grDisc[0],"FE1","pl");
  leg3->AddEntry(grDisc[1],"FE2","pl");
  leg3->AddEntry(grDisc[2],"FE3","pl");
  leg3->AddEntry(grDisc[3],"FE4","pl");
  leg3->Draw();
  c3->SetBottomMargin(0.25);
  c3->SaveAs("DiscbumpDevelopment.pdf");

  
}
  
    
    
