#include <iostream>
#include <fstream>
#include <nlohmann/json.hpp>
#include <tuple>
#include <math.h>

#include <TROOT.h>
#include <TGraph.h>
#include <TGraphErrors.h>
#include <TH1F.h>
#include <THStack.h>
#include <TStyle.h>
#include <TLegend.h>
#include <TCanvas.h>
#include <TGaxis.h>
#include <TAxis.h>

int main(){
  double digiDead[4][3]={{0,0,0},{0,0,0},{0,0,0},{0,0,0}};
  double digiBad[4][3]={{0,0,0},{0,0,0},{0,0,0},{0,0,0}};
  double anaDead[4][3]={{0,0,0},{2,2,2},{0,0,0},{0,0,0}};
  double anaBad[4][3]={{2,2,2},{1,1,8},{1,1,2},{3,2,4}};
  //  double anaDead[4][3]={{2,2,2},{1,1,8},{1,0,2},{3,2,4}};
  //  double anaBad[4][3]={{0,0,0},{0,0,0},{0,1,0},{0,0,0}};

  //  double discGood[4][3]={{18220,25893,25884},{17013,25623,25324},{14847,25862,25466},{16342,25876,25479}};
  double discGood[4][2]={{0.12,0.10},{0.19,0.33},{0.15,0.47},{0.09,0.35}};
  //  double discGood[4][3]={{14.07,0.12,0.10},{15.65,0.19,0.33},{29.21,0.15,0.47},{18.93,0.09,0.35}};

  double xAxis[3]={0.5,1.5,2.5};

  TGraph *grDig[2][4];
  TGraph *grAna[2][4];
  TGraph *grDisc[4];

  //  string label[3]={"Initial test", "Reception at CERN", "After cell loading"};

  for(int ii=0;ii<4;ii++){
    grDig[0][ii]=new TGraph(3,xAxis, digiDead[ii]);
    grDig[1][ii]=new TGraph(3,xAxis, digiBad[ii]);
    grDig[0][ii]->SetMarkerStyle(22);
    grDig[1][ii]->SetMarkerStyle(23);
    grDig[0][ii]->SetMarkerSize(3);
    grDig[1][ii]->SetMarkerSize(3);
    grDig[0][ii]->SetMarkerColor(ii+1);
    grDig[1][ii]->SetMarkerColor(ii+1);
    grDig[0][ii]->SetLineColor(ii+1);
    grDig[1][ii]->SetLineColor(ii+1);

    grAna[0][ii]=new TGraph(3,xAxis, anaDead[ii]);
    grAna[1][ii]=new TGraph(3,xAxis, anaBad[ii]);
    grAna[0][ii]->SetMarkerStyle(22);
    grAna[1][ii]->SetMarkerStyle(23);
    grAna[0][ii]->SetMarkerSize(3);
    grAna[1][ii]->SetMarkerSize(3);
    grAna[0][ii]->SetMarkerColor(ii+1);
    grAna[1][ii]->SetMarkerColor(ii+1);
    grAna[0][ii]->SetLineColor(ii+1);
    grAna[1][ii]->SetLineColor(ii+1);

    double xAxis2[2]={1.5,2.5};
    grDisc[ii] = new TGraph(2,xAxis2,discGood[ii]);
    grDisc[ii]->SetMarkerStyle(20+ii);
    grDisc[ii]->SetMarkerSize(3);
    grDisc[ii]->SetMarkerColor(ii+1);
    grDisc[ii]->SetLineColor(ii+1);
  }
  TCanvas *c1 = new TCanvas("c1", "",1200,800);
  grDig[0][0]->SetTitle("Development of Digital scan");
  grDig[0][0]->GetXaxis()->SetTitle("");
  grDig[0][0]->GetYaxis()->SetTitle("# of Pixels");
  grDig[0][0]->GetYaxis()->SetRangeUser(-1,5);
  grDig[0][0]->GetXaxis()->SetRangeUser(0.,3.);
  grDig[0][0]->GetXaxis()->SetLabelSize(0.05);
  grDig[0][0]->GetXaxis()->SetBinLabel(8.0, "#splitline{Initial test}{at assembly site}");
  grDig[0][0]->GetXaxis()->SetBinLabel(50.0, "#splitline{Reception}{test}");
  grDig[0][0]->GetXaxis()->SetBinLabel(92.0, "#splitline{After}{cell loading}");
  grDig[0][0]->Draw("apl");
  for(int ii=0;ii<4;ii++){
    grDig[0][ii]->Draw("samepl");
    grDig[1][ii]->Draw("samepl");
  }
  TLegend* leg1 = new TLegend(0.15,0.6,0.35,0.85);
  leg1->AddEntry(grDig[0][0],"Dead pixel","p");
  leg1->AddEntry(grDig[1][0],"Bad pixel","p");
  leg1->AddEntry(grDig[0][0],"FE1","pl");
  leg1->AddEntry(grDig[0][1],"FE2","pl");
  leg1->AddEntry(grDig[0][2],"FE3","pl");
  leg1->AddEntry(grDig[0][3],"FE4","pl");
  leg1->Draw();
  c1->SetBottomMargin(0.25);
  c1->SaveAs("DigitalDevelopment.pdf");

  TCanvas *c2 =	new TCanvas("c2", "",1200,800);
  grAna[0][0]->SetTitle("Development of Analog scan");
  grAna[0][0]->GetXaxis()->SetTitle("");
  grAna[0][0]->GetYaxis()->SetTitle("# of Pixels");
  grAna[0][0]->GetYaxis()->SetRangeUser(-1,10);
  grAna[0][0]->GetXaxis()->SetRangeUser(0.,3.);
  grAna[0][0]->GetXaxis()->SetLabelSize(0.05);
  grAna[0][0]->GetXaxis()->SetBinLabel(8.0, "#splitline{Initial test}{at assembly site}");
  grAna[0][0]->GetXaxis()->SetBinLabel(50.0, "#splitline{Reception}{test}");
  grAna[0][0]->GetXaxis()->SetBinLabel(92.0, "#splitline{After}{cell loading}");
  grAna[0][0]->Draw("apl");
  grAna[0][0]->Draw("apl");
  for(int ii=0;ii<4;ii++){
    grAna[0][ii]->Draw("samepl");
    grAna[1][ii]->Draw("samepl");
  }
  TLegend* leg2 = new TLegend(0.15,0.6,0.35,0.85);
  leg2->AddEntry(grAna[0][0],"Dead pixel","p");
  leg2->AddEntry(grAna[1][0],"Bad pixel","p");
  leg2->AddEntry(grAna[0][0],"FE1","pl");
  leg2->AddEntry(grAna[0][1],"FE2","pl");
  leg2->AddEntry(grAna[0][2],"FE3","pl");
  leg2->AddEntry(grAna[0][3],"FE4","pl");
  leg2->Draw();
  c2->SetBottomMargin(0.25);
  c2->SaveAs("AnalogDevelopment.pdf");

 TCanvas *c3 = new TCanvas("c3", "",1200,800);
 c3->SetLeftMargin(0.15);
  grDisc[0]->SetTitle("Development of Bump disconnection");
  grDisc[0]->GetXaxis()->SetTitle("");
  grDisc[0]->GetYaxis()->SetTitle("Occupancy 0 Pixels [%]");
  grDisc[0]->GetYaxis()->SetRangeUser(0,0.5);
  grDisc[0]->GetXaxis()->SetRangeUser(0.,3.);
  grDisc[0]->GetXaxis()->SetLabelSize(0.05);
  grDisc[0]->GetXaxis()->SetTitleSize(0.06);
  //  grDisc[0]->GetXaxis()->SetBinLabel(8.0, "#splitline{Initial test}{at assembly site}");
  grDisc[0]->GetXaxis()->SetBinLabel(8.0, "#splitline{Reception}{test}");
  //  grDisc[0]->GetXaxis()->SetBinLabel(50.0, "#splitline{Reception}{test}");
  grDisc[0]->GetXaxis()->SetBinLabel(92.0, "#splitline{After}{cell loading}");
  grDisc[0]->Draw("apl");
  grDisc[0]->Draw("apl");
  for(int ii=0;ii<4;ii++){
    grDisc[ii]->Draw("samepl");
  }
TLegend* leg3 = new TLegend(0.15,0.6,0.32,0.85);
  leg3->AddEntry(grDisc[0],"FE1","pl");
  leg3->AddEntry(grDisc[1],"FE2","pl");
  leg3->AddEntry(grDisc[2],"FE3","pl");
  leg3->AddEntry(grDisc[3],"FE4","pl");
  leg3->Draw();
  c3->SetBottomMargin(0.25);
  c3->SaveAs("DiscbumpDevelopment.pdf");

  
}
  
    
    
