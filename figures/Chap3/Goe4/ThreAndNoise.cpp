#include <iostream>
#include <fstream>
#include <nlohmann/json.hpp>
#include <tuple>
#include <math.h>

#include <TROOT.h>
#include <TGraph.h>
#include <TGraphErrors.h>
#include <TH1F.h>
#include <THStack.h>
#include <TStyle.h>
#include <TLegend.h>
#include <TCanvas.h>
#include <TGaxis.h>
#include <TAxis.h>

int main(){
  double threMean[4][3]={{1391.2,1380.9,1379.7},{1367.5,1365.6,1389.2},{1421.0,1418.0,1444.6},{1378.9,1377.7,1419.5}};
  double threSig[4][3]={{28.6,34.7,39.4},{30.8,33.1,41.6},{35.5,38.3,52.7},{30.8,33.0,46.0}};
  double noiseMean[4][3]={{48.4,47.9,47.3},{48.9,47.9,47.4},{49.8,49.3,48.6},{49.2,48.5,47.8}};
  double noiseSig[4][3]={{11.2,11.0,11.7},{10.6,11.2,11.3},{10.7,10.7,11.3},{10.7,11.1,11.9}};

  double xAxis[3]={0.5,1.5,2.5};
  double exAxis[3]={0,0,0};
  TGraphErrors *grThr[2][4];
  TGraphErrors *grNoi[2][4];
  TLine *tick[3];


  //  string label[3]={"Initial test", "Reception at CERN", "After cell loading"};

  for(int ii=0;ii<4;ii++){
    grThr[0][ii]=new TGraphErrors(3,xAxis, threMean[ii], exAxis, threSig[ii]);
    grThr[1][ii]=new TGraphErrors(3,xAxis, threMean[ii], exAxis, threSig[ii]);
    grThr[0][ii]->SetMarkerStyle(20+ii);
    grThr[1][ii]->SetMarkerStyle(20+ii);
    grThr[0][ii]->SetMarkerSize(3);
    grThr[1][ii]->SetMarkerSize(3);
    grThr[0][ii]->SetMarkerColor(ii+1);
    grThr[1][ii]->SetMarkerColor(ii+1);
    grThr[0][ii]->SetLineColor(ii+1);
    grThr[1][ii]->SetLineColor(ii+1);

    grNoi[0][ii]=new TGraphErrors(3,xAxis, noiseMean[ii], exAxis, noiseSig[ii]);
    grNoi[1][ii]=new TGraphErrors(3,xAxis, noiseMean[ii], exAxis, noiseSig[ii]);
    grNoi[0][ii]->SetMarkerStyle(20+ii);
    grNoi[1][ii]->SetMarkerStyle(20+ii);
    grNoi[0][ii]->SetMarkerSize(3);
    grNoi[1][ii]->SetMarkerSize(3);
    grNoi[0][ii]->SetMarkerColor(ii+1);
    grNoi[1][ii]->SetMarkerColor(ii+1);
    grNoi[0][ii]->SetLineColor(ii+1);
    grNoi[1][ii]->SetLineColor(ii+1);
  }
  TCanvas *c1 = new TCanvas("c1", "",1200,800);
  grThr[0][0]->SetTitle("Development of Threshold mean");
  grThr[0][0]->GetXaxis()->SetTitle("");
  grThr[0][0]->GetYaxis()->SetTitle("Threshold [e]");
  grThr[0][0]->GetYaxis()->SetRangeUser(1300,1600);
  grThr[0][0]->GetXaxis()->SetRangeUser(0.,3.);
  grThr[0][0]->GetXaxis()->SetLabelSize(0.05);
  //  grThr[0][0]->GetXaxis()->SetBinLabel(8.0, "#splitline{Initial test}{at assembly site}");
  //  grThr[0][0]->GetXaxis()->SetBinLabel(50.0, "#splitline{Reception}{test}");
  // grThr[0][0]->GetXaxis()->SetBinLabel(92.0, "#splitline{After}{cell loading}");
  grThr[0][0]->GetXaxis()->SetBinLabel(92.0, "");
  grThr[0][0]->GetXaxis()->ChangeLabel(22,0,-1,-1,-1,-1,"#splitline{Initial test}{at assembly site}");
  grThr[0][0]->GetXaxis()->ChangeLabel(55,0,-1,-1,-1,-1,"#splitline{Reception}{test}");
  grThr[0][0]->GetXaxis()->ChangeLabel(99,0,-1,-1,-1,-1,"#splitline{After}{cell loading}");
  grThr[0][0]->GetXaxis()->SetLabelOffset(0.03);
  grThr[0][0]->Draw("apl");
  for(int ii=0;ii<4;ii++){
    grThr[0][ii]->Draw("samepl");
    //    grThr[1][ii]->Draw("samepl");
  }
  for(int ii=0;ii<3;ii++){
    tick[ii]=new TLine(0.5+ii,1300, 0.5+ii,1310);
    tick[ii]->Draw();
  }
  TLegend* leg1 = new TLegend(0.15,0.6,0.35,0.85);
  //  leg1->AddEntry(grThr[0][0],"Dead pixel","p");
  //  leg1->AddEntry(grThr[1][0],"Bad pixel","p");
  leg1->AddEntry(grThr[0][0],"FE1","pl");
  leg1->AddEntry(grThr[0][1],"FE2","pl");
  leg1->AddEntry(grThr[0][2],"FE3","pl");
  leg1->AddEntry(grThr[0][3],"FE4","pl");
  leg1->Draw();
  c1->SetBottomMargin(0.12);
  c1->SaveAs("ThresholdDevelopment.pdf");

  TCanvas *c2 =	new TCanvas("c2", "",1200,800);
  grNoi[0][0]->SetTitle("Development of Noise mean");
  grNoi[0][0]->GetXaxis()->SetTitle("");
  grNoi[0][0]->GetYaxis()->SetTitle("Noise [e]");
  grNoi[0][0]->GetYaxis()->SetRangeUser(30,90);
  grNoi[0][0]->GetXaxis()->SetRangeUser(0.,3.);
  grNoi[0][0]->GetXaxis()->SetLabelSize(0.05);
  //  grNoi[0][0]->GetXaxis()->SetBinLabel(8.0, "#splitline{Initial test}{at assembly site}");
  //  grNoi[0][0]->GetXaxis()->SetBinLabel(50.0, "#splitline{Reception}{test}");
  //  grNoi[0][0]->GetXaxis()->SetBinLabel(92.0, "#splitline{After}{cell loading}");
  grNoi[0][0]->GetXaxis()->SetBinLabel(92.0, "");
  grNoi[0][0]->GetXaxis()->ChangeLabel(22,0,-1,-1,-1,-1,"#splitline{Initial test}{at assembly site}");
  grNoi[0][0]->GetXaxis()->ChangeLabel(55,0,-1,-1,-1,-1,"#splitline{Reception}{test}");
  grNoi[0][0]->GetXaxis()->ChangeLabel(99,0,-1,-1,-1,-1,"#splitline{After}{cell loading}");
  grNoi[0][0]->GetXaxis()->SetLabelOffset(0.03);
  grNoi[0][0]->Draw("apl");
  grNoi[0][0]->Draw("apl");
  for(int ii=0;ii<4;ii++){
    grNoi[0][ii]->Draw("samepl");
    //    grNoi[1][ii]->Draw("samepl");
  }
  for(int ii=0;ii<3;ii++){
    tick[ii]=new TLine(0.5+ii,30, 0.5+ii,32);
    tick[ii]->Draw();
  }
  TLegend* leg2 = new TLegend(0.15,0.6,0.35,0.85);
  //  leg2->AddEntry(grNoi[0][0],"Dead pixel","p");
  //  leg2->AddEntry(grNoi[1][0],"Bad pixel","p");
  leg2->AddEntry(grNoi[0][0],"FE1","pl");
  leg2->AddEntry(grNoi[0][1],"FE2","pl");
  leg2->AddEntry(grNoi[0][2],"FE3","pl");
  leg2->AddEntry(grNoi[0][3],"FE4","pl");
  leg2->Draw();
  c2->SetBottomMargin(0.12);
  c2->SaveAs("NoiseDevelopment.pdf");
}
  
    
    
